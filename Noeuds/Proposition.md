Titre : [[Noeuds/Proposition]]
Type : Noeud
Tags : #Noeud
Ascendants : [[Noeuds/Prise de notes]]
Date : 2023-12-05
***

Une proposition est une déclaration ou une phrase qui apporte une précision moins formelle sur un Noeud. 

Une relation est un type de proposition qui établit une relation entre deux Noeuds ou plus. Par exemple, "L'influence de l'éthique spinozienne sur la philosophie morale moderne"  pourrait être une proposition liant les Noeuds "Spinoza" et "Philosophie morale moderne".

La différence entre une proposition est un sous-Noeud est importante. Ainsi, “L'influence de l'éthique spinozienne sur la philosophie morale moderne” n’est pas en soi un Noeud, c’est plus une exploration d’une idée. Mais si on se rend compte que c’est une idée clé du Noeud, on peut la formaliser en tant que Noeud pour en faire un brique de construction. On pourrait alors la renommer “Spinozisme moral moderne”. On considère alors que l’influence de Spinoza est telle que cela a créé un “mouvement”, une “dynamique homogène”. Cela devient alors un Noeud, un Noeud d’étude.

La démarche globale ce serait alors de prendre des notes au fur et à mesure sur une source sous la forme de propositions.

## Propositions

## Sources