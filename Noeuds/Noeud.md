Titre : [[Noeuds/Noeud]]
Type : Noeud
Tags : #Noeud
Ascendants : [[Noeuds/Prise de notes]]
Date : 2023-12-05
***

**Définition :** Un noeud est une idée principale ou une catégorie dans le système de prise de notes. Cela peut être `Spinoza` ou `Matérialisme`.

**Proposition 1 :** Un noeud peut avoir plusieurs sous-noeuds et plusieurs sur-noeuds. Dans Obsidian, les noeuds sont liés entre eux par liens hypertextes.

**Proposition 2 :** Le noeud est indiqué dans Obsidian par un tag #Noeud. Cela permet de voir l’arborescence dans les graphs en recherchant le tag.

**Proposition 3 :** Un noeud peut contenir des propositions et des sources. Le lien peut se faire dans les deux sens.

## Propositions

## Sources