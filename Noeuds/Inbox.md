Titre : [[Inbox]]
Type : Noeud
Tags : #Noeud
Ascendants : [[Noeuds/Prise de notes]]
Date : 2023-12-05
***

L'inbox c'est un lieu où on peut se décharger de nos pensées et de nos inspirations. Il ne faut pas que ce soit un espace contraignant pour ne pas bloquer les réflexions.

Cela correspond à mon [[Utilisation de Notion]]

## Provenance
[Voir la partie Zettelkasten as a dynamic system](https://zettelkasten.de/posts/zettelkasten-building-blocks/)

