MOC : [[Sociologie des techniques]]
Type : Note littéraire
Titre : [[Science, industrie, innovation et société au XIXème siècle]]
Date : 2023-11-08
Tags : #Fini
***

### L'histoire des techniques de la révolution industrielle n'est pas fixe  
« Elles peuvent être bousculées par le choix d’aborder autrement l’industrialisation du XIXe siècle » (Chauveau, 2014, p. 3) 
Il y a plusieurs façons de raconter l'histoire des techiques à cette époque. Cela peut être parler des innovations, de la consommation. Cela peut être lié aux territoires pour permettre l'analyse de l'impact sur l'environnement. Ce dernier exemple reprend les travaux de [[@Technocritiques_ du refus des machines à la contestation des technosciences,François Jarrige]]
[[L'histoire des techniques de la révolution industrielle n'est pas fixe]]

### Place de la science dans l'industrie du XIXè siècle
« Dans les histoires du XIXe siècle industriel, la science occupe en effet une place de choix. » (Chauveau, 2014, p. 4)   
Les savants et inventeurs s'associent de plus en plus avec les industriels et entrepreneurs pour développer de nouvelles techniques.
[[Place de la science dans l'industrie du XIXè siècle]]

### Distinction des inventeurs aux XVIIIème siècle  
« Tout au long du XVIIIe siècle, tant en France qu’en Angleterre, la construction de la figure de l’inventeur s’est effectuée en suivant deux registres, celui de l’académie et celui du marché. » (Chauveau, 2014, p. 5) 

Les inventeurs sont devenus de plus en plus appréciés au long du XVIIIe siècle. Leurs inventions étaient montrés dans les Académies des sciences par exemple. Une fois ces distinctions obtenus, iels pouvaient aller proposer ces inventions sur les marchés.
[[Distinction des inventeurs aux XVIIIème siècle]]

### L'industrie devient le moyen de distinction des inventeurs au XIXème siècle  
« Au XIXe siècle, l’industrie devient le lieu où l’invention est pleinement reconnue, et ce déplacement est lourd de sens. » (Chauveau, 2014, p. 5) 
  
Les industries deviennent le lieu de légitimisation des inventions. Il y a alors une ambiguité. La science perd de sa pureté et l'inventeur de son autonomie.
[[L'industrie devient le moyen de distinction des inventeurs au XIXème siècle]]
### Forme commerciale de légimisation des techniques par l'industrie  
« Le succès commercial et la diffusion légitiment l’innovation » (Chauveau, 2014, p. 5) 
« Le cheminement de l’innovation n’a rien de linéaire et impose de penser en termes de trajectoires, tout en intégrant les multiples facettes de la spécialité pharmaceutique : substance médicamenteuse, produit industriel, objet commercial. » (Chauveau, 2014, p. 6)
"Le succès commercial et la diffusion légitiment l'innovation". Les nouvelles technologies sont commercialisées avant même que les tests soient effectués. C'est l'empirisme du produit sur le marché (son succès et sa diffusion) qui guide les prochaines étapes d'innovation. Il y a parmi ces déterminants les différents métiers créés par ce nouveau marché.
[[Forme commerciale de légimisation des techniques par l'industrie]]
## Référence
[[@Science, industrie, innovation et société au XIX _sup_e__sup_ siècle,Sophie Chauveau]]










