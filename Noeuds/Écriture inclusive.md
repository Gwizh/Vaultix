Titre : [[Écriture inclusive]]
Type : Noeud
Tags : #Noeud
Ascendants :
Date : 2023-12-07
***

## Propositions
[[L’écriture inclusive ne détruit pas la langue française]]
[[L’écriture inclusive visibilise les femmes]]

## Sources