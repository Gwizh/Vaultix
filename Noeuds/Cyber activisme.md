Titre : [[Cyber activisme]]
Type : Noeud
Tags : #Noeud
Ascendants :
Date : 2023-12-07
***

## Propositions

## Sources

### Def
https://en.wikipedia.org/wiki/Hacktivism
https://en.wikipedia.org/wiki/Open-source_governance#History

### Assange et WikiLeaks
https://fr.wikipedia.org/wiki/Julian_Assange

### Sci hub et Cie
https://en.wikipedia.org/wiki/Alexandra_Elbakyan
https://fr.wikipedia.org/wiki/Z-Library
https://fr.wikipedia.org/wiki/Library_Genesis
https://fr.wikipedia.org/wiki/Sci-Hub#En_France

### Aaron
https://www.wired.com/2011/07/swartz-arrest/

### Autres
https://fr.wikipedia.org/wiki/Luxembourg_Leaks#Procès_en_première_instance
https://fr.wikipedia.org/wiki/Antoine_Deltour

