Titre : [[Fixette]]
Type : Noeud
Tags : #Noeud
Ascendants :
Date : 2023-12-27
***

## Graph
tag:#Fixette 
tag:#Fixette -tag:#Source  

## Texte
L’Exposition internationale d’électricité qui s’est déroulée en 1881 marque pour de nombreux historiens du domaine le début de son histoire. Jusque-là, l’électricité n’était que peu utilisée, peu maîtrisée. Le XIXème siècle a été très riche en innovation dans ce domaine mais il n’y avait alors qu’une seule utilisation massive dans la société de cette force. Cette utilisation avait déjà changé les espaces, changé la conception du temps. C’était le télégraphe, inventée dans les années 1830 et utilisée en masse depuis les années 1840. [[Invention du télégraphe dans les années 1830]]. [[Le télégraphe changea la conception du temps]] Prise simplement sous cette forme, l’électricité avait déjà bouleversé le monde en connectant les continents. Parmi les décennies d’innovation de l’électricité, celle de 1870 a été particulièrement impressionnante ([[Les nombreuses innovations de la décennie précédent 1881]]). La bougie de Jablochkoff notamment, inventée et présentée en 1878, a marqué le public par l’apparence de sa lumière. On découvrait alors une lumière blanche, pure, non vacillante comme la flamme. Toutes ces innovations ont motivées la formation de l’exposition de 1881, prévue pour se concentrer sur ce domaine plein de dynamisme.

### Innovation tout de suite adoptée

Rares sont les innovations qui reçoivent directement une Exposition internationale où les buts principaux sont la vulgarisation et la commercialisation d'un produit. 

### Le besoin d’éclairer

Nous sommes constamment bombardés de lumières, le jour ou la nuit, dehors ou chez nous. Nous sommes habitués à une lumière unie, blanche, constante, froide. Nous sommes habitués à nos soirées. Nous sommes habitués aux usines et entreprises qui fonctionnent 24h/24. Nous sommes habitués à être détachés du cycle du Soleil. Nous imaginons aussi que ces lumières électriques sont nécessairement meilleures que ce qui existait avant, une vie éclairée par des flammes. La marche du progrès nous le fait penser en tout cas. On semble imaginer que plus le temps avance et plus nos technologies se doivent d'être meilleures. Et alors on ne réfléchit plus à la lumière électrique, cette technologie qui est partout et qui n'a pas besoin d'autorisation pour s'inviter en toutes circonstances. Qui ne nous demande même plus avant de s'allumer avec les capteurs de présence.

Je trouve personnellement qu'on ne questionne pas assez nos besoins et ces changements de technologies. Si nous avons fini par privilégier une technologie par rapport à une autre, je considère que cela peut-être pour des raisons arbitraires voire même fondamentalement mauvaises. Aujourd'hui nous vivons avec un réseau électrique immense qui permet un éclairage intense. Je questionne ce besoin d'un réseau aussi gigantesque, je questionne le besoin de cet éclairage omniprésent.

Questionner ne renvoie pas nécessairement à rejeter. Je ne dis pas que nous devons choisir un autre moyen d'éclairage ou que nous ne devons plus nous éclairer. Je me demande simplement si on aurait pu faire autrement, si notre conception de l'éclairage aurait pu être drastiquement différente de par un ou plusieurs changements de curseurs. Je me demande si cet éclairage aurait pu être retenu, cantonné à des usages précis. Je me demande si on aurait pu avoir un XXIe siècle au rythme du Soleil. 

#### Un éclairage centralisé

Nous n’imaginons pas aujourd’hui une maison sans éclairage électrique centralisé. Notre premier réflexe en entrant dans une pièce est d’appuyer sur un interrupteur qu’on devine juste à côté de la porte. En faisant ainsi, la pièce est alors éclairée immédiatement. Nous commençons même à être habitués aux capteurs de présence et à l’éclairage automatique des pièces.

Pourtant, cet éclairage centralisé semble être loin d’être évident pour les contemporains du XIXe siècle. Les bougies et lampes à huile n’était pas centralisées, il s’agissait d’objets uniques qu’il fallait recharger et allumer individuellement. Seul le gaz avait cette caractéristique. Le gaz dépendait d’un réseau auquel il fallait se connecter. Il fallait ensuite allumer à l’aide de robinets ou interrupteurs l’afflux en gaz. Enfin seulement, on retrouvait les méthodes d’antan lorsqu’il fallait allumer les lampes à gaz.

### Exemple du téléphone

Une des autres grandes attractions de l’Exposition de 1881, le téléphone, est un exemple de l’effort mis en œuvre pour créer un marché. Ce qui apparaît des retours du public quant à l’apparition du téléphone, c’est que les gens considéraient qu’il s’agissait plus d’un gadget que d’un réel outil pratique. Le fait de parler à distance était intéressant bien sûr mais le fait de parler en direct semblait superflu.

Ainsi, pour vendre le téléphone, pour montrer son intérêt, les exposants ont privilégiés une utilisation qui pourrait nous sembler originale. Au lieu de mettre à disposition des téléphones pour appeler des gens à l’autre bout de la ligne, les téléphones ont été utilisés pour faire écouter un opéra qui se déroulait plus loin dans Paris. La sensation devait être magique pour l’époque en effet mais elle est loin de la manière dont les industriels voulaient réellement vendre le produit plus tard. Loin de l’idée que les utilisateurs d’alors du télégraphe n’attendaient qu’une chose : réellement entendre la personne en direct, ce besoin a du être créé, façonné. Communiquer en direct n'était pas évident.
[[Le téléphone n'intéresse que peu les Français, c'est l'Exposition de 1881 qui va changer les perceptions]]

## Propositions
```query
tag:#Fixette tag:#Proposition
```
### L'électricité était vu comme le symbole de la modernité de la Belle Époque
"L'électricité a laissé au gaz la tâche banale d'éclairer et elle s'est donné mission d'illuminer" (dans Étonner)

### Techno-nationalisme sur l'éclairage public des métropoles de la Belle Époque

### Le travail de nuit contesté par les ouvriers de la Belle Époque

### La pollution lumineuse était déjà identifiée au milieu du XIXè siècle

### Edison vs Tesla - concours du plus spectaculaire

### Exposition de Paris de 1881 - événement fondateur de l'électricité

### L’incendie de l’Opéra comique - l’éclairage au gaz interdit dans les théâtres

[[L'incendie de l'Opéra comique]]


### Les lampes à incandescence représente l'introduction de l'électricité dans les foyers

### L'éclairage est l'application de l'électricité qui a le plus marqué le public lors de l'exposition de 1881

### L'élaboration de l'allégorie de la Fée Électrique

### Thomas Edison et l'exposition de 1881

### Les débats autour de l'électricité en 1881 tourne autour de la sécurité des équipements 
### L'électricité revêt d'un pouvoir magique, divin

En 1881, Armand Silvestre, futur auteur du livret de la cantate _Le Feu céleste_ écrite par Camille Saint-Saëns pour l’Exposition de 1900, écrit : « Tout est conquis dans la nature : / Au ciel, restait à conquérir / Sa flamme redoutable et pure, / Le feu qui fait vivre et mourir ! / Aigle s’envolant de son aire, / Volta lui ravit le tonnerre / Et l’apporte à l’humanité. / À servir l’homme condamnée, / Par lui la foudre est enchaînée/ Et s’appelle Électricité [45](https://www.fondapol.org/etude/une-civilisation-electrique-1-un-siecle-de-transformations/#note24_11)

### Les expositions sont là pour créer un marché et pour tester l'émerveillement d'un public néophyte

### Fièvre du "tout électrique" à la belle époque

### Rapprochement clair entre Lumières et lumière, entre obscurantisme et obscurité

### Rapprochement entre le tout électrique de le Belle Époque et le tout numérique actuel

### Ferranti et sa démonstration audacieuse de la résistance de ses câbles

### Nous avons sur-utilisé le courant alternatif suite à sa victoire sur le courant continu
Quoi de Neuf

### Nous avons grandement financé les voitures électriques en pensant que c'était l'avenir 
Quoi de Neuf

### L'adoption des inventions n'est pas du tout aussi spectaculaire que sa publicité

## Sources
### Des luttes pacifiques. Les expositions de la seconde moitié du XIXe siècle
[[Des luttes pacifiques. Les expositions de la seconde moitié du XIXe siècle]]