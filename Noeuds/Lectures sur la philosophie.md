| Auteur                  | Titre                      | Année     | MOC             | Lecture           |
| ----------------------- | -------------------------- | --------- | --------------- | ----------------- |
| [[René Descartes]]      | [[Discours de la Méthode]] | 1637      | [[Philosophie]] | 04/2022           |
| [[Albert Camus]]        | [[L'homme révolté]]        | 1951      | [[Philosophie]] | 08/2022 - 11/2022 |
| [[Friedrich Nietzsche]] | [[Le Gai Savoir]]          | 1882-1887 | [[Philosophie]] | 11/22 -                   |

