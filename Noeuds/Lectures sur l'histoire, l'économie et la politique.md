| Auteur                                | Titre                                             | Année | MOC                                     | Lecture   |
| ------------------------------------- | ------------------------------------------------- | ----- | --------------------------------------- | --------- |
| [[Jean-Christophe Cassard]]           | [[L'âge d'or Capétien (1180-1328)]]               | 2011  | [[Histoire]]                            | 02/2022 - |
| [[Serge Bernstein]] [[Pierre Milza]]  | [[Histoire de la France au XXe Siècle - Livre 3]] | 1990  | [[Histoire]] [[Politique]] [[ECONOMIE]] | 05/2022 - |
| [[Jacques Rougerie]]                  | [[La Commune de 1871 - Que sais je]]              | 2021  | [[Histoire]] [[Politique]]              | 07/2022   |
| [[Jean Boncoeur]] [[Hervé Thouément]] | [[Histoire des idées économiques - Tome 1]]       | 1989  | [[Histoire]] [[ECONOMIE]]               | 10/22 -           |

