Titre : [[Murray Bookchin]]
Type : Noeud
Tags : #Noeud
Ascendants : [[Anarchie]], [[Écologie]]
Date : 2023-06-08
***

## Propositions
Franco-russe et juif.

Murray Bookchin, né le 14 janvier 1921 et mort le 30 juillet 2006, est un philosophe, militant et essayiste écologiste libertaire américain. Il est considéré aux États-Unis comme l'un des penseurs marquants de la [[Nouvelle gauche]] (New Left).

Il est le fondateur de l'[[Écologie sociale]], école de pensée qui propose une nouvelle vision politique et philosophique du rapport entre l’être humain et son environnement, ainsi qu'une nouvelle organisation sociale par la mise en œuvre du municipalisme libertaire.

L'influence de ses idées sur le dirigeant kurde [[Abdullah Öcalan]] a conduit à l'élaboration du confédéralisme démocratique, modèle adopté par le Parti des travailleurs du Kurdistan (PKK) à partir de 2005, puis par le Parti de l'union démocratique (PYD) en [[Syrie]], où il connait un début de mise en œuvre dans les cantons du [[Rojava]].

> « Faisons l’impossible, car sinon nous aurons l’impensable ! »
## Sources
