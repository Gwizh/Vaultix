Titre : [[Noeuds/Prise de notes]]
Type : Noeud
Tags : #Noeud
Ascendants : [[Notes/Prise de notes]]
Date : 2023-11-06
***
## Philosophie globale

L’idée est de pouvoir travailler sur des Noeuds en développant des idées à travers l’arborescence.

## Noeuds
Il y a plusieurs Noeuds clés à cette méthodologie. Les voici avec un résumé :
[[Noeuds/Noeud]] : Noeud d'étude
[[Noeuds/Proposition]] : Idée lié à un ou plusieurs Noeuds
[[Source]] : Source d'où proviennent des Noeuds ou propositions

## Outils
[[Utilisation d’Obsidian]]
[[Utilisation de Notion]]
[[Utilisation de Zotero]]

- #Question Puis-je retrouver toutes les questions d'un Noeud ?

## Graph
Recherches possibles :
tag:Noeud OR tag:Proposition  
tag:Noeud OR tag:Proposition OR tag:Source
tag:Proposition OR tag:Source OR file:"Fixette"
## Propositions

## Sources