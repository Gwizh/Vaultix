Titre : [[L'incendie de l'Opéra comique]]
Type : Noeud
Tags : #Noeud #Fixette 
Ascendants : [[Fixette]]
Date : 2023-12-29
***

## Propositions
### Marcellin Berthelot a été critiqué quant à sa phrase sur l'Opéra comique
[[Marcellin Berthelot a été critiqué quant à sa phrase sur l'Opéra comique]]
Monsieur Steenackers, député de la Haute-Marne interpelle, lors d'une séance houleuse à l'Assemblée Nationale le 12 mai 1887, Monsieur Berthelot ministre des Beaux Arts. Il dénonce la vétusté du bâtiment, son extrême exigüité, rappelant les dangers encourus tant par le public que par le personnel, ainsi que les nombreux rapports établis sur le sujet. La réponse du ministre surprend : « Nous pouvons considérer comme probable que l'Opéra-Comique brûlera, c'est un fait statistique. » Tout le monde conservait en mémoire les incendies survenus ces dernières décennies (Opéra Le Peletier 1873 ; Montpellier 1876 ; Nice et Ring Theater de Vienne en 1881 avec ses quatre cent soixante-dix morts). 

### L'incendie de l'Opéra comique de 1887 tombe en plein remaniement ministériel
[[L'incendie de l'Opéra comique de 1887 tombe en plein remaniement ministériel]]
Le 17 mai 1887, René Goblet remet la démission du Gouvernement au président de la République, Jules Grévy.

Le 25 mai 1887, l'incendie de l'Opéra comique survient.

Le 30 mai 1887, Jules Grévy nomme Maurice Rouvier président du Conseil des ministres. Le nouveau gouvernement est formé.

### Description de l'incendie de l'Opéra comique par artlyrique.fr
[[Description de l'incendie de l'Opéra comique par artlyrique.fr]]
```
L'incendie de l'Opéra-Comique a été raconté par la presse quotidienne tout entière ; il nous suffira de rappeler qu'il a eu lieu subitement le mercredi 25 mai, au moment même de la représentation de Mignon, et pendant le premier acte de cette pièce. A 20 h 55, sous l'action d'un bec de gaz, le feu prit tout à coup aux frises de la scène ; en quelques secondes, les décors se trouvèrent enflammés, et le foyer ne tarda pas à acquérir une intensité et un développement que rien alors ne pouvait arrêter.

A 22 heures la toiture et le chapeau de la salle s'écroulent. Le feu fera rage jusqu'à 1 heure du matin, attirant quelques deux cent mille curieux, que l'infanterie et la Garde républicaine devront contenir. Au matin du 26 un spectacle de souffrance et d'horreur s'offre aux sauveteurs. Lourd bilan : cent dix morts environ dont seize membres du personnel (danseuses, choristes, ouvreuses...) et plus de deux cents blessés. Des scènes éprouvantes d'identification se déroulent aux ambulances ouvertes rue Drouot, à la Bibliothèque Nationale ainsi qu'à l'Hôtel-Dieu. La presse se déchaîne, les compositeurs et les chansonniers s'emparent des faits. Les chambres votent l'octroi de dons en aide aux familles des victimes et des galas de soutien s'organisent dans les théâtres. Des funérailles nationales pour les vingt-deux victimes non identifiées ou non réclamées ont lieu à Notre-Dame le 30 mai 1887.
```

### Les dons pour les victimes de l'opéra comique font réagir suite aux récents morts dans les mines de Saint-Etienne 
[[Les dons pour les victimes de l'opéra comique font réagir suite aux récents morts dans les mines de Saint-Etienne]]
```
Certains considérèrent en effet que la justice sociale était bien mal respectée : le journal satirique _Le Grelot_ fit à la une, sous le titre « Le cabotinage de la charité », un cinglant parallèle entre les largesses des mélomanes parisiens et la souscription, peu suivie d’effet, lancée en faveur des soixante-dix-neuf mineurs, victimes en mars d’un coup de grisou à Saint-Etienne.
```

### Images de l'incendie
https://www.parismuseescollections.paris.fr/fr/musee-carnavalet/oeuvres/ruines-de-l-opera-comique-apres-l-incendie-du-15-mai-1887#infos-principales

## Sources
https://artlyriquefr.fr/dicos/Opera-Comique%20incendie.html
https://www.furet.com/media/pdf/feuilletage/9/7/8/2/0/1/3/4/9782013422161.pdf
https://musee-mine.saint-etienne.fr/sites/default/files/ckeditor_uploads/dossier_les_dangers2017.pdf
https://www.diapasonmag.fr/a-la-une/un-autre-25-mai-1887-lincendie-de-lopera-comique-6444.html