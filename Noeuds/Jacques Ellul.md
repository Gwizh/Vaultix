Titre : [[Jacques Ellul]]
Type : Noeud
Tags : #Noeud
Ascendants : [[Sociologie des techniques]], [[Karl Marx]]
Date : 2023-11-03
***

Jacques Ellul, né le 6 janvier 1912 à Bordeaux et mort le 19 mai 1994 à Pessac, est un historien du droit, sociologue et théologien protestant libertaire français. 
## Propositions

## Sources