Titre : [[Les sondages sont-ils dangereux pour la démocratie]]
Type : Source
Tags : #Source
Ascendants :
Date : 2023-12-09
***

## Propositions
## Résumé
[[C ce soir]] avec [[François Bégaudeau]]
Son dernier livre [[Notre Joie]], est une conversation avec un garçon d'ED appelé M. Pour lui l'ED ne veut rien dire mais il définit l'autre camp à des termes comme l'[[Identité]] par exemple. Begaudeau définit sa pensée comme [[Autoritarisme|autoritaire]], identitaire et [[Idéaliste|idéaliste]]. Formé avec [[Alain Soral]]. 

Begaudeau dit qu'on peut parler avec tous le monde pour pouvoir comprendre. La vacuité n'est pas un monopole de l'ED. Il y a qq chose de très puéril dans les élections électorales. [[Éric Zemmour]] est un enfant.

Maladie du commentaire. Langue autoritaire = langue qui se nourrit d'elle-même. Internet = logosphère. 

#### Le Rassemblement National n'est qu'un parti électoral
[[Le Rassemblement National n'est qu'un parti électoral selon Bégaudeau]]
Le [[Rassemblement National]] n'est qu'un parti électoral, il n'a pas de militants, ne défendent pas de projets en dehors des élections, ne participent pas aux manifestations. Il ne joue que sur le jeu des élections pour survivre. 

La joie est un fait politique. Il parle de [[Bernard Friot]] et du [[Déjà-là communiste]] comme un vecteur de joie commun. [[Clément Rosset]] parle de la joie aussi comme une bonne nature. 

En 1871, le premier geste de la [[IIIe République]] était de conjurer la [[Commune de Paris]]. 

#### La politique au quotidien
[[La politique au quotidien]]
Le mode de scrutin est important pour permettre aux gens de participer à la politique et la démocratie mais ça ne suffit pas. Encore une fois, le vote en soi ne permet rien. Ce qui compte c'est la manière de faire de la politique et les mandats par exemple. Une femme de ménage ou un ouvrier ne peut pas aujourd'hui entrer en politique par le vote et conserver son travail. On ne peut pas associer les deux. Il faudrait pouvoir faire de la politique tout en étant travailleur. 

#### Le business des sondages selon Begaudeau
[[Le business des sondages selon Begaudeau]]
Le nombre de sondage publié l'année de la présidentielle est passé de 120 à 560 entre 2002 et 2017. Bégaudeau parle de produits, à la fois pour les sondages que pour l'élection et son traitement. Avant, les élections étaient commentées un mois avant, maintenant c'est plutôt 8-9. C'est du business. Bégaudeau pense qu'il faut pas seulement faire de la pédagogie dessus, il faut le mettre hors d'état de nuire. Et pour ça, il faut mettre hors d'état de nuire le système qu'il appelle totalitarisme marchand. La politique devient marchandise.
#MIE Je n'ai en effet jamais aimé les sondages mdr

## Référence

## Liens 