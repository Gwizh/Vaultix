Titre : [[La fée et la servante]]
Type : Source
Tags : #Source, #Fixette
Ascendants :
Date : 2024-01-27
***

## Propositions
### Rapprochement pasteurisation et éclairage électrique par Corbin - p7
[[Rapprochement pasteurisation et éclairage électrique par Corbin]]
Belle idée de Corbin
L'éclairage électrique révèle crûment. Il conforte la révolution pastorienne. 
"Tout deux conduisent à traquer le désordre et le sale, dès lors assimilés"
"À elles deux, elles imposent peu à peu le lisse."

### La domestication de la foudre fascine le XVIIIe - p20
[[La domestication de la foudre fascine le XVIIIe]]
L'expérience de l'électricité fascine les savants et élites en France.
Voir image p21
C'est un spectacle de cour. Louis XV fait passer "la commotion électrique à travers [80] militaires en même temps."
La Révolution fait un constat par Robespierre, celui que les Hommes vont dominer les cieux grâce à la puissance de la foudre.
Voir image p23 !

### L'électricité dans la médecine et pseudo-médecine du XVIIIe - p 23
[[L'électricité dans la médecine et pseudo-médecine du XVIIIe]]
Il y a confusion entre magnétisme et électricité. Il y a des cures (soin collectif). "Toute maladie provient d'une obstruction à la circulation du fluide vital, donc la guérison est facile".
Le magnétisme est utilisé comme remède aux maladies nerveuses mais aussi aux problèmes d'érections. La volupté semble avoir une place importante dans ces sessions.
Le magnétiseur doit avoir un rapport physique avec le (souvent la) malade.

### Invention du télégraphe dans les années 1830 - p 33
[[Invention du télégraphe dans les années 1830]]
Le télégraphie a été permise par la pile de Volta et l'électroaiment. C'est aux US que le télégraphe fut inventé en 1830. C'est donc contemporain du rail !
Aux R-U, où les réseaux ferroviaire sont importants, la communication rapide de l'information est cruciale. C'est donc là-bas que se développe cette technologie. Le télégraphe électrique correspond à 1 besoin.

### L'installation de la télégraphie électrique a été ralentie en France par la télégraphie aérienne et son utilisation - p36
[[L'installation de la télégraphie électrique a été ralentie en France par la télégraphie aérienne et son utilisation]]
Mais pas que. La lente appariation du rail en France en est aussi la cause.
Mais la télégraphiue aérienne existait déjàç depuis 1794. C'était un ensemble d'avant-postes munis de sémaphores. D'abord pour l'armée mais ensuite pour l'administration. La télégraphie inquiétait l'État, et si on coupait les fils ?

### La télégraphie électrique est d'abord conservée par l'État, avant d'être étandue au public - p38
[[La télégraphie électrique est d'abord conservée par l'État, avant d'être étandue au public]]
La première ligne en France a été construite en 1845. En 46, une ligne existe de Paris à Lille. Ce n'est qu'en 50 que le ministre de l'intérieur (chargé de la télégraphie d'État) ouvre la technologie au public. Le rail et le télégraphe se développe ensuite très vite au Second Empire pour intérêt commercial.

### La pose de la ligne transatlantique entre 1857 et 1866 - p41
[[La pose de la ligne transatlantique entre 1857 et 1866]]
On trouva dans les années 40 la gutta-percha, substance semblable au caoutchouc, qu'on utilisa vite pour isoler et protéger les cables. C'est von Siemens qui met au point la machine pour enrober les cables. En 57, le projet est lancé mais est interrompu en 58 par la guerre de Sécession. Il est reprit en 65 et achevé en 66. On a toute l'histoire dans les journaux 
-> Cyrus Field.

### Le télégraphe changea la conception du temps - p40
[[Le télégraphe changea la conception du temps]]
Le plus gros changement qu'a causé le télégraphe fut pour la presse et la communication. S'instaure alors une véritable chasse à la nouvelle. En 71, le Parlement est connecté au télégrape pour retransmettre les comptes rendus. 75 pour le Congrès de Versailles. Début du reporter
 
### Le téléphone n'intéresse que peu les Français, c'est l'Exposition de 1881 qui va changer les perceptions - p47
[[Le téléphone n'intéresse que peu les Français, c'est l'Exposition de 1881 qui va changer les perceptions]]
Les Français furent peu réceptifs à l'idée de dialoguer en direct (contrairement aux américains, anglais et allemands). L'idée qui a été utilisée pour susciter l'attention fut de transmettre en direct des opéras.
La marche du progrès et les idéaux de démocratisation furent utilisés pour montrer les capacités égalisatrices de cette technologique
Citations p50 !

### Les nombreuses innovations de la décennie précédent 1881 - p58
[[Les nombreuses innovations de la décennie précédent 1881]]
1871 : Dynamo de Gramme
1873 : Transport d'électricité par Fontaine
1875 : régulateur pour lampes à arc de Brusch
1876 : Bougie de Joblochkoff
1879 : Lampes incandescentes de Edison et Swan
1879 : Siemens et la locomotive électrique

### Création d'un véritable lobby électrique de 1875 à 1881 selon Beltran et Carré - p 59
[[Création d'un véritable lobby électrique de 1875 à 1881 selon Beltran et Carré]]
Au début dans les Annales de Télégraphie, les articles sur les nombreuses innovations passionnant savant comme hommes d'affaires, comme Herz. Admirateur d'Edison, il souhaite voir surgir autant d'innovations en France
-> Syndicat français d'Électricité
Lui et d'autres sont sont prolifiques
76 : L'Électricité est né (le journal)
79 : La Lumière Électrique
Ils participent à la participation de 1881.

### Conception avec l'État de l'Exposition de 1881 - p61
[[Conception avec l'État de l'Exposition de 1881]]
Adolphe Cochery devient ministre des Postes et Télégraphes en 1879. Il semble être proche du "lobby" électrique (Voir [[Création d'un véritable lobby électrique de 1875 à 1881 selon Beltran et Carré]]) qui s'est formé depuis 75
En octobre 80, il remet une lettre au Président Jules Grévy suggérant la tenue d'une Exposition dédiée aux nouvelles innovations du domaine.
Des moyens sont dégagés, du public comme du privé. 
En moins d'un an, les divers organismes nécessaires sont dégagés. Jules Grévy nomme Gaston Berger.
Financement p64 !

### Edison de l'Exposition de 1881 vu par Beltran et Carré - p68
n15 montre comment était présenté l'élec. 16 Fox
"Stratégie résolue de publicité"
-> Vendre un produit mais surtout l'image d'un produit. Sorcier de Menlo p69
-> force d'Edison dans la vente d'un système complet à l'image du gaz. n19!
### Quelques chiffres de l'Exposition de 1881 de Beltran et Carré - p64
[[Quelques chiffres de l'Exposition de 1881 de Beltran et Carré]]
Du 10/08 au 20/11, 900 000 visiteurs sont allés au Palais de l'Industrie.
1768 exposants, 16 pays (55% des exposants étaient français)

### Faits marquants des trams et métros électrique de 1880 à 1900 par Beltran et Carré - p75
[[Faits marquants des trams et métros électrique de 1880 à 1900 par Beltran et Carré]]
France encore 1 fois "en retard" par rapport aux US. 86% en Français des trams sont tractés par animaux. 76% élec aux US
Les villes françaises sont trop petites -> trams peu rentables, forcément public.
Métro qu'à Paris pour la même raison.

### Méfiance passagère face au tram électrique et au métro chez les Parisiens du XIXe - p89
[[Méfiance passagère face au tram électrique et au métro chez les Parisiens du XIXe]]
On se moque des trams qui passent vide mais lorsqu'on est en retard ça devient intéressant. C'est ainsi qu'on apprivoise la vitesse.
Pourtant, il y a eu de nombreux accidents comme celui de 1903 !
--> Rapprochement avec l'Opéra Comique

### Différence de sensibilité vis à vis de lumière dans les métros entre les Français et les Anglais à la fin du XIXe - p89
[[Différence de sensibilité vis à vis de lumière dans les métros entre les Français et les Anglais à la fin du XIXe]]
Il fallait aux Parisiens de la lumière dans le métro là où les Londoniens s'accommodaient de l'obscurité.

### L'éclairage électrique a été tout de suite été préféré au gaz dans les parcs pour des raisons environnementales - p96
[[L'éclairage électrique a été tout de suite été préféré au gaz dans les parcs pour des raisons environnementales]]
On savait que le gaz polluait les sols où passait les tuyaux - n2
Le sol devenait noir, infecte et les arbres mouraient. 
C'est pour ça que les parcs furent les premiers lieux de l'éclairage électrique. On disait que la couleur de sa lumière allait mieux avec le vert l'herbe.

### L'éclairage électrique a su créer un symbole et un besoin dans les années 1880 - p96
[[L'éclairage électrique a su créer un symbole et un besoin dans les années 1880]]
Démonstration politique au 15 aout napoléonien et au 14 juillet républicain. Représente les Lumières pour ces derniers.
C'est aussi un marqueur social jusqu'à lasser. n4
De nouveau la citation comme quoi l'électricité illumine - n7

### Daniel Halévy imaginant l'attrait des villes par leurs lumières - p95
Il pense que l'humain a un besoin de lumière ! Il y voit une des explications de l'exode rural. "L'Homme a horreur de la nuit et si les paysans viennent à la ville, c'est beaucoup parce qu'ils y trouvent la clarté."

### Philippe-Auguste, Rambuteau et Haussmann voyaient en la lumière un instrument de sécurité - p95
Surcroit d'éclairage artificiel.
Créer une ville lumière sécuritaire.

### En 1889, les lampes électriques ont rattrapées en nombre le gaz - p98
1150 lampes à arc, 10000 lampes à incandescence.
Rapport municipal de 1889.
La lumière électrique éclaire plus et cela semble satisfaire plus.
### La lumière électrique a servi pour les trucages au théâtre dès 1846 - p100
Effets de scène assez simples mais spectaculaires : soleil levant, arc-en-ciel, éclairs.
"L'aspect messianique de la lumière divine."
Effets de filtres colorés. Fils invisibles reliés à une pile. Fontaines électriques dès 1853.
### Beltran évoque la transition à l'élec dans les théâtres des années 1880 - p102
Les scènes de théâtres très inflammables. Rouen en 1878, Vienne en 1881 (500 morts) et Opéra-Comique en 1887.
Électricité rendue obligatoire à la fin des années 80 ! n30 !
Opéra de Paris en premier. Remplacer 8000 becs de gaz. Véritable usine électrique au sous-sol.
### Le gaz a aussi été remplacé des théâtres pour la pollution de l'air selon Beltran - p104
Dégagement d'acétylène quand la combustion n'est pas parfaite, particules qui se reposent sur les murs et sur les peintures
-> Théâtres, opéras et musées.
### En 1889, l'essentiel de la production électrique parisienne est auto-générée - p106
Tableau p107 !
Seul 29% vient des stations centrales, le reste est produit sur place.
-> Puissances publiques.
### Utilisation industrielle et militaire de l'éclairage électrique dans les années 1850-80 - p107
1863 -> phares du Havre
Tour Eiffel, lumière visible depuis Orléans. Navires, pour pouvoir naviguer de nuit.
p108 Anecdote Bretagne et statue de la Liberté !
Militaire : surveiller les déplacement ennemis
Construction : Faire travailler plus tard le soir (Notre Dame 53, Expo de 78)
Travail de nuit peut doubler l'efficacité du travail
### Utilisation de l'éclairage électrique dans les mines au XIXe selon Beltran - p109
Les galeries des mines pouvaient être éclairées par l'électricité.
Mais pb d'isolation des lampes ne protègent pas du grisou. Donc surtout dans les mines métalliques ou les ouvrières dans un 1er temps.
"Surveillance plus sûre"
### Liberté, égalité et fraternité et l'éclairage électrique - p113
Je t'en supplies, vas lire !!!!!!!!!!!!!!!!!!
### Espoir que l'électricité pourrait faire revivre les petits ateliers à la fin du XIXe - p115
"Conservatisme social" de la volonté de retrouver les ateliers ruraux d'antan.
Au début du siècle, 80% en activités domestiques mais seulement 15% à la fin du siècle.
Espoir mis sur les petits moteurs indépendants.
Recréer la cellule familiale détruite par les grandes usines.
### Marx, Lénine et Engels voyaient en l'électricité une accélération du capitalisme bourgeois - p119
Marx n'a pu trop en parler mais Engels pensait que l'électricité allait augmenter le contrôle des bourgeois sur les ouvriers. Globalement pas contre la machine mais contre l'exploitant. Lénine voyait dans les grandes sociétés électriques le symbole des grandes firmes capitalistes internationales.
### Nouvelle efficacité et rationalisation dans les usines grâce à l'électricité des années 1880s - p121
Décentralisation de la production, réduire l'écart entre grosses et petites entreprises. Meilleurs rendements.
Transformation radicale de l'atelier -> Fordisme et efficacité.
Appareils d'enregistrement et rationalisation de la production
### Houille verte et revitalisation des campagnes - p124
Henri Bresson a créé le terme houille verte pour parler de l'hydraulique. Etude statistique pour privilégier l'hydraulique dans les départements de campagne.
Entre 1901 et 1914, le nombre de métiers de Saint-Etienne stagna (4730 à 4798), doubla dans la Loire (2600 contre 1372) et décupla dans la Haute-Loire (440 contre 434).
### Conservatisme social, électricité et travail des femmes - p127
L'usine a fait sortir les femmes de son domicile pour la faire travailler à l'usine. Elles quittèrent leur rôle traditionnel. L'électrification a nourrit des espoirs de production domestique pour qu'elles retrouvent ce rôle
Lire p127
### Malgré les espoirs, l'électricité n'a pas permis une décentralisation - p128
Le modèle de l'ouvrier quittant l'usine pour travailler chez lui est resté très théorique.
Il n'a existé que dans le cas d'entreprises moyennes, voulant échapper aux contrôles (p130) ou cherchant une main d'oeuvre rurale bon marché.
Perrot et Marx p130
### Relayer les dangers de l'électrification de New_York à Paris à la fin du XIXe - p136
Réseau tellement dense qu'il entraîne des morts chaque semaine. Popularisation des faits divers.
Le scandale plait et les industriels jouent sur la sécurité et cachent les accidents.
### Les accidents dus à l'électricité qui passionnent le public concernent les consommateurs et non les travailleurs - p140
Dès 1888, il y a eu des accidents sur des passants qui choquent. Thèse de doctorat n7.
Accidents impressionants et mystérieux.
### Peur et fascination d'un arme électrique dans les 1890s - p143
L'exécution électrique menée par Edison fut croire à l'apparition proche d'une arme électrique. Roman p144 !
L'électrique est le nucléaire de l'époque pour être anachronique au point qu'on imagine une arme qui puisse détruire des villes à distance.
Commplot imaginant Edison offrant une telle arme aux Allemands.
n14 !!
### Importance de la faible intensité de la lampe d'Edison - p147
Beaucoup pensent à l'époque que l'éclirage électrique ne pourra s'imposer car elle est trop intense, c'est le cas d'Haussmann par exemple qui sont absolument convaincus !
C'est une peur d'être éblouie, une peur justifiée par des scientifiques de l'époque.
### Peur que l'électricité apporte trop de confort, trop de luxe, ramolissement et dénaturation - p149
Sybaratisme croissant
Critique du progrès. La lumière électrique trompe, change les rythmes traditionnels. On oppose l'authentique au technique. On imagine dans l'éclairage électrique le déclenchement d'instincs bas, les sexologues s'inquiètent du charme qu'il donne aux peripatétitiennes. 
Cet éclairage provoque le désir !
### Romans de Robida crystallisent les peurs de la vie électrique - p163
Peur de la maîtrise de l'électricité comme changement complètement le monde. Perte de la nature.
Création d'une réserve en Bretagne
A lire absolument !!!
### Le service électrique était peu sûr dans les années 1880s - p157
Beaucoup d'interruptions et d'incidents, beaucoup de déception.
Tjs aux frais du service public qui était las d'avoir des pannes.
Eclaire toujours trop ou pas assez.
En 1900, l'électricité était à l'origine de 72 sinistres à Paris contre 12 pour le gaz, alors que les deux systèmes étaient loins d'avoir le même poids dans la capitale...
### Plus grand risque d'incendie de l'électricité que du gaz en 1890 - p159
28 incendies par l'électricité contre 20 pour le gaz n5
Très nombreux accidents spéctaculaires à cause de l'électricité. L'incendie du Grand Café -> enquête 
Le Théâtre-Français
Incendie du Bazar de la Charité de 1897 
1903 !!
### Sabotages de l'électricité à la Belle-Époque - p161
De nombrux campagnards coupaient les fils électriques pour résister à cette technologie.
Émile Pataud à la tête du syndicat des électriciens. Prince des ténèbres avec Émile Pouget. Rédaction de : "Comment nous ferons la Révolution ?"
-> Grand Soir (Lourd de sens pour des électriciens)
-> Se révélaient pivots du mouvement socialiste de l'époque.
### La grande grève de 1907, Paris plongée dans le noir - p164
Fin des concessions des 6 secteurs parisiens. Le soir du 7 mars, Paris a été complètement éteinte.
Immense impact médiatique car réelle paralysie. Clémenceau a envoyé les soldats pour remettre en marche les usines.
Autes grèves de 1907 à 1910, conséquences sur le refus de centralisation. Augmentation du nombre d'installations indépendantes.
Anecdote du Roi du Portugal de 1907.
### Électricité réservée aux services publics et aux particuliers aisés - p169
Technologie chère et plus chère à Paris quèà Londres ou Berlin (+50 à 75% !)
Cela refroidit les enthousiastes En 1898, une douzaine de particuliers consommaient 2/3 de l'électricité.
### Législation frileuse sur l'électricité en 1888 - p 171
Décret du 15/05/1888, premier texte officiel sur les usages de l'électricité, parlait surtout des risques et des graves accidents.
Réglementations spéciales qui nécessitaient de faire vérifier toute nouvelle installation.
### L'Eve future de Villiers vue par Beltran - p174
Personnage central du roman est une machine qui se substitue à une femme.
Cette femme est créée par Edison pour ressembler à la femme du client mais avec l'intelligence en +.
Sorcier de Menlo-Park comme mi-homme, mi-Dieu
Oeuvre scientiste et mystique
Alire ! n3
### Tout électrique : agriculture, transport maritime et automobile notamment - p182
De très nombreuses applications ont été pensées sans finalement y avoir des débouchés. Véritable frénésie dans les années 1880-1890.

## Source
[[@La fée et la servante_ la société française face à l'électricité, XIXe-XXe siècle,Alain Beltran, Patrice-Alexandre Carré]]