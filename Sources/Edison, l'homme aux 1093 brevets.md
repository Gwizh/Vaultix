Titre : [[Edison, l'homme aux 1093 brevets]]
Type : Source
Tags : #Source
Ascendants :
Date : 2024-02-02
***

## Propositions
### Zoummeroff note qu'Edison a reçu la médaille d'or et le diplome d'honneur de l'Exposition de 1881 - p249
[[Zoummeroff note qu'Edison a reçu la médaille d'or et le diplome d'honneur de l'Exposition de 1881]]
"Quel pied de nez pour l'arc électrique"
La Jumbo a été amenée à Paris avec du retard mais "ce n'était pas indispensable".
Parle de l'Opéra de Paris

### L'Exposition électrique au Crystal Palace de Londres suivant celles de Paris de 1881 a connu un moins grand succès - p 249
[[L'Exposition électrique au Crystal Palace de Londres suivant celles de Paris de 1881 a connu un moins grand succès]]
L'exposition s'est déroulée après celle de Paris dans le Crystal Palace, un lieu loin de Londres et surtout lié au divertissement.
Les exposants européens ne sont pas tous venus, c'était principalement une expo anglophone.

Voir Electricity at the Crystal Palace

### Zoummeroff note qu'Edison a fondé 3 entreprises et 1 usine fin 1881 - p250
[[Zoummeroff note qu'Edison a fondé 3 entreprises et 1 usine fin 1881]]
Il parle de l'Edison Continental Company (200 000$), Société Elec Edison, Société Industrielle et Commerciale.
Achat d'une usine à Vitry-sur-Seine. Une société habiltiée à prendre toutes les commandes en provenance de l'Europe Théâtre Brünn de Vienne, la raffinerie de sucre d'Anvers, le bureau central de Budapest, grande arcane commerciale à Milan.

### Zoummeroff évoque les premières maisons qui ont reçu la lumière d'Edison - p254
[[Zoummeroff évoque les premières maisons qui ont reçu la lumière d'Edison]]
Edison voulait éclairer tout Manhattan avant 83. La première maison fut celle de J.P. Morgan, puis la New York Herald. En mai, c'était les maisons des 3 Vanderbilt. Morgan eut des problèmes, murs et tapis brulés, bruit et fumée. Il dit "on ne fait pas de fumée sans feu."
