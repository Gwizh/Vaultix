Titre : [[La Nuit Désenchantée]]
Type : Source
Tags : #Source #Fixette 
Ascendants : [[Fixette]]
Date : 2024-01-10
***

## Propositions

### Projet de "Tour Soleil" de Jules Bourdais, alternative rejetée de la Tour Eiffel - p9
[[Projet de Tour Soleil de Jules Bourdais, alternative rejetée de la Tour Eiffel]]
360m de haut. 
But : éclairer tout Paris pas dans chaque rue mais depuis le haut. 
On pensa ça possible, la Tour Eiffel semblait en tout cas tout aussi farfelue.
Ce projet a été rejeté car on craignait d'être ébloui par autant de lumière centralisée
Sébillot voulait éclairer jusqu'à l'intérieur des maisons note115.
On avait peur de l'éblouissement.
On avait peur que ce ne soit qu'une seule tour qui pourrait devenir la cible des insurgés.
### Explication de la combustion par Lavoisier - p10
[[Explication de la combustion par Lavoisier]]
On pensait que la flamme se nourrissait d'une substance nommée phlogiston. Les écrits de Lavoisier nous ont permis de contrôler les flammes. On a pu grandement augmenter le rendement de toutes formes de combustion. Ça a eu des conséquences pour la machine à vapeur de Watt par exemple.

### Révolution de l'éclairage par la bougie selon Schivelbusch - p12
[[Révolution de l'éclairage par la bougie selon Schivelbusch]]
La bougie a été la première utilisation du feu uniquement dédiée à l'éclairage. Le feu est selon lui rendu docile, il ne détient quasiment plus le lieu de combustion. C'est encore plus le cas avec la lampe à huile dont la mèche ne s'épuise quasiment pas.

### La bourgeoisie utilisa la lumière comme outil de travail dès le XVIe siècle - p13
[[La bourgeoisie utilisa la lumière comme outil de travail dès le XVIe siècle]]
L'heure commença alors à être mesurée mécaniquement. La bourgeoisie souhaite alors rendre la journée de travail indépendante de la journée du soleil. Il fallait pouvoir travailler à heure fixe, été comme hiver. L'utilisation de l'éclairage était limitée et rationnelle. Elle était aussi individuelle.

### Les usines furent le lieu où l'éclairage a eu le plus d'importance - p14
[[Les usines furent le lieu où l'éclairage a eu le plus d'importance]]
Dans les usines, l'éclairage n'était plus individuel et pourtant la taille du lieu rivalisait avec les châteaux des nobles. Un éclairage centralisé et peu couteux est alors devenu nécessaire. Il fallait aussi grandement augmenter la puissance des lampes.

### Les lampes Argand, révolution de l'éclairage par la mise en pratique de la théorie de Lavoisier - p14
[[Les lampes Argand, révolution de l'éclairage par la mise en pratique de la théorie de Lavoisier]]
Les lampes d'Argand de 1783 ont été une révolution par l'utilisation d'une mèche plate en prenant en compte de notre compréhension nouvelle de l'importance de l'air dans la combustion. C'était une lumière très vive qui ne produisait pas de fumée.
Le cylindre de verre protégeait la flamme et un système permettait de changer l'intensité de la lumière !

### On considérait le gaz comme un déchet avant la révolution industrielle - p22
[[On considérait le gaz comme un déchet avant la révolution industrielle]]
Dès 1691, ont prit conscience en Europe du gaz et sa combustibilité. On produisait même du gaz avec la cokéfaction de la houille, pour la production de goudron -> Derby
On ne savait simplement pas quoi faire avec ce gaz qui émanait de cette production.

### L'utilisation charbon demandait de changer les industries - p22
[[L'utilisation du charbon demandait de changer les industries]]
La combustion du charbon provoquait une chaleur plus intense que les autres sources d'énergie. Il fallait donc des fours plus hauts et globalement des outils plus résistants. C'était donc tout l'équipement qu'il fallait revoir.

### La thermo-lampe de Lebon et la locomotive de Trevithick ont été dévoilés comme des attractions - p26
[[La thermo-lampe de Lebon et la locomotive de Trevithick ont été dévoilés comme des attractions]]
"Faiseur de projets" à la Argand, Lebon inventa en 1801 la thermolampe, lampe à gaz alimentée par des tuyaux, disposés au centre des maisons et servant d'éclairage ou de chauffage. Lire p26 comment il présente son produit.
Il chercha la commercialisation et fit payer l'entrée à sa propre maison p27 pour montrer les qualités de son invention.

### Winsor a permis par sa publicité à populariser l'éclairage au gaz - p 29
[[Winsor a permis par sa publicité à populariser l'éclairage au gaz]]
Connaissances assez limitées en chimie et mécanique mais doué en conférences !
Il a rendu l'éclairage au gaz séduisant malgré ses descriptions extravagantes et trompeuses. Il connu une quasi aussi grande renommée que Lebon.
Popularisa le concept de tuyaux pour le gaz en faisant 1 rapprochement avec l'eau (acheminée dans les maisons dès 1726 au Royaume-Uni)

### Gaz et Rail perçus par certains au XIXe siècle comme une perte de liberté individuelle et rattachement à des monopoles - 32
[[Gaz et Rail perçus par certains au XIXe siècle comme une perte de liberté individuelle et rattachement à des monopoles]]
Deux techs très liées par leur proximité technologique mais aussi par le fait nouveau qu'il fallait se connecter à un réseau. Réseau qui était mis en place de très loin. Généralement l'État contrôlait (ou essayait en tout cas) ces monopoles
En France, le département de la Seine a imposé d'avoir une seule compagnie par rue ! 1839, Paris découpée en arrondissements donnés aux métropoles.

### Dynamique industrie/public/État vis à vis du danger du gaz au XIXe siècle selon Schivelbusch - p35
[[État vis à vis du danger du gaz au XIXe siècle selon Schivelbusch]]
Les explosions des réservoirs arrivaient régulièrement. 1862 à Paris et 1865 à Londres. Commission Congreve en 1823. Eloigner les usines à gaz et réduire la taille des récipients. Les industries du gaz se sont révoltées en évoquant la pression des contrôles, le viol de leur propriété privée et les conséquences économiques néfastes.

### Accum et la propagande paternaliste et sexiste pour le gaz - p40
[[Accum et la propagande paternaliste et sexiste pour le gaz]]
Comme il y avait littéralement des robinets de gaz, ils pouvaient rester allumés et répandre le gaz et potentiellement causer des explosions. Accum, propagandiste du gaz en profitait pour vanter les mérites d'un père assurant la fermeture du gaz pour la famille.

### Intoxication des vivants, des sols et de l'air par le gaz au XIXe siècle - p41
[[Intoxication des vivants, des sols et de l'air par le gaz au XIXe siècle]]
Intoxication si gaz laissé la nuit. Etudier l'emplacement des usines pour ne pas polluer atteignent des habitants de même pour les rivières (aval). Mais pollution du sol par les conduites non étanches. Noircissement du sol et odeur, vie sous terraine impossible. Souffre et ammoniaque.
### Les problèmes de ventilation liés à l'éclairage au gaz durant le XIXe - p46
[[Les problèmes de ventilation liés à l'éclairage au gaz durant le XIXe]]
Les flammes des lampes à gaz était très grande et consommait bcp d'air. Elle faisait aussi bcp chauffer les pièces. Des solutions avaient été trouvées de plafond lumineux. (enfermer les lumières dans un autre espace) mais c'était très oppressant p47

### La lumière à incandescence au gaz, come-back du gaz en 1886 - p48
[[La lumière à incandescence au gaz, come-back du gaz en 1886]]
Auer von Welsbach inventa en 1886 une lampe qui se servait d'un bec Bunsen pour pousser à incandescence un alliage. La lumière était très intense et ça consommait bcp moins de gaz que la flamme. Concurrent sérieux à l'électrique.
Pourtant la tech existait depuis 1830 mais il fallait le succès de l'élec pour que ça fonctionne.
Effet navire à voile. 
En 1900, des interruptions pour le gaz ont été inventé pour concurrencer l'élec.

### La chaleur était le principal problème du gaz - p51
[[La chaleur était le principal problème du gaz]]
La fréquentation des théâtres causait des maux de tête et des nausés -> +15 à 38°C à cause de l'éclairage !
Aller chercher 1 livre en haut de la bibliothèque -> température de four
Combustion -> acide carbonique, eau, souffre et ammoniaque -> toxique

### La lampe à arc est une lampe à incandescence - p53
[[La lampe à arc est une lampe à incandescence]]
L'arc produit de la lumière mais le gros de l'éclairage venait du chauffage des électrodes de charbon, il y a combustion. On a ensuite mis le vide dans ces lampes pour ne pas avoir cette destruction. Inventée en 1830 ?
-> Vraiment trop clair pour le coup.

### L'originalité d'Edison était qu'il a imité le gaz en reprenant ce qui existait déjà selon Schivelbusch - p56
[[L'originalité d'Edison était qu'il a imité le gaz en reprenant ce qui existait déjà selon Schivelbusch]]
Davy avait découvert l'incandescence. Le fait de mettre dans le vide De Moleyne 1841
Göbel le filament carbonique en 1850
Edison a perfectionné et mis en pratique en un produit prêt à l'application 
-> Comparaison avec Winsor
Il voulait faire tout pareil sauf utiliser l'air ! Aussi - d'intensité.
Citations de la page 58 : au lieu du blanc, dur, il y a alors une lumière "civilisée", accommodée à nos habitudes, même luminosité que le gaz mais juste, constant et sans pollution et sans danger.
Même coloration car filament carbonique
On peut alors voir l'ancien dans le nouveau.

### Edison a expérimenté avec de très nombreux matériaux pour ses filaments - p60
[[Edison a expérimenté avec de très nombreux matériaux pour ses filaments]]
Plus de 6000 matières végétales, papier, coton, liège, celluloïd...
Il découvrit le bambou et chercher la bonne espèce.
Il chercha en Chine, Japon et Amérique du sud.
Il construisit une ferme au Japon juste pour ça. Puis dans les pays qui pouvaient en accueillir. 
Anecdote bambou pourri note100
Plus tard on utilisa des matériaux artificiels et on trouvera des matériaux pour tout type de luminosité.

### Conservation des caractéristiques du gaz pour l'électricité - p64
[[Conservation des caractéristiques du gaz pour l'électricité]]
L'électricité était aussi produite loin de la ville comme l'électricité et on a donc repris le principe d'apprivoisement central. -> Modèle de Winsor
On a aussi gardé le robinet alors que ce n'était pas assez instantanné pour l'électricité.
Exemple de regression technologique

### Contrairement au gaz, on confiait à l'électricité plein de caractéristiques vitales : tout électrique - p66
[[Contrairement au gaz, on confiait à l'électricité plein de caractéristiques vitales - tout électrique]]
On fit entrer l'électricité directement dans les grandes demeures bourgeoises ou aristocratiques car elle faisait moins peur que le gaz. On disait de l'électricité que c'était de l'Energie.
Un moyen de lutter contre la fatigue.
Engrais pour l'agriculture.
Ellectrothérapie.
Citation note117

### L'électricité était perçue comme collectiviste et ou monopoliste - p68
[[L'électricité était perçue comme collectiviste et ou monopoliste]]
L'électricité devait être produite par des grandes institutions et acheminée. Elle s'accommodait doncbien du tournant du capitalisme de l'époque. Il n'y avait plus d'autarcie et de moins en moins de petites entreprises. L'entreprise était vue comme le berceau de l'homme moderne. 
Exemple très pertinent de Charles Steimetz
Formule de Lénine
Conversation fictive.

### Les couvres-feux étaient courants dans les villes occidentales jusqu'au XIXe siècle - p71
En 1380, à Paris, les maisons étaient fermées à clé et les clés étaient confiées au magistrat. Personne ne devait sortir. Il faut lire les 12 heures noires. Les veilleurs étaient autorisés à avoir la lumière. Toute personne sans lumière était suspecte par défaut.
Après, les lumières étaient obligatoires au 1e étage des maisons.

### L'éclairage policier de la France absolutiste - p75
Les lanternes étaient suspendues par des cordes au milieu de la rue. Au même moment, le Roi avait fait apver les rues et réduit les enseignes. Les message était clair ! Éclairage géré par la police ! 
On comprend le rapprochement avec les caméras de surveillance. 5000 lanternes à Paris en 1700.
"Après minuit, chaque lanterne vaut un veilleur de nuit" ! 1849

### Réverbères commandés par le Roi en 1763 - p81
L'instigateur était plus précisément le chef de la police mais sous ordre du Roi. 2000 francs au gagnant. Lavoisier proposa une lanterne qui se servait de verres polis pour récupérer la lumière perdue auparavant. Il utilisait aussi des lampes à huile.
Citation note 29

### Psychologie de la perception de Bachelard - p83
"Tout ce qui brille voit" 
La lumière d'une lampe est surveillance, contre-surveillance et surveillance mutuelle. 
C'est l'instrument pour surveiller autrui et ce qui rend possible la surveillance de ce qui le détient. C'est une arme.
Détruire les lanternes, c'est la liberté, une rébellion contre l'ordre.

### Exécutions pendant la Révolution française des représentants de l'Ancien Régime sur les lanternes - p86
La destruction des lanternes pendant la Révolution française prit un tournant symbolique. On a pendu des représentants aux lanternes de l'Hotel de Ville. Les lanternes étaient symboles de l'ordre. On changea le mot lanterner pour parler de ces exécutions. Le portrait de Louis XIV fut posé au-dessus des lanternes.
Desmoulins : Discours de la lanterne aux Parisiens

### La destruction des lanternes comme symbole pendant la révolte en 1830 - p88
À la fois symbole de l'ordre (couplé à la destruction d'insignes royaux) mais aussi intérêt stratégique. Le noir était une barrierre aux veilleurs, +vulnérables; Les patrouilles n'allaient que dans les endroits éclairés.

### Utilisation du symbole des pavés et des lanternes par les poètes et romanciers des années 1830-40 - p90
Les pavés sont utilisés comme symbole de l'ordre, du peuple et du peuple mis en ordre. Hugo note60.
Un mot très utilisé à l'époque.
Couplé parfois avec les lanternes qui symbolisent ce même ordre ancien, utilisé pour illuminer, surveiller les pavés. Dans les misérables, Hugo se revendique de cette obscurité comme symbole de la lutte, comme moyen de la lutte.
### Les lanternes ont moins été cibles d'attaques en 1848 qu'en 1830 - p93
Alors qu'il s'agissait d'une stratégie en 1830, on ne retrouve pas une telle tactique en 1848. Or il se trouve qu'entre temps le passage de l'huile au gaz pour l'éclairage s'est quasiment fait. Ainsi, pour avoir un réel impact sur l'éclairage, il fallait attaquer les usines far away. Mais celles-ci étaient protégées par des gardes.
### Tours de lumières aux US au XIXè siècle - p96
Les lampes à arc illuminaient trop et l'installation de milliers de réverbères coûtait trop cher. Alors +s villes des US ont construit de grandes tours pour illuminer des villes ou des quatiers entiers.
Citation p103 note102
Philosophiquement très différent : égalitaire, utilitariste
Pb : pas si efficace -> grosses zones d'ombres et moche de ouf note107
### Argument moral de l'éclairage : bon au XIXe puis désillusion au XXe 
Jules Michelet fut l'un des premiers à le noter 
La lumière devient de + en + synonyme de surveillance
note 119
### Se coucher tard est un marqueur social fort de la société bourgeoise du XIXe - p114
[[Se coucher tard est un marqueur social fort de la société bourgeoise du XIXe]]
Lorsque le travail s'est détaché du cycle du jour, les bourgeois aimaient rester plus tard pour faire la fête ou s'amuser. Le fait de se lever tard était très bien vu. Toujours aujourd'hui on parle des gens qui se lavent tôt comme les laborieux.
p114 -> les heures se décalent !
### La psychologie de la perception analyse les boulevards du XIXe comme une extension des salles des fêtes - p120
[[La psychologie de la perception analyse les boulevards du XIXe comme une extension des salles des fêtes]]
Les vitrines des magasins, décorées, illuminées, sont perçues comme des murs. Là où les lanternes sont placées devient un plafond. Le fort contraste entre l'ombre et la lumière devient aussi fort que l'intérieur et l'extérieur.
### Le gaz a été une transition cruciale entre la flamme et l'électricité - p126
La lampe à pétrole a continué à être utilisée, peut-être, pour garder l'autarcie sur l'éclairage, réagir face à l'industrie et la dépossession par le gaz. De même, le gaz a été le premier appareil "industriel", "mécanique", moche pour beaucoup. Utilisation d'abat-jour car lumière trop importante et pour cacher la laideur -> séparation de la flamme et de la lumière. La flamme qui selon Bachelard et des courants d'historiens était le centre d'un foyer, disparaissait. 
p135 : passionnant retour à la bougie

### Retour à une lumière tamisée à l'intérieur des foyers bourgeois de la fin du XIXe - p137
[[Retour à une lumière tamisée à l'intérieur des foyers bourgeois de la fin du XIXe]]
La lumière devient si intense qu'on ne devait plus le regarder pour des raisons de santé.
Création d'abat-jours pour diffuser mais aussi colorer la lumière. 
Les lampes Tiffany sont l'exemple de cet obscurcissement progressif. Il en va de même pour les fenêtres qui se sont perfectionnées et agrandies. Il fallait maintenant diluer cette lumière derrière un rideau qui devenait de + en + opaque. La coloration du verre comme au Moyen-âge revient à cet effet.
p142 : Lumière et bourgeoisie.

### Schivelbusch fait le parallèle entre l'éclairage industriel et l'aversion bourgeoise contre l'industrie extérieure et la sphère publique - p143
[[Schivelbusch fait le parallèle entre l'éclairage industriel et l'aversion bourgeoise contre l'industrie extérieure et la sphère publique]]
Au XIXe seulement l'intérieur a commencé à être éclairé par l'extérieur du fait des lampes à arc particulièrement puissantes des grandes fenêtres. La protection face à cette intrusion, qui pouvait être perçue comme des regards, commença. De plus, l'éclairage au gaz ou électrique venait d'usines lointaines, le fichage de la consomamtion commença. On comprend alors la volonté d'autarcie et les stratégie de voilage.
### Évolution de la scène au XIXe du fait de l'éclairage - p145
[[Évolution de la scène au XIXe du fait de l'éclairage]]
Il faut d'abord comprenre qu'on n'arrivait pas à éclairer correctement les scènes avant le XIXe. On note la frustration des pros qui sont déçus des technos disponibles. Mais lorsque les technos arrivent, les changements sont marquants !
Les couleurs changent et il s'agit alors de choisir si on s'y fait ou si on adapte pour aue ça rende comme avant. 
Or l'esthétique bourgeoise diffère de l'aristocratie, la sobriété s'oppose au faste. 
Et Diderot note26. De même les décors, devenus trop éclairés perdent en reliefs et en illusion. Un plus gros travail était alors nécessaire note28.

### Jusqu'au XIXe, le public des théâtres et opéra était éclairé - p157
[[Jusqu'au XIXe, le public des théâtres et opéra était éclairé]]
On savait depuis la Renaissance qu'on voyait d'autant plus la scène que le public était dans le noir.
Pourtant, ce n'est qu'au XIXe siècle qu'on a commencé à éteindre le public.
Cela venait du public et non des metteurs en scène. Ces derniers voulaient au contraire illuminé au mieux mais étaient contraints par les volontés du public.
Pour ceux-ci, le théâtre était un lieu de rencontre, de regroupement, le parfait éclairage de la scène n'était pas crucial.
À la fin du XIXe, il n'était pas rare d'entendre les vieux se plaindre de l'obscurité dans la salle.
Pourtant, la culture a changé et il est devenu normal d'avoir le public dans le noir à la fin du XIXe 

[[@La nuit désenchantée_ à propos de l'histoire de l'éclairage artificiel au 19e siècle,Wolfgang Schivelbusch, Anne Weber]]