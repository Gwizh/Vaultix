Titre : [[Edison contre Westinghouse - La première bataille moderne pour un standard industriel]]
Type : Source
Tags : #Source #Fixette 
Ascendants : [[Fixette]]
Date : 2024-01-07
***

## Propositions
### Corbel considère que rien ne garantit que la technologie la plus performante s'impose
[[Corbel considère que rien ne garantit que la technologie la plus performante s'impose]]
Les économistes évolutionnistes parlent d'externalités du réseau avec l'exemple du clavier Qwerty. [David 1985] et l'accroissement de l'utilité d'une techno en fct du nb d'utilisateur.

Les sociologues regardent aussi la nécessité d'avoir le soutien des acteurs clés du système [Akrich 1988]. En effet, outre les nouveaux utilisateurs et promoteurs, les acteurs de l'ancienne technologie sont tout autant importants. Le passage avec l'ancienne technologie peut mener à des négociations pour garder les mêmes normes, les mêmes utilisations (comme le DVD qui est compatible avec les CD ROM).

### Edison a eu du retard sur ce qu'il avait promis aux investisseurs à la fondation de son entreprise
[[Edison a eu du retard sur ce qu'il avait promis aux investisseurs à la fondation de son entreprise]]
Il a directement voulu apporter l'éclairage dans les foyers et a fondé l'Edison Electric Light Company en 1878 avec le soutien de riches investisseurs de New York. Il promit qu'il serait rapidement capable d'apporter l'éclairage électrique dans les rues de New York. Il fit tout de suite des démonstrations. [M. Akrich, M. Callon et B. Latour 1988] pour patienter les investisseurs et journalistes.

Et, en septembre 1882, avec beaucoup de retard sur le planning annoncé, tout un quartier d’affaires de New York était illuminé par du courant électrique provenant de la première centrale de production centralisée installée par Edison.

### Premières démonstrations d'Edison
 Le 31 décembre 1879, il éclaira Menlo Park avec des ampoules. En 1882, il était l’une des attractions d’une exposition à Londres [NYE, 1990, p. 37].

### Les premiers utilisateurs de la lampe d'Edison expliquent en partie son succès
[[Les premiers utilisateurs de la lampe d'Edison expliquent en partie son succès]]
En 1882, quelques entreprises et particuliers, comme J.P. Morgan, avaient également été équipés de leur propre installation, dotée d’un générateur individuel. Aspect non négligeable en terme de communication, le New York Times et le New York Herald figuraient parmi ces premiers utilisateurs de la lumière électrique [JONNES, 2003, p. 85]. 

### De 1885 à 1887, l'alternatif rattrape déjà son retard sur le continu avec Westinghouse
