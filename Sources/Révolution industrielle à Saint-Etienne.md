Titre : [[Révolution industrielle à Saint-Etienne]]
Type : Source
Tags : #Source, #Fixette
Ascendants :
Date : 2024-02-21
***

## Propositions
### Fourneyron, la turbine stéphanoise - p92
Turbine hydraulique à la fin des années 1820s. Parfaite à une époque où l'eau était abondante et le charbon cher. Peu diffusée pour autant.
129 usines équipées en 1843, 200 en 1867.
Ne correspondait pas au rythme industriel qui devait être constant.
### Rendement industriel des rivières stéphanoises du début du XIXe - p141
Furan qui traverse Saint-Etienne, irrégulier avec 2 périodes d'étage en hiver et été (moins de débit)
Force motrice établie en 1823 -> 38500 hommes soit 154 machines à vapeur de 25 chevaux/s
1833, 237 roues pour 100000 francs de produit annuel.
### Contrats de travail du début du XIXe à S-E - p128
Ouvriers dans les usines oui, mais lien avec la campagne avec des contrats de 10 mois pour revenir à la maison !
### Ouvriers stéphanois vus comme + sauvages et indisciplinés que ceux de Lyon - p110
Adolphe Blanqui préférait les ouvriers de Lyon à ceux de SE. Ces derniers avaient déjà une conscience de classe. En 1848, ils ont formés des gouvernements provisoires dans chaque puits et chassaient les contremaîtres et ingénieurs.
1830 : Société mutuelle de secours
1848 : Société des ouvriers mineurs et charbonniers de la Loire
### Rave Stéphanoise, instrument adapté aux paysans-mineurs pauvres - p58
Il est dit que le mineur payait lui-même sa lampe ! D'où l'achat d'une lampe qu'il peut utiliser pour d'autres tâches. Il y a une photo de Jeanon à Terrenoire où tous les mineurs tenaient 1 rave !
Ça semble quand même d'être d'avant 1860.