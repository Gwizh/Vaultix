Titre : [[La flamme d'une chandelle]]
Type : Source
Tags : #Source #Fixette 
Ascendants : [[Fixette]]
Date : 2024-01-20
***

## Propositions
### Dans le traité du feu et du sel, Vigenère décrit les 2 parties d'un feu - p27
[[Dans le traité du feu et du sel, Vigenère décrit les 2 parties d'un feu]]
Il voit deux flammes, l'une blanche qui éclaire et l'autre rouge qui brule. Bachelard dit que la lumière est alors sur-valorisation du feu. L'éclairage (blanc) est une conquête de la flamme vulgaire, dont la couleur et l'intensité changent. La flamme blanche est non-corrompue.

### Rêver ou étudier sous le feu d'une lanterne selon Bachelard - p53
[[Rêver ou étudier sous le feu d'une lanterne selon Bachelard]]
Étudier sous le feu d'une lanterne c'est être constamment entre l'étude de l'objet éclairé (un livre par exemple) et la rêverie provoquée par le feu. Le temps qu'on passe à l'un n'est pas passé à l'autre. Dynamique intéressante entre les deux actions.

### Conférence populaire de Faraday sur la chandelle allumée - p68
[[Conférence populaire de Faraday sur la chandelle allumée]]
Faraday donnait des cours du soir pendant lesquels il faisait des expériences. Voir Histoire d'une chandelle de Faraday.

### Les fleurs et les fruits ont la même capacité à faire rêver qu'une lampe selon Bachelard - p77
[[Les fleurs et les fruits ont la même capacité à faire rêver qu'une lampe selon Bachelard]]
Les fleurs et les fruits naissent du soleil. Leurs vives couleurs proviennent de cette lumière, de cette chaleur. Mettre une tulipe rouge dans un vase au centre de la table, c'est créer un centre d'attention semblable au feu. On peut tout autant se perdre dans ses pensées.

### Bachelard pense qu'on ne rêve pas en allumant la pièce immédiatement et uniformément avec l'électricité - p90
[[Bachelard pense qu'on ne rêve pas en allumant la pièce immédiatement et uniformément avec l'électricité]]
Bachelard a connu la transition de la lampe à huile à l'ampoule électrique. Cette lumière est administrée, nous sommes un sujet mécanique inclut dans un procédé mécanique. Nous ne sommes plus le seul sujet du verbe allumer. Entre l'espace sombre et l'espace tout-de-suite-clair, il n'y a plus qu'un instant, il n'y a plus qu'un geste, il n'y a plus de conscience, il n'y a plus de conscience, il n'y a plus l'"amitié attentive" qu'on donnait à cette action, à cette lumière.

### Bachelard utilise les mots de Bosco pour redémontrer l'importance de l'attention qu'on porte aux objets - p91
[[Bachelard utilise les mots de Bosco pour redémontrer l'importance de l'attention qu'on porte aux objets]]
Le poêle est pour Bachelard qq'1 qui se pose sur les choses, qui étudie et décrit nos affections, notre séduction. Il critique l'ustensilité, trop froid.
Bosco pense la lampe comme une créature et comme un être créant. La lampe pense à nous.
"Donnez des qualités aux choses, donnez du fond du cœur, leur juste puissance aux êtres agissants et l'univers resplendit"

### Bachelard reprend Novalis pour qualifier le rapport entre l'huile et la lumière - p95
[[Bachelard reprend Novalis pour qualifier le rapport entre l'huile et la lumière]]
L'huile est la matière même de la lumière. L'homme libère avec la flamme la lumière emprisonnée dans la matière. Le pétrole est aussi de l'huile pétrifiée.

### Le roman Hyacinthe de Bosco met selon Bachelard la lumière au centre du récit - p100
[[Le roman Hyacinthe de Bosco met selon Bachelard la lumière au centre du récit]]
Bosco évoque une lampe dès la première page. Il parle d'une lampe lointaine, une lampe d'un autre. C'est génial pour Bachelard qui voit toutes les implications d'une lampe d'autrui. Est-ce pour le soir ou le matin ? La lampe dérange le narrateur, elle veille sur cet autre mais surveille le narrateur.