Titre : [[Écoute, camarade !]]
Type : Source
Tags : #Source
Ascendants : [[Murray Bookchin]]
Date : 2024-01-13
***

## Propositions
### Bookchin voit comme différence majeure avec le XIXe de Marx l'abondance matérielle
[[Bookchin voit comme différence majeure avec le XIXe de Marx l'abondance matérielle]]
Aujourd'hui notre vie est banale et caractérisée par l'abondance matérielle, il n'y a plus de précarité des objets. Bien sûr, il s'agit d'avoir les moyens de consommer mais le XIXe n'offrait pas à tous assez de bien. La faim existait avant tout par manque, pas par volonté politique.
### Murray Bookchin souhaite transcender le marxisme et donc suivre la dialectique de Marx
[[Murray Bookchin souhaite transcender le marxisme et donc suivre la dialectique de Marx]]
Il considère que le marxisme ne décrit plus assez le monde d'aujourd'hui mais c'est plutôt le fait de suivre les analyses passées qui lui pose problème. Il veut utiliser la dialectique comme Marx l'a fait avec Hegel.

### Bookchin questionne la dialectique marxiste de la transition entre les sociétés
[[Bookchin questionne la dialectique marxiste de la transition entre les sociétés]]
Bookchin note que Marx étudie la transition d'une société de classes vers une société d'autres classes (féodalisme -> bourgeoisie) par l'opposition entre agriculture et artisanat. Bookchin s'interroge alors sur l'utilité de ce raisonnement pour étudier la transition d'une société de classes à une société sans classes. Comment on transitionne de la dictature du prolétariat à la suite en somme.
### Rechercher la lutte des classes du XIXe c'est s'assurer que la révolution n'arrive pas selon Bookchin
[[Rechercher la lutte des classes du XIXe c'est s'assurer que la révolution n'arrive pas selon Bookchin]]
Le prolétariat d'aujourd'hui n'est plus le même qu'au XIX. Nous avons l'abondance matérielle (+ ou -), nous avons en Europe de nombreux avantages sociaux dignes du communisme. Il faut constituer une image plus parlante aux masses d'aujourd'hui. Une sensation de dissonance avec ce que nous vivons et une image (le problème) de ce qui pourrait advenir.

### Le capitalisme libéral a pu offrir ce que la dictature du prolétariat devait organiser selon Bookchin
[[Le capitalisme libéral a pu offrir ce que la dictature du prolétariat devait organiser selon Bookchin]]
Le but de la période de transition était de supprimer la contre-révolution et aussi de permettre l'abondance des conditions matérielles. Or le capitalisme a su organiser cette dernière grace aux technos et à la logistique. Il a aussi nationalisé, laissé fondre l'État dans certains secteurs, il a su proposer des avantages sociaux (tout ça sous des formes qu'on n'accepte pas mais ça pose quand même problème).

### Les groupes d'avant garde de la gauche française n'a pas tout de suite suivi le mouvement de mai 68 selon Bookchin
[[Les groupes d'avant garde de la gauche française n'a pas tout de suite suivi le mouvement de mai 68 selon Bookchin]]
Il y a eu un certain dédain de la plupart des groupes de type bolchevique français vis à vis des révoltes de mai 68. Le PCF tenta de manipuler les Assemblées de la Sorbonne en targant qu'il fallait une direction centralisée.

### Bookchin pense le parti comme une institution fondamentalement bourgeoise
[[Bookchin pense le parti comme une institution fondamentalement bourgeoise]]
Le but d'un parti est de prendre et de garder le pouvoir et non le dissoudre. Le parti n'a donc psa lieu d'être au sein du communisme. Le parti respecte des considérations hiérarchiques, il y a dogmes, discipline et manipulation. C'est encore plus le cas lorsqu'il se prend au jeu électoral bourgeois. Il devient magouilleur !

### Une institution bourgeoise ou reprenant les codes bourgeois est facile à neutraliser pour la bourgeoisie selon Bookchin
[[Une institution bourgeoise ou reprenant les codes bourgeois est facile à neutraliser pour la bourgeoisie selon Bookchin]]
Si la bourgeoisie capture la direction d'une institution centralisée type bourgeoise, elle peut la neutraliser. Si le parti est habitué à l'obéissance, les membres seront d'autant moins à même de réagir !

### Création de l'"unité léniniste" en 1921. Ban des factions selon Bookchin
[[Création de l'unité léniniste en 1921. Ban des factions selon Bookchin]]
Bookchin analyse les débats des bolcheviques de 1904 à 1921. Il observe un mouvement rempli de factions, de divisions, etc... Lénine dut combattre une direction bolchevique très conservatrice. En 1921,lors du Xe congrès, il convainc la direction de bannir les factions "anarchistes petit bourgeois".

[[@Écoute, camarade !,]]