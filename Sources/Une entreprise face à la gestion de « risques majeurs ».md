Titre : [[Une entreprise face à la gestion de « risques majeurs »]]
Type : Source
Tags : #Source
Ascendants :
Date : 2024-03-27
***

## Propositions
### Brève liste des travaux sur les mines de 2006 - p2
[[Brève liste des travaux sur les mines de 2006]]
De nombreuses recherches ont été consacrées à l'histoire de bassins miniers : sans
être exhaustif, nous citerons les travaux de Marcel Gillet, d'Odette Hardy-Hémery et de
Louis Fontvieille sur le bassin du Nord-Pas-de-Calais, ceux de Rolande Trempé sur le
bassin houiller de Carmaux et plus récemment de Philippe Mioche, Xavier Daunalin et
Olivier Raveux sur le bassin des Bouches-du-Rhône. Les monographies d'entreprises
houillères sont moins nombreuses : la compagnie des mines de la Loire par Pierre
Guillaume, la Société des Mines de Lens par Olivier Kourchid, la Compagnie des mines
de la Grand-Combe par Jean-Michel Gaillard.

### Les 3 événements de Courrières entre 1900 et 1920 - p2
[[Les 3 événements de Courrières entre 1900 et 1920]]
Il y a eu le coup de grisou en 1906 et ses 1099 morts, la destruction en 1914 de la mine par l'Allemagne et après la reconstruction la réaction à la contraction du marché charbonnier.

### Courrières était en 1905 la 3ème mine houillère de France - p3
[[Courrières était en 1905 la 3ème mine houillère de France]]
Avec 7% de la prod natio, derrière Anzin et Lens et leur 8%. Excellente santé financière. C'est la compagnie houillère du Nord qui a distribué les plus gros dividendes, eu égard à l'importance de son extraction1.

### La rentabilité de Courrières provenait souvent d'une compression des coûts de production - p5
[[La rentabilité de Courrières provenait souvent d'une compression des coûts de production]]
Les actionnaires n'augmentaient pas le capital de l'entreprise car ils ne voulaient pas perdre en dividendes. Dans un sens, l'entreprise était trop rentable. Il fallait alors prendre des emprunts et réduire les couts. Construction de fosses simples (4/11 en 1905) au lieu des fosses doubles plus sécurisées. Toujours des lampes à flamme nue dans les parties nous grisouteux. Les bois usagés sont abandonnés. L'arrosage du front de taille et des galeries n'est pas pratiqué. Travail de 12h attesté et permis par des dérogations.

### Magouilles sur les salaires et montants d'indemnités après la catastrophe de Courrières - p8
[[Magouilles sur les salaires et montants d'indemnités après la catastrophe de Courrières]]
Salaires plus bas liés à la perte de production. Gestion par l'entreprise de la caisse de solidarité, etc...
Mais le directeur aussi obtient une grosse augmentation temporaire

### La catastrophe de Courrières de 1906 n'en est pas spécialement une du point de vue financier - p9
[[La catastrophe de Courrières de 1906 n'en est pas spécialement une du point de vue financier]]
Malgré les lourds investissements que l'entreprise a du faire pour construire de nouveaux tunnels, payer les indemnités et reprendre la production normale, l'entreprise ne fut pas tant impactée d'un point de vue financier.

Les dividendes par exemple n'ont que légèrement baissées. En à peine 4 ans la capitalisation était revenue au niveau d'avant la catastrophe. Parmi les investissements faits, il y a eu l'installation de certaines infrastructures plus sécurisées que le voulait la norme.

### L'électrification dans les mines concernent entre autres les foreuses, les chaudières, l'éclairage etc... - p12
[[L'électrification dans les mines concernent entre autres les foreuses, les chaudières, l'éclairage etc...]]
À Courrières, après la 1GM, une politique du matériel est entrepris. L'électricité remplace alors la vapeur et offre puissance, sécurité et maniabilité. Pas simplement les lampes, plein de différentes machines.

### Le passage à la lampe électrique ne se fait en partie pas en 1919 à Courrières car la lampisterie n'a pas été détruite - p14
[[Le passage à la lampe électrique ne se fait en partie pas en 1919 à Courrières car la lampisterie n'a pas été détruite]]
"Ils minimisent les dépenses d'investissement liées au travail : la lampisterie ayant été épargnée par les destructions, les lampes de sûreté à flamme et à benzine sont remises en usage alors que les compagnies dont la lampisterie a été détruite, investissent dans l'achat de lampes électriques portatives."

### Courrières a toujours souhaité baisser les salaires - p14
[[Courrières a toujours souhaité baisser les salaires]]
Malgré les nombreuses grèves, les salaires ont été baissés dès qu'ils pouvaient. Il y a un recourt à l'immigration pour combler l'insuffisance de main-d'œuvre.

### 2 milliards de francs versés aux actionnaires de Courrières suite à la nationalisation - p19
[[2 milliards de francs versés aux actionnaires de Courrières suite à la nationalisation]]
"La Seconde Guerre mondiale se traduit pour les mineurs de Courrières et du Nord-Pas-de-Calais par une régression considérable : allongement de la durée de travail sans augmentation de salaire, dégradation des conditions de travail et de vie, arrestations suite à la grève du 28 mai au 7 juin 1941 83... Les mineurs devront attendre la fin de la « bataille du charbon » pour connaître des lendemains meilleurs. Entre temps, l'histoire de la compagnie des mines de Courrières, en tant que société privée, s'est achevée par l'ordonnance du 13 décembre 1944 réquisitionnant les houillères du Nord et du Pas-de-Calais. La loi du 17 mai 1946, portant nationalisation de la quasi-totalité des mines de combustibles minéraux solides, intègre la compagnie au sein du groupe Hénin-Liétard
du bassin du N.P.C. À cette occasion, le montant des obligations de Charbonnages de France versées aux actionnaires de la société en dédommagement de la nationalisation fournit une dernière information sur sa santé financière : avec près de 2 milliards de francs, il s'agit du 4e montant versé aux actionnaires des anciennes sociétés privées du bassin du Nord-Pas-de-Calais 84."