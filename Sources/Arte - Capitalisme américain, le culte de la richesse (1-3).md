Titre : [[Arte - Capitalisme américain, le culte de la richesse (1-3)]]
Type : Source
Tags : #Source
Ascendants :
Date : 2023-12-27
***

## Propositions
### Début de la richesse de Rockefeller
[[Début de la richesse de Rockefeller]]
Il a commencé par acheté pour qq milliers de dollars la Standard Oil en Ohio.
Il va s'arranger avec l'entreprises de chemins de fer pour avoir des prix plus bas que la trentaine de concurrent qu'il veut faire couler. En 2 ans, il va racheter ou couler toutes les raffineries de Cleveland puis va finir par contrôler 90% du pétrole américain. 

Un fois le monopole atteint, il a augmenté les prix.

Dans les années 1890, c'était l'homme le plus riche du monde mais aussi le plus détesté des Etats Unis.

### The Gospel Of Wealth, la philosophie de Canergie
[[The Gospel Of Wealth, la philosophie de Carnegie]]
Canergie est à la tête d'un empire métallurgique, tiré par l'industrie ferroviaire.
Il a écrit The Gospel of Wealth, qui sont ses réflexions personnelles sur son succès.

"Les inégalités sont naturelles et bénéfiques car seuls les plus capables s'enrichissent. Les millionnaires ont le devoir d'utiliser leur fortune utilement. Il ne doit rien léguer à sa dépendance. Le millionnaire doit consacrer toute sa fortune à la philanthropie."

### J. P. Morgan, clé de voute de la deuxième révolution industrielle américaine
[[J. P. Morgan, clé de voute de la deuxième révolution industrielle américaine]]
Il contrôle tout ce que Canergie et Rockefeller ne contrôlent pas. La marine, le train, l'électricité, le charbon, le télégraphe, etc...

Il a la confiance des milieux financiers européens. Il a récupéré les investissements que les européens ont fait aux US.
Morgan n' Co est la clé  de voute de la révolution industrielle américaine et de Wall Street.
Elle est à la fois prêteuse, actionnaire et Banque centrale des US.

### Politique américaine pendant la deuxième révolution industrielle
[[Politique américaine pendant la deuxième révolution industrielle]]
Les Républicains sont le parti de Lincoln, des industriels et du monde des affaires.
Les démocrates englobent tout le reste et notamment le Sud ségrégationniste et NY.

Le Sénat est alors un groupe de millionnaires. Ce sont eux qui contrôlent toutes les lois. Il n'y a pas de droit du travail, pas de contrôle des banques, du sexisme, du racisme et du travail des enfants.

### La grève de Homestead est un symbole de la colère sociale américaine au moment du krash de 1893
[[La grève de Homestead est un symbole de la colère sociale américaine au moment du krash de 1893]]
Dans une période de colère sociale liée à une baisse de performance de l'économie américaine, les grèves se multiplient dans le pays.

La deuxième plus grande bataille du syndicalisme américain est alors la grève de Homestead. 
Le conflit opposait la Amalgamated Association of Iron and Steel Workers à la Carnegie Steel Company. Il a culminé avec une bataille entre les grévistes et des agents de sécurité privés le 6 juillet 1892, qui a mené à la défaite des grévistes et à une diminution de la volonté de syndiquer les travailleurs de l'acier. 15 morts sont comptés.

Publiquement, Carnegie est en faveur des syndicats. Il condamne l'utilisation de briseurs de grève et affirme à des associés qu'aucune aciérie ne mérite de verser une seule goutte de sang. Il défend qu'il n'a fait que protéger sa propriété privée.

### Le krash de 1893 aux États-Unis va créer une grande colère sociale, utilisée pour les élections de 1896
Le krash de 1893 aux US a plusieurs conséquences :
- Licenciements et baisse des salaires
- Révolte des syndicats 
- Répression par la police (alliée aux patrons), intervention de l'armée si besoin