Titre : [[Les conséquences de l’incendie de l’Opéra-Comique]]
Type : Source
Tags : #Source
Ascendants :
Date : 2024-02-11
***

## Propositions
### On ne connait pas réellement le nombre de fatalités de l'incendie - p357
On sait que l'incendie a fait entre 95 et 130 victimes. Le bilan s'est aggravé avec les blessés.

### Il y a eu de nombreux autres accidents dus au gaz dans les années 1880 - p358
mars 81 -> Nice -> 70 personnes
83, plusieurs explosions -> gros dommages Hotel de Ville et Sorbonne

### Réflexe sécuritaire de couper le gaz du fait des accidents - p358
Couper les robinets pour éviter les explosions en cas d'incendies. Bonne logique mais en pratique l'obscurité cause une panique générale.
Création de l'éclairage de sécurité.
En effet, avant que la lumière ne s'éteigne, les gens étaient calmes. p364

### Les dangers de l'Opéra Comique étaient perçus depuis 1840 - p360
Plusieurs centaines de rapports des sapeurs-pompiers ou ceux de la commission des théâtres.
1874 : Lettre au prefet de police demandant de refuser les nouveaux délais à la direction du théâtre.
Steenackers avait là aussi prédit le pire p361

### Le colonel Couston qualifie l'opéra comique de scène la plus dangereuse de Paris - p361
Suite aux rapport des pompiers, le chef de corps des sapeurs pompiers de Paris
Il rédigea ensuite une lettre le 3 avril 1886, alertant sur les départs de feux.

### Les problèmes de l'Opéra Comique provenaient en grande partie du gaz mais ce n'est pas tout - p361
Beaucoup d'objets dans tous les sens, du désordre, du bois, un toit pas adapté, peu d'aération, des rideaux dangereux, etc...

### L'éclairage de sécurité existe depuis 1860 - p364
Pour éviter les mouvements de panique à cause de l'obscurité 