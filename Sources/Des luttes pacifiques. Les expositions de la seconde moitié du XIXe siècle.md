Titre : [[Des luttes pacifiques. Les expositions de la seconde moitié du XIXe siècle]]
Type : Source
Tags : #Source #Fixette 
Ascendants : [[Fixette]]
Date : 2024-01-06
***

## Propositions

### Le nationalisme des expositions du XIXe siècle est aussi couplé à une volonté d'unification du monde
[[Le nationalisme des expositions du XIXe siècle est aussi couplé à une volonté d'unification du monde]]
En cette fin du siècle, la mondialisation commence à vraiment s'imposer. Une volonté partagée par de nombreuses figures du siècle comme Victor Hugo, Napoléon III et Saint-Simon souhaite voir l'unification du monde. 

Werner Plum parle de luttes pacifiques. C'est en tout cas un oxymore retrouvé souvent par les historiens et écrivains de l'époque. On comprend que l'idée est de séduire par le soft power, de gagner par le prestige. 

### Les expositions sont toujours des moments de remaniements architecturaux et urbanistiques
[[Les expositions sont toujours des moments de remaniements architecturaux et urbanistiques]]
En tant que vitrines de pays en représentation, les Expositions universelles sont aussi des moments de profonds remaniements urbanistiques et architecturaux, que certains peintres se plaisent à illustrer, comme Georges Clairin avec _Les Fontaines lumineuses à l’Exposition de 1889_.

### L'Exposition de 1878 est la première à consacrer une partie aux techniques, celle de 1880 se consacre exclusivement à une technologie
[[L'Exposition de 1878 est la première à consacrer une partie aux techniques, celle de 1880 se consacre exclusivement à une technologie]]
> Après l’Exposition universelle de 1878, dans un souci d’approfondissement pédagogique et de renouvellement, les autorités de l’Union centrale décident d’organiser des « expositions technologiques ». Celles-ci présentent les matières premières, les outils de travail et un ensemble d’œuvres remarquables du point de vue de l’art et de la technique.

Celle de 1880, la première « exposition technologique », est consacrée au métal avec l’orfèvrerie, la joaillerie, la bijouterie, les bronzes d’art et d’ameublement, ainsi que la fonte.


[[@Des luttes pacifiques. Les expositions de la seconde moitié du XIXe siècle,]]