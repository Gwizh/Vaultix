Titre : [[Les lumières de la ville (XIX e -XX e siècle)]]
Type : Source
Tags : #Source #Fixette 
Ascendants : [[Fixette]]
Date : 2024-01-10
***

## Propositions
### Simone Delattre étudie la nuit à Paris au XIXe dans les douzes heures noires
Elle regarde les archives judiciaires et policières de la capitale. Elle note l'opposition entre une "nuit haussmannienne" active, éclairé et un "ancien régime nocturne" et ses couvre-feux.

### La lumière produit une ségrégation socio-spatiale dans la ville du XIX selon Sophie Reculin
L'éclairage vient du pouvoir central. Ce sont d'abords les lieux de pouvoir qu'on éclaire. les faubourgs restent dans l'obscurité. Le métro amène l'éclairage dans les sous-terrains mais de manière linéaire, non radiocentrique.

## Source
[[@Les lumières de la ville (XIX e -XX e siècle)_,Sophie Reculin]]