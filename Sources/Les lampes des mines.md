Titre : [[Les lampes des mines]]
Type : Source
Tags : #Source, #Fixette
Ascendants :
Date : 2024-02-21
***

## Propositions
### Quelques faits sur le grisou - p10
Découvert au XVe siècle. Mélange de gaz (Méthane, azote, O2 et CO2)
< 6% : Flamme bleue sans danger
[6, 10%] : Détonant
[16, 30%] : Nada
> 30% Anoxie

Inodore. Pas dans les mines de charbon mais aussi sel, fet, bitume, plomb, argile, souffre, etc...
Grisou peut jaillir à une pression de 10m3/min !
### La poussière de charbon et ses dangers - p13
Maladie : pneumoconiose du houilleur.
Poussières de charbon en suspension dans l'air.
Faraday fut l'un des premier à conclure qu'elles représenaient un danger. Pourtant, on était scéptiques en France, refusants de voir aute chose que du grisou.
Jusqu'à 1906 et ses 1099 victimes.
### Techniques pré-lampes de sûreté contre le grisou dans les mines au XIXe - p16
Première méthode pour éviter les explosions était de faire bruler le grisou avant qu'il n'explose. Dès 1650 en GB, un homme était envoyé en vêtement de cuir mouillés, le visage dans une cagoule. "Pénitent" ou "canonier". On envoyait généralement des prisonniers vu le risque.
Même au XIXe avec les lampes de sûreté on a continué à faire ça. Par exemple à la Rive-de-Gier.
Arrêté du 20/09/1835 en Loire !
Voir aussi lampes éternelles et Toque-feu
### Rave (ou crézieux) à flamme nue pour les mines non grisouteuses - p18
À partir du XVIIe.
Réserve d'huile pour bruler près de 10 heures. Plus ou moins décorée.
Huiles végétales ou animales.
Parfois huiles au frais des mineurs. Révolte en 1862 pour lutter contre ça à Carmaux. Lampes Clozet très connues.
Il y a aussi le flint-mill, la blende, la midgie, etc...
### Il y a des coups de grisou dès 1760 en Angletterre - p24
Grande industrie charbonnière d'Angleterre de l'époque augmenta le nombre de mines. Tellement d'accidents que le "Newcastle Journal" recommanda en 1760 à ses lecteurs de ne pas en parler pour ne pas effrayer les ouvriers.
Coup de grisou de 1812 Brandling Main -> 92 morts !
Fondation de la "Sunderland Society for Preventing Accidents in Coal Mines"
### Diffusion des 1ères lampes de sureté en 1816 - p29
Au RU, les lampes Davy sont utilisées dès 1816. Après 1847 et un article de Henry de la Beche et Lyon Playfair qu'elles sont devenues obligatoires.
Mais pas de service d'inspection des mines. Production de lampes de sureté en Belgique et en France. Blacet Cadet à S-E.
Utilisation du gaz domestique ! en 1844 à Quaregnon.
### La plupart des accidents au grisou en France au XIXe était du aux lampes à feu nu - p30
Sur les près de 800 accidents en France entre 1817 et 1884, 52.9% sont dus à l'utilisation de lampes à flamme !
Inconscience des mineurs et exploitants. Ingénieur en chef de la Loire fit part de ses craintes quant aux lampes de sécurité Davy.
Carmaux en 1858 continuait de distribuer des lampes à feu nu.
Maison Blacet à S-E -> tjs des raves !
Lampes, meilleures OK mais pb était que les Davy n'étaient pas utilisées !
### Lampes de sûreté qui éteignaient la flamme au dévissage - p38
Dubrulle en 50 et Stephenson ont mis en place des systèmes mécaniques qui éteignaient les flammes pour éviter les accidents par inconscience des mineurs.
### Premières lampes électriques en France et RU - p136
Dès 1883, il y avait des lampes électrique
"Royal Commission" de 1886 examina plusieurs modèles et porta 1 avis favorable à leur emploi. Concours en 1910 suite à deux importantes explosions.
1910 : 2055 lampes élec en Angleterre
1915 : 75707 lampes ! (contre 750000 à flamme)
1920 en France pour les 1ères fois au Nord-Pas-de-Calais.
Inconvénient : Lampes trop lourdes et n'indiquent pas le grisou.
### "Mise en sommeil" des lampes à flamme en France en 1985 - p136
Puit Simon V en Lorraine, explosion de grisou -> 22 morts et 103 blessés. En 1960, sur 126640 mineurs : 22000 lampes à flammes, 3150 lampes à élec à main, 149000 lampes au chapeau.