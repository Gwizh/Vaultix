Titre : [[Le parti polymorphe en Europe de Est]]
Type : Source
Tags : #Source
Ascendants : [[Socialisme]], [[Communisme]], [[Guerre froide]]
Date : 2023-12-07
***

## Propositions
### Il y a plusieurs interprétations sur le réel système politique en Europe de l'Est durant la Guerre Froide
[[Il y a plusieurs interprétations sur le réel système politique en Europe de l'Est durant la Guerre Froide]]
Cela va vraiment dans tous les sens et il n'y a pas de vision claire aujourd'hui.
Certains proposent la théorie du Socialisme d'Etat (Naville et Lane).
D'autres le capitalisme d'Etat (Bettelheim, Voline, Cliff)
D'autres un état ouvrier bureaucratisé (Trotski)
D'autres un état fasciste (Plioutch) !

La source ici propose le parti polymorphe. 

### Le Parti léniniste était le point central mais multiforme des socialismes de la Guerre froide
Ce serait l'institution unique de ce régime
Il y a deux caractéristiques apparemment
- Le Parti est le lieu de pouvoir de la politique léniniste. Tout passe par lui comme un pôle. Les "instances inférieures" que sont les syndicats et organisations sociales par exemple sont intégrés au Parti. Il n'y a pas de multiplicité institutionnelle en Europe de l'Est.
- Le principe d'organisation est le pouvoir - c'est pour Lénine la même chose.

### Le Parti bolchevique a continué à accroitre en pouvoir après la Révolution
Du fait de Staline, le Parti qui Lénine a fondé pour supplanter l'Etat Russe prérévolutionnaire ne va pas dépérir mais au contraire se renforcer.

### Marx et Lénine ne voyaient pas le Parti politique de la même manière
[[Marx et Lénine ne voyaient pas le Parti politique de la même manière]]
La source n'évoque pas trop les différences mais c'est tout de même super intéressant !

Nous pouvons apparemment le lire sur Pages Choisies Pour Une Ethique Socialiste [ici](https://www.chasse-aux-livres.fr/prix/F035234798/pages-pour-une-ethique-socialiste-choisies-traduites-et-presentees-par-maximilien?query=Pages%20choisies%20pour%20une%20%C3%A9thique%20socialiste)


## Source
[[@Le parti polymorphe en Europe de l'Est,Thomas Lowit]]