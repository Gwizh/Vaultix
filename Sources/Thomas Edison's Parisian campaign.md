Titre : [[Thomas Edison's Parisian campaign]]
Type : Source
Tags : #Source #Fixette 
Ascendants : [[Fixette]]
Date : 2024-02-02
***

### Fox dit que les lampes à incandescence avaient une grande popularité depuis l'Exposition de 1878 - p157
[[Fox dit que les lampes à incandescence avaient une grande popularité depuis l'Exposition de 1878]]
La bougie de Jablochkoff a été présentée en 78 et sembla avoir attiré du monde. C'est ce qui a permis d'affirmer le nom de ville lumière pour Paris.

### Fox note la supériorité des lampes à incandescence et du système d'Edison après l'Exposition de 1881 - p158
[[Fox note la supériorité des lampes à incandescence et du système d'Edison après l'Exposition de 1881]]
Alors qu'il y avait peu d'opinions de formulées sur les différents systèmes, l'expo semble avoir changé les avis.
La lampe à incandescence s'est montré digne de concurrencer les lampes à gaz et les bougies de Jablo. Mais il semble que la supériorité d'Edison s'est aussi établie par rapport à ses rivaux : Maxim, Swan et Lane-Fox.

## Propositions