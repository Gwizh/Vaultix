Titre : [[Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité]]
Type : Source
Tags : #Source #Fixette
Ascendants :
Date : 2024-01-19
***

## Propositions
### Première apparition connue de l’expression Ville lumière
[[Première apparition connue de l’expression Ville lumière]]
Paris, incarnation de la « Ville Lumière » a une longue et fière histoire qui remonte jusqu’à 1470, date à laquelle le premier livre issu de la première presse typographique en France était consacré à Paris. « De même que le soleil répand partout la lumière, ainsi toi, ville de Paris, capitale du royaume, nourricière des muses, tu verses la science sur le monde. »

### Dès 1881, Edison pu éclairer l’Opéra de Paris
[[Dès 1881, Edison put éclairer l’Opéra de Paris]]
En 1881, l’Opéra de Paris ouvrit la voie à une conversion complète à l’électricité en installant des lampes Edison dans le foyer. En 1883, l’Opéra fit d’autres installations électriques destinées à préserver les peintures du plafond exécutées par Paul Baudry. Cet effort fut acclamé comme un « succès pour la société Edison »

Voir à ce sujet le document récapitulatif :

[P84.18 : p.585 - vue 589 sur 646 - Cnum](https://cnum.cnam.fr/pgi/fpage.php?P84.18/589/80/646/0/0)

### Les années 1880 voient un changement de style pour les théâtres, plusieurs explications existent
[[Les années 1880 voient un changement de style pour les théâtres, plusieurs explications existent]]
[Wood Cordulack](https://www.cairn.info/publications-de-Shelley-Wood-Cordulack--112345.htm) nous dit sur les théâtres :

“Bien que ceci n’ait pas encore été confirmé auparavant, cette époque du début de l’électrification coïncida avec ce que plusieurs érudits décrivirent comme une nouvelle période dans l’art de Chéret. Le changement de style qu’il opéra au début des années 1880 est caractérisé par une simplification et une absence de relief. On a soutenu que ceci était certainement une conséquence de l’influence des estampes japonaises mais il est également très probable que ce fut le résultat de l’intensité de la nouvelle forme de lumière.”

Cela semble aller dans le sens de Schivelbusch.

### Les vulgarisateurs des sciences trouvaient en 1880 que l’électricité n’arrivait pas assez vite en France
[[Les vulgarisateurs des sciences trouvaient en 1880 que l’électricité n’arrivait pas assez vite en France]]
Louis Figuier, se lamentait en 1882 dans son livre sur _les Merveilles de l’électricité_ de la lenteur avec laquelle elle était installée dans Paris. En cette même année, dans la préface de leur livre _la Lumière électrique_, Émile Alglave et Jean Boulard en appelaient au conseil municipal, lui demandant d’agir sans hésitation « dans une question où l’esprit démocratique s’allie si heureusement à l’esprit scientifique »

### Résumé de l’ascention d’Edison à l’aube des années 1880 par Wood Cordulack
[[Résumé de l’ascension d’Edison à l’aube des années 1880 par Wood Cordulack]]
L’évènement étant bien couvert par la presse, les Français furent donc ainsi presque immédiatement informés des prouesses électriques américaines [[23]](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no23) [Edmond de Goncourt publia son roman Les Frères Zemganno…](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no23) . Edison présenta son système pour la lampe incandescente à l’Exposition internationale d’électricité à Paris de 1881, dont son exposition fut l’attraction principale

[[24]Pour une discussion sur la manipulation de la presse et de…](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no24)

La même exposition avait aussi en vitrine une galerie de peintures illuminées par des lampes à incandescence, donnant lieu à une gravure qui parut dans _La Lumière électrique_ (1881).

### Wood Cordulack évoque l’Opéra comique
[[Wood Cordulack évoque l’Opéra comique]]
L’année 1887 marqua un tournant dans l’histoire de l’électricité à cause de l’horrible incendie qui se déclara le 25 mai à l’Opéra comique. En l’espace de quelques mois, pratiquement tous les théâtres avaient installé l’éclairage électrique par mesure de sécurité pour se prémunir contre les risques de l’éclairage au gaz.

La source citée est celle-ci :

[https://www.persee.fr/docAsPDF/hes_0752-5702_1985_num_4_3_1402.pdf](https://www.persee.fr/docAsPDF/hes_0752-5702_1985_num_4_3_1402.pdf)

### La lumière de la statue de la liberté comme symbole de l’avance de la France
[[La lumière de la statue de la liberté comme symbole de l’avance de la France]]
Conçue et construite par les Français [[29]L’image d’une femme brandissant une lumière n’était pas en soi…](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no29) comme un présent aux États-Unis d’Amérique, la statue de la Liberté célébrait l’amitié franco-américaine et l’unité philosophique.

« Les amis des États-Unis ont pensé que le génie de la France devait se déployer sous une forme éblouissante. Un artiste français a exprimé cette pensée… La Liberté éclairant le monde. La nuit, une auréole resplendissante sur son front va lancer ses rayons par delà le vaste océan. » [[30]](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no30)

L’illumination électrique de la statue « servirait à illustrer le triomphe des derniers développements de l’électricité, ainsi que l’amitié unissant les deux grandes Républiques ».

### Wood Cordulack émet l’hypothèse que c’est Lowery qui a lancé Edison sur le marché de l’éclairage électrique
[[Wood Cordulack émet l’hypothèse que c’est Lowery qui a lancé Edison sur le marché de l’éclairage électrique]]
Visitant l’Exposition en 1878, Grosvenor P. Lowery, un avocat américain spécialisé en propriété industrielle, fut captivé par l’installation de lumière à l’arc de Jablochkoff [[33]La bougie de Jablochkoff, inventée en 1876 par le Parisien…](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no33) et envoya un dossier sur le sujet à son ami Thomas Edison, avec l’espoir d’inspirer l’inventeur et de susciter une nouvelle expérience pour une application pratique de l’éclairage électrique.

### L’imagerie féminine de l’électricité influencée par la statue de la liberté
[[L’imagerie féminine de l’électricité influencée par la statue de la liberté]]
Avant 1884, la lumière électrique était représentée avec une simple tour ou un phare. Mais petit à petit si je comprends bien, on commença à utiliser une métaphore féminine pour évoquer l’électricité. → [https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#pa17](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#pa17)

Les artistes français s’appropriaient donc graduellement des images réunissant les rayons de l’électricité et de la statue de la Liberté qui, ironiquement, ne fonctionnèrent jamais comme l’avait prévu Bartholdi, pour vendre la modernité.

### Wood Cordulack évoquant les fontaines lumineuses de l'Exposition de 1889
[[Wood Cordulack évoquant les fontaines lumineuses de l'Exposition de 1889]]
Un des spectacles les plus populaires de l’exposition était « les fontaines lumineuses ». Une de ces fontaines représentait une statue tournée vers la tour Eiffel, nommée « La France éclairant le monde » avec sa torche et ses figures allégoriques arrosées de jets d’eau illuminés électriquement. Au cours de cette année, la statue de la Liberté fut momentanément illuminée et photographiée de nuit par S.R. Stoddard, et le modèle réduit de _la Liberté éclairant le monde_ fut transporté de la place des États-Unis à l’île des Cygnes, où elle se trouvait derrière la tour Eiffel.

### Edison lors de l'exposition de 1889 par Wood Cordulack
[[Edison lors de l'exposition de 1889 par Wood Cordulack]]
Une photographie de l’Exposition de 1889, rappelle l’exposition d’Edison qui comprenait une copie de grande dimension du tableau de Robert Outcault de 1880 montrant Menlo Park avec une ampoule à incandescence géante superposée au paysage.
L’exposition comportait aussi les drapeaux américain et français illuminés avec un portrait en buste de l’inventeur américain surmonté de l’aigle américain et l’inscription « Edison 1889 » dans des ampoules allumées.

### Edison reçu la Légion d'honneur en 1889 suite à l'exposition
[[Edison reçut la Légion d'honneur en 1889 suite à l'exposition]]
```
La presse continua de rendre compte de l’énorme battage publicitaire qui entoura l’arrivée d’Edison à Paris en août de cette année, y compris les délégations officielles, les interviews, les foules et l’écharpe de la Légion d’honneur remise par le président français ; ainsi que le déjeuner à la tour Eiffel avec une invitation d’Eiffel lui-même. La photo d’Edison fut publiée dans le numéro du 28 août 1889 du _Bulletin officiel de l’Exposition universelle,_ mais les articles de presse n’étaient pas tous très flatteurs à son égard [[52]](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no52)[[52]Voir la brève discussion dans Les dossiers du Musée d’Orsay,…](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no52). Lors de sa participation à l’Exposition, Edison acheta à A. Bordiga une statue nommée « _Le Génie de l’électricité_ », qui brandissait et célébrait le triomphe de l’ampoule à incandescence. Edison exposa cette pièce bien en vue dans sa bibliothèque de West Orange dans le New Jersey, où elle est toujours visible.
```

### La rivalité franco-américaine était perçue par les contemporains à travers l'éclairage électrique
[[La rivalité franco-américaine était perçue par les contemporains à travers l'éclairage électrique]]
Dans son essai sur l’Exposition internationale de 1889, Figuier révèle que les rivalités sous-jacentes entre les États-Unis et la France étaient bien perceptibles dans la conception de l’éclairage électrique, et faisait la liaison entre l’électricité et le chauvinisme français, caractérisant l’électricité comme « l’événement pacifique le plus colossal du XIXe siècle, celui qui, pour relever le juste prestige de notre patrie, a fait plus qu’une guerre victorieuse »  [[57]Louis Figuier, « L’Exposition universelle de 1889 », L’Année…](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no57).
