Titre : [[Spinoza et les passions du social]]
Type : Source
Tags : #Source
Ascendants : [[Philosophie]]
Date : 2023-12-09
***

## Propositions
### Frédéric Lordon - La querelle du déterminisme en sciences sociales : un point de vue spinoziste

#### Influence de Spinoza sur Mauss et Durkheim - p105
[[Influence de Spinoza sur Mauss et Durkheim]]
Spinoza avait le bon instinct de dire que la société humaine n'était pas un empire dans un empire mais bien une part de la nature et de l'univers au global. La morale individualiste métaphysique s'oppose alors à la pensée moderne, la science et la causalité déterministe.

#### Bourdieu n'est pas assez déterministe pour Lordon - p108
[[Bourdieu n'est pas assez déterministe pour Lordon]]
Bourdieu n'est pas un pur déterministe, il a laissé une part de liberté à sa théorie, une part de l'homme qui échappe au déterminisme. Il a aussi laissé entendre qu'il y aurait plusieurs déterminismes.

#### Déterminisme strict de Spinoza selon Lordon - p110
[[Déterminisme strict de Spinoza selon Lordon]]
Tout dépend des affects qui existent déjà autour de nous. Tel affect provenant de tel autre corps à travers notre corps, nous affecte, nous fait désirer puis nous fait bouger .Un corps qui bouge est un corps qui a été affecté, déterminé à un certain désir de mouvement pour une certaine affection.

Les institutions sont régies par des corps affectés et agissent pareil

Lire Eth III def 27.

#### Le social est en tous les individus selon Spinoza - p111
[[Le social est en tous les individus selon Spinoza]]
Nous avons le global en nous du fait de tous les affects que nous avons reçu dans notre vie. Nous avons gardés des traces, nous sommes nous-mêmes trace et ces traces sont notre ingenium, notre complexion corporelle. Nous recevons les normes du global par des acteurs locaux.

#### Les transclasses n'échappent pas au déterminisme - p113
[[Les transclasses n'échappent pas au déterminisme]]
Les transclasses sont généralement utilisés pour contrer Bourdieu et le déterminisme. Le self-made-man a pu pourtant très largement faire ce changement par des affects. Eth IV, 7.

Par définition, les transclasses dépassent l'entendement sociologique car on ne peut voir que les affects locaux.

#### La réflexivité ne sort pas du déterminisme - p116
[[La réflexivité ne sort pas du déterminisme]]
Réfléchir à nos actions n'est pas en soi du libre-arbitre. Le fait d'être conduit à la réflexion est déterminée. Au final, l'action n'en est que mieux réfléchie mais c'est tout.

#### Le déterminisme n'est pas le fait de tout excuser - p121
[[Le déterminisme n'est pas le fait de tout excuser]]
"Si tout est écrit, pourquoi agir et si j'ai ça, c'est parce que j'étais déterminé"
#Alire Note 13
Nous sommes puissants, nous affectons le monde autour. Il ne faut pas comprendre le déterminisme comme un abandon. Il est un moyen d'entendre la complexité du monde alors ne faisons pas exprès de rendre ce concept simple. Comme nous affectons, nous pouvons travailler à ce que certains affects touchent les gens. Comme nous ne pouvons pas entendre la complexité, nous ne savons pas si notre présence dans une situation (pourtant typique), pourrait provoquer une réaction bénéfique ou mauvaise.

### Eva Debray - Imitation des affects et production de l'ordre social : une critique spinoziste de l'approche de René Girard
#### Critique spinoziste de l'approche de René Girard - p130
L'anthropologie de Girard et la philosophie spinoziste sont proches par leur étude commune de l'imitation des désirs. Girard critique comment la sociologie moderne pense que le social n'entrerait nullement de la détermination de nos actions et que l'ordre social dépendrait d'un contrat social.

#### Girard et sa critique d'une conception monadique du comportement humain - p132
[[Girard et sa critique d'une conception monadique du comportement humain]]
"Monadique" signifie qu'on observe qu'1 seul élément de la globalité. La conception actuelle veut qu'on étudie un homme seul et qu'on détermine les interactions ensuite. Un malade serait un "monade" au sein du monde social. Le désir est typiquement vu comme "choisi".

#### Girard pense que les individus ne peuvent pas réellement former un contrat social - p134
[[Girard pense que les individus ne peuvent pas réellement former un contrat social]]
Un contrat social enraciné dans la "raison", le "bon sens", la "bienveillance". L'auto-contrainte des individus ou des institutions est impossible à bien des niveaux. On ne peut que produire des affects. Il ne faut pas fonder la politique sur la raison mais sur la passion.

#### Origine du désir pour Girard - p137
[[Origine du désir pour Girard]]
Le désir vient quand on voit un autre désirer et posséder un objet. C'est l'interdiction du partage qui provoque le désir. Différence entre la plénitude de l'un et le vide de l'autre. On imagine l'autosuffisance de l'autre.

#### Caractéristiques du désir pour Girard - p138
[[Caractéristiques du désir pour Girard]]
Valeur d'un objet ne provient que de la résistance du modèle (la personne qui possède). Il y a forcément déception au moment où obtient sans qu'on voit que ça provenant du mimétisme.
Rivalité : disciple comme modèle ont mais différemment et jamais assez. Finalement, le disciple ne revendiquera pas l'objet mais aussi la violence que le modèle utilise pour interdire l'objet.

#### Violence et lynchage selon Girard - p140
Quand des disciples et des modèles ont répandus la violence de désir pour un objet dans toute cette société. Alors il peut y avoir lynchage de tout le groupe sur 1 individu, c'est un modèle qui prend avoir interdit. C'est alors la violence l'objet comme signe d'autosuffisance. Puis retour au calme car pas de vengeance et cela prouve qu'il était coupable. 

#### Sacralisation de la victime pour Girard - p142
La paix revient après avoir sacrifié la victime et elle devient alors le symbole du passage du désordre à l'ordre. Sacralisation de cette victime manipulatrice aui aura voulu tout ça, même sa mort (ou expulsion). Signes de Dieu. Bannir cette violence pour ne pas provoquer la colère divine, interdiction par l'Etat, inscription dans la morale.

#### Une imitation ne peut-être en elle-même source de conflit selon Spinoza - p146
Nous imitons les affects de qq sujet de nature semblable (Eth III 27).
Puis : nulle chose ne peut être mauvaise par ce qu'elle a de commun avec notre nature. Lien avec la sociologie récente qui dit que l'empathie réduit le conflit (note 50).

#### Spinoza et les émotions des affects communs - p150
Comme nous imitons les affects des autres, nous imitons le bonheur d'avoir un bien et le malheur de ne pas l'avoir. Nous ressentons donc bien de l'envie pour la personne qui détient mais aussi de la pitié pour celui qui n'a pas. Il y a fluctuatio animi, déchirement affectif lorsque nous prenons à qq'1. Il n'y a pas escalade du conflit, il y a aussi coopération et entraide.

#### Multitude comme seule pensée selon Spinoza
Comme les individus cultivent les mêmes affects de par leur proximité, une multitude d'individus peuvent avoir de mêmes affects en commun. Iels peuvent alors avoir entre eux une position de disciple ou de modèle si on reprend la pensée de Girard.

#### Lynchage imitant l'institution régulatrice selon Matheron citant Spinoza
A attaque B et C observe. C va s'allier avec celui qui lui ressemble le plus, souvent B car on ressent de la peine à voir souffrir.
Si plein de C, on a alors des normes sur les affects et c'est celui qui sera le plus bizarre selon ces normes que les C attaqueront. Création d'un système qui avantagera A ou protègera B.

Placer l'indignation comme principale instigateur
#### La peur des conséquences matérielles ne suffit pas pour constituer l'ordre social selon Debray - p161
Les différents types d'affects possibles et décrits par Spinoza nous permettent d'expliquer bien mieux l'ordre que la simple envie et l'indignation.

#### Evolution de Spinoza quand à sa théorie politique
Rappel de l'ordre : 1 Traité théologique -> 2 Ethique -> 3 Traité politique (pas achevé)
Dans 1 il était très contractualiste en reprenant des concepts de Hobbes. Puis dans 3, il explique quasiment tout par un jeu des passions. 
On peut même être contractualiste en imaginant que la peur puissante de passer pour un fou nous fait accepter le contrat social.

### Christophe Miqueu - Des luttes sociales en démocratie : Pettit face à Spinoza, de la contestation au conflit
#### Spinoza n'est pas un républicain comme les autres - p168
Il s'oppose à la République anglaise de l'époque qui se centrait sur la neutralité de la Raison. Oceana d'Harrington
Spinoza renonce à cette rationalité et préfère l'économie des passions. 

#### Définitions de la liberté selon Pettit - p169
[[Définitions de la liberté selon Pettit]]
Négative : être libre par défaut mais à condition de n'avoir aucunes entraves à notre volonté -> libéral
Positive : être libre et être déterminé rationnellement et non affectivement -> populiste
Non domination : être libre c'est n'être soumis à aucun autre. Refus de toute interférence arbitraire

#### La loi en fonction de la définition de la liberté selon Pettit - p170
[[La loi en fonction de la définition de la liberté selon Pettit]]
Négatif : forcément une contrainte à ma volonté
Positif : conditionne ma volonté e l'orientant vers une certaine conception du bien commun.
Tjs l'impact du plus fort. 
Dans un système négatif, c'est celui qui fait ce qu'il veut en dépit des contraintes.
Dans un système positif, c'est celui qui peut imposer sa conception du bien
Dans les deux cas, les lois sont ce qui permet la fin de la force car les lois sont "délibérées rationnellement".

#### Différence de fondation entre l'anthropologie de la république anglaise et celle de Spinoza - p172
[[Différence de fondation entre l'anthropologie de la république anglaise et celle de Spinoza]]
Si la République anglaise cherche avant tout à permettre à chacun leur définition de la liberté, cela ne permet pas selon Spinoza d'échapper aux conflits, aux haines, colères, ruses, etc...
La brique élémentaire d'un système politique selon lui devrait être les rapports de domination et non une liberté arbitraire. Il n'y a pas de légitimité selon Spinoza.

-> Autoconstitution démocratique de la puissance commune par les puissances individuelles.

#### Républicanisme de Pettit - p174
[[Républicanisme de Pettit]]
Pousser au + loin la non-domination grâce à la rationalité et les contre-pouvoirs.
Projet dynamique qui n'a pas de forme fixe. Pour pouvoir jouer son rôle de gestionnaire de la vie publique le pouvoir doit être discrétionnaire.
Comme on ne peut pas assurer le consentement de tous, il y a un besoin vital de contre-pouvoirs à toutes les échelles.

#### Spinoza aurait rejoint Breaugh sur la plèbe - p178
[[Spinoza aurait rejoint Breaugh sur la plèbe]]
La pensée de la plèbe heurte la logique délibératrice car elle a trait immédiatement aux affects. Ça rapproche le concept spinoziste de multitude. C'est l'unité d'une pluralité réelle.

#### Spinoza radicalement pour les mouvements des masses, constitutifs de la démocratie - p180
[[Spinoza radicalement pour les mouvements des masses, constitutifs de la démocratie]]
Contrairement aux visions de l'époque (théologie de Hobbes) la plèbe pour Spinoza constitue un mouvement démocratique en soi, passionnel par nature. Ensuite, le caractère démocratique ou despotique des institutions vient de la conquête ou non de la liberté commune.

### Nicolas Marcucci - La nature de l'obligation : Le moment Spinoza dans la première génération de sociologues en France
#### Naissance de la sociologie à la fin du XIXème siècle - p190
La première génération de sociologue français a vu le jour sous la IIIème Rep.
Il y avait une volonté de créer une autonomie propre à ce domaine pour étudier.
Volonté qui se pensait comme une obligation (Nécessité d'une action libre)

-> Permettait aux sociaux de réfuter la pensée libérale de l'époque et l'idée que le monde était épiphénomène des forces de la Nature.

#### Républicanisme français de la IIIe Rep selon Marcucci - p193
Principalement l'association de 2 concepts : liberté comme non-domination et individualisme libéral. C'est la recherche d'individus rejetant la société d'ordres, moins attaché au droit naturel que d'autres libéraux.

#Alire Note 8.

#### François Eswald et la redéfinition du concept d'accident par l'État social - p194
Si l'accident ne tient plus de Dieu mais ne tient pas non plus de la faute de chacun selon l'État social. L'accident vient du concours d'activités des individus.
-> Bonté naturelle comme Rousseau

#### Obéissance de Hobbes et Kant vs celle de Spinoza - p195
[[Obéissance de Hobbes et Kant vs celle de Spinoza]]
Hobbes et Kant sont considérés libéraux.
L'obéissance pour eux dépend du libre-arbitre et de la loi souveraine.
Spinoza dépasse complètement ça. Ce n'est pas des "formes de composition rationnelle" mais un procès de rationalisation continuelle.

#### Principaux concepts spinozistes selon Marcucci - p196
[[Principaux concepts spinozistes selon Marcucci]]
Il liste certains concepts géniaux : dépassement de l'individualisme moderne, théorie naturaliste des affects / passions, critique du contractualisme, réalisme conflictuel venant de Machiavel, critique du finalisme, conception du droit naturel.

#### Résumé des conceptions principales du XIXe siècle selon Marcucci - p196
Vision de la modernité humaine qui était autant le fait de l'individualisme utilitariste et libéral que du déterminisme organiciste et naturaliste du XIXe siècle

#### Critique spiritualiste du spinozisme au XIXe siècle - p198
Spinozisme perçu par les spiritualistes descendants d'Hegel comme une distorsion du cartésianisme. "Réfutation" du déterminisme. Par contre perception que la rationalité spinoziste pourrait s'inscrire comme un paradigme pertinent en sciences.

#### Réception naturaliste du spinozisme au XIXe siècle
Vacherot positiviste et naturaliste admet le rôle crucial qu'à joué le spinozisme pour les 2 courants. Spinoza était alors à la fois une inspiration et un adversaire. D'autres comme Jean-Felix Nourisson défendant Spinoza, pour eux à la base même de leurs théories, s'écroulent sans elle.

#### Comprendre le déterminisme global pour être libre selon Guyau et Spinoza - p216
[[Comprendre le déterminisme global pour être libre selon Guyau et Spinoza]]
Il n'y a de libre que le monde entier, le déterminisme global. Tenter de le comprendre, c'est participer à la liberté. La science est la nécessité ne fait qu'un avec la liberté. Notre existence, notre pensée, notre désir dépendent de l'existence, la pensée, le désir du monde entier.

#### Jean-Marie Guyau et son esquisse d'une morale sans obligation ni sanction - p217
[[Jean-Marie Guyau et son esquisse d'une morale sans obligation ni sanction]]
La morale ne peut pas être que philosophie nous dit-il. La science sociale doit pouvoir faire sans obligation ni sanction. Pas de métaphysique. L'utilitariste cherche un but et une finalité. Le sociologue cherche les infinies causes. Question de la source de l'obligation. Si ce n'est pas une rationalisation externe (naturel ou autoritaire), alors ça vient des existences individuelles.

#### Beaucoup de penseurs du XIXe catégorisaient Spinoza comme un contractualiste optimiste - p221
[[Beaucoup de penseurs du XIXe catégorisaient Spinoza comme un contractualiste optimiste]]
Ils font le rapprochement entre Spinoza, Hobbes et Rousseau avec cette idée de création rationnelle d'un système avec comme élément central un contrat social. Souvent en échec car Spinoza réfute l'idée que le contrat social soit si important.

#### Lectures postrévolutionnaires et anachroniques de Spinoza - p232
[[Lectures postrévolutionnaires et anachroniques de Spinoza]]
Il faut faire attention à ne pas déformer la pensée des philosophes d'avant la Révolution française. Spinoza en particulier ne décrivait pas les révolutions comme une bonne chose en soi car elles apportent nécessairement une perte de stabilité, un arrêt de la conservation. Et l'après marx voit aussi une interprétation sur la multitude et une politique du commun. 

Il a étudié la persévérance des choses et notamment des autres régimes (monarchiques, aristocratiques). Il était méfiant des révolutions qui ne réussissent souvent pas à remplacer un État par un autre. 
Sa vision de la démocratie est aussi plus large que la moyenne. Il pense que ce qui a fait la stabilité est paradoxalement les aspects démocratiques des monarchies et aristocraties.
Le suffrage ne fait pas pour lui la démocratie.

#### Comprendre l'État avec Spinoza - p235
[[Comprendre l'État avec Spinoza]]
La souveraineté ou l'État est la puissance de la multitude. Elle n'est pas rationnelle mais affective. On ne peut donc pas chercher et expliquer l'État par la Raison mais par les lois de la nature (pourquoi on en vient à constituer ces institutions).

#### Unité d'un corps selon Spinoza - p237
Un individu n'est jamais clos sur lui-même par le besoin de sociabilisation que lui donne la persévérance dans son existence. Toute individualité est collective. Def 2 de Eth II.
Choses singulières peuvent être plusieurs individus si iels encourent aux mêmes objectifs. 
Exemple [[Nuit debout]]

#### Lecture marxiste de la philosophie de Spinoza par Laurent Bove - p240
Formation d'institutions collectives par auto-organisation par l'effort d'affirmation et de préservation des individus. La rationalité de la démocratie vient de la collectivité anonyme et nombreuse.

#### Lecture par Sartre de la philosophie spinoziste - p241
Sartre marxiste non-orthodoxe de la Critique de la raison dialectique propose une réflexion sur l'individu et les groupes. 

#### Concept de série chez Sartre - p241
[[Concept de série chez Sartre]]
La série c'est pour Sartre l'unité collective du hasard, regroupement de gens au guichet de la poste à la même heure par exemple. Mus par de mêmes besoins et sûrement contraints de manière similaire. La série est une unité d'impuissance

#### Concept de groupe pour Sartre - p242
[[Concept de groupe pour Sartre]]
Le groupe se définit par le praxis de l'auto-organisation. Toujours basé sur un collectif précédent qui l'engendre et le soutient. Une grève existe dans le contexte industriel.

#### Concepts de la philosophie de Sartre - p244
Il y a des objets sociaux : 
- ensembles d'êtres humains, de faune, de flore, de matières, de technologies
- groupes

Ces objets peuvent être décrits et expliqués avec d'autres concepts comme l'organisme pratique, la praxis et le pratico-inerte.

#### Organisme pratique chez Sartre - p245
[[Organisme pratique chez Sartre]]
Conception marxiste du besoin couplé à la phénoménologie qui dément le besoin comme purement interne. Il y a toujours transcendance, l'organisme (vivant) pratique va vers l'extérieur, s'ouvre, pour répondre au besoin. Ce dépassement implique que pour persévérer dans son être, il faut le dépasser, le transcender. Nous avons à être son être.

#### Les besoins de l'organisme pratique de Sartre sont vécus - p246
[[Les besoins de l'organisme pratique de Sartre sont vécus]]
Le besoin est un manque *vécu*, dégradation organique qu'il s'agit de restaurer. Travail d'emblé.
Ce n'est pas une donnée naturelle, cause de u travail de l'homme oeconomicus. Les conditions matérielles sont alors le champ des possibles de l'auto-organisation.

#### Concept de praxis chez Sartre - p246
[[Concept de praxis chez Sartre]]
"C'est un projet organisateur dépassant la condition matérielle vers une fin..."
Possible incompatibilité avec le rejet de la finalité de Spinoza.
Rappel : Spinoza s'oppose à l'explication de la nature par les causes finales. Seul le conatus est une finalité.
La "mens" = conscience chez Sartre.

#### La liberté chez Sartre - p248
[[La liberté chez Sartre]]
La liberté est cardinale pour la philosophie, même pour les déterministes radicaux. Sartre ne parle pas de déterminisme mais admet un "destin social", l'individu reste un projet libre. La liberté se trouve dans la marge entre les effets du déterminisme sur l'individu et ce qu'il va vraiment faire un retour. Les hommes sont fait par et font l'histoire.

#### Marshall Sahlins et sa remise en question du concept de rareté - p251
[[Marshall Sahlins et sa remise en question du concept de rareté]]
Pour lui, la rareté est un concept de l'économie classique. Selon lui et les preuves ethnographiques, l'économie primitive n'était pas de subsistance. Nous avons connu des situations d'abondance sans capitalismes ni féodalisme. L'économie ne tournait pas alors sur la rareté. Mais il est donc avec Sartre que le ration population ressource compte.

#### Le lynchage spinoziste vu par Matheron - p253
2 concepts : 
1) le lynchage est la forme élémentaire de la démocratie, c'est lui qui laisse 1 trace et qui est le modèle des systèmes répressifs 
2) C'est l'indignation du lynchage qui conduit à l'État ou à la Révolution

#### Le pratico-inerte pour Sartre - p253
Un des concepts du renversement nature/histoire dialectique de Sartre. Nos actions sont complexes mais tout de même au final réductibles à une succession de processus inertes.
L'Homme gouverne la matière inanimée qui devient le pratico-inerte et qui gouverne en retour les Hommes.
La machine exige de l'ouvrier et de l'employé. C'est nous qui avons conçu la machine mais elle nous exige des comportements en retour.
#### Complexe fer-charbon et affections des Hommes par Ong Van Cung - p254
L'éclairage des usines à permit de faire travailler les ouvriers jusqu'à 15 à 16h par jour et a fait augemnter la production.
Mais ça pourrait tout à fait être l'inverse. 
Et ça pourrait être les deux (dialectique).

### Yves Citton - Puissances du social, puissances du nombre
#### Mathématisation des essences spinozistes selon Charles Ramond - p262
La puissance d'agir est 1 quantité. Nous sommes toujours + ou - aptes. Cette puissance se joue dans le rapport de mouvement et de repos. (Eth IV 38 et 39). Si ce rapport est conservé, c'est l'être qui est conservé et c'est alors une bonne chose. Nous pouvons donc plus ou moins calculé cette puissance des êtres.
#### Gouvernementalité statistiques de Berns et Rouvroy - p264
La mesure et la simulation est 1 façon non neutre de gouverner. Mesurer c'est déjà influencer. Modéliser c'est normaliser. 
On tombe vite dans une société des sondages.
#### Les statistiques permettent de comprendre la puissance du nb - p266
Pas d'études de l'essence des individus dans la statistiques. On peut être tous différents et même se lever différent chaque matin. Si les individus comptent, c'est parce que la multitude a été mue. Napoléon et son armée.
#### Citton et la puissance du social - p267
Il prend 2 exemples : grève avec séquestration et enlèvement d'un enfant.
Les 2 sont des faits divers mais ne nous enseigne pas autant sur l'état du monde. L'un augmente la puissance, l'autre la diminue. Pour faire augmenter la puissance à partir d'un exemple de type 2, il nous faut soit la statistique, soit le récit. La première par a compréhension de la multitude, l'autre par la communication avec elle.

#### La liberté chez Sartre - p248
[[La liberté chez Sartre]]
La liberté est cardinale pour la philosophie, même pour les déterministes radicaux. Sartre ne parle pas de déterminisme mais admet un "destin social", l'individu reste un projet libre. La liberté se trouve dans la marge entre les effets du déterminisme sur l'individu et ce qu'il va vraiment faire un retour. Les hommes sont fait par et font l'histoire.

#### Marshall Sahlins et sa remise en question du concept de rareté - p251
[[Marshall Sahlins et sa remise en question du concept de rareté]]
Pour lui, la rareté est un concept de l'économie classique. Selon lui et les preuves ethnographiques, l'économie primitive n'était pas de subsistance. Nous avons connu des situations d'abondance sans capitalismes ni féodalisme. L'économie ne tournait pas alors sur la rareté. Mais il est donc avec Sartre que le ration population ressource compte.
## Source
[[@Spinoza et les passions du social,]]