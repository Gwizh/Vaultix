Titre : [[L'Écho des mines et de la métallurgie - 13-03-1887]]
Type : Source
Tags : #Source
Ascendants : [[L'Écho des mines et de la métallurgie]], [[Mines]]
Date : 2024-04-01
***

https://gallica.bnf.fr/ark:/12148/bpt6k5740172m/f3.image.zoom
## Propositions

### Francis Laur donne des détails sur la catastrophe du Puit de Châtelus dans l'écho des mines
[[Francis Laur donne des détails sur la catastrophe du Puit de Châtelus dans l'écho des mines]]

### Des lampes Mueseler à fermeture ordinaire étaient utilisée à Châtelus dans les années 1880s
[[Des lampes Mueseler à fermeture ordinaire étaient utilisée à Châtelus dans les années 1880s]]

### Hanarte ne parle pas dans les lampes de mines dans ses précautions face au grisou en 1887
[[Hanarte ne parle pas dans les lampes de mines dans ses précautions face au grisou en 1887]]