MOC : 
Source : [[Fronde (histoire)]]
Date : 2023-05-22
***

## Résumé
La Fronde (1648-1653) est une période de troubles graves qui frappent le royaume de France alors en pleine guerre contre l’Espagne (1635-1659), pendant la minorité du roi [[Louis XIV]] (1643-1651). Cette période de révoltes marque une brutale réaction face à la montée de l’autorité monarchique en France commencée sous [[Henri IV]] et [[Louis XIII]], renforcée par la fermeté de [[Richelieu]] et qui connaîtra son apogée sous le règne de Louis XIV. Après la mort de Richelieu en 1642, puis celle de Louis XIII en 1643, le pouvoir royal est affaibli par l'organisation d'une période de régence, par une situation financière et fiscale difficile due aux prélèvements nécessaires pour alimenter la [[Guerre de Trente Ans]], ainsi que par l'esprit de revanche des grands du royaume subjugués sous la poigne de Richelieu. Cette situation provoque une conjonction de multiples oppositions aussi bien parlementaires qu’aristocratiques et populaires. 

## Référence

## Liens 