MOC : 
Source : [[DrahiLeaks]]
Date : 2023-04-12
***

## Résumé
Courant août 2022, le groupe de hackers russes Hive a mis en ligne dans un recoin caché d’Internet des centaines de milliers de documents piratés à Altice après avoir échoué à faire chanter l’homme d’affaires. [Reflets](https://reflets.info/), [Blast](https://www.blast-info.fr/) et StreetPress se sont associés pour explorer ces leaks.

Les documents mettent en lumière un groupe industriel complexe, implanté dans des pays très souples en matière fiscale et très endetté. Ils donnent incidemment à voir le train de vie faramineux d’une famille aussi discrète que riche. **Bien loin de la fin de l’abondance annoncée par Emmanuel Macron**.

Drahi a voulu censurer cette affaire par des moyens juridiques mais la justice a reconnu que le travail était d'intérêt général et que les 3 journaux pouvaient continuer sans risque de censure. 

Deuxième volet : 
[[Pour payer moins d'impôts, Patrick Drahi a fait semblant de quitter sa femme]]
![[Drahi vide les caisses de SFR]]
On parle d'environ 15Mds d'€ 

## Référence

## Liens 