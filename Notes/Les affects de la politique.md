MOC : 
Source : [[Les affects de la politique]]
Date : 2023-07-17
***
[[Frédéric Lordon]]
## Résumé
#### Avant-propos
##### Lire hors de soi
Le lecteur d'un livre se projette dans le livre mais comme on ne peut complètement se projeter dans qq chose (ex [[Le Terrier]] de [[Franz Kafka]]), on se réapproprie l'oeuvre et celle-ci devient une oeuvre de degré n, modifiée par la lecture de tous les lecteurs. 

> "La lecture comme exercice nécessairement projectif est donc ambivalente : elle est au principe de la création secondaire qui prolonge l'œuvre au-delà d'elle-même; elle l'est également de la recherche hermétique de soi dans l'autre."

##### La politique en ses affects
Les affects de la politique est un pléonasme pour [[Spinoza]] car pour lui tout ce qui touche à la société tient des affects. Mais placer cette expression permet de parler de différent sujets et donc permet de se libérer de l'idée stricte. De plus, ça nous permet de clairement identifier pourquoi c'est un pléonasme.

#### Affect, idée, ingenium
##### Non pas les "émotions", les affects
La politique n'est essentiellement que passion. Ça semble exagéré, la politique étant aussi une histoire de débat, d'idées, etc...
L'affect chez Spinoza est le nom le plus général donné à l'effet qui suit de l'exercice de puissance. Une chose exerce sa puissance sur une autre, cette dernière s'en trouve modifiée : affect est le nom de cette modification. Le vent courbe une tige : la tige est modifiée - affectée. Une personne parle à une autre, qui se met en colère, ou bien s'en trouve agréablement songeuse : elle a été modifiée.

Il y a des ordres d'expression de la puissance - Spinoza les appelle des attributs (les attributs de la substance) -, et le conatus se décline aussi bien en puissance d'agir du corps (dans l'attribut de l'Étendue) qu'en puissance de pensée de l'esprit (dans l'attribut de la Pensée). Et pour Spinoza, il n'y de causalité qu'intra-attributive. Ainsi, seules des modes de l'Étendue peuvent déterminer d'autres modes de l'Étendue à faire qq chose, et aucune causalité n'est possible d'un mode de la Pensée à un mode de l'Étendue. 
"Le corps ne peut déterminer l'esprit à penser, ni l'esprit déterminer le corps au mouvement ni au repos." Seul l'image d'un ours nous fait peur, pas son idée.

Les idées en tant qu'idées n'ont pas d'effets sur notre corps mais l'image ou les images qui lui sont associées. Souvent, des idées ne nous émeuvent pas, elles ne nous font rien et nous ne bougeons pas. La politique est une affaire de passion car tout ce qui nous pousse à agir l'est. Mais donc pour être affecté par une idée, il faut qu'on ait des antécédents qui nous aient donnés des images. Spinoza récapitule sous le nom d'ingenium l'ensemble de nos susceptibilités affectives.

L'ingenium est aussi la récapitulation de toute notre trajectoire socio-biographique telle qu'elle a laissé en nous des plis durables - quoique toujours modifiables en principe - au fil des affectations - des rencontres - qui nous ont marqués. Ces marques ont formé nos manières : manières de sentir, de juger, de penser. À quoi ai-je été exposé et quels plis en ai-je gardés ? 

Mon ingenium, c'est moi. Mais un moi qui n'a du coup aucun caractère d'essence, un moi jamais fixé, toujours fluent, susceptible, selon les conditions, c'est à dire selon les rencontres et les affectations, de se déplacer...

###### Sociologie
La sociologie, celle de [[Pierre Bourdieu]] tout particulièrement, a montré l'inégale distribution du sentiment de légitimité à juger et parler en matière de politique, inégalité entre hommes et femmes, inégalités entre groupes sociaux, etc... Or c'est aussi affecté par nos affectations personnelles.

De même, la conscientisation entre dans l'ingenium. On peut être affectée par celle-ci et on peut la retenir dans les plis. La conscientisation, c'est la politique incorporée.

###### Relation avec les autres êtres vivants
Nous sommes ontologiquement les mêmes que tout ce qui existe : nous sommes une partie de l'univers. Mais il y a égalité dans l'être mais inégalité dans l'existence car nous ne sommes pas tous aussi puissant. L'amplitude des effets qu'un corps peut produire, c'est la complexité de son organisation interne qui en est l'index. #MIE On n'est pas différent pas nature mais par degré hihi.

> Mode est le nom que Spinoza donne à tout étant. 

Donc nous sommes puissants car nous créons des modes. 

###### Différence entre comprendre et expliquer
Comprendre, c'est ce que fait l'homme qui, projetant spontanément sa complexion dans le monde qui l'entoure, le met en sens. Expliquer, c'est rendre compte de cette activité "compréhensive" en reconstituant causalement la production des images, des idées, des significations et des jugements, qui résultent d'un certain exercice de la puissance de penser de son esprit.

Même si la politique n'était que raison, la raison est affect.

#### La politique, un _ars affectandi_
Le défaut de prédictabilité d'une intervention dans le milieu passionnel tient plus encore à la diversité des complexions au travers desquelles, par réfraction, elle va produire ses effets. Spinoza note plaisamment dans la préface d'[[Éthique]] IV : "La musique est bonne pour le mélancolique, mauvaise pour l'affligé; et pour le sourd ni bonne ni mauvaise". Ainsi une même affection produit-elle des effets très contrastés selon l'ingenium qu'elle traverse.

"Plutivocité"

L'intervention politique, comme en réalité toute intervention dans le monde social, est un pari sur les passions. Elle a nécessairement ce caractère car elle tourne entièrement autour de la question des effets - c'est à dire des affects. Ce que je m'apprête à faire, qu'est ce que ça va leur faire ?

###### Lutte pour la reconnaissance 
Spinoza dit : 
"Nous nous efforcerons de faire tout ce que nous imaginons que les hommes considèrent avec joie, et au contraire nous aurons de l'aversion à faire ce que nous imaginons que les hommes ont en aversion. " Eth III 29

[[Karl Marx]] parle aussi de validation sociale. 

Spinoza va plus loin :
"Si quelqu'un fait qq chose qu'il imagine affecter toutes les autres de joie, il sera affecté de joie accompagnée de l'idée de lui-même comme cause; autrement dit, il se contemplera lui même avec joie" Eth III 30


## Référence

## Liens 