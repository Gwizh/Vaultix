MOC : 
Source : [[Mouvement des sans-terre]]
Date : 2023-07-23
***

## Résumé
Le Movimento dos Trabalhadores Rurais Sem Terra (MST, Mouvement des travailleurs ruraux sans terre) ou Mouvement des sans-terre est une organisation populaire brésilienne qui milite pour que les paysans brésiliens ne possédant pas de terre, disposent de terrains pour pouvoir cultiver. Depuis la création du mouvement en 1985, 1 722 militants ont été assassinés.

## Référence

## Liens 
[[Brésil]]