MOC : 
Source : Individualisme méthodologique
Date : 2023-07-03
***

## Résumé
In the social sciences, methodological individualism is a framework that describes social phenomena as a consequence of subjective personal motivations by individual actors. Class or group dynamics, which operate on systemic explanations, are deemed illusory, and, thus, rejected or de-prioritized. With its bottom-up micro-level approach, methodological individualism is often contrasted with methodological holism, a top-down macro-level approach, and methodological pluralism.

## Référence

## Liens 
[[Individualisme]]