MOC : 
Source : [[Loi de 1884 sur l'organisation municipale]]
Date : 2023-07-07
***

## Résumé
La loi de 1884 sur l'organisation municipale est une des lois fondatrices de la République française. Elle a précisé le fonctionnement des communes françaises. Elle est à ce titre considérée comme l'une des premières lois de décentralisation. 


## Référence

## Liens 