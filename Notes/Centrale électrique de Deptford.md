MOC : 
Source : [[Centrale électrique de Deptford]]
Date : 2023-04-17
***
#Adam

## Résumé
Three distinct coal-fired power stations were built at Deptford on the south bank of the River Thames, the first of which is regarded as the first central high-voltage power station in the world.

https://web.archive.org/web/20110830091523/http://swehs_archive.swelocker.co.uk/news25su.pdf

## Référence

## Liens 
[[Sebastian Ziani de Ferranti]]
