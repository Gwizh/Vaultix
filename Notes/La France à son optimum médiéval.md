[[Livre]]
MOC : [[Histoire]]
Titre : [[Chapitre]]
Auteur : [[Jean-Christophe Cassard]]
Livre de base : [[L'âge d'or Capétien (1180-1328)]]
Date : 2022-08-05
***

<-- Chapitre Précédent : 
--> Chapitre Suivant :

## Résumé
### L'intensification des pratiques agricoles
#### Les derniers défrichements
L'Empire Romain avait entrainé avec sa chute la chute de la population gauloise mais aussi des cultures que ces derniers entretenaient. Il y a eu donc une reprise des territoires forestiers partout dans le pays. C'est seulement environ vers l'an mille que nous avons recommencer à nous approprier les territoires. Au XIIe siècle cependant, nous avions déjà mis en culture toutes les meilleures terres et rien ne valait la peine de défricher en masse. 

Les seuls défrichages restant à faire étaient les marais, véritable prouesse technologique qui demandait travail considérable. Mais les intérêts sont nombreux, par besoin de terres en plus pour les seigneurs par besoin de ressources en plus comme notamment le sel, or blanc de cette époque. 

Les villages sont créés de plus en plus en périphérie par les seigneurs pour d'abord se démarquer des autres mais aussi pour marquer des frontières de domaines. C'est notamment utile dans le cas d'un conflit comme typiquement entre la France et l'Angleterre. Ces frontières se définissent de plus en plus à mesure que les territoires deviennent accessibles. 

#### Un sol mieux cultivé
Le travail à la main est indispensable, il faut remuer la terre via plein de processus pour qu'elle soit apte à produire. Il faut aussi ajouter des murets en pierre autour des plantations pour les protéger. 

Il y a deux techniques à l'époque. 
L'araire, plutôt méditerranéen, peu coûteux mais ne creusant que des sillons peu profonds, adapté à une terre légère. 
La charrue, qu'il faut entretenir avec un forgeron spécialisé, qui comporte donc des pièces coûteuses en fer, est plus puissante et permet des sillons très profonds même dans des terres plus chargées en débris. L'utilisation d'une roue permet d'optimiser le terrain avec la possibilité de faire demi-tour. 

## Notes
La France Capétienne a commencé à enregistrer tous les aspects de la gestion du royaume et des duchés, dans l'idée d'améliorer et de fluidifier la société. C'est pourquoi nous avons beaucoup plus de sources aujourd'hui sur cette période. 



## Citations

## Passages amusants

## Références
