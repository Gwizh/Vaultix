MOC : 
Source : [[Traité d'indépendance franco-syrien]]
Date : 2023-07-02
***

## Résumé
Le traité d'indépendance franco-syrien de septembre 1936, également appelé Accords Viénot, est un traité diplomatique conclu entre la France et la [[Syrie]], alors placée sous mandat français de la Société des Nations, qui prévoyait l'accès de la Syrie à l'indépendance. 

La menace montante de l'Allemagne nazie doublée de penchants impérialistes persistants à certains niveaux du gouvernement français conduisent la France à reconsidérer ses promesses. Dans les milieux militaires, coloniaux et religieux, des oppositions s’élèvent contre la ratification des traités franco-syrien et franco-libanais. Face à l’opposition du Sénat, le Parlement ne ratifie pas le traité. De plus, la France cède la province d'Alexandrette, dont le traité prévoyait qu'elle intégrerait la République syrienne, à la Turquie. Des émeutes éclatent à nouveau, le président Hachem al-Atassi démissionne en 1939 et le projet d'indépendance syrienne est reporté sine die. 
## Référence

## Liens 
[[Liban]]