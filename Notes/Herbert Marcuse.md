MOC : 
Source : [[Herbert Marcuse]]
Date : 2023-06-18
***

## Résumé
Herbert Marcuse, né le 19 juillet 1898 à Berlin et mort le 29 juillet 1979 à Starnberg (Bavière), est un philosophe, sociologue marxiste, américain d'origine allemande, membre de l'[[École de Francfort]] avec [[Theodor Adorno]] et [[Max Horkheimer]].

## Référence

## Liens 