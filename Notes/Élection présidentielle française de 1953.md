[[Note Permanente]]
MOC : [[Histoire]] [[POLITIQUE]]
Source : [[Élection présidentielle française de 1953]]
Date : 2022-07-08
***

## Résumé
**L'élection présidentielle française de 1953** est la seconde élection présidentielle de la [[Quatrième République]]. Elle voit l'élection de [[René Coty]] après **13 tours de scrutin**. Il succède à [[Vincent Auriol]], élu en 1947. Première élection française à être retransmise à la télévision, la diffusion du processus électoral s'avère interminable et est interrompue par ordre du gouvernement avant la fin du scrutin, mettant alors en image les dysfonctionnements de la Quatrième République.

## Notions clés
Selon la Constitution de la Quatrième République, le président est élu à **la majorité absolue des suffrages par le Parlement**, c'est-à-dire le Conseil de la République et l'Assemblée nationale réunis en Congrès. 

Réunissant les suffrages de gauche, notamment ceux du Parti communiste français après un premier tour de scrutin marqué par le retrait du communiste Marcel Cachin, le socialiste Marcel-Edmond Naegelen va être le seul candidat à se maintenir pendant les 13 tours de scrutin, bloquant sur son nom la majorité des voix de gauche qui désapprouvaient la CED. Après l'échec de Georges Bidault pour le MRP, la droite se rassemble alors sur l'indépendant Joseph Laniel. Dès lors, une triangulaire s'installe entre Naegelen pour la gauche, Yvon Delbos puis Jean Médecin pour le centre radical et Laniel pour la droite. Ce dernier frôle de peu l'élection au 8e tour du scrutin, mais une manœuvre du président du Congrès, le socialiste André Le Troquer, lui retire des suffrages : des bulletins ne mentionnant pas son prénom pouvaient être attribués à son frère, le sénateur René Laniel.

Après le retrait de Joseph Laniel, son bras droit Jacquinot tente lui aussi, sans succès sa chance. Finalement, les suffrages se portent sur René Coty, de droite mais n'ayant pas voté pour la CED (car absent pour raisons de santé). Ce dernier a d'ailleurs déclaré : **« Je ne me fais aucune illusion : si je suis président de la République, c'est parce que j'ai été opéré de la prostate. Cette opération m'a dispensé de prendre parti pour ou contre la CED ».**

## Référence
[[Histoire de la France]]
[[Politque française]]

## Liens 