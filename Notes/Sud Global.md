***
Selon le monde :

#### Le « Sud global », cet ensemble hétérogène de pays non alignés
Désignant les pays autrefois dits du tiers-monde, la notion regroupe les Etats du sud, principales victimes des effets néfastes de la mondialisation et refusant de s’aligner sur l’un ou l’autre des puissants du Nord global, cet autre nom de l’Occident.

[[Une théorie féministe de la violence]]