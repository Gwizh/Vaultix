[[Livre]]
MOC : 
Titre : [[Chapitre]]
Auteur : 
Livre de base : [[Histoire de la France au XXe Siècle - Livre 3]]
Date : 2022-08-05
***

<-- Chapitre Précédent : 
--> Chapitre Suivant :

## Résumé

Depuis 1958 : ouverture du marché et à la concurrence
Passer de petites/moyennes entreprises à des grandes et internationales 

Mettre fin aux subventions pour les entreprises publiques pour leur donner de l'autonomie. ==> Libéralisation

Combler le retard d'exportation industrielle dans les secteurs haute technologie

Raffinage, banque, aviation, sidérurgie, naval, informatique

Investissments de l'Etat insuffisants -> Grands groups financiers : Suez, Rothschild, Paribas, Empain, Shneider

[[Georges Pompidou]] et l'impératif industriel -> Pas de groupe international pour autant 
[[Renault]] 1er groupe français mais 22e groupe international

Part du PIB 38.8% en 1973 pour 40% de la population

## Notes

## Citations

## Passages amusants

## Références
