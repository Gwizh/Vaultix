MOC : 
Source : [[Laurent de Médicis]]
Date : 2023-06-23
***

## Résumé
Laurent de Médicis (en italien Lorenzo di Piero de' Medici : « Laurent, fils de Pierre de Médicis »), surnommé Laurent le Magnifique (Lorenzo il Magnifico), né à Florence le 1er janvier 1449 et mort dans cette même ville le 8 avril 1492, est un homme d'État italien et le dirigeant de facto de la République florentine durant la Renaissance italienne.

## Référence

## Liens 