[[Page Wiki]]
MOC : [[Histoire]] [[POLITIQUE]]
Titre : [[Notes/Gouvernement Charles de Gaulle (3)]]
Date : 2022-07-05
Lien : https://fr.wikipedia.org/wiki/Gouvernement_Charles_de_Gaulle_(3)
***

## Résumé 
Le troisième gouvernement Charles de Gaulle, sous la présidence de René Coty, est le dernier gouvernement de la Quatrième République et le premier gouvernement de la Cinquième République.

## Notions clés

## Références
[[Histoire de la France au XXe Siècle - Livre 3]]