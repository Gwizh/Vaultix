MOC : 
Titre : [[Un Monde Plus Sale Que Moi]]
Date : 2023-08-31
Autrice : [[Capucine Delattre]]
***

## Résumé
C'est un livre qui ose parler des choses comme elles sont. C'est un livre d'une précision impressionnante.
Ça évoque ce qu'est se mettre en couple pour des gens nés en l'an 2000 (et autour) et de manière plus importante : être une femme et découvrir la sexualité a ce moment-là. 

### Éléments importants
- La question des souvenirs, il nous faut les garder mais comment garder les bons et comment les garder intègres ?
- Le mot viol n'arrive que bien plus tard, émergeant petit a petit a travers les souvenirs.
- Etre un homme et a quel point c'est facile. C'est aussi être quelqu'un qui n'a pas à réfléchir ou à se prendre la tête. La fille ne pense qu'a toute cette histoire ça l'empêche de faire sa vie. On n'apprend pas gd chose du reste. Le mec lui peut suivre sa vie sans se soucier de rien. 
- Apprendre grace a internet.
- Victor est un homme qui a fait ça car il a pu, il se rendait pas forcément compte mais il a quand même pris avantage. Toutes ces souffrances viennet de ce décalage aberrant. Il est coupable de n'avoir pas fait attention au mal qu'il causait. 
- Elsa déteste et aiment les personnes pour ce qu'elles disent ou oublient de dire. c'est jamais facile et ça change tout le temps. 
- Ça impacte Victor aussi évidemment, il a appris après coup. 

### Passages marquants
- Premiers chapitres : le fait de se mettre en couple parce qu'on a que ça a faire.
- p27 : Les règles. Il me faut de telles piqures de rappel. C'est puissant, c'est fort, j'étais mal a l'aise et c'est tout ce que je voulais... Puis l'anorexie c'est vraiment fou a quel point je m'en rendais pas compte. MERCI
- p48 : Le viol. Le mot n'est pas indiqué ici bien sur mais c'est cohérent. Mais aussi le fait qu'elle ait tout fait pour le couple. Victor n'a rien fait du tout. Pilule, stérilet et tout ce que ça implique de stress. C'est le chapitre le plus important.
- p85 : Le fait qu'il y ait une asymétrie dans la perception des deux personnes. Le gars est perçu comme un avenir plus tracé, avec plus de valeur. HAHA
"Sortir avec moi, c'est un sacré changement de statut pour toi, dis donc."
- p97 : Jouir.
- p126 : La soirée. C'est SI vrai omg. L'alcool
- p135.
- p138 : Le sang. C'est horrible. Les descriptions font mal. La question du plaisir et des autres couples. 
- p145 : Les consignes. C'est horrible à quel point c'est réaliste. 
- p189 : super important la pseudo rédemption de Victor. 
- puis discussion avec les autres. Sa mère, ses amies. Les discussions sont chargées de passions. 

## Références

## Liens 