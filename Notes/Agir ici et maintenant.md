MOC : 
Source : [[Agir ici et maintenant]]
Date : 2023-06-16
***

Résumé de la pensée de [[Murray Bookchin]] par [[Floréal M. Romero]]

## Résumé
#### Préface
On connaît aujourd'hui l'apparition de nombreux courants politiques différents qui nous permettent d'avoir une intelligence collective. Elle nous permet de voir la frontière entre volonté d'intégrité théorique et dogmatisme inflexible. 

> Dans le système patriarcal, tous les êtres dominés sont assimilés à la nature et tout ce qui se rapporte à la nature se dote de caractéristiques féminines.

Une solution distincte de l'Etat moderne, bureaucratique de [[Max Weber]]

Refus essentialiste du pouvoir, du dogmatisme, de l'individualisme.

##### Transition de l'[[Espagne franquiste]] à la démocratie
Il décrit que cette transition avait été déjà discutée avant la fin du régime et que ce nouveau régime était plus un moyen de recycler les franquistes. Il n'y a eu aucun procès, aucune sanction. L'Espagne s'est ouverte à l'Europe de façon pacifique. Les partis de gauche ont accepté ça alors qu'en même temps une répression terrible s'abattait sur les travailleurs et les syndicats. Il y avait aussi l'[[ETA]]. Ce passage à la société de consommation fut accueilli avec beaucoup de soulagement, assez pour oublier en tout cas. 

##### Autres mouvements espagnols
[[Podemos (parti espagnol)]], [[Mouvement des Indignés]], [[Conseil ouvert]], [[Fueros]], [[Auzolanes]]

#### Génèse et actualité de la pensée de Murray Bookchin
##### I- Sa vie, sa lutte
Elevé par sa grand-mère, lui parlant de [[Vladimir Lénine]], [[Rosa Luxemburg]], [[Karl Liebknecht]], [[Léon Trotsky]]. 

###### 1- Militant du parti communiste
![[Parti communiste des États-Unis d'Amérique#^c2285c]]
Bookchin était choqué par cette vision très stricte des choses.

À 13 ans, il fait preuve d'une bonne force de persuasion qui le permet de convaincre des socialistes. Pour les questions les plus pointues, il se sert de réponse préconçues (il parle de la [[Révolte de Kronstadt]]). Mais pendant la [[Guerre civile espagnole]], il se verra trahi par le parti qui s'alliance avec la gauche pour participer au [[New Deal]].

###### 2- [[Trotskisme]]
Ce rapprochement est mal vu par le parti communiste qui le conseille d'aller voir un psychiatre pour savoir ce qui ne va pas. Soutient les anarchistes espagnols qu'il voit comme des héros qui ose s'opposer au communisme Stalinien. Il s'engage dans les mouvements trotskyste de l'époque qui étaient les seuls à tolérer les anarchistes. Il va travailler dans des industries difficiles et soutient les travailleurs.

###### 3- Dissidence : désillusion et évolution
Il entre dans les mouvements dissidents de ses mouvements, qu'il n'a jamais suivi réellement. Il rentre dans les syndicats mais voit que les travailleurs et les prolétaires peuvent s'acheter et ne sont pas aussi révolutionnaires que dit le [[Marxisme]]. Il s'éloigne de cette théorie qu'il voit comme un mensonge.

###### 4- L'ultime dissidence
"Contrairement à [[Herbert Marcuse]], [[Max Horkheimer]] et [[Theodor Adorno]], Bookchin ne considère pas la naissance de la raison instrumentale comme une conséquence de la domination de la nature par l'homme, tel que Marx l'avait pensé auparavant. Pour Bookchin, c'est la domination de l'homme par l'homme, en commençant par la femme, qui détermine le besoin de dominer et d'exploiter la nature pour implanter et maintenir une hiérarchie de pouvoirs."

Il critique le [[Libéralisme]] qui a selon lui changé la philosophie des [[Lumières]] en une rationalisation technocratique.

###### 5- Les potentialités de la raison
Il veut combattre la commercialisation dévastatrice et la bureaucratie froide. Il souhaite la démocratie réel et le face à face pour l'opposer contre le spectacle électoral. Il souhaite un socialisme éthique et non scientifique comme le voulait Marx. Il se rapproche alors de l'anarchisme social et de la [[CNT]]. Il s'ouvre alors en particulier à [[Pierre Kropotkine]].

###### 6- L'éveil de l'écologie
Dès 1952, il participe à des études pour montrer les dangers de l'agriculture sur la nature.
Contre le nucléaire.

## Référence

## Liens 