MOC : 
Source : [[Sextape et Saint-Étienne]]
Date : 2023-04-26
***

## Résumé
-   Le 26 août 2022, Mediapart révélait l’existence d’un chantage à la _sextape_ contre Gilles Artigues, ancien député et premier adjoint de la mairie de Saint-Étienne.
-   En janvier 2015, ce père de famille catholique a été filmé à son insu avec un _escort_ dans une chambre d’hôtel par un autre membre de la majorité municipale.
-   Sur fond de rivalités politiques et de haines recuites, [[Gilles Artigues]] a ensuite fait l’objet pendant plusieurs années de menaces, notamment de la part du maire de [[Saint-Étienne]] Gaël Perdriau et de son directeur de cabinet. L’élu centriste, qui a songé à se suicider, n’avait jamais parlé de ces pressions, pas même à sa famille.
-   Une enquête judiciaire, ouverte en août 2022, a conduit à la mise en examen des protagonistes de cette affaire, dont [[Gaël Perdriau]], le 6 avril 2023.
-   Dans une série en quatre volets, Mediapart révèle les nouveaux développements de cette affaire.

Huit mois après la révélation de l’affaire par Mediapart, qui a entraîné le dépôt d’une plainte par Gilles Artigues et l’ouverture d’une enquête judiciaire, nous avons décidé de raconter les avancées des investigations dans une enquête en plusieurs volets.

-   Le maire de Saint-Étienne **Gaël Perdriau** a été mis en examen le 6 avril du chef de « chantage ». S’agissant des faits de « détournement de fonds publics par un dépositaire de l’autorité publique », il a été entendu en qualité de témoin assisté. Sollicité par le biais de ses avocats, Me Christophe Ingrain et Me Jean-Félix Luciani, Gaël Perdriau n’a pas souhaité répondre à nos questions. _« Notre client conteste les accusations portées à son encontre, mais le respect dû à la justice ne lui permet pas de s’exprimer publiquement sur un dossier couvert par le secret de l’instruction._ _Il souhaite que le travail des magistrats se poursuive dans la plus grande sérénité »_, nous ont affirmé ses conseils.  
      
    
-   L’ex-directeur de cabinet **Pierre Gauttieri** – licencié après le début de l’affaire – a été mis en examen des chefs de « chantage avec mise à exécution de la menace », « recel de bien obtenu à l’aide d’un détournement de fonds », « complicité de soustraction », « détournement de fonds publics par un dépositaire de l’autorité publique », « utilisation, conservation ou divulgation d’un document ou enregistrement portant sur des paroles ou images à caractère sexuel et obtenu par une atteinte à l’intimité de la vie privée ». Son avocate, Me Ilie Negrutiu, n’a pas retourné notre demande d’entretien.  
      
    
-   L’ancien adjoint à l’éducation **[[Samy Kefi-Jérôme]]** – ayant démissionné après le début de l’affaire – a été mis en examen des chefs de « chantage avec mise à exécution de la menace », « recel de bien obtenu à l’aide d’un détournement de fonds », « atteinte à l’intimité de la vie privée par fixation, enregistrement ou transmission de l’image d’une personne présentant un caractère sexuel », « utilisation, conservation ou divulgation d’un document ou enregistrement portant sur des paroles ou images à caractère sexuel et obtenu par une atteinte à l’intimité de la vie privée ». Ses avocats, Me Sofia Bougrine et Mathias Chichportich ont indiqué que leur client _« s’est toujours refusé à commenter publiquement le fond de cette affaire »_ : _« Il a donné sa vérité à la justice et s’est notamment longuement expliqué sur le contexte personnel dans lequel s’inscrivent les enregistrements sur lesquels il apparaît. Il ne fera aucun autre commentaire afin de laisser travailler les juges dans la sérénité. »  
      
    _
-   **[[Gilles Rossary-Lenglet]]** a été mis en examen des chefs de « complicité de chantage avec mise à exécution de la menace », « recel de bien obtenu à l’aide d’un détournement de fonds », « utilisation, conservation ou divulgation d’un document ou enregistrement portant sur des paroles ou images à caractère sexuel et obtenu par une atteinte à l’intimité de la vie privée ». Auprès de Mediapart, son avocat, Me Sylvain Cormier, a indiqué que son client, resté constant dans ses déclarations depuis le début de l’affaire, a livré tous les éléments _« à la justice pour faire triompher la vérité »_. 

Toutes les personnes mises en examen bénéficient de la présomption d’innocence.

## Référence

## Liens 