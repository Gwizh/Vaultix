MOC : 
Source : [[Colloque Walter Lippmann]]
Date : 2023-04-14
***

## Résumé
Le colloque Walter Lippmann est un rassemblement de 26 économistes et intellectuels libéraux organisé à Paris du 26 au 30 août 1938. S’il y fut discuté de la capacité du libéralisme à faire face aux problèmes de l’époque, c’est aussi une des premières fois où les participants s’interrogèrent pour savoir s’il convenait de conserver le mot « libéralisme » ou bien d’adopter celui de « [[Néolibéralisme]] ». Pour François Bilger, le colloque [[Walter Lippmann]] « peut être considéré comme l’acte de naissance officiel du nouveau libéralisme ».

## Référence

## Liens 