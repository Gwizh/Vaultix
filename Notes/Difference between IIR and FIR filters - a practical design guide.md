A digital filter is a mathematical algorithm that operates on a digital dataset (e.g. sensor data) in order extract information of interest and remove any unwanted information. Applications of this type of technology, include removing glitches from sensor data or even cleaning up noise on a measured signal for easier data analysis. But how do we choose the best type of digital filter for our application? And what are the differences between an IIR filter and an FIR filter?

Digital filters are divided into the following two categories:

-   Infinite impulse response (IIR)
-   Finite impulse response (FIR)

As the names suggest, **each type of filter is categorised by the length of its impulse response**. However, before beginning with a detailed mathematical analysis, it is prudent to appreciate the differences in performance and characteristics of each type of filter.

### **Example**

In order to illustrate the differences between an IIR and FIR, the frequency response of a 14th order FIR (solid line), and a 4th order [Chebyshev Type](https://www.advsolned.com/iir-filters-a-practical-guide/) I IIR (dashed line) is shown below in Figure 1.  Notice that although the magnitude spectra have a similar degree of attenuation, the phase spectrum of the IIR filter is non-linear in the passband (0→7.5Hz), and becomes very non-linear at the cut-off frequency, fc=7.5Hz. Also notice that the FIR requires a higher number of coefficients (15 vs the IIR’s 10) to match the attenuation characteristics of the IIR.

![FIR vs IIR: frequency response of a 14th order FIR (solid line), and a 4th order Chebyshev Type I IIR (dashed line); Fir Filter, IIR Filter](https://www.advsolned.com/wp-content/uploads/2020/04/fir_iir.png)

_Figure 1:_ _FIR vs IIR: frequency response of a 14th order FIR (solid line), and a 4th order Chebyshev Type I IIR (dashed line)_

These are just some of the differences between the two types of filters. A detailed summary of the main advantages and disadvantages of each type of filter will now follow.

## **IIR filters**

IIR (infinite impulse response) filters are generally chosen for applications where linear phase is not too important and memory is limited. They have been widely deployed in audio equalisation, biomedical sensor signal processing, IoT/IIoT smart sensors and high-speed telecommunication/RF applications.

**Advantages**

-   **Low implementation cost**: requires less coefficients and memory than FIR filters in order to satisfy a similar set of specifications, i.e., cut-off frequency and stopband attenuation.
-   **Low latency**: suitable for real-time control and very high-speed RF applications by virtue of the low number of coefficients.
-   **Analog equivalent**: May be used for mimicking the characteristics of analog filters using s-z plane mapping transforms.

**Disadvantages**

-   **Non-linear phase characteristics**: The phase charactersitics of an IIR filter are generally nonlinear, especially near the cut-off frequencies. [All-pass equalisation filters](https://www.advsolned.com/linear-phase-iir-filters-analysis-and-design/) can be used in order to improve the passband phase characteristics.
-   **More detailed analysis:** Requires more scaling and numeric overflow analysis when implemented in fixed point. The **Direct form II** filter structure is especially sensitive to the effects of quantisation, and requires special care during the design phase.
-   **Numerical stability**: Less numerically stable than their FIR (finite impulse response) counterparts, due to the feedback paths.

## **FIR filters**

FIR (finite impulse response) filters are generally chosen for applications where linear phase is important and a decent amount of memory and computational performance are available. They have a widely deployed in audio and biomedical signal enhancement applications. Their all-zero structure (discussed below) ensures that they never become unstable for any type of input signal, which gives them a distinct advantage over the IIR.

**Advantages**

-   **Linear phase**: FIRs can be easily designed to have linear phase. This means that no phase distortion is introduced into the signal to be filtered, as all frequencies are shifted in time by the same amount – thus maintaining their relative harmonic relationships (i.e. constant group and phase delay). This is certainly not case with IIR filters, that have a non-linear phase characteristic.   
-   **Stability**: As FIRs do not use previous output values to compute their present output, i.e. they have no feedback, they can never become unstable for any type of input signal, which is gives them a distinct advantage over IIR filters.
-   **Arbitrary frequency response**: The Parks-McClellan and ASN FilterScript’s [firarb()](https://www.advsolned.com/asn-filterscript-reference/#toggle-id-16) function allow for the design of an FIR with an arbitrary magnitude response. This means that an FIR can be customised more easily than an IIR.
-   **Fixed point performance**: the effects of quantisation are less severe than that of an IIR.

**Disadvantages**

-   **High computational and memory requirement:** FIRs usually require many more coefficients for achieving a sharp cut-off than their IIR counterparts. The consequence of this is that they require much more memory and significantly a higher amount of MAC (multiple and accumulate) operations. However, modern microcontroller architectures based on the Arm’s Cortex-M cores now include DSP hardware support via SIMD (signal instruction, multiple data) that expedite the filtering operation significantly.
-   **Higher latency**: the higher number of coefficients, means that in general a linear phase FIR is less suitable than an IIR for fast high throughput applications. This becomes problematic for real-time closed-loop control applications, where a linear phase FIR filter may have too much group delay to achieve loop stability.
-   **Minimum phase filters**: A solution to ovecome the inherent N/2 latency (group delay) in a linear filter is to use a so-called **minimum phase filter,** whereby any zeros outside of the unit circle are moved to their conjugate reciprocal locations inside the unit circle. The result of the zero flipping operation is that the magnitude spectrum will be identical to the original filter, and the phase will be nonlinear, but most importantly the **latency will be reduced from N/2 to something much smaller** (although non-constant), making it suitable for real-time control applications.  
          For applications where phase is less important, this may sound ideal, but the difficulty arises in the numerical accuracy of the root-finding algorithm when dealing with large polynomials. Therefore, orders of **50 or 60 should be considered a maximum** when using this approach. Although other methods do exist (e.g. the Complex Cepstrum), transforming higher-order linear phase FIRs to their minimum phase cousins remains a challenging task.
-   **No analog equivalent**: using the [Bilinear](https://www.advsolned.com/asn-filterscript-reference/#toggle-id-22), [matched z-transform](https://www.advsolned.com/asn-filterscript-reference/#toggle-id-23) (s-z mapping), an analog filter can be easily be transformed into an equivalent IIR filter.  However, this is not possible for an FIR as it has no analog equivalent.