MOC : 
Source : [[Gouvernement Jacques Chirac (2)]]
Date : 2023-04-14
***

## Résumé
Le pari du gouvernement Chirac était, grâce à la politique d’[[Édouard Balladur]], d’obtenir un redressement significatif de l’économie française au prix d’une logique libérale supposée résoudre tous les problèmes du pays : le rôle de l’État dans l’économie doit être diminué par une réduction des dépenses publiques, permettant de mettre fin au déficit budgétaire, de diminuer la pression fiscale, d’alléger la dette. Il en résultera une baisse des taux d’intérêt permettant aux entreprises d’emprunter pour investir, relançant ainsi la [[Croissance économique]]. La rupture apparaît ainsi davantage dans les intentions proclamées haut et fort que dans des pratiques dont la plupart ont été mises en œuvre en 1984 par le gouvernement Fabius.

## Référence
[[Libéralisme]]

## Liens 