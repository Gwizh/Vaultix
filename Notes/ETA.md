MOC : 
Source : [[ETA]]
Date : 2023-06-17
***

## Résumé
Euskadi ta Askatasuna

Euskadi ta Askatasuna5, plus connu sous son acronyme ETA (pour « Pays basque et liberté » en basque), est une organisation basque indépendantiste d'inspiration marxiste-léniniste active du 31 juillet 1959 au 2 mai 2018 (officiellement). Plusieurs organisations ont porté ce nom depuis la création de la première ETA en raison de plusieurs scissions.

Inspiration [[Marxisme|marxiste]] révolutionnaire qui a évolué d'un groupe résistant à l'[[Espagne franquiste]], admiré et populaire à un groupe révolutionnaire qui continuait de perpétuer des attentats sans grande cohérence.

## Référence

## Liens 