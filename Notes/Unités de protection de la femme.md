MOC : 
Source : [[Unités de protection de la femme]]
Date : 2023-06-09
***

## Résumé
Les Unités de protection de la femme ou Unités de défense de la femme (en kurde : Yekîneyên Parastina Jin, YPJ) est une organisation militaire kurde composée exclusivement de femmes.

Les YPJ furent mises en place en 2013 à titre de brigade féminine des milices des Unités de protection du peuple (Yekîneyên Parastina Gel, YPG) et est devenue indépendante en 20161. Les YPJ et YPG sont l'aile armée d'une coalition kurde qui a pris le contrôle de facto sur l'essentiel du Nord de la Syrie à prédominance kurde dénommé [[Rojava]].

## Référence

## Liens 