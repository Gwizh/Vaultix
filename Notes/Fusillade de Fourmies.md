MOC : 
Source : [[Fusillade de Fourmies]]
Date : 2023-07-07
***

## Résumé
La fusillade de Fourmies est un événement qui s'est déroulé le 1er mai 1891 à Fourmies (Nord). Ce jour-là, la troupe met fin dans le sang à une manifestation qui se voulait festive pour revendiquer la journée de huit heures. Le bilan est de neuf morts, dont deux enfants, et de trente-cinq blessés. Bien que les forces de l’ordre aient été mises en cause, neuf manifestants furent condamnés pour entrave à la liberté de travail, outrage et violence à agent et rébellion, à des peines de prison de deux à quatre mois ferme.

## Référence

## Liens 
[[XIXe siècle]]
[[Police]]
[[Armée]]