[[Article]]
MOC : [[Politique]]
Titre : [[Politique migratoire - le bras de fer se poursuit entre Budapest et Bruxelles]]
Auteur : [[L'Opinion]]
Date : 2018-07-29
Lien : https://www.lopinion.fr/international/politique-migratoire-le-bras-de-fer-se-poursuit-entre-budapest-et-bruxelles
Fichier : 
***

## Résumé
La Commission européenne a décidé mi-juillet de former un recours contre la Hongrie devant la Cour de justice de l’UE pour non-conformité de sa législation en matière d’asile et de retour avec le droit de l’Union. Elle a également ouvert une procédure d’infraction contre la loi dite « Stop Soros » qui criminalise le soutien à l’immigration illégale, notamment par des ONG. 

Viktor Orban est une nouvelle fois furieux contre Bruxelles. «Nous avons besoin d’une nouvelle Commission, d’une nouvelle approche», a-t-il déclaré vendredi. Une intervention qui fait suite à la décision prise quelques jours plus tôt par l’exécutif européen de saisir la Cour de justice de l’UE contre la législation hongroise de décembre 2015 en matière d’asile et de retour, jugée contraire à la législation européenne sur plusieurs points.

Bruxelles estime que la Hongrie n’assure pas un accès effectif aux procédures d’asile, puisque les migrants en situation irrégulière sont raccompagnés au-delà des frontières même s’ils souhaitent introduire une demande d’asile, et que ces procédures de retour se font sans les garanties appropriées, en violation du principe de non-refoulement. Un principe qui interdit l’extradition, l’expulsion ou le renvoi d’une personne dans un autre pays s’il y a des raisons sérieuses de penser que la personne concernée y encourt des risques sérieux de torture, de traitements inhumains ou de toute autre forme de violation sévère des droits humains.

Les autorités hongroises disposent désormais de deux mois pour répondre aux préoccupations exprimées par la Commission dans la lettre de mise en demeure, sous peine de recevoir un avis motivé susceptible de conduite à une nouvelle saisine de la Cour de justice. Une issue très probable, Viktor Orban ayant déclaré que la décision était sans valeur et que Bruxelles ferait mieux de punir les Etats qui laissent entrer en Europe des millions de migrants en violation des règles communautaires. Cette attaque vise directement Angela Merkel. En 2015, au plus fort de la crise migratoire, la chancelière allemande, aujourd’hui contestée par l’aile droite de sa majorité, avait décidé d’ouvrir les frontières de l’Allemagne aux réfugiés.

## Référence
[[Hongrie]]
[[Viktor Orban]]
[[Union Européenne]]
[[Migration]]

## Liens 