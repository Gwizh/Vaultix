MOC : 
Titre : [[Carnet anthropométrique]]
Date : 2023-09-26
***

## Résumé
Le Carnet anthropométrique était un document administratif français obligatoire permettant d'identifier et surveiller les déplacements des nomades sur le territoire français. Institué en 1912, il est remplacé en 1969 par le livret de circulation1. 


Instauré par la loi du 16 juillet 1912 sur l'exercice des professions ambulantes et la circulation des nomades[2](https://fr.wikipedia.org/wiki/Carnet_anthropom%C3%A9trique#cite_note-:0-2), ce carnet était obligatoire pour tous les nomades âgés de plus de 13 ans et devait consigner tous les déplacements, rendant possible une étroite surveillance de ces populations[1](https://fr.wikipedia.org/wiki/Carnet_anthropom%C3%A9trique#cite_note-ldh_toulon-1). Les carnets individuels devaient être signés dans chaque lieu d'arrêt des nomades, quotidiennement, par les agents locaux[3](https://fr.wikipedia.org/wiki/Carnet_anthropom%C3%A9trique#cite_note-:1-3) (maires, adjoints, et plus rarement instituteurs et gardes champêtres[4](https://fr.wikipedia.org/wiki/Carnet_anthropom%C3%A9trique#cite_note-4)). En parallèle, les groupes de nomades possédaient des carnets collectifs contenant les noms de tous les individus, dont les enfants trop jeunes pour être porteurs du dispositif individuel[3](https://fr.wikipedia.org/wiki/Carnet_anthropom%C3%A9trique#cite_note-:1-3).

Il contenait plusieurs informations [anthropométriques](https://fr.wikipedia.org/wiki/Anthropom%C3%A9trie "Anthropométrie"), ainsi que les empreintes digitales et des [photos d'identité](https://fr.wikipedia.org/wiki/Photographie_d%27identit%C3%A9 "Photographie d'identité") de profil et de face[1](https://fr.wikipedia.org/wiki/Carnet_anthropom%C3%A9trique#cite_note-ldh_toulon-1),[2](https://fr.wikipedia.org/wiki/Carnet_anthropom%C3%A9trique#cite_note-:0-2). Ce processus de consignation s'inscrit dans la logique des évolutions des techniques de surveillance, dans le sillon du développement du [Bertillonnage](https://fr.wikipedia.org/wiki/Bertillonnage "Bertillonnage")[5](https://fr.wikipedia.org/wiki/Carnet_anthropom%C3%A9trique#cite_note-5).

Le 3 janvier 1969 la loi est abrogée, les nomades sont maintenant appelés [gens du voyage](https://fr.wikipedia.org/wiki/Gens_du_voyage "Gens du voyage") et le carnet est remplacé par le [livret de circulation](https://fr.wikipedia.org/wiki/Livret_de_circulation_(France) "Livret de circulation (France)")[1](https://fr.wikipedia.org/wiki/Carnet_anthropom%C3%A9trique#cite_note-ldh_toulon-1). La suppression de ce dernier est votée par l'[Assemblée nationale](https://fr.wikipedia.org/wiki/Assembl%C3%A9e_nationale_(France) "Assemblée nationale (France)") en [2015](https://fr.wikipedia.org/wiki/2015 "2015")[6](https://fr.wikipedia.org/wiki/Carnet_anthropom%C3%A9trique#cite_note-6).
## Références

## Liens 