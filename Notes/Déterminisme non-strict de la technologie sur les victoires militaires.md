MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081614
Tags : #Fini
***

# Déterminisme non-strict de la technologie sur les victoires militaires
Des guerres ont été gagnés même lorsqu'il y avait un immense déséquilibre technologique. Les exemples les plus évidents sont l'Allemagne nazie contre le reste de l'Europe durant la seconde guerre mondiale, le Japon contre la Malaisie puis l'URSS contre l'Allemagne. De loin l'exemple le plus parlant est le cas du Vietnam contre les États-Unis. Sur ce coup, le déterminisme technologique n'est pas aussi puissant qu'on pourrait le penser.

## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]