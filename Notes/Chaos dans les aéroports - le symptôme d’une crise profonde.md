[[Article]]
MOC : [[MOC/ECONOMIE]]
Titre : [[Chaos dans les aéroports - le symptôme d’une crise profonde]]
Auteur : [[Mediapart]]
Date : 2022-07-05
Lien : https://www.mediapart.fr/journal/economie/050722/chaos-dans-les-aeroports-le-symptome-d-une-crise-profonde
***

## Résumé
#### Situation actuelle
- Alors que les salariés d’Aéroports de Paris entrent en grève, les délais et les annulations dans les aéroports devraient accompagner les voyageurs cet été. Une situation qui est le fruit des impasses d’un modèle dans lequel ce secteur s’est enfermé.
- Aucun pays n’est réellement épargné par ce phénomène, mais les problèmes se concentrent surtout en Europe et en Amérique du Nord. Aux États-Unis, près de 2 500 vols ont été supprimés pendant la semaine précédant la fête nationale, et le 17 juin, c’est même le vol du secrétaire d’État aux transports, Pete Buttigieg, qui a été annulé. Le nombre de vols annulés aux États-Unis en juin est proche de 3 %, contre 2,1 % en 2019.
- En Europe, les annulations sont moins nombreuses (2 %), mais elles ont doublé par rapport à 2019 et, en juin, leur nombre a atteint 8 228, soit 162 % de plus qu’il y a trois ans, selon les données du bureau d’analyse de données aériennes Cirium.
- Ce n’est donc pas la hausse de la demande qui explique le chaos, mais bien l’incapacité de l’offre à faire face, et donc de très mauvais choix réalisés pendant la pandémie. En 2020 et 2021, la pandémie a été l’occasion pour les acteurs du système de réduire les effectifs. En théorie pour _« adapter les coûts »_ à la nouvelle demande. Ce sont ces décisions qui ont créé les dysfonctionnements que l’on observe aujourd’hui et qui causent des défaillances en série qui, à leur tour, provoquent le chaos.

#### Les contradictions du secteur

- La véritable source du chaos, c’est la situation du secteur aérien lui-même. Et pour le comprendre, il faut revenir à la source de tous les maux : la libéralisation dudit secteur dans les années 1990-2000. Cette libéralisation n’arrive pas au hasard si on se place dans une perspective plus large. Dans l’économie occidentale d’alors, soumise aux pressions de la désinflation, du ralentissement structurel de la croissance et des délocalisations, tous les gisements de croissance doivent être exploités.
- Dans ce cadre, le secteur aérien apparaît comme idéal : largement dominé par des acteurs étatiques et fortement régulé, il y a là moyen de gagner beaucoup d’argent. On libéralise donc le secteur par une concurrence à bas coût et des ouvertures de lignes. Cette croissance fonctionne dans un premier temps et favorise d’autres secteurs : le tourisme, la construction, les services aux entreprises. Mais, comme d’autres secteurs de services, l’affaire se corse au bout de quelques décennies.
- Pour gagner des marges, les plus grandes entreprises ont transféré leurs activités les moins rentables à des fournisseurs qui eux-mêmes sont sous forte pression. À Roissy, par exemple, il existe ainsi près de huit cents entreprises pour réaliser les activités aéroportuaires. En tant que sous-traitantes, ces entreprises ne récupèrent qu’une faible part de la [[Croissance économique]] globale de l’activité, ont peu de capacité d’imposer leurs prix et n’ont guère de possibilité de gagner de la productivité.
- C’est un fait que les syndicalistes soulignent beaucoup : _« Les conditions de travail se sont tellement dégradées que le secteur n’est plus attirant »_.
- > Le ministre de l’économie, Bruno Le Maire, a ainsi accepté de verser 4 milliards d’euros à [[Air France]] « au nom de l’emploi », mais sans autre contrainte qu’une amélioration de sa rentabilité. L’occasion pour la compagnie de réduire ses effectifs de 7 500 personnes, ce qu’elle a annoncé en juillet 2020.
- Le chaos d’aujourd’hui peut alors se comprendre comme le résultat de l’impasse de cette logique.

## Référence
[[Libéralisation]]

## Liens 