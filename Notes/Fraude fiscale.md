MOC : 
Source : [[Fraude fiscale]]
Date : 2023-04-19
***

## Résumé
Rapport d'avril 2020

#### La France, eldorado fiscal pour les milliardaires
https://www.mediapart.fr/journal/economie-et-social/060623/la-france-eldorado-fiscal-pour-les-milliardaires?utm_source=global&utm_medium=social&utm_campaign=SharingApp&xtor=CS3-5

Contrairement au commun des mortels, les milliardaires français ne paient quasiment pas d’impôts sur le revenu ni de prélèvements sociaux, souligne une étude de l’Institut des politiques publiques publiée mardi. Par exemple, les 75 foyers les plus riches ne paient que 0,3 % au regard de ce qu’ils gagnent réellement. 

L’Institut des politiques publiques (IPP) a publié, mardi 6 juin, une étude montrant que les [[Ultrariche]] en France ne payaient que très peu d’impôts sur le revenu (IR) en proportion de leurs revenus globaux. Un contraste saisissant en ce jour de manifestation contre la réforme des retraites, qui montre bien qu’il demeure en France un traitement injustement différencié entre, d’un côté, le peuple à qui l’on demande sans cesse des efforts supplémentaires, et de l’autre une caste de privilégiés à qui l’on permet d’échapper à l’impôt. 

Pour nourrir leurs recherches, les quatre chercheurs de l’IPP auteurs de la note – Laurent Bach, Antoine Bozio, Arthur Guillouzouic et Clément Malgouyres – expliquent avoir mobilisé « des données d’une rare qualité » provenant de la Direction générale des finances publiques (DGFiP) et du Centre d’accès sécurisé aux données (CASD).

Ils ont ainsi pu recueillir, tout en respectant l’anonymat des riches contribuables, des données datant de 2016 sur leurs déclarations d’impôt sur le revenu, d’impôt sur la fortune et d’impôt sur les sociétés. Le tout recoupé avec des éléments concernant les actionnaires de référence de chaque entreprise.

Première révélation de l’IPP : les revenus fiscaux déclarés par les milliardaires sont bien inférieurs aux revenus réellement à leur disposition, car ils ne prennent pas en compte les revenus logés dans des sociétés holding qu’ils contrôlent. 

En considérant tous ces revenus, les chercheurs de l’IPP sont arrivés à la conclusion que le « revenu économique » global des 378 foyers les plus riches s’établissait à 171,8 millions en moyenne, soit 14 fois plus que leur revenu fiscal moyen. Pour les 75 foyers les plus riches, c’est pire : leur revenu fiscal moyen était de 35,9 millions d’euros en 2016, alors que leur revenu économique global dépassait le milliard d’euros en moyenne. 

Les chercheurs de l’IPP ont ensuite croisé ces chiffres réactualisés avec le montant d’impôt sur le revenu dont les ultrariches se sont effectivement acquittés en 2016. En termes de taux d’imposition, les conclusions sont accablantes : la note de l’IPP révèle que sur l’année 2016, les 378 foyers les plus riches en France ne payaient effectivement que… 2 % d’IR et de prélèvements sociaux en proportion de leurs revenus économiques globaux.

Pis, plus les milliardaires sont riches, plus les taux d’imposition de leurs revenus personnels tendent vers zéro : dans les annexes de la note l’IPP, on constate ainsi que les 75 foyers les plus riches ne payaient que 0,3 % d’IR et de prélèvements sociaux en proportion de leurs revenus économiques.

Pour résumer, l’impôt sur le revenu, l’instrument fiscal le plus progressif en France – et donc le plus juste — échoue dramatiquement à taxer les milliardaires. L’article 13 de la Déclaration des droits de l’homme et du citoyen de 1789, qui définit un principe de répartition de la charge fiscale « également répartie entre tous les citoyens, en raison de leurs facultés », est donc dévoyé. 

Comment cela s’explique-t-il ? Trivialement : par l’optimisation fiscale. En effet, plutôt que de se payer en salaire, les ultrariches préfèrent créer des holdings dans lesquels ils remontent sous forme de dividendes les bénéfices générés par les entreprises qu’ils possèdent.

Ces bénéfices sont d’abord taxés au niveau de la filiale à l’impôt sur les sociétés – 25 % aujourd’hui et 33 % en 2016 au moment de l’étude de l’IPP. Mais grâce à la directive européenne dite « mère-fille » qui empêche la taxation d’une société sur les dividendes issus d’une filiale, ces bénéfices ne sont plus taxés une fois remontés à la holding. Ils restent au chaud sans que le fisc ne puisse s’y attaquer. 

Reste ensuite aux milliardaires – toujours entourés de conseillers fiscaux ultra-compétents – à utiliser les niches fiscales à leur disposition pour réduire à peau de chagrin le taux d’imposition lorsqu’ils souhaiteront débloquer tout cet argent remonté dans les holdings.

Les chercheurs de l’IPP ont détecté trois niches très utilisées par les ultra-riches pour se soustraire à la solidarité nationale : l’exonération des plus-values en cas de cession professionnelle ; la sous-imposition des plus-values latentes lors d’un transfert de domicile fiscal hors de France – un dispositif aussi appelé « exit tax » ; ainsi que les abattements permis par le pacte Dutreil lors des donations. 



## Référence

## Liens 
[[Cash Investigation - Paradis Fiscaux]]


