MOC : 
Source : [[Adoption au Japon]]
Date : 2023-04-13
***

## Résumé
On peut adopter des adultes au [[Japon]].

Ça joue pour l'héritage, seul un homme peut hériter donc si une famille veut léguer leur biens, il faut adopter un gars qui veut changer de famille. 

C'est le seul moyen pour les couples homosexuels d'être de la même famille, tu adoptes ton mec et tu sors avec ton fils. 
Ça concerne 80 000 personnes

## Référence 
[[Heritage]]
[[Homosexualité]]

## Liens 