MOC : 
Source : [[Affaire du 8 décembre 2020]]
Date : 2023-06-08
***

## Résumé
L'affaire du 8 décembre 2020 est une affaire controversée de peines de détention préventive, dont une peine préventive de seize mois à l'isolement, et d'intentions terroristes supposées sans qu'aucun acte terroriste ni projet d'acte terroriste ne soit reproché aux prévenus.

Sept personnes seront jugées par la 16e chambre du tribunal judiciaire de Paris en octobre 20231.

L'affaire commence en 2018 avec la surveillance par la Direction générale du renseignement intérieur (DGSI) de personnes revenues du [[Rojava]] (Syrie) où elles sont parties combattre Daesh aux côtés des forces kurdes du YPG.

L'affaire se fait surtout connaître à travers le traitement réservé à l'un d'entre eux, Florian D., qui prend le nom de Libre Flot une fois en prison et qui est maintenu seize mois à l'isolement en détention provisoire. Il est remis en liberté sous bracelet électronique le 7 avril 2022 après avoir mené une grève de la faim.

## Référence

## Liens 