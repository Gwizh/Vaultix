MOC : 
Source : [[Marcel Deprez]]
Date : 2023-08-09
***
#Adam 
## Résumé
Marcel Deprez, né le 12 décembre 1843 à Aillant-sur-Milleron (Loiret) et mort le 13 octobre 1918 à Vincennes, est un ingénieur français ayant essentiellement travaillé sur l'électricité. De 1876 à 1886, Deprez mène les premiers essais de transport d'électricité sur de longues distances à Creil. À l'[[Exposition internationale d'Électricité de Paris en 1881]], il présente pour la première fois une installation de distribution d'énergie électrique alimentée par deux dynamos.

## Référence

## Liens 