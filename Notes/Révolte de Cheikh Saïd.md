MOC : 
Source : [[Révolte de Cheikh Saïd]]
Date : 2023-07-19
***

## Résumé
La révolte de Cheikh Saïd (en kurde : Serhildana Şêx Seîdê Pîranî, en turc : Şeyh Said İsyanı, en anglais : Sheikh Said rebellion) ou incident de Genç (en turc : Genç Hâdisesi) a été une révolte (ou rébellion) kurde en 1925 visant à raviver le califat islamique et le sultanat, à la suite de la proclamation de la République le 29 octobre 1923 par Mustafa Kemal Atatürk. Elle a utilisé des éléments du nationalisme kurde pour recruter. Elle était dirigée par Cheikh Saïd (en anglais : Sheikh Said) et un groupe d'anciens soldats ottomans également connus sous le nom de Hamidiés. La révolte a été menée par deux sous-groupes kurdes, les Zazas et les Kurmandjis. 

## Référence

## Liens 
[[Kurdistan]]