MOC : 
Source : [[Léo Szilárd]]
Date : 2023-04-24
***

## Résumé
Leó Szilárd, né le 11 février 1898 à Budapest et mort le 30 mai 1964 à La Jolla en Californie, est un physicien hongro-américain.

Parmi les premiers à envisager les applications militaires de l'énergie nucléaire dès 1933, il a participé au projet Manhattan tout en menant une action publique contre l'utilisation de ces armes et promouvant le désarmement. Dans la deuxième partie de sa carrière, il s'intéressa à la biologie moléculaire naissante et fut l'un des initiateurs du Laboratoire européen de biologie moléculaire.

Il envoie une lettre à [[Albert Einstein]] pour prévenir des dangers d'une bombe atomique. Einstein envoie alors une lettre à son nom au Président américain [[Franklin D. Roosevelt]] qui commencera alors le [[Projet Manhattan]]


## Référence

## Liens 