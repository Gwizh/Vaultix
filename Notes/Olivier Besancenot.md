MOC : 
Source : [[Olivier Besancenot]]
Date : 2023-06-01
***

## Résumé
Facteur de profession, il est candidat de la [[Ligue communiste révolutionnaire]] (LCR) aux élections présidentielles de 2002 et 2007, où il recueille respectivement 4,25 % et 4,08 % des voix. Jusqu'en 2011, il est porte-parole du [[Nouveau Parti anticapitaliste]] (NPA), qui a succédé à la LCR

La question de la bureaucratie l'obsède, il voit que dans l'histoire trop souvent la bureaucratie a volé l'émancipation des populations. Le nationalisme aussi car il représente pour lui le pire de l'humanité.

## Référence

## Liens 