[[Film]]
MOC : [[CINEMA]]
Titre : [[Le Fabuleux Destin d'Amélie Poulain]]
Auteur : [[Jean-Pierre Jeunet]]
Date : 2001
***

## Résumé
Amélie est une petite fille qui vit isolée chez ses parents, la raison : son pere pense qu'elle est atteinte de troubles cardiaques alors que son coeur ne bat que pour lui en réalité. Sa mère mourut assez tot dans sa vie dans des circonstances bizarres.

Elle va ensuite travailler dans un café et découvrir un jour dans son appartement une petite boite laissée la par un ancien locataire. Elle se dit qu'elle va tout faire pour retrouver cet homme et lui donner. C'est ce qu'elle fit avec quelques péripéties et ce sentiment d'accomplissement lui fit tant de bien qu'elle continuat avec d'autres personnes.

Elle se mit donc à arpenter les rues de Paris a la recherche de personnes dans le besoin. 
Il y a en gros :
- Son voisin d'en face qui est atteint du saturnisme et qui peint et repeint le [[Le Déjeuner des canotiers]] cherche à trouver un visage pour un jeune fille, il s'inspirera d'Amélie amoureuse.
- Le marchand de légume en bas de chez elle emploie un jeune trisomique qui se prend des remarques franchement atroces. Elle va se venger de cet homme en s'introduisant chez lui et en lui faisant du maman j'ai raté l'avion. 
- Elle va créer une fausse lettre pour une dame qui ressasse son passé avec son mari depuis sa mort.

Pendant tout ça, elle fera la rencontre d'un homme qui, comme elle, rêve énormément. C'est Lucien. Une de ses passions consiste en la création d'albums photos des ratés du photomaton, comme un répertoire des usagers de la gare.

Amélie trouvera cet album et jouera avec lui au jeu du plus timide en amour. Elle gagnera largement en passant un bon quart du film à l'éviter avec des stratagèmes tous plus fous les uns que les autres. Finalement, ils s'embrasseront sur le palier de sa porte.

## Faits importants
- L'humour est fou, il est couplé avec des scènes complètement innatendues et des effets spéciaux surnaturels. Un exemple étant le moment où Lucien sort du restaurant où elle l'avait invité : elle fond LITTÉRALEMENT en larmes. Ou encore, après avoir commis ses petits crimes chez le vendeur de légumes, elle se transformera pour une scène en Zoro.
- Elle regarde souvent la caméra, il y donc une grande complicité avec elle. Franchement, elle me ressemble un peu mais je suis pas aussi loin dans le spectre de l'insocial. Ceci dit, son histoire d'amour pourrait être une des miennes.
- La musique est folle
- Le sexe est assez important pour l'humour. Du décompte des orgasmes à la scène où ils font trembler le café entier, c'est fou.
- Il dure plus de 2 heures mdr et pourtant on ne s'ennuie jamais.
- EXTREMEMENT émouvant à la fin jean pouvais p. 

## Acteurs
[[Audrey Tautou]]
[[Mathieu Kassovitz]]
[[Rufus]]
[[Jamel Debbouze]]
[[Isabelle Nanty]]
[[Dominique Pinon]]

## Référence

## Liens 
Dans l'ordre : 
<-- [[La Haine]]
--> [[L'armée des ombres]]