MOC : 
Source : [[Salvador Allende]]
Date : 2023-06-09
***

## Résumé
Salvador Guillermo Allende Gossens, né le 26 juin 1908 et mort le 11 septembre 1973, est un homme d'État chilien. Il est président de la république du [[Chili]] du 3 novembre 1970 à sa mort.

Candidat socialiste à l’élection présidentielle de 1970, il l’emporte avec 36,6  % des suffrages exprimés, devançant de justesse l’ancien président de droite Jorge Alessandri Rodríguez (35,3 %), ainsi que le démocrate-chrétien Radomiro Tomić (28,1 %).

Le gouvernement de Salvador Allende, soutenu par la coalition de partis de gauche Unité populaire, tente de mettre en place un État socialiste de façon non violente et légale, la « voie chilienne vers le socialisme », par des projets tels que la nationalisation des secteurs clés de l'économie et la réforme agraire. Il fait face à la polarisation politique internationale de la guerre froide et à une grave crise politique, économique et financière dans le pays.

Le coup d'État du 11 septembre 1973 mené par [[Augusto Pinochet]], et soutenu par les États-Unis, renverse par la force le gouvernement et instaure une dictature militaire. Salvador Allende se suicide dans le palais de la Moneda, sous les bombes putschistes.

## Référence

## Liens 