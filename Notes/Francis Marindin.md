MOC : 
Source : [[Francis Marindin]]
Date : 2023-04-20
***
#Adam
## Résumé
He became an Inspecting Officer for the [[Board of Trade]] in 1875, rising to Senior Inspector of Railways in 1895. His work in this regard involved travelling the country to test and inspect new works on passenger railways to ensure their safety before they could be used, and also compiling reports on railway accidents - the accident at Thirsk in 1892 being a notable example. In describing this period of his life, his obituary in The Times of 24 April 1900, described him as "plain speaking, coupled with a complete mastery of his subject", making the point that the railway companies of the time knew that his office "was not likely to allow irregularities to remain long unnoticed". In 1899 he submitted a report on accidents on railway workers on which a new Act of Parliament concerning rail safety was based, and throughout the 1890s was responsible for a host of improvements in the working practices of Britain's railways.

He helped develop London's new electrical lighting system and was knighted in 1897. He died aged 61 on 21 April 1900 at home at Hans Crescent, London S.W., and was buried on the family Scottish property at Craigflower, Torryburn, Dunfermline.

## Référence

## Liens 
[[Sebastian Ziani de Ferranti]]