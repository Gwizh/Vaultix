MOC : 
Source : [[Francis de Pressensé]]
Date : 2023-07-11
***

## Résumé
Francis Charles Dehault de Pressensé, né le 30 septembre 1853 à Paris où il est mort le 19 janvier 1914, est un journaliste, député et homme politique français. Il est membre fondateur et président de 1903 à 1914 de la Ligue des droits de l'homme. 

Radicalisé au moment de l'[[Affaire Dreyfus]], il participa à des meetings communs à des anarchistes mais se revendiquait socialiste (il était journaliste modéré avant ça).

## Référence

## Liens 
[[La Revue blanche]]