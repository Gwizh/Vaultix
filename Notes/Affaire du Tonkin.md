MOC : 
Source : [[Affaire du Tonkin]]
Date : 2023-06-08
***

## Résumé
L’affaire du Tonkin est une crise politique française qui a éclaté en mars 1885 dans les dernières semaines de la guerre franco-chinoise. Elle mit fin à la carrière de président du Conseil de [[Jules Ferry]], et marqua un brusque coup d’arrêt à la succession de gouvernements républicains inaugurée quelques années plus tôt par [[Léon Gambetta]]. Le soupçon au sein de la classe française publique et politique que les troupes françaises avaient été envoyées à la mort loin de chez eux, tant au [[Tonkin]] qu’ailleurs, pour un profit difficile à évaluer discrédita, par la même occasion, les partisans de l’expansion coloniale française, pendant près d’une décennie, et raviva le mouvement anticolonialiste en France. 

À voir : 10 décembre 1883 : discours de [[Georges Clemenceau]] contre la politique française au Tonkin

## Référence
[[Guerre franco-chinoise]]

## Liens 