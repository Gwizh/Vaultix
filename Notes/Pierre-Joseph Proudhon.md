[[Note Permanente]]
MOC : [[Histoire]]
Source : [[Pierre-Joseph Proudhon]]
Date : 2022-08-05
***

Figure de l'[[Anarchie]], du [[Mutuellisme]] et du [[Fédéralisme]].
Il pense qu'on a pas besoin d'une révolution frontale, juste de créer un système concurrent qui va remplacer le capitalisme et l'État.
Très misogyne et antisémite. 

## Résumé
Pierre-Joseph Proudhon, né le 15 janvier 1809 à Besançon et mort le 19 janvier 1865 à Paris (16e arrondissement), est un polémiste, journaliste, économiste, philosophe, politique et sociologue français. Précurseur de l'**[[Anarchie]]**, il est le seul théoricien révolutionnaire du XIXe siècle à être issu du milieu ouvrier. 

> « La liberté est anarchie, parce qu'elle n'admet pas le gouvernement de la volonté, mais seulement l'autorité de la loi, c'est-à-dire de la nécessité »

En 1840, dans son premier ouvrage majeur, Qu'est-ce que la propriété ? ou Recherche sur le principe du Droit et du Gouvernement, il rend célèbre la formule "La propriété, c'est le vol !". Dans ce même ouvrage, il est le premier auteur à utiliser l'expression « socialisme scientifique », lorsqu'il écrit : « La souveraineté de la volonté cède devant la souveraineté de la raison, et finira par s'anéantir dans un socialisme scientifique ». 

## Pensée
D'après Proudhon, le monde vit et progresse selon l'équilibre de forces antagonistes. Organiser la société, ce n'est pas chercher la synthèse des contraires, qui serait leur mort, c'est travailler à l'établissement d'un équilibre nécessairement instable mais éclairé par l'idée de justice. 

### **Une philosophie de l'action**
C'est par le travail que l'homme inscrit la marque de sa liberté dans le monde, qu'il devient proprement humain et développe sa faculté de penser.
Proudhon développe une philosophie de l'action, "pratique et populaire", qui s'oppose aux spéculation stériles de la métaphysique.

La science sociale souhaitée par Proudhon doit étudier l'opposition des forces et des consciences collectives. La classe des travailleurs salariés est à la fois la véritable productrice de richesse et la victime permanente du système aux mains de la classe des propriétaires. L'État n'a jamais engendré que la violence, l'aliénation et l'abus de pouvoir. La **révolution sociale** doit renverser cet ordre et imposer l'idéal de justice propre à la raison collective.

La priorité est l'**abolition de la propriété privée** mais pas en vue du collectivisme. Le capital est le premier dieu que le communisme adore, le second étant le pouvoir, car il prétend s'approprier aussi les consciences et les facultés des individus. Or ce n'est pas ce que veulent les ouvriers, spontanément cela tend plutôt vers le [[Mutuellisme]].

### **La démocratie socialiste**
Proudhon en appelle donc à la révolution prolétarienne et exige un dépossession brusque de la caisse capitaliste. Il est temps, sous peine d'une régression pour l'humanité, d'instaurer la démocratie socialiste, système fédératif agricole et industriel, mutuelliste et autogéré. L'État ne sera pas aboli mais il n'aura plus qu'un rôle de consultation, d'instruction et d'encouragement.

## Référence

## Liens 