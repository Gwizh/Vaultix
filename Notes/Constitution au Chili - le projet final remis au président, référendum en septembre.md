[[Article]]
MOC : [[Politique]]
Titre : [[Constitution au Chili - le projet final remis au président, référendum en septembre]]
Auteur : [[Le Monde]]
Date : 2022-07-05
Lien : https://www.lemonde.fr/international/article/2022/07/05/constitution-au-chili-le-projet-final-remis-au-president-referendum-en-septembre_6133344_3210.html
Fichier : ![[Constitution au Chili _ le projet final remis au président, référendum en septembre.pdf]]
***

## Résumé
- Pour ce scrutin à vote obligatoire, 15 millions de Chiliens devront dire s’ils acceptent (« Apruebo ») ou rejettent (« Rechazo ») cette nouvelle Constitution. « Une nouvelle fois le peuple aura le dernier mot sur son destin. Nous démarrons une nouvelle étape » a déclaré le chef d’État et du gouvernement Gabriel Boric.
- Vu comme une sortie de crise politique au soulèvement de 2019 pour plus d’égalité sociale, le projet constitutionnel consacre à travers ses 388 articles de nouveaux droits sociaux, principales revendications des manifestants. Dans l’article premier, le Chili est notamment défini comme un _« Etat social et démocratique de droit »_, _« plurinational, interculturel et écologique »_, et _« sa démocratie est paritaire »_.
- _« Je vous invite à débattre intensément de la portée du texte, mais pas des mensonges, déformations ou interprétations catastrophistes qui sont déconnectées de la réalité »_, a exhorté le chef de l’Etat, alors que la campagne pour le référendum débute mercredi. Ces dernières semaines, le jeune président (36 ans) a réitéré son soutien au projet de Constitution, estimant que la Loi fondamentale actuelle – adoptée en 1980 en plein régime militaire et qui limite au maximum l’intervention de l’Etat –, représentait un _« obstacle »_ à toute réforme sociale de fond.
- A deux mois du référendum, de nombreux sondages indiquent toutefois que le « non » (Rechazo), soutenu par la droite, pourrait l’emporter. Mais une partie des Chiliens reconnaît ne pas avoir d’opinion définitive sur le texte qui va être désormais diffusé dans sa totalité.

## Référence
[[Chili]]
[[Démocratie]]

## Liens 