MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081614
Tags : #Fini
***

# Industrialisation de la mise à mort (animal et humaine)
Nous tuons des milliards de bêtes aujourd'hui. #Question Combien ?
Les techniques pour contrôler et tuer les êtres vivants sont assez proche, que nous parlons des camps de la mort nazis ou des abattoirs. Et ce n'est pas récent, déjà avant la [[Seconde Guerre mondiale]], nous tuions des millions de bêtes dans une même usine, les plus connues étant celles de Chicago et celles d'Uruguay.

## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]