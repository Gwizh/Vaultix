MOC : 
Source : [[L'extrême droite et le système de retraite - Minuit dans le siècle]]
Date : 2023-04-09
***

## Résumé 
L'extrême droite se voit comme une troisième voie entre le socialisme et le capitalisme. En réalité c'est une façon de gouverner le capitalisme en supprimant tout contre pouvoir syndical, politique, artistique et culturel. 

Commenté par Denis Durand 
https://blogs.mediapart.fr/denis-durand

[[Jordan Bardella]] ne soutien pas le blocage de l'économie et les grèves reconductibles. Pourtant, [[Marine Le Pen]] est vue comme la principale opposition à la réforme des retraites. Il a eu deux périodes du [[Front National]], une période très extrême avec des réformes économiques néolibérales. Et une autre période, avec une grande démagogie sociale, sans jamais acter les changements. Pour réindustrialiser la France, elle propose de réduire les taxes et les charges sur les entreprises, c'est tout. Toujours contre l'impôt et l'atteinte au pouvoir patronal. La réduction des impôts sur la production des entreprises est déjà fait par [[Bruno Le Maire]] et donc dans un sens fait déjà ce que Le Pen voudrait faire. Le [[Rassemblement National]] est en phase avec ça. 1/4 des gens qui sont capables de travailler ne peut pas travailler aujourd'hui. Au contraire, il y a culpabilisation de ces populations. Dans un sens, il y a déjà un tel extrémisme néolibéral avec [[Emmanuel Macron]] que le RN serait une forme de régression par rapport à ça, et donc potentiellement plus social pour le coup, un comble ! Le RN, un moindre mal économique :'(

Avec la crise inflationniste et politique, c'est un moment propice pour une crise de régime. Pour continuer cette politique, il y a possibilité que la classe dirigeante ouvre la porte au fascisme pour continuer cette politique néolibérale jusqu'au bout. 

[[Mario Draghi]] a aménagé l'arrivée au pouvoir de [[Giorgia Meloni]] et ça donne le vertige par rapport à la France. 

Avant 2022, elle voulait la sortie de l'euro et la retraite à 60 ans. Ça a bien changé ensuite pour vraiment suivre le néolibéralisme. Quand il y a changement, c'est quasiment toujours par démagogie en trouvant ce qui plaît dans la population et l'électorat. 

Selon Durand, la culture hégémonique à gauche est la [[Sociale-Démocratie]], ce qui participe à l'idée qu'une gauche extrême signifie un État extrêmement fort qui semble complice de l'extrême droite. Aujourd'hui, au vu du déficit de l'État et de la toute puissance apparente du Capital, on perd espoir face à l'État et ses solutions, on se dit directement : mais comment on finance ? C'est l'idée que l'État ne peut pas tout. Et donc que l'extrême gauche n'est pas viable. C'est à la fois idéologique mais réel, l'État n'a aujourd'hui plus la possibilité de financer la retraite à 60 ans, il a régressé depuis les années 80. Il faut forcément redonner de la force à l'État pour qu'il puisse faire ça mais ça demande donc de revoir tout le système économique. 

[[Viktor Orban]] a réduit complètement l'impôt sur les bénéfices des sociétés, c'est le plus faible taux d'indemnisation des chômeurs en Europe (3 mois). C'est un Impôt sur le revenu qui est une flat taxe, non regardante du niveau de richesse. Mesures anti-syndicale et anti-grève. Le néolibéralisme actuel montre la présence d'un État fort, mais dans le sens des entreprises avec beaucoup d'aides publiques pour les entreprises privées. 

Pour répondre à la crise économique des années 30, tous les pays dominants de l'époque ont opté pour l'intervention de l'État, les [[Fascisme]] pour l'Allemagne et l'Italie, le [[New Deal]] de [[Franklin D Roosevelt]] et les réformes du [[Front Populaire]] et de la [[Libération]]. Le capitalisme s'est nié lui-même pour permettre à son économie de survivre la crise et pour regagner confiance de la population. Paradoxalement, ça a très bien marché pendant un moment. 

## Référence

## Liens 