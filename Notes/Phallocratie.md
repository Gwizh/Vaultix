MOC : 
Source : [[Phallocratie]]
Date : 2023-06-10
***

## Résumé
La phallocratie (du grec phallos, « pénis en érection » et cratos « pouvoir ») est la domination sociale, culturelle et symbolique exercée par les hommes non apparentés sur les femmes. Par extension, le terme désigne une structure sociale [[Misogynie|misogyne]] et patriarcale dans laquelle la domination est exercée par des hommes apparentés.

Son antonyme est la gynocratie.

## Référence
[[Patriarcat]]

## Liens 
