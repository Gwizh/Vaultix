MOC : 
Source : [[Badge Man]]
Date : 2023-05-05
***

## Résumé

The Badge Man is a figure that is purportedly present within the Mary Moorman photograph of the assassination of United States president [[John F. Kennedy]] in Dealey Plaza on November 22, 1963. Conspiracy theorists have suggested that this figure is a sniper firing a weapon  president from the grassy knoll. Although a reputed muzzle flash obscures much of the detail, the Badge Man has been described as a person wearing a police uniform—the moniker itself derives from a bright spot on the chest, which is said to resemble a gleaming badge.


## Référence

## Liens 
[[Complotisme]] 