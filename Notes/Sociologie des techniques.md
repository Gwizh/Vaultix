Titre : [[Sociologie des techniques]]
Type : Noeud
Tags : #Noeud
Ascendants :
Date : 2023-12-07
***
## Définitions
La sociologie des techniques (ou sociologie de l'innovation) est une discipline scientifique et de recherche apparue au début des années 1980 dans le domaine des STS (Sciences, technologies et société), qui traite des interactions entre « la société » et « la technique ». 

## Plan de route
[[Déterminisme technologique]]
[[Technocritique]]
[[Sciences au XIXème siècle]]

## Sources
#Alire Le Système technicien Jacques Ellul [Résumé ici](https://fr.wikipedia.org/wiki/Le_Syst%C3%A8me_technicien) [15€ ici](https://www.chasse-aux-livres.fr/prix/TWM3PQZBME/le-systeme-technicien-jacques-ellul)
#Alire Science, industrie, innovation et société au XIXe siècle, Sophie Chauveau [Dans le drive]
#Alire [Page Wiki, c'est un bon début](https://fr.wikipedia.org/wiki/Sociologie_des_techniques)
#Alire Sciences et société, [ici](https://www.chasse-aux-livres.fr/prix/2200347286/sciences-et-societe-dominique-vinck?query=Dominique%20Vinck%2C%20Sociologie%20des%20sciences)
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]
