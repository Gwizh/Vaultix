MOC : 
Source : [[École autrichienne (économie)]]
Date : 2023-07-03
***

## Résumé
L’école autrichienne d’économie est une école de pensée économique hétérodoxe. Elle se fonde sur l'individualisme méthodologique et rejette l'intervention de l'État. Elle s'oppose à l’application à l’économie des méthodes employées par les sciences naturelles. Ses partisans défendent des idées très libérales en matière économique et plus généralement d’organisation de la société. Elle s'oppose tant à la nouvelle économie keynésienne qu'à la nouvelle économie classique. 

## Référence

## Liens 
[[Économie]]
[[XIXe siècle]]
[[Individualisme]]