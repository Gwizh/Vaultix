MOC : 
Source : [[Élections parlementaires italiennes de 1994]]
Date : 2023-05-10
***

## Résumé
Ce scrutin a signé un panorama politique très différent dans l’histoire de l'Italie. Le parti traditionnel [[Démocratie chrétienne (Italie)]] connut une profonde crise. 

En effet, De 1992 à 1994, l'[[Opération Mains propres]] révéla un système de corruption à grande échelle dans les partis politiques italiens, DC comprise. Pris dans la tourmente, le parti fut finalement dissous le 29 janvier 1994, se dispersant alors dans un vaste nombre d'organisations politiques.

Cest aussi l'ascension de [[Silvio Berlusconi]] et de [[Forza Italia]].

## Référence

## Liens 