MOC : 
Source : [[Recherches sur le secteur financier]]
Date : 2023-06-23
***

## Résumé

La Bourse est une institution où les produits financiers, tels que les actions et les obligations, sont échangés.

Au XIXe siècle, différentes bourses existaient pour la vente d'actions et de produits spécifiques, comme les produits industriels.

Les obligations sont des titres de créance émis par des entités, tandis que les titres gouvernementaux sont des obligations émises par les gouvernements.

Les obligations offrent un flux de revenus fixe, tandis que les actions offrent la possibilité de participer aux bénéfices et aux décisions de l'entreprise.

Être coté en bourse signifie que les actions d'une entreprise sont négociées sur un marché boursier public, ce qui permet aux investisseurs d'acheter et de vendre ces actions.

Les bourses étaient créées par des entités publiques ou privées, telles que des sociétés de courtage ou des chambres de commerce.

Les prix des produits vendus en bourse sont déterminés par l'offre et la demande, ainsi que par d'autres facteurs tels que les nouvelles économiques, les performances de l'entreprise et les conditions du marché.

La spéculation peut augmenter les prix des produits en créant une demande artificielle sur les marchés à terme et en influençant les prix sur les marchés physiques.

Une bulle spéculative se produit lorsqu'il y a une hausse excessive et insoutenable des prix d'un actif, alimentée par la spéculation, ce qui peut entraîner un effondrement ultérieur des prix.

Les spéculateurs peuvent bénéficier de leurs activités, mais leurs actions peuvent également avoir des effets sur l'économie réelle, notamment sur les prix des matières premières.

Les actifs financiers comprennent les actions, les obligations, les devises, les matières premières, les options, les contrats à terme et bien d'autres.

Les actifs financiers disponibles varient en fonction des opportunités d'investissement et des produits financiers offerts sur les marchés.

À la fin du XIXe siècle, les actifs financiers principaux incluaient les actions de sociétés minières, les obligations d'entreprises et les titres gouvernementaux.

Posséder des actions d'une entreprise ne garantit pas automatiquement une position de pouvoir ou de direction dans cette entreprise.

La spéculation peut avoir un impact sur le prix réel des produits, notamment en augmentant la demande sur les marchés financiers et physiques.

La spéculation peut être liée à l'inflation, mais elle n'est pas la seule cause de l'inflation. L'inflation est influencée par de nombreux facteurs économiques et monétaires.

Les outils de spéculation modernes, tels que les plateformes de trading en ligne, permettent aux individus d'investir et de spéculer sur les marchés financiers via des dispositifs mobiles ou des ordinateurs.

Les institutions financières fournissent des actifs financiers pour répondre aux besoins des investisseurs et gagnent des revenus grâce aux transactions et aux services qu'elles proposent.

La spéculation peut profiter aux spéculateurs, mais elle peut également apporter de la liquidité aux marchés et faciliter la découverte des prix.

Les spéculateurs peuvent avoir un impact sur les prix des produits, mais ils ne sont pas les seuls acteurs à les influencer. D'autres facteurs tels que l'offre et la demande, les fondamentaux économiques et les politiques gouvernementales jouent également un rôle.

La spéculation sur les marchés des matières premières, comme le blé, peut influencer la demande sur les marchés à terme et les prix sur les marchés physiques, en fonction des positions prises par les spéculateurs.

Les spéculateurs peuvent bénéficier de leurs activités, mais leur impact sur l'économie réelle peut être complexe et dépendant de divers facteurs économiques et comportementaux.

## Référence

## Liens 