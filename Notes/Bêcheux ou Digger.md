MOC : 
Source : [[Bêcheux ou Digger]]
Date : 2023-07-23
***

## Résumé
Les Bêcheux, ou Piocheurs (Diggers en anglais), sont une faction protestante de la Première Révolution anglaise, fondée en 1649 par Gerrard Winstanley. Se faisant appeler Vrais Niveleurs à leurs débuts (True Levellers), le public finit par les baptiser « Bêcheux », Diggers, en raison du mode de vie qu'ils prônaient. Il s'agit du plus ancien collectif de squatteurs connu à ce jour et considéré comme un des précurseurs de l'anarchisme moderne.
## Référence

## Liens 