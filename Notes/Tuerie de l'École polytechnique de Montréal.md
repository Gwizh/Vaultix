MOC : 
Source : [[Tuerie de l'École polytechnique de Montréal]]
Date : 2023-06-11
***

## Résumé
La tuerie de l'École polytechnique est une tuerie en milieu scolaire qui a eu lieu le 6 décembre 1989 à l'École polytechnique de Montréal, au Québec (Canada). Marc Lépine, âgé de 25 ans, ouvre le feu sur vingt-huit personnes, tuant quatorze femmes et blessant treize autres personnes (9 femmes et 4 hommes)1, avant de se suicider. Cette tuerie de masse est perpétré en moins de vingt minutes à l'aide d'une carabine obtenue légalement2,3. Il s'agit de la tuerie en milieu scolaire la plus meurtrière de l'histoire du Canada.

Lépine s'est assis plusieurs minutes dans le bureau d'enregistrement situé au deuxième étage. Il est aperçu fouillant dans un sac en plastique, ne parlant à personne, même lorsqu'un membre du personnel lui a offert son aide. Il quitte le bureau et est ensuite vu dans d'autres parties du bâtiment, avant d'entrer dans une classe d'ingénierie mécanique au deuxième étage. Cette classe est composée, vers 17 h 10, d'environ soixante étudiants2. Après s'être approché de l'étudiant qui effectue une présentation, il demande à tout le monde d'arrêter ce qu'ils font. Puis, il ordonne aux hommes et aux femmes de se séparer en deux groupes distincts de chaque côté de la salle. Croyant à une blague, personne ne bouge sur le moment, jusqu'à ce que Lépine tire un coup de feu au plafond4.

Il sépare alors les neuf femmes de la cinquantaine d'hommes présents et ordonne à ceux-ci de partir. Il demande ensuite aux femmes restantes si elles savent ou non pourquoi elles sont là, et lorsqu'une d'elles répond « non », il réplique : « Je combats le féminisme. » L'étudiante Nathalie Provost répond :

> « Écoutez, nous sommes juste des femmes étudiant l'ingénierie, pas forcément des féministes prêtes à marcher dans les rues criant que nous sommes contre les hommes, juste des étudiantes cherchant à mener une vie normale. »

Ce à quoi Lépine rétorque :

> « Vous êtes des femmes, vous allez devenir des ingénieures. Vous n'êtes toutes qu'un tas de féministes, je hais les féministes. »

## Référence
[[Féminisme]]
[[Sexisme]]
[[Misogynie]]
[[Terrorisme]]

## Liens 