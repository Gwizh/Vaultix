MOC : 
Titre : [[Herbert Spencer]]
Date : 2023-11-02
***

## Résumé
Herbert Spencer, né le 27 avril 1820 à Derby et mort le 8 décembre 1903 à Brighton, est un philosophe et sociologue anglais. Son nom est associé à l'application des théories de [[Charles Darwin]] à la [[Sociologie]], et donc au [[Darwinisme social]], même si les partisans de ces théories rejettent ce terme, lui préférant celui de spencérisme. Il popularise par ses publications l'idée d'évolution et de survie des plus aptes (survival of the fittest). 
## Références

## Liens 