MOC : 
Source : [[Fédération nationale des Jaunes de France]]
Date : 2023-06-11
***

## Résumé
La Fédération nationale des Jaunes de France est un syndicat français fondé le 1er avril 19021 et dont l'existence s'achève en 19122.

Créé par [[Pierre Biétry]] après sa rupture avec [[Paul Lanoir]], ce syndicat s'inscrit dans l'histoire du syndicalisme jaune qu'il a tenté de fédérer, tout en développant une idéologie de collaboration de classes et un fort antisémitisme. En 1902, sa devise était « Patrie, famille, travail », termes employés dans un autre ordre (Travail, Famille, Patrie) par le député républicain [[Sadi Carnot (homme d_État)]] en 1882 et par les Croix-de-Feu ainsi que le Parti social français (PSF) dès 1933. Le [[Régime de Vichy]] reprendra ce même slogan le 10 juillet 1940.

## Référence

## Liens 