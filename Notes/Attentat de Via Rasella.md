MOC : 
Source : [[Attentat de Via Rasella]]
Date : 2023-05-05
***

## Résumé
L’attentat de Via Rasella, en italien Attentato di Via Rasella, parfois appelé Attacco di Via Rasella (« attaque de Via Rasella »,,,,), est la plus importante action menée à Rome par la [[Résistance italienne]] contre les forces d'occupation allemandes. Le 23 mars 1944, l'attaque tue 32 soldats du Polizeiregiment Bozen et en blesse environ 110 autres contre aucune perte coté italien. En représailles les Allemands tueront plus de 335 personnes le lendemain lors du massacre des Fosses ardéatines. 

Cette action est menée par les [[Groupes d'action patriotique]] (GAP) contre un corps armé des troupes d'occupation allemandes, le Polizei-Regiment Bozen, un régiment de l' Ordnungspolizei, la police d'ordre publique. Au moment de l'attaque, le régiment était à disposition du commandement militaire allemand de la ville de Rome. Le régiment a pris le préfixe « SS », et donc le nom « SS-Polizei-Regiment Bozen », seulement le 16 avril 1944. 


## Référence

## Liens 
[[Nazisme]] 
[[Seconde Guerre mondiale]]