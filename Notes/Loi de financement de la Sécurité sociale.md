MOC : 
Source : [[Loi de financement de la Sécurité sociale]]
Date : 2023-05-21
***

## Résumé
En France, une loi de financement de la Sécurité sociale (LFSS) autorise le budget de la [[Sécurité sociale]], sur le modèle de la loi de finances. Cette catégorie de lois a été créée par la révision de la Constitution du 22 février 1996.

Dans le prolongement de la loi organique relative aux lois de finances (LOLF), les lois de financement de la Sécurité sociale sont placées sous le régime de la loi organique relative aux lois de financement de la Sécurité sociale (LOLFSS), promulguée le 2 août 2005.

## Référence

## Liens 