MOC : 
Source : [[Communication NonViolente]]
Date : 2023-06-02
***

## Résumé
### L'Élan du coeur
Dirigeons notre conscience là où nous avons des chances de trouver ce que nous recherchons.
Les quatre composants de la CNV :
1. Observations
2. Sentiments
3. Besoins
4. Demandes

Les deux phases de la CNV :
1. Exprimer notre sincérité en utilisant les quatres composantes
2. Écouter avec empathie en utilisant les quatres composantes. 

#### Quand la communication entrave la bienveillance
Certaines façons de communiquer nous coupent de notre bienveillance naturelle. 

Dans le monde des jugements, notre intérêt se porte sur qui est quoi. 

Notre analyse d'autrui est en fait l'expression de nos propres besoins et sentiments. 

Cataloguer et juger les autres favorisent la violence. Les comparaisons sont une forme de jugement. 

Notre langage nous empêche de voir clairement notre responsable personnel. Nous pouvons remplacer le langage impliquant une absence de choix par un langage qui reconnaît le choix. Nous sommes dangereux quand nous ne sommes pas conscients que nous ne sommes responsables de nos actes, de nos pensées et de nos sentiments. 

Il n'est pas en notre pouvoir de faire faire quelque chose à quelqu'un. Penser à "qui mérite quoi" bloqué la communication empathique. La communication aliénante a des racines philosophiques et politiques très profondes.  

#### Observer sans évaluer
Lorsque nous amalgamons observation et évaluation, notre interlocuteur risque d'entendre une critique. 
1. Emploi du verbe être sans indiquer qu'il s'agit d'un jugement
2. Emploi de verbes à connotation évaluative. L'exemple ici c'est traîner à faire qq chose. 
3. Propension à considérer notre évaluation des pensées, sentiments, intentions ou désirs d'autrui comme la seule possible. 
4. Confusion entre prédiction et certitude
5. Emploi de référents trop vagues. En gros généraliser à partir d'un exemple. 
6. Emploi de mots exprimant l'aptitude ou l'inaptitude à agir, sans indiquer qu'il s'agit d'un jugement. 
7. Emploi d'adverbes ou d'adjectifs sans indiquer qu'il s'agit d'un jugement. 
8. Utiliser toujours, jamais, un vocabulaire péremptoire

#### Identifier et exprimer les sentiments
Exprimer notre vulnérabilité peut aider à résoudre des conflits. Faire une distinction entre sentiments et pensées. Ne pas utiliser sentir à la place de penser. Toujours détailler le plus possible nos sentiments pour chercher plus loin que les "bien", "mal", " bête", "nul", etc... Faire une distinction entre ce que nous ressentons et notre interprétation des réactions ou comportements des autres à notre égard. Ça va dans la même idée que la différence entre jugement et observation

## Référence

## Liens 