MOC : 
Source : [[École]]
Date : 2023-05-20
***

## Résumé
#### Mixité sociale des collèges : Angers à l’heure de l’évitement généralisé

Le poids du privé, les contours de la carte scolaire ou les dérogations entre établissements publics conduisent la plupart des familles angevines à ne pas choisir leur collège de secteur. Non sans conséquences sur le fonctionnement des établissements les plus contournés. 
Article du monde


## Référence

## Liens 