[[Article]]
MOC : [[Noz/ECONOMIE]]
Titre : [[Energie - la France se prépare à une économie de guerre]]
Auteur : [[Les Echos]]
Date : 2022-07-07
Lien : https://www.lesechos.fr/industrie-services/energie-environnement/energie-la-france-se-prepare-a-une-economie-de-guerre-1759738
Fichier : 
***

## Résumé
Le projet de loi sur le pouvoir d'achat comporte un volet « souveraineté énergétique » qui attribue des pouvoirs sans précédent à l'Etat en cas de crise majeure cet hiver. Il prévoit la réquisition des centrales à gaz, le remplissage forcé des stockages, des dérogations pour accélérer l'installation d'un terminal d'importation de GNL au Havre et le redémarrage de la centrale à charbon de Saint-Avold.

La France fait un grand pas vers l'économie de guerre, en tout cas pour ce qui concerne le secteur de l'énergie. [La loi pouvoir d'achat](https://www.lesechos.fr/economie-france/budget-fiscalite/pouvoir-dachat-des-mesures-pour-faciliter-les-accords-dinteressement-dans-les-entreprises-1415822) , présentée en Conseil des ministres la semaine prochaine, comporte un volet « souveraineté énergétique » qui attribue des pouvoirs sans précédent à l'Etat pour faire fonctionner les infrastructures gazières françaises comme il le jugera indispensable cet hiver. Elle compte plusieurs dispositions « relatives à la sécurité d'approvisionnement en gaz et en électricité », selon une version provisoire du texte transmise aux « Echos ».

Le gouvernement se prépare ainsi au [scénario d'une rupture totale d'approvisionnement en gaz russe](https://www.lesechos.fr/industrie-services/energie-environnement/gaz-la-france-sonne-le-branle-bas-de-combat-1415375) , jugé hautement probable désormais. Première mesure d'exception, la loi autorisera l'Etat à dicter la conduite des centrales à gaz si nécessaire, passant outre les mécanismes de marché en vigueur en temps normal.

« Les centrales à gaz sont vitales pour le système électrique français qui est proche de la défaillance, estime l'un des opérateurs concernés. L'absence totale de précisions sur la façon dont la réquisition fonctionnerait, et sur les conditions d'indemnisation, ne peut qu'introduire une incertitude supplémentaire susceptible de faire progresser encore plus les prix de marché. » Pour le quatrième trimestre 2022, les prix des contrats d'électricité atteignent 790 euros le MWh en France, contre 378 euros le MWh en Allemagne.

D'autres dispositions visent à accélérer [l'installation d'un terminal flottant d'importation de gaz naturel liquéfié](https://www.lesechos.fr/industrie-services/energie-environnement/la-france-va-se-doter-dun-nouveau-terminal-dimportation-de-gaz-liquefie-1396363) (GNL) de TotalEnergies dans le port du Havre. Le gouvernement vise une mise en service en septembre 2023 pour accroître nos capacités en vue de l'hiver 2023-2024. Un calendrier serré qui suppose de passer outre une série de procédures et d'autorisations susceptibles de retarder les travaux (raccordement au réseau de gazoducs de GRTgaz, aménagement du port…).

Le texte comprend des mesures du même acabit pour l'électricité. Des dérogations au droit du travail sont en particulier prévues afin d'assurer une « reprise temporaire des installations de production d'électricité […] pour faire face à des difficultés d'approvisionnement en énergie susceptibles d'affecter la vie de la nation ».

## Référence
[[Energie]]

## Liens 