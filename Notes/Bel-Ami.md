[[Livre]]
MOC : [[LITTERATURE]]
Titre : [[Bel-Ami]]
Auteur : [[Guy de Maupassant]]
Date : 1885
***

## Résumé
C'est l'histoire d'un homme, George Duroy, qui cherche à devenir riche en gravissant les échelons de la société française de l'époque (fin du 19e). Il est aidé au début par son ami M. Forestier qui le fait devenir journaliste à la Vie Française, un journal alors peu populaire. Il fait connaissance de sa femme puis de ses amis dont il va charmer la plupart. Il va avoir plusieurs maitresses dont Mme. Forestier elle-même après la mort de son mari. De part d'habiles alliances et stratagèmes, il va devenir le rédacteur en chef du journal, très riche et disons le, avoir 4 maitresses en tout. Il se marie à la fin du livre avec la riche héritière d'une grande fortune de la ville, son avenir est tout tracé.

## Notions clés

## Citations
> "la phrase qui montre des images dévêtues avec des expressions couverte"

> « C’est l’instant d’acheter des tableaux. Les peintres crèvent de faim. Ils n’ont pas le sou, pas le sou… »

> Pensez à tout cela, jeune homme, pensez-y pendant des jours, des mois et des années, et vous verrez l’existence d’une autre façon. Essayez donc de vous dégager de tout ce qui vous enferme, faites cet effort surhumain de sortir vivant de votre corps, de vos intérêts, de vos pensées et de l’humanité tout entière, pour regarder ailleurs, et vous comprendrez combien ont peu d’importance les querelles des romantiques et des naturalistes, et la discussion du budget. La mort, ce n’est pas seulement un silence autour du corps, mais un silence autour de l’âme.

> L’espace derrière les cimes sombres était rouge, d’un rouge sanglant et doré que l’œil ne pouvait soutenir.

> Duroy subissait malgré lui la majesté de cette fin du jour.

> Le souffle qui entra les surprit tous les trois comme une caresse. C’était une brise molle, tiède, paisible, une brise de printemps nourrie déjà par les parfums des arbustes et des fleurs capiteuses qui poussent sur cette côte. On y distinguait un goût puissant de résine et l’âcre saveur des eucalyptus.

> Il sentait bien qu’il lui plaisait, qu’elle avait pour lui plus que de la sympathie, une de ces affections qui naissent entre deux natures semblables et qui tiennent autant d’une séduction réciproque que d’une sorte de complicité muette.

> Comprenez-moi bien. Le mariage pour moi n’est pas une chaîne, mais une association. J’entends être libre, tout à fait libre de mes actes, de mes démarches, de mes sorties, toujours. Je ne pourrais tolérer ni contrôle, ni jalousie, ni discussion sur ma conduite. Je m’engagerais, bien entendu, à ne jamais compromettre le nom de l’homme que j’aurais épousé, à ne jamais le rendre odieux ou ridicule. Mais il faudrait aussi que cet homme s’engageât à voir en moi une égale, une alliée, et non pas une inférieure ni une épouse obéissante et soumise. Mes idées, je le sais, ne sont pas celles de tout le monde, mais je n’en changerai point. Voilà.

> C’était une nuit tiède dont l’ombre caressante et profonde semblait pleine de bruits légers, de frôlements, de souffles.

> C’était une nuit sans vent, une de ces nuits d’étuve où l’air de Paris surchauffé entre dans la poitrine comme une vapeur de four.

> comme s’il avait reçu une commotion morale.

> S’il fait beau, c’est une canne ; s’il fait du soleil, c’est une ombrelle ; s’il pleut, c’est un parapluie, et, si on ne sort pas, on le laisse dans l’antichambre.

> Il s’arrêta en face d’elle ; et ils demeurèrent de nouveau quelques instants les yeux dans les yeux, s’efforçant d’aller jusqu’à l’impénétrable secret de leurs cœurs, de se sonder jusqu’au vif de la pensée. Ils tâchaient de se voir à nu la conscience en une interrogation ardente et muette : lutte intime de deux êtres qui, vivant côte à côte, s’ignorent toujours, se soupçonnent, se flairent, se guettent, mais ne se connaissent pas jusqu’au fond vaseux de l’âme.

> Ils l’entraînaient à l’autre bout du bassin, s’agitaient au-dessous, formant maintenant une grappe mouvante, une espèce de fleur animée et tournoyante, une fleur vivante, tombée à l’eau la tête en bas.

> Tantôt elles jetaient des clameurs prolongées, énormes, enflées comme des vagues, si sonores et si puissantes, qu’il semblait qu’elles dussent soulever et faire sauter le toit pour se répandre dans le ciel bleu.

> comme si un grain de sable se métamorphosait en un monde

## Passages amusants
> "Une séduction irrésistible dans la moustache"

> "Il n’y a rien de plus drôle que de regarder cette vieille bedole de Norbert jouer au bilboquet. Il ouvre la bouche comme pour avaler la boule."

> "— Hein ? Est-il à la Balzac, celui-là ? Duroy n’avait pas lu Balzac, mais il répondit avec conviction : — Bigre oui."
 
> "Car l’adresse au bilboquet conférait vraiment une sorte de supériorité dans les bureaux de La Vie Française."
 
> "Duroy hésitait, un peu perplexe, ne s’étant jamais trouvé encore en face d’un homme dont il possédait la femme."

> Il regardait la figure sérieuse et respectable de M. de Marelle, avec une envie de rire sur les lèvres, en pensant : « Toi, je te fais cocu, mon vieux, je te fais cocu.

> Il avait été soldat, il avait tiré sur des Arabes, sans grand danger pour lui, d’ailleurs, un peu comme on tire sur un sanglier, à la chasse.

> « Tu es encore plus popote que mon mari, ça n’était pas la peine de changer. »

> Moi j’avais pensé à prendre le nom de mon pays, comme pseudonyme littéraire d’abord, puis à l’ajouter peu à peu au mien, puis même, plus tard, à couper en deux mon nom comme vous me le proposiez. Elle demanda : — Votre pays c’est Canteleu ? — Oui. Mais elle hésitait : — Non. Je n’en aime pas la terminaison. Voyons, est-ce que nous ne pourrions pas modifier un peu ce mot… Canteleu ? Elle avait pris une plume sur la table et elle griffonnait des noms en étudiant leur physionomie. Soudain elle s’écria : — Tenez, tenez, voici. Et elle lui tendit un papier où il lut : Madame Duroy deCantel. Il réfléchit quelques secondes, puis il déclara avec gravité : — Oui, c’est très bon.

> — Oh ! comme je voudrais boire vos larmes !

> Elle se sentait prise comme une bête dans un filet, liée, jetée entre les bras de ce mâle qui l’avait vaincue, conquise, rien que par le poil de sa lèvre et par la couleur de ses yeux.

> Vous vous êtes offert la fantaisie artiste d’orner l’angle de gauche d’un bibelot tunisien qui vous coûte cher, vous verrez que M. Marrot va vouloir imiter son prédécesseur et orner l’angle de droite avec un bibelot marocain.

> « Il suffisait pourtant d’épouser cette marionnette de chair. »

> Les employés se rendant à leur bureau, les petites ouvrières, les garçons de magasin, s’arrêtaient, regardaient et songeaient vaguement aux gens riches qui dépensaient tant d’argent pour s’accoupler.


## Références
