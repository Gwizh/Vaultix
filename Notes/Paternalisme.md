MOC : [[Philosophie et psychologie sociale]]
Type : Note permanente
Titre : [[Paternalisme]]
Date : 2023-11-06
***

## Définitions
Pour reprendre l'exemple du pont endommagé, on pourrait imaginer plusieurs cas de figure en fonction du type de paternalisme.

Imaginons une autorité locale, typiquement la mairie, qui connaît la situation de ce pont. Cette autorité peut avoir plusieurs comportement vis-à-vis de la population. 

Premièrement, elle peut reconstruire le pont, c'est-à-dire mettre les moyens nécessaires pour que le problème n'en soit plus un. Par rapport à la première définition, ce n'est du paternalisme que lors de la période de travaux, il est momentanément impossible d'accéder au pont. Pourtant, la troisième définition nous fait voir ce cas sans paternalisme : aucun jugement n'est porté sur les individus.

Si la mairie ne peut (ou ne veut) pas engager les fonds nécessaires à la reconstruction, elle a plusieurs façons de voir les choses :
- Ne rien faire, laisser le lieu dangereux tel quel

Si le paternalisme est 
## Idées
[[La baguette est un exemple de paternalisme]]

## Références 
[[@Paternalisme libéral dossier,]]

## Mises en lien
[[Autoritarisme]] : [[Le paternalisme est un autoritarisme qui se veut bienveillant]]
[[Autonomie]] : [[Nous n'avons pas besoin de guides]]


