MOC : 
Source : [[Loi Méline]]
Date : 2023-06-09
***

## Résumé
La loi Méline est une loi française qui a créé un double tarif douanier. Votée le 11 janvier 1892, elle est devenue un symbole du retour du [[Protectionnisme]] en France à la fin du xixe siècle.

Contexte
Si les pays européens sont principalement protectionnistes au XIXe siècle, un mouvement de libéralisation des échanges de biens progresse à partir des années 1860. En 1860 est signé le traité Cobden-Chevalier, qui abolit les droits de douane entre la France et le Royaume-Uni.

Toutefois, les années 1870 voient un durcissement des politiques commerciales en Europe. Les tarifs douaniers augmentent déjà dès 1881, en passant de 6,5 % à 7,1 %.

C'est dans ce contexte que [[Jules Méline]], président de la commission des douanes de la Chambre des députés, propose à la Chambre une loi protectionniste. Méline, très lié au lobby agricole, est également proche des filateurs français. La crise agricole des années 1880, caractérisée par une baisse des prix du fait de la concurrence internationale, le pousse à écrire la loi.

## Référence
[[Agriculture]]

## Liens 