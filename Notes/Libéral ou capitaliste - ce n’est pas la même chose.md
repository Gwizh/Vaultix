MOC : 
Titre : [[Libéral ou capitaliste - ce n’est pas la même chose]]
Date : 2023-08-23
***

## Résumé
Le [[Capitalisme]] défend l’idée que l’efficience économique est fondée sur l’accumulation du capital (machines, ordinateurs, robots, etc.), la division du travail et la spécialisation des travailleurs. La manufacture d’épingles d’[[Adam Smith]] dans De la richesse des nations en est l’archétype. S’il est plus efficient d’accroître la taille des unités de production (jusqu’à un certain point où les rendements marginaux diminuent), c’est parce que l’effet de taille conduit à un abaissement des coûts de production et/ou une augmentation de la productivité, ce sont les rendements d’échelle. Ces derniers résultent de l’accumulation du capital, c’est-à-dire du fait que la productivité globale d’une entreprise est supérieure à la somme des productivités individuelles de ses employés s’ils devaient s’acquitter séparément de leurs tâches.

Ainsi, le [[Communisme]] tel que pratiqué dans l’[[URSS]] était un capitalisme d’Etat, n’en déplaise à certains. Le mécanisme était d’accumuler des moyens de production pour obtenir des rendements d’échelle. Bien évidemment, une différence essentielle tenait dans la propriété des moyens de production –étatique pour le communisme, privée pour le capitalisme– ainsi que dans les buts généraux du régime politique dans lequel cet arrangement productif s’insérait. Mais le mécanisme de base était le même.

De son côté, le [[Libéralisme]] repose sur l’idée que l’efficience économique découle de l’échange libre entre des agents. Ces derniers peuvent entrer et sortir sans contrainte du marché, possèdent un pouvoir de marché faible (c'est-à-dire qu’ils sont incapables de déterminer les prix), ils ont une connaissance parfaite des prix, etc. (la fameuse compétition «pure et parfaite»). Ces conditions peuvent être considérées comme théoriques, voire utopiques (ce qu’elles sont), elles n’en remplissent pas moins la fonction d’idéal pour tout libéral économique qui se respecte.

Le mécanisme au cœur du libéralisme économique est l’échange mu par deux types de différences entre agents: des différences de préférences (je préfère les bananes aux pommes, vous préférez les pommes aux bananes, on a donc intérêt à échanger) et des différences de «dotations initiales» (j’ai des chaussures, vous avez des pantalons, à moins de me promener en caleçon et vous pieds nus, on a tout intérêt à échanger).

Capitalisme et libéralisme peuvent toujours se combiner dans les discours politiques et réalités économiques. Mais, en bout de ligne, libéralisme et capitalisme désignent deux mécanismes coopératifs (échange vs économie d’échelle) et deux systèmes théoriques distincts. Plus que cela, ces deux systèmes entrent souvent en conflit, car ils ne justifient pas les mêmes mécanismes économiques, politiques publiques et ne s’appuient pas sur les mêmes valeurs.
## Références

## Liens 