MOC : 
Source : [[Rébellions de 1837 et 1838 au Canada]]
Date : 2023-07-21
***

## Résumé
Les rébellions de 1837 et 1838 au [[Canada]] est une période d'insurrection contre le pouvoir impérial dans les colonies canadiennes de l'Amérique du Nord britannique. Elles regroupent deux soulèvements armés distincts s'étant déroulés dans les provinces britanniques du Bas-Canada et du Haut-Canada en 1837 et 1838.

Ces soulèvements, qui se déroulent sur fond de crises économiques et agricoles, sont provoquées par le refus, de la part des autorités britanniques coloniales, d'accorder les réformes politiques réclamées par les assemblées législatives des provinces, notamment l'établissement d'un gouvernement responsable. Dans le Bas-Canada, ce conflit se double d'une volonté d'émancipation de la majorité francophone face à la domination politique et économique que lui inflige la minorité britannique.

## Référence

## Liens 
[[Colonialisme]]
[[France]]
[[Royaume-Uni]]
