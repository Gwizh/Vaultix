MOC : 
Titre : [[Semaines sociales]]
Date : 2023-09-25
***

## Résumé
Les Semaines sociales de France (SSF) sont un observatoire de la vie sociale et un lieu de réflexion. L'association veut être un espace de rencontres, de formation et de débat  pour l’ensemble des acteurs qui, par leur action et leur réflexion, cherchent à contribuer au bien commun en s’appuyant sur la pensée sociale chrétienne. Elle est surtout connue par la session qu'elle organise chaque année au mois de novembre et qui rassemble chaque année des centaines de personnes.

Les Semaines sociales de France ont été créées par deux laïcs catholiques en **1904**, le Lyonnais Marius Gonin et le Lillois Adéodat Boissard, dans le mouvement suscité par la réception de Rerum Novarum, l'encyclique de Léon XIII considérée comme fondatrice de la doctrine sociale de l'Église catholique.

Les Semaines sociales sont présidées depuis juin 2022 par Isabelle de Gaulmyn, rédactrice en chef à La Croix1, succédant ainsi à Dominique Quinio. 


Conservateurs libéraux ???

## Références

## Liens 