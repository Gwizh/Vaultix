***
Opposition entre le [[Communisme]] et l'[[Anarchie]]

C'était à l'époque l'AIT, elle a été fondée en 1864 à Londres. 

Malgré les répressions gouvernementales, elle connaît un succès rapide et se constitue en sections nationales dans plusieurs pays dont la Suisse, la Belgique, la France, l'Allemagne et, à partir de 1867, l'Italie, l'Espagne, les Pays-Bas, l'Autriche ou les États-Unis.

En 1869, un débat divise l'AIT entre partisans de [[Karl Marx]], favorables à la gestion centralisée de l'association et à la création de partis politiques, et les « anti-autoritaires » réunis autour de l'anarchiste [[Mikhaïl Bakounine]].

Elle disparaît en 1876 et réapparaît en 1889 avec la [[Deuxième Internationale]]