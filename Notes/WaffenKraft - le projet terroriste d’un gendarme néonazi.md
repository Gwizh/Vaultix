MOC : 
Source : [[WaffenKraft - le projet terroriste d’un gendarme néonazi]]
Date : 2023-05-12
***

## Résumé
Quatre personnes, dont un gendarme, comparaîtront devant la cour d’assises de Paris, du 19 au 30 juin, pour des projets d’attentats liés à l’[[Extrême droite]] : une première en France. Une affaire jamais médiatisée qui relance le débat sur l’infiltration des services de sécurité. Révélations.

« S_i vous en êtes à lire ceci, c’est probablement que vous avez décidé, comme moi et d’autres, de prendre part aux opérations violentes. »_ C’est par ces mots qu’Alexandre G., gendarme adjoint volontaire de 22 ans, commence son premier manifeste. Intitulé « Tactiques et opérations de guérilla »_,_ son document, qu’il définit devant les enquêteurs comme un _« manifeste terroriste comme Daech ou Al-Qaida »,_ ressemble plutôt aux écrits d’[[Anders Breivik]], **terroriste** d’extrême droite auteur des attentats d’Oslo et d’Utøya en 2011, qui ont tué 77 personnes.

Il est structuré en plusieurs chapitres et détaille les méthodes d’action : attaques en véhicule, à l’arme blanche, fusillades, explosions ou _« attaques multiples »._ L’auteur évoque même un attentat suicide, plus efficace pour faire _« un bien plus grand carnage »._ Pour le jeune militaire, _« une attaque réussie_, _c’est 50 morts »_.

Un groupe de discussion sur Discord concentre son attention : le « projet WaffenKraft » – puissance de feu en allemand. Là, un petit groupe prépare des actions violentes qui se muent, sous l’influence du gendarme, en projets d’attentats.

Adepte de la violence, il aime se battre et pratique les arts martiaux mixtes (MMA) dans un club. Il crée, sur Discord puis sur Wire – réseaux sociaux très sécurisés – deux groupes de discussion. Le premier, « Formation oiseau noir », permet de trier les personnes qui, après avoir répondu à un questionnaire, peuvent intégrer un second groupe, plus confidentiel : le « projet WaffenKraft ».

Julien trouve ce terme dans _Siege_, le livre de James Mason, idéologue du groupe néonazi [[Atomwaffen Division]], classé par les autorités états-uniennes sur la liste des organisations terroristes. Sa propagande innerve de nombreux dossiers terroristes en France. Julien s’en imprègne et en fait part à son nouvel ami, qui épouse leur vision accélérationniste – mouvement qui prône la précipitation d’une guerre raciale. Une affiche appelant au [« white Jihad » (Jihad blanc)](https://www.politis.fr/articles/2022/03/jihad-blanc-la-menace-hybride-44222/) est retrouvée dans le téléphone du gendarme.

_« Les juifs sont une clique de parasites, ce sont nos ennemis mortels », « les homosexuels, il faut les éliminer »_, lui écrit un candidat sous le pseudonyme « Panzer Führer ». Les enquêteurs soupçonnent Gauthier F., alias « Panzer » ou « Panzer Granata », du fait de la proximité des pseudonymes. Celui-ci conteste.

Gauthier F., étudiant en ingénierie, est recruté début 2018. Lors du week-end d’entraînement, il achète au gendarme son fusil à pompe. Il rencontre Julien plusieurs fois. Ensemble, ils évoquent la possibilité de braquer des Arabes ou d’incendier les bureaux de la Ligue internationale contre le racisme et l’anti­sémitisme ([[Licra]]). Le père de Gauthier est lieutenant-colonel dans l’armée de l’air – aujourd’hui à la retraite.

Dans un second manifeste intitulé « Reconquista Europa – opération croisée – communiqué de guerre », flanqué du symbole nazi du soleil noir, le militaire l’annonce : _« Le vendredi 13 novembre, une opération spéciale de représailles va être appliquée pour venger les victimes des attentats islamiques_ [sic]_. Le Templier utilisera fusils d’assaut, explosifs et n’aura absolument aucune pitié contre ces chiens qui détruisent l’Europe. »_ Se revendiquant d’un « _nationalisme encore plus violent que celui de Hitler »,_ il met dans son viseur les communautés musulmane et juive, mais aussi _«_ _les traîtres marxistes communistes »._

D’après Évandre A., plus l’année 2018 avance, plus ses **cibles** sont **précises**. Dès février, Alexandre G. se renseigne sur la mosquée Omar à Paris, la mosquée Ibn Al-Khattab à Creil ou encore la mosquée de Tours. Il évoque aussi le concert de [Médine](https://www.politis.fr/articles/2023/03/medine-dockers-assistantes-maternelles-infirmieres-eboueurs-on-partage-la-meme-douleur/) au Bataclan, note les dates des dîners du Conseil représentatif des institutions juives de France (Crif) ainsi que celles des meetings de [[Jean-Luc Mélenchon]], pour lesquels ==il imagine un scénario précis d’attaque par un sniper dissimulé dans le coffre d’une voiture.== _« Il se voyait comme en concurrence avec les jihadistes et voulait faire mieux qu’eux,_ témoigne Évandre A. _Pour lui, le [[Bataclan]] n’avait pas un ratio terroristes-morts assez important. Il voulait faire pire. »_

Apparaissant parmi les cibles potentielles dans l’affaire « Waffenkraft » – projets d’attentats ourdis par un groupe de jeunes néonazis menés par un gendarme révélés hier par _Politis_ – Jean Luc Mélenchon souhaite se constituer partie civile. Et s’agace de ne pas avoir été prévenu des risques encourus.

## Référence

## Liens 
https://www.politis.fr/articles/2023/05/waffenkraft-le-projet-terroriste-dun-gendarme-neonazi/