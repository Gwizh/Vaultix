***
Du site directement :

### Our Organization and Mission

_Truthout_ is a nonprofit news organization dedicated to providing independent reporting and commentary on a diverse range of social justice issues. Since our founding in 2001, we have anchored our work in principles of accuracy, transparency, and independence from the influence of corporate and political forces.

A lire : https://truthout.org/articles/beyond-the-economic-chaos-of-coronavirus-is-a-global-war-economy/
