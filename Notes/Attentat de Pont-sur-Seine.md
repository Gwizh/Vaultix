[[Note Permanente]]
MOC : [[Histoire]]
Titre : [[Attentat de Pont-sur-Seine]]
Date : 2022-07-11
***

## Résumé
L'attentat de Pont-sur-Seine est une tentative d'assassinat de [[Charles de Gaulle]], alors président de la République, par un groupuscule se réclamant de l'[[OAS]] qui a eu lieu le 8 septembre 1961 à Pont-sur-Seine, dans l'Aube. Alors que la voiture présidentielle, conduite par Francis Marroux, traverse la commune, une explosion est déclenchée manuellement à la hauteur du véhicule. L'humidité ayant diminué la puissance des explosifs et neutralisé une partie du dispositif, l'attentat ne fait aucun mort ou blessé. Ses auteurs sont condamnés à de la prison au cours d'un procès très médiatisé et qui eut lieu à Troyes moins d'un an après.

Aujourd'hui, il semble acquis que le cerveau de l'attentat, désigné par le pseudonyme « Germain », ait été Jean-Marie Bastien-Thiry, condamné à mort en 1963 et fusillé pour avoir organisé l'attentat du Petit-Clamart.

## Référence
[[Histoire de la France au XXe Siècle - Livre 3]]

## Liens 
[[Histoire de la France]]