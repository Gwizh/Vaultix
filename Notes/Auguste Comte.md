MOC : 
Source : [[Auguste Comte]]
Date : 2023-07-07
***

## Résumé
Auguste Comte, né Isidore Marie Auguste François Xavier Comte le 19 janvier 1798 (30 nivôse an VI) à Montpellier (Hérault) et mort le 5 septembre 1857 à Paris, est un philosophe et sociologue français, fondateur du positivisme. 

##### Suffrage universel est une maladie morale
Comte considérait que la société devait être organisée de manière hiérarchique, avec des experts et des scientifiques à sa tête, pour guider le progrès et le bien-être de l'humanité. Il estimait que les décisions politiques devraient être prises par des spécialistes qualifiés, plutôt que par la volonté populaire. Comte pensait que le suffrage universel, en permettant à tous les citoyens de participer au processus démocratique, risquait d'amener des décisions irrationnelles ou de conduire à des divisions et à des luttes de pouvoir.

## Référence

## Liens 