MOC : 
Source : [[György Dózsa]]
Date : 2023-06-08
***

## Résumé
György Dózsa de Makfalva (en hongrois : Makfalvi Dózsa György ; roumain : Gheorghe Doja din Ghindari ; allemand : Georg Dózsa von Makfalva ; latin : Georgius Dosa Siculus de Makfalva), né vers 1470 à Dálnok/Dalnic et exécuté en juillet 1514 à Temesvár/Timișoara, est le chef sicule d’une grande jacquerie paysanne en 1514, en Transylvanie. 


## Référence

## Liens 