MOC : 
Source : [[Coup d'État de 1980 en Turquie]]
Date : 2023-07-19
***

## Résumé
Un coup d'État militaire se produit en Turquie le 12 septembre 1980. Les Forces armées turques, dirigées par leur chef d'État-major [[Kenan Evren]], s'emparent du pouvoir et mettent en place un régime politique autoritaire.

## Référence

## Liens 