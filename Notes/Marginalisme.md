MOC : 
Source : [[Marginalisme]]
Date : 2023-07-03
***

## Résumé
Le marginalisme est un mouvement intellectuel économique qui promeut une méthode d'analyse économique basée sur le raisonnement à la marge, résultant de « l'utilité marginale » (l'utilité de la dernière unité consommée). Il façonne le courant de l'école néoclassique à la fin du XIXe siècle.

Cette théorie résulte de travaux menés dans la seconde moitié du XIXe siècle de façon indépendante par des chercheurs comme [[Léon Walras]], [[Carl Menger]] et [[William Stanley Jevons]]. Surnommée « révolution marginaliste », elle contribue fortement à la formation des écoles néoclassique et autrichienne. 


## Référence

## Liens 
[[École néoclassique]]