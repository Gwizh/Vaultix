MOC : 
Source : [[Colonialisme]]
Date : 2023-07-16
***

## Résumé
Le colonialisme est une doctrine ou une idéologie justifiant la colonisation entendue comme l'extension de la souveraineté d'un État sur des territoires situés en dehors de ses frontières nationales. La notion intellectuelle du colonialisme est cependant souvent confondue avec la pratique même de la colonisation étant donné que l'extension de sa souveraineté par un État implique dans les deux cas la domination politique et l'exploitation économique du territoire annexé. 

## Référence

## Liens 