MOC : 
Source : [[Parti communiste des États-Unis d'Amérique]]
Date : 2023-06-18
***

Comme la plupart des partis communistes du XXème siècle, étaient des sous-divisions du parti de l'[[URSS]], sous les ordres de [[Joseph Staline]]. Considérait les socialistes comme des fascistes et des traîtres. ^c2285c

## Résumé
Le Parti communiste des États-Unis d'Amérique (en anglais : Communist Party of the United States of America), généralement mentionné comme Parti communiste USA (en anglais : Communist Party USA, ou CPUSA), est un parti politique américain.

Créé en 1919, de tendance [[Marxiste-léninisme]], le parti s'aligna durant la période du stalinisme et de la guerre froide sur la politique de l'Union soviétique. De 1959 à 2000, il fut dirigé par Gus Hall. Depuis 2019, ses deux co-présidents sont Rossana Cambron et Joe Sims.

## Référence

## Liens 