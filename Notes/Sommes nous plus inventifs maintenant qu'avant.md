MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081614
Tags : #Fini
***

# Sommes nous plus inventifs maintenant qu'avant
p252
C'est une question difficile car on n'a pas toujours défini une invention de la même manière. Et même ainsi, nous n'avons pas bcp plus de brevets par an maintenant qu'avant. De plus, cela change nationalement (Japon et Corée du Sud font des brevets pour tout et rien) et cela ne veut pas dire que toutes les inventions sont aussi utiles les unes que les autres. Est-ce que la somme de toutes les inventions de l'année 2010 est plus impressionnante que celle de 1910 ? On ne peut pas vraiment dire.
## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]