MOC : 
Source : [[Disco Elysium]]
Date : 2023-04-14
***

GROSSE CLAQUE

J'ai envie de refaire le détail par jours pour me souvenir de ma première partie
Build: 5412 avec Conceptualisation

Jour 1 :
Je suis allé voir le corps, je suis allé dans la planque de Cuno mais je n'ai pas pu aller plus loin.
La grosse claque est arrivé à peu près là, les intéractions avec les différentes intelligences m'ont fascinées. 
J'ai immédiatement eu peur de l'argent dans ce jeu. Finalement ça va
J'ai adoré le sous-texte avec la grève, évidemment tout n'est ni blanc ni noir mais enfin les termes sont dit quoi, on parle des grèves, des syndicats, du communisme, etc...
Des skills je dois dire que la projection analytique est la plus trippantes pour l'instant. La conceptualisation est fun de temps en temps pour comprendre des petits trucs en plus. La perception est méga importante. Ouais donc en gros ce sont les trucs que j'ai max au début.

Jour 2
J'ai passé tout le matin à visiter l'appartement abandonné en gros
Après j'ai commencé à douter que j'allais pouvoir remplir le temps, j'avais plus rien à faire. genre à 14 15h
En gros, j'attendais 21h pour pouvoir parler au fumeur et le lendemain pour pouvoir visiter le reste
J'ai blessé le gars juste devant omg je m'en veux trop alors que c'était mon reuf
Pareil j'ai intériorisé le truc des races, ça m'a fait bcp de mal mais je crois pas qu'on puisse passer sans avec le build que j'avais
Donc finalement ça m'a permis de bien avancer : 21heures, jétais en pleine autopsie et à 22 heures je suis allé voir le fumeur tout seul

Jour 3
Je suis immédiatement allé à l'ouest.
C'était immense, deuxième claque en gros. C'est beaucoup plus vide je dirais mais en fait ça crée un espace tellement fou. La centrale, la glace, l'église olala. La pauvreté en fait... Le silence.
Le moment de la balançoire m'a fait bizarre aussi, c'était un moment contemplatif d'une beauté rare <3
Trop drôle le soir après que Kim soit parti, je suis allé seul peindre le mur mais je me suis rendu compte que j'avais vendu le bidon. Je me suis senti tellement con, j'étais en phase avec le jeu en fait.

Jour 4
Communisme mdr, il faut que je trouve le mot de passe apparemment.
Je me demande combien de temps ça va durer.
Ok donc j'ai pu faire preuve d'autorité avec Titus et boom ça m'a envoyé en haut et wow, quelle histoire je suis chokbar. Of course je suis pas avec Titus, il ne comprend rien aux VSS. Miss Oranje est un super perso, j'ai essayé de ne pas la pousser à bout mais franchement c'est super qu'ils parlent de ça dans un jeu comme ça. 
Par contre faut que je parle d'Evrat là c'est dingue. Le syndicat il propose une super bonne idée : que tout le monde devienne actionnaire, ce n'est pas con comme truc, c'est même le mieux qu'on puisse faire dans le capitalisme non ? En faisant ça, il se met dans la full sauce par contre et il l'assume ! Il compte butter le capital avec son méga syndicat de bz. 2700 gars je crois pour une guerre contre des mercenaires ? Dingue. Et il a tout prévu, pour éponger les pertes du fait de l'abandon de certains clients, il va partir en full prod de drogue/médoc. C'est moralement gris et ça fait vraiment mafia pour le coup. Mais s'il arrive à seize les moyens de prod, c'est banger. Le problème c'est que c'est un port mondial, forcément ça dépend des flux de commerces et donc tu peux pas en faire une utopie communiste. C'est donc selon moi un projet assez mauvais mais compréhensible vu que c'était jusque là le nerf de la ville. Par conte il veut acheter les territoires du petit village pour y construire des trucs, il a une logique impérialiste. Le truc c'est que ça marche quand même pour eux, j'ai l'impression que ça améliore leur niveau de vie mais comment savoir ?
Mdr je parle avec le vent quelle galère. Ce jeu est bien fait j'ai pas eu besoin de refaire le jet 100x malgré le handicap de mon perso mdr
Jpp de la machine à glace j'ai pas la force pour faut juste le build pour ou quoi ? Pour le coup c'est chiant là xD

Prochains builds (car oui j'aime tellement que je me vois jouer 500 heures au jeu s'il le faut):
1254 avec Affinité mécanique. Je veux max endurance, res douleur, autorité, h/e coord --> Je veux tout casser, faire des trucs que j'ai jms fait 
2522 avec Empathie. Esprit de Corps, Frisson, Clair obscure, cour intérieur, suggestion, perception --> Voir si c'est possible
HARDCORE : Quelle strat ? Je vois bien 4224 non ? C'est le plus opti ? On peut se faire masse de blé avec l'encyclopédie. La motricité pour rusher le port