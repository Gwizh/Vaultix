MOC : 
Source : [[Déclaration des Nations unies sur les droits des paysans et des autres personnes travaillant dans les zones rurales ou UNDROP]]
Date : 2023-08-09
***

## Résumé
Le concept de droit des paysans se base, et complète, les droits des agriculteurs, déjà reconnu dans le Traité international sur les ressources phytogénétiques pour l'alimentation et l'agriculture ou la Convention sur la diversité biologique, ainsi que les droits des peuples autochtones déjà reconnus en 2006 dans la Déclaration des Nations Unies sur les droits des peuples autochtones. L'élaboration d'un texte sur les droits des paysans cherche à recouvrir une série de revendications paysannes exprimées pendant des décennies, soutenues par des organisations comme le CETIM ou FIAN International, et compilées en 2008 par l'ONG la Via Campesina dans sa Déclaration des droits des paysans – Femmes et hommes.

## Référence

## Liens 
[[Paysan]]
[[Agriculture]]