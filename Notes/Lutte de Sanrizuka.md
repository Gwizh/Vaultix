MOC : 
Source : [[Lutte de Sanrizuka]]
Date : 2023-07-23
***

## Résumé
La lutte de Sanrizuka (三里塚闘争?, Sanrizuka tōsō) fait référence à un conflit civil impliquant le gouvernement japonais et la communauté agricole de Sanrizuka, comprenant une opposition organisée d'agriculteurs, de résidents locaux et de groupes de gauche à la construction de l'aéroport international de Narita (alors Nouvel aéroport international de Tokyo). La lutte est née de la décision du gouvernement de construire l'aéroport de Sanrizuka sans la participation ou le consentement de la plupart des résidents de la région. Cette lutte a commencé vers les années 1960 et se poursuit encore aujourd'hui. 

## Référence

## Liens 
[[Japon]]