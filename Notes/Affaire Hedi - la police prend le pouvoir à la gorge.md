MOC : 
Source : [[Affaire Hedi - la police prend le pouvoir à la gorge]]
Date : 2023-07-29
***

## Résumé
Visé par un tir de LBD et tabassé le 1er juillet, Hedi n’a toujours pas reçu le moindre coup de fil du gouvernement. Les syndicats de police, eux, ont été accueillis avec les honneurs au ministère de l’intérieur. Ils font « pression » pour obtenir un statut juridique à part. 

Qui a dit que l’exécutif méprisait le dialogue social ? La rencontre entre le ministre de l’intérieur et les organisations professionnelles de la police, jeudi 27 juillet, avait de quoi faire pâlir de jalousie les autres membres du gouvernement. [[Gérald Darmanin]] a été « à l’écoute », « proche de ses troupes », « ouvert » et « conscient des attentes », ont salué à l’unisson les représentants des fonctionnaires de police, réunis place Beauvau. « Je veux assurer les policiers de toute ma reconnaissance et de toute ma confiance », a dit le ministre.  

Plus étonnant encore, ni [[Élisabeth Borne]] ni [[Emmanuel Macron]] n’ont adressé le moindre mot d’empathie à l’égard du jeune homme. Contactés, les conseillers presse de la première ministre et du président de la République n’avaient pas répondu à l’heure de publication de cet article. En déplacement à Marseille jeudi et vendredi, la nouvelle secrétaire d’État à la ville, Sabrina Agresti-Roubache, n’en a pas profité pour aller voir Hedi ou sa famille, ni pour leur adresser le moindre message de soutien.

Vendredi, lors d’un déplacement, [[Éric Dupond-Moretti ]]a rappelé que la justice _« a besoin, comme les policiers, de respect, elle a besoin d’indépendance, elle a besoin qu’on la laisse travailler »_. _« La justice ne se rend pas dans la rue et ne se rend pas sur les plateaux de télévision »_, a-t-il ajouté. Mais le principe est acté d’une rencontre, en septembre, avec Gérald Darmanin et les syndicats de policiers, pour évoquer leurs desiderata législatifs. _« Moi je dis merci_ [aux policiers], a insisté, vendredi, le garde des Sceaux. _Merci pour ce qu’ils ont fait, merci pour leur engagement, pour leur courage. »_

Conscients que le rapport de force est à leur avantage, soutenus par leur hiérarchie et leur ministre, les syndicats de police ne comptent pas s’arrêter là. _« Nous demandons que le policier soupçonné d’avoir commis une infraction dans l’exercice de ses fonctions reste en liberté et puisse percevoir l’intégralité de son salaire, primes comprises, tant qu’il n’a pas été jugé_, formule Éric Henry, d’[[Alliance]]. _Nous souhaitons aussi que l’interdiction d’exercer son métier ne s’applique qu’à la voie publique pendant la durée de la procédure judiciaire. »_

## Référence

## Liens 