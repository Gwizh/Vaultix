MOC : 
Source : [[L’inavouable défaite française au Sahel]]
Date : 2023-08-06
***
Monde Diplomatique : https://www.monde-diplomatique.fr/2022/04/MIELCAREK/64534
## Résumé
Accueillis en héros en janvier 2013 après avoir arrêté une offensive djihadiste partie du nord du pays, les soldats français ne sont plus les bienvenus au [[Mali]]. La junte au pouvoir révise ses alliances en se rapprochant notamment de la [[Russie]], et oblige la [[France]] à repositionner ses troupes dans la région. Sur le terrain, l’absence de progrès économiques se fait sentir.

### Intro
Depuis janvier 2013, ces forces regroupées sous les bannières successives des opérations « Serval » puis « Barkhane » avaient pour mission de lutter contre des mouvements politico-militaires disparates, se revendiquant pour les uns d’[[Al-Qaida]], et pour les autres de l’Organisation de l’État islamique (OEI). Au fil des années, les Français sont parvenus à mobiliser à leurs côtés des contingents de plusieurs États européens, dont les forces spéciales envoyées par une dizaine de pays au sein de l’opération « Takuba » ; ainsi que des bataillons africains au sein de la force conjointe du G5 Sahel ([[Mali]], [[Tchad]], [[Niger]], [[Mauritanie]], [[Burkina Faso]]).

Huit ans plus tard, au moins 2 800 djihadistes auraient été abattus au cours d’opérations menées par les Français, qui y ont laissé 58 hommes. Un bilan à comparer toutefois avec le total des pertes subies par les armées africaines : probablement des centaines de morts et de blessés. En l’absence de décompte officiel, leur nombre exact reste inconnu. Les victimes civiles dans les pays concernés se compteraient, elles, par milliers.

Depuis le coup d’État d’août 2020, qui a vu tomber le président [[Ibrahim Boubacar Keïta]], proche de la France, le Mali s’enfonce dans une crise politique profonde ([3](https://www.monde-diplomatique.fr/2022/04/MIELCAREK/64534#nb3)). Malgré ce premier coup de tonnerre, « [[Barkhane]] » était parvenue à continuer de travailler avec les autorités dites de la transition. Les opérations avaient été maintenues et la coopération avec les forces armées maliennes s’était poursuivie. La situation toutefois s’est dégradée lorsque le colonel [[Assimi Goïta]], vice-président du gouvernement intérimaire, organise un nouveau putsch pour accaparer la totalité du pouvoir en août 2021. L’officier suspend alors la préparation des élections prévues pour février 2022. Cet homme discret, dont les objectifs restent mystérieux, pousse sur le devant de la scène son premier ministre, M. [[Choguel Kokalla Maïga]], et son ministre des affaires étrangères [[Abdoulaye Diop]], deux voix hostiles envers Paris.

### Au Mali, « la junte souhaite en découdre »
La Communauté économique des États d’Afrique de l’Ouest ([[Cedeao]]) dénonce ce coup d’arrêt infligé aux efforts démocratiques et impose au pays un blocus financier. Prenant ses distances avec ses alliés européens, et notamment avec la France, la junte remet en question les accords de défense qui permettaient aux militaires français de circuler librement dans le pays. La présence des mercenaires du groupe russe paramilitaire [[Wagner]], connu pour ses exactions envers les populations, notamment en [[République centrafricaine]], est d’abord niée par la junte, avant d’être assumée — des « chiens de guerre » avec lesquels les Européens ne veulent pas coopérer.

Sur le terrain, plus rien ne fonctionne. Le 24 janvier 2022, Bamako inflige un nouveau camouflet à la France, qui a beaucoup misé sur le soutien de « [[Takuba]] », en décidant que les forces spéciales danoises fraîchement arrivées à Gao ne sont finalement pas les bienvenues. Le ministre des affaires étrangères [[Jean-Yves Le Drian]] dénonce l’« illégitimité » d’une junte « irresponsable » (Assemblée nationale, 1er février 2022). Le 28, sur Radio France internationale, le chef de la diplomatie malienne exige plus de « respect » de la part de son ancien allié. Le 31 janvier, le gouvernement malien expulse l’ambassadeur de France.

## Référence

## Liens 