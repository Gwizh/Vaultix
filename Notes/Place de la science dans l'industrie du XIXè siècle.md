MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311080949
Tags : #Fini
***

# Place de la science dans l'industrie du XIXè siècle
« Dans les histoires du XIXe siècle industriel, la science occupe en effet une place de choix. » (Chauveau, 2014, p. 4)   
Les savants et inventeurs s'associent de plus en plus avec les industriels et entrepreneurs pour développer de nouvelles techniques.
## Référence
[[@Science, industrie, innovation et société au XIX _sup_e__sup_ siècle,Sophie Chauveau]]
