MOC : 
Source : [[Ravachol]]
Date : 2023-07-07
***

## Résumé
François Claudius Koënigstein dit Ravachol, le « Rocambole de l'anarchisme », est un ouvrier, militant anarchiste et criminel français, né le 14 octobre 1859 à Saint-Chamond. S'étant rendu coupable de plusieurs délits, assassinats et attentats, il est guillotiné le 11 juillet 1892 à Montbrison. 

Admiré à l'époque par les anarchistes par son "humanisme" envers les victimes.

## Référence

## Liens 
[[Anarchie]]