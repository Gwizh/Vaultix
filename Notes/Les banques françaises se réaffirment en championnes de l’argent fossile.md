MOC : 
Source : [[Les banques françaises se réaffirment en championnes de l’argent fossile]]
Date : 2023-04-13
***

## Résumé
Selon le nouveau rapport « Banking on Climate Chaos », une étude mondiale annuelle sur le financement des énergies fossiles, le [[Crédit agricole]], [[BNP Paribas]] et la [[Société générale]] sont les principaux soutiens européens aux plus gros industriels pétro-gaziers, avec 15 milliards de dollars injectés dans le secteur en 2022.

1 331 milliards de dollars. C’est la somme gigantesque qu’ont versée les banques européennes aux industriels des énergies fossiles depuis l’adoption de l’accord de Paris sur le climat, en décembre 2015. Autant de milliards à rebours de l’urgence climatique qui ont financé l’expansion pétrolière et gazière.

Ce chiffre terrible est tiré du nouveau rapport « [[Banking on Climate Chaos]] » publié ce 13 avril. Chaque année, des organisations écologistes telles que Oil Change International, Reclaim Finance ou encore Urgewald analysent à l’échelle mondiale les activités bancaires liées aux énergies fossiles.

La France s’illustre tout particulièrement dans ce classement des banques les plus sales au monde. Les géants pétroliers [[TotalEnergies]], BP et Eni ont reçu collectivement en 2022 près de 12 milliards de dollars de la part des établissements bancaires tricolores.

Et parmi les 16 banques mondiales qui ont augmenté leurs financements aux énergies fossiles entre 2021 et 2022, deux d’entre elles sont françaises : BNP Paribas et Crédit agricole.

Enfin, toujours en 2022, les trois principaux soutiens européens aux plus gros développeurs d’énergies fossiles étaient le Crédit agricole (6,1 milliards de dollars), BNP Paribas (5,5 milliards de dollars) et la Société générale (3,4 milliards de dollars).

## Référence

## Liens 