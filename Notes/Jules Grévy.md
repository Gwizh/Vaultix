[[Note Permanente]]
MOC : [[Histoire]] [[POLITIQUE]]
Titre : [[Jules Grévy]]
Date : 2022-07-27
***

## Résumé
Jules Grévy, né le 15 août 1807 à Mont-sous-Vaudrey (Jura) et mort le 9 septembre 1891 dans la même commune, est un avocat et homme d'État français, **président de la République française du 30 janvier 1879 au 2 décembre 1887.** 

En 1879, il devient le premier président de la République française issu des rangs républicains. Réélu en 1885, il est contraint à la démission en raison du scandale des décorations impliquant son gendre, Daniel Wilson. 

##### Faits importants
Il remplace le président [[Patrice de Mac Mahon]] aux suites de la [[Crise du 16 mai 1877]] et des [[Élections législatives françaises de 1877]] qui ont vu une grande percée du mouvement républicain.

En bon républicain, il choisit d'augmenter la parlementarisation de la politique française en renonçant au droit de dissolution. Toujours dans cette idée de continuité avec la révolution, il va inscrire comme symboles le 14 juillet et La Marseillaise. 

Il participe aux Lois Ferry et à la construction d'écoles pour filles. 

Il était contre l'avis de son gouvernement, notamment [[Jules Ferry]] et [[Léon Gambetta]] d'étendre les colonies. Il renverra ce dernier, ce qui lui vaudra des critiques. De plus, il est pour la paix et s'oppose au boulangerisme et au revanchisme.

« Grévy n'était pas un homme de premier plan mais il exerça une influence certaine quoique discrète. Malgré son autoritarisme il accepta de laisser gouverner ceux qu'il avait choisis. Mais il évita de confier le soin de former le gouvernement à une personnalité trop forte qui aurait pu lui porter ombrage ».

Il est réélu facilement aux élections de 1985 


## Référence

## Liens 

