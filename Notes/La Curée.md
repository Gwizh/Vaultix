[[Livre]]
MOC : 
Titre : [[La Curée]]
Auteur : [[Emile Zola]]
Date : 2023-04-05
***

## Résumé

## Notions clés
Il y a vraiment l'idée que les lieux et l'environnement impactent les individus. La serre pleine de plantes grasses et avec de l'air chaude donne des pulsions sexuelles et que la chambre féminine et colorée donne envie de se mettre nue. D'une certaines façon, on ne peut pas se contrôler à cause de ça.

Il y a aussi tout un aspect sur le genre des personnages. Maxime est très efféminié et Renée agit souvent comme un homme. 

Je pense que c'est un livre sexiste qui voit le plaisir de la femme comme étant fondamentalement mauvais et menant à l'inceste.

## Citations

## Passages amusants

## Références
