MOC : 
Source : [[Hwang Jini]]
Date : 2023-06-10
***

## Résumé
Hwang Jini (hangul : 황진이, hanja : 黃眞伊, 1506(?) - 1560(?)) est une gisaeng qui a vécu au début du xvie siècle en Corée. Également connue sous le nom de Myeongwol (lune éclatante, 명월 ; 明月), elle est surtout célèbre pour sa poésie et pour sa maîtrise du geomungo. Elle serait née dans une famille noble de Kaesong, fille d'une épouse secondaire. 


## Référence
[[Corée du Sud]]

## Liens 