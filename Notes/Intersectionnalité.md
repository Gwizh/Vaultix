MOC : 
Source : [[Intersectionnalité]]
Date : 2023-04-07
***

## Résumé
L’intersectionnalité (de l'anglais intersectionality) ou intersectionnalisme est une notion employée en [[Sociologie]] et en réflexion politique, qui désigne la situation de personnes subissant simultanément plusieurs formes de stratification, domination ou de [[Discrimination]] dans une société. Ainsi, dans l'exemple d'une personne appartenant à une minorité ethnique et issue d'un milieu pauvre, celle-ci pourra être à la fois victime de [[Racisme]] et de mépris de classe.

Concept découvert la première fois dans [[En finir avec les violences sexistes et sexuelles]]

#MIE
J'aime vraiment l'idée d'avoir une lutte globale qui englobe autant de luttes qu'on ait de cas spécifique. Lutter, c'est englober ces luttes mais ne jamais oublier tout ce qui individuellement fait que chacun lutte avec toi. Ça explique très bien pourquoi on aura jamais une solution qui convienne à tout le monde, il faut toujours aller de l'avant. Ça explique aussi l'instabilité des mouvements, qui se fonde sur une série d'idées données qui regroupent une première population de personnes. Puis au fil du temps, il y a des divergences entre la direction initiale, et toutes les luttes internes et individuelles. On peut alors s'adapter au risque de perdre l'attrait initial ou rester sur la même ligne et se refermer dans une sorte de conservatisme.

#### Définition de [[Nous vous écrivons depuis la révolution]] 
L'intersectionnalité est un cadre théorique qui permet de comprendre comment les différentes caractéristiques sociales et identités politiques (race, genre, classe, caste, religion,...) interagissent dans le cadre des systèmes hiérarchiques d'oppression. Utiliser l'intersectionnalité permet de développer des stratégies de lutte plus riches et adaptées à la diversité des réalités sociales.
## Référence
[[Féminisme]]


## Liens 