MOC : 
Source : [[Jules Favre]]
Date : 2023-06-09
***

## Résumé
Jules Gabriel Claude Favre, né le 21 mars 1809 à Lyon et mort le 19 janvier 1880 à Versailles, est un avocat et homme politique français républicain. Il appartient à cette génération d'hommes qui a traversé le xixe siècle, jouant un rôle dans presque tous les régimes. Il cumule dans sa vie diverses activités, celle de député à l'[[Assemblée nationale]] puis au Corps législatif, celle du barreau ou encore celle de la plume : il écrit ainsi dans des journaux de la [[Monarchie de Juillet]] comme Le Précurseur et cofonde, sous le [[Second Empire]], L'électeur libre.

Il est le grand-père du philosophe Jacques Maritain.

Opposé à [[Léon Gambetta]] et à la [[Commune de Paris]]. Du coup évidemment allié de [[Adolphe Thiers]] et un des principaux contributeurs aux lois constitutionnelles de la [[IIIe République]]. 

## Référence

## Liens 