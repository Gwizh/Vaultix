[[Note Permanente]]
MOC : [[Histoire]] [[POLITIQUE]]
Source : [[Michel Debré]]
Date : 2022-07-08
***

## Résumé
Michel Debré est le premier à exercer la fonction de Premier ministre de la Ve République, du 8 janvier 1959 au 14 avril 1962. 

Avec le retour du général de Gaulle au pouvoir en 1958, il devient garde des Sceaux. Il dirige en parallèle le groupe de travail chargé de la rédaction de la [[Constitution de la Ve République]]. Nommé Premier ministre à la suite de l’élection du général de Gaulle à la présidence de la République, il démissionne trois ans plus tard après un désaccord avec celui-ci sur son projet d’élection du président de la République au suffrage universel direct. 

## Points importants
Il a été rédacteur d’un périodique extrêmement violent à l’égard de la IVe République, _Le Courrier de la colère_.



## Référence
[[Politque française]]

## Liens 