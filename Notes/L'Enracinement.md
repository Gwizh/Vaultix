MOC : 
Source : [[L'Enracinement]]
Date : 2023-05-12
***

## Résumé
L'Enracinement, Prélude à une déclaration des devoirs envers l'être humain, est un ouvrage de la philosophe française Simone Weil, publié en 1949 par Albert Camus dans la collection « Espoir » qu'il dirigeait chez Gallimard. Le manuscrit est demeuré sinon inachevé, en tout cas non révisé. Comme tous les livres de Weil, il a été publié à titre posthume. Il s'agit, avec les [[Réflexions sur les causes de la liberté et de l'oppression sociale]], de l'œuvre philosophique la plus importante de Weil.

Albert Camus y vit « l'un des livres les plus lucides, les plus élevés, les plus beaux qu'on ait écrits depuis fort longtemps sur notre civilisation ». Hannah Arendt l'a décrit quant à elle comme « l'un des ouvrages les plus intelligents et lucides sur son temps »

## Référence

## Liens 