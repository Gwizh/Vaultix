MOC : 
Source : [[Découvrir Bourdieu]]
Date : 2023-05-11
***

## Résumé 
Concept central : la violence symbolique. 

Le symbole représente autre chose que lui-même. C'est la représentation mentale, l'image que l'on se fait de ce qui est présenté. 
La violence est bien plus visible lorsqu'elle est matérielle. 
Nos actions, nos paroles, nos manières font *signe* pour autrui. Si on donne un coup de pong à quelqu'un devant témoin, j'ai une action matérielle sur le monde mais j'agis aussi sur le représentation qu'on se fait de moi (on va me craindre, me respecter, être déçu de moi, etc... )

Pour Bourdieu, la violence exercée par les hommes entre eux edt aujourd'hui largement symbolique : la domination des uns envers les autres est d'autant plus efficace et peu coûteuse qu'elle est symbolique. Cette violence dépend des signes que nous montrons et en plus de devoir constituer un capital matériel, nous devons pour vivre constituer un capital symbolique. On sait bien qu'on ne doit pas se fier aux apparences mais au final c'est inconsciemment qu'on le fait. 

On ne contrôle par notre réaction aux signes, dans la vie de tous les jours ils sont trop pratiques. On peut pourtant changer l'appréciation globale des signes, comment on les perçoit mais aussi comment les autres le perçoivent. Pour ça on peut faire des campagnes de communiation, des produits culturels, etc... Changer la façon dont les signes sont perçus. Ces signes vont loin car elles peuvent remettre en jeu notre vision de faits. En Justice, on cherche les circonstencs atténuantes, comme dans l'[[Affaire Louise Ménard]]. 

L'école est un aspect important de son œuvre. C'est cette institution qui légitime ceux qui réusissent et rendre résigné les autres. 

## Référence
La Misère du monde, Pierre Bourdieu
Leçon sur la leçon, Pierre Bourdieu
Science de la science et réflexivité, Pierre Bourdieu
Questions de sociologie, Pierre Bourdieu
Contribution à la critique de l'économie politique, Karl Marx
Lettre VII, Platon
Réponses, Pierre Bourdieu
Le Bal de Célibataires, Pierre Bourdieu
Méditations Pascaliennes, Pierre Bourdieu
La vie Psychique du pouvoir, Judith Butler
Surveiller et Punir, Michel Foucault
La Domination Masculine, Pierre Bourdieu
Ce que parler veut dire, Pierre Bourdieu
La Distinction, Pierre Bourdieu
Qu'est-ce que les Lumières ? Emmanuel Kant
Micro-violences, Simon Lemoine
Raisons pratiques, Pierre Bourdieu
Pensées, Pascal
Trois discours sur la condition des grands, Pascal
La structure des révolutions scientifiques, Thomas Kuhn
L'existentialisme est un humanisme, Jean-Paul Sartre
Trouble dans le genre, Judith Butler
Le corset invisible, Pierre Bourdieu
Choses dites, Pierre Bourdieu
La mise en scène de la vie quotidienne, Erving Goffman
L'Ordre du discours, Michel Foucault
L'archéologie du savoir, Michel Foucault
La Reproduction, Pierre Bourdieu
Les Héritiers, Pierre Bourdieu 
Les transclasses ou la non-reproduction, Chantal Jaquet
Sociologie de l'éducation, Patrick Rayou
Pour une école de l'exigence intellectuelle, Jean-Pierre Terrail
Sur la Reproduction, Louis Althusser
Entretiens, Épictete
Une classe objet, Pierre Bourdieu


## Liens 
[[Pierre Bourdieu]]