MOC : 
Titre : [[Krach asiatique - Les leçons d'une crise financière]]
Auteur : ARTE 
Date : 2022-07-26
Lien :
Fichier : 
***

## Résumé
Krach financier de 1997.
Les indicateurs donnaient l'Asie du sud-est comme très sûre financièrement dans les années 90. La croissance dans ces pays permet la construction d'infrastructures, le développement de la technologie et de l'éducation. C'était un cercle vertueux. 
En une génération, des classes populaires devenaient ingénieurs. Le FMI a salué ces initiatives au point de créer une grande confiance en ce système miracle. Elle a favorisé le libéralisme économique et la dérégulation. 

Mais cela a créé une volonté du court terme. Il y a eu un déréglement de la finance. Il y a eu un changement de philosophie. Cette même pensée causera en grande partie la [[Crise de 2008]]

Le problème venait des crédits des entreprises, qu'il fallait payer en monnaie sûre. Mais la monnaie de la Thaïlande a perdu une grande valeur du fait d'un soudain manque de confiance. Les entreprises n'ont plus pu rembourser leurs prêts qui avaient doublé. De plus, les investisseurs ont profité de cette faiblesse de la monnaie pour spéculer et en retirer une grande valeur. 

Le 2 juillet 1997, la Banque nationale Thaïlandaise laisse finalement dévaluer la monnaie, ça marque le début de la crise. L'ironie a voulu que ce soit cette année là que le FMI a organisé son siège annuel à Hong Kong. Cette crise a touché ensuite L'Indonésie, la Corée du sud. 

Ces pays ont voulu créer un [[FMI]] asiatique mais les États Unis et la Chine ont refusé. Ça leur a valu une grande frustration. 

Du 20 au 23 octobre 1997, la Bourse de Hong Kong perd un quart de sa valeur. C'était la principale place financière de l'Asie. 

Le 4 novembre 1997, le won Sud coréen perd 10%. Au final, ce sera 65%.

Cette fois-ci les États Unis agissent car la Corée du sud était l'ancien rempart contre le communisme. 

Le FMI contribuera à hauteur de 55 milliards avec l'aide des banques et des États Unis. 

Les émeutes en Indonésie à cause de cette récession feront plus d'un millier de morts. Elle mettra 10 ans à revenir à son niveau de 97. La Corée, bien plus aidée, retrouvera son niveau fin 98. 

Certains investisseurs en ont aussi profité pour acheter des entreprises à bas prix, notamment en Corée du sud. 

Les banques asiatiques nouvellement protégées ont bien mieux pris la crise de 2008.

## Référence

## Liens 