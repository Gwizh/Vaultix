[[Note Permanente]]
MOC : [[CULTURE]]
Source : [[La cinquième saison]]
Date : 2022-07-07
***

## Résumé
Nom donné au carnavals en Allemagne. Elle est très importante pour eux, d'où le nom. Elle est surtout pratiquée par les catholiques au sud du pays. Il y a plusieurs noms en fonction des régions comme Karneval, Fasching, Fastnacht ou Fasnacht.

## Référence
[[87 - Die Nachrichten heute]]
[[Allemagne]]

## Liens 