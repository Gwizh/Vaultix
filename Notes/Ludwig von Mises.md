MOC : 
Source : [[Ludwig von Mises]]
Date : 2023-04-14
***

## Résumé
Economiste libéral connu pour sa virulente critique du socialisme. Il prône un individualisme méthodologique qui suscite une méfiance envers la macro-économie. Mises rejette l'idée de tests empiriquesdes théories économiques, affirmant que contrairement aux sciences de la nature, en économie "l'étalon ultime pour apprécier si un théorème est correct ou non est la seule raison, sans l'aide de l'expérience". 

Grandement repris par les économistes du [[Néolibéralisme]]

## Référence

## Liens 