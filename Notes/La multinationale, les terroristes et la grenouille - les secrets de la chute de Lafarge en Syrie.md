MOC : 
Source : [[La multinationale, les terroristes et la grenouille - les secrets de la chute de Lafarge en Syrie]]
Date : 2023-04-25
***

## Résumé
Comment une multinationale française en est-elle venue, par pure cupidité, à financer le terrorisme islamiste pour maintenir en activité une usine en [[Syrie]] ? Que savaient les services secrets français ? À partir de documents d’enquête inédits, Mediapart a reconstitué les dessous du scandale [[Lafarge]].

Au départ était une grenouille. Le 26 juillet 2012, Christian Herrault, directeur général adjoint de la multinationale française Lafarge, le leader mondial du ciment, envoie un mail à deux de ses collaborateurs. Son objet : « Syrie ». L’heure est grave. Le pays a sombré dans la guerre civile et cela fait déjà cinq mois que l’ambassade de France a fermé ses portes à Damas, la capitale.

Des questions cruciales se posent à l’état-major de Lafarge, qui détient en Syrie la plus grande et la plus chère usine de ciment du Proche-Orient. Faut-il rester ou partir, comme Total en a pris la décision fin 2011 ? « J’ai besoin de comprendre […] car je ne voudrais pas que nous soyons comme la grenouille dans la casserole qui chauffe lentement mais sûrement, grenouille qui ne saute plus », écrit dans son mail Christian Herrault, qui est aussi le président du conseil d’administration de la société propriétaire de l’usine syrienne.

Lafarge, finalement, est restée, et au prix du pire. Deux ans après le mail de [[Christian Herrault]], l’entreprise née au début du XIXe siècle sur les terres calcaires d’Ardèche de la famille Pavin de Lafarge, parangon d’un capitalisme paternaliste empreint de valeurs chrétiennes, semble être devenue ce qu’elle redoutait : une grenouille dans un bocal, qui, comme dans la fable, meurt de s’être habituée à une eau que l’on réchauffe petit à petit... Comment ? En pactisant avec l’État islamique en Syrie (aussi appelé Daech ou ISIS) pour continuer de faire tourner son usine coûte que coûte.

Selon ce document de 62 pages, daté de la fin novembre 2020, _« les dirigeants de Lafarge s’étaient habitués à un environnement qui se dégradait progressivement au point de mettre en péril la vie de la société et du groupe tout entier_ […] _Ainsi, en deux ans, d’une méfiance naturelle devant les risques encourus dans un pays en guerre soumis à des groupes armés, la direction de Lafarge faisait de Daech un fournisseur comme les autres, dont les prestations impactaient le prix de vente du ciment et rognaient les marges opérationnelles de la société »_.

_« Alors que l’usine était encerclée par Daech, que les services diplomatiques étaient tous fermés, que les risques étaient maximaux sur les routes, que tous les aéroports étaient condamnés et que les postes-frontières avec la Turquie fermaient régulièrement, la fabrication de ciment_ […] _continuait toujours et Lafarge payait des terroristes pour protéger le site industriel »_, selon le même rapport du SEJF, dont les travaux ainsi que ceux des magistrats ont abouti à la mise en examen de Lafarge pour « complicité de crimes contre l’humanité » – une première mondiale pour une personne morale – et de plusieurs de ses dirigeants pour « financement du terrorisme ».

Pour comprendre la descente aux enfers de la multinationale, qui a récemment accepté de [plaider coupable](https://www.mediapart.fr/journal/france/201022/financement-de-daech-par-lafarge-la-justice-americaine-parle-d-un-crime-ahurissant) aux États-Unis et de payer une amende de 778 millions de dollars pour éviter un procès, il faut remonter seize ans en arrière. Fin 2007 précisément, quand le géant français s’achète pour 8,8 milliards d’euros la société de droit égyptien Orascom Cement, une filiale du groupe de la famille Sawiris, qui compte en son sein l’homme le plus riche d’Égypte (Nassef Sawiris).

Cette acquisition, qui signe le premier acte fort du tout nouveau président de Lafarge, [[Bruno Lafont]], arrivé au pouvoir quelques semaines plus tôt, va permettre au groupe de gagner de nouvelles parts de marché dans la région. Notamment en Syrie.

Entre 2008 et 2010, Lafarge fait ainsi construire au nord du pays, à la frontière turque, une usine gigantesque. Le montant de l’investissement est à la mesure des nouvelles ambitions du groupe : 680 millions de dollars, financés par seize banques différentes (libanaises, jordaniennes, syriennes), mais aussi par la Banque européenne d’investissement, l’Agence française de développement, un fonds danois et des fonds propres. Les projections pour cette nouvelle usine, dirigée et contrôlée depuis la France, font saliver les actionnaires du groupe Lafarge. Plus de deux millions de tonnes de ciment devraient être produites chaque année avec, à la clé, des recettes potentielles estimées à 200 millions d’euros.

« L’espoir d’une usine profitable était au rendez-vous mais la guerre brisait les ambitions de la jeune société », résumera l’officier de la douane judiciaire dans son rapport. Plongée dans la guerre civile, la Syrie devient au mois de mars 2013 une terre de djihad, la ville de Raqqa tombant aux mains de différents groupes rebelles islamistes, dont Ahrar Al-Cham et le Jabhat Al-Nosra. Un mois plus tard, l’État islamique (EI), né en Irak, arrive à son tour en Syrie, où il recrute des combattants issus des rangs d’Al-Nosra – certains de ses chefs refusant l’alliance avec l’EI, l’organisation prête allégeance à Al-Qaïda, connue pour avoir fomenté les attentats du 11-Septembre aux États-Unis.

Fin septembre 2013, un accord est d’ailleurs trouvé entre Lafarge et les terroristes, à la suite de quoi les ventes de ciment repartent à la hausse. « Mais l’accord ISIS devait être renégocié à plusieurs reprises, en octobre 2013, en novembre, en avril 2014, en juillet et en août », souligne le rapport du SEJF. Lequel ajoute : « Les négociations continuaient même après le vote et la publication de la résolution de l’ONU numéro 2170 du 15 août 2014 interdisant tout lien avec ISIS. »

Selon un ancien directeur financier du groupe, entendu comme témoin, l’explication serait à la fois psychologique et purement économique. Psychologique : l’usine syrienne était _« son premier grand succès après avoir pris ses fonctions de président »_. Et _« tout ce qui pouvait porter ombrage à la qualité de cette acquisition l’amenait à réagir de façon forte et à exprimer son courroux »_. Économique : _« M. Lafont avait des obligations de résultat qui l’amenaient à challenger tout ce qui pouvait conduire à la mise en danger du résultat économique de l’entreprise, le profit. »_

## Référence

## Liens 
[[État Islamique]]
[[Daech]]

Un candidat du FN a supervisé la collaboration de Lafarge avec Daech en Syrie
https://www.mediapart.fr/journal/international/030517/un-candidat-du-fn-supervise-la-collaboration-de-lafarge-avec-daech-en-syrie?utm_source=global&utm_medium=social&utm_campaign=SharingApp&xtor=CS3-5
