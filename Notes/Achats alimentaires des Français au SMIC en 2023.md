MOC : 
Source : [[Achats alimentaires des Français au SMIC en 2023]]
Date : 2023-04-10
***

## Résumé

79% des Français au Smic ont réduit leurs achats alimentaires. 42% ont même supprimé un repas en raison de l'inflation, selon une étude de l'[[Ifop]]. Environ 30% des Français gagnent le [[SMIC]] ou moins. Les prix de l'alimentation ont augmenté de 15,8% sur 1 an. ([[Insee]])

## Référence

## Liens 