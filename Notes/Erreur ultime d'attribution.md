MOC : 
Source : [[Erreur ultime d'attribution]]
Date : 2023-04-25
***

## Résumé
[[Thomas Pettigrew]]

C'est un grand ![[Biais d'auto-complaisance]] mais axé sur un groupe d'appartenance entier. On y applique la même logique en renforçant les causes internes et externes. Il y a par exemple erreur ultime d'attribution quand l'endogroupe renforce les causes défavorables (internes) pour l'exogroupe et renforce ces propres causes favorables. Cela permet à l'individu de se complimenter lui-même. Évidemment il y a un lien fort avec l' [[Ethnocentrisme]]. En effet, en tant qu'observateurs, on va faire de mauvaises interprétations si l'acteur de l'action que nous voyons n'est pas de notre groupe. La culture individualiste renforce ce renforcement des causes internes. Cette erreur est intériorisé dans le cas où le groupe est dominant. Les femmes vont elles-mêmes déprécier d'autres femmes en partant des stéréotypes intériorisés. Ces préjuges sont renforcés par ce même mécanisme, justifiant la discrimination. 

## Référence

## Liens 