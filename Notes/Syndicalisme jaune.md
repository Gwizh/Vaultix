MOC : 
Source : [[Syndicalisme jaune]]
Date : 2023-06-11
***

## Résumé
Jaune c'est la couleur des traîtres. Un syndicaliste jaune c'est un traître organisé qui est du côté de l'employeur et non des salariés.

À la suite de la grève générale de Schneider et Cie au Creusot en septembre-octobre 1899, la sentence arbitrale signée par Pierre Waldeck-Rousseau, président du Conseil, le 7 octobre 1899 autorise notamment la création de syndicats ouvriers à l'initiative de leur employeur : Eugène II Schneider impulse ainsi la création le 29 octobre 1899 du « syndicat des corporations ouvrières du Creusot et de ses dépendances ». D'après le préfet de Saône-et-Loire de l'époque : « Ce nouveau syndicat, formé à l'instigation de l'administration des usines, n'est composé que d'ouvriers favorables au patron et n'a d'autre but que d'entraver l'action du premier syndicat, organisé après la première grève dans un but d'émancipation ouvrière »2.

D'autres syndicats jaunes sont ensuite fondés à Montceau-les-Mines (le 8 novembre 1899) puis dans d'autres villes, Gueugnon, Montchanin, Carmaux, Belfort, etc. Pierre Biétry tente même de fédérer ces syndicats à travers une Fédération nationale des Jaunes de France créée le 1er avril 1902.

Dès 1887, il y avait déjà un syndicalisme chrétien à Paris qui ressemblait beaucoup aux jaunes.

Dans le milieu automobile, il fallait parfois se soumettre à ces syndicats pour avoir des promotions, des augmentations ou juste la paix. Les étrangers étaient la plupart du temps forcés de rentrer dans ces syndicats. Ils devaient parfois payer des bouteilles de [[Ricard]] au chef ou moucharder les mauvais éléments (la [[CGT]] typiquement). Il fallait la carte de la [[Confédération des syndicats libres|CSL]]

## Référence
[[Syndicalisme]]

## Liens 