MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081629
Tags : #Fini
***

# L'entretien dans la production
p115
L'entretien n'est jamais pris en compte dans le PIB mais surtout l'invention. Térotechnologie, concept fou.
Dans un monde SF dystopique, les inventions sont toutes déjà là, l'ordre est établi. Beaucoup d'entretien est mis en place pour maintenir cet ordre, pour entretenir ces technologies. L'entretien implique une discipline des utilisateurs et donc potentiellement un retrait des libertés. En effet, la technologie complexe nécessite la dépendance. On pense à l'électronique des voitures. Des "forces de police" peuvent être installés pour vérifier la bonne utilisation du matériel par les ouvriers d'une usine.
Exemple du Ghana p124 où la réparation est centrale à l'économie des grandes villes.
Btw l'entretien est toujours genré.

## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]