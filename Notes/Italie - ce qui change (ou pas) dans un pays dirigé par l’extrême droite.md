MOC : 
Source : [[Italie - ce qui change (ou pas) dans un pays dirigé par l’extrême droite]]
Date : 2023-05-05
***

## Résumé
Quelques mois ne suffisent pas pour voir tout ce qu’opère la présence d’une dirigeante d’extrême droite à la tête d’une grande démocratie européenne. Mais ils permettent déjà de saisir ce qui pourrait basculer. Premier bilan du gouvernement de Giorgia Meloni. 

L’Italie de Meloni, au pouvoir depuis octobre dernier, est-elle alors un laboratoire politique : celui où la fusion des droites traditionnelles et radicales catalyse, _in fine_, l’arrivée à la fonction suprême de la cheffe d’une formation d’[[Extrême droite]] ?

_« Tout dépend de ce qu’on appelle “laboratoire” et du référentiel que l’on se donne_, répond Caterina Froio, spécialiste de l’extrême droite et enseignante-chercheuse à Sciences Po, au centre d’études européennes et de politique comparée (CNRS). _Si on regarde les transformations et les évolutions de l’extrême droite en [[Italie]] de l’après-guerre jusqu’à nos jours, on peut juger que ce pays a été un incubateur de tendances qu’on retrouve ailleurs en Europe. Mais si on s’intéresse aux extrêmes droites contemporaines, c’est plutôt du côté de la [[Hongrie]] ou de la [[Pologne]] qu’il faut se tourner. »_ 

[[Marine Le Pen]], dans un entretien récent donné au quotidien italien La Repubblica, a refusé d’être considérée comme la « sœur jumelle » de Giorgia Meloni, en répétant sa « loyauté » vis-à-vis de [[Matteo Salvini]], le leader de la Ligue, qui fait partie de la coalition gouvernementale italienne. La présidente du groupe Rassemblement national à l’Assemblée a surtout pris ses distances avec la cheffe du gouvernement italien en raison de l’affichage atlantiste et de positions jugées trop pro-européennes de cette dernière.

Il n’empêche que, depuis la France, Giorgia Meloni semble avoir réussi l’alchimie que cherche Marine Le Pen depuis des années : convertir un petit parti d’extrême droite nostalgique du passé en première force électorale du pays, capable de propulser sa dirigeante à la tête d’une grande démocratie européenne. 

Un processus qui ne se réduit pas au terme de « dédiabolisation », que Caterina Froio juge trompeur « parce qu’il suggère une forme de modération de ces partis de droite radicale populiste ». « Je préfère parler de normalisation des idées de ces partis qui ont acquis un rôle de plus en plus central et arrivent même à participer à des gouvernements, voire à les diriger. Mais la conséquence de ce processus n’est pas de modérer ces partis, ce serait plutôt de radicaliser les partis de droite avec lesquels ils gouvernent, comme on l’a vu avec les gouvernements Berlusconi successifs. »

La différence entre une Meloni « président » du Conseil italien, et non présidente, comme elle a insisté pour se faire appeler, et une Marine Le Pen présidente de la République française, tiendrait aussi à la nature de la démocratie italienne, où l’exécutif concentre entre ses mains moins de pouvoirs qu’en France.

Le parlementarisme et la proportionnelle obligent quiconque veut gouverner à nouer des alliances, sans même évoquer l’organisation plus décentralisée et fédérale du pays, qui donne aux « gouverneurs » des régions des latitudes d’action susceptibles de battre en brèche le monopole du pouvoir exécutif.

Une réforme présidentialiste, sur le modèle français, est dans les tuyaux, même si ses contours comme son calendrier demeurent flous. Pour le chercheur [[Steven Forti]], auteur d’un livre encore non traduit intitulé Extrême droite 2.0 : de la normalisation à la lutte pour l’hégémonie, une « telle réforme n’est pas facile à faire mais pas non plus impossible. Il suffit qu’une majorité parlementaire la vote, puis qu’un référendum populaire approuve ce vote. C’est de cette façon qu’avait été facilement approuvée la réduction du nombre de députés et de sénateurs en 2020. En jouant du sentiment “antipolitique” des Italiens et de l’épouvantail de l’instabilité gouvernementale, c’est le meilleur moment pour faire approuver un changement qui serait aussi important que votre passage de la IVe à la Ve République ».

==Meloni veut créer un régime présidentiel à la française en créant un exécutif plus fort, plus centralisé. En 2020, une autre réforme avaitpour conséquence de réduire le nombre de députés et de senateurs.== 

Quels que soient les échos possibles, ou non, entre les deux côtés des Alpes, les premiers mois du gouvernement Meloni laissent un sentiment ambivalent.

Le fait que la coalition aujourd’hui au pouvoir soit, avec le parti Forza Italia de Berlusconi, la Ligue et un parti post-fasciste – Fratelli d’Italia aujourd’hui, Alliance nationale hier –, l==a même coalition qui a déjà dirigé l’Italie pendant la majorité des vingt dernières années incite à ne pas voir de bascule fondamentale.==

Toutefois, juge Steven Forti, « on ne peut pas dire que rien n’ait changé. Les politiques économiques, budgétaires ou diplomatiques sont certes similaires à ce que faisait le gouvernement précédent. Meloni a compris tout de suite que, si elle voulait conserver le pouvoir, il y avait deux lignes rouges : montrer à Washington que l’Italie est un partenaire fiable sur fond de guerre en Ukraine et ne pas heurter de front Bruxelles, alors que son parti prônait la sortie de l’euro il y a moins de dix ans ».

« Mais, poursuit le chercheur, sur les questions identitaires, les migrants, les droits sociaux et civiques, il y a déjà un vrai changement qui n’est pas que rhétorique. Ce gouvernement d’extrême droite n’est pas n’importe quel exécutif simplement plus conservateur que les précédents. ==C’est clairement une menace pour la démocratie. Et il faut penser que cela ne fait que six mois qu’il a pris ses fonctions. »== 

Tout cela dessine, d’autant que les annonces et mesures les plus importantes datent des dernières semaines, de possibles accélérations qui laissent penser que le sentiment de continuité pourrait être trompeur. Même s’il demeure souvent difficile de distinguer ce qui relève de la provocation médiatique de l’agenda déterminé. ==Et même si l’extrême droite au pouvoir paraît fonctionner davantage par petites touches que par grands ravalements, par mouvements tactiques et localisés plus qu’au moyen de grandes lois trop visibles et susceptibles de susciter des réactions des voisins européens.== 

D’abord, les questions mémorielles, qui constituent sans doute la part la plus visible des mutations provoquées par l’extrême droite depuis son entrée en fonction. Pour [l’historien](https://www.mediapart.fr/journal/culture-idees/200922/carlo-ginzburg-le-fascisme-un-futur) Carlo Ginzburg, dont le père fut assassiné par  la Gestapo en 1944, _« ce qui a déjà changé de façon dramatique, c’est le rapport au passé, et plus précisément l’effacement de l’antifascisme. Je suis convaincu que la situation contemporaine ne peut être décrite en termes de fascisme, mais l’effacement de l’antifascisme est une réalité. Ce gouvernement gomme ainsi le fait que la Constitution italienne est née de la résistance au fascisme et au nazisme »._

Le président du Sénat, [[Ignazio La Russa]], deuxième personnage de l’État et proche de Giorgia Meloni, connu pour être un collectionneur de bustes de [[Benito Mussolini]], a ainsi déclaré qu’il _« n’existe pas de référence à l’antifascisme dans la Constitution »._ Le même a ensuite ironisé sur un des épisodes les plus dramatiques de l’histoire italienne, le massacre des Fosses ardéatines. En mars 1944, en représailles à un attentat commis par des partisans communistes ayant tué trente soldats allemands, les nazis ont exécuté 335 civils dont les corps furent enterrés en périphérie de Rome. La Russa s’est amusé du fait que les partisans italiens n’auraient abattu que « des membres semi-retraités d’un groupe musical » et non de véritables combattants…  ^a4aebb

« [[Fratelli d’Italia]] a dès l’origine, souligne Caterina Froio, été divisé entre des membres qui mettaient en avant l’expérience historique du fascisme italien et d’autres tournés vers un autre modèle d’extrême droite. Giorgia Meloni n’a jamais pris position, ce qui lui permet de conserver une forme d’ambivalence. Cependant, il faut souligner qu’on a beaucoup scruté sa participation aux commémorations du 25 avril dernier, jour qui célèbre en Italie la libération du fascisme, mais que [[Silvio Berlusconi]] ne s’était, lui, jamais rendu à ces commémorations. » Giorgia Meloni a, plusieurs fois depuis son entrée en fonction, affirmé que Fratelli d’Italia avait « réglé ses comptes » avec l’héritage fasciste. 

Il est ensuite impossible de saisir vraiment la politique italienne sans mesurer l’angoisse démographique qui étreint le pays, même si les conclusions à tirer de cette situation divergent. Un des premiers actes symboliques du gouvernement Meloni a ainsi été de rebaptiser le ministère des affaires familiales, devenu celui de « la famille, de la natalité et de l’égalité des chances », confié pour l’occasion à Eugenia Roccella, figure de La Manif pour tous version italienne, qui n’a pas répondu à nos demandes d’entretien.

## Référence

## Liens 