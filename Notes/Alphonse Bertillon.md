MOC : 
Titre : [[Alphonse Bertillon]]
Date : 2023-10-05
***

## Résumé
Alphonse Bertillon, né à Paris le 22 avril 18531 et mort à Paris le 13 février 1914, est un criminologue français. Il est le fondateur, en 1882, du premier laboratoire de police d'identification criminelle et le créateur de l'anthropométrie judiciaire, appelée « système Bertillon » ou « bertillonnage », un système d'identification rapidement adopté dans toute l'Europe, puis aux États-Unis, et utilisé en France jusqu'en 1970. 

## Références

## Liens 