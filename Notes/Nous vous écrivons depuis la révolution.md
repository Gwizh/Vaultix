MOC : 
Source : [[Nous vous écrivons depuis la révolution]]
Date : 2023-06-11
***

## Résumé
#### Introduction
Xweser : Autonome en kurde : "être sa propre tête" 

Ce sont des femmes blanches françaises qui ont écrit ce livre, elles cherchent à se défaire des clichés.

>"C'est comme si notre esprit était sans arrêt rempli d'eau sale. Il faut constamment chercher à le nettoyer en le débordant d'eau propre."

#### [[Kurmancî]]
![[Kurmancî#Résumé]]

#### Prologue historique
##### Kurdistan : lutter pour l'existance
Le Kurdistan est au Mésopotamie. Pendant la [[Société naturelle]], cette partie du monde suivait une logique matriarcale. Il y a avait de nombreuses déesses mères par exemple. C'était la période pré-patriarcale de l'histoire du Kurdistan.

Les sumériens ont entre autres apportés la structure patriarcale et impérialiste dans la région.

Ont suivi 5000 ans de colonisation.

Début de la résistance kurde au XIXème siècle face aux anglais. [[Révoltes kurdes]]
[[Lady Adela]]
[[Révolte de Cheikh Saïd]]
[[Génocide kurde]]

##### Situation en Turquie
Coups d'État successifs en 1960, 1971 et 1980.
Années 70 : bouillonnement révolutionnaire dans les universités
[[Abdullah Öcalan]] fonde le [[Parti des travailleurs du Kurdistan]] d'inspiration [[Marxisme-Maoisme]]. 
[[Sakine Cansiz]] participe à ce projet et l'oriente plus vers le féminisme.

Puis vint le [[Coup d'État de 1980 en Turquie]] où s'organise une répression des révolutionnaires.

Öcalan lance des "analyses de la personnalité" pour créer de nouveaux rapports sociaux au delà du [[Patriarcat]] et du [[Racisme]]. 

1987 : "La libération du Kurdistan et de la société kurde ne peut être réalisée sans la libération des femmes." --> [[Union des Femmes Patriotes du Kurdistan]]
1990 "Guerre du sud"
1993 "Sale guerre" : fuite de 4 millions de Kurdes.
1999 Arrestation d'Öcalan.

Changement de paradigme :
Öcalan pense que la liberté des Kurdes ne tenait pas à un manque d'État Nation kurde. Alors que le mouvement était marxiste avec un état fort. Il écrit le Manifeste pour une civilisation démocratique et prône une modernité démocratique.

3 principes pour les [[Commune]] : éducation, [[Auto-défense]], réconciliation et justice.

Le confédéralisme démocatique n'est en guerre avec aucun État-nation mais il ne restera pas passif face aux tentatives d'assimilation.

Conseils, comités et structures autonomes.

##### Syrie et Rojava
Histoire de la [[Syrie]]

Syrie, protectorat de la [[France]]
"La France est chargée de conseiller, d'aider et de guider les populations dans leur administration et d'édifier les mesures propres à faciliter le développement progressif de la Syrie et du [[Liban]]".
France écrase les résistances locales.

[[Grande révolte syrienne]]
[[Coup d'État de 1963 en Syrie]] --> [[Charles de Gaulle]] est responsable du bombardement de Damas pendant 36 heures.

### Détails de la vie la bas
Dans les manifestations, les femmes vont toujours devant.

Il y a ce qu'iels appelent tekmîl : des réunions ou iels parlent des problemes en cours : critique et autocritique. On peut parler d'un probleme avec qqun et cette personne ne peut pas répondre tout de suite, elle doit réfléchir et ne pourra répondre que dans la séance suivante. De plus, c'est le seul endroit ou les femmes peuvent mal parler des femmes. Les hommes ne peuvent pas voir cette auto critique, elles semblent unies.

Autre concept génial : hevaltî : camaraderie, quelque chose qu'on travaille !
L'idée c'est de ne pas s'enfermer dans des groupes mais de construire des relations avec de nouvelles personnes. Investir des relations moins évidentes.

Éducation des femmes : les sortir de leur foyer, meme les meres par exemple. L'idée est de les enlever du patriarcat pour apporter qq chose de nouveau

## Référence

## Liens 
[[Kurdistan]]
[[Rojava]]