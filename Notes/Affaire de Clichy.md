MOC : 
Source : [[Affaire de Clichy]]
Date : 2023-07-07
***

## Résumé
L'affaire de Clichy désigne les suites d'une échauffourée survenue à Clichy le 1er mai 1891.

Le 1er mai 1891, une trentaine de manifestants improvisent un défilé allant de Levallois-Perret à Clichy, drapeau rouge en tête. Un peu avant trois heures, alors que le drapeau est roulé et que les manifestants se dispersent, le commissaire Labussière donne l'ordre de s'emparer de l'emblème. C'est l'incident, des coups de feu sont échangés et des agents de police légèrement blessés. Trois anarchistes sont aussitôt arrêtés, dont Louis Leveillé, lui-même blessé par balle. Dès leur arrivée au poste, ils subissent un violent passage à tabac, ce qui révolte les anarchistes. Lors de leur procès, le 28 août de la même année, l'avocat général Bulot requiert la peine de mort contre l'un des prévenus. Le verdict est sévère : Henri Louis Decamps est condamné à cinq ans de prison, Charles Auguste Dardare à trois ans, Louis Leveillé est acquitté.

D'abord occultée par la [[Fusillade de Fourmies]] qui eut lieu le même jour, l’affaire est suivie avec plus d’intérêt par les journaux anarchistes. La Révolte, met en valeur l'attitude exemplaire de Henri Louis Decamps lors de son procès ainsi que les violences subies par ses compagnons. [[Sébastien Faure]] édite une brochure sur les débats judiciaires intitulée L'anarchiste en cour d'assises.

La brutalité policière et les condamnations sont perçues comme un défi par les anarchistes. Avec la complicité de quelques compagnons qui volent 3 kilos de poudre dans une carrière quelques mois plus tard3, Ravachol décide de le relever. Il fut l'instigateur de deux attentats contre les magistrats impliqués dans l'Affaire de Clichy. 

## Référence

## Liens 