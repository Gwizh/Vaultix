MOC : 
Source : [[Georges Bernanos]]
Date : 2023-06-17
***

## Résumé
Georges Bernanos, né le 20 février 1888 dans le 9e arrondissement de Paris et mort le 5 juillet 1948 à Neuilly-sur-Seine, est un écrivain français.

Dans ses œuvres, Georges Bernanos explore le combat spirituel du Bien et du Mal, en particulier à travers le personnage du prêtre catholique tendu vers le salut de l'âme de ses paroissiens perdus, ou encore par des personnages au destin tragique comme dans Nouvelle histoire de Mouchette.

## Référence

## Liens 