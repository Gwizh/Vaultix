MOC : 
Titre : [[Liberalism - A Counter History]]
Date : 2023-09-22
***

## Résumé


### Critiques
#### 
[Lien](https://marxandphilosophy.org.uk/reviews/7763_liberalism-a-counter-history-review-by-chris-byron/) 
At the end of Losurdo’s masterful condemnation, the theory of Liberalism, which we have come to take for granted, and the founders who we assume with equal certainty to be embodying some particular philosophy, are left with only the most tenuous of connections to any set of pristine principles regarding the Liberty of man. 
And as Losurdo’s shrewd analysis shows, slavery was not an institution that unfortunately was grandfathered into the birth of Liberalism, instead slavery engendered its maximum development based upon the success of Liberal revolutions.

**Losurdo is essentially posing a philosophical question: what is Liberalism and who can we say belongs to its ranks? But the answer he gives in each chapter is less a philosophical one than an intricate empirical analysis of historical facts. The book sits atop a fence between philosophy and history, without being enough of one (philosophy) and too much of the other (history).** After all Losurdo is an accomplished scholar on Hegel and Marx, not to mention an accomplished philosopher, but primarily this book reads as history, with a pinch of unanswered philosophical questioning peppered throughout. Perhaps that is Losurdo’s point, the fluidity of the Liberal philosophy, and its representatives, leads to the impossibility of a solid conclusion regarding the primary question of his research. But that too is a rather philosophical position that requires more elaboration than Losurdo has given us.

Nonetheless, Losurdo’s historical account is as horrifying as it is vigorous. Radicals and Socialists have long known that the birth pangs of Liberalism have been bloody, authoritarian and hypocritical. He has offered compelling research to attest to our suspicions and he leaves no room for serious rebuttal. If one still wants to declare themselves a Liberal after reading this, they have a serious hurdle to face: their own philosophies genesis. **As Losurdo has demonstrated, Liberalism is not Liberty for the individual in the positive or negative sense; it began, and continues to be, a fluid ideological defense for those in power to justify their positions of power.**
## Références
## Liens 