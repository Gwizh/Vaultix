MOC : 
Source : [[Affaire Louise Ménard]]
Date : 2023-05-12
***

## Résumé
https://archives.aisne.fr/documents-du-mois/document-l-affaire-louise-menard-et-le-bon-juge-magnaud-59/n:85

**C’est à Château-Thierry, en mars 1898, que l’affaire Louise Ménard éclate dans un contexte de crise nationale : attentats anarchistes, grèves socialistes, [[Affaire Dreyfus]].**

Louise Ménard naît à Paris en 1875. En 1898, elle vit à Charly-sur-Marne avec sa mère et son fils, âgé de deux ans, né de père inconnu. La mère et la fille sont sans travail et vivent des allocations que leur verse le bureau de bienfaisance de la commune. Mais, elles ne sont pas suffisantes pour nourrir trois personnes. Le 22 février 1898, alors que Louise, sa mère et son fils n’ont pas mangé depuis 36 heures, elle vole, à la devanture d’une boulangerie, un pain de trois kilos. Le boulanger, qui n’est autre que son cousin, porte plainte pour vol. Les gendarmes transmettent la plainte au parquet et Louise est convoquée au tribunal de Château-Thierry le 4 mars 1898 pour répondre du délit de « vol simple ». Elle n’a pas d’avocat, comme la plupart des pauvres de cette époque.

Le juge Magnaud, né en 1848 à Bergerac, se distingue dans un premier temps lors de la guerre de 1870 durant laquelle il sert dans l’armée de la Loire. Puis, en 1887, il est nommé président du tribunal de Château-Thierry. C’est ainsi qu’il préside l’audience publique du 4 mars 1898. Ardent patriote et bon républicain, il fait flotter en permanence, au faîte du palais, le drapeau tricolore et invite l’huissier de séance à frapper le sol de sa canne en criant : « le tribunal, chapeau bas ».

Malgré le réquisitoire du procureur Vialatte, le tribunal relaxe l’accusée. Le jugement présente en effet Louise comme une bonne mère de famille, laborieuse, décrit la misère dans laquelle elle se trouve et rejette la responsabilité du vol sur la mauvaise organisation de la société. L’excuse reconnue à Louise relève de la force majeure, de l’« état de nécessité ». Cette notion sera reprise ensuite par de nombreux juristes, mais ce n’est qu’un siècle plus tard, le 1er mars 1994, qu’elle sera inscrite dans les textes.

Après l’audience, le juge Magnaud donne une pièce de cinq francs à Louise qui lui sert à payer le boulanger. Le « bon juge » est né. Le jugement est universellement connu et fait couler beaucoup d’encre. La presse s’empare de cette affaire. [[Georges Clemenceau]] est le premier à surnommer le juge Magnaud, le « bon juge », dans un article de _L’Aurore_ le 14 mars 1898. Le juge répond alors qu’il reçoit des quatre coins du globe d’innombrables lettres de félicitations écrites par des gens de toutes classes et conditions.

Toutefois, le parquet général d’Amiens fait appel de ce jugement, suscitant l’incompréhension générale. Cette fois, Louise a un avocat, René Goblet, sénateur maire d’Amiens, qui fait valoir que lors de l’arrestation, sur la plainte du boulanger impitoyable, le pain de trois kilos était déjà dévoré aux trois quarts. Le 22 avril, la Cour confirme la relaxe comme valable, considérant que les circonstances exceptionnelles ne permettent pas d’affirmer qu’il y ait eu, de la part de l’inculpée, d’intention frauduleuse et que le doute doit lui profiter. Néanmoins, les motifs des premiers juges ne sont pas adoptés.

Magnaud connaît la gloire jusqu’en 1906. Sa renommée est extraordinaire : cartes postales le représentant dans des poses avantageuses, reportages photographiques, chansons, emballages de confiserie, etc. Il quitte son « cher petit tribunal » le 15 juin 1906 pour Paris où il est élu député jusqu’en 1910 date à laquelle il reprend les fonctions de juge au tribunal de la Seine en menant une vie effacée. Il meurt en 1926. Sur sa tombe, sa femme fait inscrire : « _il fut un magistrat aimé du peuple qui le surnomma le Bon Juge »._

Quant à Louise, elle recueille une somme d’argent importante grâce à une souscription en sa faveur (2000 francs), mais la magistrature et les habitants de Charly-sur-Marne, jaloux probablement de sa soudaine notoriété, lui sont peu favorables et la tiennent pour incapable. Aussi, elle retourne vivre à Paris avec sa mère et son fils, et, grâce à la romancière [[Séverine]], trouve un emploi au journal féministe « _[[La Fronde]] »._ Elle meurt à Clichy le 1er juin 1963.

## Référence

## Liens 
[[Justice]]
