MOC : 
Source : [[Artillerie sur le champs de bataille]]
Date : 2023-04-10
***

## Résumé
A partir de XVIIe siècle, l'artillerie devient incoutournable sur le champ de bataille. Les pièces d'artillerie sont de plus en plus performantes, et leur nombre s'accroît au fil du temps, de quelques dizaines en 1650 à plusieurs centaines lors de celles de la [[Guerre de Sept Ans]]. Dans la seconde moitié du XVIIIe siècle, l'artillerie devient donc un acteur majeur des conflits européens.

## Référence

## Liens 