MOC : 
Source : [[Tuerie d'Auriol]]
Date : 2023-06-11
***

## Résumé
La tuerie d'Auriol est l'assassinat de six personnes dans la nuit du 18 au 19 juillet 1981 à Auriol (Bouches-du-Rhône) dans la bastide familiale de Jacques Massié. Celui-ci était le chef de la section marseillaise du Service d'action civique ([[Service d'action civique]]), organisation politique liée historiquement au gaullisme. L'adjoint local de Jacques Massié le soupçonnait de vouloir remettre à des mouvements de gauche des dossiers concernant les membres locaux de l'organisation.

La famille de Massié — son épouse, son fils âgé de 7 ans, ses beaux-parents et son futur beau-frère — est assassinée à son domicile, Massié lui-même est tué à son retour. Des investigations journalistiques approfondies1 font apparaitre la piste plusieurs autres victimes l'année précédente, mais celles-ci n'aboutiront pas et resteront pour la justice des affaires non élucidées.

Ce crime, qui émeut la France, devient une affaire d'État. À la demande du président Mitterrand, le gouvernement dissout le SAC le 3 août 19822.

Les assises des Bouches-du-Rhône ont jugé, en mai 1985 les six accusés dont trois ont été condamnés à la réclusion à perpétuité, deux à vingt ans de prison et un autre à quinze ans.

## Référence

## Liens 