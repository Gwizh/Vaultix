MOC : 
Source : [[Révolte des Mau Mau]]
Date : 2023-07-23
***

## Résumé
La révolte des Mau Mau ou des Mau-Mau est un mouvement insurrectionnel du Kenya des années 1950. Ce groupe rebelle agit à cette époque au nom du peuple Kikuyu opprimé par l'Empire britannique au Kenya. En octobre 1952, après une campagne de sabotages et d'assassinats imputée à des Mau Mau, les Britanniques instaurent l'état d'urgence et organisent des opérations militaires à l'encontre des rebelles. Fin 1956, selon l'historienne Caroline Elkins, plus de 100 000 rebelles et civils ont été tués au cours des combats ou dans les massacres qui caractérisent la répression et plus de 300 000 autres Kikuyu sont détenus dans des camps à l'intérieur desquels des tentatives pour les amener à adopter les vues politiques du gouvernement ont été entreprises. 
## Référence

## Liens 
[[Kenya]]
[[Décolonisation]]
[[Colonialisme]]
[[Royaume-Uni]]
[[Afrique]]