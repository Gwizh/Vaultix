MOC : 
Source : [[Ligue communiste révolutionnaire]]
Date : 2023-06-01
***

## Résumé
La Ligue communiste révolutionnaire (LCR) est un parti politique français d'extrême gauche, section française de la IVe Internationale - Secrétariat unifié. Elle a d'abord été connue sous le nom de Ligue communiste (LC) de 1969 à 1973, puis de Front communiste révolutionnaire (FCR) en 1974, avant de devenir la LCR la même année. Le parti se dissout dans le [[Nouveau Parti anticapitaliste]] (NPA) en 2009. 

La Ligue communiste révolutionnaire (LCR) est issue du mouvement communiste trotskiste, anti-stalinien. Elle a succédé en 1974 à la Ligue communiste qui a rassemblé en 1969 des militants de deux entités déjà existantes : le [[Parti communiste internationaliste]] (PCI), créé en 1944, dirigé par Pierre Frank, et la Jeunesse communiste révolutionnaire (JCR), également « frankiste », créée en 1966 à partir d'une scission, en 1965, de l'Union des étudiants communistes (UEC), à la suite du ralliement au PCI d'un certain nombre de ses militants de l'« opposition de gauche », en particulier [[Alain Krivine]], Henri Weber, Charles Michaloux, Janette Habel. Elle est rattachée à la famille « frankiste » du trotskisme français, opposée à ce titre au lambertisme et au pablisme, tour à tour exclus de la IVe Internationale, et à [[Lutte ouvrière]] (LO), qui s'était constituée, après la Seconde Guerre mondiale, en marge de la IVe Internationale.

## Référence

## Liens 
[[Extrême gauche]]