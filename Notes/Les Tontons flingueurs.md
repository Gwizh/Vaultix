[[Film]]
MOC : 
Titre : [[Les Tontons flingueurs]]
Auteur : [[Georges Lautner]]
Date : 1963
Date de visionnage : 2022-07-29
***

## Résumé
On suit un homme, , qui hérite de l'entreprise de son ami d'enfance. Ce dernier était un mafieux qui avait sous sa botte d'autres personnes peu fréquentables, les fameux Tontons flingueurs je suppose. 

Ce mafieux avait aussi une fille, , qui était apparemment très mal élevée (en vrai pas du tout mdr peut être pour l'époque mais bon). Et donc en tant qu'héritier, se devait de s'en occuper. 

Donc voilà l'histoire suit les deux intrigues en parallèle avec un certain humour de décalage. 
D'un côté les Tontons avec lesquels on manque de se faire tuer ou arnaquer et de l'autre sa nièce qui n'écoute rien. 

Le gros problème avec sa nièce est son petit ami, un gars que  ne supporte pas. C'est un artiste qui parle avec un très bon français et qui franchement influence  dans une bonne direction (elle a de super note à l'école). Mais  ne l'aime pas et ça culmine en la scène de la soirée où  fait évacuer tout le monde sous prétexte que les jeunes allaient trop loin (mais ce c'étaient eux les plus torchés). 

À la fin, y'a une super scène qui réunit les deux intrigues. On a une fusillade entre les derniers Tontons et le vieux et en même temps une rencontre avec le père du petit ami qui est sourd. Perdu dans le fracas, il accepte de donner la main de sa nièce et le mariage va commencer. 

## Faits importants
- Le petit jingle est trop drôle jpp
- Mais sinon l'humour a vieilli, vu que le langage a changé c'est difficile
- Le compartiment bruitages est cassé xD Piou Piou bang bang
- Ça fume tellement, la cigarette hein
- WTF 
- Sa nièce qu'il ne connaît à peine doit lui faire à manger, heureusement que ce n'est plus comme ça. 
- la scène à la fin est vraiment cool

## Acteurs
- [[Lino Ventura]] Fernand
- [[Bernard Blier]] Raoul Volfoni
- [[Francis Blanche]] Maître Folace
- [[Claude Rich]] Antoine
- [[Jean Lefebvre]] Paul Volfoni

## Référence

## Liens 
Dans l'ordre : 
<-- [[Léon]]
--> [[Le trou]]

[[Classement des films français]] 
