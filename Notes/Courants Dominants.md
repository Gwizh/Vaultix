Titre : [[Courants Dominants]]
Type : Noeud
Tags : #Noeud
Ascendants :
Date : 2023-12-16
***

#Adam 
[[Recherches et bibliographie de Courants Dominants]]

# Résumé de l'œuvre
Adam est un jeune aristocrate français de la fin du XIXème siècle qui va vite s'orienter vers la recherche scientifique et l'entreprenariat. Il a accès tôt à une éducation spécialisée et à du matériel de pointe. Sa rencontre avec un chercheur va notamment le passionner d'électricité et le motiver à fonder une entreprise dans ce domaine. Après un différent avec son père au sujet de ce chercheur, Adam déménage à la capitale et fonde un centre de recherche pour faire parler de lui et commencer à produire des machines en tout genre. Assez vite, il rentre en compétition avec un industriel américain du nom de Wyatt au sujet de la nature du courant. Commence alors une guerre des courants entre l'alternatif et le continu pour déterminer lequel des deux devrait illuminer les rues et électrifier les usines. Le courant continu part avec plusieurs avantages considérables. C'est une technologie plus simple qui a déjà été utilisée dans d'autres domaines. Elle profite aussi d'investissements conséquents en provenance de riches capitalistes et banquiers américains. Il en résulte donc que le grand public, les politiciens et les banquiers ont tous une opinion bien plus positive sur le courant continu que sur l'alternatif. On suit alors les différentes luttes que mène Adam pour faire valoir cette nouvelle technologie qu'il voit comme objectivement supérieure. Il va tout d'abord optimiser son entreprise de multiples manières différentes pour mettre en place sa devise personnelle : "Sécurité, Unité, Progrès". Il va ensuite se battre contre les législations et l'administration de l'État qu'il voit comme corrompu et comme un frein à l'innovation technologique. Finalement, il va lutter à l'international pour faire valoir sa solution et ne pas perdre la face contre le géant américain. À la fin de sa vie, ces différents objectifs sont accomplis et il est médaillé par le gouvernement pour avoir créé un foyer de l'innovation et une entreprise florissante. Il meurt en héros après avoir légué son entreprise à son fils qui aura tout appris à ses côtés.

# Thèmes abordés
Ce récit permet d'explorer plusieurs thématiques. Il y a tout d'abord une focalisation sur le monde de la recherche et de l'entreprenariat de la fin du XIXème siècle. Le contexte historique est crucial car nous sommes alors en pleine révolution industrielle et on connaît alors un grand renouveau de la façon même de penser les sciences et les connaissances. Cela permet d'étudier la prégnance de la pensée capitaliste et libérale sur les inventions et leur utilisation. En effet, il existe à l'époque un scientisme très importante qui cherche notamment à tout rationaliser. Cette rationalisation est légitime dans de nombreux domaines (notamment scientifique) mais était aussi souvent utilisé pour justifier la supériorité de systèmes politiques, économiques et sociaux sur d'autres, sans preuves objectives valables. Il y a parmi ces fausses théories scientifiques le Darwinisme social, largement disqualifié aujourd'hui. Cela recoupe particulièrement la doctrine libérale qui se prétendait héritière d'un ordre naturel basée sur la Raison. Cette supériorité supposée justifiée par la Raison et la science s'est manifestée de différentes manières dont certaines sont explorées à travers le récit. L'une de celles-ci est une forme de paternalisme. Ce paternalisme décrit le comportement d'une élite qui pense qu'un groupe d'individus donné serait incapable d'agir pour son propre bien et elle cherche alors à la guider, l'influencer voire à lui imposer sa propre méthode. Cela se retrouve en France dans de nombreuses politiques de la IIIème République et tout particulièrement dans la "mission civilisatrice" qu'elle se donnait pour justifier la colonisation. Ce paternalisme est tout aussi important pour décrire les entreprises de l'époque et notamment pour étudier le rapport patron/employé. Ce dernier était caractérisé par une bienveillance arrogante qui s'imaginait sauver les classes ouvrières de la pauvreté et de l'ignorance.

Le Républicanisme français suit cette logique mais cherche en plus à favoriser l'égalité entre les citoyens. Elle met alors en place des institutions 

# Détails des notions étudiées
*Il faudrait que je fasse des parties plus claires*
## L'avènement du scientisme pour tout justifier
- [[Libéralisme]]
- [[Scientisme]]
- [[Sociologie des techniques]]
## La fausse méritocratie des paternalistes
- [[Paternalisme]]
- [[Capitalisme]]
- [[Darwinisme Social]]
## la domination et la destruction des minorités et de la Nature
- [[Impérialisme]]
- [[Impact environnemental de la révolution industrielle]]
- [[Pollution industrielle]]
*Un truc comme ça ?*

## Scientisme et technocratie
La démocratie et la république bourgeoise telle qu'on l'a connue à la fin du XIXème siècle pronait la domination de la science sur les croyances. Elle part d'une bonne intention de supprimer l'arbitraire du religieux et de batir la société sur des concepts justes, précis, calculés. La méthodologie scientifique permet généralement il est vrai de s'appuyer sur des expériences empiriques pour dégager une logique et une marche à suivre. 

Pourtant, la méthodologie scientifique n'est pas facilement applicable, elle demande une grande rigueur et surtout une acceptation de la complexité du monde. C'est ce qui manquait largement aux "experts" de nombreux domaines de la fin du XIXème siècle qui avaient "démontrés" la supériorité de méthodes sur d'autres en simplifiant souvent trop leurs modèles et en généralisant des observations empiriques biaisées.

Or, la croyance que certaines sociétés s'étaient affranchies de l'arbitraire religieux au profit de l'objectivité scientifique était devenue alors très pregnante en Occident. C'est cette supériorité de la raison qui a motivé et conduit en grande partie la colonisation et les missions civilisatrices des occidentaux.

### Positivisme et naturalisme : tout doit être étudié rationnellement (biaisé car ce sont toujours les mêmes qui étudient. La différence est qu'iels étudient tout le monde)
[[Positivisme]], [[Technologie]], [[Positions politiques d'Émile Zola jusqu'à l'affaire Dreyfus]]
**Adam est le libéralisme paternaliste.**

### Progrès libéral, positivisme
*Les Inventeurices*
- Tiré par le profit
[[Marxisme]]
- Aucune considération sociale ou environnementale
[[Radium Girls]], [[Luddisme]]
- Partage des informations
[[Sci-hub]], [[Julian Assange]] et [[Aaron Swartz]], [[Nikola Tesla]]
- Comment produire une énergie propre, étions-nous obligé de recourir a autant de pollution pour passer a l'électricité ?
[[On ne dissout pas un soulèvement]]

## Étude du paternalisme dans ses différentes manifestations
### Définitions conceptuelles et exemples
Comme le montre [[Roberto Merril]] dans [[Raisons politique 44 - Paternalisme libéral]], on peut donner 3 définitions au paternalisme et je suis d'accord sur sa conclusion que la dernière semble la plus juste.

1. La définition de [[Gérald Dworkin]] - le paternalisme en tant que limitation de la liberté.
Le paternalisme est le fait qu'un groupe d'individus limite la liberté d'un autre groupe dans l'intention affichée d'agir pour leur bien. Cette attitude peut être perçue comme infantilisante à l'égard de ceux qu'elle vise, en particulier les personnes n'ayant pas intériorisé les notions d'autodiscipline ou d'émancipation.

" Je suggère les conditions suivantes pour l'analyse de X agit de manière paternaliste envers Y en faisant (en omettant) Z :

Z (ou son omission) interfère avec la liberté ou l'autonomie de Y.
    X agit ainsi sans le consentement de Y.
    X ne le fait que parce qu'il pense que Z améliorera le bien-être de Y (y compris en l'empêchant de diminuer), ou qu'il promouvra d'une certaine manière les intérêts, les valeurs ou le bien de Y. "

3. Définition 
Interférer dans les choix d'un individu ou un groupe en partant du principe qu'iels ne sont pas capable de faire le bon choix. Cette incapacité peut être intellectuelle, lié à sa volonté et ...

#### Différents types de paternalisme
- Doux et strict :
	- Le paternalisme doux ne fait que donner des conseils et n'agit pas pour empêcher de le suivre
	- Le paternalisme fort va agir pour faire appliquer un conseil

### Rapport avec l'autoritarisme
Le paternalisme et l'autoritarisme ont en commun le fait de vouloir faire dominer une vision de société. Le groupe dominant est convaincu du bienfait de cette vision et souhaite absolument convertir les groupes dominés. Il pense que les maux supposés du groupe dominé viennent de leur réticence à suivre cette vision, voire de son incapacité à la suivre.

Selon la définition ci-dessus, on pourrait définir l'autoritarisme comme le paternalisme sans ce dernier point. Or l'autoritarisme a été plus profondément étudié, cela semble crucial de s'y attarder.

Le paternalisme peut être considéré comme meilleur que l'autoritarisme. Le but d'un paternaliste n'est pas nécessairement de réprimer ou d'exploiter la population dominée. Dans les faits, de nombreuses atrocités peuvent être commises au nom du bien de la population. Pourtant, le paternaliste cherche à convertir le groupe à sa vision du monde en pensant souvent que cela va aider ce groupe. L'autoritaire peut penser que les autres groupes ne peuvent pas suivre cette vision et peut arriver à la conclusion qu'il faut éliminer ou neutraliser ce groupe. Sans aller jusque-là, penser qu'un groupe n'est pas capable peut justifier des atrocités et des violences. Dans sa quête de conversion, le paternaliste peut volontairement faire du mal aux individus dominés. L'idée étant souvent que cette souffrance infligée est nécessaire ou en tout cas justifiée pour parvenir à la conversion. Souvent dans ce contexte, la fin justifie les moyens.

#### Personnage de Wyatt - Critique des deux personnages
2 idéologies : Paternalisme et Autoritarisme.

2 façons d'imposer une vision du monde, qui peuvent être alliées tout en pouvant se faire passer pour des ennemis.
Adam et Wyatt sont deux capitalistes convaincus. 
#Question Si Adam est néoclassique, qu'est Wyatt ?
Ce sont deux conquérants monopolistes qui se battent pour des différences de valeurs ou des différences de technologies mais les effets sont quasiment les mêmes pour les dominé.e.s
Adam a d'autres ennemis qui se mélangent
- Socialistes et aristos qui souhaitent le protectionnisme.
- Luddisme et conservateurs qui combattent l'industrie.
Adam se veut l'homme moderne, en dehors des clivages
Wyatt est un tyran, un magnat du pouvoir. On le connaît pour son infidélité et ses excès.
C'est le cliché du porc capitaliste.
	#Question -> Est-ce que cette conception existait à l'époque ?

Wyatt fait tout pour le profit ou par ses passions/pulsions. 
Adam censément tout par la raison 
Le fait est que pour les deux, la quête du monopole et du pouvoir est crucial.

Comment je perçois ça :
1 vision certes mais le paternalisme souhaite que chacun suive la norme et aide chacun mais ne laisse personne décider "Parent-Enfant", littéralement.
Autoritarisme élimine ou neutralise ou rejette tout ce qui ne suit pas la norme. Tout ce qui ne suit pas naturellement la norme ne mérite pas de la suivre car c'est un cas perdu.
#Question Avait-on déjà en route la justification du capitalisme comme un système "humain", "naturel" ?

Adam doit détester les banquiers et Wyatt comme symbole d'un capitalisme grossier, irréfléchi, irraisonné.

### Différence d'échelle
Le paternalisme peut être appliqué ou subit à de très nombreuses échelles, inconsciemment ou non. L'exemple qui nous vient en tête est le père de famille, qui souhaite enseigner à son enfant sa vision du monde. Cet enseignement, s'il est paternaliste, conduit à une privation de liberté de l'enfant, pour qu'il apprenne et comprenne les valeurs.

Mais de nombreuses autres échelles existent. Une autre se manifeste dans les sectes religieuses où il existe souvent un chef spirituel.

Peut être lié à un état, généralisé à une forme de gouvernement.

### Consentement
Le première définition, bien qu'imparfaite, nous permet tout de même de nous pencher sur la question du consentement. En effet, le premier paternalisme ne prend pas en compte le consentement de la personne, il ne le questionne pas. 

Le fait de consentir réellement à quelque chose est pourtant quasiment impossible n'est-ce-pas ?
#Question Rationalité

Pour créer du consentement artificiel, on utilise des contrats. Le signataire va lire une suite de points et va signer à la fin pour admettre et consentir à les suivre. En admettant qu'on avait signer le contrat en connaissance de cause, en comprenant chacun des points du contrat et en ayant eu par exemple la possibilité de négocier les points 

### Nécessité du contrôle
On tombe sur un aspect qui va devenir contradictoire par la suite. Pour pouvoir corriger ou guider les individus, le paternaliste doit connaître leur conduite. Si le dominant n'accepte pas une certaine conduite et s'il pense que cette conduite fait du mal à ce groupe, il peut mettre une place un système de surveillance pour le "protéger". 

Beaucoup étudié par [[Michel Foucault - Que sais-je]].

*L'Admin* et *L'Ingénieur*
- Surveillance des déplacements, des flux
[[Flux]], [[Michel Foucault - Que sais-je]]
- Surveillance de la performance
- Développement d'une méthodologie par le haut, malgré les différences. Éducation
[[Jules Ferry]], [[Capitalisme, désir et servitude]]
- Sexisme, le capitalisme est machiste, toxique
[[Louise Michel]], [[Une théorie féministe de la violence]], [[Réinventer l'Amour]]
- Autoritarisme
[[Autoritarisme]]

### Lumière
- Apporter la lumière, rapport au progrès social et technologique, paternalisme. C'est d'autant plus vrai dans les villes. On a opposé la lumière à l'obscurantisme et on redoute les quartiers sales et sombres. Voir permet de savoir et permet d'agir.
[[1888, Jack l'Éventreur]], [[Croissance du marché de la sécurité]], 
- Philosophie des [[Lumières]], étroitement lié au libéralisme. Faire une métaphore plus claire (haha) entre les deux.
#Alire *La Naissance de la biopolitique*

## Étude du libéralisme
*Définition*
Le libéralisme est un vaste sujet, le but n'est pas de résumé toute la recherche faite depuis la conception de ce concept. Je vais simplement explorer la principale application de ce concept et sa définition parmi les personnalités politiques qui l'ont mis en place.

> " Ensemble des doctrines politiques fondées sur la garantie des droits individuels contre l'autorité arbitraire d'un gouvernement (en particulier par la séparation des pouvoirs) ou contre la pression des groupes particuliers (monopoles économiques, partis, syndicats). "
> [Définition du CNRTL](https://www.cnrtl.fr/definition/lib%C3%A9ralisme) 

Le libéralisme est fortement opposé au concept de privilèges, droits différents en fonction de notre naissance. Il est intimement lié à la philosophie des Lumières.

Ce qui est intéressant, c'est que la définition de libéralisme s'oppose à la définition d'autoritarisme. On comprend que le libéralisme a plusieurs définitions.

### Droits et libertés des individus

### Individualisme et libre-arbitre
[[Individualisme]], [[Individualisme méthodologique]]
Ne pas voir les rapports de classe, gonfler le sentiment de libre-arbitre

### Différence par rapport a l'aristocratie : abolition des privilège
[[Libéralisme]], [[Révolution Française]]
#Alire *Contre-histoire du libéralisme*

### Libre-échange
- Libre concurrence : rien de doit freiner les initiatives des individus
[[Libre échange]]

## Etude du Républicanisme

### Différences avec le libéralisme
Le républicanisme cherche en théorie à plus s'immiscer dans la vie des individus. En effet, le républicanisme se repose sur l'idée que les libertés individuelles ne sont pas respectées par défaut, il faut assurer l'égalité de chacun par rapport à la loi pour réellement permettre la liberté. Le républicanisme est donc généralement pour un État plus fort, capable donc non seulement de gérer les différents entre les individus mais aussi de mettre en place des institutions pour réduire les inégalités des individus. Les débats entre libéraux et républicains

## Étude du capitalisme
### Loi du plus fort
- Monopolisme par essence, toujours plus large, toujours plus fort. Comment combattre ?
[[Vivre Sans]], [[Nous vous écrivons depuis la révolution]], [[Vladimir Lénine]], [[Agir ici et maintenant]]
**Comment créer et respecter une multitude qui peut combattre un monopole ?**
### Encore autre chose
https://www.monde-diplomatique.fr/2002/09/ZINN/9340

## Le paternalisme dans un système capitaliste libéral
Le libéralisme n'est pas synonyme de capitalisme et le libéralisme ne mène pas nécessairement à du paternalisme. Pourtant, il semble qu'une formule spécifique s'est propagée. Les démocraties représentatives occidentales sont un type de système où les trois notions se mélangent. 

### Le XIXe siècle et ses démocraties
- Héritière des Lumières
[Du contrat social](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjRoZD7hLqBAxWkgv0HHZYOBlgQFnoECDIQAQ&url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FDu_contrat_social&usg=AOvVaw0qFGW4DbsU_5fxf-o2qI5C&opi=89978449), [Social Contract](https://en.wikipedia.org/wiki/Social_contract)
- Démocratie représentative
- Technocratie
- Libéralisation du pouvoir, don fait au peuple

### Économie néo-classique
- Concurrence pure et parfaite 
- Entreprenariat
- Propriété
- Naturalisme --> Justification du système comme une loi de la Nature
[Droit naturel](https://fr.wikipedia.org/wiki/Droit_naturel)
- Rationalité de l'individu
- Individualisme absolu

### Idéalisation de l'époque et justification
- Age d'or du capitalisme américain
[[Belle Époque]], [[Thomas Edison, L'homme aux 1093 brevets]], [[École néoclassique]], [[There Will Be Blood]]
- Puissance des industries et de la bourse dans les prises de décisions.
[[Bourses et marchés au XIXe siècle - entreprises, matières premières et spéculation]]
- Perte de vitesse des modèles européens
[[Crise bancaire de mai 1873]], [[Grande Dépression (1873-1896)]], [[Electrical industry lag]]
- Capitalisme comme un système naturel, humain. Anthropologie
[[Libéralisme]]
- Roman biographique centré sur l'individu et ses exploits, ses rares défauts viennent de l'époque et les problèmes sont maintenant résolus
[[Jules Ferry]], [[Thomas Edison, L'homme aux 1093 brevets]], [[Des Éclairs]], [[L'Homme aux Lacets Défaits]], [[George Westinghouse]]
**Première partie d'un auteur qui a totalement souscrit a ces idées, qui ne dénonce que l'époque et ses vices, ne voit pas les liens entre les institutions, les systèmes avec les individus.**
- Roman biographique d'un homme qui aurait tout réussi et aurait marqué son époque
- L'homme faisait du bon, l'époque lui faisait du mal

### Rêve américain, méritocratie
- Individualisme
- Pas de classes, on est libre de tous nos choix
- [[Guerre des courants]], récit capitaliste par excellence
#### Une approche différente sur le rêve americain
- Un européen
- Un aristocrate, il n'est pas pauvre et n'a pas de problèmes financiers.

#### Déterminisme, Historiographie et forme du récit
Des notions que je ne connais pas encore très bien mais que je vais explorer. 
- Déterminisme : presque tout ce qu'Adam fait tient des gens autour de lui. Il croit pouvoir tout faire grâce à son talent.
[[Talent]], [[Déterminisme]], [[François Bégaudeau]], [[Spinoza]]
- L'Histoire est racontée avec un personnage, une grande figure, comme si elle suffisait à décrire toute cette époque. Cette historiographie permet de justifier le mythe du self made man et du rêve américain. Pourtant, cette façon de faire l'histoire a été complètement debunkée et bien qu'on puisse continuer à utiliser de temps en temps pour certains cas. Mais ça va plus loin. Raconter des anecdotes du personnage est encore plus puissant. Ainsi, on parle d'un personnage célèbre sans forcément parler d'Histoire. On participe à rendre humain et proche une figure. Par exemple toutes les anecdotes d'Hitler qui font bcp de vues. Ça permet de rendre l'histoire divertissante sans la rendre précise.
[[Histony]], [[Stéphane Bern]] 
- Adam est nommé ainsi car il est un personnage inventé pour profiter de tous les moyens de dominations possibles. Il a été conçu pour régner sur ce monde.
- Il est créé pour sembler être un personnage réel. Une personne peu au fait sur l'invention de l'électricité pourrait complètement imaginer ce personnage comme étant réel. 
- L'histoire est malléable, elle dépend de nos sources, de nos affects. Si tous 

### Paternalisme des élites envers le peuple
[Noble Lie](https://en.wikipedia.org/wiki/Noble_lie)
### Darwinisme social
- Libres et égaux en droits, pas face à la nature
- Les lois naturelles doivent être respectées selon le libéralisme mais qu'en est-il des lois qu'on découvre comme fausse ?

### Opposition civilisation et sauvagerie
- Fausse méritocratie - les pauvres et révolutionnaires ne sont pas perçus comme égaux, la contestation n'est donc pas légitime aux yeux du pouvoir
[[Découvrir Bourdieu]], [[Ennemis d'État]]
- Volonté pourtant simple : pouvoir contribuer de soi-même
[[Spinoza]], [[Prenons le pouvoir sur nos retraites]], [[Autodétermination des peuples]]

### Rapport au progrès
### Discipline, contrôle
- Universalisme
- Surveillance

### Paternalisme industriel, cas particulier

## Supériorité sur la Nature
### Impérialisme, colonialisme
### Rapport avec la terre, conquête
*Le Gouverné* et *Le Courant*
- Expropriation
[[On ne dissout pas un soulèvement]]
- Colonialisme
[[Une théorie féministe de la violence]]
- Paternalisme civilisateur

#### Électrification et privatisation des villes
- Signe de modernité, par la technologie
- Symbole de cette époque ou les capitalistes étaient les rois
- Transformation de la ville, ségrégation
[[1888, Jack l'Éventreur]]
- Paupérisation volontaire
[[Marxisme]]
- Pollution des espaces, conséquences immédiates sur le vivant
[[1888, Jack l'Éventreur]], [[Charbon]]

# Contexte historique
*Pas forcément ici.*
## Aristocratie français du XIXème siècle
Références :
#Alire "L'aristocratie française au XIXe siècle : Les ambitions et les réalisations" par François Crouzet
#Alire "L'aristocratie de l'entreprise : Le rôle des grandes familles" par Michel Drancourt
#Alire "Noblesse et État sous la Troisième République" par Xavier de Montclos

**Mdr, c'est si dur de faire un truc cohérent, ça va dans tous les sens**

Mission civilisatrice du Républicanisme
Volonté d'aider ces peuples en les inscrivant dans la logique et la société occidentale
Darwinisme social

Donc finalement le récit se ressère sur l'étude du contrôle des populations et des pensées. Lumière comme mission civilisatrice. Paternalisme et capitalisme etc...
Si je veux continuer de parler de la méritocratie bourgeoise, il faut vraiment que je retrouve et relie ces éléments dans le récit. Retrouver une cohérence par rapport au reste.

Si concurrence il y a c'est bien entre méritocratie et darwinisme social, c'est même très fort.
C'était l'idée même des courants. C'est dire l'importance du concept

Ce qui manque c'est donc la forme du récit. Comment faire la deuxième partie ?
C'est intéressant de faire une première partie en mode roman entrepreneur ? 
Faut-il faire une double narration, un narrateur qui est à fond dans le projet et des parenthèses (chapitre entiers) qui montrent ce qui est invisibilisé.

En fait Adam est un républicain, il prone la liberté mais aussi l'égalité. Mais petit à petit son idéal s'érode et il devient libertarien strict. Pour lui, tout doit se faire par les entreprises et elles doivent avoir un contrôle absolu. Il passe d'un humanisme modéré à un impérialisme darwiniste (si ça fait sens).

Le récit peut donc suivre la logique méritocratique du rêve américain mais il faut rendre compte des limites de cette analogie. Tout cela peut être fait avec les anachronismes de croyance (différence entre les personnages pensent du futur et le futur qu'on connait (les années 30-40))

#Alire
https://fr.wikipedia.org/wiki/Paternalisme
https://fr.wikipedia.org/wiki/Darwinisme_social
https://books.openedition.org/editionscnrs/1681
https://books.openedition.org/editionscnrs/1681#access
https://www.persee.fr/doc/arss_0335-5322_1985_num_57_1_2263
https://fr.wikipedia.org/wiki/Palimpsestes
https://www.erudit.org/fr/revues/etudlitt/1986-v19-n1-etudlitt2230/500748ar.pdf
https://www.google.com/search?client=firefox-b-e&q=The+Corporation