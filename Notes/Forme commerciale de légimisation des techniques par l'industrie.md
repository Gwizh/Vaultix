MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311080949
Tags : #Fini
***

# Forme commerciale de légimisation des techniques par l'industrie
« Le succès commercial et la diffusion légitiment l’innovation » (Chauveau, 2014, p. 5) 
« Le cheminement de l’innovation n’a rien de linéaire et impose de penser en termes de trajectoires, tout en intégrant les multiples facettes de la spécialité pharmaceutique : substance médicamenteuse, produit industriel, objet commercial. » (Chauveau, 2014, p. 6)
"Le succès commercial et la diffusion légitiment l'innovation". Les nouvelles technologies sont commercialisées avant même que les tests soient effectués. C'est l'empirisme du produit sur le marché (son succès et sa diffusion) qui guide les prochaines étapes d'innovation. Il y a parmi ces déterminants les différents métiers créés par ce nouveau marché.
## Référence
[[@Science, industrie, innovation et société au XIX _sup_e__sup_ siècle,Sophie Chauveau]]

