MOC : 
Source : [[Frédéric Boccaletti]]
Date : 2023-04-07
***

## Résumé

Député RN

En 2000, alors secrétaire départemental adjoint du MNR, il est condamné à un an de prison dont six mois ferme pour « violence en réunion avec arme ». Au cours d'un collage d'affiches, il a une altercation raciste et participe à une course poursuite en ville avec un groupe de jeunes hommes avant de fournir une arme au militant qui l'accompagne et qui tire à hauteur d'homme. L'enquête a également conclu à un port d'arme illégal, puisqu'il ne possédait aucun permis. Il est incarcéré pendant quatre mois et demi à la prison Saint-Roch de Toulon, avant d'être « gracié pour raisons de santé ».

## Référence
[[Rassemblement National]]
[[Racisme]]
[[Violence]]

## Liens 