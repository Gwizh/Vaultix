MOC : 
Source : [[Mouvement des enclosures]]
Date : 2023-07-23
***

## Résumé
Le mouvement des enclosures comprend les changements qui, dès le xiie siècle et surtout de la fin du xvie siècle au xviie siècle, ont transformé, dans certaines régions de l'Angleterre, une agriculture traditionnelle dans le cadre d'un système de coopération et de communauté d'administration de terres qui appartenaient à un seigneur local (openfield, généralement des champs de superficie importante, sans limitation physique). 

Augmentation des terres privés 

## Référence

## Liens 
[[Royaume-Uni]]