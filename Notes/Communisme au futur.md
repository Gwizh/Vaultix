MOC : 
Source : [[Communisme au futur]]
Date : 2023-04-15
***

## Résumé
Conférence avec [[Bernard Friot]] et [[Bolchegeek]] et [[Camille Leboulanger]]

SF est par principe des idéaux qui n'ont jms eu lieu. Mais créer les imaginaires c'est pouvoir penser les avenirs désirables. Camille fait des récits basés sur les écrits de Friot. Aujourd'hui le SF qui domine c'est la dystopie ou le post-apocalyptique. Créer le désir dans le communisme, malgré le 20e siècle. Au lieu de prendre les pires éléments d'aujourd'hui pour en faire des dystopies comme dans Black miroir, on peut penser prendre ce qui est bon et le mettre à fond. 

Friot n'avait jamais lu de SF. Il a reçu le livre après avoir eu la news de l'éditeur. Il a été honoré et ravi de voir que ça puisse créer un imaginaire artistique. Friot connaît [[Usul]] et sait que beaucoup l'a connu par lui. Les dépossedés.
Retirer de la dramaturgie les dangers de la faim de la précarité en général on arrive à un roman avec un gars heureux qui vit jamais seul et qui a toujours un filet de sauvetage. 

Les imaginaires permettent de poser les bonnes questions. Il a travaillé 30 ans sur des archives parlementaires ou de budget par exemple mais ne voyait pas la Révolution de 1946. La gauche n'a pas assimilé ce changement de paradigme avec le travail non lié avec la subordination. 

[[Michel Rocard]] a supprimé le salaire des parents, les allocations familiales de 1946. Invention de la Sécu dans les années 60? La construction des CHU n'a pas été permise par une avance en capital mais par les cotisations dans les salaires. Il ne faut pas taxer les riches par un impôts mais par les cotisations. Le salaire étant un droit de la personne, elle va financer avec toutes les personnes la construction de Chu par exemple mais aussi la Sécu, les retraites et tout plein d'autres idées fu même acabit. 

Voir conférence gesticulée. 


## Référence

## Liens 