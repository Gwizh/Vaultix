MOC : 
Source : [[Robert Symmer]]
Date : 2023-04-13
***

## Résumé
Robert Symmer (1707-1763) est écossais. Après une carrière dans la finance il se consacre aux sciences. En 1759 il publie dans les «Philosophical Transactions» de la Royal Society 5 de Londres le compte rendu d’expériences qui malgré leur caractère étrange lui vaudront une durable renommée (voir note 2).  

Cela commence par une observation banale : des étincelles éclatent le soir quand il retire ses bas. Beaucoup de ses amis lui disent avoir fait la même observation mais, dit-il, «il n’a jamais entendu parler de quelqu’un qui ait considéré le phénomène d’une manière philosophique». C’est en effet une idée qui ne vient pas spontanément à l’esprit et c’est pourtant ce qu’il se propose de faire. Il décide donc de porter chaque jour deux paires de bas superposées, l’une de soie vierge l’aute de laine peignée. Heureuse initiative car alors le phénomène se renforce et surtout les deux paires de bas quand on les sépare manifestent une furieuse tendance à s’attirer. On peut même mesurer cette attraction en lestant l’une des paires au moyen de masses marquées de poids non négligeable.  

Arrive un jour ou un décès dans sa famille l’amène à porter le deuil. Il ne renonce pas pour autant à son expérience et enfile une paire de bas de soie noire sur ses habituels bas de soie naturelle. Ce soir là, au moment du déshabillage, l’effet est extraordinaire ! jamais bas ne se sont attirés avec tant de fougue !  

Quand la période de deuil touche à sa fin et que les bas plus classiques reprennent leur place en position externe sur la jambe de Symmer les phénomènes retrouvent leur cours plus modéré. Voici donc deux ma- tériaux de choix pour une expérimentation sur les attractions électriques : la soie naturelle et la soie noire à laquelle le colorant apporte de nouvelles propriétés. Pour décrire ces observations Symmer utilise d’abord le vocabulaire de [[Benjamin Franklin]] mais, dans l’incapacité de décider lequel des deux bas perd ou gagne de l’électricité, il refuse un choix arbitraire et s’oriente, après avoir lu [[Charles-François de Cisternay Dufay]], vers l’idée de deux fluides électriques différents.

## Référence

## Liens 