MOC : 
Source : [[MSC]]
Date : 2023-05-12
***

## Résumé
La Mediterranean Shipping Company plus connue sous son acronyme MSC est un armateur de porte-conteneurs et de navires de croisières italo-suisse. D'origine italienne, il est aujourd'hui basé à Genève et de droit suisse. L'activité croisière est opérée via sa filiale MSC Croisières.

Le Groupe MSC revendique une flotte de 560 navires et plus de 100 000 employés. Il gère la gestion de terminaux portuaires à Singapour, Long Beach, ou Rotterdam.

En 2020, MSC rachète les activités portuaires et ferroviaires africaines de [[Vincent Bolloré]] pour 5,7 milliards d'euros. Le groupe est détenu par le milliardaire [[Gianluigi Aponte]].

## Référence

## Liens 
[[Italie]]
[[Suisse]]