MOC : 
Source : [[Parti socialiste national]]
Date : 2023-06-11
***

## Résumé
Parti socialiste national : petit parti créé par Pierre Biétry en 1902, ceci jusqu'en 1903, à partir de la [[Fédération nationale des Jaunes de France]] (FNJF), qui regroupait les syndicats « jaunes » (voir syndicalisme jaune), c'est-à-dire antimarxistes. Ce mouvement obtient le soutien d'anciens amis de Paul Lanoir, de syndicats agricoles, de la Ligue de la patrie française, d'anciens boulangistes de gauche, d'antisémites et d'antidreyfusards ;

## Référence

## Liens 
[[Boulangisme]]
[[Antisémitisme]]
[[Antidreyfusard]]