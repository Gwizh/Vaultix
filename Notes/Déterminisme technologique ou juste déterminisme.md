MOC : [[Sociologie des techniques]]
Tags : #Type/Note_éphémère
Type : Note éphémère
Titre : [[Déterminisme technologique ou juste déterminisme]]
Date : 2023-11-18
***

Le déterminisme de Spinoza est unique, il comprend tout le jeu des affects dans sa globalité, il n’y a pas de sous-déterminismes. Je sais que Lordon par exemple ne serait pas d’accord avec l’appellation même de déterminisme technologique.

Et de même, ce jeu de ces affects-là n’a pas de raison en soit d’être plus puissant que les autres affects (ceux qui ne seraient pas liés à la technologie).

Dire qu’il y a un déterminisme technologique, c’est dire que les affects générés par les technologies sont particulièrement puissants.

## Provenance
[[@Spinoza et les passions du social,]]
