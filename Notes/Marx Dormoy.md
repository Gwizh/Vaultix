MOC : 
Titre : [[Marx Dormoy]]
Date : 2023-09-25
***

## Résumé
Marx Dormoy, né le 1er août 1888 à Montluçon et mort assassiné le 26 juillet 1941 à Montélimar, est un homme politique socialiste français.

Membre de la [[SFIO|Section française de l'Internationale ouvrière (SFIO)]], il est notamment président du conseil général de l'Allier de 1931 à 1933, sous-secrétaire d'État à la présidence du Conseil en 1936 et ministre de l'Intérieur de 1936 à 1938, puis à nouveau en 1938. 
## Références

## Liens 