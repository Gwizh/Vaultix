MOC : 
Source : [[Bro Leon]]
Date : 2023-08-10 mais ça fait plusieurs semaines que j'y pense
***
#Leon
## Résumé
Idée de projet :)
Jeu-vidéo, livre et musique :)

On part de chez moi, le Leon, le pays de Brest, le pays des Abers et on crée un projet politique local et merveilleux.

Beaucoup de choses à définir mais ce n'est que le début :)
Passé ? Futur ? Utopie ? --> Surtout le lien avec la technologie en fait. Électricité ? Réseau ? Téléphones portables ? Comment ?

En fait c'est un super projet parce que j'en suis actuellement incapable mais ça me demanderait de step-up un max ! Méga exigeant mais parfaitement adapté à l'apprentissage :)

La première étape ce serait de trouver tout un tas de ressources qui se rapprocherait de tout ça.

Un Léon du futur méconnaissable du fait de l'aridité et des catastrophes naturelles

Reprendre les romans comme [[Une Vie]] qui parle beaucoup du pays natal et de la culture locale mais ici dans un contexte beaucoup plus futuriste.

Ce n'est pas post apocalyptique

Plus grande influence : [[Rojava]] et [[Nous vous écrivons depuis la révolution]].
### Jeu
Pourquoi un jeu ?
Je pense que ça peut créer un espace pour ressentir le monde. Et puis tout connement si l'économie est planifiée et que la démocratie est numérisée, il faut un modèle comme ça. Le fait de créer un tel modèle est en soi une réflexion sur ses limites et sa définition.

Je résume ça là : https://docs.google.com/document/d/1oQGRnHN6ejOaYqIfvw8hhiKqcLpwus4wPM2bxyyuH7w/edit

### Livre
Comme Damasio qui a écrit le scénario des jeux. Les quêtes doivent être des événements éminemment politiques. Mais osef du jeu deux secondes. Un livre suffit à se poser toutes les questions.

## Référence

## Liens 
https://arxiv.org/abs/2304.03442
