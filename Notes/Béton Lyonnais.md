MOC : 
Source : [[Béton Lyonnais]]
Date : 2023-07-23
***

## Résumé
Béton Lyonnais, entreprise de fabrication de béton et autres matériaux de chantier s’est installée en 1993 à Décines-Charpieu, dans le quartier des Marais. Premier problème : l’entreprise n’aurait jamais dû s’installer sur les lieux. En effet, depuis 1976, les parcelles sur lesquelles elle s’installe sont protégées par une Déclaration d’Utilité Publique (DUP) qui protège une zone de captage d’eau potable extrêmement importante pour la Métropole de Lyon. Bien que le captage de La Rubina ne fournisse que 2% de l’eau potable distribuée par la Métropole, il constitue l’un des principaux captages de secours qu’elle pourrait utiliser en cas de problème avec le captage principal de Crépieux-Charmy qu’elle qualifie elle-même de “vulnérable”. En effet, le captage de la Rubina a une capacité potentielle de captage de 6 000 m3 d’eau par jour. L’entreprise est de plus installée sur une zone agricole définie par le Plan Local d’Urbanisme et de l’Habitat (PLUH), sur laquelle toute activité industrielle est interdite.

Mais l’entreprise, bien qu’==installée illégalement==, possède un récépissé de déclaration d’activité de la Préfecture du Rhône, l’autorisant à en exercer certaines sur les lieux. Mais presque dès l’installation de l’entreprise, les activités qu’elle exerce ne correspondent absolument pas à celles déclarées en 1993, et sont classées au registre des Installations Classées pour la Protection de l’Environnement (ICPE). L’entreprise Béton Lyonnais exerce donc illégalement des activités réglementées dangereuses pour l’environnement et pour la santé publique sur une zone où elle n’a pas le droit d’être installée selon des législations nationales et locales.

Et là ne s’arrêtent pas les méfaits de l’entreprise. Elle possède au moin==s 2 forages non déclarés et illégaux qui pompent l’eau potable de la Métropole de Lyon pour ne pas payer l’eau nécessaire à l’exercice de ses activités, déverse ses eaux usées à même le sol, stocke sans aucune mesure de protection des barils d’hydrocarbures alors que cette activité est réglementée par la DUP, entrepose une grande quantité de déchets==, activité là aussi interdite,…

L’entreprise Béton Lyonnais a déjà été mise en demeure à maintes reprises par la Préfecture du Rhône, qui lui demande de se mettre en conformité avec ses injonctions. Après avoir été condamnée à payer des astreintes et des amendes administratives, l’entreprise ne change toujours rien à son comportement.

## Référence

## Liens 
[[Lyon]]