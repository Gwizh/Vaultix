MOC : 
Source : [[Manuel Azaña]]
Date : 2023-04-28
***

## Résumé
Président du gouvernement provisoire de la République espagnole (du 14 octobre au 16 décembre 1931), président du Conseil des ministres de 1931 à 1933 et, à nouveau en 1936, second président de la Seconde République jusqu'en 1939, Manuel Azaña est une des grandes figures du républicanisme en Espagne.


## Référence

## Liens 