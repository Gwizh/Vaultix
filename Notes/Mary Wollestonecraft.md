MOC : 
Source : [[Mary Wollestonecraft]]
Date : 2023-08-13
***

## Résumé
Mary Wollstonecraft, née le 27 avril 1759 à Spitalfields, un quartier du Grand Londres, et morte le 10 septembre 1797 à Londres, est une maîtresse d'école, femme de lettres, philosophe et femme engagée anglaise. 

Au cours de sa brève carrière, elle écrit des romans, des traités, un récit de voyage, une histoire de la [[Révolution française]] et un livre pour enfants. Elle est surtout connue pour Défense des droits de la femme, pamphlet contre la société patriarcale de son temps. Elle y avance l'idée que si les femmes paraissent inférieures aux hommes, c'est là une injustice non pas liée à la nature mais résultant du manque d'éducation appropriée auquel elles se trouvent soumises. Pour elle, hommes et femmes sans distinction méritent d'être traités en êtres rationnels, ce qui implique que l'ordre social soit fondé sur la raison. 

Elle épouse le philosophe [[William Godwin]], l'un des pères du mouvement [[Anarchie|anarchiste]].
## Référence

## Liens 
[[Féminisme]]
