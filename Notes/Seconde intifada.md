MOC : 
Source : [[Seconde intifada]]
Date : 2023-06-11
***

## Résumé
La seconde intifada ou Intifada el-Aqsa (arabe : الإنتفاضة الفلسطينية الثانية ou إنتفاضة الأقصى ; hébreu : אינתיפאדת אל-אקצה) désigne la période de violence israélo-palestinienne à partir de septembre 2000 jusqu'à environ février 2005. Les événements sont décrits par [[Israël]] comme une campagne de terrorisme palestinien, tandis qu'ils sont décrits par les [[Palestine|Palestiniens]] comme une révolte contre l'occupation et la [[Colonisation]].

## Référence

## Liens 