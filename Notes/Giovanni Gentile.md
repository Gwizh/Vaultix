MOC : 
Source : [[Giovanni Gentile]]
Date : 2023-05-10
***

## Résumé
Giovanni Gentile (30 May 1875 – 15 April 1944) was an Italian philosopher, educator, and politician. Described by himself and by [[Benito Mussolini]] as the "philosopher of Fascism", he was influential in providing an intellectual foundation for Italian [[Fascisme]], and ghostwrote part of The Doctrine of Fascism (1932) with Mussolini. He was involved in the resurgence of Hegelian idealism in Italian philosophy and also devised his own system of thought, which he called "actual idealism" or "actualism", which has been described as "the subjective extreme of the idealist tradition".

## Référence

## Liens 
[[Hegel]] 
[[Italie]]
