MOC : 
Source : [[Parti ouvrier d'unification marxiste]]
Date : 2023-06-18
***

## Résumé
Le Parti ouvrier d'unification marxiste (en espagnol : Partido Obrero de Unificación Marxista et en catalan : Partit Obrer d'Unificació Marxista, abrégé en POUM) est un mouvement communiste espagnol indépendant.

Il fait partie de l'Opposition communiste internationale. Il est anti-stalinien.

Créé en 1935 et devenu illégal en 1937, il a participé activement à la guerre d'Espagne contre le général Franco. Violemment réprimé, le POUM a par la suite survécu en [[Espagne]] et en exil, notamment en France, luttant contre la [[Espagne franquiste]].

## Référence

## Liens 