MOC : 
Source : [[Accords d'Oslo]]
Date : 2023-06-11
***

## Résumé
Les accords d'Oslo sont le résultat d'un ensemble de discussions menées en secret, en parallèle de celles publiques consécutives à la conférence de Madrid de 1991, entre des négociateurs israéliens et palestiniens à Oslo en Norvège, pour poser les premiers jalons d'une résolution du conflit israélo-palestinien.

Cette tentative de processus de paix israélo-palestinien, largement soutenue par la communauté internationale, sera mise en difficulté entre 1996 et 1999 à la suite du durcissement des positions de part et d'autre lorsque seront abordés les thèmes cruciaux du statut de [[Jérusalem]], du problème des réfugiés palestiniens, de la lutte contre le terrorisme. Les positions les plus extrêmes s'expriment dans les années qui suivent, lors de l'assassinat de Yitzhak Rabin en 1995 par un étudiant israélien d'extrême droite, et dans la multiplication des attentats menés par les mouvements palestiniens Hamas et Jihad islamique. Le processus d'Oslo ne pourra plus être relancé après 2000, au déclenchement de la [[Seconde intifada]].

## Référence

## Liens 