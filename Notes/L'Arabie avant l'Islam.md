MOC : 
Source : [[L'Arabie avant l'Islam]]
Date : 2023-05-05
***

## Résumé
Avant 4000 avant notre ère, l'Arabie était verdoyante, beaucoup de cahsseurs cueilleurs
Après, ça devient aride les peuples vont se réfugier dans les montagnes à l'ouest et à l'est qui forme un micro climat habitable. Faut comprendre que ces montagnes sont plutôt un fléau pour la région car c'est ce qui empâche les pluies indiennes de passer. L'[[Âge du bronze]] était une période prospère pour la région, qui jouit du commerce entre la [[Mésopotamie]] et l'[[Inde]]. Domestication du dromadaire à la fin dui deuxième millénaire avant notre ère. On ne passe alors plus par la dangereuse mer rouge mais par le désert, la majorité du commerce passera par là pendant l'[[Antiquité]]. C'est un royaume marchand qui va être appelé arabe le premier en 853 avant notre ère. 

## Référence
https://www.youtube.com/watch?v=FbjL5OMOB58

## Liens 
[[Arabie]]
[[Islam]]