MOC : 
Source : [[Universalisme républicain]]
Date : 2023-07-03
***

## Résumé
L'universalisme républicain est un des principes fondamentaux des différentes Républiques françaises, et dans une moindre mesure d'autres régimes et pays, selon lequel la République et ses valeurs sont universelles.

Il se base sur un certain nombre de principes de la philosophie des Lumières, selon lesquels tous les êtres humains sont également dotés de droits naturels et de raison, ainsi que sur une vision de la nation comme une construction politique et civique plus que comme une communauté ethnique déterminée, et sur les idéaux de liberté, d'égalité, et de fraternité, qui constituent la devise de la République (article 2 de la Constitution de 1958). 

L'universalisme républicain a profondément influencé les politiques des gouvernements français successifs, avec toutefois de très sérieuses limites dans son application, tant par le passé que de nos jours. Il est également la cible de nombreuses critiques, de la part d'autres visions philosophiques, sociales, ou politiques, tels que le nationalisme ethnique ou certaines variantes de nationalisme culturel, ou encore le relativisme culturel, le régionalisme, ou le multiculturalisme, qui à l'inverse reprochent à ce concept d'universalisme républicain l'emphase qu'il place sur l'intégration par assimilation culturelle et la centralisation politique. 

## Référence

## Liens 