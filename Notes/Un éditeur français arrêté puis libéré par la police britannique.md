MOC : 
Source : [[Un éditeur français arrêté puis libéré par la police britannique]]
Date : 2023-04-19
***

## Résumé
Un responsable des éditions [[La Fabrique ]]a été arrêté, puis libéré, par la police britannique, qui invoque sa participation présumée à des manifestations en France. Ses défenseurs dénoncent une inédite « atteinte à la libre circulation des idées ».

À 20 heures, mardi 18 avril, un rassemblement était prévu devant le consulat britannique dans le VIIIe arrondissement de Paris. Au même moment, une mobilisation devait avoir lieu devant l’Institut français, à Londres. Deux manifestations pour faire pression sur les autorités des deux pays après l’arrestation d’un éditeur français dans la capitale britannique. Dans la soirée, ce responsable de La Fabrique, une maison engagée, a été remis en liberté au terme de près de vingt-quatre heures de garde à vue. Sans faire l’objet, pour l’heure, de poursuite.

Ernest est responsable des droits étrangers aux éditions La Fabrique ainsi que pour l’auteur [[Alain Damasio]] (éditions La Volte). Lundi, il se rendait à la London Book Fair, la Foire internationale du livre à Londres, qui a lieu du 18 au 20 avril, et où, selon La Fabrique, il avait prévu une trentaine de rendez-vous avec des éditeurs étrangers et avait un billet pour rentrer à Paris le vendredi 21 avril.

Un déplacement professionnel, donc. À son arrivée à la gare de St. Pancras, le jeune homme est interpellé par des policiers _« sous prétexte de vérifier qu’il n’est pas sur le point de commettre des actes terroristes ou en possession de matériel destiné à une entreprise terroriste »_, précise le communiqué de presse de ses employeurs.

L’avocate dénonce une _« instrumentalisation »_ britannique qu’elle suspecte être en lien avec les autorités françaises. _« Il y a tout lieu de croire que cette interpellation visant à obtenir des données personnelles et professionnelles de mon client soit en lien avec la France puisque les_ [[PAF]] _ [polices aux frontières – ndlr] _française et britannique l’ont retenu si longtemps à son départ de Paris qu’Ernest a raté son train et a dû prendre le suivant. »_ 

_« Cela suggère une collaboration inquiétante entre les autorités britanniques et françaises, et constitue de fait une attaque de l’État français contre une maison d’édition dont le catalogue et la politique éditoriale sont notoirement inscrits dans les pensées critiques et en opposition avec les politiques gouvernementales »_, s’insurge le collectif d’éditeurs et éditrices appelant à la libération d’Ernest dans le Club de Mediapart.

## Référence

## Liens 