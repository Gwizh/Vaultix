MOC : 
Source : [[Affaire Sacco et Vanzetti]]
Date : 2023-06-18
***

## Résumé
L’affaire Sacco et Vanzetti est le nom d'une controverse judiciaire survenue dans les années 1920 aux [[États-Unis]], concernant les [[Anarchie|anarchistes]] d'origine italienne Nicola Sacco et Bartolomeo Vanzetti, condamnés à mort et exécutés dans la nuit du 22 au 23 août 1927. Leur culpabilité fut extrêmement controversée aussi bien à l'époque que par la suite, et plusieurs œuvres artistiques leur rendent hommage. Leur jugement a été invalidé sur la forme par le gouverneur du Massachusetts Michael Dukakis le 23 août 1977 mais leur culpabilité ou leur innocence n’a pas été établie pour autant.

## Référence

## Liens 