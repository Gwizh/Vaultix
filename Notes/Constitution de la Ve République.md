[[Note Permanente]]
MOC : [[POLITIQUE]] [[Histoire]]
Source : [[Constitution de la Ve République]]
Date : 2022-07-08
***

## Résumé
Elle est rédigée en pleine guerre d'Algérie dans le but de mettre un terme à l'instabilité gouvernementale et au risque de coup d'État militaire ; elle est marquée par le retour d'un pouvoir exécutif fort. Deux hommes y ont notamment imprimé leurs idées : [[Michel Debré]], inspiré par le modèle britannique d'un Premier ministre fort, et le général [[Charles de Gaulle]], entendant ériger le président de la République en garant des institutions, conformément aux principes énoncés dans ses discours de Bayeux et d'Épinal en 1946. 

La Constitution est adoptée le 3 septembre 1958 par le Conseil des Ministres. Le général de Gaulle la présente symboliquement à la nation place de la République à Paris, le 4 septembre 1958, jour anniversaire de la proclamation de la IIIe République. Pour qu’elle entre formellement en application, il lui faut encore être adoptée par le peuple, qui doit être consulté par référendum le 28 septembre 1958.

La Constitution de 1958 accorde un poids institutionnel très important au président de la République. Toutefois, elle ne remet nullement en question le caractère parlementaire du régime : en effet, à l'instar des constitutions de la IIIe République et de la [[Quatrième République]], celle de la Ve République demeure fondée sur une séparation souple des pouvoirs (l'exécutif peut dissoudre l'Assemblée nationale, laquelle peut renverser le Gouvernement), par opposition aux régimes présidentiels fondés sur une séparation stricte des pouvoirs (l'exécutif n'a pas le pouvoir de dissoudre le législatif, lequel ne peut renverser l'exécutif). Le rôle politique central du président de la République semble toutefois résulter beaucoup moins du texte initial de la Constitution de 1958 que de deux éléments majeurs : i) la pratique institutionnelle insufflée par le premier président de la Ve République, Charles de Gaulle, dont l'aura politique et le poids historique sont considérables à la différence de ceux de ses prédécesseurs ; ii) l'élection du président de la République au suffrage universel direct à partir de 1962, laquelle a très fortement élargi le poids politique du chef de l'État (jusque là élu au suffrage indirect), donnant naissance à la notion de « majorité présidentielle », inconcevable en 1958.

## Référence
[[Politque française]]

## Liens 