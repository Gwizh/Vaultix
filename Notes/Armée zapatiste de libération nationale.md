MOC : 
Source : [[Armée zapatiste de libération nationale]]
Date : 2023-06-10
***

## Résumé
L'Armée zapatiste de libération nationale (espagnol : Ejército Zapatista de Liberación Nacional, EZLN) est une faction révolutionnaire politico-militaire controlant une partie des [[Chiapas]], l'un des États dont les habitants sont parmi les plus défavorisés du Mexique, pays qui est depuis ces dernières années une importante puissance économique mondiale en termes de PIB.

L'EZLN affirme lutter non seulement pour la protection et la promotion des droits des populations indigènes mais aussi de toutes les minorités du pays.

L'attitude non-violente du mouvement est une des raisons de la longévité de l'EZLN et aussi de la popularité qu'elle rencontre selon elle au sein de la population mexicaine et de la communauté internationale. L'organisation est devenue pour certains un symbole de la lutte [[Altermondialisme|altermondialiste]].

## Référence

## Liens 