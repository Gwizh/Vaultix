MOC : 
Source : [[Mayotte]]
Date : 2023-04-25
***

## Résumé
Devenue département français depuis un référendum en 2009, Mayotte est le fruit d’un rapt. Violant la règle internationale de respect des frontières, la France l’a arrachée à l’archipel dont elle faisait partie, les [[Comores]], lors de la décolonisation de ce territoire en 1975. Cette annexion est illégale au regard du droit international, qu’il s’agisse des résolutions de l’[[ONU]] ou de celles de l’[[Union africaine]]. De ce même droit que l’on invoque, à juste titre, pour combattre les annexions russes qui ont précédé la guerre d’invasion contre l’Ukraine. La [[France]] qui vote à l’ONU les résolutions qui condamnent la Russie en viole donc allègrement les principes.

Les chantres de la souveraineté française sur Mayotte opposent au droit international que cette annexion fut conforme à la volonté majoritaire des Mahorais, faisant fi des intérêts de quelques familles de notables qui y ont œuvré. En vérité, comme l’illustrèrent longtemps les menées barbouzardes de mercenaires, dont le fameux Bob Denard, dans cet archipel, il ne s’est jamais agi pour la France de l’intérêt des populations locales, mais des siens, dans une logique de puissance impériale au vu de la position stratégique de Mayotte dans le canal du [[Mozambique]].

La meilleure preuve en est donnée par l’état lamentable dans lequel la France maintient la population de Mayotte et dont un rapport de 2022, rédigé par six ministères et révélé par Mediapart, dressait un inventaire exhaustif. Département pour la forme, Mayotte est reléguée dans les bas-fonds de la République française. Elle en est le département le plus pauvre, avec 8 personnes sur 10 qui vivent au-dessous du seuil de pauvreté, un actif sur trois au chômage et une espérance de vie qui plafonne à 75 ans. Avec, surtout, une dotation par habitant trois à quatre fois moins élevée que dans l’Hexagone.

## Référence

## Liens 