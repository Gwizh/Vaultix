[[Note Permanente]]
MOC : [[SCIENCE]] [[Histoire]]
Titre : [[Sadi Carnot (physicien)]]
Date : 2022-07-29
***

## Résumé
Nicolas Léonard Sadi Carnot est un **physicien et ingénieur français**, né le 1er juin 1796 à Paris et mort le 24 août 1832 à Ivry-sur-Seine ou à Paris.

Durant sa courte carrière (il meurt du choléra à l’âge de 36 ans), Sadi Carnot ne publia qu’un seul livre (comme Copernic) : Réflexions sur la puissance motrice du feu et sur les machines propres à développer cette puissance, en 1824, dans lequel il exprima, à l’âge de 27 ans, ce qui s’avéra être le travail de sa vie et un livre important dans l’histoire de la physique. 

Dans cet ouvrage il posa les bases d’une discipline entièrement nouvelle, **la thermodynamique**.

Sadi Carnot a découvert les deux lois sur lesquelles repose toute la science de l’énergie malgré des obstacles qui paraissaient insurmontables. Il a donné une mesure de la puissance exceptionnelle de son intuition en énonçant ses lois quand les faits étaient en nombre insuffisant, leur précision grossière et surtout quand les progrès de la science naissante étaient freinés par la théorie erronée du calorique indestructible. 

Avec sa portée universelle, son œuvre constitue probablement un cas unique dans l’histoire de la science moderne et, en ce sens, Nicolas Léonard Sadi Carnot fut certainement l’un des penseurs les plus pénétrants et les plus originaux que notre civilisation ait produits. 

Il est l'oncle de [[Sadi Carnot (homme d'État)]]

## Référence
[[Thermodynamisme]]


## Liens 