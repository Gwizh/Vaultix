MOC : 
Source : [[Pour payer moins d'impôts, Patrick Drahi a fait semblant de quitter sa femme]]
Date : 2023-04-12
***

## Résumé 
#### Stratégie
En 2005, les Drahi annoncent aux services fiscaux leur rupture. Depuis, chacun a son domicile et sa fiche d'impôt. Mais le [[Fisc]] genevois flaire l’arnaque et leur réclame 7,5 milliards d'impôts et autres pénalités. La procédure est toujours en cours. 

Il font semblant de se séparer en 2005. En [[Suisse]], le taux d’imposition n’est pas le même sur tout le territoire. À Rolle, Patrick Drahi va bénéficier d’un « forfait fiscal » calculé sur la base de son train de vie, et non de ses revenus ou de son capital. 

En 2011, l’homme d’affaires déménage à nouveau. Il quitte les bords du lac Léman pour s’installer à la montagne. Il s’achète sept chalets à Zermatt, une station de ski située dans le canton du Valais qui, en plus d’offrir une vue imprenable sur le mont Cervin, lui propose un nouveau « forfait fiscal » encore plus avantageux.

Voilà pour la version officielle. Sauf que l’administration fiscale genevoise n’est pas vraiment convaincue. Comme l’ont révélé nos confrères d’Heidi News, elle soupçonne le couple d’avoir raconté une fable pour échapper aux impôts. Si Patrick Drahi est toujours en couple avec [[Lina Drahi]], monsieur doit lui aussi être imposé dans le canton de Genève où madame réside.

Le fisc ouvre une première enquête sur la période 2009-2016, avant de l’étendre jusqu’à 2019. Les fonctionnaires montent un épais dossier (198 pages) visant à démontrer que le couple a simulé sa séparation. Et leur demande de passer à la caisse. L’addition est salée : l’administration réclame presque 7,5 milliards d’euros (7,4 milliards de francs suisses).

#### Fausse séparation
En plus d’utiliser le jet privé, le yacht et les différentes demeures familiales, Madame finance son train de vie « grâce à des donations relativement régulières de PDR ». Trois millions d’euros entre 2012 et 2015, selon les intéressés. Auxquels il faut ajouter, selon nos calculs basés sur des documents tirés des DrahiLeaks, presque 550.000 euros en 2017, quelque 438.000 euros en 2018, puis 1.695 million en 2019 et 695.000 euros en 2020. Soit au total, la modique somme de 6.378 millions d’euros en huit ans. Lina Drahi bénéficie aussi d’une « ligne de crédit » de dix millions d’euros. Une somme prêtée par Patrick Drahi (1) pour « l’entretien de sa maison ». 


#MIE On ne devrait pas effectuer des opérations judiciaires à partir d'un couple, vue comme une seule entité. Comment alors créer des couples libres et gérer les intérêts des deux personnes ? Impossible. La justice ne devrait pas avoir à faire avec les relations personnelles des gens. "Deux êtres qui s'aiment n'en font qu'un : lequel ?" comme dans [[Réinventer l'Amour]]

## Référence
[[Patrick Drahi]]

## Liens 
https://www.streetpress.com/sujet/1681199665-drahi-fait-semblant-quitter-femme-fiscalite