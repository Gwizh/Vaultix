MOC : 
Titre : [[Charles Rappoport]]
Date : 2023-09-30
***

## Résumé
Charles Rappoport, né le 14 juin 1865 à Dūkštas en Lituanie et mort le 17 novembre 1941 à Cahors, est un militant socialiste et communiste français d'origine russe. 

[[Marxisme|Marxiste]], il milita à la [[SFIO]]. En 1914, il dénonça l'acceptation par la SFIO de la [[Première Guerre mondiale]], le vote des crédits de guerre, et la participation au gouvernement « d'union sacrée ». Il écrivit : « Une guerre malheureuse peut avoir des conséquences bienfaisantes. Mais la désirer pour cela est un crime »4.

Après la guerre, il milita au sein de la SFIO pour que le parti rejoigne l'Internationale communiste. Lors du congrès de Tours en décembre 1920, il fit partie de la majorité qui fonda la Section Française de l'Internationale Communiste (qui deviendra plus tard le PCF), et il fut élu au Comité directeur.

À la suite de la bolchevisation du [[Parti communiste français|PCF]], il fut écarté des responsabilités. En désaccord profond avec la ligne du parti et son soutien à l'Union soviétique, il quitta le PCF en 1938.

Au moment de la 2e guerre mondiale, il se retira à Saint-Cirq-Lapopie dans le Lot où il fut d'abord inhum5. On peut lire sur sa tombe (cimetière du Montparnasse, 25e division, Paris) l'épitaphe suivante : « Le socialisme sans la liberté n'est pas le socialisme, la liberté sans le socialisme n'est pas la liberté ». 
## Références

## Liens 