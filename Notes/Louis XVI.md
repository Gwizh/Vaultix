MOC : 
Source : [[Louis XVI]]
Date : 2023-04-14
***

## Résumé

Contrairement à ce qu'on pense, ce n'est pas un roi qui ne voulait pas gouverner. Il a grandi dans les idéaux d'absolutisme et se voyait vraiment comme la seule personne qui pouvait régner. C'est une idée qu'on a construit ensuite pour justifier la [[Révolution Française]]. En effet, pour les républicains, c'était la preuve que la [[Monarchie]] ne fonctionne pas et qu'il faut pouvoir choisir son dirigeant. Mais pour les monarchistes, le faire passer pour un incompétent était un bon moyen de dire que ce n'est pas la monarchie le problème mais juste Louis XVI.

![[La France à la veille de la Révolution (1787-1789)#^7aafce]]

## Référence

## Liens 