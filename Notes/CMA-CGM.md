MOC : 
Source : [[CMA-CGM]]
Date : 2023-05-12
***

## Résumé
La Compagnie maritime d'affrètement - Compagnie générale maritime (CMA CGM) est un armateur de porte-conteneurs français dont le siège mondial est situé à Marseille. Son offre globale de transport intègre le transport maritime, la manutention portuaire et la logistique terrestre. Elle est la quatrième entreprise mondiale de transport maritime en conteneurs et la première française.

Le groupe CMA CGM est issu de la fusion en 1996 de la Compagnie maritime d'affrètement (CMA) et de la Compagnie générale maritime (CGM), elle-même héritière de la Compagnie générale transatlantique et des Messageries maritimes.

## Référence

## Liens 