MOC : 
Source : [[Génocide kurde]]
Date : 2023-07-03
***

## Résumé
Le génocide kurde, aussi connu sous le nom d'Anfal, a eu lieu de février à septembre 1988. Ordonné par le régime irakien de [[Saddam Hussein]] et dirigé par son cousin Ali Hassan al-Majid, il a causé la mort de 50 000 à 180 000 civils kurdes. L'épisode le plus marquant de ce génocide est le bombardement aux armes chimiques de la ville kurde d'Halabja le 16 mars 1988. 

## Référence

## Liens 
[[Kurdistan]]