Titre : [[Michel Foucault - Que sais-je]]
Type : Source
Tags : #Source
Ascendants : [[Michel Foucault]]
Date : 2023-04-14
***
## Résumé
1926-1984
### Petite biographie
Né de famille aisée et catho. Il est homosexuel. Il a fait son éducation pendant la guerre. Il tente de se suicider en 1948. Ami de [[Pierre Bourdieu]], [[Paul Veyne]] (lire _Michel Foucault. Sa pensée, sa personne_, Paris, Albin Michel, Sciences humaines, 2008), [[Jean-Claude Passeron]] etc...
Adhère au [[Parti Communiste Français]] en 1950 pour le quitter en 52. Il navigue entre les deux courants de l'après guerre : la [[Phénoménologie]] et le [[Marxisme]]. Il fait sa thèse sur [[Hegel]] et quelques années plus tard publie l'ouvrage _Maladie mentale et personnalité_. En 58, il publie l'_Histoire de la folie_. Se rapproche ensuite de [[Gilles Deleuze]]. 
Il est philosophe de formation, d'abord considéré comme un historien de la psychologie, et ce jusqu'au succès de _[[Les Mots et les Choses]]_ en 1966. Ce livre fait scandale à gauche (chrétiens de gauche, marxistes et existentialistes), qui dénoncent un anti-humanisme réactionnaire et un désengagement politique nihiliste.

En 1967, [[Guerre des Six Jours]].

Entre à l'Université de Vincennes. Dans les années 70, ce le retour de la politique dans sa vie. Il fonde le Groupe d'information sur le prisons pour récupérer les témoignages. Anti-raciste : il entre dans le comité Djellali avec [[Jean-Paul Sartre]] et [[Claude Mauriac]] entre autres.

Il meurt du [[Sida]] en 1984.

### L'archélogie des sciences humaines
Le fait d'enfermer les gens malades vient du Moeyn Age mais c'est à la Renaissance qu'on commence à enfermer les gens fous, qu'on considère comme malade. On sépare Raison et folie, qu'on pense incompatible. Descartes exclut par exemple l'idée de la folie. Au XVIIe siècle, on commence à enfermer les vagabonds, chômeurs, oisifs, pauvres et fous (1% de la pop parisienne). Le Grand Renfermement. On change de paradigme : au lieu d'intégrer la pauvreté dans la religion, on les voit comme un problème de contrôle social. On cache ces populations. Prisons de l'ordre moral : utopie bourgeoise avec travail forcé. Il parle du monde de la Déraison, les gens qui ne sont pas inclus dans la Raison théorisée à l'époque. Ces gens ne suivent pas suffisamment les lois de la famille, de la religion et de la cité bourgeoise. On fait un amalgame entre la folie et les pensées contestataires ou blasphématoires. On commence à penser que pour devenir fou, il faut l'avoir un peu voulu et on culpabilise ces gens-là qui ne suive plus la morale. Les définitions de la folie à l'époque sont très vagues, on considère par exemple que la folie peut aussi être le vide de la pensée. On pensait que la folie se propageait et qu'il fallait donc interner. Il y avait toujours le souvenirs de la peste. Après la [[Révolution Française]], on imagine la folie comme une maladie à soigner. On enferme donc plus pour corriger mais on enferme toujours dans des conditions médicales immuables et données par un médecin. C'est toujours arbitraire mais c'est de plus en plus objectif. L'objectif est que le fou ait honte de sa folie. 

On a défini la folie et inventé la psychologie pour nous rendre nous-même légitime d'être raisonné. Les sciences de l'Homme s'appuient toujours, pour se constituer, sur des expériences négatives : une science du langage se construit à partir de l'analyse de l'aphasie, une psychologie de la mémoire s'écrit à partir de l'étude des amnésies, une sociologie s'édifie à partir de l'examen du suicide, etc...
#MIE Comme dans [[L'homme révolté]] ! On part du meurtre, du crime ultime pour découler toute la logique des lois et de la liberté.
C'est comme si toutes les sciences humaines ne pouvaient énoncer des vérités positives que sur le fond d'expériences où précisément s'exprime la perte des vérités humaines. Mais la révélation de cette dépendance vaut immédiatement comme critique : car comment une science pourrait prétendre énoncer des vérités positives sur l'homme, quand elle ne tire sa relative clarté que de la nuit de la folie où toute vérité se perd ? Que valent donc ces vérités positives sur l'homme qui ne prennent leur sens ultime que depuis une expérience de l'effondrement de toute vérité ?

Voir grammaire de Condillac, Bichat. De même l'expérience de la médecine n'est venue que par les expérieces sur les morts.

### L'expérience littéraire
Comme [[Jean-Paul Sartre]], Foucault s'est demandé ce qu'était la littérature. Il utilise des images et non des concepts pour en parler. Il parle de miroir, murmure, espace, recherche, distance, dehors, vide, fiction, volume. Typiquement, le miroir représente le petit espace confiné des mots qui révèle l'infinité des sens. Et puis il reprend aussi l'idée qu'on développe la littérature à partir du vide, le concept d'absence d'oeuvre. Il rapproche donc la littérature moderne de la folie car encore plus après la [[Seconde Guerre mondiale]]. 
#MIE On peut rapprocher ça de la [[Trümmerliteratur]] je pense, lu dans [[Le Grand Récit]]. 
Il parle ensuite de [[Raymond Roussel]], la symphonie de 1963. 

### Les Mots et les Choses
Scandale à sa sortie par l'union de la gauche radicale qui s'oppose aux nouvelles idées. Il évoque le [[Structuralisme]] en le poussant jusqu'à l'anti-humanisme. 

#### 
La première idée concerne les découvertes scientifiques qui au contraire d'être un processus progressif, allant d'un état nul à cet état supérieur auquel nous sommes, considère que le progrès est discontinu. En effet, nous voyons les grandes découvertes scientifiques avec la vision d'aujourd'hui et donc nous ne voyons qu'un long fil continu qui progresse malgré la religion, les visions fantasques etc... Pourtant, c'est pour beaucoup le langage qui fait évoluer notre vision du monde. C'est complexe, il faut de l'aide mdr

https://www.dygest.co/michel-foucault/les-mots-et-les-choses

Ressemblance à la [[Renaissance]]
Représentation à l'ère classique
Histoire  dans l'époque moderne. 

Sciences humaines sont au milieu de deux types de savoirs. Le premier qui est purement fini, pouvant se suffir à lui-même comme les mathématiques et la philosophie. Le deuxième empirique, ne pouvant se baser que sur une accumulation et jamais une finitude. Le milieu est donc nécessairement à la fois des faits et une dimension finie. La psychologie est l'étude de l'anatomie des centres corticaux du langage mais le fonctionnement du cerveau en tant qu'il libererait, pour le sujet fini, des représentations du monde et des autres. Utiliser les échanges avec l'extérieur pour et par soi-même. On dissout toute objectivité naturelle. 

Il critique donc les sciences humaines qu'il trouve dans la forme dans laquelle elle est, contradictoire. "Ce qui me faché contre l'humanisme, c'est qu'il est désormais ce paravent derrière lequel se réfugie la pensée la plus réactionnaire. " Il est contre [[Jean-Paul Sartre]] par exemple. Il veut la mort de l'homme tel qu'il est représenté actuellement. "A toutes les époques, la façon dont les gens réfléchissent, écrivent, jugent, parlent et même la façon dont les gens éprouvent les choses, dont leur sensibilité réagit, toute leur conduite est commandé par une structure théorique, un système. " De même, toute archive est politique car appartenant à un système précis et biaisé. 

#### Opposition entre Sartre et Foucault suite aux Mots et aux Choses
[[Opposition entre Sartre et Foucault suite aux Mots et aux Choses]]
La sortie des Mots et des Choses est controversée parmi les intellectuels de gauche.
"Ce qui me fâche contre l'humanisme, c'est qu'il est désormais ce paravent derrière lequel se réfugie la pensée la plus réactionnaire." - Foucault

Jean-Paul Sartre est un des principaux critiques de cette nouvelle théorie portant sur l’homme. Il accuse Foucault d’être « le dernier rempart que la bourgeoisie puisse encore dresser contre Marx ». Selon Sartre, Foucault n’agit pas en tant qu’archéologue, mais en tant que géologue, puisqu’il n’explique pas comment les changements de l’épistémè se passent, ni pourquoi les hommes passent d’une pensée à l’autre.

Michel Foucault se défendra dans des écrits ultérieurs, afin de dénoncer une certaine hypocrisie liée à la notion de l’humaniste.
### Pouvoir et Gouvernementalité 
La vérité est toujours la mise en oeuvre d'une violence qu'on impose aux choses. L'*Ordre du discours*. Le thème d'une volonté de savoir permet à Foucault de penser le jeu de vérité comme un système d'exclusion, de montrer son visage d'ombre. Cette idée d'une vérité comme entreprise tyrannique de domination n'est jamais réfléchie par la philosophie. Pour Foucault, toute philosophie depuis [[Aristote]] repose même sur le déni de cette dimension. Puis on arrive directement à [[Friedrich Nietzsche]] qui détruit le modèle de vérité neutre par l'énoncé de quatre principes : un principe d'exteriorité (derrière le savoir se cache autre chose que le savoir : un jeu tyrannique d'instincts), un principe de fiction (la vérité n'est qu'un cas particulier de l'erreur général), un principe de dispersion (la vérité ne dépend pas de l'unité d'un sujet, mais d'une multiplicité de synthèses historiques), un principe d'événement (la vérité ne définit pas un ensemble de significations originaires mais constitue à chaque fois une invention singulière). Système judiciaire de la [[Grèce Antique]] : ne considère pas à exposer la vérité mais à exposer les suspects aux dieux. C'est une épreuve de vérité. Bientôt, on préférera la présence de témoins. 

Au [[Moyen-Âge]], la justice est toujours une épreuve, un test de l'assise sociale des suspects et de leur résistance (torture). Il y a un rapport de force entre les individus et le juste apparaît après un combat. Au XIIe siècle, les monarchies ne supportent plus ces combats et préfèrent l'usage des enquêtes, dont la forme sera reprise de l'[[Inquisition]]. 

### Surveiller et Punir
Foucault étudie ensuite les pratiques pénales. C'est ainsi qu'il analyse dans ce livre la manière dont la prison devient une pratique punitive généralisée (XIXe siècle) à la suite de la mise en place progressive d'une société disciplinaire. Cet ouvrage fera aussi scandale, dans la mesure où il prétend démontrer que la fonction de la prison n'est pas d'empêcher la délinquance mais de l'entretenir. Il y a 4 types de sociétés punitives, celles qui excluent, qui organisent un rachat, qui marquent et celles qui enferment. Les procédures pénales révèlent a chaque fois une forme de pouvoir déterminée. Aujourd'hui on juge de plus en plus la personne que l'acte. Alors qu'avant on voulait être certain de l'acte avec des preuves et des témoins aujourd'hui nous avons en plus une analyse psychologique de la personne. D'ailleurs on ne peut pas être coupable et fou car tout criminel est classé non responsable en situation de démence. C'est la création d'une âme criminelle qu'il veut raconter. On considérait dans l'[[Ancien Régime]] que la prison n'était pas une peine valable puis en 1831, en sachant déjà que c'était inefficace pour les détenus qui en sortent, on rend la prison quasiment universelle. Au XVIIIe on change la façon de voir le crime, ce n'est plus un sacrilège mais un manquement par rapport a la société. Il faut donc que les peines soient socialement visibles. Il faut montrer l'exemple. Avant ça le criminel était offensant pour l'Eglise ou le Roi, et donc le supplice était la pour réparer la faute par rapport à cette Autorité. Or dans ce nouveau cadre d'analyse, la prison n'etait jamais mentionnée. Pourtant, cette forme est devenue prédominante pendant le XIXe siècle. D'autant que l'emprisonnement était très lié a l'arbitraire en France car on enfermait que sous Autorité royale. Il explique que cette forme prendra son ampleur du fait de son adéquation philosophique avec nos sociétés occidentales. 

Cela vient pour lui de la [[Discipline]], l'idée politique d'uniformisation ou de catégorisation des corps. C'est un art politique de répartition des corps dans l'espace, de contrôle de l'activité et des forces etc... Le pouvoir investit le corps comme morceau d'espace, comme noyau de comportement. Il étudie la microphysique du pouvoir, le pouvoir au niveau des processus mineurs qui cernent et investissent les corps. Il y a toujours une micro pénalité dans les usines, les écoles pour extraire la bonne conduite des corps. Ou plutôt la conduite normalisée. C'est en ça qu'il oppose la loi et la norme. Jusqu'à l'âge classique on aurait respecté la Loi mais aujourd'hui nos sociétés fonctionnent plus par la norme. La Loi n'intervient que ponctuellement, lors d'infractions, de manquement à celle-ci. La justice n'intervient pas dans le domaine du permis, elle est invisible aux innocents. La norme est plus insidieuse car recherche une contrôle totale de la conduite des individus, comme une direction normée. Elle n'intervient pas par ponctualité parce qu'elle se veut totalitaire et omniprésente. C'est la différence entre l'enquête du moyen Âge et l'examen moderne, on cherche l'individu, son respect continuel de la norme et non la ponctualité criminelle. La médecine moderne s'inscrit complètement la dedans de par la prévention : elle s'immisce (en bon) dans nos vie pour prévenir les moments ponctuels. Il y a des catégories d'anormaux : le monstre criminel, l'onaniste dégénéré et l'individu à corriger. La psychiatrie se pose comme instrument d'hygiène social. C'sts évidemment très lié à la surveillance car il faut savoir comment les individus se comportent. La surveillance c'est une machine du pouvoir. 

La [[Panoptique]], prison en rond avec une tour au centre. Les cellules sont dans les cellules autour et ont des fenêtres des deux côtés pour sur le surveillant au centre dans la tour puisse tout voir par transparence. C'est l'opposé du cachot car ici on laisse la lumière et on ne cache plus. La visibilité est un piège. "De là, l'effet majeur du Panoptique : induire chez le détenu un État conscient et permanent de visibilité qui assure le fonctionnement automatique du pouvoir. Faire que la surveillance soit permanente dans ses effets, même si elle est discontinue dans son action; que la perfection du pouvoir tende à rendre inutile l'actualité de son exercice ; que cet appareil architectural soit une machine à créer et à soutenir un rapport de pouvoir indépendant de celui de l'exercice ; bref que les détenus soient pris dans une situation de pouvoir dont ils sont eux-mêmes les porteurs". Intérioriser la surveillance sans violence. Ce principe est très adapté et lié à la démocratie (voir dits et écrits tII p722 tIII p195). L'existance des [[Livret d'ouvrier]]. Criminaliser les plaisirs des ouvriers. 

Il revient sur les prisons en décrivant sa fonction comme étant une fabrique à délinquance. C'était compris dès le début comme ça et ça a servi comme tel pour combattre la bourgeoisie. (J'ai pas compris je crois). 

Dans la monarchie ou la théocratie, l'obéissance au directeur est une fin en soi. 

### Les pratiques de subjectivations 
Notre façon de nous percevoir comme un sujet dépend de plusieurs facteurs, le rapport au savoir et le rapport au pouvoir. Les sciences humaines accompagnent le pouvoir pour créer une norme à suivre et pour discipliner les gens. Il s'agit pour eux de créer un dire-vrai. La subjectivation c'est avoir le courage de dire-vrai nous-mêmes. D'aller contre les rapports au pouvoir microscopiques. 

### Sexualité

Les dernières recherche de Foucault portent sur l'histoire de la [[Sexualité]] et de la formation, par la médiation des pratiques confessionnelles chrétiennes, d'un sujet de désir.  Le [[Capitalisme]] veut toute la force des travailleurs, ceux-ci ne doivent être occupés que par le travail. La sexualité et les plaisirs sont bannis des possibles et ce que ce soit pour l'homme pauvre que pour la femme. En Orient, la sexualié est avant tout le plaisir. En Occident, c'est plus une question de questionnement sur l'identité et nos désirs.

Les livres de médecines grecs ne parlent pas de normes dans la sexualité. Il ne parle pas non plus de problèmes de sexualité en dehors du couple pour un homme. 

Concept de [[Parrhèsia]]. Et d'[[Iségorie]]

### Autres


## Référence

## Liens 
[[Foucault et Bourdieu face au néo-libéralisme]]

