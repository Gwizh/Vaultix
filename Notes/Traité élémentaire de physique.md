MOC : 
Source : [[Traité élémentaire de physique]]
Date : 2023-04-13
***

## Résumé
Livre d'[[Adophe Ganot]] publié la première fois en 1839 (j'ai lu l'édition de 1868) qui vise à résumer les connaissances scientifiques en physique de l'époque. 

Le gros point faible du livre selon est ce qu'il appelle des "agents physiques" pour expliquer l'attraction, la lumière, la chaleur, le magnétisme et l'électricité.

L'électricité notamment est la moins bien mairtisée des notions. Il se sert des théories de Dufay, [[Benjamin Franklin]], [[Robert Symmer]] mais ne peut conclure sur le fait que l'électricité soit constitué d'un fluide ou de deux. Il pense que la "découverte" récente de l'"éther" aidera à expliquer l'électricité. Il prend l'hypothèse que l'électricité est composée de deux types, comme [[Charles-François de Cisternay Dufay]]. 

C'est pour ça qu'ils expliquent que "Deux corps chargés de la même électricité se repoussent, et deux corps chargés d'électricités contraires s'attirent."

On peut faire l'expérience avec la pendule électrique. On charge un objet avec une électricité connue et on la frotte avec un autre objet. On verra que l'autre objet est chargé de l'électricité contraire. Si on les présente ensemble, la balle de sureau ne bouge pas, les énergies s'annulent. 

Ça dépend aussi des frottements, celui qui a la surface la plus polie prend la charge positive. En gros, c'est la surface dont les molécules peuvent se déplacer plus librement qui prend la charge positive

#Adam C'est super intéressant d'avoir la notion de frottement et de charge. Il veut la liquidité et ne jamais avoir de frottement, être le plus poli possible. Il va appliquer malgré lui ces principes à l'économie et au management. 

A checker : 
	Aepinus et Becquerel


## Référence

## Liens 