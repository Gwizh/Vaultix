MOC : 
Source : [[Alain Finkielkraut]]
Date : 2023-04-13
***

## Résumé


##### Émission de 2023 sur le niveau à l'école 
Il est certain que le niveau s'est effondré. Il l'avait dit dans un livre. Mais il dit aussi que la capacité de penser, de faire des efforts intellectuels est mort aussi. 

Il parle d'un livre de [[Pierre Bourdieu]], [[Les Héritiers]] sorti en 1964 et qui a selon lui inspiré énormément de réformes de l'Education nationale. Il dit que la République a permis d'enrayer la prédominance des bourgeois par la sélection à l'école. Bourdieu dit que le mérite est un leurre et dit que les inégalités scolaires sont une partie des inégalités sociales. Il dit que l'école de la République est un lieu où les gens aisés et les mieux lotis réussissent. Puis ce système permet de légitimer la place des gens qui ne réussissent pas car ils sont en échec scolaire, honte à eux ! Ça a selon lui traumatisé l'institution qui a baissé le niveau pour ne pas avoir cette sélection injuste. 

Pour Finkiel, pas de sélection signifie que les élèves "les plus faibles" peuvent aller dans des classes olis avancées et ça fait effondrer le niveau global. De plus, les bourgeois son effectivement les meilleurs éléments car au lieu d'aller dans ces écoles médiocres, ils peuvent aller dans des écoles privées d'excellence trop chères pour les prolos. 

Et puis ce capital culturel qu'on heritait s'est perdu avec en plus l'arrivée du [[Wokisme]] qui est une culture dominante (selon lui) et qui mène à l'arrogance d'un présent qui croit avoir trouvé la solution quitte à fermer les yeux sur le passé. 

Pierre Bourdieu a produit l'Ecole qu'il dénonçait. 

## Référence

## Liens 