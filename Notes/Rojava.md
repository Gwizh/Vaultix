MOC : 
Source : [[Rojava]]
Date : 2023-06-08
***

## Résumé
Le Rojava (« l'ouest » en kurde), est une région rebelle autonome de fait dans le nord et le nord-est de la Syrie.

Depuis 2012, la majorité du Kurdistan syrien est contrôlée par des milices kurdes. En novembre 2013, des représentants kurdes, arabes, assyriens et d'autres minorités ont formé un gouvernement _de facto_ dans la région autoproclamé sous le nom d'**Administration transitoire intérimaire**. Deux millions de Kurdes vivent sur ce territoire.

Les partisans de la région soutiennent une politique laïque, fondée sur des principes démocratiques, une forme de socialisme démocratique, l'égalité des sexes et l'écologie, qui transparaissent dans sa constitution.

![[Détruire le capitalisme - Lordon et Bookchin, une discussion croisée#^deb746]]

## À lire
#Alire
La Fascinante démocratie du Rojava
https://www.decitre.fr/livres/la-fascinante-democratie-du-rojava-9791093784175.html

Pour l'instant j'ai un article sur Mediapart : https://blogs.mediapart.fr/nestor-romero/blog/110620/la-fascinante-democratie-du-rojava

On découvrira que le Contrat social de la Syrie du Nord est un réservoir d’idées. Aussi qu’il met en place une démocratie complexe susceptible d’intéresser aussi bien les démocrates que les révolutionnaires.

_**Au même titre ?**_

Non ! Les démocrates ne sont pas disposés à instaurer le confédéralisme démocratique, qui est la négation de la démocratie parlementaire et de l’[[État]]. En revanche, ils trouveront dans le Contrat social des pistes pour rénover un système de représentation usé et déconsidéré. Un exemple : la constitution d’une assemblée fédérale (nationale) unique, composée de 60 % d’élus au suffrage direct et de 40 % de délégués désignés par la société civile.

Pour les révolutionnaires, son intérêt tient à la question de l’État. L’expérience de la Fédération de la Syrie du Nord illustre en grandeur réelle les difficultés rencontrées pour se passer de l’État, ou tout au moins le réduire à des tâches fonctionnelles telles que la diplomatie. Elle conduit à abandonner toute position dogmatique pour réfléchir à tous les aspects de la société future (droits civils et libertés fondamentales, travail et emploi, santé et protection sociale, sécurité publique et défense, impôts et budget, économie et consommation, etc.). Cette démarche est sans doute la meilleure façon de trouver les procédures qui conduiront à l’élimination de l’État et à une auto-administration de la société émancipée prévenant toute résurgence étatique. Comme le fit d’ailleurs [[Murray Bookchin]] avec sa stratégie de marginalisation de l’État et d’élaboration de chartes municipales qui, dans les années soixante-dix, déconcertèrent bien des [[Anarchie|anarchistes]].

Anarchiste, car elle prend l’anarchisme comme grille de lecture, laquelle est particulièrement opérante sur la question de l’État, avec ce théorème selon lequel plus on attend pour détruire l'État, plus vite il se reconstitue. Ce qui autorise à poser deux questions liées : pourquoi l’Administration autonome, depuis sept ans, n’est-elle pas venue à bout de l’État, pour quelles raisons subsiste un proto-État incarné par des exécutifs qui se renforcent, alors qu’ils devraient dépérir ? ^71e051

Constructive, parce qu’elle dépasse la rigidité de l’orthodoxie théorique pour s’intéresser à la dynamique du mouvement, l’apprécier dans sa complexité, même si à la lecture du livre certains trouveront la critique sévère. Elle l’est. Mais c’est tout le contraire d’une entreprise de démolition. Par une lecture appliquée du [[Contrat social]], une observation minutieuse de sa mise en œuvre, un regard attentif au respect de l’esprit du confédéralisme démocratique, cette contribution s’inscrit dans le processus révolutionnaire. Elle ne perd jamais de vue les efforts et les sacrifices consentis par les populations de la Fédération. Elle respecte la détermination de ceux qui, investis d’un mandat communal ou fédéral, conduisent la lutte avec abnégation et engagent leur responsabilité devant l’histoire.

_**Le Contrat social est-il une constitution ?**_

Le Contrat social de la Fédération démocratique de la Syrie du Nord énonce les droits et libertés des citoyens en se référant aux chartes internationales sur les droits de l’homme, comme la plupart des constitutions. Il met en place des institutions politiques de représentation par des assemblées et de gouvernement par des conseils exécutifs, comme toutes les constitutions. Pourtant le Contrat social n’est pas exactement une constitution.

Sur le terrain du droit international, une constitution s’applique dans un État et les trois régions kurdes réunies aux quatre régions arabes libérées ne prétendent pas constituer un État et ne veulent pas en être un. Elles souhaitent rester une fédération de régions autonomes dans une Syrie démocratique, elle-même dotée d’une constitution républicaine, laïque et fédérale.

En effet, pour beaucoup, et notamment les lectrices et lecteurs de cette revue, le Contrat social paraîtra, au premier abord, allier deux systèmes politiques incompatibles. Le [[Communalisme]], inséparable de la [[Démocratie directe]], c’est-à-dire du gouvernement du peuple, par le peuple et pour le peuple, est le contraire du [[Parlementarisme]] et de son creuset, le [[Capitalisme]], gouvernement par la représentation et l’argent. Parlementarisme et capitalisme ne peuvent s’accommoder ni du municipalisme ni de la démocratie directe qui les privent de leurs pouvoirs politiques et économiques, sauf à les admettre, édulcorés et à doses homéopathiques, pour calmer la revendication citoyenne.

L’exécutif de la Fédération de la Syrie du Nord tire sa légitimité d’un consensus entre les différents segments de la société, et non d’une élection législative ou d’une procédure de désignation par des assemblées générales. ==Si ce « gouvernement » n’est pas démocratique au sens de la science politique==, s’il ne l’est pas davantage au regard des procédures de désignation du Contrat social, il n’est pas pour autant une dictature. Le meilleur exemple est certainement le respect des droits humains et des libertés fondamentales par les autorités locales et fédérales qui n’a rien à envier aux démocraties occidentales, avec les mêmes accidents ponctuels. On pourrait aussi parler de la justice fondée sur le consensus, la conciliation plutôt que la répression, et de cette particularité, une justice autonome des femmes. De l’éducation en recherche de pédagogies anti-autoritaires, de l’autogestion des universités et des enseignements dans les langues maternelles (arabe, kurde ou syriaque). Et de bien d’autres sujets, comme ce droit civil ou ce droit pénal déconstruisant le droit coutumier.

C'est un organisme en guerre.

_**On a oublié de parler du capitalisme. Quel sort lui est-il réservé ?**_

Dans la théorie du confédéralisme démocratique d’[[Abdullah Öcalan]] comme dans le municipalisme libertaire de Bookchin, le capitalisme est la source de tous les malheurs de la société plus encore que l’État qui n’en est que le servant. Dans le Contrat social, _« le droit à la [[Propriété privée]] est garanti »_. La question du capitalisme n’est pas, apparemment, à l’ordre du jour. J’ai essayé de comprendre les raisons de cette survivance. Dans l’immédiat, il s’agit autant de préserver la petite propriété agricole ou artisanale que de résoudre des problèmes d’exploitation industrielle comme, par exemple, l’extraction pétrolière qui nécessite la participation de multinationales étrangères. La suppression du capitalisme n’est cependant pas enterrée. L’idée est qu’il sera progressivement remplacé par l’économie sociale comme l’État le sera par l’auto-administration des communes fédérées.

## Référence

## Liens 
[[Affaire du 8 décembre 2020]]