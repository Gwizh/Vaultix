MOC : 
Source : [[Lipœdème]]
Date : 2023-06-14
***

## Résumé
Alors que le lipœdème, aussi appelé « maladie des jambes poteaux », est reconnu par l’[[OMS]] depuis 2018, [[Assurance maladie]] impose un parcours dur aux malades, essentiellement des femmes, au risque de leur santé physique et mentale.  

Le lipœdème est une maladie chronique, touchant essentiellement les femmes, reconnue par l’OMS depuis 2018 et enregistrée dans la classification internationale des maladies. Mais en France, contrairement à l’Allemagne ou au Royaume-Uni, les frais de santé dus au lipœdème ne sont pas pris en charge par l’assurance-maladie, car il n’est pas reconnu comme une maladie. La liposuccion, pour l’instant seule solution pour soulager les malades, est considérée par la Sécurité sociale comme de la chirurgie esthétique, et non réparatrice.

## Référence

## Liens 