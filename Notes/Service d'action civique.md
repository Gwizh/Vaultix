MOC : 
Source : [[Service d'action civique]]
Date : 2023-06-11
***

## Résumé

Le service d'action civique (SAC) a été, de 1960 à 1981, une association au service du [[Charles de Gaulle|général de Gaulle]] puis de ses successeurs gaullistes. Elle est créée à l'origine pour constituer une « garde de fidèles » dévouée au service inconditionnel du général après son retour aux affaires en 1958. Ses statuts mentionnent qu'il s'agit d'une « association ayant pour but de défendre et de faire connaître la pensée et l'action du général de Gaulle ». Le SAC est issu du service d'ordre du [[Rassemblement du peuple français]] (RPF), qui s'était régulièrement opposé au service d'ordre et aux militants [[Communisme|communistes]] dans des affrontements violents, de 1947 à 1955.

Fondé pendant la [[Guerre d'Algérie]] et les troubles qui l'accompagnent en métropole (attentats du [[FLN]] puis de l'[[OAS]]), le SAC est marqué par cette ambiance originelle de violences.

Le SAC est souvent présenté comme une « police parallèle » du régime gaulliste. Plusieurs de ses membres sont mis en cause dans des affaires de violences électorales et de droit commun. Il est parfois confondu avec les « barbouzes », groupe de 200 à 300 individus recrutés par Lucien Bitterlin et Pierre Lemarchand pour lutter contre l'OAS en Algérie à partir de 1961. Le SAC n'est pas engagé dans la lutte contre l'OAS.

Le SAC est finalement dissous par [[François Mitterrand]] en 1982 après la tuerie d'Auriol, survenue le 18 juillet 1981, au cours de laquelle le responsable de la section locale des Bouches-du-Rhône et cinq personnes de sa famille sont assassinés par des membres de sa section qui le soupçonnent de trahison.

## Référence

## Liens 