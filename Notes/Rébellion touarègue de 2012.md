MOC : 
Source : [[Rébellion touarègue de 2012]]
Date : 2023-07-02
***

## Résumé
L'insurrection malienne de 2012 (aussi désignée sous le nom de rébellion touarègue ou soulèvement touareg de 2012) ou guerre de l'Azawad est un conflit armé qui, au nord du [[Mali]], oppose l'armée malienne aux rebelles touaregs du Mouvement national pour la libération de l'Azawad (MNLA) et au mouvement salafiste [[Ansar Dine]], alliés à d'autres mouvements islamistes. Elle a été déclenchée le 17 janvier 2012 avec l'attaque de camps militaires maliens dans le nord du pays par des combattants du [[MNLA]]. 

## Référence

## Liens 