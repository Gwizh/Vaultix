MOC : 
Source : [[Gouvernement Jules Ferry (2)]]
Date : 2023-06-09
***

## Résumé
Formation le 21 février 1883. 

Liquidation du "[[Léon Gambetta|gambettisme]]"

Déblocage de crédits pour le Tonkin, création du Conseil Supérieur des colonies. 
Le 30-31 octobre, [[Georges Clemenceau]] interpelle le gouvernement sur la politique coloniale. La chambre renouvelle sa confiance au ministère Jules Ferry. 

[[Loi Méline]]
[[Loi relative à la création des syndicats professionnels]]

## Référence

## Liens 
[[Jules Ferry]]