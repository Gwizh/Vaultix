MOC : 
Titre : [[Faut-il répondre à Éric Zemmour sur le terrain de l’histoire]]
Date : 2023-09-17
Tract Gallimard
***

## Résumé
Il dit que le premier roi de Jérusalem était français, en 1099.

Or ce lien minore évidemment les violences médiévales contre les juifs, qu’Éric Zemmour ne fait qu’évoquer en passant : quelques juifs sont brûlés, on expulse puis on annule l’expulsion, explique-t-il. Pourtant, ces violences sont tout sauf anecdotiques et constituent même le cœur de la construction monarchique française. La naissance de la nation française se fait largement au détriment des juifs et d’ailleurs  de toute autre minorité religieuse : le roi très-chrétien français expulse et spolie les juifs de son royaume afin d’y affirmer son droit de vie et de mort, puis il les réintègre jusqu’à leur expulsion finale en 1394. La violence que représente un pouvoir qui se  croit investi d’un destin privilégié et qui cherche à unifier religieusement son territoire est fondamentale. La « souillure » que représentent les juifs pour Louis IX – des hommes et des femmes bien réels, et non pas les modèles mythiques de David et Salomon – justifie des persécutions politiques récurrentes. Éric Zemmour les
minimise, préférant rêver à un nationalisme anachronique.

Rejetant toute repentance, Éric Zemmour ne pleure pas les milliers de protestants,
hommes, femmes et enfants assassinés par des catholiques en 1572. Il regrette qu’on
n’ait alors pas fini le travail : la « vague » protestante était très haute, « la Saint-
Barthélemy l’a repoussée mais ne l’a pas brisée » (Destin français, p. 332).
Plongeons dans sa lecture du massacre. ==À ses yeux, les musulmans sont les nouveaux
huguenots ; l’enjeu souterrain de la Saint-Barthélemy, c’est donc le prétendu grand
remplacement.== Le massacre est un geste de résistance, par lequel la France catholique
refuse de céder devant l’envahisseur protestant, venu d’Allemagne – même si Calvin
est en réalité picard. Il faut donc en finir par la force avec les minorités avant que
celles-ci n’en finissent avec nous. Comme toujours, Éric Zemmour inverse les rôles,
transformant les victimes en bourreaux. À ses yeux, le massacre est la conséquence du
« fondamentalisme huguenot », la réponse justifiée à l’arrogance des protestants
« intolérants, persécuteurs de catholiques » (Mélancolie française, p. 25). Que les
huguenots n’aient jamais représenté plus de dix pour cent du royaume et aient fort peu
massacré ne gêne guère sa démonstration. Force est de constater pourtant que la
Michelade de Nîmes (1567), aussi tragique soit-elle avec quatre-vingts morts
catholiques, n’a rien de comparable avec les dix mille victimes de la Saint-Barthélemy
ni même avec les nombreux massacres antérieurs de protestants (Wassy, Tours, Sens,
Amiens, Toulouse, etc.).
==Les musulmans sont les nouveaux protestants==

#### 1756 – LES NAZIS NE SONT PAS LES HÉRITIERS DE VOLTAIRE
> « Les Chamberlain, Gobineau, Rosenberg ne sont pas les produits odieux des anti-
Lumières, mais les fils des Lumières. Pas les rebelles contre Voltaire, mais ses enfants
dégénérés. Les bâtards de Voltaire ! »
Éric Zemmour, Destin français, op. cit., p. 168

Pour Éric Zemmour, Voltaire est l’une des figures les plus répulsives de l’histoire de
France. Au fil des ouvrages, tout y passe, des poncifs les plus éculés jusqu’aux
allégations les plus vulgaires. Anglomane et donc anti-Français, Voltaire apparaît
comme le jouet de la Pompadour, symbole d’un XVIIIe siècle présenté comme le « siècle
des femmes ». C’est le moyen pour Éric Zemmour de conjuguer sa misogynie à l’idée
du déclin inexorable d’une France des Lumières entraînant la Révolution.
Entendons-nous bien : derrière la figure paternelle et souriante de la statue de
Houdon, on découvre le financier prêt à investir d’importantes sommes d’argent dans
le commerce négrier. Le Grand Homme n’est pas exempt des préjugés et des pratiques
les plus critiquables de son temps. Or, si l’histoire sert à déconstruire le mythe de
Voltaire pour rendre compte des ambivalences des Lumières, ce travail est bien éloigné
de la démarche d’Éric Zemmour qui ne vise qu’à ridiculiser le personnage et en
caricaturer la pensée.
En 1756, dans son Essai sur les mœurs et l ’esprit des nations, ==Voltaire met en
cause la supériorité de la civilisation européenne et dénonce l’égoïsme des nations==.
Revendiquant le recours à des méthodes sérieuses et à l’étude de documents fiables, il
montre que le moteur de l’histoire des peuples et des civilisations repose sur leurs
capacités à échanger et à commercer. Contre cette idéalisation nostalgique du passé et
une conception cyclique de l’histoire, cette sécularisation pose les jalons d’==une histoire
non européenne, voire décentrée==

#### 1789 – LA RÉVOLUTION FRANÇAISE N’EST PAS UN COMPLOT
Penser que la Révolution aurait été planifiée et dirigée par une minorité univoque
interdit toute intelligence de la période. Éric Zemmour renoue ici avec les discours
paranoïaques et apocalyptiques du siècle dernier qui voyaient dans la chute de la
Bastille et de la monarchie le résultat d’un plan ourdi de longue date par une clique de
protestants, de juifs et de francs-maçons.

#### 1793 – IL N’Y A PAS DE GÉNOCIDE VENDÉEN
 
## Références
[[Éric Zemmour]]
## Liens 