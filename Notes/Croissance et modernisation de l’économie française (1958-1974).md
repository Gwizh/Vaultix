[[Livre]]
MOC : [[Histoire]]
Titre : [[Chapitre]]
Auteur : 
Livre de base : [[Histoire de la France au XXe Siècle - Livre 3]]
Date : 2022-08-05
***

<-- Chapitre Précédent : 
--> Chapitre Suivant :

## Résumé

### 1- L’héritage de la IVe République
Fin de la guerre : déficit budgétaire, mauvaise balance commerciale extérieure. 
4e Rep : bloquée dans les guerres coloniales et l'instabilité politique
Charles de Gaulle : "J’ai trouvé les caisses vides !"

Mais création d'institutions donnant une grande marge de manoeuvre à l'État. Un service public très puissant avec de nombreuses nationalisations. Une sécurité sociale qui permet une amélioration constante du niveau de vie. Et une mentalité d'étude des problèmes économiques. 

## Notes

## Citations

## Passages amusants

## Références
