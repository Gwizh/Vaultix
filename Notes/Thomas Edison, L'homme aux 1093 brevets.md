MOC : 
Titre : [[Thomas Edison, L'homme aux 1093 brevets]]
Date : 2023-08-24
***
[[Philippe Zoummeroff]]
## Résumé

### Chapitre 5 : Un certain monsieur Faraday
De Magnete de [[William Gilbert]] en 1600.
1704, [[Otto von Guericke]] : premier générateur électrique.
Expliqué dans l'autre livre avec [[Charles-François de Cisternay Dufay]]
1746 : Bouteille de Leyde - premier condesateur
Dans Versailles, expérience de l'Abbé Nollet
Expérience de Franklin
[[Charles de Coulomb]] : quantifier une charge électrique
Fin du siecle : expériences de galvani sur des grenouilles.
Volta et la pile en 1800.
Loi de Joule
"Éduqué par son pere selon les principes rousseauistes, [[André-Marie Ampere]] se formera dans la bibliotheque familiale."
[[Georg Simon Ohm]] loi d'ohm
[[Humphry Davy]] : vérifie les lois de Lavoisier
Puis galvanoplastie de Jacobi
Davy encore : Arc électrique.
1844 [[Léon Foucault]] : tenter d'illuminer la place de la Concorde avec des arcs électriques.
Oersted : rapport entre électricité et magnétisme.

Puis Faraday
Famille pauvre, peu d'éducation, a lu tout un tas de livres sur l'électricité (tout ce qu'il trouvait). Puis a pu avoir des cours de [[Humphry Davy]]. Ce dernier l'engagea et ils firent un voyage de 18 mois ou ils rencontrent plein de chercheurs de l'époque.
[[Charles Wheatstone]] et [[William Crooke]] mais finalement le systeme [[Samuel Morse]].

[[Cyrus Field]] : histoire de la ligne télégraphique transatlantique. 1852 - 1858 - 1866.
Capital de 1 500 000 $, ça sort pas de nulle part mdr.

### Chapitre 6 - Merci Monsieur Mackenzie
Un jour il sauva le fils de monsieur mackenzie qui en échange lui apprit tout ce au'il savait sur le télégraphe et l'employa. Il progressa tres vite et alla ensiute dans des compagnies plus importantes. Étudier, faire des expériences et mener une existence sobre.

### Chapitre 7 - Le télégraphiste itinérant
Ce qu'on sait de tout ça ne nous vient quasiment que de ses correspondances et de ses notes, donc évidemment c'est exagéré.
Il travailla pour la Western Union, grande entreprise de télégraphie. En 1861, la premiere ligne de télégraphie relia la Californie et le Missouri. Expérience de paralysie de rats. wtf.
Avec l'argent, il a failli aller au Brésil pour y installer le télégraphe mais se ravisa.
### Chapitre 8 - Boston
Rencontre avec [[George Milliken]] et aménagement a Boston
"Il s'amusait a tuer des cafards avec sa machine a paralyser des rats."
Premier brevet d Edison : enregistreur de vote. Puis enregistreur des cours de bourse
### Chapitre 9 - Pour un dollar
1869
RAV mais encore machine pour mesurer le cours de la bourse. Ici l'or surtout. Gold Room. Il répara la machine et se fit engagé pour plus qu'il n'avait jamais eu. Avant ça, il était pauvre comme jamais. C'est vraiment romancé on dirait. Brevet 96 567. 
### Chapitre 10 - Faites moi une offre général
Pope, Edison and Co : ingénieur électricien.
Télégraphe imprimant very big in the 1870.
Notamment le cours de la bourse
Ce qui est fou c'est que c'est vraiment dit qu'il a battit son empire avec un dollar dans la poche en arrivant a New York
Gold and Stock
On lui ouvrit un compte en banque quasi
### Chapitre 11 - Les cinq contrats
Entre 1870 et 1872 : laboratoire et pleins d'inventions
> "La Western Union était le mécène avoué de tous les grands inventeurs de l'époque. Elle avait réalisé le mariage parfait entre le capitalisme et la science. Loin d'être philanthrope, cette union n'existait que par le désir de capter les compétences pour qu'elles n'aillent pas a la concurrence."

On lui vend des actions et du cash en échange des inventions, systeme de morts jipepe.

Je me fais chier alala

> "J'ai en tete un nouveau systeme de télégraphe automatique, mais celuici sera pour moi, rien que pour moi et pas pour ces foutus capitalistes."
Edison après qu'on le pressa pour des questions d'argent.

Faut que je trouve qui est Harrignton

### Chapitre 12 - Mary Stilwell
"Les femmes pour Edison étaient un phénomene aussi étrange que l'invention lui semblait naturelle."
Tres rapide en 1871 c'était plié.
Edison continua sa vie comme si elle n'existait pas. Il ne lui parlait pas de ses inventions ni de ses problemes. "Ma petite Popsy-Wopsy ne pourra rien inventer."

### Chapitre 13 - Un grand patron
[[Charles Batchelor]], son plus grand collaborateur. Parlaient [[Shakespeare]] et poésie.
[[John Kruesi]], bon horloger
C'était the Old man.
> "Quand après une longue période de travail il était satisfait de ses hommes, il leur offrait une partie de pêche. Edison était non seulement maitre de leur travail, mais aussi de leurs loisirs : un vrai potentat."

### Chapitre 14 - Des moments difficiles
Dettes et dépenses lourdes : un peu de réussite pour racheter les parts. Se fit aider par de riches bourgeois qui l'aimaient bien.
> "Une nuit, je trouvais Edison assis a cote d'une pile de livres haute d'un mètre cinquante. Il les avait fait venir de New York Paris et Londres et se mit  en devoir de les lire nuit après jour. Il mangeait sur son bureau et dormait dans son fauteuil. Quoi qu'il ne parlât ni le français ni l'allemand, grâce aux dictionnaires et a sa prodigieuse mémoire il arrivait a comprendre l'essentiel. En six semaines, il avait avalé le tout, en avait extrait un volume de notes avait fait deux mille expériences sur diverse formules et trouvé la solution."

Brevet 151209

### Chapitre 15 - La télégraphie multiple
En 1875, il avait mit derriere lui ses concurrents sur le télégraphe automatique. Aubain pour la Western Union, moins d'opérateurs, messages enregistrés et distance plus longue.
A étudier : Rapport entre la [[Western Union]], [[Cornelius Vanderbilt]] et [[William Orton]].
Objectif : inventer le quadruplex pour vraiment prendre de l'avance. En un mois, Edison travailla 22 nuits au labo de la western union.
[[John Creswell]] voulait nationaliser le télégraphe car les prix étaient trop élevés
Vanderbilt était insatiable avec l'argent, diminuant les gages de ses employés. Il dirigeait la Western comme sa propre entreprise.
Démonstration de force d'Edison qui bat des opérateurs avec son système.
Il finit le quadruplex. Article du New York Times : "Le quadruplex est la plus grande invention télégraphique après le morse. On doit cette merveilleuse invention aux efforts conjugués de Messieurs [[George B. Prescott]] et de [[Thomas Edison]]". Prescott n'étant qu'un prête nom d'Orton. Heureusement, le nom seul d'Edison sera indiqué sur le brevet.

[[Thomas Eckert]] avait toujours une revanche à prendre non sur Orton, mais sur la Western Union. Eckert et Jay Gould s'étaient connus et appréciés pendant la guerre civile. Jay Gould avait déjà remporté une victoire sur le Commodore (Vanderbilt) lui-même, en s'appropriant l'Erie RailRoad. 
Avec le [[Crise bancaire de mai 1873]], il avait acheté pour une somme dériosoire les actions de l'union pacific. Les opérations ferroviaires allant de pair avec celles du télégraphe, il était devenu propriétaire de l'Atlantic and pacific telegraph company dont les lignes recouvraient l'ensemble du réseau de la erie et de la union pacific.
D'où le double intérêt pour Gould d'acquérir l'automatic telegraph company : d'une part agrandir le réseau télégraphique qui, grace à l'apport de l'automatic desservirait aussi bien la côte est que la côte ouest, et d'autre part acheter les brevets d'Edison et surtout s'assurer de sa collaboration.
Nouveau contrat de l'at et pac comp pour Edison : 100000$ dont 70000 en actions et 30000$ en cash.

### Chapitre 16 - Jay Gould
10 mai 1869 : Est et ouest connectés ! "Cheval de fer"
The Gilded Age, Twain, Horatio Alger, Ralph Emerson.
Archétype des Barons Voleurs : financier, homme d'affaire machiavélique : Jay Gould
Barons, car ils étaient tous, dans leurs métiers respectifs, au sommet de la hiérarchie; qu'ils fussent dans le pétrole comme [[John Davidson Rockfeller]], dans l'acier comme [[Andrew Carnegie]], dans l'alimentation tel [[Philip Armour]], dans la finance avec [[J.-P. Morgan]], ou dans le chemin de fer avec [[Collis Huntington]]. Tous étaient d'une intelligence supérieure, d'un sens aigu de l'opportunité, et pourvu d'une conscience très élastique.
Voleurs, car pour eux tous les moyens étaient bon pour gagner de l'argent.

> "Pourtant cet escroc qui aujourd'hui se verrait immédiatement envoyé sous les verrous n'était pas dénué de certaines qualités : n'ayant aucun scrupule, il était capable de saisir les opportunités au bon moment, de déjouer les lois grâce à des avocats véreux et des hommes politiques corrompus, et de prendre des risques énormes même si ceux-ci étaient parfaitement mesurés."

*Je note tout mdr*

Comme beaucoup de ces "Barons", Gould était d'extraction modeste. Dès l'âge de treize ans, il manifeste une certaine indépendance. Travailleur acharné, il se place chez un forgeron pour payer ses étudeset son logement. Contrairement à Edison, il se passione pour les mathématiques et se destine à une carrière de géomètre. En 1856, il publie une histoire du Comté de Delaware. Très tôt, obnubilé par le pouvoir et l'argent, il va consacrer a vie à devenir riche, à n'importe quel prix.
"Chose curieuse, ses ouvriers lui étaient totalement acquis car, à la différence d'autres grands barons, il les rémunérait un peu mieux."

C'était l'essor du capitalisme américain. La spéculation boursière était promise à un grand avenir et Gould considérait à juste titre que c'était la voie royale vers la richesse dans laquelle il devait s'infiltrer. Attendre la faillite pour acheter des actions nulles. Fin de guerre civile et croissance économique de fou.

Deux chevaux de bataille : spéculation boursière et développement du chemin de fer.
Atouts considérables : dévaluation du dollar papier, ventes à terme et achats à crédit, corruption de fonctionnaires et de membres particulièrement bien placés au Congrès, création de sociétés "bidons" destinées à léser les actionnaires tout en rapportant gros à leurs propriétaires, ventes de produits au Gouvernement avec des bénéfices énormes grâce à des pots de vin judicieusement distribués.

"Bien sûr la conjoncture se prêtait divinement à ce capitalisme sauvage."

En 1867, Gould rentra au conseil d'administration d'une des plus importantes compagnies de chemin de fer, l'Erié.

Gould et Fisk étaient par la nature même de leurs itinéraires, en concurrence avec le lion de l'époque, le Commodore Vanderbilt. Ce dernier s'était mis en tête d'absorber la compagnie de Gould car il possédait la New-York Central qui rejoignait Albany à Buffalo sur le lac Erié. Les millions de dollars que possédait déjà la Commodore lui permettaient de faire une opération de cette envergure. Gould savait que, s'il pouvait mettre plus de titres sur le marché, le Commodore s'essoufflerait. Or, la loi l'autorisait à deux schémas : le premier consistait à émettre des obligations qui pouvaient être converties en actions; le deuxième était d'absorber d'autres sociétés qui viendraient gonfler le capital social de la compagnie absorbante. Ces deux moyens permettaient d'augmenter, presque à l'infini, le capital d'une société. Gould émettrait des obligations qui aussitôt étaient transformées en actions et servaient à acheter les lignes secondaires.

> "Mais l'affaire était compliquée. l'état de New York comptait 32 juges; certains à la solde de Gould, d'autres à celle de Vanderbilt."

Dès la fin de la guerre civile, les paiements étaient effectués soit en papier monnaie, soit en or. Mais le cours de l'or était lié au papier monnaie, d'où l'importance d'être informé du cours de l'or. C'est alors qu'intervint la première invention rentable de Edison : l'enregistreur des cours de bourses. 
Ainsi, ce qui devait arriver arriva. Le vendredi 24 septembre fut ce qu'on a coutume d'appeler le fameux Vendredi Noir. On apprit que le Général Butterfield était intervenu, sous le couvert du secrétaire au Trésor, sur le cours de l'or. Les cours qui étaient montés artificiellement , aux alentours de 160 par rapport au billet vert de référence, s'effondrèrent à 135. Dans la salle des cours de bouses, ce fut une ^panique générale. De nombreuses personnes furent ruinées, il y eut même un suicide. bien sur, une commission fut nommée. Et comme toujours, Gould à l'abri par le nombre de procédures engagées, s'en tira fort bien. 

De ce Vendredi Noir, Edison garda en mémoire deux choses : d'une part la folie qui s'était emparée des gens qu'il croyait équilibrés, notamment le banquier Speyer qu'il fallut ramasser à la petite cuillère; d'autre part la vitesse des informations qui ne permettait plus aux indicateurs de suivre le cours.

#### Chapitre 17 - Londres
#Alire De Magnete
[[William Thomson]] et [[William Preece]]
Parti pour faire des démos mais onn lui a donné du mauvais matériel exprès. Il suspecte le monopole local.
Finalement, il trouve ce dont il a besoin et il garde contact et fait ses preuves.

#### Chapitre 18 - Le prolifique inventeur
"Edison, Batchelor et Adams ne vivaient que pour leur travail. Leurs vies de famille étaient pratiquement inexistantes. Bien qu'ils aient juré à leurs femmes de revenir au foyer au moins pour la nuit, il n'en firent rien. Ils s'amusaient à tel point qu'une véritable complicité s'établit entre eux, ce qui par la suite forma une sorte d'osmose qui facilita et accéléra bougrement le processus de recherche collective."

"Le travail était sa passion. Son esprit continuellement en éveil ne laissait que peu de place pour la vie sentimentale. À un homme d'exception, il fallait une femme d'exception ! C'est à dire une sainte. Mary, avec toutes ses qualités, n'était toutefois pas cette femme-là."

#### Chapitre 19 - Menlo Park
"Peut-être était-ce l'effet du hasard si l'édification de Menlo Park coïncide avec le centenaire de la déclaration de l'indépendance des Etats Unis. N'a-t-on pas dit aussi que Thomas Edison avec ses inventions, avait donné au monde une nouvelle forme d'indépendance."
#Alire https://archive.org/details/lalumiereelectri25unse/page/n9/mode/2up

Charles Clarke arrivé à Menlo Park le 1er février 1880 pensait qu'il pouvait se reposer le soir même. Mais non ! Edison voulut le mettre immédiatemnt dans la charmante ambience du laboratoire. Il raconte : "Je fus invité à me rendre au dernier étage du laboratoire. Là je trouvais un homme assez mal vêtu assis près de l'orgue, c'était Thomas Edison. Il regardait fixement ses assistans qui commençaient à avoir une sacrée faim. Des paniers étaient pleins de bonne choses à manger. Alors un festin s'annonçait et chacun y allait de son histoire. [...] Le bonheur était de courte durée car très vite le maître se mettait debout, son sourire avait disparu, il avait repris l'air sérieux de celui qui est en train de treir plusieurs idées sans savoir exactement laquelle devait prédominer. Ce moment, les habitués le conn aissaient bien. C'était la reprise du travail."
"Ici on travaillait dans la gaité jusqu'à l'épuisement. Certains faisaient des semaines de 70h voire 100 pour Edison, mais on était heureux comme ça."
"Les célibataires see retrouvaient le soir dans lpetite pension accueillante de Sarah Jordan. [...] Sarah avait en permanence dix à seize personnes pouvant occuper les 16 chambrse de la pension. Sarah était secondée par sa fille et une servante, Kate Williams. Elles pourvoyaient aux différents travaux, de ce qui était, en sorte, un peit hôtel : la lessive, le ménage et surtoutla cuisine, car il fallait nourrir ces gaillards qui étaient pourvus d'un solide appétit. La cuisine de Sarah était délicieuse. Le tempérament un peu brutal et ripailleur des hommes était modéré par la qualité des tartes de Sarah."
"Pratiquement tous les employés de Newark avaient suivi le maître. La manière de travailler d'Edison plaisait aux hommes. Pourtant ils étaient soumis à une vie presque monacale."

"Tous les hommes étaient des praticiens et des manuels. Edison avait horreur des mathématiques, son rejet de Newton en était la preuve. Mais l'intuition, qui était sa seconde naturelui stipulait qu'il devait s'entourer de scientifiques musclés."
Documentaliste ?
"La motivation était une garantie inappréciable pour Edison. La lettre que reçut Edison le 6 janvier 1879 de John Lawson en est un vivant exemple : 'Cher Monsieur, cela fait beintôt 4 ans que j'essaie d'étudier la chimie, mais je n'ai pas encore trouvé un laobratoire convenable. Je suis desespéré, pouvez vous m'aider ? Vous pouvez e donner n'importe quel travail, je ne sollicite que la possibilité d'étudier? Depuis mon enfance je rêve d'être chimiste. Je veux bien devenir votre esclave pourvu que vous puissiez me donner une chance." + photo du 15 sept.
"Edison avait la fierté de ses découvertes bien que bcp de celles-ci étaient dues à la persévérance de ses collaborateurs."

"Il ne parlait jamais de la qualité des hommes qui composaient son équipe car pour lui, les hommes étaient plus perfectibles qu'une machine et par conséquent, il n'était pas nécessaire de les encenser. Edison avait la fierté de ses découvertes bien que beaucoup de celles-ci étaient dues à la persévérance de ses collaborateurs. « Je ne vous paie pas pour avoir des  idées, disait Edison sans plaisanter, mais pour exécuter les miennes.» Et quand un de ses collaborateurs venait lui dire: « Je crois que j'ai une bonne idée », Edison lui répondait du tac au tac: « Cela m'étonnerait, car si votre idée avait été bonne, je l'aurais eue avant vous! » Le maître avait une très haute opinion de lui-même. Ce n'était pas forcément de l’orgueil mal placé, car lorsqu'il avait bien décortiqué un sujet, il pensait sincèrement que rien ne pouvait lui échapper. Les règles communes de l’industrie n’existaient pas à Menlo Park. La règle d’Edison était surtout de ne point avoir de règles. Celles-ci étaient réglées par les impératifs du travail, Edison et ses collaborateurs quand il le fallait transformaient le jour en nuit et la nuit en jour."

Ce Menlo Park était aussi l'enfer de Mary et des épouses des collaborateurs d’Edison. Menlo Park était sinistre et le silence de ce petit village était accablant. Leur vie austère était parfois entrecoupée par des visites de femmes de fermiers avec lesquelles elles aimaient papoter sur les  derniers vêtements à la mode qui venaient d'arriver de      New York. Mary adorait se promener et faire les grands      magasins. Elle était très élégante et aurait beaucoup aimé  se retrouver au bras de son mari dans les soirées de la     haute société New-yorkaise. Mais c'était trop demander      à son égoïste de mari qui ne se sentait bien que dans son   laboratoire. Alors, dépitée elle revenait à Menlo Park dans un joli et sophistiqué attelage que lui avait offert Tom qui croyait ainsi qu’elle n'avait pas de raison de se plaindre. 
#### Chapitre 20 - Monsieur Watson
"Les lois concernant les brevets aux Etats-Unis étaient si bien faites qu'elles facilitaient la fraude, la corruption et le parjure."

Quelque temps plus tard, en juillet 1877, Edison brevetait en Grand Bretagne son premier transmetteur à carbone. Il réalisa une expérience sur une ligne de cent quatre-vingt-cinq kilomètres entre Londres et Norwich. Ce fut un franc succès. Il démontra que son appareil était bien plus clair pour l'articulation et plus puissant par le volume que celui de Bell. On s'aperçut très vite en Anglererre qu'Edison avait prise sur la moitié des inventions du téléphone. Des capitaux avaient été levés pour former la Edison Telephone Company of Great Britain
#### Chapitre 21 - Mary a un petit agneau
Toutefois — et c'est une caractéristique d’Edison — avant d'avoir mis son phonographe au point, il n'avait pas pu s'empêcher de prévenir la presse. Le New York Herald annonçait: « Monsieur Edison, l’inventeur bien connu, à non seulement réussi à fabriquer un téléphone beaucoup plus astucieux que celui de Bell, mais il vient de défricher un domaine complètement inexploré dans l'acoustique: celui de la reproduction des sons. Son appareil est capable d'enregistrer la parole humaine et de la reproduire immédiatement ou bien des  années plus tard. » On ne peut pas dire après cela qu Edison n'avait pas un sens aigu de la publicité!

Enfin apparut Edison avec son appareil, la foule retenait sa respiration. Du phonographe sortit une voix métallique : «J'ai l'honneur de me présenter moi-même devant l'Académie des sciences. » Ce fut le délire! Puis Batchelor poussa des grognements et des cris devant l’enregistreur. Lorsque la machine répéta les mêmes sons, plusieurs jeunes femmes s’évanouirent dans l'assistance. Cette invention fit tellement de bruit que le Président Hayes, lui-même, sollicita Edison pour qu'il fasse une démonstration à l’Executive Mansion - c'était le nom que l’on donnait à l’époque à ce qui allait devenir la Maison Blanche. Le Président Hayes vint accueillir Edison peu avant minuit et ce n'est que vers quatre heures du matin 
que le Président laissa partir son illustre visiteur.

Le phonographe permit aux journalistes non spécialisés de découvrir le nom de Thomas A. Edison. Ce jeune homme de trente et un ans auquel, l’année suivante, Maclure allait consacrer une biographie, avait un passé étonnant: sa carrière, son laboratoire et près de deux cents brevets, mais plus encore l'estime de savants comme Lord Kelvin ou Joseph Henry. Gardiner Hubbard, le beau-père de Bell, invita Edison dans sa luxueuse demeure. Edison avait le don de cultiver le paradoxe. En effet quelle étrange situation! Edison était le concurrent de Bell et, malgré tout, il était sollicité par son principal commanditaire.
#### Chapitre 22 - Voyage vers Wyoming
En ce début de l’année 1878, Edison commençait à ressentir de réels signes de fatigue dus non seulement à une vie trépidante, à des horaires hors du commun mais aussi à ses continuels ennuis financiers. Il était un peu comme l'oiseau sur la branche, devant son éternel dilemme: continuer ses inventions ou faire faillite. Il n’était pas sorti de Harvard et lui fallait jongler avec une foule d'activités différentes. Car à trente ans Edison était maître de tout. Le bureau d’étude c'était lui, la fabrication c'était lui, le markéting c'était lui, la vente c'était lui, la comptabilité et les finances c'était toujours lui. Du jamais vu. En sept ans hormis son voyage de noces aux chutes du Niagara, Edison n’avait pas pris un jour de vacances.

S'il n'avait aucun scrupule à discuter aussi bien avec ses employé, qu'avec ses fournisseurs pour économiser un dollar, il » voulait pas tirer profit de tout ce qui pouvait être utile à la science. Comme il ne croyait pas systématiquement à l'avenir de ses inventions, il préférait plutôt donner ds actions à ses collaborateurs que d'augmenter leur salaire: L'avenir démontra qu’il avait complètement tort.

Edison avait retrouvé son camarade, Edwin Fox, du New Wrk Herald et fait la connaissance du fameux astronome Henry Draper dont le père avait été un pionnier dans ‘électricité. En effet ce dernier avait construit en 1847 une ampe à ilament de platine, mais il s’était apercu qu'au-delà “une certaine température la lampe s’éteignait. Draper avait “pendant constaté que l'électricité pouvait non seulement “gager de la chaleur mais aussi provoquer une certaine “ingéniosité, Bien qu’Edison soitun peu dur d'oreille, le sage n était pas tombé dans l'oreille d’un sourd.

La lumière électrique hantait les savants depuis 1808 lorsque [[Humphry Davy]] découvrit l’are électrique, juste huit ans après l'invention de la pile par [[Alessandro Volta]]. 
Les choses allaient vite. Le procédé était simple et extrêmement astucieux : deux tiges de carbone sont reliées à une pile. Lorsque le courant passe, le carbone s'oxyde et une lumière bleutée, assez vive, en forme d’arc apparait. 
Mais pendant plusieurs années les expériences furent suspendues, faute d’une source abondante de courant. La démonstration de Davy avait nécessité une pile formée de deux mille cellules. Le principe de la dynamo découvert par Faraday en 1831, basé sur le principe de l'induction, convertissait la force mécanique en énergie électrique. Déjà toute une armada de savants concevaient de nouvelles lampes à arc, mais ce furent celles d'un ingénieur russe, Paul Jablockoff, qui furent les plus spectaculaires. À Paris elles éclairèrent, sur plusieurs  centaines de mètres, l'avenue de l'Opéra.

Parallèlement à la lampe à arc, la lampe à incandescence avait eu ses adeptes. De Moleyne expérimenta en France une lampe à filament de platine, métal ayant le plus haut point de fusion connu. Mais il échoua. En 1845, un américain, JW. Starr, essaya de chauffer une lamelle de carbone placée dans le vide, mais il mourut avant la fin de ses expériences. Douze ans plus tard, un pharmacien Pritannique, Joseph Swann, prit la suite de Starr: il porta à us une très fine bande de papier carbonisé placé dans le vide, mais le vide était imparfait et le carbone s'effrita. 

Swann pensa qu'il n'était pas sur la bonne Voie, il abandonna l'électricité et s'orienta vers la photographie qui lui était plus familière. La question du vide avait certainement dû gêner tous les prédécesseurs d’Edison et, lui-même, eut beaucoup de peine à trouver le vide parfait. Aux Etats-Unis, Charles Brush, Hiram Maxim, Joseph Henry et Moses Farmer étaient les pionniers incontestables de l’électricité. Mais tout cela n'était que de l'expérimentation. Si l’on voulait éclairer une maison il fallait subdiviser la lumière électrique (voir plus bas) et, à l'époque, c'était une chose parfaitement impossible.  C'est là qu’il apprit qu'il venait de remporter le Grand Prix à l'[[Exposition internationale de Paris de 1878]].
#### Chapitre 23 - Les premiers pas vers l'impossible
En décembre 1878, Edison, accompagné de [[George Barker]], de [[Charles Batchelor]] et de [[Charles Chand]] se rendit à Ansonia dans l'usine de dynamos de [[William Wallace]]. Ce dernier Partageait avec [[Moses Farmer]] le privilège d'avoir inventé la première dynamo américaine. Les américains étaient À peu près à égalité avec les européens. Le savant anglais, [[Michael Faraday]], était suivi de près par Joseph Henry et la dynamo de Gramme par celle de Wallace et Farmer. En leur honneur Wallace avait mis en marche son Télémachon. Il s'agissait d’une
batterie de huit lampes à arc montées en série. De quoi impressionner les plus difficiles! Edison était enchanté, comme Un gamin il courait d’un instrument à l’autre, il s'installa même à une table, prit des notes, fit des calculs et estima le prix de revient et la perte d'énergie entre la dynamo et les lampes. Il considéra surtout toutes les difficultés que la lampe à arc ne pouvait résoudre, notamment, l'installation de cette lampe à arc dans les demeures privées. Il dit franchement à Wallace : "Je crois que je peux faire mieux que vous, je crois que vous ne travaillez pas dans le bon sens".

Edison était comme cela, tout d'un bloc, et malgré sa franchise, n'avait jamais l'intention de 
blesser personne. Wallace ne l’ignorait pas et ny prit pas ombrage. Cependant il connaissait bien Edison et savait qu’il ne parlait jamais à la légère.

De retour à Menlo Park, Edison se Mit au travail et
comme d habitude, il fit venir toute la documentation
concernant le gaz. Il parcourut des dizaines de livres, de
notices, de rapports, les procès-verbaux déposés par les
sociétés d'ingénieurs, les brevets. Lorsqu'il avait dit à
Wallace : « Vous ne travaillez pas dans la bonne direction »
- Sa réponse avait été purement intuitive, pourtant il savait
dans son for intérieur qu’il avait raison. Alors pourquoi
les savants faisaient-ils fausse route? La solution arriva à
Edison: limpide. Le nouveau système d’éclairage devait
avoir la simplicité, les commodités du gaz et quelque chose
en plus... Les lampes devaient être simples, légères, bon
marché, sans odeur et sans danger. Les lampes devaient
s'allumer et s’éteindre comme le gaz avec un simple

interrupteur.

Thomas Edison admira de bonne foi les éclairages de Monsieur Jablokoff et les considéra comme un tour de force sans précédédent mais en plus elle identifia son nom à la lumière électrique elle lui attribua exclusivement le mérite d’avoir pu la produire et la rendre pratique pour les besoins industriel et publics. Tous les systèmes imaginés par l’incandescence n'ont donné que des résultats négatifs. Aujourd’hui c'est le seul système qui est appelé à un avenir glorieux. On en veut pour preuve l’éclairage, à Londres du British Museum, des magasins du Louvre et de la Belle Jardinière à Paris. »

Après avoir épuisé toute la documentation possible Edison acquit la certitude que les différents chercheurs n'avaient pas utilisé tout ce que la science de l'époque pouvait mettre à leur disposition.

Sans avoir fait d'expériences probantes, Edison avait l'intuition qu'il ne pouvait que réussir. Le travail qui l'attendait était pourtant énorme, apte à décourager les meilleures volontés. Son objectif: détrôner le gaz. Ce dernier rapportait la coquette somme de cent cinquante
millions de dollars et il n’était utilisé que dans les villes. Les trois quarts de la population américaine s’éclairaient encore avec la lampe à huile ou avec la bougie. Edison ambitionnait de faire passer la lumière électrique dans le moindre foyer américain.

Il commença par étudier les moyens de production : il établit les courbes de consommation et les principes d'acheminement. Il compara le coût de la transformation du charbon en gaz d'éclairage par rapport à la quantité de charbon nécessaire à la fabrication de la vapeur indispensable pour entrainer la dynamo qui production d'électricité.

Les idées d’Edison bouillonnaient dans sa tête; ses carnets de travail se remplissaient de notes. Pendant cinq jours le « Old man » resta en transe, fumant plus de vingt cigares par jour, buvant du café et mangeant les gâteaux que Mary lui avait préparés. Un vrai repos lui aurait pris trop de temps. Il regrettait de n'être pas né sur la planète Mars « car les journées avaient quarante minutes de plus ». 

Après quelques jours de réflexions bénéfiques, il entreprit de rédiger son premier brevet concernant la lumière électrique. Le brevet portait le n° 214636 et concernait un régulateur de chaleur pour empêcher une spirale de platine, portée à très haute température de fondre. La deuxième demande de brevet concernait la subdivision de la lumière électrique en petites unités.

Un quelques semaines il avait élaboré la conception d'un système électrique très sophistiqué : « J'ai vraiment trouvé un filon » et dans la lancée convoqua la presse : J'ai réussi la subdivision de la lumière électrique, je ne comprends pas qu'un autre ne l'ait pas trouvée avant moi et je suis sûr que la lumière électrique coûtera moins cher le gaz. »

Edison était déjà une personne très médiatisée. Un reporter du New York Herald l'avait suivi jusqu’à Ansonia et avait obtenu une interview choc. Avec son imagination débordante, Edison communiquait au reporter sa vision futuriste :«Une centrale électrique alimenterait des millions de lampes. L’électricité serait transportée par fil et pourrait entrer dans la plus modeste des maisons et prodiguer ses bienfaits. La consommation des lampes serait comptabilisée — donc l'électricité pourrait être vendue. »

na même que cette invention pourrait être réalisée

M quelques semaines. D'ailleurs il faisait déjà construire

Menlo Park un bâtiment destiné uniquement à cette

iuvelle invention. Le ton était tellement convainquant

ue le reporter goba entièrement ce que lui raconta Edison.
Wrtant de tout cela rien n'existait.

r


------------------------------
Text extracted from 23-3.jpeg:
THomas Epison LES PREMIERS PAS VERS L'IMPOSSIBLE

Entrait-il dans la course à la lumière électrique un pal courant lorsque la température devenait trop élevée. La
trop tard ou juste à temps? Nous savons maintenant q dy refroidie, le brûleur se mettait de nouveau en route.
profita grandement des expériences et des erreurs de M Il passa ainsi tout l’automne à expérimenter de nouvelles
prédécesseurs. Il savait déjà tout ce qu’il ne fallait pas fall pes et de nouveaux filaments*. Il réalisa trois

Ses premières réflexions l'avaient conduit à deux certitudl 2 Lavcat » (proposition de brevet) en un temps record — à

| les lampes devaient être alimentées par une unitécommuli paine un mois.

appelée «centrale» et chaque lampe devait utiliser Il essaya un alliage de platine et d’iridium qui ne donna,
| moins de courant possible pour être rentable. Bien que us, un résultat satisfaisant. Il essaya encore d'effectuer
| lampe à arc eût un avenir prometteur, Edison rejeta d'UN ln vide plus important et de mesurer la résistance des

façon catégorique cette voie. L'avenir, c'était la lampe lumpes afin de trouver le meilleur compromis comme
| incandescence. Mais pour l'instant toutes les tentatl luvait signalé Ohm.s

déployées par les savants anglais, français et américalll

étaient restées infructueuses. Edison devait trouver ll Quel était ce compromis ? Il fallait trouver la résistance la

substance supportant une très violente chaleur, ne fonda lus élevée possible afin d'empêcher le flament de brûler, et
pas tout en continuant à donner une lumière brillante! nsité la plus faible pour consommer le moins possible
quadrature du cercle. lle courant. Mais tout cela coûtait beaucoup d'argent et les

Wuources d’Edison commençaient à fondre avec autant de

La première matière qu'il utilisa fut le carbone. I) Mpidité que ses fils de platine.
bandes de papier carbonisé furent employées à cet eillil Le 17 septembre 1878 Edison reçut un téléramme de
Ces bandes sous l'effet du courant furent portéet Mn avocat et ami Grosvenor Porter Lowrey sollicitant
l’incandescence; mais sous l'effet de l'oxygène de l'air el Un entretien le plus tôt possible. Grosvenor Lowrey était
l s'éteignaient très vite. Aussi il imagina de les recouVill Un homme d’une intelligence exceptionnelle et un grand
d’un globe en verre à l’intérieur duquel il faisait le vitl lumaniste. Anglais par sa naissance, il avait eu une

La durée maximale d’une de ces lampes fut de 8 minul n buncsse aventureuse. Il était allé se battre au Kansas contre
Ce n'était pas suffisant. Aussi pensa-t-il que le carl | |

n'était pas le matériau adéquat. Il le laissa momentanéil uvre de sécession. Cela lui valut de devenir major dans
de côté et passa à autre chose. Union Army. Puis il était revenu dans l'Est afin de s'établir

Il essaya une cinquantaine de métaux non fusibles Bi tant que membre du barreau de New York. Il avait établi
après quelques expériences, lança son dévolu sur le plati
Le point de fusion du platine était un peu plus bas que cell

du carbone. Il confectionna une spirale de platine qu tinde influence sur les dirigeants de la Western Union, la
introduisit à l’intérieur d’un globe de verre partiellemal br Fargo et la New York Metropolitan Railway Company.

vide d’air. Cependant à une température élevée le brûlell
de platine fondait rapidement et la lumière s’éteignalt,

imagina alors un régulateur thermostatique qui coupiil

m a été inventé par Edison pour désigner la spirale de platine.
U est le voltage: R est la résistance exprimée en ohms: et I est
é en ampères.

196


------------------------------
Text extracted from 23-4.jpeg:
THoMas Ebison

Edison avait connu Grosvenor Lowrey tout au début
sa carrière lorsqu'il travaillait sur l’enregistreur des cout
la bourse (stock ticker), la télégraphie multiple et la plu
électrique. Il ne fut jamais angoissé par l’accumulatlt
des procès qui lui étaient intentés, car la plupart de il
brevets étaient achetés par la Western Union dont Low
était l'avocat. Depuis, Lowrey était devenu le défenseur,
conseiller et l'ami de Thomas Edison. -

Lorsque Lowrey apprit en 1867 qu'un président de |
United States Telegraph Company (devenue, un peu pli
tard la Western Union) était recherché, il proposa Willian
Orton au conseil d'administration. Ce qui fut fait. Depuli
ce moment-là, Lowrey était devenu le conseiller général dl
la Western Union. |

Le premier octobre Lowrey écrivit à Edison qu'il avall
rencontré à Wal] Street Hamilton Twombly, financlël
bien connu et gendre de William H. Vanderbilt#. Il leu
avait dit que l’inventeur était prêt à céder La moitié de sail
invention pour cent cinquante mille dollars. Edison aval
| besoin de ces fonds pour mener à bien ses expérience
sur l'électricité. Grosvenor Lowrey progressait aussi vit
comme homme d’affaires qu'Edison comme inventeur,
avait une confiance inébranlable en son ami, et sollicita
les plus grands capitalistes américains pour soutenir ui
invention qui non seulement n'existait pas encore mais qub
rassemblait contre elle les savants du monde entier.

Lowrey sollicita également Eggisto Fabri, partenaire dl
la Drexel, Morgan Company. Lowrey savait que convain
Morgan et Vanderbilt ne serait pas une tâche facile. Po
cela il était très aidé par les antériorités d’Edison:
télégraphe, le téléphone et le phonographe.

40. Ne pas confondre avec son père le fameux Commodore.

198

LES PREMIERS PAS VERS L'IMPOSSIBLE

Vdison venait de déclarer au New York Herald de Gor-
bn Bennet: « J'ai obtenu la lumière électrique en utilisant
procédés bien différents de mes prédécesseurs, ils ont
jus travaillé dans le même sillage, mais n'ont rien trouvé. »
Avec la dynamo, ajoutait-il, (elle n'était pas construite)...
pourrais faire briller des milliers de lampes. » Il déclara

d'ailleurs publiquement que sa lampe électrique était une
Néalité, qu'il suffisait de changer un peu de vapeur en élec-

lricité et le tour était joué. Il se voyait déjà éclairer toute la

ville de New York.

Les financiers de Wall Street étaient indécis. Le fameux

|, P. Morgan était tout excité. Le vieux renard savait renifler

les bonnes affaires. « Et quoi! s’écria Morgan! Une lumière
aipérieure à celle du gaz, moins chère et qui par surcroît peu
toucher infiniment plus de foyers! Voilà une affaire qui me
irait aussi bonne que d'investir dans les mines d'argent du
Noa! » .

Lowrey encouragea Edison, le connaissant bien, à rester
discret avec chacun d'eux pour ne rien compromettre.
Vdison, un peu vexé répondit: «Je n'accepterai rien, je
he dirai rien et je ne promettrai rien. Je laisserai tout sous
Votre entière responsabilité. » Lowrey comprit le message. Il
hivait qu'Edison adorait communiquer avec les médias en
leur annonçant souvent des nouvelles trop futuristes et lui
lépondit vigoureusement: « Utilisez votre intelligence à la
lumière. Je m'occupe non seulement de ce que vous espérez
vbienir mais également de toutes les attentes raisonnables
dont vous m'avez fait part et qui vous satisferont. Je veux
que les gens de la Western Union aient la première chance,
tar nous savons, vous comme moi, à qui nous avons à faire. »

Il était en effet sûr que la Wesern Union avait laissé
dchapper l'énorme opportunité du téléphone de Bell. Elle
he voulait pas rater celle de la lumière électrique.

Le 12 octobre, Lowrey rapporta qu'il avait obtenu un
tontrat et envoya à Edison un premier chèque de trente

199


------------------------------
Text extracted from 23-5.jpeg:
Thomas Epison

mille dollars. Le contrat établi entre Edison et les investis-
seurs qui allaient financer ses recherches fut lun des plus
remarquables dans les annales de l’industrie américaine,
En effet la Western Union achetait non pas une invention
existante mais une invention potentielle,

Le 15 octobre 1878, la Edison Elecrrie Light Company
était constituée, composée de: Tracy.R. Edson, James.
H. Banker Norvin Green, Robert L. Cutting Jr,
Grosvenor P Lowrey, Robert M. Galloway, Egisto P. Fabri,
George R. Kent, William K. Vanderbilt, J. Hood Wright,
Nathan G. Miller et Thomas À. Edison. À part Edison,
tous représentaient les plus hautes autorités de [a finance et
de l’industrie des Etats-Unis.

Son capital était de trois cent mille dollars répartis en trois
mille actions dont deux mille cinq cents furent données À
Edison. En échange Edison était tenu d'attribuer à la Wessern

Union toutes ses inventions Y compris les améliorations qu’il
Pouvait apporter dans le domaine de l'éclairage électrique
pendant une période de cinq ans. Si les inventions d’Edison
s'avéraient réussies, ses actions ptendraient une valeur
considérable sinon il n'aurait aucune indemnité. Lowrey
savait négocier, il réclama à Vanderbilt pour Edison vingt
centimes de royalties par lampe. Vanderbilt faillit s'étrangler.
On pouvait lui reprocher beaucoup de choses, mais le
bougre savait compter! « À ce tari£Rà, dit-il, il aura bientôt
une fortune équivalente à celle de Rockefeller! »

Edison était heureux, il avait absolument besoin de cet
afgent pour rassurer ses créanciers et satisfaire aux premiers
achats pour ses nouvelles expériences sur l'électricité, Il.
fit construire une bâtisse de deux étages dans laquelle il
installa une salle de réception pour les futurs visiteurs ainsi
qu'une splendide Pibliothèque destinée à contenir toute sa
documentation qui commençait à être immense et unique
sur le sujet, Bien plus il ft bâtir un nouvel atelier non

200

LES PREMIERS PAS VERS L'IMPOSSIBLE

seulement pour abriter les machines-outils mais aussi un
immense laboratoire et une fabrique de verre. Il ne lésina
pas non plus pour ériger un bâtiment en briques rouges
pour y installer les machines à vapeur et les dynamos.
acheta ces dernières au constructeur Wallace qu’il avait

visité à Ansonia.

Dans la carrière d'Edison, Grosvenor Lowrey prit une
place de plus en plus importante. Cette association était
très bénéfique car les deux hommes avaient une confiance
réciproque. L'influence de Lowrey se faisait même senti
dans la sélection du personnel d’Edison. L'inventeur était
connu pour être négligent et capricieux pour tout ce qui
concernait les affaires. Sur la suggestion de Dés un
jeune employé de la Western Union, 8. L. LE
cngagé comme secrétaire particulier d'Edison. £ fai
chargé de mettre de l’ordre dans ses affaires. Francis Jehl,
un employé de Lowrey, fut également engagé comme
assistant de laboratoire.

Edison promit à Lowrey qui aurait bientôt cr

lampes à lui montrer. Il fallait faire vite car pour ï

moment il m'avait rien de valable à lui faire voir. Mais i
devait sauvegarder sa réputation : si le public était refoulé,

en revanche les reporters étaient toujours les bienvenus.

Il avait toujours quelque chose à dire ou à leur montrer

pour les épater. |

Le très britannique Télegraphic Journal, sucer
prisé des professionnels, voyait les choses d'une see
beaucoup plus réaliste: « Le Napoléon del invention “

de plus en plus marcher son imagination qui n'a ce e

que son incroyable confiance en ses possibilités in nes

La moindre idée est déjà projetée par l’inventeur vers des

horizons fantasmagoriques. Peut-on vraiment prendre au

sérieux les déclarations de cet homme de génie? »


------------------------------
Text extracted from 23-6.jpeg:
Thomas Enison

Edison tomba subitement malade, Déjà le New York
Daily Graf ticanait: « Si Edison confond la nuit avec
le jour et qu'il refuse d'aller au lit, c'est qu’il a une idée,
Il devra payer pour cela d’une pneumonie, de troubles
du cerveau et peut-être d’une mort précoce. La machine
Edison travaille à trop haute pression et sans soupapes de
sûreté, »

Tandis qu'Edison était au lit, William Sawyer, un
homme instable et alcoolique, mais en revanche très bon
électricien ayant travaillé pour la Western Union, était venu
rendre visite à Lowrey. Ayant appris que la Western Union
s'intéressait à l'éclairage électrique, il déclara calmement à
Lowrey: « J'ai mis au point, il y a un an, une lampe avec
un filament de platine muni d’un régulateur magnétique.
Mais cela nest rien, aujourd’hui j'ai développé une
lampe avec, en guise de filament, un petit morceau de
carbone immergé dans l'azote qui empêche ce dernier de
se consumer. Cette lampe est prête, Elle est de très loin
supérieure à celle d'Edison! » Grosvenor Lowrey qui ne
comprenait pas grand-chose à la technique, mais qui était
loin d’être bête s'affola quand Sawyer lui parla de flament,
de carbone et de platine. Edison envoya Griffin pour voir

de quoi il s'agissait. Ni Griffin ni Lowrey n'avaient vu le
fonctionnement des lampes de Sawyer, mais déjà Lowrey se
demandait s’il ne fallait pas acheter les brevets au profit de
la Western Union. De retour à Menlo Park Griffin n'osa pas
parler de cette conversation avec Edison, Mais ce dernier
qui, de son lit, avait entendu toute la conversation, comprit
très vite de quoi il était question. Il pesta: « Je connais leg
prétentions de Sawyer. Tous ceux qui ont expérimenté de:
près ou de loin la lumière électrique pensent que leur système
est parfait mais je peux vous assurer qu’il est impensable
que Sawyer ait trouvé quelque chose de valable! » Une
telle réaction laissa Griffin sans voix. Lowrey se rendit
le lendemain, avec quelques experts, dans la boutique de

LES PREMIERS PAS VERS L'IMPOSSIBLE

Sawyer et constata, lui-même, l'exactitude des affirmations
d’Edison. Les lampes s’éteignaient les unes après les autres,
le carbone se détériorait, l'azote fuyait. Sawyer ne savait
plus à quel saint se vouer et il était sur le point de jeter
ses lampes à la poubelle. Sawyer n'avait plus d'argent et sa
demande auprès de Lowrey avait été son dernier va-tout.

Edison était assez négligeant dans la recherche
d’antériorité des brevets et ses connaissances théoriques
étaient, il faut bien le reconnaître, plus que médiocres. Il
fallait donc trouver l'oiseau rare dont il avait absolument
besoin. Un jeune homme de vingt-six ans répondait à ce
critère et avait pour nom Francis Upton. Il avait passé
un an en Allemagne pour travailler aux côtés de Ludwig
Van Helmholtz, l’un des physiciens les plus célèbres du
siècle. Que pouvait-on réver de mieux? Son travail serait
d'étudier tous les brevets anciens et nouveaux concernant
l'éclairage électrique.

Upton débarqua le 13 décembre à Menlo Park par une
pluie torrentielle. Il monta les quelques marches qui le
séparaient du laboratoire et demanda où il pourrait trouver
Monsieur Edison. On lui montra un homme qu'Upton
aurait pris plus volontiers pour un ouvrier ordinaire.
Non ce n'était pas possible! Cet homme aux manches
rctroussées, vêtu d’une chemise usée, au pantalon tâché,
un vieux mouchoir noué autour du cou, ne pouvait pas
dire Thomas Edison!

Edison devinant sa surprise lui dit: «Il ne faut pas se
lier aux apparences, ici on ne s'habille pas, on travaille.
Vous devez venir de Boston, cela se voit à votre complet
ët À votre accent. » Edison lui fit visiter tout le laboratoire.
Upton fut émerveillé par la bibliothèque et l'extraordinaire
documentation qu'elle contenait.

Upton était ému, il se remémorait ce qu'Edison avait
iléjà accompli à trente ans, c'était énorme. Devant tant de

203


------------------------------
Text extracted from 23-7.jpeg:
THoMAs Epison

simplicité, il sentit naître en Jui une admiration certaine,
mélangée à du respect. Celui-ci était réciproque: « Avec
votre pédigrée, je vais vous appeler Professeur »
également Edison.

avoua

Edison avait envie de tester immédiatement les capacités
d’'Upton. Il voulait aussi rompre le voile et le mettre
à l'épreuve puisqu'il ne connaissait pas les horaires de
travail: c'était simple il ny en avait pas. Il lui apporta
une tasse de café: « Ici on boit beaucoup de café car on
travaille souvent après minuit. » Edison regarda la réaction
d’Upton qui sourit:

«Vous savez avec le professeur Van Helmhotz, c'était
à peu près la même chose. » La réponse plut à Edison
qui enchaîna: « Pouvez-vous maintenant me calculer le
volume de cette ampoule? » Il était trois heures de 1
midi. À sept heures Upton rendit son devoir.

— Je vais contrôler, lui dit Edison.

— Comment allez-vous faire? je pensais que vous ne
Connaissiez pas grand-chose aux mathématiques.

— C'est vrai, je n’y connais tien, Il ptit l’ampoule, la
remplit d'eau et la versa dans un verre gradué et en dix
secondes il eut le résultat. « Bravo! C'est exactement ça,

Upton n'en revenait pas. Il comprit très vite qu'il était
fait pour s'entendre avec l'inventeur: lui le mathématicien,
l'autre l'expérimentateur.

Tous deux devinrent d'excellents amis. Upton s’'intégra
parfaitement à l’équipe des collaborateurs d’Edison dont il
admirait les qualités de chacun. Upton parlait couramment
le français et l'allemand, ce qui était primordial du
point de vue scientifique car outre l'anglais, toutes les
publications importantes étaient rédigées dans l’une de
ces trois langues. Certes Upton était un scientifique pur,
mais Edison eut l’habilité de lui faire comprendre que la
science était un univers sans fin où parfois

’après-

l'empirisme

204

LES PREMIERS PAS VERS L'IMPOSSIBLE

pouvait venir à son secours. La réputation d’Upton fut
définitivement acquise pour Edison lorsque Batchelor
estant une dynamo miniature obtint six fois moins vite
es mêmes résultats qu'Upton. Ce dernier n'utilisant que
du papier et un crayon. |
L'arrivée d’Upton à Menlo Park était concomitante
a création de l’Edison Electric light Company. Comme
Edison ne voulait rien cacher à Upton, il lui annonça que
a fondation de cette dernière société était uniquement
destinée à financer les recherches sur la lumière électrique,
Malgré ses nombreuses inventions, Edison était loin d être
un millionnaire et tout l'argent qu'il gagnait était investi
dans son laboratoire.

s

d

Dans l’état actuel des choses cette affaire était bonne
pour les deux parties. En somme c'était un immense pan
sur linfaillibilité d’Edison. La lumière électrique n’était
pas encore trouvée que déjà J. P. Morgan voulait en
acquérir les droits pour l’Europe. Rothschild, lui-même,
avait délégué de Vienne son collaborateur August Relnent
à Menlo Park pour qu'il lui fit un compte rendu détaillé
de la nouvelle invention. Upton était arrivé à temps pour
vivre la grande aventure.

205

## Références

## Liens  