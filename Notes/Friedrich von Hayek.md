MOC : 
Source : [[Friedrich von Hayek]]
Date : 2023-04-14
***

## Résumé
Economiste libéral qui critique l'intervention de l'Etat dans l'activité économique. Ses travaux portent sur la méthodologie et l'organisation de la société dans son ensemble. On parle de [[Libéralisme]] intransigeant de Hayek. Il a pour maître [[Ludwig von Mises]]

Il est grandement repris à partir des années 1970 par les néolibéralistes (voir [[Néolibéralisme]]).

Ses ouvrages ont été loués par [[Margaret Thatcher]] et [[Ronald Reagan]]

## Référence

## Liens 