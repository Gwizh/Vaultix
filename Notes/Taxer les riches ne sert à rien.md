MOC : 
Source : [[Taxer les riches ne sert à rien]]
Date : 2023-04-07
***

## Résumé
Dit par [[Bernard Friot]]

La [[Lutte des classes]] s'articule selon lui surtout sur la domination sur le [[Travail]] et non sur l'[[Argent]]. Le pouvoir sur l'argent de la [[Bourgeoisie]] est une conséquence de son pouvoir sur le travail et elle est prête à des concessions monétaires si elle peut garder son pouvoir sur le travail. Taxer les riches ne peut être qu'une solution au court terme, ça ne représente absolument pas un changement de société en soi. 

## Référence

## Liens 