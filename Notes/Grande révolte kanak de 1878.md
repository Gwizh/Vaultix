MOC : 
Source : [[Grande révolte kanak de 1878]]
Date : 2023-06-08
***

## Résumé
La grande révolte kanak de 1878 désigne habituellement l’insurrection kanak de 1878, en [[Nouvelle-Calédonie]], avec la figure emblématique du grand chef de Komalé, [[Ataï]]. Les Kanaks l'appelleraient plutôt La Guerre d'Ataï. 

La découverte de la garniérite, minerai à forte concentration de nickel date de 1870 et provoque une « ruée vers le nickel ». Thio, capitale du nickel à partir de 1875, connaît diverses immigrations : chinoise et indienne (1865, Malbars), pénitentiaire (1872), puis japonaise (1892), indonésienne (1895), réunionnaise… 

Le code de l’indigénat (1887-1946) tiendra lieu de politique indigène, mais il n’est pas encore complètement appliqué ni applicable. Les Kanak sont le « point aveugle » du développement de la colonie (Salaün, 2013).

Au début de la présence française, les implantations missionnaires sont presque les seules à s’intéresser au monde culturel kanak, aux langues kanak, aux pratiques sociales kanak, d’abord dans un souci d’évangélisation, ensuite avec des objectifs de civilisation, et de gestion, des populations. La prédication se fait en langue locale, ce qui suppose un apprentissage des langues kanak, ou des intervenants au moins bilingues, et compatibles. Dès 1863 est promulguée l’interdiction de l’enseignement de toute autre langue que le français. La mesure vise principalement à lutter contre l’emprise anglophone et protestante… 



## Référence

## Liens 
https://fr.wikipedia.org/wiki/Grande_r%C3%A9volte_kanak_de_1878?wprov=sfla1
