MOC : 
Source : [[Émile Zola]]
Date : 2023-07-11
***

## Résumé


### Détails





#### Zola et la Légion d'honneur
https://gallica.bnf.fr/dossiers/html/dossiers/Zola/Chrono/ZolaLit3_Legion.htm
À deux reprises, en 1878-1879, puis en 1884-1886, Zola avait refusé qu’on le décore de la Légion d’honneur.

En avril 1888, Lockroy, le nouveau ministre de l’Instruction publique, revient à la charge, et demande à Maupassant et à Marguerite Charpentier, la femme de l’éditeur, d’être ses intermédiaires auprès de Zola. Au grand étonnement de ses admirateurs, le romancier accepte, et Lockroy lui remet sa croix le 13 juillet, dans le salon des Charpentier. Le 14, Edmond de Goncourt, chevalier depuis 1867, confie à son _Journal_ : "J'écris à Zola : "Mes félicitation sincères pour une réparation - bien tardive  !" Fichtre ! c’est vrai. Mais diable m'emporte si j'avais été Zola, si j'aurais accepté la décoration à l’heure qu’il est. Il n’a pas compris donc qu’il se diminue en devenant chevalier ! Mais le révolutionnaire littéraire sera un jour commandeur de la Légion d’honneur et secrétaire perpétuel de l’Académie et finira par écrire des livres si ennuyeusement vertueux qu’on reculera à les donner aux distributions de prix des pensionnats de demoiselles  !" (_Journal_, t. II, Laffont, 1989, coll. "Bouquins", p. 143).

Deux semaines plus tard, Edmond de Goncourt reprend les mêmes idées devant un rédacteur du _Gaulois_, ce qui lui vaut, le 30 juillet, une réponse amère de Zola : "Pourquoi me blâmer d’avoir accepté la croix, lorsque je l’ai acceptée dans les mêmes conditions que vous ? J'ai eu la main affectueusement forcée par Lockroy, comme vous l’avez eue par la princesse Mathilde, et j'estime que, si vous étiez contesté après _Germinie Lacerteux_, je ne l’étais pas moins après _La Terre_. Vous ne vous souvenez donc pas que, il y a un an, on parlait de me rayer de la littérature et de m'enfermer à l’hôpital  ? Pourquoi la croix, qui était une consécration pour vous, n’en serait-elle pas une pour moi  ?" (_Correspondance_, t. VI, p. 314).

En ce mois de juillet 1888, Zola écrit encore à Maupassant, sur le même sujet : "J'ai accepté, après de longues réflexions, que j'écrirai sans doute un jour, car je les crois intéressantes pour le petit peuple des lettres, et cette acceptation va plus loin que la croix, elle va à toutes les récompenses, jusqu’à l’Académie : si l’Académie s’offre jamais à moi, comme la décoration s’est offerte, c’est-à-dire si un groupe d’académiciens veulent voter pour moi et me demandent de poser ma candidature, je la poserai, simplement, en dehors de tout métier de candidat. Je crois cela bon, et cela ne serait d’ailleurs que le résultat logique du premier pas que je viens de faire." (_Correspondance_, t. VI, p. 306).

Le 13 juillet 1893, après la publication du dernier volume des _Rougon-Macquart_, Zola sera promu officier de la Légion d’honneur par Raymond Poincaré, ministre de l’Instruction publique dans le cabinet Dupuy. Le 16 juillet, Poincaré lui écrira : "J'ai été heureux qu’il me fût donné à moi, l’un de vos plus fervents admirateurs, de contresigner un décret qu’attendait impatiemment tout le monde lettré".

## Référence

## Liens 