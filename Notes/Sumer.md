MOC : 
Source : [[Sumer]]
Date : 2023-06-18
***

## Résumé
Sumer est une région antique, située à l'extrême sud de la Mésopotamie antique (actuel Irak), couvrant une vaste plaine parcourue par le Tigre et l'Euphrate, bordée, au sud-est, par le golfe Persique. Il s'y est développé une importante civilisation de l'Antiquité, à compter de la fin du IVe millénaire av. J.-C. et durant le IIIe millénaire av. J.-C.

Les États sumériens avaient à leur tête un monarque64, souvent désigné par le terme lugal (« grand homme »), même si une pluralité de titres aux sens originels discutés sont attestés pour la période des dynasties archaïques : ensí à Lagash, en à Uruk, etc.


## Référence

## Liens 