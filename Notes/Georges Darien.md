MOC : 
Titre : [[Georges Darien]]
Date : 2023-09-26
***

## Résumé
Georges Darien, né Georges Hippolyte Adrien le 6 avril 1862 à Paris 7e2 et mort le 19 août 1921 à Paris 6e3, est un écrivain français de tendance [[Anarchie|anarchiste]], frère du peintre Henri Gaston Darien.

Son œuvre, qui comprend récits, pamphlets et pièces de théâtre, se place sous le signe de la révolte face à l’injustice et à l'hypocrisie. 
## Références

## Liens 