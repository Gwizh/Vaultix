MOC : 
Source : [[Léon Gambetta]]
Date : 2023-06-09
***

## Résumé
Léon Gambetta, né le 2 avril 1838 à Cahors et mort le 31 décembre 1882 à Sèvres, était un homme d’État français. Il est l'une des personnalités politiques les plus importantes des premières années de la [[IIIe République]].

Avocat, opposant au [[Second Empire]], élu député en 1869, il défend le « programme de Belleville », qui est assez radical (extension des libertés publiques, séparation des Églises et de l’État, vote de l'impôt sur le revenu, élection des fonctionnaires, suppression des armées permanentes, etc.).

Le 4 septembre 1870, après la défaite de Sedan et depuis l'hôtel de ville de Paris, il proclame le retour de la République. Le 7 octobre suivant, ministre de l'Intérieur du gouvernement de la Défense nationale, il quitte en ballon la capitale, assiégée par les troupes prussiennes, afin d'organiser les combats en province.

Après le traité de Francfort, il contribue à la pérennisation du régime républicain. Devenu une figure des républicains modérés, il est président de la Chambre des députés de 1879 à 1881, puis président du Conseil et ministre des Affaires étrangères pendant deux mois entre 1881 et 1882, sous la présidence de [[Jules Grévy]], avec qui son inimitié est de notoriété publique.

En 1875, il a combattu les lois constitutionnelles, la création du Sénat et la magistrature présidentielle septennale. 

Il est renversé en 1882 après avoir proposé de démocratiser les institutions fondées en 75, en établissant un scrutin de liste, en supprimant les 75 sièges inamovibles du Sénat et en limitant les compétences financières de la Haute Assemblée. Léon Gambetta meurt moins d'un an après avoir quitté la tête du gouvernement, à l'âge de 44 ans.

## Référence

## Liens 