MOC : 
Source : [[Samuel Smiles]]
Date : 2023-08-13
***

## Résumé
Samuel Smiles (23 December 1812 – 16 April 1904) was a British author and government reformer. Although he campaigned on a Chartist platform, he promoted the idea that more progress would come from new attitudes than from new laws. His primary work, Self-Help (1859), promoted thrift and claimed that poverty was caused largely by irresponsible habits, while also attacking materialism and laissez-faire government. It has been called "the bible of mid-Victorian liberalism" and had lasting effects on British political thought. 
## Référence

## Liens 
[[Libéralisme]]
[[XIXe siècle]]