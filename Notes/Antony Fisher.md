MOC : 
Source : [[Antony Fisher]]
Date : 2023-04-14
***

## Résumé
Antony Fisher (28 juin 1915 - 8 juillet 1988) est le père de nombreux think tanks libertariens et l'un des acteurs de la montée en puissance de ces mouvements dans la seconde partie du XXe siècle. Il a ainsi fondé l'Institute of Economic Affairs et l'Atlas Economic Research Foundation.

## Référence
[[Think-tank]]
[[Néolibéralisme]]
[[Libéralisme]]

## Liens 