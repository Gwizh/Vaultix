[[Livre]]
MOC : [[LITTERATURE]]
Titre : [[Sylvie]]
Auteur : [[Gérard de Nerval]]
Date : 1853
***

## Résumé
- Chapitre 1 :
Introduction. On comprend les objectifs du personnage ainsi que sa manière de penser. Il suit une logique particulière qui est détaillée pour qu'on arrive à sa conclusion. 

- Chapitre 2 :
Passé du personnage. On prend connaissance des deux femmes de son coeur. Sylvie, amie d'enfance avec qui le personnage est en couple et Adrienne, fille qu'il a connu lors d'une fête et qui lui a laissé une vive impression. Les descriptions sont vraiment superbes, il y a un parallèle entre la nature et Adrienne qui est vraiment romantique. On voit aussi le fait qu'il confonde l'actrice du chapitre 1 avec Adrienne, jouant entre le rêve et le vrai. 

- Chapitre 3 :
Chapitre amusant où on le voit paniquer par rapport à sa nouvelle résolution. Il rentre chez lui pour la fête où il avait rencontré Adrienne la première fois. 

- Chapitre 4 :
Rédemption de Sylvie, on comprend qu'il l'aime aussi. 

- Chapitre 5 et 6 :
Se répètent un peu, on voit la complicité entre Sylvie et le personnage principal. On découvre la vie de Sylvie avec sa tante, deux modestes femmes de la campagne de Flandres. 

- Chapitre 13 :
Aurélie, l'actrice part jouer dans le village de campagne du personnage principal. Elle n'est pas Adrienne mais lui ressemble beaucoup. Le perso est déçu que ce ne soit pas la même personne et lui avoue qu'il voulait la voir là pour réaliser son rêve. Elle lui dit qu'il ne l'aime pas. Tintintin. 

- Chapitre 14 :
Très belle conclusion qui résume bien la nouvelle. L'idée est exprimée clairement sans trop de subtilités. 

## Notions clés

## Citations

## Passages amusants

## Références
