MOC : 
Source : [[Eric Dupont-Moretti]]
Date : 2023-05-03
***

## Résumé
Garde des Sceaux et ministre de la justice depuis le 6 juillet 2020. 

### Le plan de Dupond-Moretti pour « remettre la justice à flot » 
Des prud'hommes au civil en passant par la refonte de la fameuse procédure pénale ou la création d’une application mobile visant à mettre la justice « dans la poche » des citoyens, le garde des Sceaux a dévoilé, pendant près de deux heures, l’essentiel de ses 60 mesures visant à remettre « à flot », une institution décriée par les Français selon différents sondages. 

L’ancien ténor du barreau s’est donc engagé à porter le budget de son ministère de 9,6 à 11 milliards d’euros en 2027. Sur cinq ans, cette promesse représente une enveloppe supplémentaire de 7,5 milliards d’euros, un effort budgétaire qui dépasse de loin celui consenti sous le quinquennat de Nicolas Sarkozy (+2 milliards) comme sous François Hollande (+2,1 milliards), n’a pas manqué de souligner l’actuel locataire de la Place Vendôme. 

Ces huit mois de consultations de dizaines de milliers de professionnels et de citoyens avaient été lancés par l’exécutif en octobre 2021 afin de répondre au malaise des professionnels, notamment exprimé au travers de plusieurs mouvements sociaux d’ampleur, et contrer les attaques qui visent la justice.

Pour continuer sur cette trajectoire, et participer à la « restauration de la place » de la Justice en France, Éric Dupond-Moretti s’est également réjoui du nombre exceptionnel, en hausse de 80 %, d’élèves intégrant l’école nationale de la magistrature. Ils seront 380 à y étudier en 2023, soit « la promotion la plus importante de l’histoire de l’école ». 

Justice : Éric Dupond-Moretti dégaine une nouvelle réforme contestée
https://www.mediapart.fr/journal/france/030523/justice-eric-dupond-moretti-degaine-une-nouvelle-reforme-contestee




## Référence

## Liens 