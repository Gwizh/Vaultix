MOC : 
Source : [[La République - Quelles valeurs]]
Date : 2023-06-23
***

Livre de [[Jean-Fabien Spitz]]
Vidéo : https://www.youtube.com/watch?v=MzBQyK27SvA

## Résumé
#### La République au service du néolibéralisme
La République est un moyen de créer un faux nationalisme sur des valeurs dites universalistes et neutres. La République permet de créer un ordre social autoritaire dirigés par des experts.

#### Les valeurs falsifiées de la République
La valeur principale, qui passe avant le reste et la sécurité et l'ordre public. On utilise la République comme un terme conservatiste, intemporel à laquelle s'oppose les communautaristes et les séparatistes. 

## Référence

## Liens 