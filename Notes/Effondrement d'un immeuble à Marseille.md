MOC : 
Source : [[Effondrement d'un immeuble à Marseille]]
Date : 2023-04-10
***

## Résumé
https://www.francetvinfo.fr/marseille/immeubles-effondres-a-marseille-il-reste-de-l-espoir-et-tant-qu-il-reste-de-l-espoir-nous-ne-nous-arreterons-pas-estime-benoit-payan_5762849.html

https://twitter.com/p_duval/status/1645087799618555904?t=FOP-dgyS2pIolVRrW4XmxA&s=09
- 00h40 un immeuble s'effondre à Marseille
- 07h36 un jet décolle de Paris pour Lorient 
- 09h28 [[Gérald Darmanin]] en week end en Bretagne, décolle dans le Jet direction Marseille 
- 10h34 Darmanin est à Marseille 
- 11h40 Darmanin est sur toutes les chaines infos 
- 12h42 le jet redécolle pour la Bretagne 
- 13h54 Darmanin est à peine en retard pour le déjeuner 
- 14h20 le jet redécolle pour rejoindre sa base

## Référence

## Liens 