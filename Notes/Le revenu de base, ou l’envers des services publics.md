MOC : 
Titre : [[Le revenu de base, ou l’envers des services publics]]
Date : 2023-10-02
***

## Résumé

Dans un ouvrage récent, deux chercheurs belges, Daniel Zamora Vargas et Anton Jäger, insistent sur la différence fondamentale entre la logique libérale de l’allocation universelle en cash, et la logique citoyenne des services publics. Entretien.

Daniel Zamora Vargas : On pourrait illustrer cette tension qui traverse les politiques sociales par un petit exercice de pensée. Imaginons deux sociétés qui seraient caractérisées par une distribution des revenus parfaitement égale. Tout le monde, dans ces deux pays, reçoit donc un revenu identique. Mais dans un des deux, une grande partie des richesses est socialisée, et dans l’autre, tout reste dans les mains du privé. En d’autres termes, dans une, vous recevez plus d’argent et dans l’autre, un peu moins, mais l’éducation, la culture, la santé ou les transports sont gratuits.

Du point de vue de la mesure des inégalités, ces deux sociétés sont parfaitement égales. Mais il n’est pas difficile de saisir en quoi elles sont également très différentes. Cette différence réside évidemment dans la place que l’on donne à la collectivité en matière d’investissement. Ou, pour le dire autrement, la capacité qu’ont ces sociétés à orienter les grandes décisions économiques.

[[Milton Friedman]] a été effectivement l’un des premiers à envisager l’idée d’un revenu garanti lorsqu’il travaillait encore dans l’administration fédérale sur des sujets liés à la pauvreté. Il n’était cependant pas le seul et les propositions qui vont dans ce sens commencent réellement à proliférer durant l’entre-deux-guerres. C’est à cette période que se déroule un grand débat sur le socialisme parmi les économistes qui opposera notamment le néolibéral [[Friedrich von Hayek]] au socialiste [[Oskar Lange]].

> Il s’agit désormais d’être libre dans le marché, plutôt que d’être libre du marché.
  Daniel Zamora Vargas

En ce sens, le tournant vers le marché précède largement ladite révolution [[Néolibéralisme]]. Tant pour la [[Sociale-Démocratie]] modernisée que pour les néolibéraux, la liberté a été circonscrite à l’accès à la consommation. Il s’agit désormais d’être libre _dans_ le marché plutôt que d’être libre _du_ marché. La discussion porte alors uniquement sur le montant, plus généreux pour les uns, plus restrictif pour les autres.

## Références

## Liens 