## Chapitre 1 - Naufrage du bonapartisme
> La Commune fut l’« antithèse » de l’Empire, a écrit Marx.

#### 1 - La fin du césarisme
Il y a un contexte à la chute de l'Empire, elle ne peut pas être expliquée que par la guerre. Cet Empire qui avait promis la paix connaît problèmes en Italie et au Mexique. Cet Empire qui avait promis redressement de l'économie voit les classes sociales se diviser. Pendant les [[Élections législatives françaises de 1869]], le bonapartisme perd du terrain et ne compte que 58.67% des voix.

#### 2 - La République et la Révolution
Il y a une grande division dans l'appréciation de la république. Les campagnes sont pour l'Empire tandis que les villes sont pour la république. Les jeunes pronent Rousseau, les jeunes rêvent de Kant. [[Léon Gambetta]] devient alors très populaire avec les vieilles idées révolutionnaires des libertés individuelles, de la presse, de la séparation de l'Eglise et de l'Etat. Il veut l'abolition des privilèges et monopoles.
Les élites républicaines veulent une révolution tranquille tandis qu'une population urbaine impatiente exacèrbe l'envie des citadins.

#### 3 - Capacité sociale et politique des classes ouvrières
Sur trente-huit millions de Français, onze à douze millions vivent des « professions industrielles », dont cinq millions de travailleurs actifs : 3,5 millions de salariés, 1,5 de patrons. En dépit de progrès accomplis, l’industrie et la classe ouvrière conservent des traits fortement traditionnels. Prolétariat ? Entre eux, les ouvriers se nomment volontiers encore « prolétaires », un mot de 1830 ; plus souvent « producteurs », ou simplement « travailleurs », « serfs du salariat », cette nouvelle « féodalité ».

Victime majeure du coup d’État de 1851, le mouvement ouvrier a repris quelque force après 1860. En 1864, deux événements importants surviennent, la nouvelle possibilité de faire la grève et la fondation de l'AIT ([[Association internationale des travailleurs]]). Le Français Pindy évoque à Bâle à l'occasion d'un congrès l’organisation de futures « communes » : le groupement des différentes corporations de producteurs par ville formerait la Commune de l’avenir. Le gouvernement est remplacé par les conseils des corps de métiers et par un comité de leurs délégués respectifs, réglant les rapports du travail qui « remplaceront la politique ». Déjà, les divions entre anarchistes et socialistes se créent sous l'impulsion notamment de Marx et Bakouine.

Pour la majorité des ouvriers, la République est consubstantiellement démocratique et sociale. Dans la capitale, les grandes villes, les Internationaux mènent généralement la lutte aux côtés de l’extrême gauche républicaine. Les dernières années de l’Empire ont été marquées par trois grandes vagues successives de grèves : 1864, 1867 et 1869-1870. À Anzin, La Ricamarie, Decazeville, en 1869, au Creusot en 1870, on a fait donner la troupe, qui a tiré.

#### 4 - Paris en 1870
La Ville a démesurément grandi depuis vingt ans ; elle comptait un million d’habitants en 1851, elle en compte presque deux en 1870 ; elle est ainsi, de loin, la plus grande agglomération laborieuse du pays. 57 % des Parisiens vivent d’activités industrielles, 12 % d’activités commerciales. 23 % des actifs travaillent dans le vêtement, 20 % dans les métiers d’art et les « articles de Paris », 13 % dans le bâtiment, 8 % dans les industries des métaux. Cette étonnante diversité fait néanmoins une remarquable unité ; il s’est forgé comme une « nationalité » ouvrière parisienne. 

Le 12 janvier 1870, après l’assassinat par le prince Pierre Bonaparte de Victor Noir, journaliste à _La Marseillaise_, 200 000 personnes ont fait de ses funérailles une immense manifestation. « Nous espérions bien ne rentrer qu’avec la République », écrit Louise Michel.

#### 5 - La République des villes
En 1869 à Lyon, les républicains, toutes tendances confondues, ont recueilli 77 % des voix. La première circonscription de Marseille a choisi Gambetta qui bat (avec le désistement complice du libéral Thiers) le candidat officiel de Lesseps, par 65 % des voix, l’année de l’inauguration du canal de Suez ! À Limoges, 78 % des votes sont allés à l’opposition, dont 59 % aux républicains. En mai 1870, , c’est Saint-Étienne qui est la capitale du non (74 %), devançant Marseille ou Limoges (65 %), Bordeaux (64 %), Lyon (61 %) : et toutes distancent Paris (57 %).

C’est à Lyon, « capitale du socialisme », que s’est tenue, le 15 mars 1870, la première grande réunion unitaire de 5 000 membres de l’Internationale.

Le principe étant que ces villes souhaitent une plus grande autonomie, devenir une Commune. Plusieurs républicains y ont donné leur aval, Eugène Pelletan, Jules Ferry, élu en 1869 à Paris sur un programme de « destructions nécessaires » : « La France, dit-il alors, a besoin d’un gouvernement faible. » Gambetta, moins féru de décentralisation, a tout de même évoqué devant les Marseillais les traditions, (les) « mœurs autonomes » de leur ville, New York d’une France régénérée.

#### 6 - La Révolution domptée ?
69 % des Français habitent des agglomérations de moins de 2 000 habitants, 51 % vivent du travail de la terre. Le bonapartisme reste populaire. L’Empire a été une période faste, les récoltes se vendaient bien, l’exode rural décongestionnait les campagnes.
 
Les extrémistes pressés, le Premier ministre Émile Ollivier, qui s’est promis de « prendre la Révolution à bras-le-corps », les dompte sans peine, mêlant répression et provocation, inventant des complots pour les mieux réprimer. Le 30 avril 1870, il a ordonné l’arrestation de « tous les individus qui constituent l’Internationale ». Le mouvement ouvrier en a sévèrement pâti, à Paris surtout où ses dirigeants sont condamnés, le 8 juillet 1870, à de lourdes peines. Les républicains méditent leur défaite de mai 1870 ; le suffrage universel s’est révélé menteur. Il faudra attendre, pensent-ils, une génération au moins pour l’emporter et œuvrer par l’éducation et la propagande.

#### 7 - Le début de la Guerre
Le 19 juillet, la guerre est déclarée avec la Prusse. Les défaites se succèdent ; l’armée de l’Est, 100 000 hommes commandés par Bazaine, se laisse bloquer dans Metz. Sous [[Patrice de Mac Mahon]], une armée hâtivement formée tente de se porter à son secours ; **le 2 septembre**, c’est le désastre de la [[Bataille de Sedan]] : 3 000 morts, 14 000 blessés, 83 000 prisonniers, dont 39 généraux, et l’empereur. 

Gambetta proclame la déchéance de l’Empire, « attendu que la Patrie est en danger ». Se créer le [[Gouvernement de la Défense nationale]], formé principalement des députés de Paris : Jules Favre, aux Affaires étrangères, [[Jules Ferry]], préfet de la Seine, Garnier-Pagès, Étienne Arago, maire de la ville, Rochefort, qu’on tire de prison. Mais aussi [[Léon Gambetta]]], bien qu’il ait opté pour Marseille, à l’Intérieur, Jules Simon, de Bordeaux, à l’Instruction publique, Dorian, de Saint-Étienne, aux Travaux publics.

> « Il y avait des fleurs aux fusils, des guirlandes ; c’était un air de fête dans la cité. Jamais révolution ne se fit avec une telle douceur » (Jules Ferry)

**Le 4 septembre 1870, la [[IIIe République]] est déclarée.**

## Chapitre 2 - Guerre Franco-prussienne
### A - Depuis Paris
#### 1 - L'impossible paix
La République installée, « sublime imprudence », c’est d’abord de paix qu’on parla. C’est la paix que [[Victor Hugo]], rentré d’exil le 5, réclamait dans sa _Lettre aux Allemands_ : 
>"Pourquoi cette invasion, pourquoi cet effort sauvage contre un peuple frère ? C'est l'empire que vous avez voulu. Il est mort. C'est bien. Nous n'avons rien à voir avec ca cadavre."  

Les Internationaux allaient plus loin en déclarant : 
>"Nous te répétons ce que nous déclarions à l’Europe coalisée en 1793 : le peuple français ne fait point la paix avec un ennemi qui occupe son territoire ; (il) est l’ami de tous les peuples libres… Proclamons la Liberté, l’Égalité et la Fraternité des Peuples. Par notre alliance, fondons les États-Unis d’Europe…"

#### 2 - Les effectifs parisiens
La Garde nationale _sédentaire_, la Garde nationale proprement dite, était composée des hommes valides non mobilisés de 20 à 40 ans. C’est cette milice citoyenne que la République décida de lever « en masse », portant son effectif de 60 à 260 bataillons, quelque 300 000 hommes.

Solidement fortifié, « Paris est inexpugnable ».

Le 21 septembre, Gambetta saluait l’anniversaire de la proclamation de la Première République : « Il y a soixante-dix-huit ans à pareil jour, nos pères fondaient la République et se juraient à eux-mêmes en face de l’étranger qui souillait le sol de la Patrie de vivre libres ou de mourir en combattant… »

#### 3 - Le Comité central des 20 arrondissements
> « L’épouvantable guerre actuelle… est maintenant le duel à mort entre le monarchisme féodal et la démocratie républicaine… Nous ne négligeons pas pour autant les précautions contre la réaction épargnée et menaçante. Nous organisons en ce sens nos comités de vigilance dans tous les quartiers et nous poussons à la fondation de districts qui furent si utiles en 93. » - L'international

Le 5 septembre, entre 400 et 500 délégués ouvriers décident de créer dans chaque arrondissement un _comité républicain ou comité de vigilance et de défense_, qui délègueront chacun deux membres pour former un Comité central.

C'est le prototype de la Commune de Paris.

Le gouvernement promettait des élections municipales pour le 28 septembre, des élections nationales pour le 2 octobre. Ministre de l’Intérieur, Gambetta eût accepté volontiers une municipalité parisienne qu’il n’hésitait pas pour sa part à nommer Commune.


#### 4 - Gain en indépendance éphémère
La population parisienne, folle de son succès avec le comité, a multiplié les rencontres et les prises de paroles. On tire de nombreux nouveaux journaux, on crée de nombreux nouveaux clubs et on manifestait un peu partout pour que le gouvernement actuel soit plus audacieux. Bien sûr, celui-ci ne voyait pas tout ça avec un bon oeil. 

Pourtant, cette "fédéralisation" était dangereuse en tant de guerre où la centralisation de pouvoirs est largement plus efficace. Car les défaites continuaient : 23 septembre, capitulation de Toul, le 29 de Strasbourg ; à Paris deux sorties échouaient, le 19 à Châtillon, le 30 à Choisy-le-Roi.

#### 5 - Soulèvement du 31 Octobre 1870
Le 27 octobre, ce fut, traîtresse, la capitulation de Bazaine à Metz : 106 000 hommes prisonniers, 6 000 officiers, 50 généraux. La France a perdu sa dernière armée régulière. Paris en apprend la nouvelle le 30, en même temps que l’échec d’une sortie sur Le Bourget (28 au 30 octobre) et que Thiers, de retour d’une tournée auprès des cours européennes, serait porteur de propositions de paix.

Le 31 octobre, une vague de mécontentement déferla sur la capitale. Plusieurs mouvements s’enchevêtrent, sans réussir à se mettre à l’unisson. Le matin, des délégations des comités de vigilance, puis de bataillons de la Garde nationale se portent à l’Hôtel de Ville pour questionner le gouvernement sur la désespérante nouvelle de Metz.

Malgré la promesse faite par Favre, le gouvernement fait arrêter quatorze membres de l'extrême-gauche. Blanqui, Flourens et Millière passent à la clandestinité.

[[Emmanuel Arago]] démissionne de son poste de maire, et le jeune [[Georges Clemenceau]] le suit par solidarité. Le préfet de la Seine [[Jules Ferry]] remplace alors Arago, conservant son poste de préfet. De ce jour, une solide rivalité s'installera entre Clemenceau et Ferry, celui-là l'accusant d'être un conservateur déguisé, celui-ci d'être un complice des révolutionnaires.

Le général Clément-Thomas succède au faible Tamisier à la tête de la Garde nationale.

Le 3 novembre, le gouvernement se soumet à un plébiscite en demandant : « La population de Paris maintient-elle OUI ou NON les pouvoirs du Gouvernement de la Défense Nationale ? ». Il obtient 557 996 voix pour et 61 638 contre. Deux jours plus tard ont lieu les élections municipales. Treize maires sur vingt (dont Clemenceau) sont reconduits. Cinq nouveaux sont des modérés. Seuls Delescluze et Ranvier représentent le parti révolutionnaire. 


### B - En province
#### 1 - La Commune de Lyon - 4 septembre 1870
Paris avait proclamé la République le 4 septembre au soir. Lyon l’avait fait, de son propre chef, dès 9 heures du matin. **La révolution arbore le drapeau rouge**, installe à la mairie un Comité de salut public, de composition majoritairement ouvrière et artisane. Le **Comité de salut public** décrète la « séparation révolutionnaire » de l’Église et de l’État, la suppression des congrégations et le séquestre de leurs biens. L’enseignement sera laïcisé. 

Il organisait une « défense lyonnaise », armant la Garde nationale sédentaire, enrôlant cinq légions de volontaires du Rhône. Il levait un emprunt patriotique de dix millions. Le travail manquant, il mettait en place des chantiers nationaux qui employaient en octobre 2 013 000 travailleurs, payés 3 F par jour. C’est bien une Commune de Lyon qui s’installe, et celle-ci, à sa manière, est un **prologue original à la Commune que Paris** se donnera six mois plus tard.

>"Et ce désir n’était pas seulement partagé par la classe ouvrière ; il l’était surtout par la bourgeoisie intelligente et laborieuse, et même par la finance et le haut commerce."

#### 2 - La Commune de Lyon - 28 septembre 1870
A l’appel de ce mouvement qui est très populaire, **le 28 septembre 1870**, ce sont plusieurs milliers d’ouvriers qui débouchent en manifestant à midi sur la Place des Terreaux.

Préfet, maire et général étaient prisonniers, tandis qu’on désignait un « général en chef des armées fédératives et révolutionnaires du Midi », Cluseret. Mais à 18 h 30, les manifestants se faisant, fatigue ou indécision, de plus en plus rares étaient dispersés par des « bons » bataillons de la Garde nationale, venant d’ailleurs de la Croix-Rousse ouvrière. Tout s’achevait pour eux sans gloire à 19 heures.

Les ouvriers rassemblés sur la Place des Terreaux vont se retrouver en effet sans armes face à la troupe et à la Garde nationale des quartiers bourgeois, qui pénètrent bientôt armées dans la cour intérieure de l’Hôtel de Ville, **ce qui fait échouer ce soulèvement populaire et la première "Commune de Lyon".**

#### 3 - Marseille et le temps des ligues
Depuis le 4 septembre, un vent de liberté soufflait sur les villes du Centre et du Midi. Avec la disparition de l’administration bonapartiste (Gambetta avait limogé 85 préfets), nombre acclamèrent des conseils ou comités municipaux républicains, souvent radicaux, s’attribuant de larges pouvoirs, exigeant, comme Lyon, des « franchises municipales ».

Le 18 septembre, Marseille, Lyon, Grenoble, Montpellier décidaient la constitution d’une _Ligue du Midi pour la Défense nationale de la République_ qui serait dirigée par Alphonse Esquiros, montagnard de 1850, arrivé le 7 septembre.

### C - Hiver de la République, étiage de la Révolution
#### 1 - Contexte de l'hiver
Comme à Paris la nouvelle de la capitulation de Metz provoqua de brefs et violents sursauts dans les grandes villes. Grenoble manifesta le 30 ; Nîmes, Saint-Étienne, Toulouse, Brest eurent chacune leur 31 octobre.

Dans Paris bloqué, la situation se faisait plus sombre avec l’hiver. Tout travail arrêté, la capitale est devenue une ville de _disoccupati_ que distraient mal les gardes au rempart ou les séances d’exercice militaire. Le blocus affame la ville où le rationnement se fait « par la cherté ». Le taux de mortalité a doublé (57 ‰). Paris connaît des émeutes de la faim et du froid.

La Ville tenait bon pourtant, attendant sa délivrance par les armées de la Loire que Gambetta levait en province. Dès son arrivée à Tours, le 11 octobre, il avait déployé une énergie remarquable. On n’obtenait nulle part de succès décisif.

Les organisations révolutionnaires mettaient de plus en plus vivement en cause le gouvernement qu’elles n’appelaient plus que de la « défection nationale ».

#### 2 - L'affiche rouge
Le Comité des vingt arrondissements, alors dominé par les blanquistes, tenta un mouvement, retentissant en apparence, mais dont l’écho semble avoir été nul. Dans la nuit du 5 au 6 janvier, il faisait placarder l’« affiche rouge ».

> "_Place au Peuple, place à la Commune !_"

Après l’échec meurtrier d’une ultime tentative pour forcer le blocus en direction à l’Ouest, à Buzenval (19 janvier 1871) où l’on fit donner pour la première fois une Garde nationale mal préparée, Paris eut un dernier accès de colère, le 22 janvier. Une poignée d’internationaux des Batignolles, quelques révolutionnaires du 18e parmi lesquels on vit [[Louise Michel]], quelque 200 gardes blanquistes du 101e bataillon du 13e arrondissement marchèrent sur l’Hôtel de Ville. Un coup de feu malencontreux parti des rangs des insurgés provoqua la riposte des mobiles bretons : la révolution avait ses premiers morts. Cet incident tragique ne sera pas oublié : le républicain Chaudey, qui commandait l’Hôtel de Ville, accusé d’avoir ordonné de tirer sur la foule, le paiera, otage de la Commune, de sa vie le 23 mai.


## Chapitre 3 - La révolte de Paris
### A - La paix
#### 1 - La capitulation
Le 28 janvier 1871 tomba comme une foudre la nouvelle que le gouvernement demandait l’armistice.

Les conditions de l’armistice étaient draconiennes ; cessation des hostilités sur tous les fronts : on livrait la capitale. L’Allemagne ne traiterait qu’avec un gouvernement régulier, issu d’une Assemblée élue. Paris était spécialement meurtri, il capitulait sans avoir été vaincu. Il devrait céder ses forts, ne conserver qu’une garnison de 12 000 hommes.

Un armistice était conclu pour vingt et un jours ; il sera prolongé d’une semaine. Il y eut de multiples protestations de dizaines de bataillons de la Garde, hommes et officiers, de mobiles, de marins artilleurs.

#### 2 - Les élections du 8 février 1871
On procéda à l’élection d’une [[Assemblée nationale]], le 8 février. C’est une chambre introuvable qui sortit du scrutin.

On votait pour ou contre la paix, mais aussi bien pour ou contre la « liberté » : entendons le refus ou non de la « dictature » républicaine, du système de gouvernement autoritaire qu’imposait la guerre. Ce fut le triomphe d’une _Union libérale_ patronnée par Thiers, élu de vingt-six départements, un échec pour la république de Gambetta, élu néanmoins dans huit départements. Des républicains modérés avaient volontiers passé alliance avec les libéraux de la nuance Thiers.

**Le vote des campagnes avait fait la décision, noyant celui de « la partie intelligente et virile des grandes villes ».**

L’Assemblée compta environ 360 monarchistes, mi-royalistes vrais, mi-conservateurs indécis, une quinzaine de bonapartistes, et seulement 150 républicains « de principe » dont tout au plus une quarantaine de radicaux gambettistes.

La situation a peu évolué en somme depuis 1869. Les villes méridionales encore une fois ont voté républicain prononcé : Bordeaux, Lille, Nantes, Rennes ; souvent radical : Marseille, Grenoble, Dijon, Saint-Étienne, Toulouse, Limoges… Lyon donnait une forte majorité à une liste conduite par Gambetta et Garibaldi, tandis que le reste du département faisait passer la liste républicaine modérée de Jules Favre.

Paris (290 000 votants) avait plébiscité cinq grands noms républicains : Louis Blanc, Hugo, Garibaldi, Gambetta, Quinet, avec 180 000 voix ou plus.

Paris patriote disait, à très haute voix, qu’il refusait qu’on touchât à sa République.

#### 3 - L'Assemblée contre Paris
Ce fut d’emblée la guerre entre les ruraux monarchistes et la Ville républicaine.

Le 28 février, il rapportait le projet préliminaire de paix : cession de l’Alsace et de la Lorraine messine, versement d’une indemnité de cinq milliards, occupation de quarante-trois départements jusqu’au règlement de la paix. « Hideux », commente Hugo. Trente mille Prussiens occuperaient, en attendant la ratification du projet, « la partie de Paris comprise entre la Seine, la rue du Faubourg-Saint-Honoré et l’avenue des Ternes ».

**Le 7 mars furent prises deux mesures d’une gravité extrême** sur les loyers et les échéances des effets de commerce ; la guerre et le blocus avaient interrompu les affaires et les paiements. **Exiger le paiement des loyers, c’était jeter à la rue la moitié de Paris – le populaire, il est vrai, avait l’habitude de déménager « à la cloche de bois ». Exiger le règlement immédiat des échéances commerciales était mener l’économie de la ville à la ruine.** La mesure heurta la bourgeoisie petite et moyenne qui bascula dans le camp des mécontents.

Le 10 mars, par 487 voix contre 154, l’Assemblée décidait d’aller s’installer à Versailles, « décapitalisant » ainsi Paris au profit de la ville des rois. L’avant-veille, on avait ôté la parole à Victor Hugo, deuxième élu de Paris ; il démissionnait, jetant aux ruraux ces mots : « Vous êtes un produit momentané, Paris est une formation séculaire. »

#### 4 - Paris en liberté
Paris avait conservé ses armes, ses canons qui étaient son bien propre, puisque nombre avaient été payés par souscription populaire. Toute autorité se diluait peu à peu dans Paris. Les quartiers populaires n’obéissaient plus à l’Hôtel de Ville. Ils s’administraient, se poliçaient, vivaient en toute indépendance. Un ordre populaire s’édifiait.

#### 5 - La fédération de la Garde nationale
En un mois se construit, radicalement différente des organisations extrémistes du Siège, la [[Fédération de la Garde nationale]]. 

"Il fallait rester en armes, se constituer en un faisceau puissant. L’exemple parisien serait contagieux, s’étendrait « au pays tout entier, aux départements débarrassés de la souillure de l’ennemi »."

Dans la nuit du 27 au 28 février, à la veille de l’occupation prussienne, la Garde eut un coup de fièvre. Elle voulait « protester contre l’entrée des ennemis, s’y opposer même les armes à la main ». Les organisations révolutionnaires – ce qu’il en subsistait : Internationale, Comité des vingt arrondissements – parvinrent à persuader le Comité central provisoire de la Garde de ne pas se laisser porter à une folle extrémité.

De Commune, pourtant, il n’était pas question. Mais comme mû par une prémonition, un délégué suggérait le 3 mars que « dans le cas où le gouvernement viendrait à être transporté ailleurs qu’à Paris, la ville devrait se constituer en République indépendante ».

À la mi-mars, la Fédération avait reçu l’adhésion de 215 bataillons sur 242, 1 325 compagnies sur 2 500, y compris la Garde de banlieue. Pour Paris seul, 1 285 compagnies sur 2 150.

> "Dans deux ou trois semaines, la ville serait contrôlée par les commandants socialistes. Par l’intermédiaire d’une fédération des gardes nationales en province, on créerait une force armée du prolétariat dans toute la France. " 
> - Varlin


### B - La semaine de l'incertitude
Rien qui ressemble moins à une révolution que l’instauration, le 26 mars, de la Commune.

#### 1 - Le 18 mars
[[Soulèvement du 18 mars 1871]]
En toute insurrection parisienne, l’objectif essentiel est l’Hôtel de Ville.

Au Quai d’Orsay, où siégeait le gouvernement, les généraux Vinoy et Le Flô avaient vu avec effroi la troupe fraterniser partout avec la foule et perdaient tout sang-froid. En dépit de l’avis des ministres civils, Favre, Simon, de Ferry, préfet de la Seine, [[Adolphe Thiers]] décidait dès 16 heures de quitter Paris et ordonnait l’évacuation des troupes. Il embellira ce qui ne fut qu’une fuite : « J’ai des devoirs envers Paris, mais des devoirs encore plus grands envers la France. » Vers minuit, le Comité central tenait séance à l’Hôtel de Ville.

#### 2 - Lendemains d'une fausse victoire
La Ville s’était libérée.
Mais que faire de cette liberté facilement, comme involontairement conquise ? On acheva d’occuper les principaux édifices publics, ministères, gares, casernes, mairies.

Les blanquistes proposaient qu’on marchât sans désemparer sur Versailles : ce que, plus tard, trop de bons « tacticiens » de la révolution reprocheront aux Parisiens de n’avoir pas su faire.

Il fallait d’abord consolider la situation dans la capitale.

#### 3 - La révolte légale
Les hommes du 18 mars choisirent d’agir dans la légalité.

>" [...] cette assemblée qui ne représente pas d’une manière complète, incontestable, la libre souveraineté populaire. Par son étroitesse de vue, par son caractère exclusif et rural, cette assemblée provinciale a prouvé qu’elle n’était pas à la hauteur des événements actuels… " - Journal officiel de la Commune, 22 mars 1871

Une dramatique séance commune se tint le **19 mars** à 20 heures à l’Hôtel de Ville.

[[Georges Clemenceau]], tout en désavouant le coup de force gouvernemental, plaida véhémentement qu’on restitue les canons, que le Comité s’efface devant le pouvoir régulier des maires, qu’on reconnaisse en dépit de tout la légitimité de l’Assemblée.

[[Jean-Baptiste Millière]], bon socialiste, prédit lucidement qu’on allait droit à de nouveaux massacres de juin.

[[Eugène Varlin]], accompagné de [[Benoît Malon]] demande :
« Nous voulons un conseil municipal élu…, des franchises municipales sérieuses, la suppression de la préfecture de Police, le droit pour la Garde nationale de nommer tous les officiers, y compris le commandant en chef, la remise entière des loyers échus au-dessous de 500 F, une loi équitable sur les échéances ; enfin, nous demandons que l’armée se retire à vingt lieues de Paris. »

Après un débat tendu d’ultimes négociations tard dans la nuit, l’accord parut se faire.

Les députés iraient porter à l’Assemblée les exigences de la capitale ; le Comité restituerait l’Hôtel de Ville. Le lendemain, à la séance d’ouverture de l’Assemblée à Versailles, dix-sept députés de Paris déposaient les projets de loi convenus : [[Georges Clemenceau]] celui d’élections municipales, [[Édouard Lockroy]] celui d’élection des cadres de la Garde nationale, Millière un troisième sur les échéances. S’installant à peine, comme abasourdie, l’Assemblée votait l’urgence pour le premier et le troisième projet. L’entente allait-elle se faire ?

#### 4 - À Versailles
L’image est belle de Marx, qui compare la révolution au cheminement d’une vieille taupe, progressant souterrainement, aveuglément : son cheminement en mars 1871 fut en vérité tortueux.

Le 20 au matin, à l’instigation du Comité des vingt arrondissements, **le Comité central refusait de rendre l’Hôtel de Ville** : il eût perdu là un atout maître.

On n’entendit donc guère s’élever les voix des [[Louis Blanc]], [[Edgar Quinet]], [[Victor Schœlcher]], notamment pas lorsque, le 22, sur proposition de Vacherot, maire du 5e, pourtant bon théoricien républicain de _La Démocratie_, **l’Assemblée rejeta le projet d’élections municipales parisiennes.**

#### 5 - La République de Paris
À Paris, le 22, quelques centaines d’« amis de l’ordre » manifestaient place Vendôme et se heurtaient à la Garde nationale. Il y eut des morts des deux côtés.

Le Comité central durcissait à son tour sa position. Fixant au 23 mars les élections, il se passerait du consentement de Versailles et de la collaboration des maires. 

> "Les grandes villes ont prouvé, lors des élections de 1869 et du plébiscite, qu’elles étaient animées du même esprit républicain que Paris ; _les nouvelles autorités républicaines_ espèrent donc qu’elles lui apporteront leur concours sérieux et énergique dans les circonstances présentes, et qu’elles les aideront à mener à bien l’œuvre de régénération et de salut qu’elles ont entreprise… Les campagnes seront jalouses d’imiter les villes" - Journal officiel de la Commune, 19 mars 1871).

> " Paris, depuis le 18 mars, n’a d’autre gouvernement que celui du Peuple… Paris est devenu ville libre. Sa puissante centralisation n’existe plus. " - Émile Eudes

C'était une vision très utopique de la "République de Paris" qui voulait unifier le peuple de toute la France. Pedant les élections municipales, tous, toutes tendances politiques confondues, communiaient dans une même illusion.

#### 6 - Le parti des maires
Le Comité central nommait le 24 trois généraux : [[Émile Eudes]], Duval, deux blanquistes, et Brunel. Leur première proclamation disait : « Tout ce qui n’est pas avec nous est contre nous. »

Le petit groupe des maires et députés n’en continuait pas moins, inlassablement, de tenter de s’entremettre. On a trop facilement fait d’eux les instruments aux mains d’un Thiers cherchant à gagner du temps ; ils étaient pour la majorité des républicains de bon aloi : Tirard, [[Georges Clemenceau]], Arnaud de l’Ariège, [[Sadi Carnot (homme d'État)]], [[Édouard Lockroy]], [[Victor Schœlcher]]. 

Ces maires étaient donc menacés des deux côtés, par l'Assemblée nationale et par la population parisienne, ne pouvant pas trouver de compromis.

> « Nous sommes pris entre deux bandes de fous. » - [[Georges Clemenceau]]

Excédés par les outrances de Versailles, les conciliateurs finirent par céder, acceptant les élections pour le 26.

#### 7 - Les élections du 26 mars 1871
La campagne avait été courte mais vive.

L’hostilité des beaux quartiers se traduisit par l’abstention : plus de 60 %. On vota peu dans les arrondissements pauvres de rive gauche, environ 35%. Tout se passe comme si le seul **quart nord-est de la Ville** – son noyau dur populaire – s’était senti réellement concerné : 77 % de votants dans le 20e, 62 % dans le 19e, de 55 à 60 % dans les 10e, 11e, 12e.

**Les « communeux » étaient loin, compte tenu des abstentions, d’être majoritaires.**
La Commune compta au total 79 membres, dont ne siégèrent jamais qu’une cinquantaine, une soixantaine aux meilleurs jours. 

On comptait neuf blanquistes avérés, groupe facile à cerner, avec Eudes, Duval, Rigault, Ferré, Tridon. L’Internationale et les chambres syndicales avaient une quarantaine d’élus. Vingt sont francs-maçons, majoritairement du rite écossais. Quatorze seulement avaient été membres du Comité central.

Le fait majeur – la mémoire socialiste aime à le rappeler – est que **l’Assemblée était principalement formée de travailleurs.**


## Chapitre 4 - La Commune de Paris : les œuvres
« L’œuvre première de la Commune, ce fut son existence même. » Le pamphlétaire qu’est Marx peut se permettre cette facile ellipse. Force est de constater que, faute évidemment de temps, l’œuvre de l’Assemblée communale fut mince.

« Nous ne sommes, dira Édouard Vaillant, qu’un petit parlement bavard », ou Billioray : « La Commune passe son temps à des niaiseries. »

### A - Organiser l'apocalypse
#### 1 - Structures
Municipalité, gouvernement ? La Commune fut un peu des deux à la fois.
Elle mit d’abord en place neuf commissions collégiales qui étaient autant de petits « ministères » : Services publics, Finances, Enseignement, Justice, Sûreté générale, Subsistances, Travail et Échange, Guerre, Relations extérieures.

Le 21 avril, les neuf commissions, renouvelées, avaient désormais à leur tête chacune un délégué. Les neuf délégués constituaient l’exécutif. C'est la [[Commission exécutive de 1871]].

Tant bien que mal, la Commune assura le fonctionnement de l’énorme machine administrative parisienne. Grâce à quatre hommes essentiellement : Jourde, Andrieu, Viard, [[Eugène Varlin]].

Décisif restait le rôle des commissions d’arrondissements, désignées par les membres de la Commune. À ce niveau, ce fut bien un gouvernement direct que connut Paris, non sans tensions avec le pouvoir « central » de l’Hôtel de Ville.

#### 2 - Une oeuvre républicaine
La Commune satisfit immédiatement **deux grandes revendications** chères au cœur des républicains parisiens. 
- Le 29 mars, « **la conscription est abolie**… Tous les citoyens valides font partie de la Garde nationale ». 
- Le 2 avril, suppression du budget des Cultes et **séparation de l’Église et de l’État**, « considérant que… la liberté de conscience est la première des libertés…, que le clergé a été le complice des crimes de la monarchie contre la liberté ».
C’était là empiéter sur deux domaines relevant de la compétence nationale.

À la commission de l’Enseignement, Vaillant œuvrait à la **laïcisation de l’école**. C’était, avec la gratuité et l’obligation, une autre grande revendication parisienne, du peuple et de la bourgeoisie, petite ou moyenne, confondus.

> « Les faits et les principes scientifiques seront enseignés sans aucune concession hypocrite faite aux dogmes que la raison condamne et que la science répudie. L’enseignement public de la morale ne procède d’aucune autre autorité que celle de la science humaine. » - Programme du 20ème

Des propositions de nombreux intellectuels donc notamment [[Ferdinand Buisson]] s'y ajoutent : 
- Des écoles pour filles.
- Une éducation plus scientifique
- Une éducation professionnelle, « II faut qu’un manieur d’outil puisse écrire un livre. »

Le 14 avril, le peintre Courbet avait constitué une fédération des artistes, « gouvernement du monde des arts par les artistes ».

#### 3 - La Commission du travail
Conduite par Frankel et Malon, la **Commission du travail et de l’échange** accomplit une **œuvre sociale** remarquable. Elle décréta le 27 avril l’interdiction dans les ateliers et administrations des amendes ou retenues sur les salaires ; le 28, la suppression, réclamée depuis longtemps par la corporation, du travail de nuit des ouvriers boulangers et surtout de leurs bureaux de placement, qui exigeaient des impétrants des droits très lourds.

La commission jeta surtout les bases de vastes projets d’organisation du travail, dans le **prolongement** de ce que les **sociétés ouvrières en 1848** avaient imaginé, œuvrant pour un [[Notes/Socialisme]] de l’association, des « ouvriers de métier ».

On avait « communalisé » plusieurs entreprises appartenant à la ville ou à l’État, la Manufacture des tabacs, l’Imprimerie nationale, gérées désormais par leurs ouvriers. Mieux gérées, puisque par les travailleurs eux-mêmes, s’entraidant au lieu de se concurrencer, « apportant à l’échange leur produit au prix de revient ». Il y a « syndicalisation » des moyens de production et un système de crédit communal gratuit.

à lire : https://www.cadtm.org/La-Commune-de-Paris-la-banque-et-la-dette

Le rôle assigné aux Chambres de métiers, rendait plausible la réalisation d’un système qui ne se limiterait pas à Paris mais s’étendrait à toutes les agglomérations industrielles constituées en Communes.

#### 4 - Guerroyer
Le principal du budget était consacré à la lutte contre Versailles. Sur le papier, la Commune disposait de quelque 180 000 hommes de la Garde nationale, et des corps francs aux noms sonnants, Vengeurs de Flourens, Lascars, Tirailleurs de la Commune…

Jamais la Commune ne disposa en réalité de plus de **quelques milliers de combattants**. En face, Versailles, qui n’avait au départ que quelques régiments démoralisés, édifiait très vite une armée avec des recrues fraîchement levées ou des prisonniers que l’Allemagne libérait : **130 000 combattants**.

Les Versaillais s’emparaient le 2 avril de Courbevoie, point névralgique de la défense parisienne. Les 3 et 4, Paris tenta en réponse une marche sur Versailles.

Paris se retrouvait une seconde fois en état de blocus, entre les Prussiens, à l’Est et au Nord, et les Versaillais qui avançaient à l’Ouest et au Sud. Le 20 mai, après de nombreuses conquêtes au dehors de la ville, les batteries versaillaises ouvraient un feu nourri sur l’enceinte.

#### 5 - Finances
Jourde, « bon comptable », trouva au jour le jour l’argent nécessaire pour faire vivre et combattre Paris.

La Commune obtint de la Banque 16 765 202 francs, dont 9 401 809 représentaient le solde créditeur de la Ville à la Banque. Dans le même temps, Versailles recevait 257,6 millions.

Quelques-uns songèrent à s’en emparer. Mais, s’en prendre à la Banque, c’était risquer l’écroulement du fragile système parisien et français d’escompte, provoquer un effondrement monétaire.

Tout ce qu’on pouvait imaginer, c’était, panacée douteuse des systèmes socialistes du moment, la transformation de la Banque en Banque du peuple, des associations ouvrières.

### B - La Commune se déchire
Dès la fin d’avril, la situation militaire s’était dramatiquement assombrie. Il manquait une direction sérieuse de la lutte comme du travail de l’assemblée communale.

#### 1 - Le Comité de salut public 
[[Comité de salut public]]

Le 1er mai, quarante-cinq élus se prononçaient pour, vingt-huit contre. Ceux qu’on désigne désormais comme la « majorité » croyaient qu’on allait tout sauver en confiant la dictature à quelques-uns. La « minorité » protestait contre la « confusion des pouvoirs ».

On vivait dans un irréel parfait : à situation désespérée, on cherchait un remède miracle. Un premier comité désigné le 1er mai ne fit rien d’efficace.

> « Pour la population parisienne, le Comité de salut public, ce sont les faisceaux, c’est la hache en permanence. Votre comité est annihilé sous le poids des souvenirs dont on le charge. » - [[Charles Delescluze]]

Le 15 mai, la majorité se constituait en « fraction révolutionnaire radicale », et déniait à la minorité le droit de siéger.

À l’appel désespéré de Delescluze, un semblant de réconciliation intervenait le 17. Mais c’était le glas de la Commune. « Nous sommes, dit Lissagaray, à la période de l’immense lassitude. »

## Chapitre 5 - La Commune, ce "Sphinx"

Dès juin 1871, Marx a posé, et résolu à sa manière, dans le style incomparable qui est le sien, la redoutable question : « Qu’est-ce donc que la Commune, ce sphinx qui tarabuste si fort l’entendement bourgeois ? »

Les idées marxiennes sur l’État ont été par la suite déformées, tant par les sociaux-démocrates de la IIe Internationale que par leurs adversaires léninistes de la IIIe. À aucun moment, il n’est question dans _La Guerre civile_ de « dictature du prolétariat » : c’est Engels qui eut plus tard ce mot imprudent : « Regardez la Commune de Paris, c’était la dictature du prolétariat. » Marx évoque bien plutôt l’abolition de l’État.

La Commune fut un sphinx, semble-t-il, tout aussi bien pour l’entendement socialiste. Trop de débats, anciens ou récents, ont de ce fait voilé la signification du projet de 1871.

### A - Paris, ville libre
#### 1 - La Déclaration du 19 avril
Il exista à coup sûr deux « sensibilités » opposées au sein de la Commune, qu’on dit trop vite l’une jacobine, l’autre proudhonienne.

Leurs partisans s’étaient accordés pour voter, le 19 avril, unanimement, une [[Déclaration au peuple français]], « charte », ou, comme on a dit, « testament » de la Commune. Paris prônait « l’autonomie de la Commune étendue à toutes les localités de France ». Chaque commune voterait son budget, nommerait ses fonctionnaires, organiserait sa police, son enseignement, sa défense, avec « intervention permanente des citoyens dans les affaires ».

> « Nos ennemis se trompent ou trompent le pays quand ils accusent Paris de poursuivre la destruction de l’unité française constituée par la Révolution aux acclamations de nos pères, accourus à la fête de la Fédération de tous les points de la vieille France. L’unité, telle qu’elle nous a été imposée jusqu’à ce jour par l’Empire, la monarchie et le parlementarisme, n’est que la centralisation despotique, inintelligente, arbitraire ou onéreuse. L’unité politique, telle que la veut Paris, c’est l’association volontaire de toutes les initiatives locales… La révolution communale… inaugure une ère nouvelle de politique expérimentale, positive, scientifique. »

Une critique du texte est l'oubli des campagnes, il y a une grande illusion que les communes françaises suivront toutes, même en Basse-Bretagne comme ils disent. 

#### 2 - La Ville Libre
> « Paris libre dans la France libre et marchant du même pas que les départements, Paris redeviendra le cœur et la tête de la France et de l’Europe, sans prétention à une suprématie qui serait la négation de ses principes les plus chers. » - Delescluze

> « Paris autonome n’en doit pas moins rester le centre du mouvement économique et industriel, le siège de la Banque, des Chemins de Fer, d’où la vie se répande plus largement à travers les veines du corps social » (_JOC_, 1er avril).

On formerait de vastes ensembles économiques, politiques, homogènes, les communes rurales étant rattachées aux villes ou réunies en agglomérations de la taille du canton.

#### 3 - De quelques sources vives
L’idée d’une France communalisée est vivace au XIXe siècle, puisant à plusieurs sources.

De Proudhon semblait venir tout prêt le projet d’une commune qui « comme l’homme, comme la famille, comme toute individualité et toute collectivité intelligente, morale et libre, est un être souverain » (_De la capacité politique des classes ouvrières_, 1865). « Supposons cette belle unité française divisée en 36 souverainetés, d’une étendue moyenne de 6 000 km2, et de 1 million d’habitants… ».

Pour le communiste Théodore Dézamy, « la communauté est le mode naturel et parfait de l’association. Cet idéal ne peut être réalisé que lorsque des groupes humains de 10 000 personnes habiteront chacun dans un palais communal. Chaque commune jouira de son autonomie. Les communes fédérées formeront la Nation et les nations fédérées l’Humanité. L’État politique fera place à une simple administration, à la tête de laquelle se trouvera un chef de comptables » (_Code de la communauté_, 1842).

Constantin Pecqueur : « L’État politique disparaîtra graduellement pour faire place à l’organisation économique des fédérations égalitaires des travailleurs solidarisés d’Europe d’abord, puis de toutes les parties du monde ».

La majorité, les Parisiens, proches du collectivisme des Belges insistaient sur la nécessité d’une collectivité nationale économiquement une, mais politiquement lâche.

Pindy déclarait : « Ce mode de groupement devient un agent de décentralisation, car il ne s’agit plus d’établir dans chaque pays un centre commun à toutes les industries, mais chacune aura pour centre la localité où elle est le plus développée ; pour la France, tandis que les houilleurs se fédéreraient autour de Saint-Étienne, les ouvriers en soieries le feraient autour de Lyon, les industries de luxe à Paris. »

### B - Relecture de jacobinisme 
Le discours prononcé par Robespierre le 11 mai 1793 :
« Fuyez la manie ancienne des gouvernements de vouloir trop gouverner. Laissez aux communes, laissez aux familles, laissez aux individus… le soin de diriger eux-mêmes leurs propres affaires en tout ce qui ne tient point essentiellement à l’administration générale de la République. En un mot, rendez à la liberté individuelle tout ce qui n’appartient pas naturellement à l’autorité publique, et vous aurez laissé d’autant moins de place à l’ambition et à l’arbitraire. »

C’étaient les thermidoriens, non les Montagnards, qui avaient fait disparaître l’institution de la première Commune de Paris le 19 vendémiaire an IV, supprimant ces franchises que Paris ne retrouvera pas avant nos jours.

En octobre 1851, Charles Renouvier, Fauvety, le communiste lyonnais Joseph Benoît avaient publié un projet de _gouvernement direct, Organisation communale et centrale de la République_, proposant la reconstruction de la France politique à partir de 2 000 « communes émancipées », administrées chacune par une assemblée élue annuellement : « Il est moins question d’administrer que de s’administrer. Lorsque chaque commune fera elle-même ses propres affaires, l’administration centrale (assemblée de 900 “mandataires”) ne sera plus obligée d’entrer dans ces mille détails où elle se perd aujourd’hui. »

La _Déclaration_, qui n’est ici encore qu’une ébauche, sous la forme d’un compromis entre projet fédéraliste et décentralisation jacobine, vient en vérité au terme d’une longue réflexion menée pendant tout le premier XIXe siècle, sur ce que doit être une république réellement « démocratique et sociale ».

Cette idée se fond étroitement en 1848 avec la puissante idée d’« Association » généralisée, économique, sociale, politique, dont la cellule communale sera à l’évidence l’élément premier. Elle se mêle, dans les années 1860, s’édulcorant peut-être, aux multiples réflexions – républicaines notamment – sur la décentralisation nécessaire.

Ce n’est pas la voie qu’empruntera la IIIe République.

## Chapitre 6 - Vivre à Paris en Floréal
Paris est en somme le « laboratoire » d’une « expérience de physique sociale » dont l’étude au quotidien sera d’autant plus fructueuse. L’historien se doit de reconnaître l’apport de cette approche neuve ; il doit aussi en marquer aussi les limites.

### A - Sociologie du "quotidien révolutionnaire"
#### 1 - Libertés de la Ville
Amis ou hostiles, ceux qui ont été des témoins directs s’accordent pourtant à décrire dans la ville une atmosphère, de paix, de tranquillité retrouvées. Vallès a des mots de poète : « Le murmure de cette révolution qui passe, tranquille et belle, comme une rivière bleue… »

Neutre alors, Zola observait par une belle journée de mai « aux Tuileries, les femmes brodant à l’ombre des marronniers, tandis que, là-haut, du côté de l’Arc de Triomphe, des obus éclatent ».

Le 18 mars, les prolétaires des quartiers extérieurs avaient repris possession, avec l’Hôtel de Ville, du vieux Paris central. C’est au tour du Palais impérial, symbole du despotisme : les jardins des Tuileries furent ouverts au Peuple le 24 mars, le Palais le 4 mai : on y entrait pour dix sous. « Le Peuple souverain est reçu en souverain dans son Palais. »

Il y a fêtes, reprises des monuments importants et desctruction de ceux qui fâchent, comme la colonne de vendome ou l'appartement d'[[Adolphe Thiers]]. 

Ce jour tragique, les Versaillais entraient dans la Ville, Paris chantait.

#### 2 - De la spontanéité 
Le 18 mars fut spontané. Spontanée, la création de la Fédération de la Garde nationale. « Spontanéité » est à prendre aussi en un sens plus élaboré : « manière d’être de la révolution dans sa quotidienneté créatrice d’événements et d’idées ».

L'idée c'est que dans la spontanéité des intellectuels, les Parisiens ont perdu souvent contact avec la réalité populaire. 

> « Spontanément, la spontanéité est anarchisante » (Henri Lefebvre).

Contradiction bien aperçue par un témoin : « Rêver de discipliner cette fantasia guerrière, de hiérarchiser ces égaux, n’est-ce pas vouloir qu’une révolution populaire ne soit plus une révolution populaire ? »

#### 3 - Sociabilité : les clubs
La forme de sociabilité populaire par excellence, c’est la compagnie de la Garde nationale, qui groupe les habitants de maisons et de rues voisines. 

> « Chaque bataillon, chaque compagnie avait fini par former une petite ville ou une petite république, ayant ses délibérations, nommant ses officiers et ses délégués, soumise à la vie fiévreuse de la grande crise » (Camille Pelletan).

Forme plus politisée de sociabilité, les clubs. Ils s’étaient multipliés pendant le Siège. Interdits en février, ils ont repris vie en avril ; ils sont un immense défouloir.

Une cinquantaine s’installait préférentiellement dans les églises, réappropriées par le peuple, et débaptisées. Cette réappropriation des « boutiques à messe » désigne l’ennemi le plus détesté du communard : le prêtre, le **« marchand de religion »**. En 1848, le peuple avait été respectueux d’un catholicisme charitable. Vingt ans d’alliance ostentatoire de l’Église et de l’Empire, une vigoureuse campagne de libre-pensée républicaine ont fait resurgir le vieux tréfonds d’anticléricalisme populaire.

Se développe dans les clubs un communisme élémentaire que résume bien le vieil axiome que « la propriété c’est le vol ». On hausse le ton pour dénoncer les parasitismes, monopoles des grandes compagnies industrielles, de chemin de fer ou de banque. Le petit patron, l’artisan de bourgeoisie populaire, ne sont pas des adversaires : on les côtoie au club ou au bataillon. On s’en prend seulement à quelques gros entrepreneurs qui ont confisqué les bénéfices de l’équipement de la Garde, au détriment des associations de cordonniers ou de tailleurs.

Dans sa conduite quotidienne, le révolutionnaire doit être « énergique ». Il doit être vertueux : **« Mort aux voleurs ! »** est le mot d’ordre de toute révolution.

Les sociologues ont défini trois catégories qui constituent la « quotidienneté » populaire ; la **violence** : on multiplie perquisitions, arrestations, on veut « dresser la guillotine sur toutes les places », « fusiller tous les riches » ; la **vigilance** : « Faisons-nous gendarmes, entrons dans toutes les maisons et les boutiques… Veillez le clergé, faites sonder les églises. » Le tout dans un climat de **bonhomie** : la violence est surtout verbale ; ces hommes qui voulaient « faire tomber cent mille têtes » étaient aussi bien ceux qui, le 9 avril, brûlèrent solennellement, place Voltaire, la guillotine, « instrument de terreur et de répression ».

#### 4 - Mémoire
Moins que de spontanéité, l’historien parlerait volontiers de mémoire, la prodigieuse mémoire du Paris populaire. On vit au rythme du calendrier révolutionnaire. La Commune s’est instaurée le 7 germinal, s’épanouit en floréal, meurt début prairial. Avec un parfait naturel, le Peuple retrouve les mots, les gestes d’un passé à peine centenaire : déchristianisation, haine du riche, de l’oisif, de l’accapareur, révolte contre la « tyrannie ».

Symbole de cette continuité un [[Charles Delescluze]] : commissaire de la République en 1848, bagnard de l’Empire, il aurait participé déjà, en juin 1832, aux émeutes du cloître Saint-Merry.

Dans la jeune génération républicaine, on est girondin, dantoniste, robespierriste. L’homme du peuple en a entendu des échos : il est, pour sa part, résolument du camp de la Montagne.

**Un texte majeur est alors dans toutes les mémoires populaires : la _Déclaration des droits de l’homme et du citoyen_, mais dans sa version d’avril 1793, celle qui avait été proposée par Robespierre : « Le Peuple est souverain ; le gouvernement est son ouvrage et sa propriété : les fonctionnaires publics sont ses commis… Les rois, les aristocrates, les tyrans sont des esclaves révoltés contre le souverain de la terre, qui est le genre humain… » Le Peuple de 1871 a presque les mêmes mots : « L’État, c’est le peuple se gouvernant lui-même… » « Serviteurs du Peuple, ne prenez pas de faux airs de souverains… Restez dans votre rôle de simples commis » (_Le Prolétaire_, 19 mai).**

#### 5 - Sociabilité ouvrière : l'avenir
En progrès constants jusqu’au début de 1870, le mouvement ouvrier parisien avait vu ses forces se diluer pendant le Siège. L’Internationale reprend vie sous la Commune, poursuit la reconstitution de ses sections de quartier et des chambres syndicales.

#### 6 - L'Union des femmes
Comme en toute révolution, les femmes – en réalité quelques groupes de femmes révolutionnaires – tinrent un rôle d’avant-garde.
On entendait dans leurs clubs les propos les plus avancés : « Plus de patrons qui considèrent l’ouvrier comme une machine à produire… Les ateliers dans lesquels on vous entasse vous appartiendront, les outils qu’on met entre vos mains seront à vous. »
Mais L’_Union_ fut bien davantage ; elle était ouvrière, syndicale, proclamait « la révolution sociale absolue, l’anéantissement de tous les rapports juridiques et sociaux existant actuellement…, la substitution du règne du travail à celui du capital, en un mot l’affranchissement du travailleur par lui-même… »

Primauté est donnée au social : on ne voit pas de femmes revendiquer alors, comme quelques-unes l’avaient fait en 1848, un **droit de suffrage** que leurs compagnons révolutionnaires leur auraient à coup sûr refusé. Mais l’Union achevait à peine de constituer ses comités d’arrondissement le 22 mai et en resta au stade des vœux et des principes.

#### 7 - La presse populaire
Paris retrouvait la liberté d’écrire. Liberté pour les feuilles populaires, mais étroite surveillance pour les journaux adverses ; le 21 mars, le Comité central interdisait les trop versaillais _Gaulois_ et [[Figaro]] ; une dizaine de journaux furent interdits en avril et mai.

Outre _L’Officiel_ qui continuait de porter le nom de _Journal officiel de la République_, la Commune eut six grands journaux : _Le Cri du peuple_ de Vallès ; _Le Père Duchêne_ d’Eugène Vermersch ; _Le Vengeur_ de Félix Pyat ; _La Commune_ de Georges Duchêne ; _Paris libre_ de Vésinier ; _La Sociale_ d’André Léo.

Proudhonienne, _La Commune_, et _Le Vengeur_, jacobin, tentaient de théoriser l’autonomie communale. Le journal le plus lu – 50 000 à 100 000 exemplaires – était _Le Cri du peuple_, grâce aux vibrants éditoriaux de Vallès.

En dépit de la censure subsistait une presse simplement républicaine. **Antiversaillaise, elle ne cachait pas sa désapprobation des « excès » de cette révolution populaire** : ainsi du _Mot d’ordre_ de Rochefort ou du _Rappel_ des fils Hugo. Ces feuilles tenaient une position médiane entre Versailles royaliste et Paris.


### B - Les hommes de 1871
L’insurgé est un homme dans la force de l’âge ; **64 % des condamnés ont de 20 à 40 ans** (la proportion, dans une population « normale », serait de 52 %). Trois quarts sont nés en province, un quart seulement à Paris. La proportion est ici normale : depuis 1820, la population parisienne se nourrit d’immigration. Certains signes laissent apparaître qu’on a affaire à une population point toujours bien intégrée : 49 % de célibataires, contre 46 % de mariés, valeur très supérieure à la normale. On compterait 11 % d’illettrés ; 62 % ne savent que mal lire et écrire. 21 % ont eu maille à partir avec la justice, dont 13 % pour « crimes et délits » contre les personnes et les propriétés (de petits vols, souvent répétés, des rixes).

Socialement, dans la majorité des cas, l’insurgé est un salarié, mais petits patrons ou marchands, ouvriers établis ne sont pas absents.

Sont surreprésentés les ouvriers du bâtiment et les journaliers sans spécialité, les ouvriers des métaux, ceux-là des spécialistes dont le travail est proche du métier d’art. Le tableau diffère peu de celui de 1848. Place sensiblement moindre en 1871 des ouvriers artistes, du textile-vêtement : cela tient à l’évolution en vingt ans du travail à Paris. **Une différence plus notable tient à la part des employés : à peu près nulle en 1848, considérable en 1871** : ils ont été cette fois des cadres pour l’insurrection du Tout-Paris du travail.

Pour un Roger V. Gould, alors que juin 1848 serait une insurrection purement sociale, 1871 serait au contraire premièrement politique, réaction non pas « de classe », mais de « communauté », de proximité d’habitat consolidée par l’opération haussmannienne de rejet des travailleurs en périphérie de la capitale. L’approche, pour n’être pas franchement inexacte, est simpliste. Il va de soi que, tout comme juin, 1871 est en même temps « démocratique et sociale », comme doit l’être la vraie République dont, dans les deux cas, les insurgés se réclament.









## Chapitre 7 - D’un « tiers parti » républicain
Paris, a-t-on dit, fut laissé « seul avec ses idées ». Il ne fut pas, c’est vrai, suivi en mars sur la voie d’une insurrection « communale » par les grandes villes, qu’on avait vues se lever au lendemain du 4 septembre. Tout un mouvement cependant, provincial et parisien, bien mis en lumière aujourd’hui, l’accompagna, à sa manière originale.

### A - Provinciales, II
Et pas plus qu’il n’y eut cette fois encore de mouvement « télégraphié », pas davantage ce ne fut un décalque de ce qui se passait à Paris : les villes de province vivaient à leur rythme propre. Elles étaient d’ailleurs dans une ignorance à peu près complète de la signification exacte des événements de la capitale, qui ne le savait guère encore elle-même.

#### 1 - Lyon 
La ville était toujours Commune : elle avait dû seulement, le 3 mars, amener son drapeau rouge. La municipalité, conduite par Hénon, bon républicain, se montra tout de suite hostile à la redoutable rupture de la légalité dans la voie de laquelle s’engageait Paris.

Le 22 mars, la ville s’agita. Une foule, appuyée par un bataillon de la Guillotière, envahissait l’Hôtel de Ville. Les manifestants voulaient obtenir du maire qu’il proclame une seconde fois la Commune, la restaurant dans la plénitude des libertés conquises le 4 septembre. Le soir, une nouvelle Commune est installée : « Notre ville qui, la première, a proclamé la République ne pouvait tarder d’imiter Paris. Elle vient de reprendre la direction de ses intérêts… » Mais vingt-deux commandants de bataillons sur vingt-quatre s’opposaient au mouvement. Lyon, satisfaite de ses franchises, n’en voulait pas davantage.

#### 2 - Le Centre
Le 23 mars, des Grenoblois manifestaient devant la préfecture. Du 25 au 27, Saint-Étienne connut cinquante heures d’insurrection des ouvriers de la Manufacture d’armes, et de la grande corporation traditionnellement rebelle des passementiers.

Globalement les révoltes sont réprimées dans la violence.

#### 3 - Le Midi 
Du 24 au 31 mars, à Narbonne, le journaliste radical Émile Digeon, s’appuyant sur un _Club de la Révolution_, se proclamait « commandant des forces républicaines de l’arrondissement », formait le projet d’entraîner Carcassonne, Béziers, Cette, Montpellier, Perpignan, pour « tendre la main à Toulouse et Marseille », soulever tout le Midi.

Marseille connut la première Commune sanglante. Le 23, la foule et les « gardes civiques » s’emparaient de la préfecture. Fausse émeute, imprudemment pacifique. Avant qu’on ne procède à de nouvelles élections municipales, décidées pour le 5 avril, la troupe, aux ordres du général Espivent de Villeboisnet, reprenait la ville, avec une brutalité froide, aux cris de « Vive Jésus, vive le Sacré-Cœur ! ». La bataille fit 150 morts du côté du peuple, 30 du côté de l’ordre.

### B - « Entre Paris et Versailles il y a du chemin »
Il existe une grande différence de conception entre les parisiens révolutionnaires et les habitants des provinces ou de Versailles. La République étant revenue, déjà avec opposition dans ces milieux, était menacée par ce socialisme qui virait rouge. Fallait-il la guerre civile « quand la République existe…, quand les Prussiens sont là ? ».

#### 1 - La ligue des droits de Paris 
[[Léon Gambetta]] lui-meme était perdu dans ces idées révolutionnaires, il était un fervent républicain. Il dit : « Qu’allons-nous devenir ? Tout ceci ne peut finir que par une catastrophe : des journées de septembre ou une Terreur blanche à courte échéance, et peut-être les deux. Il n’y aurait qu’un moyen de sauver la situation ; déclarer la République institution définitive et convoquer dans Paris la nouvelle Chambre en indiquant d’avance le programme législatif qu’elle devra suivre ; puis rentrer hardiment dans la capitale en lui tenant le langage qui convient à la fois à la France et à la population de la grande cité. »

Dans Paris, ils furent quelques-uns, proches du radicalisme gambettiste, qui prirent courageusement parti entre Versailles royaliste mais légal et Paris républicain mais insurgé une position de médiation. On en compte notamment dans la petite et moyenne bourgeoisie, chez les artisans et les commerçants. Du côté populaire – les lendemains des élections du 26 mars l’avaient montré –, l’action du Comité central n’avait pas fait vraiment l’unanimité, et, de plus en plus, de bons partisans des franchises communales trouvaient qu’on allait trop loin.

Le 6 avril, la _Ligue_, principale organisation et la plus représentative, proposait un programme d’entente : reconnaître la République ET les droits de la ville de Paris. Les revendications étaient proches des exigences **minimales** des révoltés.

_L’Union des Chambres syndicales_ proclamait que « Paris a fait une révolution aussi acceptable que toutes les autres, et, pour beaucoup d’esprits, c’est la plus grande qu’il ait jamais faite, c’est l’affirmation de la République et la volonté de la défendre… »

Le vote, le 14 avril, par l’Assemblée, d’une loi municipale contraignante bloqua toute possibilité de discussion. Elle était – Thiers l’avait imposé, y compris contre une droite légitimiste volontiers « municipaliste » – fortement centralisatrice. Bordeaux et Lyon étaient les épicentres d’une rébellion pacifique. Le 25 avril, _La Tribune_ de Bordeaux fixait aux municipalités un triple objectif : « Terminer la guerre civile, assurer les franchises municipales, constituer la République. »

#### 2 - Les élections du 30 avril 1871
On n’a pas étudié de près leurs résultats, qui paraissent bien marquer un important tournant ; sur 36 000 conseils élus, il n’y en aurait guère eu plus de **8 000** qui aient osé se déclarer franchement **monarchistes ou bonapartistes**. Victoire sûrement des républicains, parfois de radicaux avancés.

Ce 30 avril, Lyon fit une ultime récidive insurrectionnelle : l’impulsion venait toujours de Suisse et de militants bakouninistes ; un tract parlait de fédération révolutionnaire des communes de France. Le scrutin reporté au 7 mai donnait à Lyon la majorité aux radicaux. Barodet, Hénon, et quelques hommes de l’ancienne Commune, s’installaient à la mairie. Leur premier geste fut de défi à Versailles : contrevenant à la loi du 14 avril, la seconde ville de France entendait se donner librement son maire, Hénon. Lyon garda une municipalité radicale jusqu’en août 1873, date à laquelle le gouvernement décida de nommer son maire.

#### 3 - Le tiers parti
Les élections municipales avaient conforté le parti médiateur. 

Lyon dit : « Paris n’est pas la Commune, mais, tout en désapprouvant ses excès, Paris veut les libertés municipales comme base de la République. La cause qu’il défend est celle de toutes les villes de France… » Le 22 avril, Edgar Quinet, plusieurs députés de Paris déposaient un projet de loi qui prévoyait une représentation particulière des villes de plus de 35 000 habitants : « Foyers d’activité intellectuelle, élément indispensable de la nationalité française. » Venu en discussion le 8 mai, le projet fut repoussé.

On convint alors de la tenue de deux congrès, l’un, le 14 mai, à Lyon, des « villes républicaines », l’autre, le 16, des « villes patriotiques » à Bordeaux. Thiers interdit l’un et l’autre. Le congrès lyonnais se tint malgré tout le 16. Leurs propositions ne seront jamais acceptées. 

**_La Tribune_ bordelaise ne pouvait que clore cette courageuse initiative du cri : « La Commune est morte, Vive la Commune ! »**

Ainsi s’était affirmé, à voix haute et claire, ce qu’il est judicieux d’appeler un **« Tiers parti » républicain** (Jeanne Gaillard). Les villes de province tentèrent de s’interposer. Devant l’entêtement borné de Versailles, elles penchaient toujours davantage du côté de Paris.

## Epilogue - La Terreur tricolore
Paris avait été hermétiquement encerclé. Le 21 mai, quatre corps d’armée, 130 000 hommes pénétrèrent dans une ville insouciante.

### A - La semaine sanglante
À 6 heures du soir, une avant-garde s’emparait de toutes les portes du Sud-Ouest.
La surprise avait été totale, le tocsin ne sonna que ce 22 au matin. Delescluze, délégué à la Guerre, lançait un dramatique appel : « Assez de militarisme ! Place au Peuple, aux combattants aux bras nus ! L’heure de la guerre populaire a sonné. »

Le 24 à 9 heures du soir, une brigade atteignait l’Hôtel de Ville, qui flambait aussi.

La moitié de Paris était conquise. Les résistances n’avaient été qu’épisodiques : on n’était pas encore dans la ville populaire. Au soir du 24, les Versaillais arrivaient à la frontière nord-sud – la même qu’en juin 1848 – qui, de la gare du Nord à la Bastille, interdit l’entrée du Paris des bras-nus.

Le 28, un ultime bastion tenait, à la croisée des rues de Belleville et du Faubourg-du-Temple ; la barricade de la rue de la Fontaine-au-Roi céda vers 15 heures. Mac-Mahon pouvait lancer son ordre du jour : « Paris est délivré… L’ordre, le travail, la sécurité vont renaître ! » 

L’armée de Versailles n’aurait eu, officiellement, depuis le début d’avril, **que 873 morts**, 6 424 blessés : compte sûrement insuffisant. En face, les morts se comptent **par milliers** : on laisse peu de survivants derrière une barricade. Le vainqueur procéda à des massacres systématiques. Ils ne peuvent s’expliquer seulement par l’énervement des troupes, ou la sauvagerie des corps à corps de guerre civile. Les exécutions sommaires commencèrent le 22 mai, quand les troupes n’avaient pas encore rencontré de résistance. Elles se firent massives, œuvre moins des troupes régulières que de corps spéciaux qui ratissaient les quartiers, arrêtaient au moindre soupçon, exécutaient.

« On n’en parle que secondairement, note Élie Reclus. La destruction des propriétés est chose bien plus émouvante que la destruction de la vie humaine. » Plutôt que de les rendre, les communards avaient mis le feu à l’Hôtel de Ville, aux Tuileries, au Palais de Justice ; autant de symboles. Un tiers de Paris brûlait : cela aussi explique l’image qu’on se fit dans l’autre camp de la révolution populaire.

### B - La justice des hommes 
On avait formé sommairement des cours martiales, véritables « abattoirs », aux gares, aux casernes, au parc Monceau, place Clichy, au Temple, square de la tour Saint-Jacques, à l’École polytechnique, au jardin du Luxembourg, le pire.

Mac-Mahon avouait un total de 17 000 morts, mais sans autre preuve. Maxime du Camp, s’appuyant sur les registres des inhumations effectuées dans les cimetières, n’en dénombrait que 6 562. En 1879 Camille Pelletan, après examen critique des mêmes sources, croyait pouvoir porter le chiffre à 16 ou 17 000, compte non tenu d’une dizaine de milliers d’inhumations probables en banlieue : c’est incontestablement une estimation excessive. Puisant toujours aux mêmes sources, l’historien anglais Robert Tombs, après un travail minutieux d’enquête, croit devoir en revenir à une approximation de 6 à 7 000. C’est négliger des inhumations sauvages qu’un obscur statisticien évaluait en 1881 à quelque 3 000 cas. **Le chiffre de 10 000 victimes serait aujourd’hui le plus plausible, et il reste énorme pour l’époque.**

Il est trop facile de faire de Thiers le « nabot sanglant » qu’on a dit : il couvrit, et c’est bien assez.

On avait fait 43 522 prisonniers. 22 727 prisonniers avaient bénéficié d’une ordonnance de non-lieu : on avait décidément arrêté trop d’innocents.

On procéda à vingt-trois exécutions.

Selon une enquête conduite à la fin de l’année 1871 par son conseil municipal républicain modéré, Paris aurait perdu, par la mort ou par la fuite, près de 100 000 travailleurs, le septième de sa population masculine majeure. Il manquait en tout cas 90 000 inscrits sur les listes électorales.

De grands écrivains y cédèrent, la bonne [[George Sand]], trop provinciale désormais pour comprendre Paris, [[Gustave Flaubert]], incapable de comprendre, bien moins enragé qu’on n’a dit… [[Emile Zola]] était partagé : la Commune ne venait-elle pas de compromettre la fragile République ? Déchiré, [[Jules Michelet]], frappé d’apoplexie à Florence à la nouvelle de l’incendie de l’Hôtel de Ville, avait ce cri douloureux : « Quand on s’est appelé la Commune, on n’en détruit pas le vivant symbole. » Le seul [[Victor Hugo]], parisien et peuple dans l’âme, qui n’avait pas approuvé la Commune, sut aussitôt parler au nom des misérables.

## Conclusion
Par une insurrection au fond bien douteuse. Une fois encore, on soulignera son caractère exceptionnel. Oubliant les envolées de _La Guerre civile_, Marx le rappelle non sans rudesse dans une lettre au socialiste néerlandais Domela Nieuwenhuis en 1881 : 
> « Outre qu’elle fut simplement la rébellion d’une ville dans des circonstances exceptionnelles, la majorité de la Commune n’était nullement socialiste et ne pouvait l’être. Avec un tout petit peu de bon sens, elle eût pu cependant obtenir de Versailles un compromis favorable à toute la masse du peuple, ce qui était la seule chose possible. »

On croyait avoir là le modèle d’une République à venir. Urbaine, elle n’était qu’irréalisable dans une France aux deux tiers rurale, qui lors même qu’elle devient lentement républicaine, craint les excès des « partageux » des villes.

La Commune fut-elle le prélude d’une seconde onde révolutionnaire, celle du XXe siècle ? Tous les mouvements socialistes révolutionnaires ultérieurs y ont cherché une espèce de légitimation idéologique et historique. On ne saurait leur dénier ce droit : à la condition que cette recherche en paternité ne conduise pas à remodeler l’événement pour mieux prouver la filiation.

Le traumatisme de l’atroce répression fera que la **République « bourgeoise »** restera longtemps suspecte à ceux qui vont, à partir du dernier tiers du XIXe siècle, constituer la classe ouvrière, prolétaires socialistes, anarchistes, syndicalistes révolutionnaires, plus tard communistes. Il ne sera pas facile de réconcilier _La Marseillaise_ et _L’Internationale_ et Jaurès dut souvent rappeler aux déshérités que la République est aussi « la chose de tous » ; il n’aimait d’ailleurs pas parler de la Commune. On se souviendra aussi que la Commune patriote a servi d’instrument, d’alibi dévoyé contre la République, à un tout autre camp : des communards, non des moindres, se sont retrouvés aux côtés de **Boulanger**, général versaillais, médiocre fauteur de coup d’État, et se laisseront tenter par le nationalisme antisémite de la fin du siècle.

Le peuple de Paris, celui des cités provinciales étaient républicains, profondément, radicalement. Ils réclament, défendent la République : entendons la bonne, la vraie République, démocratique et sociale, décentralisatrice, inséparablement : celle qu’avaient tenté de construire les républicains « socialistes » et « communistes », depuis les années 1830 et 1840 : la République par qui, pour reprendre les mots de Gambetta, « la forme emporte et résout le fond », la « question sociale ». Elle n’est pas un préalable à des réformes sociales à venir : elle les porte déjà en elle, « forme enfin trouvée » qui transcende classes et groupes, réconcilie le peuple avec lui-même, bourgeois nantis et misérables. **Pour le socialisme républicain des années 1830, 1840, 1860, il y avait bien lutte des classes, mais celle-ci prendrait fin non par l’extinction de l’une, mais par la réconciliation de toutes.** Et la République serait européenne, « universelle ». Idéal, utopie qui jette ses derniers feux en 1871 ? **C’est une République de compromis, bien plus terre à terre, qui s’installe en 1879**, et paraît aux déshérités se soucier peu de trouver remède à la question sociale. Il faudra attendre longtemps pour que, comme l’espérait Jaurès, on vît que la forme, démocratique, pourrait être le moyen de résoudre, lentement, imparfaitement, le fond, social.