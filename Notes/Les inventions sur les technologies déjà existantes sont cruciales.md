MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081614
Tags : #Fini
***

# Les inventions sur les technologies déjà existantes sont cruciales
#Alire [[@Small is beautiful_ economics as if people mattered,E. F. Schumacher]]
Les inventions concernant les technologies peu prestigieuses ou qu'on considèrent comme vieilles sont tout aussi importantes que les autres. Une invention géniale étant par exemple l'Oxtrike qui propose un substitut résistant et fiable des pousse pousse. Réinventer les vieilles technologies n'est pas juste de la nostalgie, ce sont des technologies viables et toujours utilisées.

p249 : "J'ai inventé un nouvelle machine à faire des boutonnières pour notre industrie du vêtement. - Camarade, répond le ministre, nous n'avons rien à faire de ta machine."

## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]