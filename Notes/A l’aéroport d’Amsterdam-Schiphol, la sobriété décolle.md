[[Article]]
MOC : [[ECOLOGIE]] [[ECONOMIE]]
Titre : [[A l’aéroport d’Amsterdam-Schiphol, la sobriété décolle]]
Auteur : [[L'Opinion]]
Date : 2022-07-06
Lien : https://www.lopinion.fr/economie/a-laeroport-d-amsterdam-schiphol-la-sobriete-decolle
Fichier : 
***

## Résumé
Lutte contre la pollution sonore et impact sur l’environnement : le nombre de vols sera réduit de 12%.

Il a annoncé qu’il allait plafonner le nombre de vols depuis l’aéroport d’Amsterdam-Schiphol, le troisième plus fréquenté d’Europe, à 440 000 vols par an contre une capacité de 500 000, atteinte avant la pandémie de Covid-19, soit une baisse de 12%. Schiphol est situé dans l’une des régions les plus fréquentées des Pays-Bas « Une zone urbanisée avec beaucoup d’activités » écrit le gouvernement dans un communiqué. « Les résidents locaux … sont gênés par le bruit des avions et s’inquiètent de l’impact sur l’environnement. Nous voulons maintenir cette forte fonction de hub. Dans le même temps, il faut s’attacher à réduire les effets négatifs de l’aviation sur les personnes, l’environnement et la nature ». Cette décision est une première mondiale, et une bonne nouvelle pour le climat.

## Référence

## Liens 
[[Décroissance]]