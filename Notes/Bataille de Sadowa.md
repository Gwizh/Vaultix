MOC : 
Titre : [[Bataille de Sadowa]]
Date : 2023-09-26
***

## Résumé
La guerre austro-prussienne de 1866 opposa l'empire d'[[Autriche]] et ses alliés de la [[Confédération germanique]] au royaume de [[Prusse]] (seulement soutenu par quelques principautés mineures ou ses voisins immédiats, et par le royaume d'[[Italie]]). Prélude à l'unification allemande, elle est parfois appelée la « guerre allemande » (Deutscher Krieg) en Allemagne ; elle est également connue comme la « guerre fratricide allemande » (Deutscher Bruderkrieg), la « guerre d'Unification » (Einigungskrieg), la « guerre germano-allemande » (Deutsch-deutscher Krieg) ou la « guerre de Sept Semaines » (Siebenwöchiger Krieg). Elle est liée à la troisième guerre d'indépendance italienne. 
## Références

## Liens 