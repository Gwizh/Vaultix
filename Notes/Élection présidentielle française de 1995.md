MOC : 
Source : [[Élection présidentielle française de 1995]]
Date : 2023-04-11
***

## Résumé
#### Choix d'un candidat à gauche
On sort de 14 ans de PS de [[François Mitterand]] qui affaibli et pris dans des affaires et des révélations sur son passé. Il choisit de ne pas se représenter, ce qui est une bonne idée vu son état de santé. Plusieurs candidats se succèdent pour la candidature, il y a d'abord [[Laurent Fabius]], [[Michel Rocard]] et [[Jacques Delors]].

Finalement, chacun renonce et il ne reste que Lionel Jospin et Henri Emmanuelli. Une primaire est organisée le 5 février 1995 et Lionel Jospin l'emporte avec 66% des voix.

#### A droite
On sort de l'[[Élection législative de 1993]] qui voit une union de droite se former entre l'[[UDF]] de Chirac et le [[RPR]] de Balladur. La victoire est écrasante et constitue la majorité la plus écrasante de la Cinquième République. Un accord est formé entre Chirac et Balladur, Balladur devient premier ministre tandis que Chirac prépare la présidentielle de 1995.

[[Alain Madelin]] et [[Philippe Séguin]] ont organisé la campagne électorale de [[Jacques Chirac]] et il apparaissait plus dynamique qu'[[Edouard Balladur]]. Or Alain Madelin défend un supra néolibéralisme et Séguin se dit du Gaullisme social.

Les résultats sont dingues : 23% pour [[Lionel Jospin]], 21 pour [[Jacques Chirac]], 19 pour Balladur et 15% pour [[Jean-Marie Le Pen]]. Au global, la droite a emportée 58% des voix et Chirac avait peu de raison de douter de sa victoire. 

Voir le récap du débat d'entre deux tours : [[Débat présidentiel de 1995]]

Finalement, Chirac gagne avec 52.63% des voix.

Tu veux la suite ?? Va là : [[Gouvernement Alain Juppé (1)]]

## Référence

## Liens 