MOC : 
Source : [[MNLA]]
Date : 2023-07-02
***

## Résumé
Le Mouvement national de libération de l'Azawad (en touareg : ⵜⴰⵏⴾⵔⴰ ⵏ ⵜⵓⵎⴰⵙⵜ ⵉ ⴰⵙⵍⴰⵍⵓ ⵏ ⴰⵣⴰⵓⴷ) ou MNLA est une organisation politique et militaire majoritairement touarègue, active au nord du Mali. Son objectif est l'indépendance du territoire de l'Azawad6. Créé en 2011, c'est l'un des principaux groupes armés impliqués dans la guerre du Mali. 

### Ressources
https://www.youtube.com/watch?v=USMkzKlGY0U

## Référence

## Liens 