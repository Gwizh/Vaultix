MOC : 
Source : [[Révolte des vignerons de 1907]]
Date : 2023-05-03
***

## Résumé
La révolte des vignerons du Languedoc en 1907 désigne un vaste mouvement de manifestations survenu en 1907, dans le Languedoc et dans le Roussillon, réprimé par le cabinet Clemenceau. Fruit d'une grave crise viticole survenue au début du xxe siècle, ce mouvement, aussi appelé « révolte des gueux » du Midi, a été marqué par la fraternisation du 17e régiment d'infanterie de ligne avec les manifestants, à Béziers.

## Référence

## Liens 