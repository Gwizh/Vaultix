MOC : 
Source : [[Révolution des Œillets]]
Date : 2023-06-09
***

## Résumé
La révolution des Œillets (Revolução dos Cravos en portugais), également surnommée le 25 avril (25 de Abril en portugais), est le nom donné aux événements d'avril 1974 qui ont entraîné la chute de la dictature salazariste qui dominait le [[Portugal]] depuis 1933. Elle doit son nom à l'œillet rouge que les conjurés portaient à leur boutonnière et dans le canon de leur fusil en signe de ralliement.

Ce que l'on nomme « [[Révolution]] » a commencé par un coup d'État organisé par des militaires qui se sont progressivement radicalisés par rejet des guerres coloniales menées par le Portugal. Ce coup d'État, massivement soutenu par le peuple portugais, a débouché sur une révolution qui a duré deux ans, marquée par de profondes divisions sur la façon de refonder le Portugal, mais qui, finalement, a profondément changé le visage de celui-ci.

La révolution des Œillets a la particularité de voir des militaires, porteurs d'un projet démocratique (mise en place d'un gouvernement civil, organisation d'élections libres et décolonisation), renverser un régime, sans pour autant instaurer un régime autoritaire.

Cet événement est le début de la démocratisation du Sud de l'Europe, celui-ci étant suivi par la chute des dictatures espagnole et grecque.

## Référence

## Liens 