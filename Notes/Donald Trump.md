MOC : 
Source : [[Donald Trump]]
Date : 2023-05-05
***

## Résumé
Donald Trump, né le 14 juin 1946 à New York, est un homme d'affaires milliardaire, animateur de télévision et homme d'État américain. Membre du [[Parti républicain]], il est le 45e président des [[États-Unis]], en fonction du 20 janvier 2017 au 20 janvier 2021.

![[Ce que révèle le rachat de Twitter - Quand la liberté d’expression passe à droite#^ebc06e]]


## Référence

## Liens 