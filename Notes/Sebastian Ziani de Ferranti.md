MOC : 
Source : [[Sebastian Ziani de Ferranti]]
Date : 2023-04-20
***
#Adam 
## Résumé



#### The Deptford Experience
![[Centrale électrique de Deptford]]
Il n'avait pas la technologie des cables pour pouvoir transporter 10 000V, il a du faire le design lui-même. Personne au monde ne faisait ça, le maximum était d'envion 2 400V. Il a demandé l'aide de Sir William Thomson. Latimer, Clark & Muirhead. 150 000£ pour 7000 _joints_ de cable.

MDR un homme branche une nouvelle série de transformateurs et crée un arc électrique qui met le feu à la substation de la grosvenor gallery. Cela cause des couts de 20 000£ --> 2 mois sans lumière le temps de fixer tout ça. 

C'était plutôt une bonne nouvelle finalement car cela lui a permis de découvrir l'[[Effet Ferranti]], il fallait en effet ajouter des condensateurs et des fusibles. Heaviside 

Son entreprise vendait des machines de très haut niveau pendant les premières années (entre 1885-1889). 55 000£ de revenue en 1889. The Experimental Department. 
Créer des petites companies à l'étranger comme en France. 

En 1891, alors qu'il n'était plus avec [[LESCo]], il ne vendait des machine qu'à 3 sites sur les 65 de la Grande Bretagne.

Voir [[Secteur de l'électricité en Grande Bretagne à la fin du 19e siècle]]

En 1890, il fonde la S.Z. de Ferranti Limited avec £100 000 de capital dont 8 000 actions ordinaires de 10£ et 2000 * 10£ en privilégiée. Ferranti a pris 75% des capitaux propres et plus de 50% des actions privilégiées. Cela en a fait de facto une entreprise privée, une forme d'entreprise non reconnue en Grande Bretagne jusqu'en 1907. Gros problème des anglais de l'époque : l'envie de tout financer soi-même qui empêche la levée d'un gros capital.

En 1896, il part à Hollinwood pour avoir un espace plus grand. Il a pu construire un pattern shop, une fonderie et de quoi assembler les plus gros alternateurs. Il avait beaucoup de problème avec les syndicats car il ne voulait aucune interférence des travailleurs dans la gestion de l'entreprise. Il avait en particulier des problèmes avec l'[[Amalgamated Society of Engineers]]. 
Il était contre le [[Notes/Socialisme]] qu'il voyait comme ce qui pourrait ruiner le pays. Il engagea des polices privées pour matter les révoltes ==OMG Jipèp==

## Référence

## Liens 