[[Vocabulaire]]
## [[Benjouin]]

> Le **benjoin**, aussi appelé « baume du **benjoin** » est une résine qui provient de diverses plantes du genre « styrax ». Le **benjoin** est originaire d'Indochine, mais on le localise également en Grèce, au Laos ou au proche Orient. Le **benjoin** se présente sous forme de morceaux solides mais fragiles.