MOC : 
Source : [[Auguste Vaillant]]
Date : 2023-07-07
***

## Résumé
Auguste Vaillant est un anarchiste et criminel français, né le 27 décembre 1861 à Mézières (Ardennes) et guillotiné le 5 février 1894 à Paris.

Très vite gagné aux idées socialistes et libertaires, il émigre en Argentine. De retour en France, le 9 décembre 1893, il jette une bombe dans la Chambre des députés, blessant plusieurs personnes. Après cet attentat, la Chambre a adopté une série de lois anti-anarchistes connues sous l'appellation de « [[Lois scélérates]] ». 

## Référence

## Liens 
[[Anarchie]]