MOC : 
Source : [[Parti communiste français]]
Date : 2023-04-26
***

## Résumé
Pour les membres du PCF, ils sont communistes mais avant tout marxiste. Lors de la [[Troisième Internationale]], la [[SFIO]] choisit de suivre [[Vladimir Lénine]] et le [[Communisme]] pour suivre à la lettre ces doctrines. Ce groupe deviendra le SFIC puis le [[Parti communiste français]]. L'autre bout minoritaire du SFIO refondra la SFIO qui deviendra l'ancêtre du [[Parti Socialiste]]. L'acte de naissance du PCF est donc l'adhésion à Lénine. 

PCF usait de purges réguliaires, respectant la doctrine de [[Vladimir Lénine]]. Ils passent de 109 000 membres en 1921 à 28 000 en 1933. Il redeviendra un parti de masse après la [[Seconde Guerre mondiale]] devenant même le premier parti de France. Ils mettent alors en place avec le [[MRP]] et la [[SFIO]]
la [[Sécurité sociale]] et les [[Retraite]]. C'est une rupture avec le marxisme léninisme car ils renoncent à la révolution, gagnent des élections et mettent en place des réformes. Ce renoncement aux doctrines originels viennent alors de [[Joseph Staline]] et de l'[[URSS]], paradoxal ?

À la mort de Staline en 1953, un rapport est écrit par [[Nikita Khrouchtchev]] pour dénoncer le régime stalinien. Ce rapport sensé être secret est dévoilé au grand jour en 1956. Mais le PCF ne renie pas encore Staline et attend encore 20 ans. C'est seulement pour le [[Printemps de Prague]] que le PCF condamne l'URSS. 

En 1976, [[Eurocommunisme]] mais revient quand il voit que ça ne marche pas électoralement. Il soutient même la [[Guerre d'Afghanistan (1979-1989)]] de l'URSS et [[Georges Marchais]] dit que les pays communistes ont un bilan plutôt positif. Il devient alors plus blochevique mais pas plus marxiste.

En 1994, il abandonne finalement le centralisme démocratique. [[Robert Hue]] ne se prétend plus léniniste. 

[[PCOF]] full stalinien.

[[PRCF]] stalinien, anti UE et nationaliste. Très [[Bonapartisme|bonarpartiste]]. 

Aujourd'hui, le PCF ne parle plus de [[Lutte des classes]], de prolétariat, de bourgeoisie, etc... Le parti n'est plus marxiste. Il ne parle plus d'abolir le capitalisme, de communisme, de socialisme ou la collectivisation des moyens de production. Programme du PCF très proche de celui de la [[France Insoumise]]. 

## Référence

## Liens 
[[Financement du PCF en 2023]]