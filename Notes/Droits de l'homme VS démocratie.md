MOC : 
Titre : [[Droits de l'homme VS démocratie]]
Date : 2023-09-21
***

## Résumé
[[Tzitzimitl]]
#### Introduction
Droits de l'Homme développés par les [[Libéralisme|libéraux]] et le siècle des Lumières. Droit naturel vs Droit positif
École de Salamanque en réinterprétant [[Thomas d'Aquin]] et [[Aristote]].
Cette différence fondamentale entre le droit naturel et le droit positif s'incarne en France mieux qu'ailleurs entre le droit naturel de [[Voltaire]] et le [[Contrat social]] [[Républicanisme|républicain]] de [[Jean-Jacques Rousseau]].

#### La loi et la loi
"Les sciences sont descriptives et les lois sont prescriptives."
Les défenseurs du droit naturel cherchent des lois dans la Nature et ses propres "lois". Au début lié à l'existence de Dieu qui laisserait des lois. 
Si on pense que les lois de la nature ont un but alors forcément on les interprète comme les lois de la société humaine.
On ne peut pas se mettre d'accord par la science. Aucun système politique ne sera scientifiquement parfait. Un projet de société est un projet mouvant, empirique.
Tous les premiers libéraux étaient déistes, ils ont remplacé la monarchie de droit divin par une justification par la science et la foi de leur système.

Les droits de l'Homme sont séparées des autres lois, elles se veulent universelles non seulement au niveau national mais aussi international. Ces droits sont en effet censés avoir existés de tout temps et pour toute l'humanité. Ce ne serait pas une décision humaine.

C'était le cas de la [[Déclaration des Droits de l'Homme et du citoyen]] de 1789. Les droits sont "sacrés, inaliénables et naturels"
"EN conséquence, l'assemblée nationale reconnait et déclare en présence et sous les auspices de l'Être suprême les droits suivants de l'homme et du citoyen."

#### Les deux faces des droits de l'Homme
L'argument des droits de l'homma a été décisif pour passer de l'ancien régime aux révolutions libérales. 
Les républicains sont censés se revendiquer du droit positif et du [[Contractualisme]]. Mais au début, les républicains étaient ambigüs par rapport au droit naturel. Mais ils se sont alliés aux libéraux pour voler le droit divin aux réactionnaires. 
Les républicains étaient plus à gauche que les libéraux. Ils étaient plus présent pendant la [[Révolution Française]] que pendant les autres révolutions et ont pu ajouter des points qui font que la ddhc est très différentes des autres déclarations des autres révolutions. 
En 1793, les républicains les plus radicaux avaient le pouvoir, ils ont pu créé une déclaration plus égalitariste.
Puis en 1795 et en 1848, on voit le retour au conservatisme des libéraux.
En 1949, l[[ONU]] a créé sa propre déclaration mais en ajoutant des droits nouveaux, pleins de droits. Alors ? Sont-ce alors vraiment des lois naturelles ? Les libéraux ont fustigés face à cette réappropriation du concept et ont créés le concept de [[Faux droit]]. Par contre l'ONU cette fois-ci ne se réclame pas de Dieu mais d'un engagement politique subjectif. C'est un peu mieux.

Les deux droits couramment admis sont la liberté et la propriété. Ce deuxième concept est très subjectif par rapport à notre culture occidentale. Il ne signifie rien pour beaucoup d'autres gens. Et la liberté peut être définie par rapport à la propriété de notre corps et ça ne veut donc pas dire gd chose de plus.
#### Le rapport de force
L'opposition à tout ça est le contractualisme. Le fait d'admettre qu'on ne trouvera pas de solution objective pour tout le monde t qu'il faut un compromis. C'set aussi l'un des buts de la démocratie.

En démocratie, il y a deux types de débats. Le premier vise à se mettre d'accord sur le constat, chose qu'on peut faire scientifiquement. Puis il y a les propositions d'un idéal et d'un moyen de l'atteindre. Il faut alors trouver un compromis et avancer pour que chacun soit d'accord.

Les gens qui pensent que la politique est une science pensent aussi souvent que la démocratie n'est pas un système tout à fait convenable. En effet, il n'est pas le bon système pour charcher la vérité. L'[[Épistémologie]] propose une méthodologie qui est très éloignée de l'idéal démocratique. Le problème est qu'en matière de faits, une minorité peut tout à fait avoir raison par rapport à la majorité. Les gens qui voient la politique comme une science sont souvent contre la démocratie et ont toujours voulu réserver le pouvoir à une élite de personne censée savoir mieux que tout le monde ce qu'il faut vouloir. 

Les libéraux qui ont écrits les droits de l'homme ont ensuite réservé le pouvoir à une élite choisie par sa richesse, signe de compétence politique à l'époque apparemment.

> " Les citoyens qui se nomment des représentants renoncent et doivent renoncer à faire eux-mêmes la loi ; ils n'ont pas de volonté particulière à imposer. 
> S'ils doivent des volontés, la France ne serait plus cet Etat représentatif, ce serait un état démocratique.
> Le peuple, je le répète, dans un pays qui n'est pas une démocratie (et la France ne saurait l'être), le peuple ne peut parler, ne peut agir que par ses représentants.
> Tous les habitants d'un pays doivent y jouir des droits de citoyen passif : tous ont droit à la protection de leur persone, de leur propriété, de leur liberté, etc... mais tous n'ont pas droit à prendre une part active dans la formation des pouvoirs publics ; tous ne sont pas citoyens actifs
> Les femmes, du moins dans l'état actuel, les enfants, les étrangers, ceux encore qui ne contribuent en rien à soutenir l'établissement public, ne doivent pas influencer activemetn sur la choser ppublic.
> Tous peuvent jouir des avantages de la société ; mais ceux-là qui contribuent à l'établissement public sont comme les vrais actionnaires de la grande entreprise sociale. Eux seuls sont les véritables citoyens actifs, les véritables membres de l'association
> Emmanuel-Joseph Sieyès. Discous à l'AN en 1789"

Puis point super important sur les experts !!!!


## Références

## Liens 