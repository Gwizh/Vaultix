MOC : 
Source : [[Sébastien Faure]]
Date : 2023-07-07
***

## Résumé
Sébastien Faure, né le 6 janvier 1858 à Saint-Étienne et mort le 14 juillet 1942 à Royan, est un agent d’assurances puis conférencier professionnel.

Propagandiste anarchiste français de renommée internationale, franc-maçon, il est aussi pédagogue libertaire à l'initiative de La Ruche et est l'initiateur de l'Encyclopédie anarchiste en 1925.

Sébastien Faure ne fut pas à proprement parler un théoricien, mais surtout par l’écrit et par la parole, un vulgarisateur. 

## Référence

## Liens 
[[Anarchie]]