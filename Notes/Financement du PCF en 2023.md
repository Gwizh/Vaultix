MOC : 
Source : [[Financement du PCF en 2023]]
Date : 2023-04-26
***

## Résumé
Le PCF a publié son rapport financier, présenté au 39ème congrès. On y trouve des éléments qui mettent en doute la renaissance annoncée du parti centenaire, un thread 🧵:
Sur la chute du nombre de cotisants :
➡️de 42056 en 2018 à 37737 en 2022 soit une perte de -4319 environ -10%
➡️Le rapport note ainsi « 81 de nos 96 fédérations ont perdu 5 400 cotisants et cotisantes depuis 2018»
➡️Le montant total des cotisations baisse continuellement et la baisse s’accélère !
-154 453 depuis 2018. La + grosse baisse est en 2022 : -352 855 € par rapport à 2021, plus qu’en 2020 qui aurait dû être pourtant une année exceptionnelle basse du fait du confinement (-278 120)
Sur les sources de financement du Parti :
➡️La 1ère source de financement du PCF est son patrimoine (loyers) qui représente 25% des ressources
➡️La 2ème source de financement du PCF sont ses élus, 22%, qui dépassent désormais les cotisations (21%).
Le PCF devient ainsi un parti dépendant de son patrimoine et de ses élus, une réalité qui renforce sa nécessité de passer des accords de circonstances pour la préservation de son déploiement institutionnel...
Le PCF en grande difficulté financière :
➡️Il est passé nationalement de 198 salariés à 165 depuis 2018 (-33 soit -17%), dont 3 en moins au siège.
➡️Certaines fédérations accusent le coup :
«Nos recettes continuent de s’effriter. De nombreuses fédérations sont en difficulté,au point pour certaines de ne pas avoir pu remplir les objectifs de souscription de la présidentielle et au point de ne pas reverser le 1/3 de la cotisation au Conseil national et aux sections»
➡️Les nouveaux adhérents fièrement mis en avant par la direction du PCF et son équipe de com ne semblent pas arranger les choses.
Le rapport note ainsi :
« Pour les derniers exercices l’érosion se poursuit, et l’arrivée de nouveaux adhérents, notamment au moment de la campagne de la présidentielle, ne se traduit pas aussi en termes de nouveaux cotisants »
Le bilan est globalement négatif. Pas de risque de ruine financière pour le PCF qui peut compter pour de nombreuses années sur son patrimoine et son implantation historique mais la trajectoire financière en dit long sur son état de santé réel !

## Référence

## Liens 