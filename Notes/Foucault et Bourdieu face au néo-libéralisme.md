MOC : 
Source : [[Foucault et Bourdieu face au néo-libéralisme]]
Date : 2023-04-14
***

## Résumé
[[Pierre Bourdieu]] et [[Michel Foucault - Que sais-je]]

Foucault a listé les formes de gouvernement et il dit que le néolibéralisme est très original dans la gouvernance car c'est avant tout la mise en concurrence des individus. Pour lui, le [[Néolibéralisme]] n'est pas que économique, c'est aussi une forme de gouvernement. Pour Bourdieu, c'est un renouvellement des formes de la domination. Il regarde comment les élites se sont appropriés cette théorie. Il l'aborde de manière plus idéologique. Ils voient tous deux que c'est un changement profond de la société mais aussi de l'individu. Pour les deux, c'était quelque chose de crucial qui s'est passé dans notre histoire, il voyait que ça n'allait pas être qq chose de fragile, de temporaire. Ils n'opposent pas non plus marché et Etat, pour les deux c'est une action gouvernementale.

Il y a une chose de curieux avec ça. On accuse Foucault d'avoir adhéré au néolibéralisme simplement parce qu'il s'y était intéressé. Et Bourdieu on lui dit qu'il avait abordé le néolib qu'avec un angle idéologique. Mais Bourdieu pense que les sociologues doivent agir comme des météorologues pour prévenir des dangers de la société. [[La Misère du Monde]]

Selon Foucault, le néolibéralisme se voulait comme une interdiction de l'etat d'agir sur l'économie. En Allemagne, c'était dans l'après guerre pour éviter l'excroissance de l'Etat que fut le [[Nazisme]]

## Référence

## Liens 