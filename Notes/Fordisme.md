MOC : 
Source : [[Fordisme]]
Date : 2023-06-18
***

## Résumé
Le fordisme, est — au sens premier du terme — un modèle d'organisation et de développement d'entreprise développé et mis en œuvre en 1908 par Henry Ford (1863-1947) fondateur de l'entreprise qui porte son nom, à l'occasion de la production d'un nouveau modèle, la Ford T.

Ce modèle accorde une large place à la mise en œuvre des nouveaux principes d'organisation du travail (organisation scientifique du travail, ou OST) instaurés par le taylorisme (qui, quant à lui, se base avant tout sur la qualité du produit) en y ajoutant d'autres principes comme notamment le travail des ouvriers sur convoyeur (que William C. Klann, un employé de Ford, de retour d'une visite à un abattoir, a découvert lors de la visite d'un semblable dispositif déjà à l'œuvre aux abattoirs de Chicago1).


Henry Ford et son modèle "Ford T". Ce véhicule, l'un des premiers, est massivement produit grâce au principe du fordisme.
Leurs salaires peuvent être indexés sur cette progression, et générer une augmentation bienvenue du pouvoir d'achat. Comme le perçoit bien Henry Ford (qui voulait que ses ouvriers fussent bien payés, pour leur permettre d'acheter les voitures qu'ils avaient eux-mêmes produites), relayé plus tard par les keynésiens : « le fordisme est le terme par lequel on désigne l'ensemble des procédures (explicites ou implicites) par lesquelles les salaires se sont progressivement indexés sur les gains de productivité. Augmenter régulièrement les salaires au rythme des gains de productivité permet d'assurer que les débouchés offerts aux entreprises croîtront également au même rythme et permettront donc d'éviter la surproduction ».

## Référence
[[Capitalisme]]

## Liens 