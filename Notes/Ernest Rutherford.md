MOC : 
Source : [[Ernest Rutherford]]
Date : 2023-04-24
***

## Résumé
Ernest Rutherford (30 août 1871 à Brightwater, Nouvelle-Zélande - 19 octobre 1937 à Cambridge, Angleterre) est un physicien et chimiste néo-zélandais, considéré comme le père de la physique nucléaire. Il découvre les rayonnements alpha, les rayonnements bêta ; il découvre aussi que la radioactivité s'accompagne d'une désintégration des éléments chimiques, ce qui lui vaut le prix Nobel de chimie en 1908. C'est encore lui qui met en évidence l'existence d'un noyau atomique dans lequel sont réunies toute la charge positive et presque toute la masse de l'atome.

## Référence

## Liens 