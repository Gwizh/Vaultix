MOC : 
Source : [[Sophie Binet]]
Date : 2023-05-02
***

## Résumé
Le 31 mars 2023, en pleine mobilisation contre la réforme des retraites, Sophie Binet est élue secrétaire générale de la [[CGT]]. Elle succède à [[Philippe Martinez]] avec 82 % des voix de 32 fédérations. C’est la première femme à être élue à la tête de ce syndicat, parmi les plus influents.

Née en 1982, Sophie Binet commence à militer au syndicat étudiant l’[[Unef]]. Elle se fait connaître en 2006 lors des manifestations contre le Contrat Premier Embauche (CPE). En 2016, elle lance la pétition “Loi travail : non merci !” avec [[Caroline de Haas]] et [[Elliot Lepers]], qui recueille plus d’un million de signatures et influence l’ampleur de la mobilisation.

Élue en 2013 au bureau confédéral de la CGT, elle y travaille, entre autre sur les questions d’égalité femme-homme.

Son élection, historique, intervient alors que la CGT traverse une grave crise en interne. Espérons qu’il s’agit bien d’une avancée pour la représentation des femmes dans la sphère professionnelle et des difficultés auxquelles elles sont confrontées, et non pas un nouvel exemple de la fameuse “falaise de verre”.

## Référence

## Liens 
[[Syndicalisme]]
