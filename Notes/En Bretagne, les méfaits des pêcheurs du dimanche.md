MOC : 
Source : [[En Bretagne, les méfaits des pêcheurs du dimanche]]
Date : 2023-04-23
***

## Résumé
Les amateurs de coquillage munis de râteaux ne font pas bon ménage avec les zostères. Ces plantes qui s’étalent sur les plages d’[[Ille-et-Vilaine]] sont pourtant des refuges pour la faune. Mais une brigade veille au grain.

« Bonjour ! Est-ce que vous savez que la pêche à pied est interdite dans les herbiers ? »

Devant leurs mines étonnées, elle pointe du doigts de longs spaghettis verts formant un petit îlot sur le sable : « Ce sont des herbiers de [[Zostère]], des plantes sous-marines qui constituent un habitat pour de nombreuses espèces. Si vous grattez dedans avec vos râteaux, vous risquez de déraciner les plants et de détruire ce nid de biodiversité sous-marine », explique Charlotte Geslain, chargée de mission littoral à Cœur Émeraude, l’association qui pilote le projet du futur parc naturel régional Vallée de la Rance-Côte d’Emeraude.

#### Les zostères limitent l’érosion du littoral

## Référence

## Liens 
[[Bretagne]]