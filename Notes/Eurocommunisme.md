MOC : 
Source : [[Eurocommunisme]]
Date : 2023-05-14
***

## Résumé
L'eurocommunisme est une réforme politique adoptée de concert par des partis communistes d'Europe de l'Ouest durant la seconde moitié des années 1970, en opposition au marxisme-léninisme de l'Union soviétique. L'eurocommunisme a été mis de l'avant principalement par le [[Parti communiste d'Espagne]] (PCE), le [[Parti communiste italien]] (PCI) et le [[Parti communiste français]] (PCF) et visait à conjuguer la démocratie et le pluralisme et la transformation socialiste. Les partis ayant adopté cette orientation ont souhaité incarner une « troisième voie » entre le marxisme-léninisme et la tradition social-démocrate dans le contexte politique de l'Occident.

Ce changement d'orientation se déroule en pleine Détente et en parallèle avec la collaboration grandissante entre certains partis communistes européens et leurs interlocuteurs nationaux : du programme commun en France au compromis historique en Italie en passant par le pacte pour la liberté en Espagne.

Plus de libertés publiques et plus de mention de la dictature du prolétariat.

## Référence

## Liens 
[[Communisme]]