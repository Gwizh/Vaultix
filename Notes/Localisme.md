MOC : 
Source : [[Localisme]]
Date : 2023-04-07
***

## Résumé
Idée selon laquelle les problèmes contemporains proviennent surtout de l'ampleur du monde et que seules des actions locales peuvent être les solutions de ceux-ci.

C'est une idée qu'on retrouve à gauche mais qui va plus loin.  

## Référence
[[Inventing the Future. Postcapitalism and a World without Work]] p.43

## Liens 