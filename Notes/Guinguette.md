[[Note Permanente]]
MOC : [[CULTURE]]
Source : [[Guinguette]]
Date : 2022-07-06
***

## Résumé
La guinguette est un petit café ou restaurant très populaire à Paris pendant le 19e jusqu'aux années 1950. Ces cafés fuyaient le centre-ville de Paris pour aller au delà des murs, là où les taxes étaient moins cher [[Octroi]]. Ils étaient généralement le long de la Seine et on y jouait de la musique pour danser. Il tire son nom d'un type de vin Parisien qu'on y vendait. Il faisait partie du quotidien de la vie de l'époque et on eu le droit à des coups de plumes de la part de [[Gustave Flaubert]], [[Emile Zola]] et [[Guy de Maupassant]] et des coups de pinceau d'[[Auguste Renoir]]. Ils n'existent plus à Paris aujourd'hui du fait de la pollution et l'urbanisation mais existent toujours le long d'autres rivières comme la Loire.

## Référence
[[Le Déjeuner des canotiers]]

## Liens 