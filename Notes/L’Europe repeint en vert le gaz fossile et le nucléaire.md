[[Article]]
MOC : [[POLITIQUE]] [[ECOLOGIE]]
Titre : [[L’Europe repeint en vert le gaz fossile et le nucléaire]]
Auteur : [[Mediapart]]
Date : 2022-07-07
Lien : https://www.mediapart.fr/journal/international/060722/l-europe-repeint-en-vert-le-gaz-fossile-et-le-nucleaire
Fichier : 
***

## Résumé
Le Parlement européen a voté mercredi 6 juillet pour que certains investissements dans le gaz et le nucléaire soient classés comme verts. La France a été à la manœuvre de cette vaste opération de greenwashing qui pourrait, en l’état, générer de nouveaux flux d’argent vers la Russie.

## Référence
[[Energie]]

## Liens 