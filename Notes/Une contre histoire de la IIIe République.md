MOC : 
Titre : Une contre histoire de la IIIe République
Date : 2023-09-22
***

## Résumé
#### Introduction
Vouloir réfléchir la République par rapport à ce qu'on en dit maintenant, les clichés qui reviennent.

#### 1. "La plus longue des Républiques"
République de 70 ans qui a su durer plus longtemps que tous les autres régimes du XIXe siècle. De bcp même car aucun autre n'a dépassé la limite des 15ans.

Donc, contrairement à ce qu'on aime dire, la République n'est pas chaotique et vouée à l'instabilité. En effet, ce sont les autres qui ont échoués malgré leur prétention d'être plus solides que l'"anarchie républicaine". Paradoxe donc : ce n'est pas la monarchie ou bien le Bonapartisme|bonapartisme qui est parvenu à vaincre les effets de la Révolution et en particulier le recours à la violence politique, mais la République.
##### Dépasser les bornes : la chronologie en question
C'est facile de dire que c'est la plus longue mais plus dur de dire quand elle a commencée. Bien que la proclamation par Léon Gambetta date de 1870, la constitution arrive en 1875. De plus, cette république commence directement avec une défaite en Élections législatives françaises de 1871|1871 avec une majorité monarchique.

La question de la Commune de Paris est centrale car elle divise les républicains et a sûrement expliqué la défaite en 1871. Les répuiblicains dénoncent souvent à la fois al violence des versaillais et des communards.

L'expérience de la prise de pouvoir était très différentes des autres révolutions. Ici, le pouvoir a été battu par la guerre et les républicains l'ont - de leurs propres mots - pris par terre.

"Le pouvoir gisait à terre" selon une expression de Jules Ferry, ajoutant que les hommes de la Défense nationale n'ont "jamais eu la prétention d'avoir renversé l'Empire""

Idée centrale de la IIIe République : le redressement national avant la république. Retrouver l'honneur de la nation. Point de vue militaire et patriotique qui reste une caractéristique morale jusqu'à la Première Guerre mondiale|première guerre mondiale. L'armée permanente ne sera pas exemple jamais supprimée malgré les revendications républicaines.

Ils ne reconnaissent pas le 4 septembre car "leurs conceptions positivistes nourrissent leur méfiance à l'égard des dogmes et de toute mystique." Ils refusent la révolution comme un début de la république. Il n'accepte que la légitimité politique.

##### La séduction de l'inachèvement
"Proclamer l'inachèvement des idéaux copmme une sorte de valeur et placer cette valeur au centre du réime, c'est une manière de relancer à chaque génération l'obligaiton de poursuivre la tâche ; et c'est probablement par là que la IIIe Rep peut encore fasciner aujourd'hui." Différent de la 5eme car Charles de Gaulle était pragmatique, fondait la république sur son systeme, sa solution législative.

En 1875, la constitution n'apporte pas grand chose. Elle se vend comme provisoire et ne se veut pas absolutiste dans sa ratification.

"Les votes de 1875 font de la IIIe repun régime bigame, qui relève à la fois de la monarchie et de la republique. Du point de vue républicain notamment, la "Constitution" de 1875 renie le programme qui avait été défendu pendant tout le xix° siècle par le « parti » républicain, et que défendent encore en 1875 un Louis Blanc ou un Edgar Quinet, notamment ; un programme qui comprenait la suppression de la seconde chambre, la suppression de la présidence de la République et la suppression des administrations d'autorité comme les préfets ou le Conseil d’État."

Les idées de réforme institutionnelle n'ont ainsi jamais disparu, ni à droite ni à gauche. Elles ressurgissent en force dans les années 1920 pour certains groupes, et dans la première moitié des années 1930 pour la majorité des courants. Un certain nombre d'hommes de gauche, jeunes radicaux ou intellectuels non conformistes, participent à la nébuleuse de la « réforme de l'État », bien que celle-ci penche nettement à droite autour des années 1932-1934. Les plans pour une nouvelle République - la « vraie » République - jaillissent enfin de partout pendant la période de la Résistance. De ce point de vue, l'ultime « naissance » de la III° République, comme République républicaine, comme République sociale où la marche vers l'égalité apporte une substance au cadre formel des institutions, paraît devoir être située entre 1944 et 1946, entre le programme du Conseil national de la Résistance et les espoirs placés dans la IVe République.

D'autres dates, plus politiques et plus électorales, comme celle de 1877 ou comme la victoire écrasante des républicains en 1881, marquent-elles la vraie « naissance » de la « plus longue des Républiques » ? La « République des républicains » à partir de 1877-1879-1881 vient-elle enfin fonder une République qui, à défaut de se reconnaître dans son dispositif constitutionnel, devient une machine à gagner les élections ? Rien n'est moins sûr. Car, ces victoires électorales, même les plus écrasantes, n’aboutissent jamais à la formation d’une majorité politique digne de ce nom, c’est-à-dire une majorité capable de s'inscrire dans la durée, capable de soutenir un gouvernement et d'offrir, par là, une sorte de fondation au régime ; ni en Élections législatives françaises de 1877|1877, ni en Élections législatives françaises de 1881|1881, surtout pas en 1885, pas non plus en 1902, en 1924 ou en 1936. La conquête républicaine, accomplie dans le pays et dans les urnes, ne se traduit pas dans les faits de la politique parlementaire.

##### De quoi la durée est-elle le nom ? Les divisions de l'historiographie
Face à ces problèmes de chronologie, la question se pose dès lors de savoir
quelle historiographie a le mieux rendu compte du problème posé par la
longévité de la on République, c'est-à-dire du sens qu'on peut lui attribuer.

Une première forme d'historiographie, principalement d’origine politique, a d'abord développé la thématique de l’idée républicaine [Nicolet,1982], puis de la « culture » républicaine [Agulhon, 1979, 1989, 2001 ; Berstein, 1999]. Par elle, la République aurait conquis le pays jusque dans ses profondeurs, en parvenant à surmonter (mais non à annuler) les clivages sociaux ou régionaux. Elle aurait finalement transformé la société française en
lui donnant un cadre, des valeurs, des symboles, une identité et, comme on le dirait aujourd’hui, un sens du « vivre ensemble ». La durée du régime est conçue, de ce point de vue, comme l'indication d’une « réussite » générale du régime républicain. Cette réussite est à la fois d'ordre politique (la victoire des républicains dans les urnes), culturel (la diffusion des valeurs de liberté et d'égalité) et social (la victoire des fameuses nouvelles couches sociales annoncée par Gambetta). Un régime qui réussit à surmonter la crise du boulangisme, à éliminer ou à absorber les anciennes oppositions, monarchiste ou bonapartiste, à surmonter la crise liée à l'affaire Dreyfus, à y trouver même la source d’un renouveau, un régime qui conquiert ainsi le privilège de la durée fait donc signe à la réussite d’une politisation des masses. La réussite politique du régime se trouve en accord avec des formes sociales qu'il a lui-même contribué à faire émerger et à identifier, comme le montrent la longue domination du Parti radical et son discours sur les classes moyennes [Berstein, 1980-1982]. La durée du régime apparaît alors comme l'attestation d'un
ancrage réussi des idées républicaines dans le pays tout entier, jusqu'au cœur
du monde paysan. Elle continue d'être attestée aujourd'hui par la force des
effets de mémoire et de miroir qui nourrissent une certaine nostalgie républicaine, entre le cliché de l'« école de Jules Ferry » et celui du service militaire. Elle consacre la réussite de la pratique d'une citoyenneté qui a substitué le bulletin de vote à la violence. Le « modèle républicain »: Berstein et Rudelle, fondé sur un triple consensus politique, soc ial et mage renvoie à la force des idéaux politiques nourris par la réfénrence centrale à la Révolution, au
soutien des classes moyennes et au succès d'une méritocratie qui a emprunté la voie de « l'école de la République ».

La seconde tendance historiographique autour des travaux de Gérard Noiriel [Noiriel, 1988], a largement contesté ce premier point de vue. Pour elle, le critère de la durée s'apparente plutôt à un een Si la III République s'est installée dans la durée, elle le doit d'abord à toute une série de compromis et de renoncements qui l'ont considérablement éloignée de son
programme antérieur. La République devient rapidement conservatrice, comme le montrent son attitude à l'égard du mouvement ouvrier ou, d’une manière encore plus nette, son incapacité à modifier les Structures de l’État administratif. C'est elle qui conforte les « serviteurs de l’État » dans un type de comportement ultra-légaliste, où le respect de l’obéissance prime le recours aux Valeurs républicaines [Baruch, 1997 ; Baruch et Duclert, 2000]. Le fait de procéder à des épurations, c'est-à-dire à un simple changement des hommes, comme elle le fait pour le corps préfectoral, la magistrature et le Conseil
d’État, à la fin des années 1870 et au début des années 1880, est bien une façon
d'avouer que la République ne cherche pas à modifier les règles de fonctionnement. Elle renonce au programme de l'élection des fonctionnaires, de la suppression des préfets ou de la suppression de l’armée permanente. Au fond, ce qui rend compte de la durée de la « plus longue des Républiques », ce n'est pas la victoire « culturelle » du régime politique, mais la force maintenue de l'État. La Chronologie de l'État, en ce sens, rattache la III° République à une périodisation beaucoup plus dilatée, qui peut être séculaire ou même pluriséculaire ; elle peut remonter le cours du temps jusqu'aux régimes antérieurs du “siècle et même jusqu'à l'Ancien Régime, Une histoire sociopolitique de l'État amène ainsi à bouleverser la Chronologie traditionnellement fixée sur les deux dates de 1870 et 1940; on voit très bien pour une histoire de la magistrature, pour une histoire des grands corps, mais aussi pour l'histoire des rapports coloniaux de citoyenneté et de sujétion.

Mais une autre approche historiographique, attentive aux ressorts de la politique constitutionnelle, permet de rendre compte de la longévité de la III République d’une manière différente des deux historiographies dominantes. Elle suppose d'engager une relation critique vis-à-vis de la tradition juridique française. Car l'approche de la III‘ République comme expérience politique 2 longtemps été bloquée par le poids de la « doctrine » et les certitudes du droit constitutionnel. Les juristes, dans leur immense majorité, ont longtemps affublé la I1J° République d'images essentiellement négatives (le régime « conventionnel », le « régime d’assemblée »). 

#### 2- Le président de la République : un prince républicain
Les présidents de la 3e République sont effacés volontairement. Il y a eu un traumatisme de la 2e République. Le pouvoir du chef de l'exécutif est limité, il se limite d'ailleurs souvent tout seul au vue des démissions volontaires de 2 présidents.

La pratique de ce pouvoir est en constante évolution. Carnot va par exemple chercher à affirmer sa personnalité face au Général Boulanger|Boulangisme, tandis que bcp d'autres vont volontairement se contenter d'une place effacée, symbolique.

Pourtant, le personnage du président n'est en aucun cas démystifié. Une pratique répandue était d'aller dans les hopitaux pour voir les soignants. On prêtait aux présidents des pouvoirs de guérison étonnants. La chasse était une pratique courante, c'était une activité diplomatique qui était relayée dans la presse pour montrer la santé physique du dirigeant. La solde prêtée pour le poste de président était immense : 1,2 millions de francs de l'époque. C'était plus que pour l'homologue américain. Le président s'en servait pour faire des dons en tout genre.

#### 3- La république est vivifiée par la vertu de ses hommes
Il y a eu de nombreux scandales durant la 3e République 

Souvent, on retrouve le cas d'une porosité problématique entre le public et le privée. Un individu passant de l'un à l'autre avec des avantages certains par exemple. L'exemple typique est le Scandale des décorations, où on échangeait donc des décorations et des honneurs contre de l'argent ou des avantages en tout genre.

Les chercheurs aujourd'hui ne parlent pas nécessairement de corruption. Il s'agit plutôt pour eux de micropolitique, un façon de jouer sur l'échéquier politique qui ne bénéficie pas directement à l'État.

Les banquiers les conseillent sur les placements privés.

La corruption existait dans tous les pays occidentaux de l'époque mais c'était en France qu'elle était la plus mal perçue du fait de la révolution et la supposée moralité qui en découle.

Concept de neutralité : "En théorie, il règne en France un consensus concernant les activités politiques des députés. Tout parlementaire est obligé non seulement d'agir sans s'enrichir personnellement, mais aussi sans prendre parti pour un  intérêt particulier. Il est même douteux de faire avancer les intérêts des habitants de sa propre circonscription."

La vue sur la corruption a évoluée. "Qui dit corruption en 1800 dit aussi progrès, Lumières|Lumières, changement social. Qui le dit en 1900 met en question, désormais, le pouvoir des réseaux maçonniques, le système du capitalisme en plein essor et le pouvoir des banques. [...] Pour ainsi dire, les républicains libéraux se retrouvent maintenant tout à coup du mauvais côté de l'ancien combat."

"Jusqu'au milieu du XIXe siècle, cette opération reste probante, car il set toujours possible de se référer aux vestiges de l'ancien ordre moral. Une fois les systèmes parlementaires et les modèles sociaux "progressistes" établis, les républicains se voient inévitablement en contradiction avec leur modèle épistémologique politique. Cela ne concerne pas uniquement la France. Toutefois, on peut admettre qu'en France ce rapport est spécialement prononcé, à cause d'une tradition révolutionnaire vivante. [...] Pourtant, les contemporains n'ont pas saisi le piège épistémologique dans lequel ils se trouvaient. Par conséquent, les scandales devenaient les premières brèches pour l'extrême droite, à l'assaut du modèle libéral."

#### 4- La République est en danger
La République n'était pas assurée, les monarchistes auraient pu complètement reprendre le pouvoir en 1871. Mais pas rapport à la Commune de Paris, la sage République faisait bonne impression. 

La principale défense de la République se faisait grace aux gauches radicales. Aux élections, elles votaient pour le candidat républicain qui avait le plus de chance de gagner.

Débat central de la 3e république : suffrage en deux tours ou proportionnel

Vers la droite, la défense est variable. Elle dépend du parti en place et de l'intensité de la violence. Affaire Dreyfus et Boulangisme par exemple. Mais lorsqu'il s'agit de combattre cette droite en dehors des crises, il n'y avait que les groupes de gauche pour vraiment y mettre un peu de volonté.

Pratique du duel !

Groupes d'extrême droite de l'époque : Croix-de-Feu et Cagoule (Osarn). Assassinat de Marx Dormoy

Passage sur l'EG aussi.

#### Les formes de régulation sociale
##### 5- Le droit constitutionnel garantit le fonctionnement de la République
"Le droit constitutionnel semble donc décrire mais aussi créer le pouvoir dont il parle et le conférer aux institutions qu'il décrit dans le cadre d'un système, organisant un ensemble de relations entre ses différents éléments"

Instituée pour consolider le régime républicain et pour légitimer au cours des années 1880, la généralisation de l'enseignement du droit constitutionnel produisit la systémisation de ses concepts et ses concepts et de ses catégories savantes.

Réutilisé dans l'autre sens par les conservateurs.

###### La République dans son droit
Enseignement du droit constitutionnel pour légitimer la république à partir de Jules Grévy.

"Certes, la quasi totalité de ses schèmes et de ses catégories s'appuyait sur les théorisations existantes de la Révolution Française et sur la doctrine du parti républicain sous l'Empire très influencé par le Positivisme|positivisme."

"La principale de ces innovations n'est autre que la combinaison inédite entre une République démocratique et un régime parlementaire, forme même de la monarchie du XIXe siècle."

Eugène Pelletan compare l'enseignementdu droit constitutionnel à un "catéchisme républicain" qu'il faut désormais diffuser sur l'ensemble du territoire national et que, dans l'idéal, tout un chacun se devrait de maîtriser afin que "tout citoyen appelé plus tard à voter puisse connaître l'étendue et la limite de ses droits dès son enfance"."

L'ENS comme une forme d'apprentissage de la doctrine républicaine.

###### La République hors la loi
Réseau opposé : catholicisme social et les Semaines sociales|Semaines sociales de France
Effet boomerang

#### 6- L'ordre est républicain
> "Ordre : que de crimes on commet en ton nom"
> Gustave Flaubert

Peur des émeutes et des manifestations. Interdictions arbitraires et violences des forces de l'ordre.
##### Garantir ensemble l'ordre et la liberté
Apaches (voyous) qui rôdent. Voir la page wiki mdr !
Jeunes voyous issus des classes ouvrières dont le portrait type est travaillé par la lecture physiognomonique. Violents paresseux, alcooliques ou syphilitiques. Angoisse sécuritaire venant des incertitudes face aux changements de la société : industrialisation, nationalisme, sciences et démocratie.
==Super Chapitre, à relire==
Tany Zampa

##### Inclure et exclure
*Les étrangers.* 
La 3e république renforce la distinction, émergée avec la Révolution, entre le citoyen français et l'étragner non national.
Loi de 1889 sur la naturalisation qui équilibre droit du sol et droit du sang. Mais nationalisme ambiant qui souhaite contrôler les flux des personnes. Surtout les nomades. Constitution d'un Carnet anthropométrique|carnet anthropométrique d'identité en 1912 et d'une carte d'identité des étrangers en 1926.

Pas si efficace heureusement : 200 à 300000 étrangers fichés sur 2.7 millions.

*Les criminels*
Évolution des inquiétudes qui passe du vagabond et de la classe laborieuse au début du 19e au criminel et à l'étranger en 1930.
Emprisonnement des multi récidivistes.
Utilisation de méthodes alternatives à la prison, bagne par exemple.  70000 prisonniers en 1870 contre 12000 dans l'entre deux guerre.
###### Actualités et inactualités d'un ordre républicain
Les continuités avec le Régime de Vichy sont réelles, ce dernier a pu se servir des outils d'identification et d'explucsion de la 3e Rep. La distinction juif non juif s'est faite de la même manière que non français français.

Pourtant, on ne peut pas non plus dire que la 3e Rep était aussi répressive que Vichy.

Mais Michel Foucault - Que sais-je a bien démontré que Vichy s'appuyait sur les microtechniques de pouvoir avec l'école, l'armée, les prisons etc... C'est la disciplinarisation des espaces sociaux.

#### 7- L'armée, une institution républicaine ?
Principe de loyauté de l'armée à la République, compatibilité entre les modèles de ces deux institutions.
##### L'armée au service de la République ?
La République est née sur les faits d'armes. L'armée qui défend la patrie et qui rétablit l'ordre "menacé" par les révolutionnaires. Pas de droit de vote pour les militaires, statut spécial qui supprimait vraiment la démocratie au sein de l'armée. Pas le droit d'exprimer son opinion. Ce caractère autoritaire s'est montré lors de l'Affaire Dreyfus.

Égalité supposée face à la nation : création d'un service militaire pour tous. Pourtant, au lieu des 5 ans normalement demandés, une série d'exemptions pouvaient être demandées pour arriver à 1 an, cela concernait les classes les plus riches bien sûr.

#Alire Jean Jaurès : L'Armée nouvelle
	Rendre l'armée plus républicaine mais plus nationale. Basé sur la défense uniquement du territoire national. Défense des territoires.
	Évidemment incompatible avec la Colonialisme.

##### L'armée à l'école de la nation républicaine
Loi Guizot
Décret du 10 aout 1872 sur l'éducation morale du soldat. Autorité paternaliste et punitive qui contraste fortement avec la démocratie.
Théodore Iung.

> Georges Clemenceau dit sur l'armée en1898 :
> "Le principe de la société civile, c'est le droit, la liberté, la justice ; le principe de la société militaire, c'est la discipline, la consigne, l'obéissance. [...] Les soldats n'ont de raison d'être que parce qu'ils défendent le principe que la société civile représente. [...] Il faut que l'armée universelle, l'armée de tous, se pénètre des idées de tous, des idées universelles de droit, puisqu'elle se compose de l'universalité des citoyens."

##### Limite d'une acculturation
Savoir si l'armée a pu être réformée pour coller avec les idéaux républicains.
Existence des bagnes
#Alire 
Dominique Kalifa, Biribi
Albert Londres, Dante n'avait rien vu
Georges Darien, Biribi

De trop nombreuses punitions. Tout supérieur hiérarchique peut punir un soumis.

Globalement oui, de nombreux changements mais différences immenses avec le projet républicain.

#### 8- La République fabrique des patriotes
Question du Roman national !

##### Petite histoire de la République en guerre
Guerre franco-prussienne 
Idéal républicain qui commence avec Léon Gambetta et sa tentative de levée de masse !
Et armée jusqu'à Philippe Pétain

##### L'armée et la nation en armes
L'armée française en 1860 est l'une des meilleures du monde en dépit de quelques défaites. La Prusse a pourtant pour elle le prestige militaire qu'elle démontre notamment lors de la Bataille de Sadowa contre l'Autriche-Hongrie.

Le Nationalisme est augmenté parce que la France a perdu contre une seule nation alors que Napoléon 1er avait perdu contre une coalition.

Apparition de la guerre de masse qui menace pour la première fois les civils directement (oubliant les pillages qui sont officieux).

Revanche vue non pas comme un soutien à l'armée mais plus comme une volonté personnelle de participer à la défense de la Nation --> Acceptation de la levée en masse de la Première Guerre mondiale.

"Dressage des jeunes français" à l'armée et à la revanche.

Plus de la moitié des pertes françaises de la guerre ont eu lieu avant la fin de l'automne 1915.

Réflexions sur le livre de Jaurès cité plus haut

Surplus d'armée après la guerre (?), utilisation dans les colonies. Double jeu et allers retour entre la protection du territoire et la colonisation.

#### 9- L'école républicaine est méritocratique
Parmi les idées reçues que la première partie du livre à vocation à questionner, il en est une qui pourrait apparaître comme une provocation, sinon une boutade. Si, pendant longtemps, le caractère méritocratique de l'école républicaine était considéré comme un poncif de l’histoire scolaire française, plus encore de son modèle ferryste, il semble aujourd’hui avoir été remplacé par un autre lieu commun : celui de l'échec pérenne de cette ambition « méritocratique » dont la littérature sociologique contemporaine ne cesse de dénoncer le caractère « hypocrite » ou « socialement mortifère ». C'est là la célèbre thèse développée par Michael Young dans le livre de sociologie fictionnelle qu'il publie en 1958 sous le titre The Rise of the Meritocracy. À partir de l'exemple historique anglais, l’auteur — qui a largement contribué à populariser le terme de « méritocratie » - imagine trois phases : un premier    
Stade de forte reproduction sociale, une étape d'engouement croissant pour le modéle méritocratique de justice sociale et, enfin, une dernière phase faite de critiques et de mouvements sociaux visant à remettre en cause ce principe de justice jugé nuisible pour le lien social [Young, 1958].

C'est d’ailleurs l’un des rares points communs de cette vulgate sociologique : qu’elle soit d'inspiration Marxisme|marxiste ou influencée par les travaux sur la reproduction scolaire de Pierre Bourdieu et de Jean-Claude Passeron, ou encore fidèle à des formes d'interventions sociologiques moins surplombantes, la sociologie de l'éducation a largement renseigné les inégalités sociales (mais aussi parfois de genre et d’origine ethnique) qui affectent aujourd’hui comme hier, les conditions de possibilité de l'équité scolaire et de 
la mobilité sociale. Une place particulière devrait être ici faite aux travaux de Raymond Boudon qui font apparaître un lien faible entre le niveau d'instruction et la mobilité sociale [Boudon, 1973].

De nombreuses enquêtes internationales attestent du caractère général des inégalités sociales dans, par et face à l'école. Dans les pays occidentaux, malgré la diversité historique des systèmes éducatifs et des curriculums scolaires, l'échec scolaire est notablement le fait d'élèves ayant des caractéristiques sociales proches : cet échec est surreprésenté parmi les élèves issus des milieux sociaux « défavorisés », mais aussi parmi les élèves des minorités linguistiques dont le poids démographique a fortement progressé au fur et à mesure de l’arrivée des vagues migratoires multiculturelles nécessaires à l'économie européenne. Certes, ces inégalités sociales face à l’école ne « sont pas un pur décalque des inégalités sociales » observables dans la société ; plus encore, elles sont le résultat complexe de « carrières scolaires » et de « stratégies éducatives » qui avantagent les « héritiers » et creusent notablement les écarts au sein de la population [Duru-Bellat, 2000, p. 324-325]. Ce qui explique probablement que les politiques scolaires volontaristes, voire discriminantes,
semblent peiner à remettre en cause fortement ces inégalités.

Le diagnostic sociologique est encore plus sévère si on intègre ici les travaux qui tendent à pointer, avec plus ou moins de Prudence empirique [Duru-Bellat et Tenret, 2009 ; Tenret, 2011], le fait que l’école légitime les inégalités qu’elle reproduit en inculquant aux élèves à la fois le principe du mérite et la légitimité de la méritocratie Scolaire comme instance centrale de la hiérarchie sociale et économique [Bourdieu et Passeron, 1970]. Ce diagnostic partagé conduit de nombreux auteurs à adopter une posture critique, voire dénonciatrice, à l'égard d’une « méritocratie scolaire » devenue aujourd'hui « culte de la performance » et dont le poids est jugé dysfonctionnel ; d'où l'effort d'imaginer d'autres critères (comme celui de l'équité, par exemple) pour refonder notre politique de justice sociale [Duru-Bellat, 2009] et redonner à l'école une réelle capacité d'institution du social.

Ce court chapitre entend suspendre, le temps d'un détour par le passé, ce procès fortement intenté dès les années 1970 à l’école républicaine, c'est-a-dire à un moment où la démocratisation de l’enseignement prend la forme d’une inflation des diplômes et d’une unification renforcée de notre système scolaire. Il convient, en effet, de reconnaître que, lors de son émergence dans le débat et dans les politiques scolaires, la « méritocratie » est à la fois l'objet d'une proclamation de principe forte et d'une application pratique particulièrement circonspecte. Là encore, la III‘ République - dès lors qu'elle n'est ni idéalisée ni caricaturée, mais simplement historicisée avec le souci d'éviter tous les anachronismes et les jugements de valeur  offre l'exemple d’un laboratoire éducatif et politique qui associe une affirmation axiologique en rupture avec le passé et une mise en forme politique faite de pragmatisme et de discernement.

##### Entre égalité et mérite : la tension fondatrice de l'école de la république
Pour se convaincre de la rupture qu'engage l'oeuvre scolaire que prônent les républicains opportunistes au début des années 1880, il suffit de lire le beau témoignage qu’en donne Mona Ozouf dans sa récente au autobiographie. L'historienne y rappelle la « tranquille assurance que dispense école, de son enfance bretonne. Ce « sentiment de profonde sécurité affective » - formulation qui n’est pas sans évoquer, on le verra plus loin, la « bienfaisance » qu'un Paul Lapie met au cœur de sa politique éducative dans l'entre deux-guerres - s’enracine, nous dit Mona Ozouf, dans « un credo central, celui de l'égalité des êtres ». Et l'auteure de Composition française de préciser que l'école publique « promet le monde à qui veut s’en saisir. [...] [Si] on est muni de listes de bons livres, de plans d'existence réfléchis, d'application et de courage, il n’est rien dont un être raisonnable ne puisse venir à bout. Et devant cette tâche, nous sommes tous à égalité » [Ozouf, 2009, p. 116-1171].
Cette promesse méritocratique est au fondement de la vocation des instituteurs et institutrices que révèle l'enquête classique menée par Jacques et Mona Ozouf [1992] auprès de 20 000 d’entre eux ayant enseigné avant 1914 et continuant, dans leurs riches témoignages recueillis dans les années 1960, à opposer une école privée faite de « privilèges » autant que de « dévotion » à cette « égalité sur les bancs » [Ozouf, 2009, p. 117: Ozouf, 1992 p. 277]

Cette promesse trouve chez Jules Ferry tout à la fois son fondement idéologique et son application principale. Avant même que les républicains ne soient en mesure de réformer en profondeur le système éducatif, Ferry en fait le serment dans un célèbre discours prononcé, le 10 avril 1870, quelques mois avant l'effondrement de Sedan, devant la Société pour l'éducation primaire, en salle Molière. Lors de cette conférence prophétique, il inscrit clairement le principe de l'égalité des chances que l'école républicaine entend promouvoir
au cœur d’une rupture que la Révolution de 1789 a proclamée définitivement. « Fils de 89 » et notamment de Condorcet dont il fait sa référence en matière éducative, le futur président du Conseil y énonce le « devoir de notre siècle » :

> « L'inégalité d'éducation est, en effet, un des résultats les plus criants et les plus fâcheux, au point de vue social, du hasard de la naissance. Avec l'inégalité d'éducation, je vous défie d’avoir jamais l'égalité des droits, non l'égalité théorique, mais l'égalité réelle, et l'égalité des droits est pourtant le 	fond même et l'essence de la démocratie... Et si la société moderne n'’arrivait	pas à séparer l'éducation, la science, de la fortune, c'est-à-dire du hasard de la naissance, elle retournerait tout simplement au régime des castes. » Et le 	conférencier de préciser à dessein qu’il ne vient « pas [ici] prêcher je ne sais quel nivellement absolu des conditions sociales qui supprimerait dans la société les rapports de commandement et d’obéissance. Non, je ne les supprime pas : je les modifie. Les sociétés anciennes admettaient que l’humanité fût divisée en deux classes : ceux qui commandent et ceux qui obéissent ; tandis que la notion du commandement et de l’obéissance qui convient à une société démocratique comme la nôtre, est celle-ci : il y a toujours sans doute, des hommes qui commandent, d'autres hommes qui obéissent, mais le commandement et l’obéissance sont alternatifs et c’est à chacun à son tour de commander et d'obéir » 
> [Ferry, 1996 (1870), p. 63-64.].
> Jules Ferry - 10/04/1870

On verra à juste titre dans ce discours le fondement d’une « méritocratie scolaire » (celle d'une école républicaine qui donne à tous les enfants les moyens d’estomper les inégalités de naissance) qui débouche sur une forme d’« élitisme républicain ». Sans remettre en cause l’ordre social, il permet de distribuer les positions de commandement sur la seule base légitime dans une démocratie qui fait corps avec l'école : l'émulation qui fait de chacun un égal, mais aussi un rival. Dans l’article qu’il consacre à l’« émulation », le Dictionnaire de pédagogie et d'instruction primaire édité en 1887 par Ferdinand Buisson en donne la définition préliminaire suivante : « sentiment qui nous porte à vouloir faire aussi bien ou mieux que nos semblables » (article rédigé par Édouard Jacoulet, dans le tome I, p. 827).

Là encore le témoignage de Mona Ozouf est précieux pour comprendre cette dualité fondatrice. La « grande école » où l'historienne fait l’apprentissage d'une « sociabilité démocratique » qui l’enchante est loin d’être le lieu d'une « mystification » qui « feint de croire qu’en entrant dans la classe tous les écoliers, en dépit de leur bagage culturel, sont égaux, pour mieux faire voyager, intactes, les inégalités héritées. Rien n'était plus étranger aux croyances de mon école primaire que ce procès : elle nous assurait que nous étions les filles de nos seuls mérites et de notre seul travail, si bien que le classement était pour nous le véritable instrument de l'égalité » [Ozouf, 2009, p. 118-119]. Le « mérite » s'oppose désormais aux « hasards de la naissance » et aux « inégalités » de la fortune. Les talents révélés par une pédagogie capable d’estomper les différences se substituent à la transmission héréditaire des grandeurs. ^ad4333

Principes égalitaires et pratiques distinctives sont toutefois ici conciliés pour donner naissance à une révolution scolaire qui fait passer la société française du stade de l'« ascription » à celui de l’« achievement » pour reprendre les termes anciens de Talcott Parsons [1937]. Alors que la première expression valorise les facteurs « hérités » où « prescrits » et traduit la rigidité des stratifications sociales archaïques des sociétés d'ordre ou de caste, la seconde vise à concilier l’« égalisation des conditions », jugée inéluctable, on s'en souvient, par Alexis de Tocqueville, avec l'affirmation des facteurs « acquis » grâce et par l'école. Ce que l’on réussit par soi-même l'emporte désormais sur les positions héritées et assignées par la structure sociale. C’est là, on s'en souvient, le sens de l'avènement des « nouvelles couches sociales » proclamé, dès 1873, par Léon Gambetta lorsque l'orateur républicain évoque l'« avènement d’un monde nouveau [...] qui est heureusement arrivé non seulement au travail, la propriété, mais à la capacité politique » [Gambetta, 1881 (1873), p. 42]. Cette « démocratisation du mérite » est aussi illustrée par la multiplication des récompenses honorifiques et autres décorations officielles sous la IIIe République (voir l’article de Frédéric Caille au chapitre 24 de cet ouvrage), Comme l’a montré récemment Olivier Ih], elle inaugure une entreprise plus globale de « Cotation sociale » qui concilie l'affirmation de l'égalité civique et la nécessité de distinguer les « talents » [Ihl, 2007].

Comme le proposait Condorcet, dans le préambule du rapport qu'il présente à l’Assemblée nationale les 20 et 21 avril 1792, dans une telle «société des émules », l'école publique « [assure] à chacun d’eux [les individus] la facilité de perfectionner son industrie, de se rendre capable des fonctions sociales auxquelles il doit d'être appelé, de développer toute l'étendue des talents qu'il a reçus de la nature ; et là établir, entre les citoyens, une égalité de fait, et rendre réelle l'égalité politique reconnue par la loi» (Condorcet, 1982 (1792), p. 181]. On ne sera Pas surpris de retrouver un long commentaire approbatif de ce préambule dans l’article « Instruction publique » que rédige Ferdinand Buisson dans son Dictionnaire de pédagogie et d'instruction primaire, On en trouve aussi l'écho sous la plume éloquente d'un Jules Barni lorsque l’auteur du Manuel républicain (1872) déclare que « l'’amour de l'égalité n’est pas la haine de toute supériorité », Adaptée au contexte d'une société industrielle en développement dans les premières décennies de la III' République, le système éducatif entend désormais permettre aux mérites individuels de se révéler. Il encourage la promotion et la mobilité social et il traduit, via les diplômes qu'il consacre, les mérites en positions socioprofessionnelles. « Institutrice du peuple » - ce « peuple » dont Michelet avait brillamment évoqué en 1846 l’« avènement » ., l'école publique se voit donc confier une mission impérieuse, mais complexe.

##### Une application circonspecte
Idée que l'instruction obligatoire de Jules Ferry serait une idée Communisme. 

Idée à l'époque que le manque d'instruction religieuse conduisait au crime.

Idée du Malthusianisme scolaire. Étanchéité volontaire du secondaire pour maîtriser les flux de la main d'oeuvre. Comme la Loi Haby après finalement. 

L'éducation devient une priorité de la politique républicaine dès 1879, ce qu'atteste la multiplication des lois scolaires qui fondent pour partie encore notre système éducatif : loi du 16 juin 1881 établissant le principe de la gratuité scolaire, loi du 28 mars 1882 sur l'enseignement primaire obligatoire et laïque, loi du 30 octobre 1886 qui laïcise le personnel enseignant, loi du 19 juillet 1889 sur le traitement des instituteurs désormais assimilés à des
fonctionnaires pris en charge par le budget de l'État. La durée moyenne de scolarisation s'accroît de manière sensible sous la IIIe République : 5,5 ans en 1896, 6,34 ans en 1911, ans en 1921... atteindre, à titre de comparaison, 12,43 ans en 1996. Le budget que la République consacre à cette mission scolaire fait un bond spéctaculaire, puisqu'il est multiplié par plus de
3,6 entre 1879 et 1889. Pourtant, la promesse de démocratisation scolaire reste modérée et s'arrête au terme de l'école primaire élémentaire. Cette conception malthusienne est toutefois tempérée, notamment par l'émergence — dans une certaine indifférence - d'un enseignement secondaire féminin grâce à la réforme de Camille Sée du 21 décembre 1880. Dans son étude classique, Françoise Mayeur montre bien que cette réforme est elle aussi une réforme prudente h, à la recherche d'un équilibre improbable entre les positions hostiles des conservateurs qui la refusent et les positions revendicatrices des féministes de l'époque qui la jugent timorée (Mayeur, 1977).

##### Une ingénierie du mérite méritocratique
#Alire 
Olivier Ihl, Le mérite et la République
Pierre Barral, Jules Ferry, une volonté pour la république

Culture du Nationalisme of course
Je n'aime pas ce paragraphe (du coup je le laisse tel quel)
On pourrait discuter cette affirmation et notamment le caractère binaire de que dessine Henri Wallon en 1946 entre une école tique, celle de l'. individualisme républicain » des pères fondateurs juge abusivement sélective et une « école unique » apte à promouvoir de manière homogène un niveau sans cesse plus élevé de culture générale. Retenons seulement de cette opposition le fait qu'elle caractérise bien, sans le vouloir probablement, le destin contrarié de la « méritocratie scolaire après 1945. Dans un système éducatif de plus en plus unifié (réforme Berthoin de 1959, réforme Fouchet de 1963, réforme Haby de 1975), dans un svstème éducatif cherchant - vainement ? - à resserrer la structure des inégalités scolaires sans faire désormais de la mobilité des individus une priorité, dans un système éducatif devenu de moins en moins sensible à l'ingénierie de la méritocratie scolaire évoquée plus haut, ce sont les fondements mèmes de l'alchimie scolaire républicaine qui ont été progressivement métamorphosés. D'où probablement le sentiment très vif aujourd'hui d'une nécessaire  refondation qui permette d'adapter un système éducatif en crise prolongée à un environnement social et culturel certes nouveau, mais qui n'en rend çu.S  oins la promesse scolaire impérieuse. Et ce afin qu'. aucun enfant de la République ne Isoitl laissé de côté, abandonné, discriminé, et quel la promesse de la réussite Isoitl honorée, rx»ur l'accomplissement, VX)ur chacun, pour sa vie et pour son destin personnel IX)ur reprendre les termes du discours de François Hollande, tout juste élu président de la République, le
6 mai 2012 à Tulle.
### La dynamique des exclusions
#### 10 - La République garantit l'égalité des citoyen·ne·s 
de Laura Lee Downs
La démocratie est exclusive et non excluante, car elle n'énonce pas les règles de l'exclusion. Elle produit l'exclusion par une série d'empêchements réels et imaginaires, juridiques et médicaux, littéraires et philosophiques. Il n'y a donc pas plus d'énoncé de l'exclusion des femmes qu'il n'y a de texte fondateur de contrat sexuel. Il n'y a pas d'énoncé d'exclusion car il y aurait contradiction trop forte avec les princiVRs de la démocratie [Fraisse, 2000, p. 411]

La démocratie moderne est fondée sur la dissociation entre la sphère domestique et celle de la politique, entre la famille et la cité, ceci marquer pour la rupture avec l'analogie entre le gouvernement patriarcal de la famille et celui de l'État ; analogie qui, à l'époque moderne sous-tend et justifie la monarchie. Mais, comme le souligne Geneviève Fraisse, le nouveau contrat social ne se complétera jamais par un contrat sexuel qui aurait précisé et défini la nouvelle relation entre les sexes suite au bouleversement qu'est la Révolution. De fait, après cette dernière, les femmes sont présentes et actives dans la cité (en tant que travailleuses, par exemple), mêmes si elles sont en état de nullité » politique. Car la République naît par un geste paradoxal qui fait en sorte que les femmes sont d'un même mouvement • dissociées et asscxiées à la nouvelle société. Certes, elles sont en état de nullité politique, hors statut, et IX)urtant présentes, surchargées de taches sociales à travers leur vie privée Leur exclusion est donc produite plus qu'énoncée, fabriquée et non théorisée • [Fraisse, 2000, p. 421]. De plus, la surcharge de tâches scxiales qui leur incorntk servira, dans les faits, de tremplin vers un rôle public plus large ainsi que vers des droits scxiaux et économiques, conçus en vue de concilier maternité et travail. En ce qui concerne les femmes, l'acquisition des droits sociaux et économiques va précéder l'acquisition des droits civiques et politiques dans le cadre de la Ill' République.

Par conséquent, et précisément à cause de cette incertitude dans l'énoncé, l'inclusion dans l'espace public est rendue IX)ssibIe. Elle va déployer tout au long de la Ille République, où les femmes vont agir dans les failles de l'exclusion moderne de l'espace public, à savoir dans le monde du travail (où les femmes françaises sont particulièrement actives), dans l'éducation et dans l'action sxiale. Ces trois registres d'activité publique constituent trois voies vers la citoyenneté, statut auquel les femmes vont accéder dans une République qui ne garantit pas l'égalité des citoyen(ne)s, mais qui, n'ayant jamais fait du sexe une catégorie à part, n'arrive pas non plus à les exclure de la cité. • Ainsi fut la démocratie exclusive, explique Geneviève Fraisse : non pas un svstème délibéré d'exclusion mais une dynamique où sélection des hommes et omission des femmes se jouaient au nom de l'universel. Ainsi une certaine
inclusion se fit au travers des omissions et incohérences de la loi • [Fraisse,
p. 611] 

On donc dire que les femmes françaises se fraient un chemin vers la citoyenneté qui est distinct de celui frayé par les hommes. Au lieu d'agir dans les instances Pk)litiques après l'octroi du vote (mcxièle masculin), elles s'activent dans la cité sur plusieurs registres. Le travail en premier lieu, car les femmes françaises ont toujours participé davantage au monde du travail que leurs sœurs ailleurs en Europe ou en Amérique du Nord [Perrot, 2007,
p. 344]. Puis, avec la création de l'école républicaine, elles investissent massivement en tant qu'institutrices une institution qui est fondamentale la démocratie républicaine. Enfin, elles sont de plus en plus actives dans l'action philanthropique et sociale, domaine d'action qui est saturé de signification politique en raison de l'angoisse de la dépopulation qui se répand progressivement à partir de la défaite en 1870. À terme, les diverses formes de protection scxiale inventées soit dans la sphère politique, soit dans la société civile (philanthropes, éducateurs, médecins, industriels) constituent un véritable espace paralX)Iitique, où les services et institutions reçoivent des subventions et fonctionnent souvent comme des services publics en étant reconnus d'utilité publique [Downs, 2009]

##### L'action sociale comme action publique
L'intégration progressive des femmes dans la sphère publique et polotique, qui se fait timidement, mais régulièrement, durant la IIIe République, devient particulièrement visible au lendemain de la Première Guerre mondiale, avec l'entrée des femmes dans de nouveaux secteurs de l'économie et de l'activité publique : industries mécanique et métallurgique, secteur tertiaire, secteur sociomédical. Ce dernier domaine est particulièrement pour un processus d'intégration d'un grand nombre de femmes au monde politique.

Comme la plupart des femmes qui militent dans l'Action scriale féminine de ce mouvement d'extrême droite, Mme Horaist minimise la dimension politique de son travail, croyant de bonne foi que le social, censé rassembler tout le monde, n'a rien à voir avec la IX)litique, souvent traitée par ces femmes comme une affaire de • clans qui divise la nation au lieu de
l'unifier. • Le cOté politique ne m'a jamais intéressée ; je n'ai jamais mordu te n'ai pas l'esprit politique, Au contraire, j'étais beaucoup plus tournée vers le social 3. • Cette volonté de considérer comrne • apolitique • une action sociale entreprise dans le cadre d'un mouvement politique reflète la relation compliquée qu'aurait pu avoir une jeune bourgeoise
de l'époque avec l'action politique, surtout si elle s'est investie dans l'action sociale, autre forme d'action publique qui est valorisée justement pour sa neutralité présumée sur le plan politique. Mais elle reflète aussi la porosité de la distinction entre la sphère masculine de la politique et celle, féminine, de l'action sociale à une époque où la mise en place de services sociaux visant les classes populaires (et même les classes moyennes) se déploie de tous les
côtés de l'échiquier politique, des « municipalités providences • roses ou rouges de la banlieue parisienne aux centres sociaux du mouvement Croix-de-Feu et du Parti social français. Ainsi, la directrice de l'Action féminine du CF/PSF, Antoinette de Préval, reproduit dans ses discours la même volonté de considérer comme apolitique cette mission sociale qui envisage la conquête politique des milieux ouvriers par le moyen social : Nous demandons une vision civique à nos assistantes sociales [mais] on confondait à ce moment-là civique et IX)litique, remarque-t-elle en décembre 1940 dans une réflexion rétrospective sur l'action sociale des CF/PSF pendant les années 1930. Nous ne faisions aucune IX)litique mais nous étions farouchernent français. "Jusque dans les banlieues les plus révoltées nous plantions les
trois couleurs ", conclut-elle en des termes qui ne laissent aucun doute sur la
nature profondément apolitique de cette action sociale qui cherche à refaçonner les relations scxiales et l'inconscient politique des masses 4. C'est le social qui refait le "climat" d'un pays affirme-t•elle en 1938. Nous qui "travaillons" le gosse communiste depuis des années, nous savons ce que cela veut dire ! Discipline, sentiment du devoir, abnégation, solidarité, sensibilité sans aucune sensiblerie, confiance, initiative. Et l'âme du gosse va se former avec nous... puisque l'action sociale tend à ramener l'enfant à l'amour du
travail, de la famille et, par là même, de la Nation. 

##### Vote familial et Suffrage féminin : une rencontre fatale
Faire voter la famille et non l'homme individuelement. Idée que la famille n'est rien moins que la cellule sociale de la nation. Paternalisme !

Familialiste et natalistes alike

Un super chapitre qu'il faut relire quand j'aborderai la question !

#### 11- La République est ouverte à toutes les classes sociales
Malgré les idéaux d'égalité, la création de la République était fortement imprégnée des différences de classes. Volonté de créer l'égalité !

##### Un régime d'exclusion ? Les mots et les choses
Selon l'auteurice de ce chapitre, la Commune de Paris tient plus d'une nouvelle révolution française qu'à une révolution prolétarienne.

Malgré cette opposition sanglante et violente, les grèves qui ont suivies n'ont pas été des vengeances.

Création d'une unité des citoyens pour créer la paix sociale.

Cette interrogation, parfois cette hantise, traverse l'époque. En 1831, en des termes fameux, l'homme politique et critique littéraire Saint Marc Girardin affirme : Les barbares qui menacent la société ne sont plus dans le Caucase, ils sont dans les faubourgs de nos villes manufacturières. La crainte d'un monde ouvrier présenté comme absolument étranger, comme celle d'une lutte des classes qui, aux yeux d'une partie des élites du XIX siècle, ne saurait mener qu'au retour à la barbarie, fait aussi partie du contexte dans lequel se construit la démocratie républicaine.

Exemple :
Léon Bourgeois :
> Il n'est plus politiquement de bourgeois et d'ouvriers, la révolution et le suffrage universel ont fait de tous des citoyens et des électeurs.

> Le mot "classe" devrait sonner faux aux oreilles d'hommes qui se disent épris de la Révolution française. Portis, 1988.

#Alire 
Émile Durkheim
Pierre Rosanvallon, Le peuple introuvable. Histoire de la représentation démocratique en France
Pierre Rosanvallon, Le Modèle politique Français.

Les gens se rendent compte que l'individualisme républicain est en partie une fumisterie.

La principe de classe devrait ne pas exister dans une république utopique, mais pourtant il existe. Il fallait donc, et il y a eu, des législations prenant en compte les classes et leurs luttes.

##### La présence et l'absence : la classe ouvrière dans l'imaginaire politique républicain

Gris blablabla

Syndicalisme révolutionnaire : Pierre-Joseph Proudhon, Émile Pouget, Victor Griffuelhes et Georges Sorel.

La classe ouvrière gouverne-t-elle alors, si l'on veut reprendre la formulation non de Fabre d'Églantine, l'imaginaire des hommes en République ? Oui et non. Si elle est intégrée dans les formes politiques et culturelles républicaine. elle n'en constitue pas un élément structurant, loin de là : les caractéristiques singulières des représentants, des représentations des ouvriers ont pour être acceptées, à s'effacer. Seuls les socialistes intègrent la classe ouvrière à leur imaginaire. Mais ils voient moins en elle sa spécificité, sa relation ambivalente d'ouverture et de distance gardée par à la société environnante que la possibilité de reconstituer, à travers elle, l'harmonie et l'authenticité d'une nouvelle République, conjuguant l'égalité sociale à l'égalité politique.

##### Communisme, classe et communauté
Après la Première Guerre mondiale
#Alire plus tard

#### 12 - La République est assimilatrice
Sur les années 1930.
#Alire plus tard
Assimiler comme pour dire que les différences entre les populations disparaissent au contact avec la culture et le modèle français.

Différence entre assimilable et non assimilable. 

##### Grammaire de la concurrence
Sur les années 1930.

#### 13- De la mission "civilisatrice" à la "République coloniale" : d'une légende l'autre
Je n'ai pas aimé ce chapitre du tout

La mission civilisatrice » et la « République coloniale » se révèlent donc comme deux clichés historiographiques apparemment antagonistes - l'un procolonial, l'autre anticolonial - qui les mèmes caractéristiques. De la légende dorée de l'historiographie républicaine à la légende noire des chercheurs postcoloniaux. le rapport de la République au fait colonial est toujours appréhendé suivant le même vx"nt de vue (celui des colonisateurs), selon les mêmes sources (propagande. discours politiques et littérature) et selon une même méthode (analyse du discours). Postcoloniaux de la V' République et propagandistes coloniaux de la Ill' République sent une même obsession pour la République, également essentialisée, En
prétendant • décentrer • le regard, les études postcoloniales ont au contraire renforcé au mépris de la prise en compte de la diversité des Situations coloniales. Comme les alN3tres de la mission civilisatrice avaient surestimé l'impact de la républicanisation des colonies, les auteurs postcoloniaux ont probablement exagéré l'influence des colonies sur la République. En fait, l'Empire n'occupe une place centrale dans le débat public que tardivement, à partir des années 1930 et surtout la Seconde Guerre mondiale, lorsque le maréchal Pétain y voit le moyen de conserver son tandis que le général de Gaulle veut en faire l'instrument de libération de la métropole. Et si l'on en croit Charles-Robert Ageron, la République ne s'est enfin réconciliée avec ses colonies qu'au lendemain du conflit : S'il fallait indiquer la date exacte où l'œuvre coloniale de la France républicaine parut s'accomplir dans la fidélité à son idéal égalitaire de toujours, ce serait le 25 avril 1946 qu'on devrait désigner. Ce jour-là, l'Assemblée constituante, en accordant à l'unanimité, sur la proposition d'un député noir du Sénégal, Mt Lamine Gueye, la qualité de citoyen français à tous les ressortis.sants des territoires d'outre-mer, donna satisfaction à l'aspiration profonde de la tique coloniale de la République : l'égalité dans la famille française [Ageron, 1997, p. 514-515] Toutefois, ce n'est qu'en 1956-1958 que. dans un difficile contexte de décolonisation, les Français suppriment le système des collèges séparés et instaurent le suffrage universel. Paradoxalement, la décolonisation aura été le meilleur aiguillon d'une républicanisation impossible et
désormais anachronique.

En gros, les post coloniaux et les procoloniaux se basent sur les mêmes sources et disent n'imp ? On les met dans le même panier des gens qui comprennent R ?
WTF

#### 14 - La République et les républicains, adversaires du religieux et des religions

#### 15 - L'Empire dans la IIIe République
Rôle initial d'Adolphe Thiers, adorateur de Napoléon III mais honneur de Victor Hugo.

On pourrait considérer que la 3e République était anti-impériale.

Rejet de l'exécutif fort mais jeu avec l'histoire pour ne pas aller contre la figure de Napoléon. Bonapartisme et Boulangisme toujours présents.

##### La constitution d'un héritage : La république à l'école de l'Empire
Principale continuation de l'Empire dans la République : le Conseil d'État ! Centenaire de 1904
Code civil
Concordat de 1802.
Création des préfectures. "Empereurs aux petits pieds".
Légion d'honneur aussi !

##### Napoléon au coeur de courants politiques contre la République : du bonapartisme au boulangisme
Préférence créée par les temps impériaux de la France à laisser un homme fort et charismatique gérer le pays. Manifestations de soutien à Napoléon exilé jusqu'en 1879.

IIIe Rép veut aller contre l'idée d'un homme providentiel. 

Schisme dans le bonapartisme en 1884 :
- Prince Victor
- Prince Jérôme

Il est vrai que la greffe du bonapartisme avec le boulangisme n'a pas pris. Pourtant, les deux courants avaient bien des points en commun : l'amour et le culte du chef ; le besoin d'autorité et l'antiparlementarisme ; le nationalisme et, pour finir, le soutien de militants populaires qui attendaient d'un nouvel homme providentiel l'amélioration de leur condition sociale [Ménager, 1988, . 342-343]

##### Une référence politique et culturelle persistante : le sauveur
II demeure une référence centrale dans une culture faite d'exaltation des forces de vie, de sens du tragique et d'invitation à l'action [Girardet, 1986, p. 84]. Tout en fixant ses regards sur la ligne bleue des Vosges la France n'oublie pas celui dont l'action débordante mena ses sur toutes les routes de l'Europe : il est bien érigé, pour toute une génération, en un mcdèle d'énergie que la République ne doit pas se priver d'utiliser. Quant à Léon Bloy, en évoquant, en 1912, L 'Âme de Napoléon, il fait de lui un nouveau messie, le plus inexplicable des hommes », en tout cas un instrument Dieu a utilisé pour accomplir le destin des hommes. Par lui fut consommée la Révolution française. La vision messianique de Léon Bloy montre que le début du xx• siècle peut encore parfois faire écho à la légende du premier siècle.

##### La gloire impériale : un recours pour la République
*Gambetta admirait Napoléon*

Le mythe de I 'Empire est donc convoqué pour préserver culture républicaine de la contagion communiste. Dans les prolongements d'une victoire dont la France ne finit pas de se satisfaire et dans la certitude de la solidité d'une République capable de résister à une guerre longue et difficile, la mémoire de l'Empire n'effraie plus et semble bien au contraire utile. Le concert de louanges qui s'est manifesté à l'occasion du centenaire de la mort de Napoléon, le S mai 1921, ne se joue cependant pas dans une complète unanimité : les oppositions viennent des deux extrémités du champ politique, Le Parti communiste français|Parti communiste est le plus radical dans son refus de s'associer aux réjouissances [L 'Humanité, 5 mai 1921]. Alexandre Blanc remarque pour sa part l'étrange admiration des républicains pour celui qui étrangla la République et des royalistes pour celui qu'ils nommaient autrefois « l'usurpateur •
[L 'Humanité, 6 mai 1921]. Le lendemain, Charles Rappoport fait un parallèle entre Karl Marx, génie bienfaisant que les communistes vénèrent avec raison, et Napoléon, génie malfaisant, esclave de sa volonté de dominer et père du bourrage des crânes patriotiques [L'Humanité, 7 mai 1921]. Avec l'entrée du Parti communiste sur la scène de la vie politique républicaine, s'exprime donc une nouvelle forme d'opposition à Napoléon, lequel ne peut trouver grâce
auprès de ces pacifistes convaincus.

#### 16 - L'idée républicaine et ses métamorphoses
##### Un lien organique entre république et philosophie
Philosophie de la République définit par ses fonctionnaires et ses membres
Philosophie positive et rationalisme kantien.
#Alire Chiens de garde, Nizan

C'est pourtant lié à ce que je veux faire mais c'est en même temps ultra bizarre.

#### 17- L'historiographie de la IIIe République : ni histoire ni République ?
Euuh
> "Même s'il était encore porteur d'une vision héroïsée du passé républicain, Bourgin estimait qu'il était d'agir tout à la fois en historien et en républicain, la République garantissant comme une forme d'objectivité critique d'elle-même."

Appel aux historiens pour légitimer la République qui n'a pas la religion pour se justifier. 
Histoiriens = Aristocratie républicaine 

##### Les historiens à la recherche de souveraineté
"école morale" : Défendre la République par l'histoire en confondent la politique et la science.
Véritable propagande faite par des historiens médiocres.

Porter en héros les républicains des années 1870. En faire des portraits complets pour documenter leur pensée.

##### Controverses sur les origines révolutionnaires de la république.
Se servir de la Révolution comme référence, comme symbole. 1789
##### L'histoire contemporaine de la République : Affirmation et contestation
Grosse pression sur les autres historiens. Tous les historiens devaient parler de la République donc beaucoup s'abstenait.

Tout change après l'affaire Dreyfus.; Après ça, les historiens deviennent plus critiques par soucis de démocratie. Gros travail d'analyse des archives qui a réveillé les historiens.

Exaltation de la Nation passe ensuite au dessus de la Révo fr comme agent nécessaire de la République.

##### La nostalgie des origines et la République des professeurs
De 1919 à 1940, expliquer les origines de la République face aux dangers et défis et comment des hommes talentueux et les institutions ont réussi à venir à bout de tout ça.

1937 : Société de l'Histoire de la 3e Rep.

#### 18- La fondation de la République : histoire, mythe et contre histoire
Photos du 4/10.
Néo-Républicanisme : Aujourd'hui vu comme une idéologie fixe, elle était pourtant très dynamique et changeante.

Avant tout le compromis qui divise le moins entre Républicains et libéraux

Individualisme, propriété, patriotisme et laïcité.

Influence du Bonapartisme. 
Celui-ci n'était pas juste de l'impérialisme réac, il y avait une réelle idéologie. 
#Alire Chloé Gaboriaux La République en quête de citoyens.
Individualisme apolitique paysan 

Il n'y a pas tant que ça une discontinuité radicale.

##### Philosophie de la IIIe Rép
#Alire Hazareesingh Intellectual Founders of the Republic
#Alire Jean Garrigues Les Hommes Providentiels.
Éclectisme de la philosophie de la IIIe Rép.
Pas si positiviste que çà, inspirations multiples. Comme Charles Dupont-White et Étienne Vacherot et Jules Barni

Faire oublier le passé récent pour établir la légitimité.

Grande convergence entre le patriotisme bonapartiste et le patri de la III Rep.
==L'un des meilleurs chapitres==

### La Fabrique des normes
#### 19- Science et progrès, des mythes pour la République ?
#### La quasi-religion de la science en France au XIXe - 259
"C'est la foi de notre époque et c'est la bonne" proclame le Grand Dictionnaire Larousse. Cette quasi religion est réellement formée comme telle, on pense aux scientistes et positivistes en particulier. 
Il y a déjà à ce moment-là une critique qui se forme, une critique qui vise les anti-religieux qui au final .

L'historicisme se substitue au droit naturel du siècle précédent
Histoire a un sens. 'flèche du progrès'
Mutation permanente de la République = comme la science

Définitions Science et Progrès p262
Scientisme

p263 : civilisation ! et citation de Jules Ferry

#Alire Bouglé Célestin, La Démocratie devant la science
#Alire JeanMarc Bernardini Le Darwinisme social en France
#Alire Duclert Vincent, Eugénisme et Socialisme

#### 20- Le pacte fiscal est il républicain ?
#Alire plus tard
#### 21 - Compromis historique et déceptions démocratiques : la laïcité républicaine
#Alire plus tard

#### 22- Liberté, égalité, identité
Individualisme du concept de citoyen. Identification de la personne.
##### Intégration
État civil. Invention de la Révolution Identifier les enfants, même trouvés en leur donnant un nom non discriminatoire. 
Hégésippe Jean Légitimus

La presse joue un rôle important dans la promotion des grands hommes.

Importance du noms aussi sur les tombes : les "Morts pour la France".

Identifier les morts anonymes aussi ! Volonté de contrôler la mobilité des gens.
Mdr : cette idée mise peu à peu en oeuvre fait apparaître l'ambivalence des procédures d'identification : identifier le citoyen, ce n'est pas seulement le faire entrer dans la citoyenneté, c'est aussi le contrôler et potentiellement le surveiller.
État civil et casier judiciaire.
"Et du contrôle à l'exclusion, la distance est parfois très faible."

##### Exclusion
Obligation d'avoir un certificat pour pouvoir travailler.

Renforcement du contrôle sur les récidivistes et les migrants. Bertillonnage d'Alphonse Bertillon pour les criminels mais aussi les étrangers. Migrants soumis à une "surveillance préventive". Parmi eux les "nomades" tels que les Bohémiens, Tsiganes et forains.

Pour tous les étrangers en fait. Contexte d'augmentation de l'immigration et de tension autour du marché du travail et de la nationalité en 1889.
Loi de 1893 forçant à avoir un certificat d'immatriculation pour pouvoir travailler.

Travailleurs de l'empire colonial, moins nombreux. Citoyens à part, inférieur.

#Alire Le cri d'un enfant auvergnat de Toinou.
Bourgeois utilisent leur nom prénom. Ouvriers utilisent souvent des srunoms.
"Une femme mariée porte plutôt le prénom de son mari que son nom de famille, surtout quand il risque d'y avoir confusion avec une dame plus considérable."

Parle de la Bretagne !

"Zoos humains" et Clown chocolat.

"Pierre Gaxotte fustige ces "messieurs en ski, en vitch, et o, en of et en ez"".

Pas juste extérieur à la République, aussi volontaire.

##### Subversion
Divertissement tourné autour de la perte ou du changement d'identité. Les polars. 

Pseudonyme pour les métèques et les féministes. Représente donc aussi un affranchissement et une affirmation de soi.

Anarchie|Anarchiste considèrent ça comme le dressage de l'individu libre par la République qu'iels dénoncent.

La fraude parfaite est celle qui échappe à l'État, voire à l'historien

C'est Vichy qui instaure la carte d'identitée pour tous les gens au dessus de 16 ans.

#### 24- Une société de distinction : politiques de l'honneur
Légion d'honneur mérite indépendant de la classe sociale soi disant

Emulation du citoyen
Montrer l'exemple pour l'état
#Alire Godain
"Bijoux d'hommes"
Médaille de la famille -> récompense genrée

#### 25- La République des faveurs 
En principe incompatible avec la République, les aides des élus aux sphères privées continuent officieusement. À l'origine de tensions.
République = services publics. Monarchie = faveurs
Mais non....

État employeur ou accordeur de prix

Mauvaise réputation de la 3e Rep la dessus. L'un des arguments de Vichy = Démagogie.

p348 : Paternalisme utilisé !

#### 26- Les paysans au coeur de la République
Paysans conservateurs comme réserve de voix des Rép
Critique de Jean Jaurès envers les propriétaire terriens. Pour lui socialisme ni libre échangiste ni protectionniste. 

Monde dynamique : technologies, exode, crises. 
Hervé Budes de Guébriant
[Office central de Landerneau](https://bcd.bzh/becedia/fr/loffice-central-de-landerneau)

#### 27- La République des nouvelles couches aux classes moyennes
Classe moyenne d'abord comme traduction de bourgeoisie.
Nouveaux individus capables de voter
Fonctionnaires, cadres, ingénieurs, petits proprios
Mérite

#### Conclusion
#Alire Les origines républicaines de Vichy
## Références

## Liens 