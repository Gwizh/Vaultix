MOC : 
Source : [[La bataille de la Sécu]]
Date : 2023-05-20
***

## Résumé
### Préface 
La Sécu prend à sa création la moitié du budget de l'État. 
Une lecture de ce système qui n'est pas centrée sur la question du pouvoir économique manUe sa cible car l'enjeu principal est la décision du salaire et de la production. 
L'idée du "public" Est divisé en sous catégories contradictoires : l'étatisme et le réel public : lorganisation par les intéressés eux-mêmes. L'État social est ce qui survint pendant les guerres mondiales, on confond le soldat et le citoyen pour faire une guerre totale. Le public vint en 1871 ou de par la [[Résistance]]. C'est cette définition du public qu'il faut chérir. 

### Introduction 
En 1946, les pauvres sont devenus à une situation de 1860. La guerre les ont propulsés dans la faim, le froid, etc... Les tickets de rationnement continueront jusqu'en 1949. Pourtant ils n'ont pas attendus les [[Trentes Glorieuses]] pour créer ce système, ils l'ont fait des 1946, avec un budget immense. 

Les prémices de ce système ont été posés en 1789, on réfléchit alors à changer le système de charité initial, basé sur le clergé, qu'on ne veut plus avoir autant d'influence maintenant qu'il a perdu son privilège. On pense déjà alors à un système national et on se pose déjà la question des contraintes à l'initiative privée et à la paresse des bénéficiaires. Finalement, ils vont choisir de refuser de créer un système étatique et d'interdire les formes publiques (mutualistes et assurantielles). Même ensuite après l'interdiction, le capital n'investira pas ce champ car pas assez rentable. 

Une idée essentiele du livre est qu'il est tourné sur le rapport entre le conflit et les changements institutionnels. Ces changements émergent lors d'un conflit important et à cause de celui-ci. Il y a deux types de conflits, la guerre totale en préli. En 1928, c'est la première grande loi d'assurance santé obligatoire, elle a pour cause la première guerre mondiale. D'un autre côté, il y a la résistance à l'État. On comprend que le premier mène à l'État social et l'autre à la sociale. En 1946, dans la continuité avec 1871, les gens cherchaient plutôt à avoir un système auto-organisé. L'État devient social car il a peur des militants anit-capitalistes et anti-étatiques. 

Il pense comme le Haut Conseil pour l'avenir de l'assurance maladie que le problème actuel ne vient pas d'un manque de moyen financier mais d'un problème politique. Les complémentaires santé par exemple sont un exemple d'une confusion terrible qui ronge le système. 

#### Différences entre l'État social et la sociale
1. Question de justice et de délimitation. Toujours une question de ciblage. L'État veut toujours cibler pour ne pas dépenser pour rien. Cibler les gens d'abord, les plus pauvres, les plus méritants etc... Cibler aussi les maladies, les médicaments, etc... Il faut de la rentabilité. La Sociale en appel au vote des bénéficiaires pour le deuxième cas et pour le premier c'est à chacun selon ses besoins particuliers chacun selon ses capacités. Contrairement au paternalisme de l'église mais aussi de L'État, la sociale ne représente pas les bénéficiaires comme des fraudeurs.
2. Production publique. L'État veut très souvent réduire son influence sur l'institution car elle ne veut pas de production publique : nid à problèmes. Pour les régler, il faut contrôler la production pour que tout soit efficace. Le changement de 1946 a au contraire permis une libération des personnels de santé. "Le nouveau management public ne tue pas l'État social, il en est le produit."
3. Mobilisation du capital. L'État n'est jamais radical, elle a gardé le plus possible les systèmes de soins du clergé et a toujours été un allié du capital, même en 1946. 

Extrêmement lié à la démocratisation, le système de santé est crucial pour comprendre les systèmes politiques. L'État social prône le système représentatif, les citoyens ne sont pas suffisamment compétents pour traiter des questions de santé et c'est aux representants et à l'administration de définir la politique de santé. La Sociale souhaite un haut degré de décentralisation du pouvoir. Les citoyens ne sont pas seulement voyant mais aussi concepteurs de la politique de santé. En 1946, ce sont les travailles qui dirigent les caisses de sécurité sociale avec des assemblées locales. L'étatique est une modalité possible du public mais certainement pas la meilleure. 

Un bon exemple d'une réappropriation par l'État de politique auto-gestionnaire : [[La Mutualité impériale]] appropriation qui continuera pendant la [[IIIe République]]. [[Plan Juppé]] qui reprend la même idée de réappropriation par l'État mais surtout par le capital. 

Si l'État social ne fait pas davantage pour la santé des gens aujourd'hui, ce n'est pas par trahison d'une époque glorieuse d'un État attentif aux besoins de sa population. C'est le résultat d'une volonté consciente et relativement stable au cours du temps de l'État de cibler les bénéficiaires des prestations et de laisser le capital se déployer au détriment de la production publique. On peut donc se demander si lutter pour plus de protection sociale c'est uniquement lutter contre le capital ou si c'est aussi lutter contre l'État social. Lutter pour plus de oublic *et* moins d'etatique ? 

### I - Du féodalisme au capitalisme
[[Féodalité]] 
La [[Révolution Française]] n'a pas changé réellement le système de santé mais a permis de poser les bonnes questions. L'État doit-il prendre le relais de la charité et/oueee la prévoyance individuelle ? Dans quelle mesure les classes laborieuses sont-elles autorisées à décider de l'architecture du système de soin? 

Systèmes de soins durant la période féodale : gérés par le clergé dans le cadre de la charité. 
**il rentre bcp dans les détails et je note pas tout.**
Système financé par la dîme, les dons et la propriété foncière des lieux de soins. Ce système est mis à mal par l'arrivée du capitalisme. Tout d'abord, il y a de plus en plus d'exodes du fait de la paupérisation des classes les plus pauvres car il y a séparation des moyens de production (que ce soit les outils ou la terre). De plus, il y a de plus en plus une déchristianisation de la société et donc une baisse immense des dons. En plus, la dîme est supprimée et les propriétés foncières de l'Eglise sont en partie récupérées. Les hôpitaux sont de plus en plus confiés aux municipalités tenues par des bourgeois et notables. Ceux-ci sont en moyen de subvenir à ces institutions. 

[[Comité de la mendicité]] en 1790. Différence entre les bons pauvres qui sont temporairement hors du travail et les mauvais qui sont incapables ou qui ne veulent pas. 



## À lire 
L'État social : une perspective internationale. François Xavier merrien.

## Référence

## Liens 