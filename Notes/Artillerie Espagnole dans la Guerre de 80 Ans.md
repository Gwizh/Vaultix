MOC : 
Source : [[Artillerie Espagnole dans la Guerre de 80 Ans]]
Date : 2023-04-10
***

## Résumé
Entre 1568 et 1648, les [[Pays-Bas espagnols]] se révoltent contre leur tutelle espagnole. Alors même qu'ils imposent leur puissance partout ailleurs en Europe et dans le monde, les rois d'[[Espagne]] sont contraints de combattre leurs propres sujets. Caractérisée par de nombreux sièges où l'emploi de l'artillerie est primordial, la [[Guerre de 80 Ans]] affaiblit l'Espagne, au profit de la France du jeune roi [[Louis XIV]]

## Référence

## Liens 