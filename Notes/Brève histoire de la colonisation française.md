MOC : 
Source : [[Brève histoire de la colonisation française]]
Date : 2023-06-26
***

## Résumé
#### 2 Doctrines coloniales françaises
[[Paul Leroy Beaulieu]] voit deux types de colonies :
- Colonie d'exploitation : peu de colons avec beaucoup de richesses, ils vont se barricader pour ne pas vivre avec les colons
- Colonie de peuplement : colonie avec beaucoup de peuplement où on envoie l'excédent démographique. 
- Colonie mixte : Envoyer peu de gens pour ne pas finir dépeuplé. 
Exploitation, peuplement et mixité donc

[[Arthur Girault]] voit trois formes en fonction du projet politique :
- Assujetissement : exploitation et domination totale
- Autonomisation : on laisse les colons prendre autonomement les décisions
- Assimilation : colons qui ségrèguent la population pour les civiliser pour les faire arriver au même niveau politique que les colons. Toujours dirigé à la métropole, pas d'autonomie. Contrôle politique pour que ce soit une région comme Algérie et DOMTOM.

#### Néocolonisation
Quand un colonisateur et un colonisé garde des liens forts de domination même après une indépendance. On compte aussi la Chine dedans pour autant.

## Référence
#Alire Colonialisme ou Impérialisme ?, Julie D'Andurain

## Liens 
[[Colonialisme]]