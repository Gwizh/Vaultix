Titre : [[Socialisme]]
Type : Noeud
Tags : #Noeud
Ascendants :
Date : 2023-05-14
***

## Propositions
Le socialisme est une famille politique très large qui regroupe de nombreux concepts, de nombreux penseurs et qui sont ni d'accord sur le constat, les méthodes et l'idéal à atteindre. 
On peut tout de même dire que la socialisme va plus loin que le [[Républicanisme]] en voulant aussi abolir le capitalisme.
## Sources
https://www.lesbonscaracteres.com/livre/les-socialistes-en-france-de-1871-1914-tome-i
