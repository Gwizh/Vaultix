MOC : 
Source : [[Retraite par capitalisation]]
Date : 2023-04-14
***

## Résumé
La retraite par capitalisation fonctionne sur le principe de l'accumulation par les travailleurs d'un stock de [[Capital]] qui servira à financer les pensions de ces mêmes travailleurs devenus inactifs. C'est donc, par principe, un système d'[[Épargne]] Individuel basé sur l'autofinancement.

Dans le discours politique, on l'oppose fortement au concept de [[Retraite par répartition]], bien que l'opposition ne soit pas aussi absolue, ni en théorie, ni en pratique (les systèmes réels comportant généralement une part des deux).

## Référence

## Liens 