MOC : 
Source : [[Conseil national de la refondation logement]]
Date : 2023-04-23
***

## Résumé
Encadrement du foncier, « bouclier logement », banque de la rénovation énergétique : les propositions issues du Conseil national de la refondation logement – dont Mediapart publie en intégralité les travaux – veulent alimenter le débat sur une politique du logement, pour l’instant introuvable, malgré une crise inédite. 

« Si le président veut s’occuper vraiment des problèmes des Français, qu’il s’occupe du [[Logement]] ! » Le maire de Villeurbanne a été saisi d’un doute en écoutant l’allocution du président de la République lundi soir. [[Cédric Van Styvendael]], qui a présidé un groupe de travail du [[Conseil national de la refondation]] (CNR) sur « le pouvoir d’habiter », s’est en effet étranglé de ne pas l’avoir entendu prononcer une seule fois le mot « logement ».

« Si les Français ont des fins de mois difficiles, c’est parce que la moitié consacre un tiers de son budget à se loger, et parfois plus », souligne-t-il. « Même le [[MEDEF]] reconnaît que cela devient un problème économique énorme puisque cela entame le pouvoir d’achat des Français et que des employeurs n’arrivent pas à embaucher, faute de logements à proximité », poursuit l’élu.

À une situation de départ déjà très tendue – plus de 4 millions de mal-logés, 2 millions de personnes en attente d’un logement social, 330 000 sans-abri –, se sont ajoutés ces derniers mois les éléments d’une crise inédite. En plus de l’explosion des prix du foncier, la hausse des prix des matières premières et des prix de l’énergie a mis le marché de la construction à l’arrêt.

La Fondation Abbé Pierre dénonce une « année blanche » contre le mal-logement
https://www.mediapart.fr/journal/france/310123/la-fondation-abbe-pierre-denonce-une-annee-blanche-contre-le-mal-logement?utm_source=global&utm_medium=social&utm_campaign=SharingApp&xtor=CS3-5

Le rapport consacré au « pouvoir d’habiter » (à lire ici en intégralité) propose ainsi la création d’un « bouclier logement », soit une allocation modulable pour limiter le taux d’effort à 25 % pour la moitié de la population aux revenus les plus modestes. « Au moment de la réforme de 1977 sur les aides à la personne, le taux d’effort maximal était de 17 % pour les ménages. Aujourd’hui, a rappelé le dernier rapport de la fondation Abbé Pierre, on est à plus de 40 % », explique Cédric Van Styvendael, l’un des corapporteurs.

Autre préconisation importante de ce groupe : le rachat massif des passoires thermiques (classées F et G) pour les rénover et les proposer sous le régime du bail réel et solidaire (un bail où le foncier et le bâti sont dissociés, ce qui minore fortement le prix d’achat).

Parce qu’en France, « des familles vivent à l’hôtel pendant des années, pendant que les touristes occupent ponctuellement un nombre croissant de logements familiaux », note le rapport. 

L’encadrement du prix du [[Foncier]] – longtemps tabou pour une grande partie des acteurs – fait partie des mesures proposées par le groupe dédié à la production de nouveaux logements (lire l’intégralité de son rapport ici).  Aujourd’hui, les prix pratiqués et la spéculation à l’œuvre étouffent toute la chaîne de production. « Agir sur ces prix serait un signal extrêmement fort, et ambitieux, en direction de tous les acteurs, publics et privés, qui pratiquent la surenchère ou considèrent que la valorisation est inéluctable, légitime et peu partageable », note le rapport.

Catherine Sabbah, qui vient de publier [[En finir avec les idées fausses sur l’habitat]] (Éditions de l’Atelier, 2023).

Loi « anti-squat » : condamnée par l’ONU, adoptée par l’Assemblée nationale
https://www.mediapart.fr/journal/france/040423/loi-anti-squat-condamnee-par-l-onu-adoptee-par-l-assemblee-nationale?utm_source=global&utm_medium=social&utm_campaign=SharingApp&xtor=CS3-5

Toutes ces propositions, dont certaines sont franchement radicales, sont désormais sur la table du ministre délégué au logement [[Olivier Klein]] depuis des semaines, en attente d’un arbitrage politique. Son entourage confirme qu’elles sont aujourd’hui en cours d’examen.

_« S’il leur prenait l’idée saugrenue de ne pas sortir des mesures un peu ambitieuses de tout ce travail, ce serait très inquiétant »_, prévient le maire de Villeurbanne Cédric Van Styvendael.

## Référence

## Liens 