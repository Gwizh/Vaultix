MOC : 
Source : [[Levant Crisis]]
Date : 2023-07-02
***

## Résumé
The Levant Crisis, also known as the Damascus Crisis, the Syrian Crisis, or the Levant Confrontation, was a military confrontation that took place between British and French forces in Syria in May 1945 soon after the end of World War II in Europe. French troops had tried to quell nationalist protests in Syria at the continued occupation of the Levant by France. With heavy Syrian casualties, British Prime Minister [[Winston Churchill]] opposed French action and sent British forces into Syria from Transjordan with orders to fire on the French if necessary.

British armoured cars and troops then reached the Syrian capital of Damascus, following which the French were escorted and confined to their barracks. With political pressure added, the French ordered a ceasefire. The crisis almost brought Britain and France to the point of war.

## Référence

## Liens 
[[France]]
[[Royaume-Uni]]
[[Syrie]]
[[Colonialisme]]
[[Charles de Gaulle]]