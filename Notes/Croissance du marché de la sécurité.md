***
En 2018, le marché mondial de la [[Sécurité]] affichait une croissance de 7%, bien supérieure à la croissance économique mondiale de 2%, pour atteindre un [[Chiffre d'affaire]] de 629 milliards d'euro. 

Lire : "Nous avons visité Milipol, le salon de la répression" d'Emilie Massemin

Lu dans [[Une théorie féministe de la violence]]