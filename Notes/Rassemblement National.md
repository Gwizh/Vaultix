MOC : 
Source : [[Rassemblement National]]
Date : 2023-05-08
***

## Résumé


Les deux anciens trésoriers de Marine Le Pen présents à la manifestation néofasciste à Paris

Samedi 6 mai, des groupuscules néofascistes ont manifesté à Paris, en cagoules et masques noirs. [[Axel Loustau]] et [[Olivier Duguet]], les deux anciens trésoriers du microparti de Marine Le Pen, [[Jeanne]], étaient présents en marge de ce défilé. Le premier a tenté d’intimider notre photographe. 

Le premier, [actionnaire](https://www.mediapart.fr/journal/politique/041122/presidence-du-rn-derriere-la-victoire-de-jordan-bardella-la-gud-connection-en-embuscade) de l’agence de communication prestataire du RN, a tenté d’intimider notre photographe présent.

## Référence

## Liens 