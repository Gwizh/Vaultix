MOC : 
Source : [[Ennemis d'État]]
Date : 2023-06-28
***
Livre de [[Raphaël Kempf]]
## Résumé

#### Comment reconnaître une loi scélérate 
Les années 1890 sont marquées par une montée d'un [[Autoritarisme]] d'[[État]] en France avec notamment les lois de 1893 et 1894. L'[[Anarchie]] était l'ennemi type car anti pouvoir. 

Il y avait tout de même des soutiens comme [[Léon Blum]], [[Charles Peguy]], [[Bernard Lazare]]. 

##### Discours
Je note ça parce que c'est magnifique :
> Les anarchistes, Messieurs, sont des citoyens qui, dans un siècle où l'on prêche partout la liberté des opinions, ont cru de leur devoir de se recommander de la liberté illimitée [...]. Nous voulons la liberté et nous croyons son existence incompatible avec l'existence d'un pouvoir quelconque, quelles que soient son origine ou sa forme. [...] Les anarchistes se proposent donc d'apprendre au peuple à se passer comme il commence à se passer de Dieu. Il apprendra également à se passer de propriétaires. Nous voulons, en un mot, l'égalité : l'égalité de fait, comme corollaire ou plutôt comme condition primordiale de la liberté. 

Pareil :
> Gautier : Je reconnais avoir affirmé la nécessité de la Révolution et la propagande par le fait
> Le président : Qu'entendez-vous par là ?
> Gautier : J'entends que je ne crois pas que l'émancipation du prolétariat puisse s'accomplir autrement que par la force insurrectionnelle. C'est déplorable sans doute, et je suis le premier à le déplorer, mais c'est ainsi. En le constatant, je fais une simple observation de la physiologie sociale. L'histoire, en effet, est là pour nous apprendre que jamais des privilégiés - individus ou classes - n'ont volontairement abdiqué leurs privilèges et que jamais un ordre des choses n'a cédé sans combat la place à un nouveau régime. Il n'est guère probable que la bourgeoisie se montre plus accommodante que l'ancienne aristocratie dont elle a recueilli la succession. Au contraire. Rien que le procès actuel en est un témoignage significatif.

Lyon, centre des anarchistes, surtout la presse.

##### Principe des lois
En droit, se trouvait alors punie la simple affiliation à une association internationale des travailleurs dont le but est de provoquer à la suspension du travail, à l'abolition du droit de propriété, de la famille, de la patrie ou des cultes reconnus par l'État. [[AIT]] et la [[Première Internationale]]. On ne reprochait donc pas des actes mais des idées. Cela pouvait être un abonnement à des journaux ou à une participation à des réunions publiques.

##### Lois scélérates
De 1983 à 92, malgré les appels des anarchistes à la propagande par le fait, seul 4 attentats eurent lieu. Pourtant, après 1992 et la [[Fusillade de Fourmies]] et l'[[Affaire de Clichy]], les attentats reprennent.
[[Auguste Vaillant]]
[[Émile Henry (anarchiste)]]
[[Ravachol]]
[[Assassinat de Sadi Carnot]]

C'est au terme de cette période que sont signées les [[Lois scélérates]].

Le mythe veut que [[Charles Dupuy]], président de la Chambre des Députés, se contente de dire après l'attentat de Vaillant : "Messieurs, la séance continue". Cela fait croire que la République a répondu avec sang-froid mais c'est évidemment faux car elle a en effet profité de tout ça pour rédiger les [[Lois scélérates]]. Une comparaison est pourtant faite avec l'époque actuelle. En 2017, la comparaison entre le [[Terrorisme]] anarchiste et le terrorisme islamiste passe crème. [[Léon Blum]] dit même que la première loi a été passé sans même que les députés aient accès à la version imprimée du texte.

"En ce sens, ce n'est pas sans une certaine ironie que, défendant en 2014 sa nouvelle loi anti-terroriste, [[Bernard Cazeneuve]] alors ministre de l'Intérieur fait explicitement référence aux lois scélérates. " Il se sert alors d'une citation de [[Georges Clemenceau]] : "Si la République vit de la liberté, elle pourrait mourir de la répression." Et pourtant, Bernard crée une loi du même acabit pour restreindre la fameuse [[Loi du 29 juillet 1881 sur la liberté de la presse]] en ajoutant l'apologie du terrorisme dans le [[Code pénal]]. La loi de 2014 a envoyé des jeunes en prison pour des paroles en trop.

##### Violence de l'État et violence terroriste
Cette façon de parler de [[Bernard Cazeneuve]] place ces deux violences sur le même plan. Du point de vue juridique, la violence de l'État - qu'elle soit exercée par l'usage de la force ou par la privation de liberté, dans les commissariats ou prisons - n'est pas comparable avec la violence des individus qui doit être poursuivie et réprimée que selon les formes du droit.

##### La Revue blanche contre les lois scélérates
[[La Revue blanche]]. En 1898, [[Léon Blum]] écrivit dans la revue blanche "Comment ont été faites les lois scélérates ?". En 1899, [[Émile Pouget]], [[Francis de Pressensé]] et d'autres rédacteurs écrivirent sur le même sujet. [[Raphaël Kempf]] pense que si cette revue a connu autant de succès, c'est parce qu'elle dénonçait l'arbitraire de la Justice française en pleine [[Affaire Dreyfus]] et se plaçait dans le camp de tous les opprimés, anarchistes compris. C'est en partie [[Félix Fénéon]] qui a mis en place ce front commun. [[Émile Zola]] est arrêté à ce moment-là et des combats de rues s'engagent entre les deux camps de l'affaire. L'idée était de rapprocher les dreyfusards modérés des anarchistes. [[Bernard Lazare]] a été l'un des premiers dreyfusard et a même publié un mémoire, longtemps avant [[J'accuse]]. 

##### La critique de Blum
[[Léon Blum]] ne peut pas utiliser les outils que nous avons aujourd'hui pour faire retirée la loi. Aujourd'hui nous avons le [[Conseil Constitutionnel]], la Cœur européenne ou la Cour de Cassation. Lui ne pouvait que critiquer la privation des libertés individuelles. Il critique aussi le fait d'utiliser un événement récent pour faire passer des lois qui étaient prévues avant ça (l'augmentation du budget de la police par exemple). 
[[Jean Casimir-Perier]] nous laisse croire que les lois ne sont pas faites pour réprimer, il ment. 
> Il s'agit de nous permettre, en punissant l'apologie, d'empêcher, dans la mesure où les lois le peuvent, le retour d'événements aussi déplorables que ceux dont ici nous avons été les témoins. 
> 

##### Création d'un nouveau crime
Par rapport à 1810, ce n'est pas juste un rajeunissement de loi, c'est une radicalisation. Maintenant il y a l'entente, la participation à un groupe ayant la volonté du crime sans l'exécution et sans l'appartenance à une organisation. 
Fabreguettes dit même :
> On peut, en pleine réunion publique, arrêter un délinquant. 

Même à l'époque ils se rendaient compte de l'arbitraire du concept d'entente mais n'ont pas débattu plus que ça. L'idée c'était aussi de pouvoir agir contre n'importe quel autre groupe et n'importe quel mauvaise idée. 
C'est une autre composante des lois scelerates, elles sont volontairement vagues. 

Même si aujourd'hui on a ajouté le fait de commenter un acte matériel, on a aussi ajouté le fait de préparer un crime (ça c'est toujours condamnable) et puis on a toujours la notion d'entente. 

##### La loi du 28 juillet 1894 - La pire
C'était la troisième loi, la plus controversée. Elle a été adopté à pas grand chose et donc a failli faire renverser le gouvernement. 
1) Elle change la procédure pour ces crimes, en passant par les tribunaux correctionnels plutôt que la Cour d'Assise, jugée trop lente. 
2) Encore un nouveau crime : la propagande privée.

Loubat fantasme :
> À côté de la grande propagande que font publiquement les orateurs et les conférenciers de la secte, il y en a une autre moins bruyante, plus modeste, clandestine, non moins active, pratiqué par les compagnons eux-mêmes dans les conciliabules, dans ces "soirées familiales", comme ils les nomment, à l'atelier, à la porte des casernes, dans ces longues conversations par lesquelles l'ouvrier trompe l'ennui du chômage, dont l'anarchiste sait si bien tirer parti pour infiltrer ses idées dans l'esprit de ceux qui souffrent. 

[[Jean Jaurès]] parle de surveiller toutes les consciences. Car oui, l'idée c'était de vouloir savoir ce qu'il se passait dans les foyers, dans les cercle d'amis, etc... Par exemple, chanter des chants anarchistes pendant des repas de famille ou crier Mort aux bourgeois et devenu illégal via cette loi. 

3) Pouvoir envoyer à la relégation après la prison (au bagne). 
4) Interdire la retranscription des discours anarchistes (ou dissidents) dans la presse, en tout cas ceux qui iraient contre l'ordre public. Ces discours peuvent provenir par exemple de réunion publique mais aussi des tribunaux. Ce fut le cas de [[Caserio]].

Cette loi est la seul a avoir été abrogée, mais seulement en 1992.

[[Georges Clemenceau]] based : 
> Douze braves gens vont être invités à se prononcer sur le cas de [[Jean Grave]]. Il est fort à craindre qu'ils n'aient pas lu son livre et ne le jugent que sur des extraits habilement choisis. Avec un procédé pareil, il n'y a pas un livre de médecine qui ne put être condamné pour outrage à la pudeur. 

Le Figaro a évidemment fait un article sur le péril anarchiste. 

##### Antimilitarisme et [[Communisme]]
Ces lois scélérates ont aussi permit de réprimer les communistes en les amalgamant aux anarchistes. Le jeune [[Parti communiste français]] était profondément antimilitariste et pour la première fois dans l'histoire française les militaires ont du faire face à des groupes très organisés de militants dans le milieu des années 20. Parmi eux le fameux [[André Marty]] qui a participé aux [[Mutineries de la mer Noire]]. Il a été condamné aux travaux forcés. Il envoie une lettre en 1927 au [[Maréchal Foch]] pour lui dire qu'en cas d'attaque contre l'[[URSS]], il appellerait à la même mutinerie qu'en 1919. Ça lui vaut un procès.

##### Autres cas concernés
La décolonisation de l'[[Algérie]] et l'[[Affaire de Tarnac]].

##### Petit retour sur Cazeneuve
> Il ne s'agit pas en l'espace de réprimer des abus de la liberté d'expression mais de sanctionner des faits qui sont directement à l'origine des actes terroristes.

##### Reconnaître les lois scélérates
1. Adoptée en urgence 
2. Provient d'une loi antérieure qui n'avait pas été adoptée
3. Argumentation contradictoire
4. Réaction pseudo indignée qui permet de faire passer l'essentiel en oubliant le plus controversé
5. Surtout une loi pour la police
6. Faite contre certains mais appliquée à tous
7. Vise souvent plus l'intention que l'acte

Définition globale : Généralement adoptées sous le coup de l'émotion pour gérer une situation exceptionnelle, désignant des ennemis, elles donnent un pouvoir extraordinaire et parfois temporaire à l'État, à la police et au ministère public, avant de se normaliser et de cibler aussi les citoyens.

#### [[Francis de Pressensé]] -Notre loi des suspects
Règle générale : quand un régime promulgue sa loi des suspects, quand il dresse ses tables de proscription, quand il s'(abaisse à chercher d'une main fébrile dans l'arsenal des vieilles législations les armes empoisonnées, les armes à deux tranchants de la peine forte et dure, c'est qu'il est atteint dans ses œuvres vives, c'est qu'il se débat contre un mal qui ne pardonne pas, c'est qu'il a perdu non seulement la confiance des peuples, mais toute confiance en soi-même.

"Quiconque a gardé au cœur le moindre souffle du libéralisme de nos pères, quiconque voit dans la République autre chose que le marchepied de sordides ambitions, a compris que le seul moyen de préserver le modeste dépôt de nos libertés acquises, le patrimoine si peu ample de nos franchises héréditaires, c'est de poursuivre sans relâche l'œuvre de justice sociale de la Révolution."

#### [[Léon Blum]] - Comment elles ont été faites


## Référence

## Liens 