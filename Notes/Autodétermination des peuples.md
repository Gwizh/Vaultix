MOC : 
Source : [[Autodétermination des peuples]]
Date : 2023-05-02
***

## Résumé
#MIE L'un des points les plus importants car on ne doit jamais aller contre un peuple. C'est le peuple qui doit toujours choisir. C'est évidemment un problème du [[Capitalisme]] et de l'[[Impérialisme]], qui va fondamentalement contre ce concept. Tout doit être contrôlé sous le même système économique, suivre les mêmes règles. C'est la loi de la nature, le plus fort gagne. Le [[Communisme]] ne suit pas nécessairement ce concept mais est philosophiquement pour. L'invasion de l'Ukraine par l'[[URSS]] est un bon exemple et la répression des mouvements anarchistes au même moments en est la preuve. L'[[Anarchie]] est là-dessus bien meilleur car c'est philosophiquement au centre. Le mieux qu'un Etat devrait pouvoir faire serait d'aider les peuples à l'autodétermination en fournissant les moyens de le faire (militaire, financier, technologique, etc... ). Je pense là-dessus que la [[Guerre du Vietnam]] était bien plus respectueuse de ce principe. 


## Référence

## Liens 