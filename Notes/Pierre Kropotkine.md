MOC : 
Source : [[Pierre Kropotkine]]
Date : 2023-06-18
***

## Résumé
Pierre (Piotr) Alexeïevitch Kropotkine (en russe : Пётр Алексеевич Кропоткин), né le 9 décembre (27 novembre dans le calendrier russe) 1842 à Moscou et mort le 8 février 1921 à Dmitrov près de Moscou, est un géographe, explorateur, zoologiste, anthropologue, géologue et théoricien du communisme libertaire.

## Référence

## Liens 