MOC : 
Source : [[Ordolibéralisme]]
Date : 2023-04-14
***

## Résumé
L'ordolibéralisme (Ordoliberalismus en allemand) est un courant de pensée libéral selon lequel la mission économique de l'[[État]] est de créer et de maintenir un cadre normatif permettant la « concurrence libre et non faussée » entre les entreprises. L'ordolibéralisme naît à l'Université de Fribourg-en-Brisgau (en allemand : Freiburg im Breisgau) en Allemagne, à partir de 1932, de la rencontre d'un économiste, Walter Eucken et de deux juristes, Franz Böhm et Hans Grossmann-Doerth. Il est alors développé par l'« école de Fribourg » (Freiburger Schule). Ses fondamentaux économiques s'écartent à la fois du « matérialisme hédoniste » des libéraux et surtout de l'évolutionnisme du [[Marxisme]] ; en outre, cette école critique vivement la praxis économique du national-socialisme. Plusieurs des penseurs principaux de cette école (notamment Walter Eucken, Wilhelm Röpke et Alfred Müller-Armack) ont exposé leur analyse dans la revue ORDO. On parle généralement de l'« ordolibéralisme allemand ».

L’ordolibéralisme a influencé la politique économique des divers chanceliers allemands de l'après-[[Seconde Guerre mondiale]], ce qui les a auréolés de la paternité intellectuelle du « miracle économique ouest-allemand ».

## Référence

## Liens 