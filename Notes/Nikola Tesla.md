MOC : 
Source : [[Nikola Tesla]]
Date : 2023-07-21
***

## Résumé
Nikola Tesla, né le 10 juillet 1856 à Smiljan dans l'empire d'Autriche (actuelle Croatie) et mort le 7 janvier 1943 à New York, est un inventeur et ingénieur américain d'origine serbe. Il est connu pour son rôle prépondérant dans le développement et l'adoption du courant alternatif pour le transport et la distribution de l'électricité.

Tesla a d'abord travaillé dans la téléphonie et l'ingénierie électrique avant d'émigrer aux États-Unis en 1884 pour travailler avec [[Thomas Edison]] puis avec [[George Westinghouse]], qui enregistra un grand nombre de ses brevets.

Ses travaux les plus connus et les plus largement diffusés portent sur l’énergie électrique. Il a mis au point les premiers [[Alternateur|alternateurs]] permettant la naissance des réseaux électriques de distribution en [[Courant alternatif]], dont il est l’un des pionniers.

De son vivant, Tesla était renommé pour ses inventions, ainsi que pour son sens de la mise en scène, faisant de lui un archétype du « [[Savant fou]] ». Grand humaniste, qui se fixait comme objectif d'apporter gratuitement l'électricité dans les foyers et de la véhiculer sans fil, il demeura toutefois relativement méconnu pendant plusieurs décennies après sa mort, mais son œuvre entraîne un regain d'intérêt dans la culture populaire depuis les années 1990. 

##### Biographie
Nikola Tesla naît dans la nuit du 9 au 10 juillet 1856, à Smiljan, dans les confins militaires de l’empire d’Autriche. Il naît lors d'une nuit d'orage très violente. Sa grand-mère interprète cela en disant que l'enfant serait l'« enfant de la nuit ».

Nikola est le quatrième de cinq enfants. Il a trois sœurs, Milka, Angelina et Marica, et un frère aîné, Dane, qui décède après un accident de cheval alors que Nikola a sept ans.
Dane étant censé suivre son père en devenant à son tour prêtre, c'est sur Nikola que Milutin repose ses espoirs. Il donne alors à Nikola des exercices, tels que du calcul mental, répéter de longues phrases ou essayer de deviner ses pensées, tous dans un but d'améliorer son esprit critique. Sa relation compliquée avec son père provoque des obsessions étranges chez Nikola : il ne supporte pas la vue de boucles d’oreilles et de perles sur les femmes, il refuse de toucher les cheveux d'autres personnes, est dérangé par l'odeur du camphre, est obligé de compter ses pas et ==se force à faire un nombre d'actes divisible par trois sans quoi il recommence toute action.==

Peu après la mort de son frère, Tesla commence à lire dans la bibliothèque de son père, ce que ce dernier n'approuve pas. Milutin cache les bougies pour empêcher Nikola de se « ruiner les yeux », mais Tesla finit par créer ses propres bougies et continue à lire. À 12 ans, il découvre le livre Abafi de [[Miklós Jósika]], qui raconte l'histoire d'un « jeune homme absorbé par la débauche et l'amour du plaisir, qui, par la fermeté de sa volonté et l'énergie de sa résolution, s'exalte pour devenir l'un des héros les plus respectés et les plus exemplaires de son pays, que l'inflexibilité des objectifs peut surmonter en tout ». Ce livre est une révélation pour Tesla et il lui permet d'enfin prendre contrôle de ses émotions : « En peu de temps, j'ai vaincu ma faiblesse et j'ai ressenti un plaisir que je n'avais jamais connu auparavant, celui de faire ce que je voulais ».

#Adam Proche de Horvath avec l'arrogance en moins

## Référence

## Liens 