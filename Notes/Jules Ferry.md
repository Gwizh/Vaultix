MOC : 
Source : [[Jules Ferry]]
Date : 2023-06-09
***

## Résumé
En 1870, après la chute du [[Second Empire]], dont il était opposant, il est membre du gouvernement provisoire et, pour quelques mois, maire de Paris. C'est dans le cadre de cette dernière fonction qu'il contribue à réprimer les insurgés de la [[Commune de Paris]]

Il est président du Conseil des ministres de 1880 à 1881 et de 1883 à 1885, sous la présidence de [[Jules Grévy]]. Montrant un fort engagement pour l'expansion coloniale française, en particulier dans la péninsule indochinoise, il doit quitter la tête du gouvernement en 1885 en raison de l'affaire du Tonkin. Il se présente ensuite à l’élection présidentielle de 1887, lors de laquelle Sadi Carnot lui est préféré.

Figure du républicanisme de la [[IIIe République]], il incarne vraiment cette idée paternaliste et universaliste du pouvoir, de l'éducation au [[Colonialisme|colonialisme]].

Je cite la chronologie YIKES :
_L'autre grande oeuvre de Jules Ferry est l'expansion coloniale. C'est seulement après son arrivée aux affaiers en 1880 qu'il découvre les possibilités qu'elle offre de rendre à la France son rang international, lui ouvrir de nouveaux marchés et accessoirement, lui permettre de renouer avec l'universalité de la mission qui est la sienne depuis la [[Révolution Française]]. La création d'un vaste empire colonial en Extrême-Orient et en Afrique est une entreprise patriotique, républicaine et humanitaire. La colonisation est la gloire de l'époque, et celle de Ferry, d'ailleurs pas toujours apprécié à l'époque._

### Jules Ferry - Mona Ozouf
#### 3 - Diagnostic et ordonnance
##### Amour de la province mais désaveu des régionalismes
Il voit Paris comme le centre de la Révolution, le progressisme qui va trop loin. La province est la vraie France, la France éternelle. La région de Bordeaux notamment est la plus républicaine selon lui. Mais il fait abstraction selon elle des régions parce que ça ne représente rien pour lui. Il parle de l'infiniment petit des villages et de la grandeur de l'État mais pas du niveau intermédiaire.

##### Invisibilisation de la lutte des classes
Pour lui, dans le Tiers État, il n'y a pas de frontières. Les bourgeois sont selon lui les contentés de la société, l'"élite des travailleurs de toutes classes". Si limite il y a, il appelle tout le monde à la franchir. Pour lui la question sociale est l'inégalité d'instruction et non l'inégalité de conditions. Les avancées sociales pour les syndicats, les libertés, les clubs etc... permettront d'apporter la paix sociale.

##### Féminisme et révolution
"L'âme de la République est féminine". C'est la part de la Révolution de 89 que la France a perdu. Il faut reconcilier cette fracture par la paix. En gros, comme pour tout il est pour la réconciliation pour enfin arrêter l'affrontement politique.

#### 4 - Refaire : la France comme patrie morale
##### Suffrage universel vs ignorance
Il faut selon lui faire des citoyens éclairés, aguerris et conscients, capables de résister à la fatalité qui emporte le régime républicain vers l'émeute, puis le césarisme, aiguilles d'une même horloge. 
#MIE C'est vraiment le discours de Borne jpp.

##### Éducation gratuite, obligatoire et laïque
"La gratuité répare cette séparation scandaleuse : le but est de méler sur les bancs de la classe ceux qui seront mélés sous les drapeaux de la patrie". Les conservateurs lui reprochent de créer des divisions dans les villages ente le publique et le religieux. Ferry veut constituer l'enseignement primaire selon la doctrine positiviste. L'éducation était nationaliste et républicaine, il fallait justifier le régime actuel et créer l'unité.
#Alire Quelques mots sur l'instruction publique en France, 1873.
L'enseignement était tel qu'on glorifiait le passé, même d'avant la révolution et jusqu'à l'antiquité gréco-romaine (?). Il décrit tout le cycle comme une marche continue. Il dit que l'ancienne France avait préparé la Révolution, que celle-ci n'était qu'un dénouement logique. 
"Tout, dans ce récit bien ordonné, doit faire comprendre à l'enfant que l'histoire de France menait à la République, donc à l'École, donc à lui, le petit écolier républicain, grandi de tout ce qui l'a précédé, prêt à entrer à son tour dans l'aventure collective."

##### [[Loi de 1884 sur l'organisation municipale]]
Très important pour lui pour créer l'unité républicaine dans le nationalisme français. Ce n'est pas pour autant un don d'autonomie, au contraire, c'est donner du pouvoir au maires républicains.

#### 5 Refaire : la France comme grande puissance
##### Ferry et la colonisation
"Droit et devoir des races supérieures de civiliser les races inférieures." Il utilise le mot de civilisation pour faire passer l'humanité du sauvage au barbare puis du barbare au civilisé. "Droit des races supérieures d'aller chez les barbares". La [[Colonialisme|colonisation]] était aussi un moyen de se détourner des conflits européen.
Opposants d'ED : "Vous m'enlevez deux enfants et vous m'offrez vingt domestiques."
Pour lui, c'est pour retrouver la puissance industrielle de la France et ne pas tomber sous le [[Royaume-Uni]] et les [[États-Unis]]. Il appelle les écoles qu'il a construit en [[Algérie]] ses filles.
"Convaincu que la bourgeoisie républicaine devait, pour avoir joui des rpvilèges de la culture et de l'éducation, acquitter sa dette envers les déshérités de la démocratie française, il pensait qu'elle devait aussi l'acquitter envers les populations barbares".
Mona Ozouf est raciste elle-même, nice. Et elle met [[Robespierre]] et [[Napoléon III]] sur le même plan

#### 6 - Parfaire : les institutions
##### Constitution de 1875
Reposer sur trois pouvoirs : Sénat, Chambre et présidence combinés pour assurer l'équilibre d'une république "conservatrice", celle qui est capable de compter avec cette autre trinité : "le temps, la tradition, les intérêts". Puis, d'amortir les bourrasques toujours prévisibles du suffrage universel. Il était partisan de rendre la république définitive à la France. Il voulait réduire la puissance parlementaire en défendant le droit de dissolution pour regagner une majorité.

#### 7 - Entre unité et liberté
Positiviste et libéral. Deux allégeances qui se combattent, deux vocables qui jurent ensemble. Pour le positiviste, rien n'est plus précieux que l'unité spirituelle de la nation, garantie l'autorité de la science et l'universalité de la morale. Pour le libéral, héritier des Lumières, confiant dans la valeur de l'être individuel, la liberté est principielle. Or, comment imaginer construire l'unité sur la liberté? L'exercice de la liberté est source de dissidence individuelle et de désordre collectif. Tel est, tout au long de la vie, le dilemme de Ferry.

## Référence
https://gallica.bnf.fr/ark:/12148/bpt6k57727049

## Liens 
[[Affaire du Tonkin]]