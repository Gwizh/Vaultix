MOC : 
Source : [[Mépris de classe]]
Date : 2023-04-24
***

## Résumé
Lire le livre mépris de classe l'exercer, y faire face. 

Sociologie des émotions de classes. Liste des affects associés. Forme première : l'indifférence. On est remis à sa place dans la hiérarchie sociale. Ethnographe. *Le savant et le populaire*La plupart du temps on a pas besoin de l'utiliser explicitement, il est là dans la structure. C'est lorsque les situations deviennent plus spécifiques et tordues que le mépris apparaît directement. Faire de la sociologie ce n'est pas mépriser les méprisants. Berales

## Référence

## Liens 