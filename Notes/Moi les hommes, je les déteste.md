MOC : 
Source : [[Moi les hommes, je les déteste]]
Date : 2023-06-10
***

Livre de [[Pauline Harmange]]

## Résumé
Moi les hommes, je les déteste est un essai de l'autrice féministe française Pauline Harmange. Il paraît le 19 août 2020 aux éditions Monstrograph, et est initialement imprimé à 450 exemplaires, puis à 500 exemplaires supplémentaires. Une menace de censure d’un chargé de mission au ministère français de l'Égalité femmes-hommes le jour de sa sortie attire l’attention et crée une publicité involontaire pour l’ouvrage.

#### Préface de [[Chloé Delaume]]
Définition des termes : [[Misogynie]], [[Misandrie]], [[Phallocratie]] et comme en réalité les femmes qui sont misandres le sont dans le sens plus large d'anti-patriarcat, antisystème et non comme antonyme de la misogynie qui voit les femmes comme un "réceptacle à foutre, une créature fragile".

J'aime cette phrase : une volonté de contrer, déserter, annihiler et ghoster le patriarcat. 

> "Car la solidarité des Femmes n'est jamais frivole, elle est toujours politique."

#### Premières pages
Critiquer les féministes et lesbiennes de misandre est une critique facile. C'est un mécanisme de silenciation de la colère, des luttes. C'est un moyen d'opposer un sexisme à un supposé autre. La misandrie est objectivement mauvaise et faire l'amalgame peut donc prouver un extrémisme qui va trop loin, une radicalité néfaste. Pourtant Pauline assume cette extrémisme, malgré le fait qu'elle ait épousé un homme.

> "Quel genre de femmes sommes-nous si nous nous soustrayons au regard des hommes ? Au choix : des mal-baisées, des lesbiennes, ou bien des hystériques."

La misandrie supposée de femmes est détestée par les hommes car il (on) pense qu'on ne pourra pas accompagner les luttes si on nous accueille en nous disant que nous sommes des merdes. Mais les Femmes non pas besoin qu'on les accompagne dans leurs luttes, elles l'ont fait toutes seules depuis des siècles. 

#MIE [[Autodétermination des peuples]], des gens en général :)

#### Misandrie, nom féminin
Elle le définit comme un sentiment négatif envers la gent masculine dans son ensemble. Un sentiment qui peut aller partout sur le spectre entre la méfiance et l'hostilité, qui se manifeste la plupart du temps par une impatience envers les hommes et un rejet de leur présence dans les cercles féminins. Cette gent masculine correspond aux hommes cisgenres qui ont été sociabilisés comme tels.

> Si on continue à surveiller même les mecs qu'on trouve corrects, c'est parce que tout le monde dérape, et les mecs blancs cis hétérosexuels, riches et valides sont encore plus susceptibles de le faire que les autres. La somme de leurs privilèges est si lourde qu'elle les pousse à l'immobilisme. 

Elle ne croit pas aux hommes féministes car les efforts de ceux-ci est si misérable face à la difficulté d'être une femme dans le patriarcat. 

> On demande aux hommes de rester à leur place. Non, en fait, on exige d'eux qu'ils en prennent moins. Ils n'ont pas le premier rôle et il va falloir s'y faire.

#### Masquée avec un mec
Elle a commencé sa relation à 17 ans, elle ne connaissait pas grand chose au féminisme et son mec encore moins. Elle a appris à l'aimer et se sont déconstruits ensemble. Pourtant, cette dynamique venait quasiment systématiquement d'elle. Lui ne faisait pas assez d'efforts, il n'allait jamais plus loin.

#### Misandres hystériques et mal-baisées
Les hommes qui ne supportent pas d'être détestés ne valent pas la peine d'être acceptés dans le féminisme. On a le droit d'assumer la misandrie comme elle nous éloigne de facto du rôle docile des femmes.  

#### Les hommes qui n'aiment pas les femmes
Pas besoin de se proclamer misandre pour avoir l'air de l'être. Dire "les hommes" au lieu de "certains hommes". La misogynie et la misandrie, bien qu'opposé, n'ont pas du tout les mêmes réalités. L'un harcèle, viol, tue, l'autre ? Rien. 

Elle rappelle des chiffres :
- En 2017, 90% des personnes ayant reçu des menaces de mort de la part de leur conjoint étaient des femmes. 86% des victimes de meurtres par le conjoint ou l'ex-conjoint étaient des femmes.
- En parallèle, sur les 16 femmes qui avaient tué leur conjoint, au moins 11 étaient victimes de violences conjugales
- En 2019, 149 femmes sont mortes assassinées par leur conjoint ou ex.

La violence envers les hommes existent évidemment ([[Une théorie féministe de la violence]]). Il y a cette idée sexiste qu'un homme est toujours partant pour le sexe et qu'un viol ne peut arriver.

> "Sexiste, moi ? À la maison, j'ai une femme, deux filles, deux chattes et vingt poules. Je n'ai que des femelles à la maison. - Philippe Fasan"

La misandrie est généralement solitaire ou en tout cas inoffensive. 

> "Notre misandrie fait peur aux hommes, parce qu'elle est le signe qu'ils vont devoir commencer à mériter notre attention."

#### Que rugisse la colère des femmes
On n'apprend pas aux femmes à être en colère et encore moins envers les hommes. Les hommes par contre, doivent préférer la colère à la tristesse, il ne faut pas pleurer mais se battre. Elle parle d'une expérience perso du collège. Elle s'est faite giflée par une autre fille devant tout le monde et jamais on lui a dit de répliquer, de gifler en retour. Il lui fallait plutôt oublier ce qu'il venait de se passer. 

> "Les modèles qu'on inculque sont nocifs dans les deux cas : ni la violence qu'on encourage chez les garçons ni la passivité qu'on impose aux filles ne sont des réponses appropriées, pou soi comme pour les autres, dans les situations d'injustice ou de conflit. [...] Je n'ai découvert la colère que plus tard, en devenant féministe."

Les conflits dans les couples sont inégaux. Si une femme pleure ou témoigne un désespoir, elle sera perçue comme émotive. Si une femme montre au contraire la colère en voulant changer les choses, elle sera perçue comme violente voire folle. La femme peut difficilement faire part d'un problème dans un couple sans passer par des préjugés sexistes qui vont la décrédibiliser. 

#Alire Libérer la colère, Genièvre Morand et Nathalie-Ann Roy 

#### Médiocre comme un homme
Les hommes prennent beaucoup de place, dire qu'on peut s'en passer leur fait peur et semble même impossible. Les hommes pensent qu'on ne peut rien faire sans leurs opinions.

> "Aie la confiance d'un homme médiocre - Sarah Hagi"

#### Le piège de l'hétérosexualité
La mariage et l'enfant n'est pas la source de bonheur principale. Une femme célibataire sans enfant a comme cliché d'avoir raté sa vie, pourquoi ? Il faut sortir de cette idée de toujours avoir es relations hétéro et de faire des enfants, on fait ce qu'on veut.
Et même si on est en couple, il faut garder de bons amis, garder des activités en dehors du couple.

#### Sœurs
[[Sororité]], un concept super important pour que les femmes puissent se sentir à l'aise pour évoquer leurs problèmes dans les relations. Les hommes ne peuvent remplir se rôle, on veut toujours trouver des solutions, être des sauveurs. Il faut souvent juste écouter, croire, ressentir de l'empathie. De plus, elle consomme plus d'oeuvres féminines car c'est là où elle trouve du confort.

#### Éloge des réunions Tupperware, des soirées pyjama et de nos girls' club
La [[Non-mixité]] est un concept que les hommes détestent. 
Exclure les hommes des activités, des rassemblements, n'est pas misandre. Ça permet de cultiver un lieu où enfin les hommes n'ont pas connaissance. Un point intéressant par ailleurs : les femmes peuvent parler de cuisine, de couture et de fringues et se réapproprier les clichés dans l'autre sens. 

#### Postface
Elle dit que ce n'était pas une censure à proprement parler, c'était plus une intimidation d'un fonctionnaire du ministère qui a envoyé un mail à la maison d'édition. Ça a été relayé par la presse nationale et internationale qui s'est offusqué qu'on interdise un livre pour son titre. Ça a provoqué un immense [[Effet Streisand]]. Elle a ensuite reçu une vague de haine sur internet ou sur les médias. Des hommes qui la menaçaient de viols ou de violences en tout genre, prenant des photos pour la faire passer pour Hitler par exemple. Elle l'analyse comme la réelle [[Cancel Culture]], rien n'est plus dur que d'endurer de telles insultes tous les jours, dans 18 langues. 

## Référence

## Liens 