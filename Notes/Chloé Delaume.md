MOC : 
Source : [[Chloé Delaume]]
Date : 2023-06-10
***

## Résumé
Chloé Delaume, née Nathalie Dalain à Versailles le 10 mars 1973, est une écrivaine française. Elle est également éditrice et, de manière plus ponctuelle, performeuse, musicienne et chanteuse. Son œuvre littéraire, pour l'essentiel autobiographique, est centrée sur la pratique de la littérature expérimentale et la problématique de l'autofiction.

## Référence

## Liens 