MOC : 
Source : [[British Insulated Callender's Cables]]
Date : 2023-04-21
***

#Adam
## Résumé
**British Insulated Callender's Cables (BICC)** was a 20th-century British cable manufacturer and construction company, now renamed after its former subsidiary [Balfour Beatty](https://en.wikipedia.org/wiki/Balfour_Beatty "Balfour Beatty"). It was formed from the merger of two long established cable firms, Callender's Cable & Construction Company and British Insulated Cables.

British Insulated Cables was founded as the British Insulated Wire Company at Prescot, near Liverpool in 1890. It bought the rights to a paper-insulated power cable capable of transmitting electricity at 10,000 volts, for use at [[Centrale électrique de Deptford]], from [[Sebastian Ziani de Ferranti]]. It went on to acquire the Telegraph Manufacturing Company in 1902 and was renamed British Insulated Cables in 1925.

Voir p59 du bouquin sur Ferranti.

## Référence

## Liens 