MOC : 
Source : [[Marxisme]]
Date : 2023-05-14
***

## Résumé
Fait parti du [[Notes/Socialisme]] mais se veut scientifique, contrairement aux socialisme utopique. Son analyse de l'économie et de l'histoire s'appelle le matérialisme historique et consiste en l'explication de ces concepts par des rapports socio-économiques entre des classes sociales. 

La définition du [[Capitalisme]] pourle [[Marxisme]] est pour lui la propriété privée des moyens de production qui crée la [[Lutte des classes]]. 

Il ne donne jamais de monde idéal et le peu de fois où il le fait il cite une phrase qui vient d'une personne avant lui à savoir : "De chacun selon ses moyens, à chacun selon ses besoins." 

Pour la méthode, il dit qu'un état doit d'abord renverser la monarchie ou l'empire pour arriver à une démocratie bourgeoise, sorte d'étape intermédiaire nécessaire pour arriver à la révolution du prolétariat et à la [[Dictature du prolétariat]] (qu'il appele [[Notes/Socialisme]] mdr quel bordel). [[Vladimir Lénine]] pense au contraire qu'on peut aller directement d'un empire au communisme, il le démontrera avec le [[Bolcheviks]]. ^4a1e9c

La main-mise sur l'État vient avec une nationalisation de tous les moyens de production et il est pour le [[Collectivisme]].  

## Spécificités du Marxisme
La première grande spécificité vient avec la [[Première Internationale]] où il y a une rupture entre [[Karl Marx]] et [[Mikhaïl Bakounine]] et donc entre le [[Communisme]] et l'[[Anarchie]]. Ces derniers critiquent la façon autoritaire et hiérarchique que Marx a de gérer l'événement. Marx dit alors que l'anarchisme est le monde idéal des socialistes mais lui pense que ça ne peut venir qu'après la dictature du prolétariat. Marx critique qu'appliquer l'anarchisme dans l'international c'est aller trop vite, et dans un monde hiérarchisé, il faut de l'ordre chez les travailleurs pour aller de l'avant. Marx critique l'abstention chez les anarchistes, lui ne souhaite pas gagner les élections mais pense que ceux-ci sont un bon moyen de se faire connaître et d'augmenter l'agitation populaire.

## Référence

## Liens 