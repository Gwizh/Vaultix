[[Article]]
MOC : [[ECONOMIE]]
Titre : [[Tesla rattrapé par la crise de l'automobile]]
Auteur : [[Les Echos]]
Date : 2022-07-03
Lien : https://www.lesechos.fr/industrie-services/automobile/tesla-rattrape-par-la-crise-de-lautomobile-1771797
Fichier : 
***

## Résumé
Perturbées par des problèmes d'approvisionnement et des fermetures d'usines, les livraisons du constructeur reculent au dernier trimestre, une première en deux ans.

Deux ans d'une progression linéaire, et puis la panne. Handicapée par des problèmes d'approvisionnement et par des fermetures d'usines, la production de Tesla a calé au cours des trois derniers mois, avec un ralentissement des livraisons plus prononcé qu'attendu au terme de ce second trimestre « très dur », de l'aveu même d'Elon Musk. Après s'être joué de la crise de l'automobile, voilà le roi de la voiture à batterie - un peu - rattrapé par les contraintes auxquelles fait face l'industrie depuis des mois.

Sans entrer dans les détails, Tesla se justifie en évoquant « les problèmes persistants de chaînes d'approvisionnement et les fermetures d'usines, des éléments hors de notre contrôle ». La contrainte la plus tangible reste la fermeture pendant plusieurs semaines, pour cause de confinement, de la gigafactory de Shanghai, la plus productive : 52 % (484.130 unités) des voitures livrées en 2021 y ont été produites, à destination des marchés asiatiques et européens.

## Référence
[[Automobile]]
[[Tesla]]


## Liens 