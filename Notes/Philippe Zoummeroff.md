MOC : 
Titre : [[Philippe Zoummeroff]]
Date : 2023-09-15
***

## Résumé
Auteur de [[Thomas Edison, L'homme aux 1093 brevets]]
Industriuel et ingénieur (1930-2020), fut aussi l'un des collectionneurs et mécènes les plus actifs de France et a constitué tout au long de sa vie plusieurs ensembles patrimoniaux de grande importance, dont la collection de 100 000 disques anciens d'opéras est l'une des plus spectaculaiires. Depuis de longues années, Philippe se penchait sur la vie de l'oeuvre de [[Thomas Edison]]. Son ultime ouvrage est préfacé par Thierry Grillet, écrivain et essayiste.
## Références

## Liens 