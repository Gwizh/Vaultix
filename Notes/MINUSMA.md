MOC : 
Source : [[MINUSMA]]
Date : 2023-07-03
***
https://www.youtube.com/watch?v=NtNRvm9-SN4
## Résumé
La mission multidimensionnelle intégrée des Nations unies pour la stabilisation au [[Mali]] (MINUSMA) est une opération de maintien de la paix des Nations unies au Mali. Elle intervient dans le cadre de la guerre du Mali et est la composante principale de l'intervention militaire au Mali. 

## Référence

## Liens 