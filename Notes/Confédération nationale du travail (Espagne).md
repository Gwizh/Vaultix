MOC : 
Source : [[Confédération nationale du travail (Espagne)]]
Date : 2023-06-17
***

## Résumé
La Confédération nationale du travail (Confederación Nacional del Trabajo ou CNT) est une organisation anarcho-syndicaliste fondée en 1910 à Barcelone (Catalogne, Espagne).

Elle est à l'origine, le produit des compromis politiques entre différents groupes, collectifs et forces sociales, synthétisés dans la notion de communisme libertaire qu’il s’agit d’instaurer selon les principes de l’anarcho-syndicalisme.

La CNT est à la fois une structure syndicale de masse portée à l'amélioration de la condition ouvrière et au contractualisme, et en même temps, une organisation révolutionnaire de rupture politique radicale, facilement gagnée par le spontanéisme révolutionnaire.

## Référence

## Liens 