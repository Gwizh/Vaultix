MOC : 
Source : [[La Forme-Commune]]
Date : 2023-07-23
***

## Résumé
> « L’espace-temps de la forme-Commune s’ancre dans l’art et l’organisation de la vie quotidienne et dans une prise en charge collective et individuelle des moyens de subsistance. Il suppose donc une intervention éminemment pragmatique dans l’ici et maintenant et un engagement à travailler avec les ingrédients du moment présent. »

Elle raconte les événements un peu oubliés survenus à Nantes en mai et juin 1968 : « la création d’une sorte d’administration parallèle pour satisfaire autrement et très concrètement les besoins fondamentaux de la ville » en réponse à l’effondrement des services publics pendant la grève générale. Un « Comité central de grève », sorte de gouvernement populaire, est installé à la mairie et contribue à la coordination du ravitaillement et à d’autres opérations.
[[Mai 68]]

Pour elle relève, « la vraie guerre du capital, c’est celle contre la subsistance », contre la paysannerie dans le monde entier. « La subsistance est orientée vers la valeur intrinsèque et l’intérêt des petits producteurs, artisans et paysans. Elle entraîne la création progressive d’un tissu de solidarités vécues et d’une vie sociale bâties sur des échanges de services, des coopératives informelles, la coopération et l’association. »

## Référence

## Liens 
[[Commune de Paris]]
![[Commune de Paris]]