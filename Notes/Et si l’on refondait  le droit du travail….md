[[Article]]
MOC : [[ECONOMIE]]
Titre : [[Et si l’on refondait  le droit du travail…]]
Auteur : [[Le Monde Diplomatique]]
Date : 2022-07-11
Lien : https://www.monde-diplomatique.fr/2017/10/SUPIOT/58009#tout-en-haut
Fichier : 
***

## Résumé
Il faudrait être aveugle pour disconvenir de la nécessité d’une profonde réforme du droit du travail. Toujours, dans l’histoire de l’humanité, les mutations techniques ont entraîné une refonte des institutions. Ce fut le cas des précédentes révolutions industrielles, qui, après avoir bouleversé l’ordre ancien du monde en ouvrant les vannes de la prolétarisation, de la [[Colonialisme|colonisation]] et de l’industrialisation de la guerre et des massacres, entraînèrent la refonte des institutions internationales et l’invention de l’État social. La période de paix intérieure et de prospérité qu’ont connue les pays européens après guerre est à mettre au crédit de cette nouvelle figure de l’État et des trois piliers sur lesquels il reposait : des services publics intègres et efficaces, une Sécurité sociale étendue à toute la population et un droit du travail attachant à l’emploi un statut garantissant aux salariés un minimum de protection.

Nées de la seconde révolution industrielle, ces institutions sont aujourd’hui déstabilisées et remises en cause. Elles le sont par les politiques néolibérales, qui entretiennent une course internationale au moins-disant social, fiscal et écologique. Elles le sont aussi par la révolution informatique, qui fait passer le monde du travail de l’âge de la main-d’œuvre à celui du « cerveau d’œuvre ([1](https://www.monde-diplomatique.fr/2017/10/SUPIOT/58009#nb1)) », c’est-à-dire du travailleur « branché » : on n’attend plus qu’il obéisse mécaniquement à des ordres, mais on exige qu’il réalise les objectifs assignés en réagissant en temps réel aux signaux qui lui parviennent. Ces facteurs politiques et techniques se conjuguent en pratique. Il ne faut cependant pas les confondre, car le néolibéralisme est un choix politique réversible tandis que la révolution informatique est un fait irréversible, susceptible de servir des fins politiques 
différentes.


Mais la révolution informatique s’avère aussi source de dangers nouveaux si, plutôt que mettre ainsi les ordinateurs au service des hommes, on cherche à organiser le travail des hommes sur le modèle de celui des ordinateurs. Au lieu que la subordination laisse place à plus d’autonomie, elle prend alors la forme d’une gouvernance par les nombres ([5](https://www.monde-diplomatique.fr/2017/10/SUPIOT/58009#nb5)), qui étend aux cerveaux l’emprise que le taylorisme exerçait seulement sur les corps.

En principe, le droit est à la vie civile ce que nos maisons sont à notre vie matérielle : un cadre ferme et stable, avec ses murs, son toit, ses portes et fenêtres, ses pièces aux fonctions différenciées. Mais l’indexer en temps réel sur des calculs d’utilité revient à lui ôter toute stabilité, comme une maison maudite dont les murs seraient mous, les moquettes colleraient aux pieds, les plafonds s’affaisseraient, les fenêtres et les portes changeraient chaque jour de place. Celui qui se trouverait piégé dans un tel édifice serait naturellement tenté de l’abattre, à la grande satisfaction du mauvais génie qui lui aurait jeté pareil sort.

Et de fait, les grands simplificateurs qui crient aujourd’hui haro sur le code du travail sont ceux-là mêmes qui, année après année, s’acharnent à l’alourdir et le compliquer. Ils n’attendent même plus que l’encre de la dernière loi soit sèche pour entamer la rédaction de la suivante. Le gouvernement s’étant privé de tous les grands leviers macroéconomiques susceptibles de peser sur l’emploi (maîtrise de la monnaie, contrôle des frontières du commerce, taux de change, dépense publique), il s’agrippe frénétiquement à celui qui lui reste entre les mains : le droit du travail, présenté comme un obstacle à l’embauche. Aucune étude sérieuse ne confirme cet argument.


## Référence

## Liens 