MOC : 
Source : [[Horlogerie, ordre et anarchisme]]
Date : 2023-06-08
***

## Résumé
Il y a plus d’un siècle et demi, les travailleurs de l’industrie horlogère suisse contribuèrent à l’essor de l’anarchisme en tant que courant politique révolutionnaire. Organisés, rompus aux combats sociaux, conscients de la réalité économique mondiale et pionniers en matière d’entraide ouvrière, ils influencèrent des mouvements antiautoritaires à travers toute l’Europe.

## Référence

## Liens 
https://www.monde-diplomatique.fr/2023/06/EITEL/65821