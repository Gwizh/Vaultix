MOC : [[Sociologie des techniques]]
Type : Note littéraire
Titre : [[Jonathan Blow - Preventing the Collapse of Civilization]]
Date : 2023-11-08
Tags : #Fini 
***

##[[Nous perdons les techniques au fil du temps]]

##[[Une société peut se reposer sur des ressources et techniques clés puis les perdre]]

##[[La complexité accélère la perte du savoir]] 

##[[Les techniques se complexifie par défaut, nous devons simplifier]]

## Référence
[[@Jonathan Blow - Preventing the Collapse of Civilization (English only),]]