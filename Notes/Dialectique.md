MOC : 
Source : [[Dialectique]]
Date : 2023-06-10
***

## Résumé
Du grec dialegein (de legein, parler et dia à travers). "Parler l'un avec l'autre."
Sens strict : Art de la discussion, ou technique du dialogue en vue d'atteindre le vrai, par questions et réponses. 
Sens large : Interaction dynamique et féconde entre éléments opposés, qu'il s'agisse de faits ou de pensées. 

## Référence

## Liens 