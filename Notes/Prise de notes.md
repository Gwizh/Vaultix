MOC : [[Notes/Prise de notes]]
Type : Note permanente
Titre : [[Notes/Prise de notes]]
Date : 2023-11-08
***

[[Zettelkasten]]

### Différentes notes
Notes éphémères, littéraires et permanentes

### Workflow
[[Noeuds/Prise de notes]]
## Utilisation de Zotero
[[Zotero]]

## Références
[[@Understanding note-taking _ Zettelkasten,]]
[[@Introduction to the Zettelkasten Method, sascha]]
[[@Prendre de bonnes notes dans OBSIDIAN - ZETTELKASTEN dans obsidian,]]
[[@Understanding note-taking _ Zettelkasten - YouTube,]]
[[@Taking effective notes_ the Zettelkasten method,]]
[[@An Interactive Introduction to Zettelkasten,]]
[[@How to make atomic literature notes using the Zettelkasten method,]]
