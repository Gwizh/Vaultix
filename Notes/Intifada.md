MOC : 
Source : [[Intifada]]
Date : 2023-06-11
***

## Résumé
Une intifada (de l'arabe انتفاضة [intifaːdˁa] litt. « soulèvement ») désigne en général une révolte contre un régime oppresseur ou un ennemi étranger. Le terme est le plus souvent utilisé pour désigner deux forts mouvements d'opposition populaire contre l'armée israélienne présente dans les territoires occupés et dans certaines zones dévolues à l'Autorité palestinienne (bande de Gaza et [[Cisjordanie]]). C'est un fait majeur depuis l'indépendance de l'Etat d'[[Israël]], mais aussi employé à plusieurs reprises au [[Liban]] mais également en [[Irak]], en [[Algérie]], en [[Tunisie]] (lors de la révolution tunisienne de 2010-2011 notamment), au Sahara occidental (les camps de Gdeim Izik3 en 2010), ou au Maroc.

La [[Première intifada]], appelée guerre des pierres, a débuté le 9 décembre 1987 et a fini en 1993.
La seconde intifada palestinienne, également appelée « Intifada Al-Aqsa », commence le 29 septembre 2000, au lendemain de la visite d'Ariel Sharon sur l'esplanade des Mosquées à Jérusalem.

## Référence

## Liens 