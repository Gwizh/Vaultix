MOC : 
Source : [[United Auto Workers]]
Date : 2023-06-18
***

## Résumé
United Auto Workers (UAW) (nom officiel : United Automobile, Aerospace & Agricultural Implement Workers of America International Union) est un des plus importants syndicats de travailleurs d'Amérique du Nord avec environ 700 000 membres aux États-Unis, Canada et Porto Rico, organisés dans environ 950 sections. Elle est affiliée à IndustriALL global union et à l'American Federation of Labour - Congress of Industrials Organisations.


## Référence

## Liens 