MOC : 
Source : [[Grande révolte syrienne]]
Date : 2023-07-02
***

## Résumé
La révolte druze de 1925-1927, appelée plus tard révolution syrienne, ou révolution nationale, ou en arabe grande révolte syrienne (الثورة السورية الكبرى, alththawrat alssuriat alkubraa), est la plus importante révolte ayant eu lieu contre le pouvoir français sur le territoire de l'actuelle [[Syrie]]. Menée par Sultan al-Atrach, elle a éclaté au Djébel el-Druze (aujourd'hui Jabal al-Arab) pour se propager vers Damas, Qalamoun, Hama, au Golan et dans le Sud-Est du Liban. La répression par les forces françaises a été sanglante, causant la mort de près de 10 000 Syriens, pour la plupart des civils ; la révolte a également coûté la vie à près de 4000 soldats de l'armée française1,2 (pour la plupart des Africains).

La révolte des druzes constitue une des quatre rébellions anticoloniales dirigées contre la France dans l'entre-deux guerres, avec la guerre du Rif au nord du Maroc ; la mutinerie de Yen Bay en Indochine, qui a eu des répercussions sur le nord de l'Annam et quatre provinces du Tonkin en 1930-31 ; et la Guerre du Kongo-Wara en Afrique Equatoriale Française (AEF). 

## Référence

## Liens 