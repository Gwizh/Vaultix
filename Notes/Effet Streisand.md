MOC : 
Source : [[Effet Streisand]]
Date : 2023-06-13
***

## Résumé
L’effet Streisand désigne un phénomène médiatique involontaire. Il se produit lorsqu'en voulant empêcher la divulgation d’une information que certains aimeraient cacher, le résultat inverse survient, à savoir que le fait caché devient notoire.

Ainsi, les efforts déployés par une personne pour masquer ou supprimer une publication, ou encore faire retirer un produit du commerce, conduisent à l'exposition et à la médiatisation de ce que cette personne désirait que l'on ne sache pas ou que l'on ne voie pas. Il s'agit donc à proprement parler d'un effet pervers de la démarche, qui est d'ailleurs renforcé par internet.

## Référence

## Liens 