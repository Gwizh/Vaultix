MOC : 
Titre : [[Les grandes périodes de l'économie mondiale]]
Date : 2023-08-29
***

## Résumé
La très forte croissance mondiale des années 1830 interrompue par la Panique de 1837.
La très forte croissance mondiale des années 1850, interrompue par le Krach de 1857.
La longue dépression 1873 à 1896 dans le sillage de la [[Crise bancaire de mai 1873]].
La forte croissance mondiale des années 1900, interrompue par la [[Première Guerre mondiale]].
La très forte croissance mondiale des années 1920, interrompue par le Krach de 1929.
La Grande dépression des années 1930 dans le sillage du Krach de 1929.
La forte croissance mondiale des années 1945 à 1974, interrompue par le Premier choc pétrolier.
## Références

## Liens 
[[Capitalisme]]