MOC : 
Source : [[Loi du 29 juillet 1881 sur la liberté de la presse]]
Date : 2023-07-07
***

## Résumé
La loi du 29 juillet 1881 sur la liberté de la presse est une loi française, votée sous la IIIe République, qui définit les libertés et responsabilités de la presse française, imposant un cadre légal à toute publication, ainsi qu'à l'affichage public, au colportage et à la vente sur la voie publique. 

## Référence

## Liens 