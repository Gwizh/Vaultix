MOC : 
Source : [[Coup d'État de 1963 en Syrie]]
Date : 2023-07-02
***

## Résumé
The 1963 Syrian coup d'état, referred to by the Syrian government as the 8 March Revolution (Arabic: ثورة الثامن من آذار), was the successful seizure of power in Syria by the military committee of the Syrian Regional Branch of the Arab Socialist Ba'ath Party. The planning and the unfolding conspiracy was inspired by the Iraqi Regional Branch's successful military coup. 

## Référence

## Liens 
[[Ba'athism]]
[[Syrie]]