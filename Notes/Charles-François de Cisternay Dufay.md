MOC : 
Source : [[Charles-François de Cisternay Dufay]]
Date : 2023-04-13
***
#Adam

## Résumé
Petit-fils d’un capitaine des gardes du prince de Conti, fils d’un lieutenant aux gardes françaises, il naît à Paris en 1698. A quatorze ans, il est lieutenant au régiment de Picardie, à vingt ans il fait la courte guerre d’Espagne puis il quitte l’armée. Il se consacre alors aux sciences et s’y montre un prodigieux expérimentateur. Il a vingt-cinq ans quand il entre à l’Académie Royale des Sciences comme adjoint chimiste, à trente cinq ans il en occupe le poste de directeur. A cette époque (1733) il publie une série de mémoire sur l’électricité.

Il réalise de nombreuses fois l'expérience de la pendule électrique avec laquelle il conclue qu'il y a deux types d'électricités : une qui attire et une qui repousse. On comprendra plus tard que ce qu'il décrit vient du courant positif et négatif. Mais lui parlera respectivement d'électricité vitreuse et d'életricité résineuse. 

Voila ce qu'il écrit dans son mémoire :
	"Voilà donc constamment deux électricités d’une nature  
	toute différente, savoir celle des corps transparents et soli-  
	des comme le verre, le cristal, etc... et celle des corps  
	butimineux ou résineux, comme l’ambre, la gomme copal, la  
	cire d’Espagne, etc..."


## Référence
[[Chimie]]
[[Électricité]]

## Liens 