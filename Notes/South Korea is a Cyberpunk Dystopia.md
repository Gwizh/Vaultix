MOC : 
Source : [[South Korea is a Cyberpunk Dystopia]]
Date : 2023-06-09
***

## Résumé
[[Samsung]] constitue 20% du PIB de la [[Corée du Sud]], un chiffre incomparable dans le monde. Ils contrôlent de très nombreux secteurs comme l'énergie, le secteur de construction, les assurances, les porte-conteneurs et des équipes sportives. 

Les emplois à Samsung sont considérés comme les meilleurs du pays mais cela signifie qu'il y beaucoup de compétitions. On connaît le problèmes de l'éducation en Corée du Sud... 11h de cours avec en plus à partir du collège 95% des élèves qui vont dans des cours privés du soir (les Hagwon). Ça peut donc aller jusqu'à 16h par jour. Il y a une expression qui dit que dormir 3 heures par nuit permet d'aller dans les meilleurs universités mais dormir 5 heures ou plus signifie ne pas aller à l'université du tout. Suneung exam qui n'arrive qu'une fois par an et qui met une pression dingue sur les élèves. Sidenote : les parents ont pris l'habitude d'offrir de la chirurgie esthétique aux enfants qui réussissent l'exam. Le taux de suicide le plus élevé du monde et le taux de natalité le plus bas. 16% des revenus des foyers pour l'éducation privée. Bannir l'éducation après 22h.

![[Chaebol#Résumé]]

Il y a d'autres entreprises : SK, Hyunday et LG qui représentent plus de 50% des exportations.

![[Hoesik#Résumé]]

## Référence

## Liens 
https://www.youtube.com/watch?v=Ahl1lexWxbM