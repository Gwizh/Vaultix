[[Film]]
MOC : 
Titre : [[La Haine]]
Auteur : [[Mathieu Kassovitz]]
Date : 1995
Date de visionnage : 27-07-2022
***

## Résumé
Film où on suit 3 jeunes de banlieues, Vinz, Saïd et Hubert. L'action se passe pendant une seule journée, après des émeutes dans un quartier de Paris. On y apprend qu'un de leur proche a été gravement blessé et transféré à l'hôpital. C'est aussi pendant ces émeutes qu'une arme est perdue par les policiers, elle est trouvée par Vinz qui jure qu'il tuera un flic s'il apprend la mort d'Abdel, le gars à l'hôpital. 

Hubert, noir (c'est important car racisme), essaiera de l'empêcher de l'utiliser pour tout et rien pendant le film. Saïd, un arabe (c'est important car racisme) plus jeune ne comprend pas trop les dangers d'une telle arme et joue plutôt le jeu de Vinz. 

Pour pouvoir être remboursé, Saïd partira pour le centre de Paris pour rencontrer Astérix, qui a pu se faire de l'argent on ne sait comment. Ils vont se faire choper sans raison apparente par les policiers qui vont les violenter au poste. Ils rateront le train et devront rester toute la nuit dans Paris. 

Plus tard, ils apprennent à Paris qu'Abdel est bel et bien mort de ses blessures. Il y a ensuite une scène de dingue où on pense que Vinz va se servir de son arme sous les conseils de Hubert. Finalement, il ne le fera pas et repartiront le lendemain. 

Arrivés là-bas, Vinz confie l'arme à Hubert et se séparent. Interpellés par les policiers, Vinz va se faire tuer par un de ces derniers par accident avec son arme. Hubert qui s'en doutait s'est rapproché et à pointé son arme sur le policier qui fait de même. On ne sait pas qui sont morts, seul Saïd restera traumatisé. 

## Faits importants
- Tout est en noir et blanc ce qui donne un aspect si dramatique au film. 
- Aucun des deux camps n'est mis en cause, il y a des bavures des deux côtés. 
- Le film n'a pas vieilli, seulement un peu sur le language (golri typiquement mdr) 
- La menace de l'utilisation de l'arme plane pendant tout et on ne sait quand est ce qu'elle va frapper. À la fin on est pas bien mdr.
- La scène où ils vont voir Astérix dans Paris et qu'ils sont coincés dans l'appart est un bon symbole de l'ascenseur cassé des classes. 
- Un film qui se veut globalement simple, donnant un exemple concret qui est certes romancé mais jamais romantismé. 


## Acteurs
[[Vincent Cassel]]
[[Hubert Koundé]]
[[Saïd Taghmaoui]]

## Référence

## Liens
Dans l'ordre : 
<-- 
--> [[Le Fabuleux Destin d'Amélie Poulain]]