MOC : 
Titre : [[Républicanisme en France au XIXe siècle]]
Date : 2023-09-26
***

## Résumé
Le républicanisme en France au xixe siècle est un mouvement politique plus ou moins organisé issu notamment de la philosophie des Lumières et de la [[Révolution française]], ainsi qu'une tradition de pensée politique distincte, bien que convergente sur tel ou tel point, avec la tradition plus générale du républicanisme telle qu'identifiée notamment par le philosophe Philip Pettit. Opposé à la monarchie, absolue ou constitutionnelle, et donc à la Restauration, le républicanisme français se distingue aussi du [[Libéralisme]] et du [[Notes/Socialisme]], même s'il peut emprunter des éléments à l'un et à l'autre. Après de longs débats et de nombreuses crises, succès et échecs, le républicanisme finira par s'imposer en France comme le régime le plus consensuel, notamment après la crise du 16 mai 1877, et ne sera plus remis en cause, hormis sous [[Régime de Vichy]].

Pour Maurice Agulhon, la diffusion des idées républicaines se fait des centres vers les périphéries, avec les [[Bourgeoisie]] instruits et républicains transmettant leurs idées dans les villages où ils ont une activité commerciale

Les campagnes sont naturellement plus hostiles au républicanisme, même si ce n'est pas uniforme. Dès 1848, plusieurs zones rurales votent républicain. Plusieurs explications sont avancées : villages plus gros qui permettent la réunion et la sociabilité, différence de tradition religieuse ou de régime de propriété[](https://fr.wikipedia.org/wiki/R%C3%A9publicanisme_en_France_au_XIXe_si%C3%A8cle#cite_note-Agulhon_1998-2).

Une partie des républicains et les radicaux anticléricaux s'opposent au vote des femmes car elles sont moins instruites et plus favorables à la religion, donc favorables à la contre-Révolution[](https://fr.wikipedia.org/wiki/R%C3%A9publicanisme_en_France_au_XIXe_si%C3%A8cle#cite_note-Agulhon_1998-2).

Pour Vincent Bourdeau, les Républicains du xixe siècle sont opposés à la redistribution et défendent la [[Propriété privée]][](https://fr.wikipedia.org/wiki/R%C3%A9publicanisme_en_France_au_XIXe_si%C3%A8cle#cite_note-3).

### Différents courants 
#### Partis
##### Républicains modérés (1871 - 1901, avec scission en 1888 )
Les « républicains modérés » ou « modérés », aussi appelés « républicains opportunistes » ou « républicains de gouvernement », sont en France, au cours de la première moitié de la Troisième République, un courant politique républicain initialement considéré comme étant de gauche, et qui est à l'origine de certaines idées de la gauche mais aussi de la droite républicaine et libérale d'aujourd'hui. Le mot a été essentiellement utilisé sous la IIIe République et la IVe République (de Jules Ferry à Paul Reynaud). 
Personnalités (selon Wikipedia) : [[Jules Ferry]], [[Léon Gambetta]], [[Jules Grévy]], [[Jean Casimir-Perier]], [[Pierre Waldeck-Rousseau]]

##### Républicain progressistes (1885 - )
https://fr.wikipedia.org/wiki/R%C3%A9publicains_progressistes

#### Unions de partis
##### Gauche républicaine (1871-1885)
Elle réunit les républicains « modérés », de sensibilité libérale, aussi appelés Républicains opportunistes, et prend place entre l'extrême gauche radicale de Gambetta et le « Centre gauche » de Thiers. Ses principaux dirigeants sont Jules Grévy, Jules Ferry, président du Conseil en 1880-1881 et 1883-85, et Jules Simon. 

##### Union républicaine (1871 - 1885)
Le groupe parlementaire de l'Union républicaine est créé de façon informelle à l'issue des premières élections législatives de la Troisième République, en 1871. Mené à l’Assemblée nationale puis à la Chambre des députés par Léon Gambetta, le groupe disparaît en 1885, avant d’être relancé en 1905 puis 1911, avec une orientation plus centriste. Il existe également un groupe de l’Union républicaine au Sénat de 1876 à la fin de la Troisième République.
[[Léon Gambetta]], [[Pierre Waldeck-Rousseau]], [[Louis Blanc]], [[Victor Hugo]], [[Georges Clemenceau]]
"Extrême-gauche"

##### Union des gauches (1885 - 1893)
https://fr.wikipedia.org/wiki/Union_des_gauches

##### Union démocratique
https://fr.wikipedia.org/wiki/Union_d%C3%A9mocratique_(groupe_parlementaire)

##### Centre gauche (groupe parlementaire)
Idéologie : [[Républicanisme]],  [[Libéralisme économique]], [[Conservatisme social]]
Personnalité : [[Adolphe Thiers]]

 

## Références

## Liens 