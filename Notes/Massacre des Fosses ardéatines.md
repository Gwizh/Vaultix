MOC : 
Source : [[Massacre des Fosses ardéatines]]
Date : 2023-05-05
***

## Résumé
Le massacre des Fosses ardéatines est le massacre de 335 civils italiens perpétré par les troupes d'occupation nazies à Rome le 24 mars 1944. Ce massacre fut commis en représailles à un attentat perpétré le jour précédent sur la via Rasella.
[[Attentat de Via Rasella]]  

![[Italie - ce qui change (ou pas) dans un pays dirigé par l’extrême droite#^a4aebb]]

## Référence

## Liens 
[[Nazisme]]
[[Italie]]