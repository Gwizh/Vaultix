[[Livre]]
MOC : 
Titre : [[La Fortune des Rougon]]
Auteur : [[Emile Zola]]
Date : 2022-10-28
***

## Résumé
Chapitre 1 :
Très joli début avec de belles descriptions du village et des paysages. Les deux amoureux aussi sont bien décris. Le moment avec le drapeau est particulièrement bien. 

Chapitre 2 :
Description de la famille Rougon, immense background qui est super bien écrit. Le ton simple rend certains passages vraiment drôles. 

Chapitre 3 :
Commence par une étudie sociologique de Plassans, c'est trop bien !! Oh la description des intrigues liées au coup d'État de [[Napoléon 1er]], c'est trop bien !! Meilleur chapitre ! 

Chapitre 4 :
Il continue à décrire la famille, c'est un chapitre plus dur à lire. Je déteste Antoine omg, c'est le pire personnage ever. Mais j'adore Silvère !! Fait le lien avec le premier chapitre entre Silvère et Miette. Pareil, Pascal le best. 

Chapitre 5 :
Relation entre Miette et Silvère. J'adore Miette, c'est vraiment un super perso. C'est super romantique le moment du puit omg, même moi le reconnais. Ah mais naaaan elle meurt à la fin... 

## Notions clés

## Citations

## Passages amusants

## Références
