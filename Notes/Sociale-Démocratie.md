MOC : 
Source : [[Sociale-Démocratie]]
Date : 2023-04-07
***

## Résumé

Vient de la [[SPD]], un parti allemand qui était socialiste et démocratique mdr. Mais c'était important dans l'Allemagne impérialiste de l'époque.

Grande influence à l'époque avec 1 millions de membres et une présence forte durant la [[Deuxième Internationale]].

Cela signifiat être marxiste et socialiste et théoriquement, [[Vladimir Lénine]] été Socialedémocrate.

Ils votent les crédits de guerre pour la [[Première Guerre mondiale]] ce qui est vu comme une trahison par Lénine et de nombreux socialistes. Lénine appelle alors les révolutionnaires à quitter les sociaux-démocrates avec la [[Ligue Spartakiste]] qui est réprimée  par le sang par la SPD. 

Puis, au fur et à mesure, elle s'accomode de l'économie de marché en devenant de + en + réformiste. Pour eux, il fut gagner les élections pour corriger les problèmes du capitalisme par l'Etat. Impots élevés et compromis constants entre patronat et les travailleurs.

BIzarrement, c'est le [[PCF]] qui suit le plus l'organistation (pas l'idéologie) de la SPD.

Grande influence en Europe notamment dans les pays Scandinave.

[[François Mitterand]] parlait de rupture avec le capitalisme et l'ordre établi et se revendiquait de Marx mais été en réalité surtout Soc-dem. Les seuls à assumer cette idéologie à l'époque sont des gens comme [[Michel Rocard]] et [[Dominique Strauss-Kahn]]
[[François Hollande]] se revendiquait du Socdem mais en fait ne suivait pas non plus l'idéologie qu'il disait suivre. Suivre une politique économique libérale, remettre en cause le droit du travail, passage en force au moment de la loi travail. Le [[Parti Socialiste]] est surtout [[Social-Libéralisme]]

## Référence

## Liens 