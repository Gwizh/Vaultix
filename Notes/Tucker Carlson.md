MOC : 
Source : [[Tucker Carlson]]
Date : 2023-05-05
***

## Résumé
Tucker Carlson, né le 16 mai 1969 à San Francisco (Californie), est un éditorialiste et animateur de télévision américain évoluant dans le camp conservateur. De 2016 à 2023, il présente l'émission Tucker Carlson Tonight sur [[Fox News]], suivie par environ trois millions de téléspectateurs quotidiennement.

Tucker Carlson est un opposant virulent du progressisme, un critique de l'immigration, et a été décrit comme un nationaliste. Tout d'abord de tendance libertarienne sur un plan économique, il est devenu un supporter du protectionnisme. En 2004, il renonce à son soutien initial à la [[Guerre d'Irak]] et il affiche depuis lors son scepticisme concernant les interventions américaines à l'étranger. Tucker Carlson est, et a été, un promoteur de théories du complot sur des sujets tels que le « Grand remplacement » aux États-Unis, le Covid-19, l'élection présidentielle américaine de 2020 et l'assaut du Capitole du 6 janvier 2021 ; ses discours trompeurs sur ces sujets et un nombre substantiel d'autres sont notables.

Le 24 avril 2023, Fox News annonce abruptement la fin de sa collaboration avec Tucker Carlson.

Conservateur-libertarien et xénophobe. 

## Référence

## Liens 
[[Nationalisme]]
[[États-Unis]]