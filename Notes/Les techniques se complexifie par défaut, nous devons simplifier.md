MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081430
Tags : #Fini
***

# Les techniques se complexifie par défaut, nous devons simplifier
En informatique, nous avons créés des langages toujours plus abstraits, toujours plus éloignés de la machine réelle pour rendre le travail plus efficace. Dès que nous avons un nouveau besoin, nous inventons une nouvelle pièce qui s'ajoute au reste. 

Nous devons simplifier de nous-mêmes ce que nous faisons et rester terre à terre. Si nous avons trop de dépendances pour faire marcher le système alors il est probablement bancal. Réduire la complexité n'est pas revenir en arrière, c'est faire moins avec l'expertise du plus. Tout devrait pouvoir fonctionner à bas niveau tout le temps, le haut niveau ne doit servir que dans un second temps.

## Référence
[[@Jonathan Blow - Preventing the Collapse of Civilization (English only),]]