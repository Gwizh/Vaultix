MOC : 
Source : [[Folk Politics]]
Date : 2023-04-07
***

## Résumé
Folk politics places the everyday above the structural, the appeal to feelings above the labour to develop a conceptual analysis and critique of our present state.

Ce terme vient des deux sens du mot Folk en anglais. A la fois les politiques des personnes, à une échelle réduite mais aussi des politiques populaires, provenant des traditions et de la culture populaire

Nick Srnicek les critique dans [[Inventing the Future. Postcapitalism and a World without Work]] car ça empêche la [[Gauche]] de prendre de l'ampleur sur le monde et sur les idées. 

## Référence

## Liens 