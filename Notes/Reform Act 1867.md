MOC : 
Titre : [[Reform Act 1867]]
Date : 2023-08-23
***

## Résumé
Le Reform Act de 1867 est une loi promulguée pour élargir la base électorale en Angleterre à une partie des couches laborieuses. Cette réforme électorale permit de doubler le nombre d'électeurs qui passa ainsi à environ 2 millions, soit un cinquième de la population totale de l'Angleterre et du Pays de Galles. Elle donna le droit de vote à tous les locataires en résidence depuis plus d'un an dans la même circonscription, abolissant par là même la pratique du compounding, une taxe locative qui servait à certains propriétaires pour payer les impôts locaux leur garantissant le droit de vote.
## Références

## Liens 
[[Royaume-Uni]]
[[Démocratie]]