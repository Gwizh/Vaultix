MOC : 
Source : [[Felice Orsini]]
Date : 2023-07-17
***

## Résumé
Teobaldus Orsus Felice Orsini (né le 10 décembre 1819 à Meldola, près de Forlì, Émilie-Romagne - mort le 13 mars 1858 à Paris) est un révolutionnaire et patriote italien, figure importante du [[Risorgimento]] italien, auteur d'un attentat contre [[Napoléon III]], empereur des Français, le 14 janvier 1858. 

## Référence

## Liens 
[[Italie]]