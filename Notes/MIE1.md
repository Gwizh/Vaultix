Nous sommes composés de millions de cellules et des millions de bactéries nous habitent. De nombreux virus modifient notre ADN en permanence. 

Nous ne nous controlons pas vraiment, nous ne pouvons que porter un masque pour cacher la réelle complexité en nous.

