MOC : 
Source : [[Auguste Blanqui]]
Date : 2023-06-02
***

## Résumé
Louis Auguste Blanqui, surnommé « l’Enfermé », né le 19 pluviôse an 13 de la République (8 février 1805) à Puget-Théniers (Alpes-Maritimes) et mort le 1er janvier 1881 à Paris, est un révolutionnaire socialiste français, souvent associé à tort aux socialistes utopiques. Il défend pour l'essentiel les mêmes idées que le mouvement socialiste du xixe siècle et fait partie des socialistes non-marxistes. L'historien Michel Winock le classe comme l'un des fondateurs de l'extrême gauche française, qui s'oppose aux élections démocratiques, les considérant comme « bourgeoises », et qui aspire à l'« égalité sociale réelle ».

Après 1830, encore étudiant, Blanqui fait le constat que la révolution ne pourra traduire la volonté du peuple que par la violence : « l'interdiction politique » qui place le peuple sans garantie, sans défense, devant « l'odieuse domination des privilégiés », conduit fatalement à la lutte. Il fut, en conséquence de ses tentatives insurrectionnelles, emprisonné une grande partie de son existence, ce qui lui a donné le surnom de « l’Enfermé ». Il est à l'origine du blanquisme.

En 1880, il publie le journal Ni Dieu ni Maître dont le titre est devenu une référence pour le mouvement [[Anarchie|anarchiste]].

## Référence

## Liens 
[[Socialisme utopique]]