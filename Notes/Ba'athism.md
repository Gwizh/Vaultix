MOC : 
Source : [[Ba'athism]]
Date : 2023-07-02
***
https://en.wikipedia.org/wiki/Ba%27athism
## Résumé
Ba'athism, also spelled Baathism, meaning "renaissance" or "resurrection" is an Arab nationalist ideology which promotes the creation and development of a unified Arab state through the leadership of a vanguard party over a socialist revolutionary government.

Idéologies : 
    Anti-imperialism
    Arab nationalism
    Arab socialism
    Pan-Arabism
    Republicanism
    Anti-Zionism
    Progressivism
    Secularism
    Socialism

## Référence

## Liens 