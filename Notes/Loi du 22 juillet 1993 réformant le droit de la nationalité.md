MOC : 
Source : [[Loi du 22 juillet 1993 réformant le droit de la nationalité]]
Date : 2023-04-14
***

## Résumé
La loi n°93-933 du 22 juillet 1993 réformant le droit de la nationalité dite « loi Mehaignerie » introduit la condition de « manifestation de volonté » pour l'acquisition de la nationalité française.

Elle a fait l'objet d'un recours devant le [[Conseil constitutionnel]] (Décision n° 93-321 DC du 20 juillet 1993).

Proposé par [[Edouard Balladur]] et son gouvernement.

[[Jacques Chirac]] est pour.

## Référence

## Liens 