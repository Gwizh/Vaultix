[[Article]]
MOC : [[Noz/ECONOMIE]]
Titre : [[EDF - l'Etat confirme la nationalisation à 100 %]]
Auteur : [[Les Echos]]
Date : 2022-07-07
Lien : https://www.lesechos.fr/industrie-services/energie-environnement/letat-confirme-la-nationalisation-dedf-a-100-1775016
Fichier : 
***

## Résumé
Elisabeth Borne a indiqué devant le Parlement que l'énergéticien devait revenir à 100 % dans le giron de l'Etat. Ceci afin d'engager les grands chantiers promis par Emmanuel Macron et notamment le renouvellement du parc nucléaire. L'opération serait comprise entre 5 et 7 milliards d'euros.

En grande difficulté financière, EDF - déjà détenu aujourd'hui à 84 % par l'Etat -, est aujourd'hui incapable de mettre en musique seul le plan de renouvellement du parc nucléaire annoncé par Emmanuel Macron en début d'année. Pour construire les six EPR visés a minima par l'exécutif, un budget d'environ 60 milliards d'euros pourrait s'avérer nécessaire, soit un montant proche de la dette nette d'EDF prévue à la fin de l'année !

L'opération permettrait par ailleurs de rassurer les investisseurs inquiets de la menace qui plane sur le groupe d'une dégradation des agences de notation. Dans une note sur le sujet, S&P Global met toutefois en garde : « Une nationalisation, sans autre mesure complémentaire, ne conduirait pas à une révision à la hausse de la note de crédit du groupe EDF. Un renforcement de notre critère de soutien public implique un changement radical pour EDF, notamment des évolutions des règles de concurrence au niveau européen ».

Les syndicats s'y préparent. « Si l'Etat veut détenir 100 % d'EDF, c'est pour ouvrir la voie à un morcellement de l'entreprise. On détourne l'attention du Parlement en parlant de souveraineté alors qu'il est question de la première étape du démantèlement du groupe », déplore Fabrice Coudour, à la CGT d'EDF.

## Référence
[[Energie]]
[[EDF]]
[[EDF - les contours de la révolution qui vient]]

## Liens 