MOC : 
Source : [[Rojava - Le Rôle Déterminant Des Femmes Dans La Révolution]]
Date : 2023-06-09
***

## Résumé
#Alire Nous vous écrivons depuis la révolution. [[Corinne Morel Darleux]]
Rojava déclaré indépendant et autonome en 2013. Contrat Social en 2014. Révolution des femmes. Population évaluée à 6 millions de personne : comme le Danemark en gros. Vient de 40 ans de luttes des femmes et des radicaux. Notamment pour le féminisme, ça ne propose pas juste d'inclure les femmes dans le système actuel mais bien de changer le système pour les femmes. Compilation de témoignages, poèmes, traités politiques, etc...  

Des cours sont données : processus de formation sur les idées du système. Autonomie des femmes : discussions fermées pour vraiment discuter des enjeux. N'exclure personne : pas de logiques de classes, de nations etc...
Beaucoup de mises en pratiques, c'est super important dans le mouvement. Le [[Néolibéralisme|néolibéralisme]] était bien moins implanté dans cette région, il y avait déjà des pouvoirs locaux (agricoles notamment).

Concept de [[Jineoloji]]

Interdiction des mariages précoces, de la polygamie. 

Pas de glorification de la guerre et des armes. Unité d'auto-défense et non d'attaque : ce n'est pas une armée qui veut envahir. Ça change complètement la philosophie : ce n'est pas une armée de carrière mais des civils. Auto-défense d'un territoire proche : c'est tout. 

## Référence

## Liens 