MOC : 
Source : [[Benjamin Franklin]]
Date : 2023-04-13
***

## Résumé
Né à Boston en 1706 il est autodidacte. Son père est un modeste fabricant de chandelles et c’est chez son frère imprimeur qu’il peut assouvir sa passion pour la lecture. Il rencontre l’électricité par  
hasard à l’âge de quarante ans. Il est alors à Philadelphie où il participe aux activités des cercles cultivés de la ville. Ceux-ci ont reçu d’Angleterre une «machine électrique» avec son mode d’emploi. C’est un tube de verre horizontal actionné par une manivelle et qui s’électrise par le frottement de la main ou d’un coussinet de cuir.

Il est l'inventeur du paratonnerre mais aussi le premier à avoir l'intuition quant à la notion de charge. Il considère en effet l’électricité comme un fluide qui imprègne tous les corps.

J'adore cette expérience :
A, B et C. A est isolé sur un gâteau de cire, il frotte un tube de verre qu’il tend à B lui même isolé. B approche la main du tube et en reçoit une étincelle. A ce moment le personnage C resté au sol, en contact avec la terre, tend les doigts vers A et B et reçoit de chacun une décharge électrique.

## Référence
[[Électricité]]

## Liens 