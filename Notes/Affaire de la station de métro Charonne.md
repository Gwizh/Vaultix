[[Note Permanente]]
MOC : [[Histoire]] [[POLITIQUE]]
Titre : [[Affaire de la station de métro Charonne]]
Date : 2022-07-11
***

## Résumé
L'affaire de la station de métro Charonne est un cas de violence policière qui a eu lieu le 8 février 1962, dans la station de métro Charonne à Paris, à l'encontre de personnes manifestant contre l'Organisation armée secrète ([[OAS]]) et la [[Guerre d'Algérie]].

Étant donné le contexte des plus tendus et l'état d'urgence décrété en avril 1961 après le putsch d'Alger, la manifestation, organisée par le Parti communiste français et d'autres organisations de gauche, avait en effet été interdite, et le préfet de police de Paris, Maurice Papon, avait donné l'ordre de la réprimer, avec l'accord du ministre de l'Intérieur, Roger Frey, et du président de la République, [[Charles de Gaulle]].

Parmi les manifestants qui essaient de se réfugier dans la bouche de la station de métro, huit personnes trouvent la mort, étouffées ou à cause de fractures du crâne, ainsi qu'une neuvième à l'hôpital, des suites de ses blessures. 

## Référence
[[Histoire de la France au XXe Siècle - Livre 3]]

## Liens 