MOC : 
Source : [[La grande subvention - Le capitalisme français sous perfusion]]
Date : 2023-04-14
***

## Résumé
Article de [[Frustration]] et de [[Nicolas Framont]]
https://www.frustrationmagazine.fr/subvention-capitalisme/

Le thème est bien connu : dans notre pays, il y aurait d’un côté les « entrepreneurs » du secteur privé qui ne comptent pas leurs heures pour « créer des richesses » et des emplois, ne devant leurs revenus qu’à la force de leur travail, et de l’autre les fonctionnaires, fardeau budgétaire terrible pour la société, qui ne produisent rien d’autre que du service public déficitaire et peu performant. D’un côté un apport, de l’autre un coût. Or, rien n’est plus faux : désormais, le secteur privé coûte au contribuable bien plus cher que nombre de services publics. Car chaque année, l’Etat dépense **157 milliards d’euros en subventions, crédits d’impôts et exonérations de cotisations au profit des entreprises privées de toutes tailles et de tous secteurs**. C’est deux fois le budget de l’Education nationale, et cela représente 30% du budget de l’État en 2021.

Sauf que ça ne marche pas. Toutes les études, y compris ministérielles, sur les effets de ces 140 à 200 milliards d’euros annuel dépensés pour les entreprises privées, montrent que les effets sont faibles voire inexistants. Le dernier rapport en date, celui de l’Institut de Recherches Economiques et Sociales (IRES), n’y va pas par quatre chemins : “L’efficacité des allègements du coût du travail se trouve sans doute ailleurs : dans le soutien apporté aux marges des entreprises”, nous dit-il… et ces marges, les entreprises en font bien ce qu’elles veulent. Et ça n’a pas servi à créer de l’emploi, ni à relocaliser notre industrie, mais bien à augmenter les dividendes des actionnaires.

Faire transiter des milliards d’euros du public au privé par intervention étatique semble a priori contraire à la doctrine de non-intervention étatique prônée par le libéralisme économique. Mais en réalité, la doctrine néolibérale n’a jamais prôné la fin des transferts vers le privé, au contraire. Ce que ses partisans souhaitent, c’est que l’État ne se mêle pas du fonctionnement des entreprises, c’est tout. Mais son argent est le bienvenu. C’est pourquoi, tandis qu’il enrichit les entreprises privées et ses actionnaires, l’État réduit la régulation du [[Droit du travail]] en leur sein, leur offre des marges de manœuvre plus grandes, ouvre de nouveaux marchés… Le [[Néolibéralisme]] n’est pas la non-intervention de l’État. C’est une intervention massive de l’État pour aider le [[Capitalisme]] à fonctionner mieux et plus fort.

Un exemple particulièrement parlant : en 2019, on apprenait une drôle d’histoire en provenance du groupe [[Michelin]], grosse entreprise qui avait alors reçu 65 millions d’euros de Crédit d’Impôt pour la Compétitivité des Entreprises ([[CICE]], un dispositif mis en place sous Hollande et reconduit sous Macron en 2019 sous la forme d’une exonération de cotisations patronales pour le même montant, 20 milliards par an). Le groupe avait annoncé s’être servi de ce crédit d’impôt pour investir dans ses usines, notamment celle de La Roche-sur-Yon… avant d’annoncer sa fermeture et la suppression de 74 emplois. Après avoir annoncé des investissements en grande pompe en nouvelles machines, [nous apprend _Libération_,](https://www.liberation.fr/checknews/2019/10/24/la-roche-sur-yon-michelin-a-t-il-achete-avec-le-cice-des-machines-expediees-a-l-etranger_1758128/?redirected=1&redirected=1) l’entreprise avait en effet changé son fusil d’épaule : _« Sur huit machines achetées, seules deux ont été installées. Les six autres, encore dans les cartons, sont alors réexpédiées vers d’autres usines à l’étranger. Deux machines de finition sont envoyées dans l’usine d’Aranda, en Espagne, trois machines de confection partent sur le site de Zalau, en Roumanie, et une machine de confection s’en va sur le site d’Olsztyn, en Pologne. »_ 

Michelin s’est donc servi d’un crédit d’impôt justifié par la création d’emploi en France pour… délocaliser sa production.

## Référence
[[Délocalisation]]

## Liens 