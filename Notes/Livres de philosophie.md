### Sur Dieu et la Métaphysique :

La Monadologie de Leibniz
Ainsi parlait Zarathoustra de Nietzsche
L'Etre et le Néant de Sartre

### Sur la Morale:

Aristote : Ethique à Nicomaque
Kant : Critique de la raison pratique
Hume : Traité de la nature humaine
Spinoza : L'éthique
Hans Jonas : Le Principe responsabilité
Epicure : Lettre à Ménécée

### Sur la Politique:

Marx : Le Capital
Platon : La République
Kant : Projet de paix perpétuelle
Hobbes : Le Léviathan
Rousseau : Du contrat social
Rawls : Théorie de la Justice