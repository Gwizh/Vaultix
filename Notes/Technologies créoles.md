MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081614
Tags : #Fini
***

# Technologies créoles
p71
On imagine par défaut l'[[Occident]] transférer ses technologies à un tiers monde plus en retard. On imagine ce lien comme une dépendance, comme un geste de salvation des occidentaux. Quand ils obtiennent les technologies, nous jugeons alors ces populations sur le fait qu'elle ne savent pas s'en servir. Pourtant, l'utilisation des technologies est simplement différente d'un pays à un autre et on peut vivre sans certaines technologies par choix ou pas inintérêt. Ce n'est pas passé vs futur, c'est toujours une question de contexte.

## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]