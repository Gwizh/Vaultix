MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081614
Tags : #Fini
***

# Non proportionnalité entre croissance économique, niveau technologique et innovation technologique
Le taux d'innovation n'est pas directement lié au niveau technologique. On peut ne pas innover mais garder un niveau technologique important. Cela vient principalement des transferts technologiques.
Exemple : L'Espagne n'innovait presque pas pendant le XXème siècle mais a toujours gardé un niveau technologique d'un pays riche.
## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]