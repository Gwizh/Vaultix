MOC : [[Philosophie et psychologie sociale]], [[Autoritarisme]]
Type : Note littéraire
Tags : #Type/Note_littéraire
Titre : [[Bertillonnage]]
Date : 2023-10-05
Tags : #EnCours 
***

Le bertillonnage, appelé aussi système Bertillon ou anthropométrie judiciaire, est une technique criminalistique mise au point par le Français Alphonse Bertillon en 1879. La technique repose sur l'analyse biométrique (système d'identification à partir de mesures spécifiques) accompagnée de photographies de face et de profil.

À peu près contemporain de l'identification judiciaire au moyen des empreintes digitales, le bertillonnage intégrera progressivement celle-ci, avant d'être en grande partie supplanté par elle. 

## Référence
[[@Les sauvages de la civilisation_ regards sur la Zone, d'hier à aujourd'hui,Jérôme Beauchez]]