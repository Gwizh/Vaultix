MOC : 
Source : [[Déclaration des droits des peuples autochtones ou UNDRIP]]
Date : 2023-08-09
***

## Résumé
La Déclaration, recommandée par la Déclaration et programme d'action de Vienne4 affirme notamment que les peuples autochtones ont le droit à l'autodétermination interne et qu'en vertu de ce droit ils déterminent librement leur statut politique et recherchent librement leur développement économique, social et culturel1. Elle dispose que les peuples autochtones ne peuvent être expulsés de leur terre ; qu'ils ont droit aux ressources naturelles situées sur leur terre. La Déclaration devient la référence de l'[[ONU]] pour le respect des droits des peuples indigènes ; elle permet d'évaluer l'attitude des États envers les peuples indigènes, mais n'est pas dotée d'effet contraignant en droit international. Il s'agit donc de droit mou.

## Référence

## Liens 
[[Indigène]]