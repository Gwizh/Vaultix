MOC : 
Source : [[Mutineries de la mer Noire]]
Date : 2023-07-16
***

## Résumé
Les mutineries de la mer Noire sont une série de révoltes survenues dans les troupes terrestres et les bâtiments français de l’escadre de la mer Noire en 1919, alors que le gouvernement français soutient les forces russes « blanches » (tsaristes) contre les révolutionnaires « rouges » (bolcheviques) pendant la guerre civile russe. L’intervention française, menée avec de trop faibles moyens navals et terrestres dans un pays hostile, est un échec qui n’est pas dû aux mutineries, ces dernières n'intervenant qu'après la décision de mettre un terme aux opérations militaires. Après le retour de l’expédition, les mutineries reprennent et touchent presque tous les ports où stationnent des navires de guerre : à Brest, Cherbourg, Bizerte, Lorient et Toulon avant de se terminer par une ultime mutinerie en Méditerranée orientale. 

## Référence

## Liens 
[[Armée Rouge]]
[[Révolution russe de 1917]]
[[Communisme]]
