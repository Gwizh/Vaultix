MOC : 
Source : [[Doï-van]]
Date : 2023-06-08
***

## Résumé
En 1888, le Doi-van , ex chef pirate fraichement soumis, et dont la bande aguerrie connaît à fond la tactique de la jungle est la bras droit du Tong-Doc (mandarin gouverneur de une ou deux provinces) Hoang-cai-Khai.

Ce dernier commande avec le titre de Kham-Sai, envoyé du roi, une colonne de miliciens auxquels s’ajoutent des partisans recrutés et armés sur place.
Mais le loyalisme de Doi-Van est éphémère : il fait défection à l’automne 1888 et reprend le maquis.

En 1889, la colonne du chef de bataillon Dumont poursuit la bande de Doi-Van et la refoule vers le bas Yen-thé.
Traqué abandonné par ses hommes, grelottant de fièvre le Doi-van se faufile entre ses poursuivants. Mais il n’en peut plus et finit par se rendre.

Transporté à Hanoi dans une cage, il est décapité le 7 novembre.

Selon la tradition annamite, la tête du Doï Van sera exposée un certain temps dans un panier en bambou suspendu à une branche, tandis que son corps sera jeté dans le fleuve rouge.

## Référence 
[[Viêtnam]]

## Liens 
[[Louise Michel]]