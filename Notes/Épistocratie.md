MOC : 
Source : [[Épistocratie]]
Date : 2023-06-23
***

## Résumé
L’épistocratie, aussi appelée épistémocratie, est un système politique qui accorde plus de crédit ou de pouvoir à des personnes ayant une meilleure maîtrise des sujets traités dans la prise de décisions.

Le mot « épistocratie » vient du grec ἐπιστήμη, « science » dans le sens de « savoir », et de κράτος, « souveraineté ».

## Référence

## Liens 