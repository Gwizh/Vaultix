[[Livre]]
MOC : [[Politique]]
Titre : [[Pourquoi le socialisme]]
Auteur : [[Albert Einstein]]
Date : 2022-09-05
***

## Résumé
Son analyse de la société de l'après-guerre.
L'idée que le [[Notes/Socialisme]] est un outil très précis pour étudier cette société et son économie.

## Notions clés
L'idée du conquête apporte l'imposition de méthodes. 

Les sciences n'ont pas de buts en soit. Ce sont des outils pour arriver à des buts. Or certaines sciences économiques actuelles ont des buts en soit. Le socialisme se place sur un point de vue ethico-social et se veut le plus neutre possible avec l'étude du [[Capitalisme]] notamment.

L'origine du trouble de la société actuelle : nous avons grande conscience de notre dépendance à celle-ci mais on y voit une influcence néfaste : on pense qu'on pourrait faire mieux sans. 
Selon lui, c'est à cause de la [[Compétitivité]]. On nous apprend petit à vouloir être meilleur que les autres. 

Et cette idée enseignée à l'[[École]] continue par la suite. La compétitivité est partout. Elle se voit dans deux points particuliers :
- La [[Propriété privée]] des machines
- Les contrats libres (payés en réalité toujours au minimum possible)

Sa solution : une [[Économie planifiée]], sans compétition. Et donc avec une éducation libre.

Il met en garde contre une économie trop centralisée qui aurait des effets tout aussi néfastes.


## Citations

## Passages amusants

## Références
[[Lectures sur le socialisme et le communisme]]
