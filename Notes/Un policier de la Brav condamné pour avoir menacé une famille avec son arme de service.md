MOC : 
Source : [[Un policier de la Brav condamné pour avoir menacé une famille avec son arme de service]]
Date : 2023-04-12
***

## Résumé 
Poursuivi pour avoir brandi son arme en promenade face à une femme et des enfants, un fonctionnaire a été condamné mardi à Évreux à quatre mois de prison avec sursis et deux années sans arme. « Je n’étais pas dans le cadre réglementaire », a reconnu ce policier, rattaché à la préfecture de [[Police]] de Paris. 

https://www.mediapart.fr/journal/france/110423/un-policier-de-la-brav-condamne-pour-avoir-menace-une-famille-avec-son-arme-de-service?utm_source=global&utm_medium=social&utm_campaign=SharingApp&xtor=CS3-5

Si je comprends bien, un policier était en promenade avec ses enfants et son arme de service. Un chien arrive alors et leur aboie dessus. Le policier a peur et le frappe. Le chien ensuite attaché, il point son arme sur la famille possédant le chien. De nombreux témoins l'ont vu agir ainsi. 

## Référence


## Liens 