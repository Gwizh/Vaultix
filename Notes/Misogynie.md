MOC : 
Source : [[Misogynie]]
Date : 2023-06-10
***

## Résumé
La misogynie (du grec ancien μῖσος / mîsos, « haine » ; et du préfixe gyno-, « femme, femelle ») est un terme désignant un sentiment de mépris ou d'hostilité à l'égard des femmes motivé par leur sexe. C'est l'antonyme de philogynie. Dans certains cas, elle peut se manifester par des comportements violents de nature verbale, physique ou sexuelle, pouvant dans des cas extrêmes aller jusqu’au meurtre. Le terme est sémantiquement antonymique à celui de misandrie (sentiment de mépris ou d'hostilité à l'égard d'un ou des hommes).

## Référence

## Liens 