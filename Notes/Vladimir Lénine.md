Titre : [[Vladimir Lénine]]
Type : Noeud
Tags : #Noeud
Ascendants :
Date : 2023-05-14
***
## Résumé
Lors de la [[Troisième Internationale]], il appelle tous les courants socialisme à faire le choix entre l'internationale communiste et l'internationale socialiste. Les groupes qui choisissent le premier devront suivre à la lettre le parti communiste de l'[[URSS]]. 

Lénine pense que les paysans sont aussi importants que les ouvriers.
![[Marxisme#^4a1e9c]]

Il théorise l'idée d'une avant-garde révolutionnaire. Un parti constitué de révolutionnaires professionnels chargés de rempalcer les bourgeois dans l'ancien régime et de donner la conscience de classe aux ouvriers et paysans qui n'ont pas pu l'avoir de part l'absence de capitalisme. 

Idée de centralisme démocratique : on peut avoir des débats internes mais une fois la décision prise à la majorité, tout le monde doit être d'accord. Il est aussi interdit de critiquer la ligne du parti à l'extérieur du parti. Il est aussi interdit de créer des courants à l'intérieur du parti car ce serait aller contre la ligne officiel du parti. Il faut aussi faire des purges pour virer les gens qui n'adhèrent pas totalement à la ligne officielle. 

> "Le Parti communiste ne pourra remplir son rôle que s'il est organisé de la façon la plus centralisée, si une discipline de fer confinant à la discipline militaire y est admise et si son organisme central est muni de larges pouvoirs, exerce une autorité incontestée, bénéficie de la confiance unanime des militants."



## Référence

## Liens 