MOC : 
Source : [[Léon Blum]]
Date : 2023-07-17
***

## Résumé
Figure du socialisme, il est président du Conseil de juin 1936 à juin 1937 et de mars à avril 1938, puis président du Gouvernement provisoire de la République française de décembre 1946 à janvier 1947. 

## Référence

## Liens 
[[Notes/Socialisme]]
[[IIIe République]]