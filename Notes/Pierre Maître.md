MOC : 
Source : [[Pierre Maître]]
Date : 2023-06-11
***

## Résumé
Pierre Maître, né le 9 novembre 1945 à Reims et mort le 6 juin 1977 à Reims des suites de ses blessures, est un ouvrier syndicaliste de la [[CGT]] qui fut grièvement blessé à la tête par un commando d'extrême droite affilié au [[Service d'action civique]], à Reims le 5 juin 1977, devant son usine.

## Référence

## Liens 