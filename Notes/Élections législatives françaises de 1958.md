[[Note Permanente]]
MOC : [[Histoire]] [[POLITIQUE]]
Source : [[Élections législatives françaises de 1958]]
Date : 2022-07-08
***

## Résumé
**Les élections législatives françaises de 1958** ont lieu les 23 et 30 novembre 1958. Elles visent à choisir les députés de la Ire législature de la [[Cinquième République]]. À l’issue du scrutin, 579 parlementaires (avec trois sièges vacants) sont ainsi élus. Elles sont précédées d'un mois par le référendum du 28 septembre 1958, qui a posé une partie des fondements de la Cinquième République et se traduit par un vote beaucoup plus favorable au gaullisme que celui des législatives précédentes, 1951 et 1956, même si l'abstentionnisme est significatif chez une partie des jeunes. 

La campagne électorale voit toutes les grandes formations politiques se réclamer du gaullisme, depuis le parti socialiste jusqu’à l’_Union pour la nouvelle République_ (UNR), créée à la veille des élections pour rassembler les gaullistes, jusqu’alors dispersés entre diverses obédiences. Seuls prennent nettement parti contre le gaullisme le parti communiste et les candidats de l’[[Union des Forces démocratiques]].

Du premier tour des élections, on peut tirer quatre conclusions :

1) Une assez forte abstention (environ 23 %) qui révèle que l’opinion publique demeure très méfiante envers les partis politiques, alors qu’elle fait une **large confiance au général [[Charles de Gaulle]]** (il n’y a eu que 15 % d’abstentions au référendum de septembre).
2) Une lourde défaite des adversaires du gaullisme, à gauche comme à droite. Le poujadisme et l’extrême-droite qui avaient effectué une percée en 1956 s’effondrent, ne rassemblant que 2,6 % des suffrages (dont 0,5 % pour les candidats se réclamant de Pierre Poujade). Le parti communiste qui n’était jamais descendu au-dessous de 25 % sous la IVe République tombe à 19,2 %, perdant ainsi le tiers de ses électeurs. Quant à l’Union des Forces démocratiques, en dépit du prestige de ses dirigeants, elle connaît une déroute totale qui vaut condamnation politique avec 1,2 % des voix.
3) Les partis qui s’étaient identifiés à la IVe République connaissent la stagnation ou l’effondrement, stagnation pour la SFIO (15,7 %) ou le MRP (11,1 %), effondrement pour les radicaux (7,3 %).
4) Enfin les élections manifestent une forte poussée à droite parmi les partis qui semblent le mieux s’identifier au mouvement gaulliste. Les Indépendants remportent un succès spectaculaire avec 22,1 % des voix, cependant que l’UNR, inconnue quelques jours auparavant, rassemble plus de 20 % des suffrages exprimés.

Finalement, dans l’Assemblée élue en 1958 il ne reste plus que 10 communistes, 44 socialistes, 23 radicaux, 57 MRP. La gauche et le centre sont littéralement balayés. En revanche les vainqueurs du premier tour sont les triomphateurs du second : l’UNR fait élire 198 députés et ses alliés de la droite classique, les Indépendants 133. L’Assemblée de 1958 est ainsi fortement marquée à droite, cependant que quelques-uns des personnages-clés de la IVe République disparaissent de la représentation parlementaire : Pierre Mendès France, François Mitterrand, Robert Lacoste, Edgar Faure, Joseph Laniel, Gaston Defferre, etc.

Qui sont les nouveaux élus ? Aux yeux de l’opinion publique, l’Assemblée possède une forte majorité de droite qui se rassemble sur le thème de « l’Algérie française ». Pourtant, ce sont les gaullistes « historiques », ceux de la Résistance, qui constituent le gros des élus et des cadres du nouveau parti. Son ciment n’est nullement l’« Algérie française » mais la fidélité au général de Gaulle.


## Référence
[[Histoire de la France au XXe Siècle - Livre 3]]

## Liens 
[[Politque française]]