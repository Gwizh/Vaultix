MOC : 
Source : [[Émeutes de Qamichli]]
Date : 2023-07-03
***

## Résumé
Les émeutes de Qamichli sont des affrontements entre les Kurdes et des forces de l'ordre syriennes ayant eu lieu en mars 2004 dans le nord-est de la [[Syrie]], en particulier dans les deux villes de Hassaké et Qamichli. Elles ont fait 43 morts, des centaines de blessés et environ induit 2 000 arrestations. Ces troubles sont à l'origine liés à des affrontements entre des supporters de football après un match entre l'équipe de Qamichli, Al-Jihad, et celle de Der Ez Zor, Al-Fatwa. Puis à la répression après les funérailles des victimes de ces affrontements. Il s'ensuit plusieurs incendies de bâtiments institutionnels, ainsi que des pillages et des meurtres à Hassaké et Qamichli. Ces troubles ont gagné la région d'[[Alep]] et touche notamment les villes d'Afrin, d'Amouda, de Derik. 

## Référence

## Liens 
[[Kurdistan]]