#MIE 

# Grandes parties provisoires
## Complexité de l'humain
**Nous ne maîtrisons pas notre corps autant qu'on le pense**

## Des problèmes du contrôle du vivant
**Problématique de normalisation du vivant**

# Concepts à trier
### Organisme composée d'organismes
- Impact des bactéries et virus sur notre corps.
### Nos choix ne viennent pas que de nous
- Neurologie et prise de décisions.
### Tout le monde est unique
- On a tous une combinaison de "problèmes", de caractéristiques qui ne peuvent être prévisible ou complètement compris.
### Aucun système politique ne peut englober tous les besoins
- Cette complexité est infinie et tout système fait par l'humain est fini.
### Concept de violence
- Violence symbolique qui vient d'un système qui ne nous est pas adapté. On souffre parce qu'on ne rentre pas dans les cases. D'autant plus si ces cases apparaissent naturelles ou les seules valables.
[[Pierre Bourdieu]]
### Intersectionnalité
Faire converger les luttes car nous sommes tous hors des normes imposées. Il faut pouvoir voir que nous avons des intérêts en commun sans pourtant pouvoir se comprendre les uns les autres. Nous pouvons radier des groupes qui nous semblent autoritaires ou oppressants. 
[[Intersectionnalité]]
### Auto-détermination
- Pouvoir décider de nous-mêmes de ce dont nous avons besoin. Nous sommes seuls à comprendre notre expérience.
[[Autodétermination des peuples]]
### Démocratie complète
Il faut pouvoir faire remonter les besoins
### Quid des experts ?
- Nous allons bien voir un psy ou des médecins pour nos problèmes physiques. Nous devons organiser la société d'une manière complexe qui ne peut être réalisé qu'avec de la technique. Il faut que ces experts soient capables de faire des constats et leur travail sans prendre des décisions à la place des gens. On ne peut pas tout gérer à nos vies. On ne peut pas vérifier tout. 
Il faut confier les tâches aux autres. Si on devait gérer l'infinie complexité de nos vies tout seul, on serait bien incapable de faire quoi que ce soit.
Mais quand un service nous est rendu, il faut pouvoir contrôler absolument ce qui nous est fait. Il faut qu'on puisse stopper ce service quand on le souhaite, ou changer de service.
Que ce qui est public, lié à la société, soit entièrement sous notre contrôle pour empêcher le plus possible la violence.
### Détruire le paternalisme
- Volonté de faire pour les autres, d'aider les autres en partant du principe qu'ils ne savent pas faire. Ça ne signifie pas qu'on ne peut pas demander que certains s'occupent de certaines parties de nos vies.
### Détruire l'idée que nous ne faisons rien quand on ne nous donne rien à faire
- [[Spinoza]], [[Frédéric Lordon]] et [[Bernard Friot]]
### Il n'y a pas d'assistés ! Nous avons de la valeur par notre existence même
- [[Bernard Friot]]
### La naïveté
### Gros mot : Dictature du prolétariat
[[Dictature du prolétariat]]
### Détruire l'Autoritarisme
[[Autoritarisme]]
### Rôle de l'école dans tout ça