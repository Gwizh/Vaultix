[[Note Permanente]]
MOC : [[Histoire]] [[POLITIQUE]]
Source : [[Référendum sur l'autodétermination de l'Algérie]]
Date : 2022-07-08
***

## Résumé
Le référendum sur l'autodétermination de l'Algérie a eu lieu le 8 janvier 1961. Il ouvre la voie à une [[Indépendance de l'Algérie]].

La question posée aux Français était :

> « Approuvez-vous le projet de loi soumis au peuple français par le président de la République et concernant l'autodétermination des populations algériennes et l'organisation des pouvoirs publics en Algérie avant l'autodétermination ? »

L'article 1 de ce projet de loi prévoit que, lorsque les conditions de sécurité le permettront, **le destin politique de l'Algérie par rapport à la République française sera décidé par les populations algériennes**. Son article 2 prévoit que, jusqu'à l'autodétermination, des décrets pris en conseil des ministres organiseront l'institution d'un organe exécutif et d'assemblées délibérantes en Algérie, et la coopération entre communautés. 

Le taux de participation est de 76 % en métropole et de 59 % en Algérie. **75 % des votants votent « oui »**. Pour l'Algérie seule, 70 % votent « oui » et 30 % votent « non ».

## Référence
[[Histoire de la France au XXe Siècle - Livre 3]]

## Liens 
[[Histoire de l'Algérie]]
[[Guerre d'Algérie]]