MOC : 
Source : [[Nicolas Machiavel]]
Date : 2023-06-23
***

## Résumé
né en 1469 et mort en 1532

Écrit [[Le Prince]] pour conseiller le fils [[Laurent de Médicis]]
Décrit l'art de la politique pour garder le pouvoir le plus possible avec toutes les méthodes possibles.

Il ne suffit pas d'être bon, juste et moral, il faut être efficace et rusé. Il est pragmatique, l'autorité divine n'existe pas.


## Référence

## Liens 