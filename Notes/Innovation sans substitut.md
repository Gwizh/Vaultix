MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081614
Tags : #Fini
***

# Innovation sans substitut
p15. L'histoire des sciences se concentre sur les technologies et innovation qui ont eu un impact presque immédiat. Exemple : Bombe atomique, IPhone. On ne se concentre pas sur l'usage de la technologie, ou même du besoin d'origine. De ce fait, on oublie les substituts qui existaient. On imagine les nouvelles technologies qui arrivent comme des révolutions et on arrive pas à imaginer comment les gens faisaient avant. Exemple : GPS, électricité.

Pourtant, même s'il y a des exemples valables à ces biais, la plupart des inventions n'ont pas inventé un nouveau besoin de rien, elles constituent pour la plupart en une nouvelle façon pour répondre à un besoin déjà existants. De plus, elles sont rarement révolutionnaires. La plupart des innovations mettent longtemps à se mettre en place et à vraiment répondre au besoin réel des gens. L'électricité a mis plusieurs décennies à "remplacer" le gaz, l'aviation n'a quasiment pas servit à transporter des gens pendant (?).

## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]