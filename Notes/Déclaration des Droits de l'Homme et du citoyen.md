MOC : 
Source : [[Déclaration des Droits de l'Homme et du citoyen]]
Date : 2023-05-17
***

## Résumé
Déclaration des Droits de l'Homme et du citoyen

Article 12 : La garantie des droits de l'homme et du citoyen nécessite une force publique : cette force est donc instituée pour l'avantage de tous, et non pour l'utilité particulière de ceux auxquels elle est confiée. 

## Référence

## Liens 