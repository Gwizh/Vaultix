MOC : 
Source : [[À l’Assemblée, l’homme clé des financements russes du RN invoque le « secret des affaires »]]
Date : 2023-05-05
***

## Résumé
Auditionné jeudi par les députés membres de la commission d’enquête sur les ingérences étrangères, l’intermédiaire du prêt russe du [[Rassemblement national]], [[Jean-Luc Schaffhauser]], a multiplié digressions et thèses douteuses.

L’audition de ce consultant international, ancien eurodéputé du Front national, était très attendue. Avant de claquer la porte du parti en 2018, Jean-Luc Schaffhauser était l’homme clé des financements du RN. C’est lui qui a négocié les deux prêts qui ont sauvé le Front national de la faillite – tenus secrets et révélés par Mediapart. Celui [de 9 millions d’euros](https://www.mediapart.fr/journal/france/dossier/largent-russe-du-rassemblement-national) contracté en 2014 auprès d’une banque russe. Celui [de 8 millions d’euros](https://www.mediapart.fr/journal/france/041019/un-pret-emirati-de-8-millions-d-euros-sauve-le-rassemblement-national) octroyé par un Français lié aux autorités russes par de mystérieux accords, le tout _via_ une une banque basée aux Émirats arabes unis.

## Référence
https://www.mediapart.fr/journal/france/050523/l-assemblee-l-homme-cle-des-financements-russes-du-rn-invoque-le-secret-des-affaires

## Liens 
[[Russie]]