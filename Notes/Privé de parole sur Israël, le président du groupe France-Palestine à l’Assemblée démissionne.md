MOC : 
Source : [[Privé de parole sur Israël, le président du groupe France-Palestine à l’Assemblée démissionne]]
Date : 2023-05-05
***

## Résumé

[[Jean-Charles Larsonneur]], député [[Horizons]], a démissionné jeudi de la présidence du groupe d’étude France-[[Palestine]] à l’[[Assemblée nationale]]. Alors qu’il devait prendre la parole sur la résolution communiste dénonçant un « [[Apartheid]] » en [[Israël]], le président du groupe Horizons l’a remplacé par une collègue.

Une démission aussi inattendue que commentée, jeudi, dans les couloirs de l’Assemblée nationale. Le député Horizons Jean-Charles Larsonneur a annoncé qu’il quittait la présidence du groupe d’étude à vocation internationale France-Palestine (GEVI).

La raison de sa démission ? Alors qu’il était décidé depuis un mois qu’il prenne la parole lors du débat sur _« l’institutionnalisation »_ par Israël d’un _« régime d’apartheid »_ organisé jeudi 4 mai dans l’hémicycle à l’initiative du groupe communiste, l’élu du Finistère a été retiré de la liste des orateurs par le président de son groupe, Laurent Marcangeli.

## Référence

## Liens 