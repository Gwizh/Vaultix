[[Article]]
MOC : [[Noz/ECONOMIE]]
Titre : [[EDF - les contours de la révolution qui vient]]
Auteur : [[Les Echos]]
Date : 2022-06-09
Lien : https://www.lesechos.fr/industrie-services/energie-environnement/edf-les-contours-de-la-revolution-qui-vient-1412017
Fichier : 
***

## Résumé
Prioritaire pour le gouvernement, la réforme d'EDF bénéficie d'un nouveau souffle grâce au contexte de crise énergétique. Pour éviter de s'attirer les foudres des syndicats ou de l'opposition, Emmanuel Macron met en avant un projet de nationalisation fédérateur.

Plusieurs fois ajournée pendant le précédent quinquennat, la réforme d'EDF semble aujourd'hui inévitable. Le champion tricolore de l'atome est au plus mal à cause de [la faiblesse historique de la disponibilité de son parc de réacteurs](https://www.lesechos.fr/industrie-services/energie-environnement/electricite-le-parc-nucleaire-risque-de-tourner-au-ralenti-pendant-plusieurs-annees-1407726) , ce qui entraîne sa dette, déjà lourde, par le fond. La guerre en Ukraine a, par ailleurs, mis sous le feu des projecteurs les besoins pressants en énergie abondante, abordable et souveraine.

Pour le gouvernement, l'alternative 100 % publique offre de nombreux avantages. Politiquement d'abord : engager une nationalisation permet de relancer la réforme très controversée d'EDF sous un jour plus favorable - puisqu'à gauche comme à droite, on considère que le nucléaire doit être sanctuarisé dans les mains de l'Etat. Pour les syndicats d'EDF, c'est aussi un symbole essentiel. « La nationalisation marquerait un retour aux fondamentaux, au groupe fondé par Marcel Paul », pointe une source proche des pouvoirs publics. De fait, c'est une loi de nationalisation qui a donné naissance à EDF en 1946.

En réalité, parler de nationalisation pour EDF - déjà détenu à 84 % par l'Etat et largement contraint par les décisions du gouvernement - est un abus de langage. Dans cette opération, ce qui est surtout visé par l'exécutif, c'est la sortie d'EDF de [la Bourse](https://www.lesechos.fr/industrie-services/energie-environnement/prix-de-lelectricite-edf-demande-a-letat-de-faire-machine-arriere-1406577) .

Les objectifs de cette opération qui pourrait être combinée à augmentation de capital sont multiples : rassurer les investisseurs, faire baisser ses coûts de financement, bénéficier d'une position de négociation plus favorable avec Bruxelles sur la réforme du prix régulé de l'électricité nucléaire (Arenh) ou encore financer les nouveaux EPR.

## Référence
[[Energie]]
[[EDF]]

## Liens 