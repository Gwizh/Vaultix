MOC : 
Source : [[Histoire de l'électricité au Royaume-Uni]]
Date : 2023-04-07
***

## Résumé

## Références
#Alire #Adam Heavy Current Electricity in the United Kingdom
https://www.twinkl.fr/teaching-wiki/the-history-of-electricity
https://ieeexplore.ieee.org/abstract/document/6545260

### Trucs en plus
https://www.doc-transition-energetique.info/Record.htm?record=19122400124919406829&idlist=285
https://www.doc-transition-energetique.info/Record.htm?record=19132009124919502819
https://www.persee.fr/doc/helec_0758-7171_1983_num_1_1_883
https://fr.wikipedia.org/wiki/Fabienne_Cardot
https://www.ammareal.fr/livre/3524854-d-546-416-histoire-de-l-electricite-en-france-tome-1-espoirs-et-conquetes-1881-1918-9782213027807.html
https://id.elsevier.com/as/authorization.oauth2?platSite=SD%2Fscience&scope=openid%20email%20profile%20els_auth_info%20els_idp_info%20els_idp_analytics_attrs%20els_sa_discover%20urn%3Acom%3Aelsevier%3Aidp%3Apolicy%3Aproduct%3Aindv_identity&response_type=code&redirect_uri=https%3A%2F%2Fwww.sciencedirect.com%2Fuser%2Fidentity%2Flanding&authType=SINGLE_SIGN_IN&prompt=login&client_id=SDFE-v4&state=retryCounter%3D0%26csrfToken%3Df4b60a10-d045-4c54-b832-e098d601a1f5%26idpPolicy%3Durn%253Acom%253Aelsevier%253Aidp%253Apolicy%253Aproduct%253Aindv_identity%26returnUrl%3D%252Fscience%252Farticle%252Fpii%252FS0039368112001021%26prompt%3Dlogin%26cid%3Datp-ec92387b-44f3-4fd2-ab2d-9201131179cb&els_policy=idp_policy_indv_identity_plus
https://www.google.com/search?q=Heavy+Current+Electricity+in+the+United+Kingdom&safe=active&rlz=1C1GCEU_enFR1049FR1049&ei=DVvTZJ-sGrKzkdUPoseUgA8&start=10&sa=N&ved=2ahUKEwjf5ubAoM-AAxWyWaQEHaIjBfAQ8tMDegQIBxAE&biw=1920&bih=931&dpr=1


## Liens 