[[Note Permanente]]
MOC : [[Histoire]] [[Politique]]
Source : [[Référendum constitutionnel français de 1958]]
Date : 2022-07-08
***

## Résumé
Référendum pour l'adoption de la [[Constitution de la Ve République]].

La campagne pour le référendum révèle l’ampleur des soutiens dont dispose le général de Gaulle. Sauf le parti communiste, tous les grands partis préconisent le « oui », le MRP, la SFIO (qui réunit pour la circonstance un congrès extraordinaire), le parti radical-socialiste, les Indépendants, les Républicains-Sociaux.

Le 28 septembre, la Constitution est adoptée par 79,25 % des voix.

## Référence
[[Histoire de la France]]
[[Politque française]]

## Liens 