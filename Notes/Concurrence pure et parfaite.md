MOC : 
Source : [[Concurrence pure et parfaite]]
Date : 2023-06-28
***

## Résumé
La concurrence pure et parfaite (CPP) ou, tout simplement, la concurrence parfaite, correspond à la théorie de la formation du prix élaborée au XIXe siècle par les économistes néo-classiques. La concurrence pure et parfaite est censée permettre l’équilibre sur tous les marchés sous des conditions suffisantes très particulières. La concurrence pure et parfaite représente un des deux cas extrêmes de structures de marché étudiés par les économistes néoclassiques, le second étant le cas de monopole. 

### Détails
Selon une vieille habitude, qui ne persiste qu'en France, la concurrence « ==pure et parfaite== » comporte, comme son nom l'indique, deux volets : la « pureté », d'une part, la « perfection », de l'autre. Plus précisément, la distinction est faite entre d'une part, la « pureté de la concurrence » (atomicité de l'offre et de la demande, homogénéité du produit, libre entrée et sortie du marché) et la « perfection du marché » (transparence des marchés, mobilité des facteurs de production) ; cette distinction amène à parler de « concurrence pure sur des marchés parfaits ».


## Référence

## Liens 
[[École néoclassique]]