MOC : 
Titre : [[Stress hydrique]]
Date : 2023-08-22
***

## Résumé
Le stress hydrique, ou stress osmotique, est le stress abiotique (c'est-à-dire lié aux conditions physiques du milieu) subi par une plante placée dans un environnement amenant à ce que la quantité d'eau transpirée par la plante soit supérieure à la quantité qu'elle absorbe. Ce stress se rencontre en période de sécheresse du sol ou de l'atmosphère, mais aussi lors de l'augmentation de la salinité du milieu (conduisant à l'abaissement du potentiel osmotique du milieu) ou en période de grand froid.

C'est un concept qu'on pensait propre à l'animal mais qu'on a redécouvert chez les plantes. C'est un des très nombreux points communs des vivants. 

## Références

## Liens 
[[Sécheresse]]
[[Eau]]