MOC : 
Source : [[Indigène]]
Date : 2023-08-09
***

## Résumé
"Indigène" désigne une personne ou une communauté originaire d'un lieu déterminé, où elle vit et-ou auquel elle est attachée par un lien immanent ; qui se sent une "propriété" de la terre et non le propriétaire de celle-ci.

## Référence

## Liens 