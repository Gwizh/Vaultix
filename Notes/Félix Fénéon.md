MOC : 
Source : [[Félix Fénéon]]
Date : 2023-07-11
***

## Résumé
Félix Fénéon, né à Turin (Italie) le 29 juin 1861 et mort à Châtenay-Malabry (Seine) le 29 février 1944, est un critique d'art, journaliste, collectionneur d'art et directeur de revues français.

[[Anarchie|Anarchiste]], il s'engage dans le mouvement libertaire dès 1886 et collabore à de nombreux journaux ou revues, comme L’En-dehors (dont il assume la direction pendant l'exil de son fondateur Zo d'Axa à Londres). En 1894, il est inculpé, lors du procès des Trente.

Jean Paulhan a écrit un essai intitulé Félix Fénéon ou le critique : Félix Fénéon incarne en effet avant tout le critique au goût très sûr, qui savait que Rimbaud, Jules Laforgue, Stéphane Mallarmé, Paul Valéry et Guillaume Apollinaire seraient les grands écrivains de son temps et non Sully Prudhomme ou François Coppée, et qui rendait justice aux impressionnistes puis aux postimpressionnistes quand ses confrères encensaient l'art académique. 

Go va voir sur wiki son procès est trop drole.

## Référence

## Liens 