MOC : 
Source : [[Hétéronomie]]
Date : 2023-04-07
***

## Résumé
L’hétéronomie est le fait qu'un être vive selon des règles qui lui sont imposées, selon une « loi » subie. L'hétéronomie est l'inverse de l'[[Autonomie]], où un être vit et interagit avec le reste du monde selon sa nature propre.

## Référence

## Liens 