Cher.e.s collègues, étudiantes et étudiants,

Au mois de mai dernier, le Club INSAtisfait.e.s, association féministe de l’Amicale des Élèves de l’INSA Rennes, a lancé une enquête interne anonyme auprès de la communauté étudiante pour faire état des violences sexistes et sexuelles sur le campus, soutenue par la direction de l’établissement.

673 étudiantes et étudiants ont participé à ce sondage et les premiers résultats mettent en lumière des comportements répréhensibles et sexistes fréquents, entre étudiant.e.s mais également de la part des personnels, que cela soit sur les lieux de vie des élèves (résidences, foyer des étudiant.e.s), dans les salles de cours ou encore sur les lieux de stage. Parmi les faits les plus graves, des viols ont été signalés. L’enquête fait également état d’agressions sexuelles, lors de temps festifs, majoritairement par des personnes identifiées comme extérieures à l’établissement. Concernant ces faits graves, j’ai effectué un signalement auprès du Procureur de la République selon les dispositions de l’article 40 du Code de procédure pénale. J’encourage les personnes concernées à porter plainte et à se faire accompagner pour cela par la cellule « Harcèlement et autres violences sexistes et sexuelles », qui pourra, si elles le souhaitent, les diriger vers l’association SOS Victimes qui offre un accompagnement psychologique et juridique pris en charge par l’INSA Rennes.

Plus largement, les violences sexistes et sexuelles sur le campus concernent également la communauté LGBTQIA+ avec notamment des propos relevant de l’homophobie.

L’ensemble des résultats sera communiqué à la communauté INSA Rennes à la rentrée 2022/2023.

Notre établissement se mobilise, depuis plusieurs années, dans la lutte contre toutes les discriminations afin que chacun.e puisse accéder aux études supérieures et puisse vivre ses études dans le respect et avec les mêmes chances. Les résultats de cette enquête confirment que nous devons poursuivre ce travail et le renforcer afin de faire cesser ces agissements inadmissibles dans un établissement d’enseignement supérieur.

Voici un récapitulatif des actions qui sont menées dans notre institut afin d’agir contre les violences sexistes et sexuelles :

-   Depuis la rentrée 2021, une cellule « Harcèlement et autres violences sexistes et sexuelles » à destination des étudiant.e.s a été créée. Elle vise à répondre à TOUTES situations de violences sexistes et sexuelles commises sur le campus de l’INSA et/ou par une personne étudiant, travaillant ou résidant à l’INSA Rennes. La cellule est là pour prévenir et informer mais également pour encourager les personnes à solliciter une aide, un soutien, un accompagnement au sein de l’établissement. Nous invitons toutes les personnes victimes ou témoins de propos sexistes ou LGBTQIA+phobes, de harcèlement sexuel, d’agressions sexuelles ou de viols à se rapprocher de la cellule dédiée : etudiants-stopVSS@insa-rennes.fr
-   Un guide de lutte contre le sexisme à l’INSA Rennes, co-réalisé par Fabienne Nouvel, chargée de mission Égalité Femmes/Hommes de l’INSA Rennes, le Club INSAtisfait.e.s et le service Communication, est distribué à tous les étudiant.e.s qui intègrent l’INSA Rennes au moment de la rentrée universitaire. Ce guide illustre les situations les plus courantes de sexisme ordinaire, mais néanmoins inacceptables, rencontrées au cours du cursus de formation et propose quelques rappels élémentaires sur des comportements encore trop fréquents et banalisés.
-   Dès la rentrée universitaire, l’ensemble des élèves de 1re année est convié à participer à des conférences Santé, dont une portant sur la thématique « Liberté, Égalité, Sexualité » proposée par le Planning familial 35.  
-   En septembre 2021, nous avons aussi signé, pour 2 ans, une convention avec l’association SOS Victimes 35, afin de pouvoir proposer aux victimes un accompagnement juridique et psychologique.
-   Les associations étudiantes, que nous soutenons et accompagnons, agissent également au quotidien, sur le terrain en informant, en sensibilisant via des soirées thématiques, en suivant des formations de prévention.
-   Plus largement, l’INSA Rennes s’est engagé, depuis plusieurs années, auprès de 8 autres établissements d’enseignement supérieur rennais dans un groupe de travail dédié au sujet des violences sexistes et sexuelles. De nombreuses actions collectives de sensibilisation mais également de formation sont ainsi menées tout au long de l’année sur l’ensemble des campus rennais.

Les résultats de cette enquête nous rappellent qu’en 2022, le sujet des violences sexistes et sexuelles est toujours et plus que jamais d’actualité…. Même si ces résultats semblent, à première vue, moins alarmants que dans d’autres écoles d’ingénieurs, ils doivent être pris très au sérieux.

La lutte contre les violences sexistes et sexuelles sera l’une de mes priorités. Je m’engage ainsi personnellement à mettre en œuvre lors de mon mandat toutes les actions possibles afin de prévenir, prendre en charge les victimes et sanctionner les auteurs de ces violences qui n’ont pas leur place dans notre institut. J’envisage notamment de faire appel à des prestataires spécialisés afin de pouvoir bénéficier des meilleures pratiques de prévention et de prise en charge.

Je reste personnellement, ainsi que la cellule, à votre écoute, afin d’améliorer le fonctionnement des dispositifs de signalement des actes de violence, de discrimination, de harcèlement et d’agissements sexistes au sein de l’INSA Rennes. 

Bien cordialement,

Vincent Brunie

**Vincent BRUNIE  
Directeur**


[[Féminisme]]