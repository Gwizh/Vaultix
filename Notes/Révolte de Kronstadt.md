MOC : 
Source : [[Révolte de Kronstadt]]
Date : 2023-06-18
***

## Résumé
La révolte des marins de Kronstadta, contre le pouvoir bolchevique se déroule en Russie soviétique [[URSS]] en mars 1921.

Elle est le dernier grand mouvement contre le régime bolchevique, sur le territoire russe, pendant la guerre civile et la plus importante manifestation ouvrière d'opposition au communisme de guerre.

En 1917, les marins de Kronstadt sont à l'avant-garde, « foyer le plus ardent de la révolution d'Octobre ».

En 1921, les marins, soldats et ouvriers de Kronstadt, y compris de nombreux communistes déçus par la direction du gouvernement bolchevique, exigent une série de réformes et rejoignent les revendications des ouvriers de Petrograd en grève : élections libres des soviets, liberté de la presse et de réunion pour toutes les forces socialistes, suppression des réquisitions et rétablissement du marché libre.

Dénonçant la « dictature des commissaires bolcheviques  », les insurgés revendiquent la démocratie ouvrière et paysanne confisquée par le Parti communiste : « Tout le pouvoir aux soviets et non aux partis ».

Isolée du continent, cette révolte spontanée débute le 1er mars 1921 et est écrasée militairement deux semaines plus tard, le 18 mars, par l'[[Armée rouge]], sur ordre de [[Léon Trotsky]].

Au même moment se tient à Moscou le Xe congrès du Parti communiste de l'Union soviétique qui, comme en écho, accélère la mise en œuvre de la Nouvelle politique économique (NEP) qui remplace le « communisme de guerre ».

## Référence

## Liens 