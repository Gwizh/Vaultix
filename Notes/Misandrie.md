MOC : 
Source : [[Misandrie]]
Date : 2023-06-10
***

## Résumé
La misandrie (du grec ancien μῖσος / mîsos (« haine ») et ἀνήρ / anếr (« homme ») est un terme désignant un sentiment de mépris ou d'hostilité à l'égard des hommes. Ce terme est sémantiquement le correspondant inverse de celui de [[Misogynie]] (sentiment de mépris ou d'hostilité à l'égard des femmes).

## Référence

## Liens 