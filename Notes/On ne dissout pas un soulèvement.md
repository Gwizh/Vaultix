MOC : 
Source : [[On ne dissout pas un soulèvement]]
Date : 2023-07-23
***

## Résumé
#### Préface
La question des terres est sociale, écologique et coloniale. Tout ça réuni notamment dans les projets [[Colonialisme|coloniaux]] de [[TotalEnergies]] par exemple #MIE 

Exemples similaires :
- [[Bêcheux ou Digger]] 
- [[Mouvement des sans-terre]]
- Mouvement des paysans travailleurs de Loire Atlantique
- [[Révolte des Mau Mau]]

Contexte pour moi du futur : Après [[Sainte-Soline]], [[Gérald Darmanin]] a menacé de dissoudre les [[Soulèvements de la Terre]] mais ça a surtout créé un mouvement nouveau avec plein de gens qui s'en revendique. 

#### Accaparement Par [[Philippe Descola]]
Le développement du [[Capitalisme]] a été marqué par l'accaparement des biens communs par une minorité de personnes avec si besoin de la violence. L'exemple le plus connu est le [[Mouvement des enclosures]] mais on pourrait prendre l'expropriation de la Gascogne par [[Napoléon]]. Mais le plus important est la [[Colonialisme]] des Amériques par les Européens qui est sans égal dans l'Histoire. 

Et puis [[Néocolonialisme]] moderne avec [[TotalEnergies]] en [[Guyane]] 

#### [[Autonomie]] par [[Jérôme Baschet]] 
Pour les [[Zapatistes]], la terre est la base matérielle de l'autonomie. Travail collectif sur les terres qui compte. [[Conseils de bon gouvernement]] : fédèrent de nombreuses communes autonomes et rendent leur propre justice.

Grande attention à la déspécialisation des tâches, la révocabilité des mandats et les allers retours entre assemblées et conseils.

L'autonomie zapatiste rompt ainsi avec le modèle classique de la révolution, guidée par une avant-garde éclairée et faisant de l'État le levier de la transformation. Au contraire, c'est à partir d'une dignité partagée par toutes et tous - à partir aussi de l'attachement à des formes de vie autodéterminées - que se déploie la capacité collective à s'organiser par soi-même.

Lutter contre et œuvrer pour sont indissociables. Construire sans résister serait aussi vain que résister sans construire serait stérile

#### [[Base arrière]]
Ne pas individualiser les luttes mais ne pas généraliser non plus. Plusieurs pôles en fonction des besoins. Legal Team, Riot Fights Sexism, médic, soutien psycho-émotionnel, garderies, pôle handidévalidiste. Action avant pendant et après.

#### Béton par [[Lea Hobson]]
La bétonisation, symbole de la croissance économique est aussi une arme du pouvoir politique qui utilise nos terres pour créer du capital. Il représente 8% des émissions de CO2. 50 000 hectares de terres en France par an. L'industrie du BTP représente 39%. Consommation de sable terrible. Voir [[La tête dans le sable]]. 

Campagne [[Fin de chantier]] d'[[Extinction Rébellion]] en 2019. [[Grand Péril Express]] en particulier.

[[Béton Lyonnais]]

#### [[Cantines en lutte]]
Alimentation local avec du matériel partagé. Une super organisation.

#### Commune par [[Kristin Ross]]
Lutte commune qui regroupe plein de gens différents.
Fait le rapprochement avec la [[Lutte de Sanrizuka]].
Par leur souplesse et leur respect mutuel, par un sens clair de l'ennemi partagé, et surtout par dse gestes de coopération répétés, elles parviennent à constituer un front commun.
De plus les soulèvements sont un mouvement que n'importe qui peut rejoindre. Ni parti, ni classe, ni rien sauf une structure organisée.

La forme-commune est une manière de faire vivre les communs, qui éclot quand l'état se retire. Les communes sont toujours aussi marquées par le pragmatisme qu'elles sont ancrées dans une situation locale.
[[Commune de Paris]]

#### Composition par [[Édouard Glissant]] et [[Blue Monk]]
Composition plutôt que Front ou Alliance ou Union car ce n'est pas une jonction temporaire mais bien une structure durable inter-orga. L'écologie institutionnelle ne peut être autrement. 
[[Gilles Deleuze]] dit de la composition que c'est cette étrange unité qui ne se dit que du multiple. Ce n'est pas une homogénéité qui efface les singularités.

#### Désarmement par [[Lotta Nouqui]]
En 1975 a commencé l'idée de sabotage pour l'écologie avec [[Fessenheim]] et un roman : Le gang de la clé à molette. [[Ecodefense]] en 1985

Le début des années 2020 voit [[Youth for Climate]] et Extinction passer de contestations symboliques à de réels sabotages. Au même moment le banger Comment saboter un pipeline sort et tue le game. 

En 2021, on parle plutôt de désarmement pour montrer la portée éthique des actions.

> La procédure de dissolution vient alors tenter d'endiguer une hypothèse politique qui menace de se concrétiser : le désarmement sera aux révoltes écologistes du XXIe siècle ce que le sabotage, théorisé par [[Émile Pouget]], fut à certaines grandes grèves ouvrières du début du XXe, un prérequis autant qu'une colonne vertébrale. 

#### [[Écoféminisme|Écoféministes]] par [[Virginie Maris]]
Ce concept est déjà une composition et rien d'autre. Théorie et pratique, bas niveau au niveau.
#Alire 
Le sexocide des sorcières de d'Eaubonne
La Mort de la nature de Carolyn Merchant 
Francis Bacon pensait "la nature comme une femme publique" et se donnait pour mission de "la mater, pénétrer ses secrets et l'enchaîner selon ses désirs". 

#### État (toujours) colonial par [[Françoise Vergès]]
L'État colonial apporte la destruction mais aussi la stérilisation. L'histoire et les sciences naturelles détruisent les savoirs des pays colonisés, créant une hégémonie occidentale. Il n'y a qu'un sens au progrès. C'est une destruction à tous les étages et elle continue aujourd'hui. 
[[Mayotte]] : manque d'eau, déforestation élevée, stérilisation proposée. Légion étrangère protège les intérêts impérialistes de l'industrie fossile. 

#### Foncier par [[Tanguy Martin]]
Part des travailleurs de la terre est passé de 31 à 2.5% de la population depuis 1950. Un tiers des oiseaux et on ne sait combien d'insectes. D'ici 2030, 20% des terres devront changer de mains avec 'es départs à la retraite. La taille des fermes augmentent et il faut en moyenne 275000€ de capital professionnel. Plein d'associations qui veulent éviter ça. 

#### [[Indigène]] par [[Eduardo Viveiros de Castro]] 
"Indigène" désigne une personne ou une communauté originaire d'un lieu déterminé, où elle vit et-ou auquel elle est attachée par un lien immanent ; qui se sent une "propriété" de la terre et non le propriétaire de celle-ci.

En français courant, trop lié au [[Colonialisme]].

Les indigènes sont aussi ces peuples qui n'ont pas rejoint de gré ou de force la marche unilinéaire du [[Progrès]], et qui seraient restés emprisonnés dans le passé révolu de l'espèce. L'extra-modernité devient pré-modernité. 

[[Vine Deloria Jr]] pense que la différence ontologique entre les indigènes aux [[États-Unis]] et les américains sont la différence de priorité entre temps et espace. Les américains sont très tournés sur le temps qui passe et le progrès que ça implique, c'est toujours un mouvement en avant. Tandis que les indigènes sont attachés à leur espace et font tout par rapport à lui. 

Deux tendances de l'indigénisme en Europe : les premiers, xénophobes qui défendent un espace arbitraire et son [[État]]. Les deuxièmes se rapprochent de leur territoire en opposition à l'État. Ces peuples sont répartis sur 20% de la surface terrestre. 476 millions de personnes, 6% de l'ensemble des humains, plus que l'[[Union européenne]]. Englobés dans la population des États-nations modernes en tant que "minorités ethniques". Territoires qui abritent 80% de la biodiversité de la planète. 

Premier rempart de la défense de la [[Nature]]. 

#### [[Internationalisme]] par pleins de groupes (flemme de noter mais faudrait)
Les [[Méga bassines]] ont déjà été implémenter au [[Chili]] et ce qu'il en résulte est que les villageois doivent maintenant faire venir leur eau en camion citerne. 

"No es sequia, es saqueo!" : Ce n'est pas une sécheresse mais un pillage.

40% de la population connait une pénurie d'eau.

[[Guerre de l'eau (Bolivie)]]

Militants venus de partout à Sainte Soline : [[Chili]], [[Mali]], [[Kurdistan]], autochtones, [[Mexique]], [[Italie]]

Une grenade toutes les deux secondes.

[[Déclaration des droits des peuples autochtones ou UNDRIP]] et [[Déclaration des Nations unies sur les droits des paysans et des autres personnes travaillant dans les zones rurales ou UNDROP]]

Capitalocène par [[Gaïa Marx]] et [[Terra Zassoulitch]]
Réexpliquation d'[[Anthropocène]], Impérialisme, Capitalisme. 

Il y a deux noms : I. Wallerstein et J. Moore

#### [[Luddisme]] par [[François Jarrige]]
Révolte anti-industriel du début du XIXe siècle. Étiquette péjorative qu'on collait à ces gens pour dénoncer leur "archaïsme". Première révolte ouvrière du pays. Ned Ludd était une figure de justicier violent. La mobilisation commençait souvent par une pétition ou une lettre enjoignant le patron à abandonner telle machine.
#### [[Méga bassines]] par Julien Le Guet
Pieuvre [[FNSEA]]. Paysans qui deviennent exploitant agricole. 
#### [[Naturalistes des terres]]
Compter les insectes, oiseaux et autres et former les jeunes générations à faire de même. De moins en moins, tous les ans. Ils font tout ce qui est légalement et administrativement possible. Naturalisme comme prise de conscience de la nature autour de nous. Reprise de conscience de l'espace autour de nous. Engagement collectif. 
#### Outre-mer par [[Malcom Ferdinand]]
Le 27 mars 2023, deux jours après [[Sainte-Soline]], les forces de police de la [[Martinique]] évacuaient un campement anti-[[Chlordécone |chlordécone]] situé juste devant la Cour d'appel de Fort de France. Campement qui s'appelle Lakou Kont Non-Lieu. 90% des Antillais sont contaminés. L'ignorance n'est pas une excuse. Les collectifs locaux sont condamnés, criminalisés tandis que les fruits sont dans les supermarchés français.
Les soulèvements de la terre seront décoloniaux ou ne le seront pas.
Suivons les mots de [[Franz Fanon]] et d'[[Aimé Césaire]].

#### Paysannes et paysans par [[La Confédération paysanne]]
Être paysan, c'est être résistant
Ils en veulent un million. Préserver notre commun, ensemble.

#### Prises de terre par [[Christophe Bonneuil]]
[[Rosa Luxemburg]] avait vu la première que le [[Capitalisme]] est né d'une prise de terres mais surtout qu'il ne peut pas vivre sans. Un nouveau régime politique implique un coup de force foncier. À l'ère industriel, cette prise de terre s'opère au nom de la production. Les autres territoires sont vus comme inexploités, vu comme pas prise en charge comme il faut, vu comme une opportunité gâchée, ect... Expropriation interne aussi évidemment. On a des terres qu'il faut développer pour les rendre productive. Et on suit la logique des industries, il faut faire plus avec moins, moins de personnes, en donnant tout à une minorité. Toujours l'efficacité en premier en retirant toute externalité. Tous les vivants inutiles sont détruits.
Le Capitalisme a fait l'exploit de nous faire sortir de l'[[Holocène]].
Super métaphore avec la prise de terre. Par l'accaparement du foncier, le capitalisme va être difficile à déloger.

#### [[Queer]] par [[Cy Lecerf Maulpoix]]
Queer est une espèce menacée en gros, c'est pas un bon mot mais j'imagine ça comme ça. Faut sauver la biodiversité chez l'humain aussi.

#### [[Résistance]] par [[Kassim Niamanouch]]
Les soulèvements sont une forme assez nouvelle de lutte. Elle recoupe : le partage d'outils de formation, d'enquête et d'autodéfense, le renforcement de luttes locales, établir des sortes de maquis, pour créer de nouveaux espaces de vie communs. 

#### [[Saisonnalité]] par [[Alessandro Pignocchi]]
Idée encore existante dans les pays du [[Sud Global]] qui veut que les citadin.e.s aillent travailler dans les champs en fonction de la main-d'œuvre nécessaire et donc des saisons. Les hinuits avaient par exemple deux modes de vie complètement différent entre l'hiver et l'Ete. Les [[Nanbikwara]] de [[Claude Lévi-Strauss]] aussi. Créer Cette structure quelque part nécessite de créer des institutions adaptées ce qui est contraignant mais radical. De plus, une telle organisation créerait de nouveaux liens, une nouvelle société. Plus des liens familiaux ou religieux, ce serait des liens d'usages, des façons d'habiter et de créer une autonomie. De plus, ça permet de désenclaver les villes et de recréer de l'attraction pour les espaces ruraux. 

#### [[Soin]] 
Concept de corps-propriétaires. 

#### Subsistance 
Ancienne forme de production commune. Ringardisé voire criminalisé par le pouvoir. 

#### [[Technologie]] 
Gros problèmes des paysans est qu'ils sont aujourd'hui des exploitants, utilisant des technologies ne venant pas d'eux. Il faut repartir sur des technologies plus simples, plus sain et qui peuvent être développées en autonomie. [[CUMA]] pour partager les outils et les faire réparer. Technologie n'est pas neutre car elle signifie une dépendance aux moyens de production ou les possédants. 

#### Urgence Climatique
En 2022, le président de l'Autorité environnementale, chargé de fournir des avis argumentés sur les grands projets d'aménagement, a conclu que la "transition énergétique" n'a pas été amorcée en France. 
Il faut éviter les propositions qui deviennent des verrous de l'accès aux ressources mais aussi de la participation à la prise de décision.

#### Violence et Contre-violence
"Il y a trois types de violence.

La première, mère de toutes les autres, est la violence institutionnelle, celle qui légalise et perpétue les dominations, les oppressions et les exploitations, celle qui écrase et lamine des millions d'hommes dans ses rouages silencieux et bien huilés..

La seconde est la violence révolutionnaire, qui naît de la volonté d'abolir la première.

La troisième est la violence répressive, qui a pour objet d'étouffer la seconde en se faisant l'auxiliaire et la complice de la première violence, celle qui engendre toutes les autres.

Il n'y a pas de pire hypocrisie de n'appeler violence que la seconde, en feignant d'oublier la première, qui la fait naître, et la troisième qui la tue."  - [[Dom Hélder Câmara]]

Étude de [[Vanessa Codaccioni]] sur la répression policière. 

[[Contre violence]], un concept de [[Françoise d'Eaubonne]] pour ne pas mettre sur un même pied d'égalité les deux concepts. 

"Loin de l'épouvantail médiatique construit autour du militant d'ultra gauche violent, loin duodénum de la violence policière et de la violence d'Etat, moins d'une dichotomie fossilisée entre violence et non-violence, il y a une place pour une pratique contre violente créatrice efficace et non oppressive."

#### Vivant par [[Baptiste Morizot]]
Notion de [[Stress hydrique]], le vivant est lié. On peut faire front commun pour affronter tous les problèmes à la fois car on peut créer une sensibilité de ces points communs. Tout le monde peut ressentir la sécheresse. 
Il prend l'exemple des paysans qui ne voulaient pas proteger les zones humides mais qui ont changé d'avis car ils comprenaient ensuite que tout était lié. Littéralement la notion de [[Sensibilisation]]

#### WWW par [[Célia Izoard]]
La transition écologique et numérique est une contradiction dans les termes. 
Crolles, l'usine de micro, 336 litres d'eau par seconde !!!! 
11 à 23 millions de tonnes de métaux pour la [[5G]]. 

#### Asphyxie par [[Virginie Despentes]]
Elle parle d'[[Anarcho-capitalisme]] pour parler de Macron, c'est fort ! 
Je comprends qu'elle dise ça mais ça se trouve Macron s'en rend pas compte. 
"Il semble que cette génération de résistants préfère imaginer la fin du capitalisme plutôt que la fin du monde."

#### [[Youth for Climate]]
Jeunes qui se croient plus aux voies institutionnelles. 

#### [[ZAD]]
40000 personnes en 2012.
Le gouvernement n'a rien pu faire, la violence n'a pas fonctionné. C'est une forme [[Commune]] intéressante, nouvelle. 


## Référence


## Liens 