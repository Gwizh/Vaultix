MOC : 
Source : [[Aléxis Tsípras]]
Date : 2023-06-09
***

## Résumé
Aléxis Tsípras, né le 28 juillet 1974 à Athènes, est un homme d'État grec. Il est Premier ministre de janvier à août 2015 et de septembre 2015 à juillet 2019.

Il est nommé Premier ministre le 26 janvier 2015, au lendemain des élections législatives remportées par son parti. Critique envers l’[[Union européenne]], il organise notamment un référendum sur la dette publique grecque, puis signe un accord avec les créanciers de la Grèce. Contesté par une partie de sa majorité, il démissionne le 20 août 2015, puis est de nouveau nommé le 21 septembre suivant, à la suite d'élections législatives anticipées.

À la suite des défaites de son parti aux élections européennes et locales de 2019, il provoque de nouvelles élections législatives anticipées, lors desquelles son parti est battu par la Nouvelle Démocratie de [[Kyriákos Mitsotákis]], qui lui succède comme Premier ministre le 8 juillet 2019.

## Référence
[[Grèce]]

## Liens 