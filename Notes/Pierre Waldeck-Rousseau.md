MOC : 
Source : [[Pierre Waldeck-Rousseau]]
Date : 2023-06-09
***

## Résumé
Pierre Waldeck-Rousseau, né à le 2 décembre 1846 à Nantes et mort le 10 août 1904 à Corbeil-Essonnes, est un homme d'État français. Républicain et libéral, il est principalement connu pour avoir participé à la légalisation des syndicats (loi Waldeck-Rousseau de 1884) ainsi que pour la loi de 1901 sur les associations.

En 1899, en pleine affaire Dreyfus, il est appelé par le président [[Émile Loubet]] pour former un gouvernement. Représentant éminent des républicains modérés, ministre de [[Léon Gambetta]] puis de [[Jules Ferry]] dans les années 1880, Waldeck-Rousseau forme un gouvernement de « Défense républicaine », incluant des personnalités de sensibilités différentes, comme le général de Galliffet et le socialiste indépendant [[Alexandre Millerand]].

Durant près de trois ans, son cabinet est le plus long de la [[IIIe République]]. Il marque un tournant dans l'[[Affaire Dreyfus]] et poursuit une politique économique et sociale faite à la fois d'avancées sociales (incarnées en particulier par Millerand) et d'une certaine modération (représentée par le ministre des Finances [[Joseph Caillaux]]). Il est soutenu dans l'ensemble par le camp républicain, allant de l'Alliance républicaine démocratique, proche des milieux d'affaires, aux socialistes révolutionnaires, incarnés par [[Édouard Vaillant]] et [[Jules Guesde]], qui, s'ils critiquent la participation de Millerand, prônent encore pour un temps l'unification.

Discours page 347 de [[La République imaginée]]

## Référence
[[Loi relative à la création des syndicats professionnels]]
[[Gouvernement Pierre Waldeck-Rousseau]]

## Liens 
