[[Article]]
MOC : [[Noz/ECONOMIE]]
Titre : [[L’impact complexe de l’inflation sur les finances publiques]]
Auteur : [[L'Opinion]]
Date : 2022-07-06
Lien : https://www.lopinion.fr/economie/limpact-complexe-de-linflation-sur-les-finances-publiques
Fichier : 
***

## Résumé
Le Trésor ne veut pas laisser croire que l’inflation est une bonne affaire pour l’Etat. 

L’inflation devrait continuer d’augmenter pour se stabiliser entre 6,5% et 7% à l’automne, selon l’Insee.

Dans un billet de blog publié mardi, la chef économiste du Trésor Agnès Benassy-Quéré souligne que le dynamisme des recettes fiscales découle d’abord de trois facteurs temporaires : « la hausse des profits des entreprises en 2021, sous l’effet d’une reprise très vive de l’économie », qui dope l’impôt sur les sociétés (IS) ; le dynamisme de l’emploi en 2021 qui soutient les prélèvements fiscaux et sociaux, et enfin la baisse du taux d’épargne qui dope la consommation et fait augmenter l’assiette de la TVA.

**Automatismes**. Quant à l’effet spécifique de l’inflation sur le budget de l’Etat, il n’est pas si positif pour deux raisons. Primo, le décalage temporel : les recettes augmentent plus vite que les dépenses. « En cas de hausse des prix, les recettes de TVA augmentent mécaniquement pour un volume donné de consommation », illustre Agnès Benassy-Quéré. De même, la hausse des salaires gonfle les cotisations sociales perçues, celle des profits dope mathématiquement l’IS.

Or beaucoup de dépenses publiques sont aussi indexées sur les prix : « Retraites, allocations familiales, allocations logement, revenu minimum [sont notamment indexés dans un délai inférieur à un an](https://www.lopinion.fr/economie/comment-reapprendre-a-vivre-avec-linflation) », souligne-t-elle. « Chaque point d’inflation en plus augmente automatiquement ces dépenses sociales de près de 5 milliards d’euros ». Pour les autres dépenses, les délais d’indexation sont variables, comme les contrats de fourniture des collectivités territoriales ou des hôpitaux, ou les salaires de la fonction publique. « A court terme, l’inflation peut donc réduire le déficit public, mais l’effet est appelé à se dissiper rapidement. »

  ![P4-Infog-Inflation.png](https://beymedias.brightspotcdn.com/dims4/default/fe797da/2147483647/strip/true/crop/904x706+0+0/resize/840x656!/quality/90/?url=http%3A%2F%2Fl-opinion-brightspot.s3.amazonaws.com%2F4b%2Fe7%2F3e9570d84a0ca9ef9006cca595d1%2Fp4-infog-inflation.png)

**Effet volume**. Deuzio, la nature du choc inflationniste. « Si, comme ce fut le cas au début de la reprise économique après la crise Covid, l’inflation est liée à un redressement rapide de la demande, alors les assiettes fiscales augmentent non seulement en valeur mais aussi en volume. Mais si, comme aujourd’hui, l’inflation provient d’un [renchérissement des énergies et matières premières importées](https://www.lopinion.fr/economie/prix-de-lenergie-ce-nest-que-le-debut), alors les prix augmentent mais le PIB, l’emploi et la consommation sont affectés négativement (l’économie s’appauvrit) », explique la cheffe économiste. Dans ce cas, la soutenabilité de la dette publique se détériore, car les assiettes fiscales baissent en volume.

Alors, certes, grâce à l’inflation les assiettes fiscales augmentent en valeur, et comme la dette est exprimée en euros, le rapport de la dette sur le PIB diminue. « Ces deux arguments ne sont pas faux, mais ils oublient des éléments importants et notamment l’origine de l’inflation, résume Agnès Benassy-Quéré. Dans le contexte actuel, l’argent que nous versons aux pays producteurs de pétrole et de gaz va tôt ou tard manquer dans les caisses de l’Etat. »


## Référence

## Liens 