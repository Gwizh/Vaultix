[[Article]]
MOC : [[MOC/ECONOMIE]] [[Politique]]
Titre : [[Mélenchon contre Le Maire - le gestionnaire, c’est moi !]]
Auteur : [[Charlie Hebdo]]
Date : 2022-07-07
Lien : https://charliehebdo.fr/2022/06/economie/melenchon-contre-le-maire-le-gestionnaire-cest-moi/
Fichier : 
***

## Résumé
À l’approche du second tour, les esprits s’échauffent. Pour une fois, le débat est économique, youpi ! Mélenchon accuse Le Maire de vouloir augmenter la TVA. Le Maire accuse Mélenchon de pousser le pays à la faillite. Et s’ils avaient tous les deux tort ?

Jean-Luc Mélenchon y détaille deux points essentiels de son programme. D’une part, il estime possible de financer le passage à la retraite à 60 ans à taux plein lorsque l’on a travaillé durant 40 annuités. L’intérêt de sa démonstration est de montrer que cette mesure est très coûteuse – 29 milliards – et que son financement requiert un grand nombre de mesures : suppression des inégalités de salaire entre femmes et hommes ; accroissement des cotisations sociales sur les plus hauts salaires ; et, surtout, hausse des cotisations vieillesse. Un programme possible, qui renvoie à un véritable choix politique.

Par contre, c’est à propos du blocage des prix que Jean-Luc Mélenchon est moins convaincant. Il estime que ce blocage est possible en raison des _« profits exceptionnels »_ des entreprises pétrolières : sur ce point, il a raison. Mais dire que, _« quant à l’alimentation, la baisse sera payée par la grande distribution »,_ c’est faux, car, loin des fantasmes de nombreux Français, les marges sont très faibles dans ce secteur. Le blocage de certains prix me semble nécessaire, aujourd’hui. Mais cela requiert bien une part de soutien public, pour compenser financièrement le manque à gagner des producteurs.


Par contre, Bruno est trop heureux de rappeler qu’il a baissé les impôts au cours du quinquennat écoulé : impôt sur le revenu, taxe d’habitation, impôt sur les sociétés, et qu’il propose désormais de supprimer la redevance pour l’audiovisuel public. Bien sûr, selon Bruno, baisser les impôts n’est pas du tout des cadeaux faits aux plus aisés, mais un moyen de « récompenser le travail », à commencer par celui, bien connu, des héritiers qui vivent de la fortune de papa maman.

Alors que, dans les faits, Bruno et Emmanuel sont ceux qui ont pillé l’État comme personne avant eux. Et ce n’est pas dû seulement, et de très loin, seulement au Covid. Les géniales baisses d’impôts dont Bruno est si fier ont fait perdre 160 milliards de recettes à l’État au cours des cinq dernières années. Car lorsque l’on baisse les impôts, les recettes publiques diminuent, qui l’eut cru ?

Résultat : pour la première fois de notre histoire, l’an dernier, l’État français s’est plus financé par la dette que par l’impôt. Oui oui. De la folie pure, au moment où l’inflation, la guerre en Ukraine, et la hausse des taux d’intérêt vont faire exploser le coût de la dette publique dans les années qui viennent. Bruno le Maire devrait donc être un peu plus modeste avant d’accuser Jean-Luc Mélenchon de conduire le pays à la « faillite » : la vérité est que, sur ce plan-là, ils sont tout aussi irresponsables l’un que l’autre. 


## Référence
[[Législatives de 2022]]
[[France Insoumise]]
[[Bruno Le Maire]]
[[Jean-Luc Mélenchon]]

## Liens 