MOC : 
Source : [[Hoesik]]
Date : 2023-06-09
***

## Résumé
**_Hoesik_** ([Korean](https://en.wikipedia.org/wiki/Korean_language "Korean language"): 회식, [Hanja](https://en.wikipedia.org/wiki/Hanja "Hanja"): 會食, transl. eating together) is a popular type of gathering in the society of [South Korea](https://en.wikipedia.org/wiki/South_Korea "South Korea"), and refers to a group of people getting together to eat and drink. In Korean society, Hoesik has been established as a subculture of an organization or enterprise. In Korean corporate culture, Hoesik which have been a longstanding tradition, typically take place after work hours. However, such events have come under scrutiny as social issues due to the collectivist nature of Korean corporate culture, which has led to structural problems characterized by rigidity and coercion.

## Référence

## Liens 