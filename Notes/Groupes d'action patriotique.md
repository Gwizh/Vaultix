MOC : 
Source : [[Groupes d'action patriotique]]
Date : 2023-05-05
***

## Résumé
Les **Groupes d'action patriotique** (GAP) étaient de petits groupes de partisans de la [[Résistance italienne]] formés par le commandement général des Brigades Garibaldi à la fin du mois de septembre 1943 à l'initiative du [[Parti communiste italien]] sur la base de l'expérience de la [[Résistance intérieure française]].

## Référence

## Liens 
[[Seconde Guerre mondiale]]