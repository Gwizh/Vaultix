MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311080949
Tags : #Fini
***

# L'industrie devient le moyen de distinction des inventeurs au XIXème siècle
« Au XIXe siècle, l’industrie devient le lieu où l’invention est pleinement reconnue, et ce déplacement est lourd de sens. » (Chauveau, 2014, p. 5) 
  
Les industries deviennent le lieu de légitimisation des inventions. Il y a alors une ambiguité. La science perd de sa pureté et l'inventeur de son autonomie.
## Référence
[[@Science, industrie, innovation et société au XIX _sup_e__sup_ siècle,Sophie Chauveau]]

