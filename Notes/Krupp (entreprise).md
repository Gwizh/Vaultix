MOC : 
Source : [[Krupp (entreprise)]]
Date : 2023-04-20
***

## Résumé
Fried. Krupp AG, communément appelé Krupp, est un ancien conglomérat industriel multinational allemand du secteur de l'acier fondé et dirigé par la famille du même nom depuis 1811, et qui s'est notamment enrichi, durant la révolution industrielle dans l'expansion du chemin de fer et la fabrication d'armes puis, entre autres, avec les deux dernières guerres mondiales. L'entreprise, qui a absorbé la Sté Gruson en 1893, devient société par actions en 1903, puis s'implante dans le monde entier et devient l'une des premières mondiales de son secteur. Collaborant à la montée du nazisme et sanctionnée en 1947, indirectement gérée depuis 1967 par une fondation, elle fusionne en 1992 avec Hoesch AG (en), puis en 1999 avec son concurrent Thyssen AG pour former ThyssenKrupp AG.

## Référence

## Liens 
[[Secteur de l'électricité en Grande Bretagne à la fin du 19e siècle]]
