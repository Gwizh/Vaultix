### Saturday Drip
Franchement je devrais écouter plus cet album, les morceaux sont funs
C'est un peu hyperpop non ? En tout cas ça reste cohérent avec Glitch Mode
7/10

### Valkyrie
Je trouve ça un peu simple.
Ouais je suis désolé, je pense que c'est pour ça que je ne m'en souvenais pas trop.
En fait, c'est plutôt efficace mais je crois que je préfère quand c'est plus excentrique
J'aime bien le bridge du coup
4/10

### Unforgiven
Ya plein de mélanges de plein de trucs et ça colle trop bien au concept
Genre le petit truc en mode film de far-west, cowboy en fond c'est trop cool
Et à côté ya les espèces de cloches tibétaines bizarres ça rend ça intéressant
En fait c'est un mélange qu'on a jamais entendu
Mais en vrai c'est pas pour autant ma préférée d'elle
8,5/10

### Wicked Love
Ah, je m'en veux de ne pas l'avoir plus écoutée T-T
C'est très joli et franchement la mélodie est méga efficace
Je pense que je pourrais encore plus l'aimer si je l'écoutais plus :)
6,5/10

### S-Class
Ouais, ils sont forts
Bon je connaissais super bien déjà héhé comme Unforgiven
Par contre je vois toujours les animaux chelous dans ma tête c'est très perturbant
J'aime à quel point c'est varié
8,5/10

### Spicy
Ça aussi je connais of course
J'aime comme c'est méga agressif en fond 
Les harmonies sont méga belles
Bizaremment je trouve qu'elles ont fait plus intéressant, là c'est un peu uni
8/10

### Bouncy
Pareil, j'adore le fond, c'est méga puissant
Je les trouve similaires mdr
C'est vraiment une tornade en fond jpp c'est vraiment cool 
Je la connaissais mais j'avais moins écouté, merci pour ça <3o<3
7/10

### Queen Card
Ouais, c'est vraiment super je sais :p
J'adore l'anglais dans ce morceau, c'est vraiment des accents qui ajoutent qq chose je sais pas pk xD
Ya plein de petits détails sonores super cools, des petites voix, des petits effets sonores (les photos genre), ça rend ça super fun
8/10

### Bite Me
Méga bass j'aime bien ^^
Je crois que j'aime pas l'air principal mais c'est vraiment subjectif
En fait je crois que ma partie préférée c'est le build up avec les come here and get some mdr, le reste c'est plutôt bof à mon gout.
5,5/10

### Parting
C'est méga beau, faut vraiment que j'écoute la lupop 9 snifouille
Après faut vraiment rentrer dedans je trouve.
la guitare à la fin c'est vraiment trop cool
En fait c'est méga original c'est cool
6,5/10

# Classement
1. Unforgiven
2. S-Class
3. Spicy
4. Queen Card
5. Saturday Drip
6. Bouncy
7. Parting
8. Wicked Love
9. Bite Me
10. Valkyrie

# Moyenne
7/10, en fait ya pas de morceaux que je méga adore mais j'aime beaucoup la playlist :) 