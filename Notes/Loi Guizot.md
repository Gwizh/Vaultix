MOC : 
Titre : [[Loi Guizot]]
Date : 2023-09-26
***

## Résumé
Cette loi proposée par [[François Guizot]], ministre de l'Instruction publique dans le premier gouvernement Soult, et qu'il contribua activement à mettre en place, précède celles de [[Jules Ferry]]. C'est l'un des textes majeurs de la monarchie de Juillet. Il répond à l'article 69 de la Charte de 1830, qui avait prévu qu'une loi porterait sur « l'instruction publique et la liberté de l'enseignement ».

En 25 articles, la loi Guizot traite de l'objet, de l'organisation de l'enseignement primaire et de son contrôle. Elle distingue l'instruction primaire élémentaire.

Article 1 est rédigé ainsi: L'enseignement comprend nécessairement l’instruction morale et religieuse, la lecture, l’écriture, les éléments de la langue française et du calcul, le système légal des poids et mesures

Article 2 Le vœu des pères de famille sera toujours consulté et suivi en ce qui concerne la participation de leurs enfants à l'instruction religieuse .

La présence de cet article s'éclaire si l'on se rappelle que François Guizot était de confession réformée.

L'instruction primaire supérieure comprend des éléments de mathématiques, de sciences de la nature, d'histoire et de géographie. Les notions plus avancées seront étudiées « selon les besoins et les ressources des localités.

## Références

## Liens 