MOC : 
Source : [[Les milliardaires font main basse sur l’économie du livre]]
Date : 2023-04-12
***

## Résumé 
[[Vincent Bolloré]], qui veut racheter Hachette à [[Arnaud Lagardère]], va céder Editis à [[Daniel Kretinsky]], par ailleurs principal actionnaire de la [[Fnac]], premier libraire de France. Preuve que l’économie du livre est toujours plus concentrée entre les mains de quelques-uns.

Certes, la [[Commission européenne]] a contraint le milliardaire breton à revendre, pour des raisons concurrentielles, Editis (Plon, Robert Laffont, XO Éditions, etc.), numéro 2 du marché – 850 millions de chiffre d’affaires – qu’il possédait depuis 2018. Le pire a donc été évité : que Bolloré ne devienne l’empereur du livre en France en fusionnant [[Hachette]] et [[Editis]] – ces deux mastodontes concentrent plus de 50 % du top 100 des ventes de livres en France.

In fine, on devrait donc assister à un jeu de chaises musicales entre milliardaires dans l’édition : Lagardère se retirant, Bolloré vendant le numéro 2 pour le numéro 1, et Kretinsky devenant le nouveau dauphin. 

Mais ce jeu sera-t-il à somme nulle pour leurs concurrents ? Pas totalement. En effet, le milliardaire tchèque est en parallèle devenu, avec 25 % du capital, le principal actionnaire de la Fnac-Darty, premier libraire de France. 

Les librairies indépendantes – qui sont plus de 3 000 en France – s’inquiètent. « Que le propriétaire du numéro 2 de l’édition soit aussi l’actionnaire principal du plus gros vendeur de livres en France est évidemment un souci pour nous », concède Guillaume Husson, délégué général du Syndicat de la librairie française (SLF).

Concrètement, _« ce qui poserait problème, ce serait qu’[[Editis]] puisse soit réserver à la Fnac l’exclusivité de ses productions, soit faire bénéficier la Fnac de conditions commerciales avantageuses au regard de celles qu’il imposerait aux concurrents »_, explique Julien Pillot, économiste à l’Inseec (Institut des hautes études économiques et commerciales), spécialiste des sujets relatifs à la concurrence. _« Cela pourrait s’apparenter, sans présumer de l’appréciation d’un juge de la concurrence à l’issue d’une instruction minutieuse, à une tentative d’exclusion ou à une discrimination anticoncurrentielle, potentiellement constitutive d’un abus de position dominante »_, ajoute-t-il.

## Référence

## Liens 