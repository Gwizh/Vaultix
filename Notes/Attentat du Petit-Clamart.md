[[Note Permanente]]
MOC : [[Histoire]]
Titre : [[Attentat du Petit-Clamart]]
Date : 2022-07-11
***

## Résumé
L’attentat du Petit-Clamart, désigné par ses auteurs sous le nom d'opération Charlotte Corday, est un attentat organisé par le lieutenant-colonel Jean Bastien-Thiry, visant à assassiner le général de Gaulle, président de la République, le 22 août 1962 à Clamart dans le département de la Seine. 

> « Le 22 août 1962 […], au Petit-Clamart, la voiture qui me conduit à un avion de Villacoublay avec ma femme, mon gendre Alain de Boissieu et le chauffeur Francis Marroux est prise soudain dans une embuscade soigneusement organisée : mitraillade à bout portant par plusieurs armes automatiques, puis poursuite menée par tireurs en automobile. Des quelque 150 balles qui nous visent, quatorze touchent notre véhicule. Pourtant — hasard incroyable ! — aucun de nous n'est atteint. »

— [Charles de Gaulle]


## Référence
[[Histoire de la France au XXe Siècle - Livre 3]]

## Liens 
[[Guerre d'Algérie]]