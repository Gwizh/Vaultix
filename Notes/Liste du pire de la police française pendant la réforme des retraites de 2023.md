MOC : 
Source : [[Liste du pire de la police française pendant la réforme des retraites de 2023]]
Date : 2023-04-10
***

## Résumé

https://twitter.com/syndicat_CNT/status/1645036229426118659?t=Fn-thHwvaTBq08gW7VAa8Q&s=09

[[Un policier de la Brav condamné pour avoir menacé une famille avec son arme de service]]

Bretagne : le RAID se trompe de maison et plaque au mur un couple de retraités
https://www.francetvinfo.fr/faits-divers/bretagne-le-raid-se-trompe-de-maison-et-plaque-au-mur-un-couple-de-retraites_5768306.html

https://twitter.com/violencespolice/status/1646572829251346434?t=zbFywUFSZ3MR7eXCdprCZw&s=19

Un policier mis en examen pour avoir percuté trois adolescents à scooter
https://www.mediapart.fr/journal/france/210423/un-policier-mis-en-examen-pour-avoir-percute-trois-adolescents-scooter?utm_source=global&utm_medium=social&utm_campaign=SharingApp&xtor=CS3-5

## Référence

## Liens 

[[Réforme des retraites 2023]]