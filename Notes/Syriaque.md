MOC : 
Source : [[Syriaque]]
Date : 2023-07-03
***

## Résumé
Le syriaque, en syriaque : ܣܘܪܝܝܐ / suryāyā ou suryoyo, est une langue sémitique du Proche-Orient, appartenant au groupe des langues araméennes. L'araméen existe au moins depuis le XIIe siècle av. J.-C. et a évolué au cours des siècles. Le syriaque est couramment présenté comme dialecte de l'araméen, en tant que géolecte de la région d'Édesse, qui s'est constitué comme langue écrite au début de l'ère chrétienne. 

## Référence

## Liens 
[[Syrie]], [[Irak]], [[Turquie]], [[Liban]]