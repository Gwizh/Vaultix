MOC : 
Source : [[Raymonde Poncet Monge]]
Date : 2023-05-02
***

## Résumé
Sénatrice EELV du Rhône et de la métropole de [[Lyon]]

"J’ai déposé une proposition de loi «visant à renforcer un suivi gynécologique et obstétrical bientraitant».

Cette proposition de loi cherche ainsi à lutter contre les violences obstétricales et gynécologiques."
https://twitter.com/PoncetRaymonde/status/1653385966520868867t=5vxWTTqu29zX0Q6KV2JfhA&s=19 

Anciennement économiste au comité central d'entreprise de [[Renault Trucks]], elle est membre d'[[Europe Écologie Les Verts]] (EÉLV). Elle est à ce titre élue le 27 mars 2011 conseillère générale aux élections cantonales pour le canton de Lyon-III (La Croix-Rousse). Son mandat prend fin le 31 décembre 2014 avec la disparition du canton et la création de la métropole de Lyon.

Entrée au Sénat
Après s'être présentée sans succès comme tête de liste EÉLV aux élections sénatoriales de 2014 dans le Rhône, elle est élue sénatrice du Rhône et de la métropole de Lyon le 27 septembre 2020,. Elle siège dans le groupe écologiste - solidarités et territoires.

Au [[Sénat]], elle est vice-présidente de la commission des Affaires sociales, vice-présidente de la [[Délégation aux droits des femmes et à l'égalité des chances entre les hommes et les femmes]]. Elle est également secrétaire de la mission d'évaluation et de contrôle de la [[Sécurité sociale]].


## Référence

## Liens 