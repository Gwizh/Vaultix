MOC : 
Source : [[Thadée Natanson]]
Date : 2023-07-13
***

## Résumé
Thadée Natanson, né à Varsovie le 28 mars 1868 et mort à Paris le 26 août 1951, est un avocat, homme d'affaires, journaliste, collectionneur et critique d'art français d'origine polonaise, connu surtout pour avoir été le cofondateur et le principal animateur de [[La Revue blanche]].

## Référence

## Liens 