MOC : 
Source : [[Livret d'ouvrier]]
Date : 2023-05-05
***

## Résumé
Le livret d'ouvrier est un document officiel français mis en service par le [[Consulat]] le 12 avril 1803, généralisé par [[Napoléon]], dont l'usage décline à partir de 1860 sous [[Napoléon III]] pour s'éteindre en 1890. Il permet le contrôle des déplacements des ouvriers par les autorités auxquelles il doit être présenté à de multiples occasions. 

C'est l'un des pires articles Wiki OMG sadge https://fr.wikipedia.org/wiki/Livret_d%27ouvrier

## Référence

## Liens 