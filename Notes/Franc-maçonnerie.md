MOC : 
Source : [[Franc-maçonnerie]]
Date : 2023-06-02
***

## Résumé
Le terme franc-maçonnerie désigne un ensemble d'espaces de sociabilité sélectifs qui recrutent leurs membres par cooptation et pratiquent des rites initiatiques se référant à un secret maçonnique et à l'art de bâtir.

En gros c'est que pour tous les métiers en lien avec la construction qui demandait au Moyen Age un grand savoir faire. Pour enseigner le savoir et travailler ensemble, les travailleurs formaient des groupes sociaux bien définis (rites de passages par exemple). Au début c'était juste à côté des sites de construction mais bientôt, ils trouveront des endroits permanents appelés les loges. Puis ça devient des groupes qui n'ont plus à voir avec le secteur de la construction, des groupes se forment simplement pour reprendre les rituels. Le symbole de l'équerre et du compas sont des outils qui renvoyaient à la science et à la philosophie. Ça devient un endroit de discussion privilégié pour les bourgeois et les aristocrates qui peuvent échapper à la pression religieuse.

[[Isaac Newton]] était un franc-maçon au XVIIe siècle. 

## Référence

## Liens 