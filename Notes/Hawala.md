MOC : 
Source : [[Hawala]]
Date : 2023-06-23
***

## Résumé
Un hawala (en arabe : حِوالة, qui signifie mandat ou virement), ou hundi, est un système traditionnel de paiement informel. Son origine exacte n'est pas déterminée, mais il semble être apparu comme moyen de financement du commerce sur les grandes routes d'échange (comme la route de la soie). Le [[Coran]], qui condamne l’usure, encourage le hawala et plusieurs hadiths détaillent ce système de transfert de fonds. En Asie du Sud, il s'est développé en un système bancaire complet, seulement remplacé progressivement par le système bancaire classique depuis le xxie siècle. De nos jours, il est essentiellement utilisé pour les envois de fonds par les travailleurs immigrés vers leur pays d'origine, mais aussi pour l'évasion fiscale, le blanchiment d'argent ainsi que pour rémunérer des passeurs de migrants. 

## Référence

## Liens 
[[Commerce]]
[[Flux]] 
[[Fraude fiscale]] 
[[Marché Noir]]