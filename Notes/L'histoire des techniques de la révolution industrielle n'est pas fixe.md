MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311080949
Tags : #Fini
***

# L'histoire des techniques de la révolution industrielle n'est pas fixe
« Elles peuvent être bousculées par le choix d’aborder autrement l’industrialisation du XIXe siècle » (Chauveau, 2014, p. 3) 
Il y a plusieurs façons de raconter l'histoire des techiques à cette époque. Cela peut être parler des innovations, de la consommation. Cela peut être lié aux territoires pour permettre l'analyse de l'impact sur l'environnement. Ce dernier exemple reprend les travaux de [[@Technocritiques_ du refus des machines à la contestation des technosciences,François Jarrige]]

## Référence
[[@Science, industrie, innovation et société au XIX _sup_e__sup_ siècle,Sophie Chauveau]]
