[[Article]]
MOC : [[Politique]]
Titre : [[Au Royaume-Uni, la chute de Johnson laisse un Parti conservateur exsangue]]
Auteur : [[Mediapart]]
Date : 2022-07-07
Lien : https://www.mediapart.fr/journal/international/070722/au-royaume-uni-la-chute-de-johnson-laisse-un-parti-conservateur-exsangue
Fichier : 
***

## Résumé
Longtemps persuadé qu’il pouvait rester en poste malgré les mensonges, scandales et conflits d’intérêts, Boris Johnson a fini par démissionner jeudi, sous la pression de poids lourds de son gouvernement. Il laisse un Parti conservateur épuisé par l’exercice du pouvoir, et très fracturé.

Au-delà du seul « [[Partygate]] » - l’affaire des apéritifs clandestins pendant le confinement, pour laquelle il avait fini par écoper [d’une amende](https://www.mediapart.fr/journal/international/120422/fetes-confinees-boris-johnson-ecope-d-une-sanction-historique) historique –, [[Boris Johnson]] a été éclaboussé par [d’autres scandales](https://www.mediapart.fr/journal/international/190122/boris-johnson-et-le-partygate-un-scandale-qui-en-cache-de-plus-graves), liés en particulier au financement du Parti conservateur et à des conflits d’intérêts flagrants.

Mais c’est l’épisode de la promotion du député Chris Pincher, accusé de faits de harcèlement sexuel et d’agression sexuelle par plusieurs hommes – dont des députés conservateurs – au cours des dernières années, [d’après la presse britannique](https://www.bbc.com/news/uk-politics-62025612), qui a accéléré la chute. Boris Johnson a reconnu, après l’avoir nié plusieurs jours, qu’il avait été mis au courant en 2019 de l’existence d’une procédure disciplinaire, au sein du ministère des affaires étrangères, qui visait déjà Pincher, et au terme de laquelle ce dernier avait été sanctionné.

Johnson avait déjà reçu le 6 juin un gros avertissement. Il avait remporté ce jour-là, [dans la douleur](https://www.mediapart.fr/journal/international/070622/au-royaume-uni-johnson-sauve-sa-peau-de-justesse-son-parti-divise), un vote de défiance déclenché par les frondeurs de son parti. Mais près de 41 % des élu·es tories lui avaient retiré son soutien. Pour beaucoup, la « magie » de Johnson n’opérait plus sur le terrain ; en témoigne la série de défaites du parti lors d’élections partielles organisées [ces dernières semaines](https://www.theguardian.com/politics/2022/jun/24/lib-dems-win-tiverton-and-honiton-byelection-overturning-huge-tory-majority). Le souvenir de sa [victoire historique](https://www.mediapart.fr/journal/international/131219/royaume-uni-la-majorite-absolue-pour-johnson-ouvre-la-voie-un-brexit-en-janvier) de 2019, face à Jeremy Corbyn, semble bien loin.

La chute de Johnson intervient à un moment particulier, sur fond d’inflation carabinée – [pronostiquée](https://www.ft.com/content/79f922d3-ccdd-4e42-b786-4ca79a9becf6) à un rythme de 11 % cet automne –, du retour de [conflits sociaux](https://www.mediapart.fr/journal/international/210622/greve-massive-sur-le-reseau-de-chemin-de-fer-britannique) dans les transports, d’une guerre sur le continent européen partie pour durer, mais aussi d’un bras de fer épuisant avec l’UE au sujet de la non-application du _« protocole nord-irlandais »_ par Londres, ce mécanisme censé éviter le retour d’une frontière en dur entre l’Irlande et le Royaume-Uni malgré le [[Brexit]].

Au-delà de la chute de Johnson, c’est tout le Parti conservateur, épuisé par l’exercice du pouvoir (depuis 2010), qui se trouve mal en point. Pour au moins trois raisons :
- **De nombreuses figures du parti ont relayé les mensonges de Johnson pendant de longs mois de crise**.
- **Le parti peine à définir une ligne claire, pour lutter contre l’inflation ou répondre aux défis indépendantistes, en Écosse ou en Irlande.**
- **Le parti est encore dans le déni des conséquences économiques du Brexit.**


## Référence
[[Boris Johnson]]
[[Royaume-Uni]]

## Liens 