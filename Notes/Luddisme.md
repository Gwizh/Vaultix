MOC : 
Source : [[Luddisme]]
Date : 2023-08-10
***

## Résumé
Le luddisme est, selon l'expression de l'historien britannique Edward P. Thompson (1924-1993), un « violent conflit social » qui, dans l'[[Angleterre]] des années 1811-1812, a opposé des artisans — tondeurs et tricoteurs sur métiers à bras du West Riding, du Lancashire du sud et d'une partie du Leicestershire et du Derbyshire — aux employeurs et aux manufacturiers qui favorisaient l'emploi de machines (métiers à tisser notamment) dans le travail de la laine et du coton. Les membres de ce mouvement clandestin, appelés luddites ou luddistes, sont considérés comme des « briseurs de machines ». 

## Référence

## Liens 
[[XIXe siècle]]