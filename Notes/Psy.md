## Contexte
Je ne viens pas parce que ce je vais mal, je viens justement parce que tout va bien.
Quand je vais mal, je ne peux rien faire de neuf, je suis obligé de tout faire par habitude.

## Aspects importants
### Peur de l'attention des autres
#### En cours
##### Impossibilité de participer en classe
Impossibilité de participer en classe
même en sachant la réponse
des heures passées avec le stress qu'on puisse m'intéroger. 
De la CP à l'école sup.

C'est au moment de demander la parole que j'avais un blocage, je devenais anxieux directement, du mal à respirer, j'avais chaud etc...
En fait je n'avais pas confiance en moi, je doutais immédiatement de ma réponse. 
Ce n'était pas simplement le fond de la réponse, si c'était correct ou non, c'était aussi la forme. J'avais une fixation sur l'idée que j'allais répondre bizarrement. Qu'on allait rire de ma réponse.

Quand j'allais au tableau notamment c'était horrible. 

En fait les seules fois où j'ai pu me tirer de ça c'était quand je pouvais comparer mes réponses avec ma copine. Ainsi, ce n'était pas juste ma confiance en moi.

#### En public
Je ne peux pas porter des vêtements trop voyants, trop fluos. J'ai toujours voulu porter des couleurs flashies, de belles chaussures et globalement d'avoir un style qui ne soit pas sobre. Mais c'est impossible, je n'assume pas mes vêtements. Lorsque que je suis dehors, j'ai l'impression que les gens me regardent, qu'on me fixe de travers. Quand je suis dans des transports en commun, j'ai l'impression qu'on me juge. J'ai le besoin de mettre des vêtements sobres pour qu'on ne me remarque pas. Je dois me faire discret. Je parle très bas et je ne fais pas de mouvements brusques.

### Charge mentale de la sociabilisation
#### Au travail
Exemple le plus simple.
Aujourd'hui je travaille dans une entreprise au sein d'une équipe qui est très portée sur la cohésion. Ils font plusieurs soirées par semaine et passent toutes les pauses ensemble à faire des jeux et des activités. 
Si j'avais été dans ce genre d'équipes il y a quelques années, ça m'aurait grandement fait du mal, j'en suis certain.
Le problème ici est le prise du temps et la charge mentale que ça me prend. Pour moi travailler autour d'eux est déjà fatigant, ils peuvent voir mes faits et gestes et me surveiller. Je travaille très mal quand des gens peuvent me regarder.
Un collègue de travail m'a dit un jour que j'étais évasif. D'autres m'ont dit que j'étais froid. En fait j'ai l'impression que j'ai besoin que toute intéraction sociale soit soit bien délimitée dans le temps et prévue à l'avance soit que je puisse y mettre un terme quand je veux. J'ai peur d'être bloqué dans une situation sociale trop fatigante.
Du coup quand on me propose un afterwork, j'ai beaucoup de mal à ne pas refuser par principe. 