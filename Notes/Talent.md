MOC : 
Source : [[Talent]]
Date : 2023-05-03
***

## Résumé
https://youtu.be/MQWZ4mdl9kI 
Essai de Samah Karaki. Elle dit que le talent ne peut être expliqué seulement par l'ADN. La prédisposition génétique n'est qu'un élément et on sait aujourd'hui que la part de l'acquis change l'inné en permanence. Ce n'est pas une addition mais une interaction, qq chose de complexe et unique pour chaque individu, même avec le même ADN. Exemple de l'oreille absolue, qu'on pense être très lié aux gènes, on l'a ou on l'a pas mais en fait c'est très lie à l'entraînement et a l'individu. Un chinois va par exemple avoir beaucoup plus de facilité car sa langue fait qu'il a une plus grande notion des tonalité dès la naissance, contrairement à un français typiquement. D'ailleurs on pensait que faire des maths et des calculs étaient réservés aux gens qui en avaient les capacités génétiques mais on sait aujourd'hui sur tout le monde peut apprendre. peut être qu'un jour on se rendra compte que tout le monde peut aussi avoir l'oreille absolue avec l'entraînement adéquat. 

On ne connaît pas le patrimoine génétique de Mozart. On sait par contre qu'il est né dans une famille de musiciens et que son père avaient pour projet de faire de lui un musicien. On ne peut pas dire s'il vaile talent de rendre le projet de son père possible ou si c'est juste un bon entraînement. Son père avait publié le jour de la naissance de Mozart un manuel d'apprentissage de la musique, il était lui même très pédagogue et avait des réseaux avec tous les plus grands musiciens de l'époque. Mozart était soumis a un entraînement struct et donc on a pas besoin d'un ingrédient magique pour expliquer son talent. 

Le talent associé au [[Mérite]] était déjà une idée importante de l'ère moderne. Passer de lignées royales à la méritocratie bourgeoise est déjà en soit une bonne chose. Pourtant, cela maque la réalité des choses. Oui, des rois n'étaient pas capables génétiquement de gouverner (si un tel truc existe) mais leur milieu, leur Capital financier, culturel, social était tel qu'ils pouvaient en fait gouverner. Paradoxalement, on pourrait même penser que la notion du talent était mieux comprise à l'époque de l'absolutisme, ce qui fait de chacun ce qu'il est, c'est avant tout la place qu'il occupé dans le monde, la classe, le genre, l'origine, l'environnement socio-économique etc... Aujourd'hui, il est important de déconstruire ça parce qu'il a été montré que les gens à haut potentiel reste la plupart du temps bloqué dans leur classe social, du fait des freins qui existent toujours. On est dans une méritocratie biaisée. La notion de talent actuelle permet de légitimer le système mais aussi de de tourner le regard. 

[[Salomé Saqué]] pose la question de la volonté innée, celle qui nous pousserait à nous en sortir. Samah répond qu'en neurosciences, on arrive vite à un constat pessimiste. La plupart des actions que nous faisons est déjà automatiquement en nous et beaucoup plus rapide que notre décision rationnelle. Le fait de fumer une cigarette par exemple est un geste que nous faisons sans réfléchir et que nous rationalisons seulement ensuite. Nous ne décidons pas vraiment de le faire, la sensation de volonté arrive après. Le jugement des gens se font en quelques millisecondes et ensuite au fur d'une première conversation, nous rationalisons le choix inconscient de l'esprit en trouvant des preuves. La réelle volonté vient de l'introspection, une réflexion profonde pour remettre en question nos actions. Mais ce n'est ni facile si suffisamment rapide pour pouvoir influencer tous nos choix. Si on est sous pression, fatigué, malade etc... On sera incapable de pouvoir faire cette effort de volonté et on va vraiment vivre en automatique. Donc en fait c'est tout le monde mdr. On ne peut pas parler de liberté sans conscience de notre déterminisme. 

#MIE C'est littéralement la preuve scientifique de la théorie spinoziste de la volonté ! Faut vraiment creuser ça ! [[Spinoza]] Ah bah elle le cité directement 😱 à 24:50

En France, on a beaucoup plus de chances d'accéder à de Hautes positions politiques en naissant dans une famille politique qu'en passant par une grande école politique. 

Olala elle parle de capital culturel et informationnel c'est trop bien. En gros notre façon d'aborder les sujets et d'accrocher ou non à certains concepts provient en grande partie de 

#Alire
Son livre xD

## Référence

## Liens 