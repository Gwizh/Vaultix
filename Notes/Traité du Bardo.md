MOC : 
Source : [[Traité du Bardo]]
Date : 2023-06-09
***

## Résumé
Le traité du Bardo (arabe : معاهدة باردو), appelé aussi traité de Ksar Saïd, est un traité signé entre le bey de Tunis et le gouvernement français le 12 mai 1881. Appelé « traité de garantie et de protection » dans la législation française, le texte porte les signatures du général Jules Aimé Bréart, du consul Théodore Roustan, du monarque Sadok Bey et du grand vizir Mustapha Ben Ismaïl.

Il instaure le protectorat de la [[France]] sur la [[Tunisie]]. Le bey est alors contraint de confier tous ses pouvoirs dans les domaines des affaires étrangères, de la défense du territoire et de la réforme de l'administration au résident général de France.

Les conventions de La Marsa, conclues le 8 juin 1883, vident le traité de son contenu et dépouillent le bey du reste de son autorité et instaurent l'administration directe. Le traité et les conventions seront révoqués lors de l'indépendance du pays proclamée le 20 mars 1956.

Entrée des troupes françaises en Tunisie
En avril 1881, sur ordre du [[Président du Conseil]] [[Jules Ferry]], un corps expéditionnaire de 24 000 hommes traverse la frontière pour poursuivre des montagnards kroumirs qui sèment le trouble en Algérie (selon les autorités coloniales). Les hommes du général Léonard-Léopold Forgemol de Bostquénard envahissent les terres kroumirs en pénétrant dans le massif forestier d'Aïn Draham tout en débarquant à Tabarka et en investissant Le Kef.

Démission de Jules Ferry
Jules Ferry ne profite pas longtemps de cette victoire car les députés ne lui pardonnent pas de les avoir entraînés dans une guerre de conquête au lieu de l'expédition punitive annoncée. Le 9 novembre, il présente la démission de son gouvernement mais revient au pouvoir le 31 janvier 1882, après le court intermède du gouvernement Gambetta.

[[Léon Gambetta]] dit à ce moment là que la France a enfin retrouvé son statu de grande puissance.

## Référence
[[Gouvernement Jules Ferry (1)]]

## Liens 