[[Note Permanente]]
MOC : [[Histoire]] [[POLITIQUE]]
Titre : [[OAS]]
Date : 2022-07-11
***

## Résumé
L'Organisation de l'armée secrète, ou Organisation armée secrète, surtout connue par le sigle OAS, est une organisation politico-militaire clandestine française proche de l'**extrême droite** créée le 11 février 1961 pour la défense de la présence française en Algérie par tous les moyens, y compris le terrorisme à grande échelle.

Un an après l'échec de la semaine des barricades, alors que le gouvernement français souhaite manifestement se désengager en Algérie, elle est créée à Madrid, lors d'une rencontre entre deux activistes importants, Jean-Jacques Susini et Pierre Lagaillarde, ralliant par la suite des militaires de haut rang, notamment le général Raoul Salan.

Le sigle « OAS » fait volontairement référence à l’Armée secrète (AS) de la Résistance. Il apparaît sur les murs d'Alger le 16 mars 1961, et se répand ensuite en Algérie et en métropole, lié à divers slogans : « L'Algérie est française et le restera », « OAS vaincra », « l'OAS frappe où elle veut et quand elle veut », etc.

Sur le plan pratique, il ne s'agit pas d'une organisation centralisée unifiée ; d'une façon très générale, elle est divisée en trois branches plus ou moins indépendantes, parfois rivales : l'« OAS Madrid », l'« OAS Alger » et l'« OAS Métro ».

**L'OAS est responsable d'au moins 2 200 morts en Algérie et de 71 morts et 394 blessés en France.**

## Référence
[[Histoire de la France au XXe Siècle - Livre 3]]

## Liens 
[[Histoire de la France]]
[[Guerre d'Algérie]]