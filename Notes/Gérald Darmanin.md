MOC : 
Source : [[Gérald Darmanin]]
Date : 2023-04-11
***

## Résumé

Une enquête pour « complicité de disparition forcée » a été ouverte à l’encontre de Gérald Darmanin suite à une plainte déposée par la Ligue des droits de l’Homme. Le ministre est accusé d’avoir « collaboré avec les autorités russes et tchétchènes pour avoir fait disparaître un demandeur d’asile en 2021. »
https://www.leparisien.fr/faits-divers/une-enquete-ouverte-apres-une-plainte-contre-gerald-darmanin-pour-complicite-de-disparition-forcee-dun-tchetchene-07-04-2023-E2ESQC52GREWBF7VIT6DAFXSOM.php

## Référence

## Liens 