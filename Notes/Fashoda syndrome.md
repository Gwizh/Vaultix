MOC : 
Source : [[Fashoda syndrome]]
Date : 2023-07-02
***

## Résumé
Fashoda syndrome, or a 'Fashoda complex', is the name given to a tendency within French foreign policy in Africa, giving importance to asserting French influence in areas which might be becoming susceptible to British influence. It refers to the Fashoda incident, which is considered the climax of the imperial territorial disputes between the United Kingdom and France in Eastern Africa, drawing these two nations to the brink of war in their bid to control the African Upper Nile region.

## Référence

## Liens 
[[France]]
[[Royaume-Uni]]
[[Colonialisme]]