MOC : 
Source : [[Zemmour et la commune de Paris]]
Date : 2023-04-16
***

## Résumé
Il dit que c'est une révolution patriotique, le peuple parisien ne voulant pas de la guerre contre [[Otto von Biesmark]]. S'en suit alors des événements qui s'apparentent à [[La Terreur]], des crimes, des meurtres. Et en face on a pas quelqu'un d'aussi gentil que Louis XVI mais on a [[Adolphe Thiers]]. 

Il dit que ça a été récupéré par les communistes avec [[Karl Marx]] car celui-ci voulait battre Proudhon et il considérait que la commune aurait dû être plus dure, plus violente, plus centralisée, plus communiste. 

Il dit que y'a plus de communiste et il dit que la gauche n'accepte plus la violence contrairement à avant. 


## Référence
[[Eric Zemmour]]

## Liens 