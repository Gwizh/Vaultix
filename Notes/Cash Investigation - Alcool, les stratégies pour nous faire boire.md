MOC : 
Source : [[Cash Investigation - Alcool, les stratégies pour nous faire boire]]
Date : 2023-04-13
***

## Résumé
1 foyer sur 2 a une bouteille de Ricard
Numéro 2 des vins et spiritueux 
32 marques différentes 

Commercial qui va dans des bars pour payer des tournées --> 5 à 10 ricards par jour. Pire pour les jours de fête. 400 commerciaux dont 90 dans les grandes surfaces.C'était beaucoup de dévouement, les commerciaux ont déménagé, partent en vacances avec les collègues. Appartenance à une famille. On apprend des chansons durant des fêtes et des vacances. Visite de villes à l'étranger avec des stars, à Barcelone ou Varsovie. 
C'est un engrenage, ton travail c'est de boire de manière importante et régulière. Aucun médecin de travail spécialisé. Un budget alcoolémie pour pouvoir aller dormir dans un hôtel. 

Bras droit du PDG. Charte pour la consommation responsable. Mais aucune connaissance s'il y a un médecin du travail. Nie les cas d'alcoolémie. 

En plus cette stratégie ne marche plus. Plan de reconquête des consommateurs. Créer des soirées sponsorisées. Utiliser des stagiaires pour créer des événements. 

Loi Évin. L'abus d'alcool est dangereux pour la santé. Réglementer les pubs. On ne peut plus montrer des gens sur les pubs. On ne peut parler que de l'alcool. Donc les soirées sponsos c'est pas possible en théorie mais ils font leaker la soirée et la prog musicale pour que genre Konbini fasse la pub à leur place. C'est toujours contractuel mais à l'oral. Dans le cas de la pub indirecte par des influenceurs, seuls ceux-ci sont judiciairement répréhensibles, Ricard ne l'est que moralement. Il y a quand même des contrats mais la marque profite clairement 

Loi Évin ANPAA. 

Placements de produits dans des clips de raps. Organisation d'événements pour Oxmo
"Infiltrer les tribus urbaines avec un roi du rap" dans un document officiel. "Ces mecs ont une parole, pas besoin de contrats". Même dans l'intérieur de l'album il y a le nom de Chivas. Organisation de 9 concerts en France pour Oxmo. C'est du parrainage mais la responsable marketing nie le savoir. 

Se font passer pour des conseillers marketing. 

7.7Mds de dépenses liées aux dégâts collatéraux de l'alcool. Le vin n'est pas un alcool comme les autres, 56% de la conso d'alcool. Il est le moins taxé des alcools. Pour les spiritueux, 50% de taxes, 9% pour les bières et -de 1% pour le vin. 

Ancien ministre de l'agriculture : "La santé ce n'est pas les taxes."

Vin avec du sirop, très consommés par les jeunes femmes. Sénatrice Joceline Guidez veut augmenter les taxes de 170%. Ça passe au Sénat mais pas à l'AN. 

Vigne vin œnologie. Groupe d'étude des députés à l'AN. Plus de membres que celui du cancer. 

[[Didier Guillaume]]

Association Regard Citoyen 

Groupe d'étude : parler d'un thème précis avec des membres de tout bord. C'est clairement du lobbying caché. Il n'y a pas de transparence, c'est ce qui publie le moins de rapport. Il y a eu un rapport sur les groupes d'études. 

[[Jacques Cattin]], député et chef d'entreprise de la maison Cattin, grande maison du vin. Vote contre toutes les taxes. Co-président du groupe Vigne, vins et œnologie. 

Nouvelle loi cette fois ci adoptée, mais on passe de 170% à 45%.

Courbe en J pour l'alcool : 1 ou 2 verres seraient bénéfiques. Beaucoup de scientifiques le montraient. Le premier biais dans les études étaient que les personnes saines étaient d'anciens buveurs. Ça devient une droite, comme les cigarettes. 

ISFAR : Forum avec des scientifiques, dont des dizaines ayant eu des financements de l'industrie de l'alcool donc Pernod Ricard. Mais pas que ce Forum, la recherches en faveur de l'alcool reçoit des financements. 

Diageo : numéro 1 de l'alcool. 

## Référence

## Liens 