MOC : 
Source : [[Grande Dépression (1873-1896)]]
Date : 2023-06-28
***

## Résumé
La Grande Dépression, ou plus exactement Grande Déflation, est une période de ralentissement économique mondial entre 1873 et 1896 qui démarre par un épisode brutal, la crise bancaire de mai 1873, qui n'est cependant pas l'origine ou la cause de ce phénomène.

Après la crise de 1929, l'expression Grande Dépression devient inadéquate. Surtout, cette locution ne révèle absolument pas la réalité économique de cette période. Le terme de Longue Dépression lui a été préféré. Cependant, les deux expressions de Grande Dépression ou Longue Dépression sont aujourd'hui jugées impropres car il s'agit d'un ralentissement économique ou d'une stagnation économique, ponctué par des périodes de déflation et non d'une dépression, puisque le produit national brut continuait à croître de 1880 à 1896.

Il faut raccorder cette période aux profondes mutations industrielles d'alors, qui perturbent le champ économique et provoquent de forts mouvements de capitaux se dirigeant vers de nouveaux secteurs d'avenir. Entre 1870 et 1910, le volume de la production mondiale double. 

## Référence

## Liens 