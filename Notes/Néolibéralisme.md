MOC : 
Source : [[Néolibéralisme]]
Date : 2023-04-14
***

## Résumé
A son origine une idée marginale, elle a finit par devenir la doctrine hégémonique dans les années 1980. Elle parraissait une solution à la crise des années 70 que le [[Keynésianisme]] mais ce n'était pas nécessaire, c'était une construction politique. Ça tombait bien comme ça et les experts étaient déjà prêts. Après la chute du communisme en Europe de l'Est, les politiques néolibérales se sont développées très vites car pareil il y avait beaucoup de conseillers : voir _Mass privatisation and the post-communist mortality crisis: a cross-national analysis_. C'était aussi apprécié des régimes militaristes tel ceux du Chili et de l'Argentine de l'époque. Cette doctrine s'est mise en place en France sous la présidence de Mitterand, c'était devenu le bon sens, la seule alternative.

C'est un terme qui n'a pas une définition précise mais il sert à décrire tout un courant et des sous courants qui en dérivent. C'est justement cette souplesse qui lui a permis d'être adopté par le grand nombre.

Définition par le libre marché
	Glorification du libre marché par le libre-échange et la libre circulation des capitaux.
Définition par la concurrence
	Suciter la concurrence à chaque fois que c'est possible, privatisation dès que possible par exemple.
Définition par l'action de l'Etat
	Ne dirige pas mais donne des aides économiques et législatives. Il permet aussi de dissoudre l'opposition.

Après la crise de 2008 par exemple, le rôle des Banques Centrales ne sont pas à négliger. Il fallait que les Etats aident l'économie pour qu'elle retrouve son niveau. 


Frustration le définit comme ça. Pronant donc la troisième définition.
> Le néolibéralisme n’est pas la non-intervention de l’État. C’est une intervention massive de l’État pour aider le capitalisme à fonctionner mieux et plus fort.

Foucault et Bourdieu:
[[Foucault et Bourdieu face au néo-libéralisme]]

## Référence
[[Inventing the Future. Postcapitalism and a World without Work]]
[[Frustration]]


## Liens 