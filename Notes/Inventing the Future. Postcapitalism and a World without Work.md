A book by [[Nick Srnicek]] and [[Alex Williams]]

### Notion de néolibéralisme
Idées libérales d'un attrait au libre-échange et des marchés internationaux. Susciter la concurrence dès que c'est possible. Mais pour eux c'est surtout que l'Etat a un rôle de créateur de marché, de soutien au [[Capital]], de dissuasion des révoltes populaires et de stabilisation des prix. Début de conception dans les années 30 face à la chute du libéralisme classique face au collectivisme. Finalement, réel avènement durant les années 70 et prise de pouvoir dans les années 80. Le but était directement d'organiser une utopie libérale et de monopoliser le terrain idéologique.

Diffusion des idées néolibérale à travers de think-tanks dans les années 50. Voir [[Friedrich Hayek]] et [[MPS]]. [[Milton Friedman]] aux US. Voir [[Ordolibéraux]]. Reconstruction de l'Allemagne après la GM2 qui va suivre directement les idées néolibérales et non keynésiennes. Cibler directement les universités, les médias et la police. Voir Antony Fisher 

### Notions clés
[[Folk Politics]]
[[Accélérationnisme]]
[[Post-capitalisme]]
[[Horizontalisme]]
[[Mouvement Occupy]]