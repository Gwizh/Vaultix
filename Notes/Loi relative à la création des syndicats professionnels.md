MOC : 
Source : [[Loi relative à la création des syndicats professionnels]]
Date : 2023-06-09
***

## Résumé
La loi relative à la création des syndicats professionnels, dite loi Waldeck-Rousseau autorise la mise en place de syndicats en France. Votée le 21 mars 1884, elle abroge la loi Le Chapelier et fixe leurs domaines de compétence. Elle est intégrée au [[Code du travail]].

## Référence

## Liens 