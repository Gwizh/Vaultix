MOC : 
Source : [[Gouvernement Pierre Waldeck-Rousseau]]
Date : 2023-06-09
***

## Résumé
Le gouvernement Pierre Waldeck-Rousseau dit « gouvernement de Défense républicaine » est le gouvernement de la Troisième République en France du 22 juin 1899 au 3 juin 1902. D'une durée de presque trois ans, il détient le record de longévité des cabinets de la IIIe République.

## Référence

## Liens 