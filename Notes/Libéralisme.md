MOC : 
Source : [[Libéralisme]]
Date : 2023-05-15
***

## Résumé
#### [[Tzitzimitl]]
https://www.youtube.com/watch?v=vAwy0pJfHws
Dans le sens politique du 19e siècle : [[Orléaniste]], représentant la gauche pendant la [[Révolution Française]] Sont devenus de droite lorsque qu'ils sont arrivés au pouvoir et ont pu mettre en pratique ce qu'ils voulaient. 

Le libéralisme peut être considéré comme la mise en pratique des idéaux philosophiques des [[Lumières]] en matière de gouvernance, de droits de l'homme et de liberté individuelle. Les Lumières ont fourni la base intellectuelle sur laquelle le libéralisme politique et économique a été construit.

#### Wikipedia

#### [[Liberalism : A Counter History]]

## Référence
Si je veux un avis centriste nul :
Le libéralisme contre le capitalisme --> Livre de Valérie Charolles
## Liens 