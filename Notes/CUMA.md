MOC : 
Titre : [[CUMA]]
Date : 2023-08-20
***
# Coopérative d'utilisation de matériel agricole
## Résumé
Une coopérative d'utilisation de matériel agricole est une société coopérative agricole de droit français ayant pour objet de mettre à la disposition de ses adhérents du matériel agricole et des salariés.

Les Cuma ont largement favorisé l'accès à la mécanisation dans les régions de petites exploitations dans lesquelles le coût de la mécanisation individuelle la rendait inaccessible. Instruments communautaires, elles permettent souvent d'enclencher des dynamiques de développement territorial. Elles permettent aussi aux agriculteurs d'utiliser un matériel performant dont l'investissement ne se justifierait pas sur une seule exploitation.

Il existe en France 11 740 Cuma regroupant 202 000 adhérents en 2018.

## Références

## Liens 
Rien à voir avec [[Cuba]] mdrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
[[Agriculture]]