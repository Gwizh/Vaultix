MOC : 
Source : [[Retraite par répartition]]
Date : 2023-04-14
***

## Résumé
La [[Retraite]] par répartition est un système de financement des caisses de retraite qui consiste à les alimenter par les cotisations, basées sur les revenus professionnels de travailleurs en activité (« assurance vieillesse »), lesquelles servent au paiement des pensions des retraités « au même moment ». En contrepartie des cotisations qu'ils versent, les salariés actifs acquièrent des droits qui leur permettront, à leur tour, de bénéficier d'une pension de retraite financée par les générations d'actifs suivantes. C'est donc, par principe, un système basé sur la [[Solidarité]] intergénérationnelle.

Dans le discours politique, on l'oppose au concept de [[Retraite par capitalisation]].

[[Bernard Friot]] dit qu'il y a dans ce concept à la fois un principe communiste et un principe capitaliste. La Giercarco machin de 1947 c'est de la répartition mais capitaliste

## Référence

## Liens 