MOC : 
Source : [[Ansar Dine]]
Date : 2023-07-02
***

## Résumé
Ansar Dine ou Ansar Eddine (arabe : أنصار الدين, ʾAnṣār ad-Dīn, « Les défenseurs de la religion ») est un groupe armé salafiste djihadiste fondé et dirigé par Iyad Ag Ghali. Apparu au début de l'année 2012, c'est l'un des principaux groupes armés participant à la guerre du Mali. Le 1er mars 2017, Ansar Dine fusionne avec plusieurs autres groupes djihadistes pour former le Groupe de soutien à l'islam et aux musulmans qui reste sous la direction d'Iyad ag Ghali.

Ce groupe ne doit pas être confondu avec son homonyme, mouvement légalisé en 1992, revendiquant 800 000 soutiens dirigés par le prédicateur Chérif Ousmane Haïdara, vice-président du Haut Conseil islamique malien. 

## Référence

## Liens 