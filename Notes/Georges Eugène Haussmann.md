MOC : 
Titre : [[Georges Eugène Haussmann]]
Date : 2023-10-06
***

## Résumé
Préfet de la Seine de 1853 à 1870, il a dirigé les transformations de [[Paris]] sous le Second Empire en approfondissant le vaste plan de rénovation établi par la commission Siméon2, qui vise à poursuivre les travaux engagés par ses prédécesseurs à la préfecture de la Seine, Rambuteau et Berger. Les transformations sont telles que l'on parle de bâtiments « haussmanniens » pour les nombreux édifices construits le long des larges avenues percées dans Paris sous sa houlette, les travaux réalisés ayant donné à l'ancien Paris médiéval le visage qu'on lui connaît aujourd'hui. 
## Références

## Liens 