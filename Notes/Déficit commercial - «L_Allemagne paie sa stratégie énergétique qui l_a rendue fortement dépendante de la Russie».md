[[Article]]
MOC : [[ECONOMIE]]
Titre : [[Déficit commercial - «L'Allemagne paie sa stratégie énergétique qui l'a rendue fortement dépendante de la Russie»]]
Auteur : [[Figaro]]
Date : 2022-07-12
Lien : https://www.lefigaro.fr/vox/monde/deficit-commercial-l-allemagne-paie-sa-strategie-energetique-qui-l-a-rendue-fortement-dependante-de-la-russie-20220712
Fichier : 
***

## Résumé
L'Allemagne connaît pour la première fois depuis longtemps une période de récession. Cela s'explique par son modèle économique très particulier qui mise sur l'importation d'énergie peu chère en provenance de Russie et d'une exportation massive de produits d'une très grande qualité en Chine. Or, ces deux points sont maintenant compromis, les sanctions sur la Russie ont rendu le gaz plus cher et la Chine peut maintenant concurrencer l'Allemagne sur ses propres marchés comme l'automobile notamment.

Ces inquiètudes sont à mettre en contraste avec les surperformances de l'économie allemande en 2018 et 2019, preuve que ce modèle reste toujours très efficace.


## Référence

## Liens 