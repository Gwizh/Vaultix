MOC : 
Source : [[Antoine Cyvoct]]
Date : 2023-07-13
***

## Résumé
Antoine Cyvoct, né le 28 février 1861 à Lyon (Rhône) et mort le 5 avril 1930 à Paris, est un militant anarchiste lyonnais impliqué dans le [[Procès des 66]].

Il est accusé, à tort, d’être l’auteur de l’attentat perpétré le 22 octobre 1882 au restaurant du théâtre Bellecour dit « L’Assommoir » et dans un bureau de recrutement de l’armée. 

## Référence

## Liens 
[[Anarchie]]