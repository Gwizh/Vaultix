[[TotalEnergies]], malgré des prix du gaz et du pétrole en baisse, a annoncé ce jeudi avoir engrangé au premier trimestre 2023 un bénéfice net en hausse de 12 % à 5,6 milliards de dollars.

L’an dernier, de janvier à mars, le géant français avait enregistré un résultat net de 4,9 milliards, tenant cependant compte d’une importante provision, conséquence d’une dépréciation de 4,1 milliards de dollars d’actifs en raison des sanctions contre la Russie. La hausse est plus nette par rapport au 4e trimestre de l’exercice 2022, à + 70 %.

20,5 milliards de dollars
Le groupe a annoncé, dans le même temps, la vente de ses activités d’exploitation des sables bitumineux au Canada à SunCor Energy pour 4,1 milliards de dollars, dans le cadre d’une stratégie de réduction de son empreinte carbone après l’annonce en début d’année d’un projet de scission de ses actifs canadiens.