MOC : 
Source : [[La littérature au service du colonialisme]]
Date : 2023-07-09
***

## Résumé

Le silence sur d’atroces réalités coloniales est lourd de sens, il va dans le sens de la complicité.


Souvent, quand on parle de la colonisation, on oublie souvent la part prise par certains écrivains dans la préparation de cette entreprise déshumanisante. Les uns soutenaient ouvertement l’entreprise coloniale, d’autres se taisaient, malgré l’évidence de la tragédie coloniale.

Zola écrivit un roman, "La débâcle", s'attaquant aux communards alors que Jules Vallès défendit ces "petites gens" dans un roman intitulé "L'insurgé". [[Emile Zola]] dont on cite toujours sa défense de Dreyfus prit position contre les communards en justifiant les massacres et les déportations de ces "gueux" et de ces "sauvages".

Certes, [[Victor Hugo]] n’avait pas condamné les communards, mais était un grand défenseur de la [[Colonialisme|colonisation]].

La liste va de [[Eugène Delacroix]] Chateaubriand à [[Jean Cocteau]], de [[Théophile Gauthier]] à [[André Gide]], [[Guy de Maupassant]] à Montherlant, en passant par [[Gustave Flaubert]], [[Alphonse Daudet]], Loti, Jammes, Louys et cent autres. »

Ces écrivains accompagnaient l’empire colonial, cherchant à justifier son entreprise de conquête tout en mettant en scène, dans leurs textes, les espaces marqués de soleil et d’exotisme, donnant à voir un monde stable peuplé de colons, positivement décrits et des indigènes, mal en point, sauvages, en haillons, de simples ombres fugitives. La dimension idéologique est prégnante. L’usage de longues descriptions s’inscrivait dans ce désir d’occulter les territoires humains et les tortures coloniales pour privilégier l’exposition de paysages et de natures mortes. Ce discours, péjorant et minorant les indigènes est le produit d‘une longue histoire faite de déni de l’Autre et d’un processus mémoriel travaillé par la fabrication d’une Afrique du Nord musulmane sauvage et barbare. L’écrivain se mue en illustrateur du discours colonial.

Même André Gide (1869-1951) évacue totalement les dimensions sociales et politiques privilégiant l’aspect esthétique et la sensualité des lieux et des personnages, comme s’il était aphone et aveugle.

L’écrivain reproduit ainsi, consciemment ou inconsciemment le discours impérial, participant indirectement de l’entreprise génocidaire coloniale. Jacqueline Arnaud fait un constat judicieux : « Tout livre d’un écrivain français sur l’Afrique du Nord est donc, volontairement ou non, un document, non seulement par ce qu’il dit, mais aussi par ce qu’il ne dit pas. »

![[Algérianisme#^09cc42]]

![[Algérianisme#^bc7073]]


## Référence

## Liens 
[[Colonialisme]]