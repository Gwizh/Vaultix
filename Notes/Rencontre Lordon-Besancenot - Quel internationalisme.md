MOC : 
Source : [[Rencontre Lordon-Besancenot - Quel internationalisme]]
Date : 2023-06-10
***

https://www.youtube.com/watch?v=thUEmJy6pc0

## Résumé
[[Olivier Besancenot]] :

[[Aléxis Tsípras]] et la [[Grèce]] doivent servir d'exemple pour la gauche quant à l'idée de contrer les politiques européennes de l'intérieur. Une fois au pouvoir lors de négociations, des fonctionnaires de la commission européenne sont venus dans les ministère accompagnés par l'armée (à vérifier) pour trouver où récupérer de l'argent pour payer la dette.

___
[[Frédéric Lordon]] :

Pour Lordon, l'internationalisme c'est un internationalisme qui sait qu'il ne pourra pas sauter au dessus de la question nationale. 
Modaliser 
Dialectiser

Pour lui le [[Brexit]] est n'est pas un nationalisme mais plutôt une façon nationale de reconfigurer l'internationalisme capitaliste, parce que c'est le [[Royaume-Uni]]
et la seule contrainte qu'ils ont c'est juste les contraintes (faibles mais existantes) de l'[[Union européenne]]. 

Lordon dit qu'opposer [[Nationalisme]] et [[Internationalisme]] dans l'Union européenne ce n'est pas juste dire que la sortie de l'UA conduit au nationalisme, au contraire. Il y a des façons de sortir de l'Euro qui sont internationaliste et qui préviennent la montée du fascisme. La Politique c'est pour lui d'appuyer sur le cours des choses pour faire apparaître les bons comportements. Par exemple, l'internationalisme de gauche c'est sortir de l'Euro et de nationaliser les banques (pas de cryptomonnaies of course), c'est créer des parlements européen des ouvriers, chercheurs, étudiants, etc... 

___
[[Olivier Besancenot]] :

Problème dans la gauche radicale sur les flux des personnes. On ne sait pas d'où ça sort mais certains en viennent à être contre la liberté de circulation, que des étrangers prennent le travail des français etc... Il y a une régression évidente sur ces questions. Il faut pouvoir dire qu'on est pour la liberté de circulation, d'installation et de la régularisation des sans-papiers. 

#Alire Sans patrie ni frontières, Jan Valtin

Les idées progressistes qui nous étaient évidentes au siècle dernière ont du plomb dans l'aile à cause de deux drames : le [[Stalinisme]] et la [[Sociale-Démocratie]]. Lui n'est pas inter-nationaliste dans le sens où ce sont plein de nations qui s'unissent peut-être mais avec une souveraineté qui reste nationale. Il assume et revendique un dépassement de ces idées. Les cadres de l'émancipation dépasse largement ce cadre et peuvent aller dans toutes les dimensions : nationales certes mais aussi continentales, et locales. Locale surtout au vu des dernières expériences concrètes.

___
[[Frédéric Lordon]] :

La forme désirable de l'[[État]], voila une question importante et non si on en veut ou pas, qui vient clore trop vite le débat. L'idée de créer une nation européenne est un nationalisme, un étatisme souverainiste. C'est la même chose que le nationalisme allemand au moment de la création de l'Allemagne. La dépassement de la souveraineté n'est pas questionné, c'est même l'idée de devenir plus gros pour concurrencer les autres états. Aujourd'hui les gens qui se revendiquent de cette idée veulent juste pouvoir jouer à jeu égal avec les Etats-Unis et la Chine. 

Il a quasiment les mêmes obsessions que Besancenot. 

___
[[Olivier Besancenot]] : 

N'accepte pas de voir la souveraineté se crée à partir d'un périmètre arbitraire, venant du haut. Car dans tous les cas, les gens doivent pouvoir redéfinir leur souveraineté dans ses dimensions et ses paramètres. Il croit au contact des entités souveraines là où se trouve leur périmètre. 

Parle du [[Kurdistan]], qui à la base se voyait comme un etat [[Marxisme]]-[[Vladimir Lénine]] mais qui a évolué en comprenant que le cadre de la souveraineté nationale allait les étouffer. Le [[Rojava]] aujourd'hui ainsi que le [[Parti des travailleurs du Kurdistan]] sont des exemples concrets d'une définition dynamique de la souveraineté. 

De plus l'internationalisme en France c'est aussi voir notre [[Impérialisme]] à travers le [[Colonialisme]] toujours présent. C'est vraiment une exception en Europe qu'un pays qui intervient militairement en Afrique chaque année pour défendre des multinationales françaises. C'est terrible d'oublier que ce sont les banques françaises et allemandes qui ont fait couler la Grèce par la spéculation. L'internationalisme c'est voir et affronter notre nationalisme. 

___
[[Frédéric Lordon]] :

Souveraineté : le nom d'un décidé collectif. Si on dit que la souveraineté c'est "C'est nous qui décidons", on généralise et on voit qu'on a besoin d'aller plus loin. Qui est ce nous. Peut-il changer ? Qui peut entrer dedans ? etc... Puis pour lui le concept de nation c'est la même idée mais prise à une dimension supérieure avec une question de nombre. Ce qui différencie peut être communauté et nation c'est le fait qu'on puisse faire nation avec des gens qu'on ne connait pas et qu'on ne veut pas connaître. Le [[Chiapas]] et le [[Rojava]] sont des nations selon ces définitions générales, même si elle sont prises dans une autre nationalité. Preuve pour le Chiapas : [[Armée zapatiste de libération nationale]]. Les usines auto-gérés de l'Argentine ne le sont pas. 

___
[[Olivier Besancenot]] :

Lui définit le nous à partir de la question sociale : nous ne sommes pas les mêmes que les milliardaires qui contrôlent autant que des milliards de personnes. Nous ne sommes pas actuellement les décideurs. Il parle d'un truc que j'adore (décidément je l'aime) : nous sommes tous différents et il y a constamment un mélange, un métissage qui dépasse n'importe quel périmètre donné.  Son obsession sur la question stalinienne est cette haine de l'idée d'une armée de clone où on nie les différences des individus. 

___ 
[[Frédéric Lordon]] :

Collectivement quand on voit qu'on ne peut pas faire corps : ne pas faire partie du nous. On voit qu'on a des scissions froides comme le [[Divorce de velours]] ou sinon c'est la [[Guerre civile]]. Individuellement, c'est devenir apatride. 

#Alire Comment j'ai cessé d'être juif, Shlomo Sand : il dit renoncer à être juif plutôt que d'arrêter d'être israelien du fait des politiques terribles de son gouvernement. Il ne peut pas physiquement renoncer à sa nationalité, ce serait trop dur à porter par rapport à son existence matérielle.

La définition du nous n'est pas de trouver un groupe avec lequel on est d'accord, ça ne peut pas fonctionner comme ça, ce serait trop dangereux. 

___
[[Olivier Besancenot]] : 

Il faut de la démocratie à toutes les échelles de souveraineté. Notamment aujourd'hui avec l'[[Écologie]] car il faut une coordination entre les entités de souveraineté, pour les ressources, pour les territoires etc... Il ne veut pas définir ce nous, ce périmètre par peur d'étouffer dedans ou d'étouffer des gens dedans. C'est quelque chose de dynamique qu'on ne peut pas prévoir et qu'on a pas besoin de prévoir.  

## Référence

## Liens 