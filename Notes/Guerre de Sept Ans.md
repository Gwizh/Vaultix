MOC : 
Source : [[Guerre de Sept Ans]]
Date : 2023-04-10
***

## Résumé

Tiré de La date de début de la 3eme Guerre Mondiale

Oui c'était un clique-bête, je ne parle pas d'une guerre future. Mais alors peut être que je ne sais pas compter ? Si, mais le monde ne connaît que l'histoire du 20e siècle, c'est quasiment le seul qu'on apprend à l'école. Pourtant, une autre guerre mériterait qu'on en parle plus, bcp plus, car elle a eu autant voire plus d'impact que les deux guerres que nous connaissons tous. 

Je parle de la guerre de 7 ans (oui forcément avec un nom comme ça c'est moins mémorable). J'aimerais qu'on l'appelle la guerre mondiale 0, merci :) (d'où le titre hihi)

Mais alors, la guerre de 7 ans c'est quand ? Si c'est pas au 20e, c'est au 19e ? Nope même pas, c'était au 18e ! Mais alors, était-ce vraiment mondiale ? Alors bah voilà on va voir si je sais compter : Europe, Amérique du Nord, Amérique du Sud, Inde, euuh 4 sous-continents :) Alors oui je sais compter ! Fin de l'article ! 

… 

Bon, je vous vois avec vos yeux de ninja qui doutent -_-. C'est pas assez mondial pour vous je suppose, rien en Afrique? Rien en Extrême Orient ? Pffff. Mais en fait d'après la première et deuxième guerre, le fait qu'il y ait des colonies européennes un peu partout suffit à rendre ce conflit mondial. Ah bah alors si on compte comme ça je peux aussi compter l'Afrique et l’Océanie, plutôt mondial alors ! 

Bref, fini de me justifier, la question maintenant c'est : quelle est l'influence de cette guerre, est-elle aussi importante que les deux du siècle dernier ? 
Oui ! Mais alors tellement ! 

C'est l'heure du contexte !!! Yayyyyyyyyyyyy
La guerre de sept ans, c'est de 1756 à 1763, oui le compte est bon. Voici la carte :

Quelles sont les grandes puissances ? En premier lieu il faut dire la France (plus grande puissance militaire), le Royaume-Uni (plus grande puissance navale et coloniale) et l'Autriche (en bleu ciel sur la carte, empereur du [[Saint Empire Romain Germanique]] avec les contours en bleu foncé). 
En second lieu, nous avons la Russie, l'Espagne, la Prusse (gardez celle-ci en mémoire), et dans une moindre mesure la Suède et le Portugal. (Je ne parle pas des Ottomans et de l’Empire Qing qui étaient bien sur de grandes puissances mais non concernées ici) 

Et pourquoi la guerre a commencé ? Alors c'est là que l'histoire se répète. Si je dis que l'Allemagne est devenue un peu expansionniste, ça surprend ? Bon c'est pas juste de dire ça parce que la même chose peut être dite des Anglais et Français mais ce sont toujours les Allemands les méchants apparemment. Mais je me contredis, il n'y a encore d'Allemagne j'ai dis. Certes mais comme je vais l'expliquer plus tard, la Prusse va devenir au fil du temps l'unificateur du peuple allemand, donc ça compte. Et oui du coup c'est la Prusse qui a attaqué (la Saxe en l'occurrence, déso Theresa). Et selon le système de l'Empire Germanique, l'Empereur (l'Autriche si vous avez suivi -_-) se devait de la protéger, c'est ce qu'elle a fait. Ensuite, par un jeu d'alliance, la France et la Russie sont entrées en guerre avec l'Autriche tandis que le duché de Hanovre et le Royaume-Uni sont entrés en guerre avec la Prusse. Et honnêtement ça arrangeait la France et le Royaume-Uni parce qu'ils avaient déjà commencé à se battre en Méditerranée et en Amérique du Nord et plus on est de fous plus on rit haha. Donc voila, l'Europe retrouve la guerre. 

Quelles sont les grandes puissances ? En premier lieu il faut dire la [[France]] (plus grande puissance militaire), le [[Royaume-Uni]] (plus grande puissance navale et coloniale) et l'[[Autriche]] (en bleu ciel sur la carte, empereur du Saint Empire Romain Germanique avec les contours en bleu foncé). 
En second lieu, nous avons la [[Russie]], l'[[Espagne]], la [[Prusse]] (gardez celle-ci en mémoire), et dans une moindre mesure la Suède et le Portugal. (Je ne parle pas des Ottomans et de l’Empire Qing qui étaient bien sur de grandes puissances mais non concernées ici) 

Et pourquoi la guerre a commencé ? Alors c'est là que l'histoire se répète. Si je dis que l'Allemagne est devenue un peu expansionniste, ça surprend ? Bon c'est pas juste de dire ça parce que la même chose peut être dite des Anglais et Français mais ce sont toujours les Allemands les méchants apparemment. Mais je me contredis, il n'y a encore d'Allemagne j'ai dis. Certes mais comme je vais l'expliquer plus tard, la Prusse va devenir au fil du temps l'unificateur du peuple allemand, donc ça compte. Et oui du coup c'est la Prusse qui a attaqué (la Saxe en l'occurrence, déso Theresa). Et selon le système de l'Empire Germanique, l'Empereur (l'Autriche si vous avez suivi -_-) se devait de la protéger, c'est ce qu'elle a fait. Ensuite, par un jeu d'alliance, la France et la Russie sont entrées en guerre avec l'Autriche tandis que le duché de Hanovre et le Royaume-Uni sont entrés en guerre avec la Prusse. Et honnêtement ça arrangeait la France et le Royaume-Uni parce qu'ils avaient déjà commencé à se battre en Méditerranée et en Amérique du Nord et plus on est de fous plus on rit haha. Donc voila, l'Europe retrouve la guerre. 

Et là je sais que c'est déjà long donc je vais résumer les points importants :
La France, malgré la taille de son armée, n'est pas aussi ingénieuse sur le champ de bataille que la Prusse, surprenamment bien organisée pour sa taille. 
Le Royaume-Uni, du fait de l'hégémonie absolue de sa Royal Navy, n'est même pas inquiété en Europe, et réussit au long terme, grâce au contrôle de l’Atlantique, à battre les Français en Amérique. 
L'Autriche démontre encore une fois que sa puissance est surtout diplomatique et que la guerre la réussit moins, elle arrive à faire des avancées mais la Prusse est bien plus avancée militairement. 
500 000 à 800 000 civils et environ 650 000 soldats sont tués, certes c’est beaucoup moins que les guerres du 20e siècle mais ya eu une chtitite révolution industrielle entre temps vous savez. Ce sont la Prusse et la France qui ont perdu le plus d’hommes avec 180 000 et 165 000 respectivement morts au front.

Et là vous voyez une tendance (sauf si vous lisez en diagonal grrr), ouep, la France a perdu cette guerre, énormément d'ailleurs, à un point où elle perd complètement la face. Mais quelles en sont les conséquences ? 

Et bien cette guerre va être le point de départ d'une Époque Moderne, pas immédiatement bien sûr mais la bulle qui va éclater plus tard a commencé à grandir à partir de là. Pourquoi ? Parce que la guerre a été surprenamment longue, et coûteuse. La France et le Royaume-Uni, qui étaient déjà endettés jusqu'au cou, voulaient justement faire une courte guerre à la base, cherchant à éponger ces dettes avec l'argent du vaincu. La guerre était beaucoup trop longue pour ça. Alors ces pays ont connu une crise économique sans précédent, et je pèse mes mots. De là découle des points cruciaux de l'histoire du monde : 

L'[[Indépendance des États-Unis]] en 1776, le Royaume-Uni ayant essayé de taxer de plus en plus ses colonies. (Je pense que ce point seul est suffisant en terme d'impact non?) 
La [[Révolution Française]] en 1789, c'est un point important parce qu'en tant que Français on en entend tellement parler qu'on pense presque qu'on en fait tout un fromage pour pas grand chose. Pourtant, wow, la Révolution française a changé le monde. Le petit [[Napoléon 1er]] qui est venu après a juste complètement redessiné les cartes de l'Europe et de l'Amérique (l'indépendance de toutes les colonies sauf le Canada). On parle quand même de 25 ans de guerre quasi constante en Europe jusqu'en 1815 hein. 
La montée de nouvelles idées (République, Laïcité, [[Nationalisme]]) et le rejet d’anciennes (Féodalité, Esclavagisme) amenée par les classes sociales les plus touchées par la crise, qui vont aussi faire basculer le monde dans une nouvelle ère. 
La montée de la Prusse comme unificateur d'un peuple allemand (une idée nouvelle), qui va évidemment déboucher sur une des plus grandes puissances mondiales. 

Bref, c'est une guerre mondiale qui a marqué son époque et j'ai du mal à comprendre pourquoi on en parle pas plus. 


## Référence

## Liens 