[[Article]]
MOC : 
Titre : [[Nicole Notat, comment la CFDT est-elle devenue « réformiste »]]
Auteur : [[Usul]], [[Ostpolitik]] [[Blast]]
Date : 2023-04-05
Lien : 
Fichier : 
***

## Résumé
Cheffe de la CFDT de 1992 et 2002. 
"Pas bête et méchant comme la CGT" Dialogue social et pas de blocage

[[Syndicalisme]] d'accompagnement et non révolutionnaire :)

Proche de Macron et a failli être première ministre à la place d'[[Elizabeth Borne]]

"J'aime tellement la négociation que j'en suis venue à aimer l'entreprise." - Notat

En partie pour le [[Plan Juppé]], tout ça fut vu comme une trahison .

Mais avant ça dans les années 60-70, la CFDT c'était l'Auto-gestion et la PSU. CFDT a été crée en 1964 et avec une grande convergence avec le PSU de [[Michel Rocard]]. Les idées sont beaucoup moins étatistes que la CGT, le PCF et le [[Parti Socialiste]]. En 1981, après l'élection de [[François Mitterand]], il ya surtout un dialogue avec le PC et la CGT. La CFDT a beaucoup été influencé par [[Edmond Maire]] qui a apolitisé le syndicat et a marginalisé les gauchistes. C'est le recentrage.
Il dit que la grève est une vieille mythologie syndicale :O.

Nicole Notat suit complètement la ligne recentrée d'Edmond Maire. Elle était pour la mondialisation, les délocalisations etc...

A lire, CFDT socilologie d'une conversion réformiste.

Les grèves sont archaïques et ceux qui soutenaient ces grèves étaient virés. 

A voulu négocier avec le [[MEDEF]] pendant les années [[Lionel Jospin]].

## Référence

## Liens 