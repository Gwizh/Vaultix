MOC : 
Source : [[Bonapartisme]]
Date : 2023-05-15
***

## Résumé
Culte du chef, xénophobie, nationalisme. Ni gauche ni droite, cherchant à rassembler pour combattre un ennemi commun venant de l'étranger ou d'un complot. Toujours finalement de droite : défendre des valeurs traditionnelles nationales c'est du [[Conservatisme]]

## Référence

## Liens 
[[Boulangisme]]