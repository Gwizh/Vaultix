MOC : 
Titre : [[Les sauvages de la civilisation]]
Date : 2023-10-05
***

## Résumé

## Détails
### En marge
Concept de "sauvages de la civilisation" inventé au XIXe siècle à l'endroit des chiffonniers de Paris. Mis au ban des "classes dangereuses" situés aux marges de la ville, quelques part entre abandon et transgression.

### Introduction - Aux origines d'un monde liminaire
Zones : anciennement une zone réservée aux manœuvres militaires.
Vocable "zonier" pour en parler.

- Description de Courget 

Huttes et cabanes installées sur un terrain militaire.
Une honte pour Paris.

L'expression sauvages de la civilisation vient de [[Alfred Delvau]] et son livre *Les Dessous de Paris*
Un peu de compassion mais surtout du cynisme. Il parle des sauvages comme des "autres" qui serait loin en temps et en espace. Comparaison à la fois avec les Peaux-Rouges et les gueux médiévaux. "Race prolifique et tenace". Rien n'en vient à bout : "ni le canon, ni la peste, ni la famine, ni la misère, ni la débauche , ni l'école mutuelle."

Entamé dans les années 50 : le périphérique qui remplace la Zone. À partir de là la Zone est dématérialisée.

Inspiration du livre :
	#Alire Peter Wagner, Liberté et Discipline
	Concept de description en bottom-up p26 --> Mélange de micro et macro analyse
		Super important en [[Anthropologie]] et source de violence au siècle dernier.

#### Archéographie
Archê : Commencement et commandement, ou pouvoir
Concept d'[[Hannah Arendt]].
Couplé avec [[Michel Foucault - Que sais-je]]
	Archéologie des discontinuités historiques

#### Au sources des "classes dangereuses"
Concept de lumpenproletariat p35
	Une classe dangereuse qui serait au dela de la classe ouvrière. Évoqué par [[Karl Marx]] et [[Friedrich Engels]] notamment. Figure de l'incontrôlable.

Idée que les chantier d'[[Georges Eugène Haussmann]] ont créé le territoire bourgeois p36
	Changement de la capitale pour correspondre à la nouvelle classe dominante. Fin en 1870.
	Comme dans [[1888, Jack l'Éventreur#^fbc40f]]

Rapport avec la lumière p38
	"Dans le clair obscur où les uns (qui se placent du côté de la "civilisation") ne s'éclairent qu'à la faveur de l'ombre projeté sur les autres (qu'ils traitent de "sauvages")"

### Côté Zone 1
#### Au pied du mur
##### Sentiment d'injustice - p45
Les zoniers échappent aux règles, volent un terrain, construisent illégalement et créent un marché noir.
Marché noir à l'entrée de la ville, profits supposés.
Les ouvriers pensent qu'ils gagnent + car ils n'ont ni taxes, ni douanes ni rien.

##### Rapport aux patrons - p57
"Métiers" des zoniers : cueillir, récolter et vendre.
Cela consiste au fait de toujours se déplacer pour trouver du revenu.
C'est donc toujours un travail nomade non encadré par un patron.

"Errance urbaine" fait le "type social". Mais iels restent de bons pauvres par rapport aux migrants.

Dernière phrase p63 : Les protéger d'eux-mêmes et leurs penchants nomadique et alcoolique. Iels font ça par pulsion, par émotion.

#### Situer les classes dangereuses
##### Destruction et construction d'Haussmann p72
[[Georges Eugène Haussmann]]
Les grands projets d'Haussmann avaient plusieurs buts hors l'aspect esthétique.
1. Éviter les barricades. Il fallait donc constuire de grandes avenues pour permettre à l'armée de passer dans la ville.
2. Vider le centre. Créer de grands espaces et reconstruire pour gentrifier et vider le centre-ville des pauvres.

p82 : Sécurisation des espaces publics.
Il y aurait autour de Paris un "ceinture sauvage"
On laisserait alors l'insurrection dehors pour pouvoir se protéger.

[[Friedrich Engels]] dit que la bourgeoisie déplace les prolos car elle ne "sait pas la gérer".

##### Autre peuple - p77
Les zoniers seraient un peuple + étranger que les japonais. 
C'est un peuple intemporalisé, primitif, qui n'évolue pas. Ce serait le même qu'au [[Moyen-Âge]]. En fait, on fait un double amalgame. Tout ce qui n'est pas européen et descendant des Lumières seraient moyen ageux et sauvage. On pousse loin dans l'espace et dans le temps.

Ces gens seraient des sauvages. Origine de l'expression p252.

Poème p99.

Par nature violent, insolent et superstitieux, pire que les méchants des Grands Lacs.

##### Marx et les classes dangereuses - p91
[[Karl Marx]] considérait cette classe dangereuse comme un ennemi de la révolution prolétaire de part leur appât du gain. Même sentiment de menace que les bourgeois du baron Haussmann mais par pour les mêmes raisons évidemment.

#Alire Association des écrivains et artistes révolutionnaires

#### Les conteurs du crime
##### Apaches - 113
[[Dominique Kalifa]]
Exotisme des plaines. Très populaire dans les journaux et les cabarets. 
"Interdit de suriner"

Descripion physique p118-119
Zone comme base de repli après les actes en ville.
#Alire Casques d'or de Becker.

[[Sexisme]] p124

##### Critique de la République - 116
Justification que l'école et l'armée ne répondent pas à leurs devoirs si une telle population existe. Ce qu'iels leur faudrait, ce serait un foyer et une religion.

##### Lien avec Courants Dominants
Adam veut faire travailler ce qui le veulent. Il essaie de faire travailler ces gens mais "échoue".
Salariat et emploi pour sauver ce peuple.
Zone comme ceinture noire de poudre sale qui pourrait prendre feu (se révolter).

Ce sont des chomeurs involontaires.

##### Paternalisme - 130
[[Aristide Bruant]]
Chansons utilisant les mots de la zone.
"Défendre le Peuple avec lui et au besoin contre lui."
"Pas emprunter la voix mais parler pour eux."

p134 : [[Émile Zola]] condescendant.

Domination empathique.

#### Les diagnostics et leurs mesures

##### Combattre l'insalubrité - p141
1913 : Paris obtient la Zone de l'État.
Volonté de verdir la capitale mais plutot de remplacer la Zon,e (cette "crasse indélébile") par une ceinture verte (des parcs).

Littéralement l'idée de nettoyer Paris.

Déjà à l'époque in sous entendu (crasse = crime, pauvreté, etc...)

Intersection du social et de la biologie pour critiquer la nature de ces zoniers.

p142 : Dégénérescence de la race française !
	Virer les parasites pour protéger la race et retrouver la victoire. 

Question d'hygiène et de génétique. [[Darwinisme Social]].

Habitations non pérennes, illégales et précaires.

##### Porteurs de maladies - 146
"Maladie n°9" -> Peste bubonique à Paris (zone nord). 34 morts.
Miasmes venant des déchets dans lesquels iels sont.
Comparés aux rats du Moyen Age
Population nomade venue de l'est (asiatiques ou juifs) porteurs idées révolutionnaires (anti-rep) et de maladies dangereuses p152

Microbe communiste et anarchiste
Mdr 154 : intégrer les chiffoniers dans les usines pour mieux les encadrer
Ceinture rouge.

p159 : Dynamique des experts.

##### Projet d'inclusion - p163
Reloger et employer ces populations
Construire des logements bon marché. 

p167 [[Jean Giraudoux]] parlant d'une guerre de tranchées entre l'administration et les locaux. 

Accord passé avec les "gds zoniers" qui ont le droit de rester sur la zone et d'employer cette main-d'oeuvre

p169 : "Ville = organisme qui est gouverné par la tête pensante"

[[Céline]] conservatisme fascisant. 

#### Le fantastique social et ses clichés
#### Fatalité - p189
Une fois dans les logements fournis, iels les cassent et utilise les portes pour le feu et les baignoires comme poubelles.

#Alire La Zone p215

Peur d'un million de révoltés p229

p278 Parfaitement ça : "Il faut leur pardonner à ces sauvages..."
p289 : ville change + vite que les moeurs
Enfants perdus (ADN)
Descendants de la cour des miracles --> Lépreux.

#### Notre Dame de la mouise
##### Paternalisme contre communautarisme - p254
Dominations hiérarchiques et empathiques
"Guider les enfants turbulents vers la lumière, le salut chrétien ou le Grand Soir."
"Distance irréductible qui finit par l'emporter."
Assimilation pour s'assurer que le groupe "participe à la société"
"On peut sauver les corrompus en les guidant"

##### Entre abandon et transgression
##### Représentations théâtrales et photographiques - 267
Fréhal p266
Aristide Bruant
p181 [[Joseph Kessel]]
p273 [[Harry Grey]] 
#Alire p273 à 275 !
"Construit à la nègre."

#### Archives et dispersion
##### Réaction vichyste - p299
En 1940, plan de rénovation
Détruire et reconstruire partout --> Combattre le chomage
Exproprier les habitants 
Les déporter pour certains
Nettoyer...
[[Zucca André]]

"L'objectivation garde à distance les sujets" p304
"Urgence sanitaire"
"Propagation du mal"

20% de la population zonière n'a pas été retrouvée.
Hygiéniste -> santé de la race
Chevalerie moderne = sport.

##### Regard des experts - p321
Catholicisme social 
Près mais pas assez pour opérer une critique des pouvoirs publics.
Lumpensolidarité
Culture de la pauvreté.
#### Dans les restes du monde
##### Histoire des zoniers - les dernières parties
[[Georges Brassens]] Pensée anarchiste
[[Renaud]] réutilise le vocab 

Inspiration culture américaine, motard et Hell's Angels, Bikers

Incarner le mal contre la bien pensance, quitte à passer à l'ED.

Punk, Skinhead.

#### Conclusion - p381
"Paternalisme conservateur" p384


### Autres
p133 : Tradutorre, traditore : traduire c'est trahir.

## Références

## Liens 