MOC : 
Source : [[Courant lambertiste]]
Date : 2023-04-06
***

## Résumé
Le courant dit « lambertiste » est un courant trotskiste lancé par Pierre Boussel, alias « Pierre Lambert », représenté par la Quatrième Internationale lambertiste et présent dans plusieurs pays du monde.

Certains dirigeants du Parti socialiste sont d'anciens lambertistes. Parmi eux, [[Lionel Jospin]], les frères David et Daniel Assouline, [[Jean-Christophe Cambadélis]] et [[Jean-Luc Mélenchon]].

## Référence

## Liens 