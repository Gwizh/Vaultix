MOC : 
Titre : [[Croix-de-Feu]]
Date : 2023-09-25
***

## Résumé
L'association des Croix-de-Feu, ou Association des combattants de l'avant et des blessés de guerre cités pour action d'éclat (1927-1936), est à l'origine un mouvement d'anciens combattants français de la Grande Guerre qui se transforme ensuite en organisation politique [[Nationalisme|nationaliste]], voire [[Fascisme|fasciste]] selon certains historiens. Elle est dirigée par le colonel [[François de La Rocque]] (1885-1946).

L'association est dissoute en 1936 par le gouvernement du [[Front populaire]], puis remplacée par le Parti social français. 

## Références

## Liens 