MOC : 
Source : [[Paradoxe de la tolérance]]
Date : 2023-05-10
***

## Résumé
Le paradoxe de la tolérance affirme que si une société est tolérante sans limite, sa capacité à être tolérante est finalement détruite par l'intolérant. [[Karl Popper]] l'a décrit comme l'idée apparemment paradoxale que « pour maintenir une société tolérante, la société doit être intolérante à l'intolérance. » Popper développe cela en écrivant : « Je n'implique pas, par exemple, que nous devions toujours supprimer l'énoncé des philosophies intolérantes ; tant que nous pourrons les contrer par des arguments rationnels et les contrôler par l'opinion publique, la suppression serait très imprudente. Mais nous devons revendiquer le droit de les supprimer si nécessaire, même par la force. »

## Référence

## Liens 
[[Platon]]