MOC : 
Titre : [[Fair Trade League]]
Date : 2023-09-17
***

## Résumé
The Fair Trade League was a British pressure group formed in August 1881 to campaign for protectionism.

Lord Dunraven was President of the League.

The Liberal Prime Minister William Ewart Gladstone in response to the forming of the League delivered a root and branch defence of free trade in a speech in Leeds in October. Thomas Farrer, the permanent secretary of the Board of Trade, wrote Free Trade versus Fair Trade specifically to argue against the League's proposals.

## Références
[[Protectionnisme]]
## Liens 