MOC : 
Source : [[Simone Weil]]
Date : 2023-05-12
***

## Résumé
Elle a suivi une éducation philosophique en étudiant Marx, Descartes et Platon. Elle s'est converti tardivement au christianisme tout en restant ouverte aux autres religions.
Elle entre dans une usine en 1934 pour voir le quotidien des prolétaires. 
Elle participe à la [[Guerre civile espagnole]] de 1936 pour "voir le malheur" des gens à l'arrière.
Elle devient résistante pendant la guerre et meurt en 1943 de la tuberculose.
Elle faisait régulièrement des grèves de la faim.
Vivait avec un salaire minimum pour connaître les conditions difficiles. 
Elle était aimé d'[[Albert Camus]] et [[Hannah Arendt]] pour [[L'Enracinement]]
[[Marxisme]] et anti-[[Stalinisme]].

## Référence

## Liens 