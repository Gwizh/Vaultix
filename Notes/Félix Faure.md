MOC : 
Source : [[Félix Faure]]
Date : 2023-06-09
***

## Résumé
Félix Faure, né le 30 janvier 1841 à Paris et mort le 16 février 1899 dans la même ville, est un homme d'État français. ==Il est président de la République du 17 janvier 1895 à sa mort.==

Issu d'une famille modeste, il entame une carrière de tanneur, avant de devenir un riche négociant en cuir. Progressivement, il entre en politique, œuvrant d'abord à l'échelon local, dans la ville du Havre, avant d'être élu député de la Seine-Inférieure à quatre reprises, siégeant parmi les Républicains modérés à la Chambre des députés.

Désigné ministre de la Marine par Charles Dupuy en 1894, il est élu, quelques mois plus tard, président de la République grâce à l'appui des monarchistes et des modérés ligués contre la candidature d'Henri Brisson, du centre-gauche. Sa présidence est d'emblée marquée par l'[[Affaire Dreyfus]], qui divise la France en deux camps résolument opposés.

Les circonstances de sa mort, survenue brutalement au palais de l'Élysée quatre ans seulement après son élection et alors qu'il se trouvait en compagnie de sa maîtresse Marguerite Steinheil, sont passées à la postérité.

## Référence
Précédent président : [[Jean Casimir-Perier]]
Président suivant : [[Émile Loubet]]

## Liens 