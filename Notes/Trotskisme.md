MOC : 
Source : [[Trotskisme]]
Date : 2023-06-01
***

## Résumé
Le trotskisme, parfois nommé bolchévisme-léninisme, est une philosophie politique de type [[Marxisme|marxiste]] se réclamant de [[Léon Trotski]], de ses écrits, de son action et de ses idées. L'expression est d'abord apparue chez les staliniens, pour laisser entendre que les idées défendues par Trotski, dans les années 1920 seraient opposées à celles de [[Vladimir Lénine]]. Après 1924, l'idéologie trotskiste se distingue surtout par son opposition à la vision stalinienne du communisme, en contestant le règne de la bureaucratie (nom donné par Trotski à la nomenklatura) et en prônant la démocratie et la liberté de débat au sein du [[Parti communiste]].

Le trotskisme soutient en premier lieu les principaux aspects du léninisme, à savoir l'idée de la construction d'un parti ouvrier révolutionnaire, de l'[[Internationalisme]] et de la [[Dictature du prolétariat]] comme base de l'autoémancipation de la classe ouvrière et de la démocratie directe. Le terme « trotskisme » est cependant utilisé comme idéologie communiste opposée au « marxisme-léninisme » (stalinisme), notamment à la théorie du « socialisme dans un seul pays », mais surtout pour critiquer la bureaucratie stalinienne qui s'est développée en Union soviétique.

Trotski fonde la [[Quatrième Internationale]] en France en 1938, à la suite de l'exclusion violente des Oppositions communistes de la Troisième Internationale, et à la répression qui s'est abattue sur les opposants en URSS. Déjà divisé du vivant de Trotski, le trotskisme éclate en multiples tendances (pablisme, lambertisme, Union communiste, posadisme, morenisme…) après la Seconde Guerre mondiale.

[[Entrisme]] : entrer dans les autres partis de gauche pour influencer les lignes officielles. 

En France, il n'y a en 2023 qu'un seul parti qui se revendique du trotskisme : [[Lutte ouvrière]]. 

## Référence

## Liens 