### What is an artifact and Artifact Repository?
#### Artifact
The files that contain both the compiled code and the resources that are used to compile them are known as artifacts. They are readily deployable files.

In Java an artifact would be a .jar, .war, .ear file
In NPM the artifact file would be a .tar.gz file
In .NET the artifact file would be a .dll file

Source code -> Build tool -> complilation -> Binary code -> Artifact (with the dependencies/ resources).

#### Artifact repository
An artifact repository is a repository which can store multiple different versions of artifacts. Each time the war file or tar.gz file is created it is stored in a server dedicated for the artifacts.

Pour corriger des erreurs ou juste vérifier l'exécution des anciens artifacts. On peut donc déployer d'anciuenne versions tres facilement

#### Sources vs Binaries
Source : Text, Diffable (can compare), Versioned by content, stored by override
Binaries : Blob, Not diffable, versionned by name, not stored through override

### What is JFrog Atrifactory?
JFro Artifactory is a tool used in DevOps methodology for multiple purposes. One of it's main purpose is to store artifacts (readily deployable code) that have been created in the code pipeline. Another one of it's purpose is to act as a sort of a buffer for downloading depedencies for the build tools and languages.
![[Pasted image 20230827140136.png]]

Store all the artifacts
Help in downloading and managing dependencies (for local servers)
### Use Cases of JFrog Artifactory
