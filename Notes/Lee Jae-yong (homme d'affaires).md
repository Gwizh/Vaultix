MOC : 
Source : [[Lee Jae-yong (homme d'affaires)]]
Date : 2023-06-09
***

## Résumé
Lee Jae-yong (en coréen : 이재용), né le 23 juin 1968, connu professionnellement comme Jay Y. Lee, est un homme d'affaires sud-coréen. Il est chef de facto du Groupe [[Samsung]] depuis 2014, et en devient président officiellement le 25 octobre 2020, à la mort de son père, Lee Kun-hee.

Corruption !!!!!

En janvier 2017, Lee est accusé « de corruption, de détournement de fonds et de parjure ». Après un mois d'enquête, il est arrêté dans la nuit du 16 février 20171. Le 28 février 2017, il est mis en examen pour corruption et détournement de fonds. Le 25 août de la même année, il est condamné à cinq ans de prison. Le 28 août 2017, il fait appel de sa condamnation4.

Le 5 février 2018, il est libéré de prison par la cour d’appel, qui écarte la plupart des poursuites pour corruption, et lui inflige une peine symbolique de prison avec sursis, ce qu'une députée conteste en dénonçant une « république Samsung », tant l'influence du groupe est grande dans les milieux judiciaire, politique et médiatique.

Mais la Cour suprême ordonne un nouveau procès, et le 18 janvier 2021, il est condamné par un tribunal de Séoul à deux ans et demi de prison, et réincarcéré.

Il reçoit le soutien du patronat américain, qui tente d’exercer des pressions sur les autorités sud-coréennes afin d'obtenir sa libération. La plupart de la presse sud-coréenne - généralement proche des milieux d’affaires -, l'opposition conservatrice et la majorité de l'opinion publique soutiennent sa libération. Le Parti démocrate au pouvoir se montre plus réticent à l'idée d'une grâce présidentielle ; Park Yong-jin, un des candidats à l’élection présidentielle de 2022, y voit un « affront à l’État de droit », montrant que « les riches et les puissants ont toujours une issue favorable ».

Lee obtient, en août 2021, une libération conditionnelle de la part du gouvernement sud-coréen.

En octobre 2021, son nom est cité dans les [[Pandora Papers]].

En août 2022, à la fin de sa peine de prison, il obtient la grâce présidentielle qui lève l'interdiction d'exercer pendant cinq ans ses fonctions au sein du groupe Samsung.

## Référence

## Liens 