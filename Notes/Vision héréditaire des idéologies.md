MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081629
Tags : #Fini
***

# Vision héréditaire des idéologies
p116
L'entretien est d'autant plus important que le système est étendu et complexe.
Je cite un peu : "Pour survivre, l'Egypte, la Mésopotamie et la Chine anciennes devaient surveiller, entretenir et réparer en permanence de complexes systèmes d'irrigation. Cela exigeait un Etat immenseet tout puissant : ces anciennes "sociétés hydrauliques" furent non démocratique par nécessité. On pouvait dès lors conclure que les traditions asiatiques et despotiques, tout à fait différentes de celles qui conduisirent au féodalisme et au capitalisme, avaient été déterminantes pour la construction tant de l'URSS que de la Chine communiste"
Cela vient du [[Despotisme oriental]] de [[Karl August Wittfogel]]

## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]