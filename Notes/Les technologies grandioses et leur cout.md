MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081614
Tags : #Fini
***

# Les technologies grandioses et leur cout
Il existe beaucoup de technologies qui nous ont coutés plus cher qu'elles nous ont rapportés. La fusée par exemple a été extrêmement couteuse. La bombe atomique était plus un concept qu'une réelle arme pratique. On détruisait très bien les villes avant : 50 à 250 bombes classiques auraient suffit pour détruire les villes japonaises. 6000 Mds d'$ ont été dépensé pour ce projet sans qu'en pratique on soit certains que ça ait permit la capitulation du Japon. p47.
Il faut bien comprendre que le bombardement de villes se faisait très bien avec les bombes mais que surtout, c'était bien moins efficace que la destruction d'infrastructure ou de complexes industriels. Cette idée que les bombardements de ces villes et donc que le développement de la bombe atomique était nécessaires est douteux au mieux.

Ce que ces technologies nous rapportent peut être est un avantage diplomatique ou symbolique. Mais on ne peut alors vraiment pas dire que ces technologies sont neutres.

## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]