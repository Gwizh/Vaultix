[[Note Permanente]]
MOC : [[Histoire]]
Titre : [[Semaine des barricades d’Alger]]
Date : 2022-07-11
***

## Résumé
Le nom de semaine des barricades désigne les journées insurrectionnelles qui se sont déroulées du **24 janvier au 1er février 1960 à Alger** durant la guerre d'Algérie (1954-1962). Son instigateur Pierre Lagaillarde (28 ans), député d'Alger (et ex-parachutiste), ainsi que Guy Forzy (35 ans), officier de renseignement au Deuxième Bureau, Joseph Ortiz (47 ans), patron du bar algérois le Forum, et Robert Martel (42 ans), agriculteur de la Mitidja, organisent une manifestation au cours de laquelle une partie des Français d'Algérie manifeste son mécontentement face à la mutation en métropole du général Massu ([[Affaire Massu]]), le 19 janvier 1960, sur décision du président [[Charles de Gaulle]]. Des barricades sont dressées rue Michelet et rue Charles Péguy.

Cette semaine qui marque une escalade des partisans de l'Algérie française fait plusieurs morts parmi la foule et parmi les forces de police.

Les meneurs sont arrêtés et jugés par un tribunal militaire en Métropole. Le procès dit « des Barricades » se tient à Paris au mois de novembre 1960. Les accusés Pierre Lagaillarde et Joseph Ortiz, mis en liberté provisoire pour la durée du procès, s'enfuient à Madrid où ils fondent l'OAS en décembre. 

**Le bilan fait état de 22 morts et 147 blessés** : huit morts parmi les manifestants, quatorze morts parmi les gendarmes, vingt-quatre blessés civils et cent vingt-trois blessés parmi les forces de l'ordre.

## Référence
[[Histoire de la France au XXe Siècle - Livre 3]]
[[Histoire de l'Algérie]]
[[Guerre d'Algérie]]

## Liens 