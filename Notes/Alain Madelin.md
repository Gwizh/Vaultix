MOC : 
Source : [[Alain Madelin]]
Date : 2023-04-11
***

## Résumé
Membre de la campagne de [[Jacques Chirac]] aux présidentielles de 1995. Prône le néolibéralisme. 

## Référence
[[Élection présidentielle française de 1995]]

## Liens 