MOC : 
Source : [[Abdullah Öcalan]]
Date : 2023-06-08
***

## Résumé
Abdullah Öcalan, également connu sous le surnom d'Apo (signifiant « oncle » en kurde), né le 4 avril 1949 dans le village de Ömerli (Amara en kurde, rattaché à la ville de Halfeti), est un homme politique [[Kurdistan|kurde]] de nationalité turque. Il est le leader du Parti des travailleurs du Kurdistan et l'initiateur du confédéralisme démocratique.

Öcalan fonde en 1978 le Parti des travailleurs du Kurdistan (PKK), sur la base d'un programme revendiquant un Kurdistan unifié, indépendant et socialiste. Son organisation lance une lutte armée prolongée en 1984, avant qu'il ne propose plusieurs trêves au cours de la décennie suivante. Au cours des années 1990, il abandonne ses revendications maximalistes, donnant désormais la priorité à une solution politique, pacifique et démocratique à la question kurde.

Öcalan est arrêté le 15 février 1999 et emprisonné en [[Turquie]], où il est condamné à la peine de mort pour terrorisme. La sentence n'est cependant pas mise à exécution, le pays abolissant la peine capitale en 2002. Sa peine est alors commuée en prison en vie ; il est détenu sur l'île d'İmralı. En 2008, ses avocats et Amnesty International dénoncent des tortures à son encontre par les gardiens.

## Référence

## Liens 
[[Syrie]]