MOC : 
Source : [[Société du Mont-Pellerin]]
Date : 2023-04-14
***

## Résumé
La Société du Mont-Pèlerin (en anglais Mont Pelerin Society) est un groupe de réflexion créé en 1947 et composée d'économistes, d'intellectuels ou de journalistes. Fondée par, entre autres, [[Friedrich von Hayek]], Karl Popper, [[Ludwig von Mises]], ou [[Milton Friedman]], la Société du Mont-Pèlerin revendique défendre des valeurs libérales, telles que l'économie de marché, la société ouverte et la liberté d'expression.

Appelé aussi MPS

But : créer un programme d'opposition au [[Keynésianisme]] et s'armer pour la bataille des idées pour avoir une position culturelle hégémonique.

Création de [[Think-tank]]

## Référence
[[Néolibéralisme]]

## Liens 