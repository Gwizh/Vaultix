MOC : 
Source : [[Bard]]
Date : 2023-05-12
***

## Résumé
C'est la réponse du célèbre moteur de recherche au succès fulgurant de ChatGPT. Google a lancé son chatbot d'intelligence artificielle générative, mercredi 10 mai, dans 180 pays. Baptisé Bard, cet assistant n'est pour l'instant accessible qu'en anglais (et il n'est pas disponible en France), a prévenu le géant américain lors de sa conférence annuelle (en anglais) à Mountain View en Californie. En plus d'une interface dédiée, [[Google]] a également précisé que Bard serait intégré dans plusieurs de ses services, dont la messagerie Gmail. Franceinfo vous résume ce que l'on sait de ce nouveau service.
https://www.francetvinfo.fr/internet/google/intelligence-artificielle-ce-qu-il-faut-savoir-sur-bard-la-reponse-de-google-a-chatgpt_5819303.html


## Référence

## Liens 