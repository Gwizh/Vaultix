MOC : 
Source : [[Printemps de Prague]]
Date : 2023-05-14
***

## Résumé
Le Printemps de Prague (en tchèque : Pražské jaro, en slovaque : Pražská jar, nommé ainsi en référence au Printemps des peuples) est une période de l’histoire de la République socialiste tchécoslovaque durant laquelle le Parti communiste tchécoslovaque introduit le « socialisme à visage humain » et prône une relative libéralisation. Il débute le 5 janvier 1968, avec l'arrivée au pouvoir du réformateur Alexander Dubček et s’achève le 21 août 1968 avec l’invasion du pays par les troupes du pacte de Varsovie.

Dubček introduit la liberté de la presse, d’expression et de circulation, dans la vie politique la démocratisation et enclenche une décentralisation de l’économie. Il dote le pays d'une nouvelle Constitution qui reconnaît l'égalité des nations tchèque et slovaque au sein d'une république désormais fédérale. Cette innovation politique sera la seule à survivre à l’intervention soviétique.

Le printemps de Prague provoque la réaction de l’[[URSS]] qui, après l’échec des négociations, envoie chars de combat et soldats pour imposer une « normalisation ». L’occupation soviétique entraîne des manifestations non violentes et une vague d’émigration parmi la population tchécoslovaque. Au printemps suivant Gustáv Husák remplace Alexander Dubček à la tête du parti et la plupart des réformes libérales sont abandonnées. Le printemps de Prague a inspiré la culture des années 1960-1980 avec les œuvres de Karel Kryl et Milan Kundera (L'Insoutenable Légèreté de l'être).

## Référence

## Liens 
[[Tchécoslovaquie]]