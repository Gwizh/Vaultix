MOC : 
Source : [[Triple assassinat de militantes kurdes à Paris]]
Date : 2023-06-09
***

## Résumé
Le triple assassinat de militantes kurdes à Paris est le meurtre de Fidan Doğan, Sakine Cansız et Leyla Söylemez, membres du [[Parti des travailleurs du Kurdistan]], dans la nuit du 9 au 10 janvier 2013 dans le 10e arrondissement de Paris.

L’auteur présumé est un nationaliste turc, Omer Güney, infiltré dans les milieux kurdes. Il meurt fin 2016 quelques semaines avant l’ouverture de son procès. Les services secrets turcs, le MIT, sont considérés comme les probables commanditaires par la justice française.

## Référence

## Liens 