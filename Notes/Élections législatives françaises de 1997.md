MOC : 
Source : [[Élections législatives françaises de 1997]]
Date : 2023-04-06
***

## Résumé
Les élections législatives françaises de 1997 ont lieu le 25 mai et le 1er juin 1997 en raison de la dissolution de l'Assemblée nationale décidée par le président de la République, [[Jacques Chirac]].

Alors que la droite (RPR-UDF) était initialement donnée en tête des intentions de vote, les élections aboutissent à la victoire de la coalition de [[Gauche plurielle]] ([[Parti Socialiste]], PCF, LV, MDC, PRS) et à une nouvelle période de cohabitation en France. Le premier secrétaire du PS, [[Lionel Jospin]], est alors nommé à la tête d'un gouvernement de coalition (gouvernement Lionel Jospin), inaugurant ainsi la troisième cohabitation (1997-2002).




Il n'y a qu'une majorité relative donc c'est pas non plus la fiesta.

Sur la dissolution :
Selon le chiraquien Jean-Louis Debré, la dissolution avait pour but de « renforcer l'autorité politique de [[Alain Juppé]], modérer les ardeurs contestataires de [[Philippe Séguin]], contenir les ambitions clairement exprimées de [[Nicolas Sarkozy]] et anéantir les ultimes prétentions de [[Edouard Balladur]] ».

## Référence

## Liens 