MOC : 
Source : [[Émile Pouget]]
Date : 2023-07-11
***

## Résumé
Émile Pouget, né à Pont-de-Salars (Aveyron) le 12 octobre 1860 et mort à Lozère (Palaiseau, Seine-et-Oise) le 21 juillet 1931, est un militant anarchiste, antimilitariste et syndicaliste révolutionnaire français1.

Fondateur de journaux libertaires comme Le Père peinard, La Sociale et La Révolution, il est secrétaire adjoint de la section des fédérations de la [[CGT]] de 1901 à 19082. En 1906, il participe à la rédaction de la charte d'Amiens, l'une des références théoriques du syndicalisme en France. 

## Référence

## Liens 
[[Anarchie]]
[[Syndicalisme]]
