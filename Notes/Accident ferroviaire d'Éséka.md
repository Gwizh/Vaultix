MOC : 
Source : [[Accident ferroviaire d'Éséka]]
Date : 2023-05-12
***

## Résumé
L’accident ferroviaire d’Éséka est un déraillement survenu le 21 octobre 2016 à 13 h 30, près de la gare d'Éséka sur la ligne de Douala à Yaoundé à Éséka au [[Cameroun]]. Le train avait été doublé (seize voitures au lieu de neuf habituellement). L'accident a fait 79 morts et 551 blessés.

## Référence

## Liens 