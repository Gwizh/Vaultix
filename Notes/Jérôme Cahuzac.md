MOC : 
Source : [[Jérôme Cahuzac]]
Date : 2023-04-15
***

## Résumé
En 2018, il est condamné en appel pour [[Fraude fiscale]] et blanchiment de fraude fiscale à deux ans de prison ferme (peine aménagée en port d'un bracelet électronique) et deux ans avec sursis, cinq ans d'inéligibilité et 300 000 euros d'amende.

## Référence

## Liens 