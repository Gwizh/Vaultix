MOC : 
Source : [[En Bretagne, une banque vole aux habitants « le droit de vivre au pays »]]
Date : 2023-05-05
***

https://reporterre.net/En-Bretagne-une-banque-vole-aux-habitants-le-droit-de-vivre-au-pays

## Résumé
Alors qu’une grave crise du [[Logement]] touche la [[Bretagne]], la [[Caisse d’épargne]] promeut les maisons secondaires. « Une gifle pour tous ceux qui ne peuvent pas acquérir ou louer une maison », dénonce un élu local dans cette tribune.

_À M. Christophe Pinault, président du directoire de la Caisse d’Épargne Bretagne — Pays de la Loire._

Bonjour,

J’ai découvert il y a peu, sur un réseau social connu, que la Caisse d’épargne de Bretagne — Pays de la Loire faisait la promotion de l’achat de résidences secondaires. Un site internet est même exclusivement dédié au financement de ce type d’achats : madeuxiememaison.fr. Sur ce site, on apprend que la Caisse d’épargne propose des modalités de remboursement spéciales pour les résidences secondaires, et donne des conseils d’optimisation du bien, y compris en location saisonnière.

Vous n’êtes pas sans savoir qu’une grave crise du logement touche certains secteurs de la Bretagne. Du pays de Retz à Saint-Malo, l’excès de résidences secondaires et de locations saisonnières remet en cause le droit au logement pour les habitants à l’année. La plupart d’entre elles et eux ne peuvent plus acheter et les locations sont rares... La Bretagne n’est d’ailleurs pas la seule concernée, nos voisins Vendéens et Normands en savent quelque chose.

Les conséquences sont connues elles aussi : vieillissement accéléré de ces secteurs, difficultés de recrutement pour les entreprises et les services publics, obligation de s’installer loin de son travail — avec les pollutions et dépenses associés aux longs trajets quotidien — sans oublier bien sûr la consommation effrénée de terres agricoles ou naturelles. En tant qu’élu et habitant du Trégor, il ne se passe pas une semaine sans qu’on ne m’entretienne de cas de personnes, souvent actives, souvent jeunes, qui se retrouvent dans l’impossibilité de se loger. Et les difficultés s’étendent dans les terres, de plus en plus loin des zones touristiques historiques. C’est le droit à vivre au pays qui nous est retiré.

## Référence

## Liens 
[[Banque]]