MOC : 
Source : [[Prix de l’électricité - l’incompréhensible gestion du parc nucléaire par EDF]]
Date : 2023-04-20
***

## Résumé

Prix de l’électricité : l’incompréhensible gestion du parc nucléaire par EDF
https://www.mediapart.fr/journal/economie-et-social/190423/prix-de-l-electricite-l-incomprehensible-gestion-du-parc-nucleaire-par-edf?utm_source=global&utm_medium=social&utm_campaign=SharingApp&xtor=CS3-5

L’association de consommateurs CLCV a étudié sur une période de dix ans le fonctionnement du marché de l’électricité en France. Son constat est troublant : la production nucléaire baisse de façon continue au profit de celle des centrales à gaz. La gestion du nucléaire se fait-elle vraiment dans l’intérêt des clients ?

Le premier constat relevé par CLCV, qui va à l’encontre de bien des propos répétés par le gouvernement, porte sur « la dégradation structurelle de la production nucléaire ». Et cela bien avant que les corrosions sous contrainte détectées dans certains réacteurs fin décembre 2021, et qui ont entraîné un gel d’une partie de la production nucléaire tout au long de 2022, fassent prendre conscience de la fragilité de l’outil de production d’[[EDF]].

Avec plus de cinquante-six réacteurs, le parc nucléaire français est en mesure, s’il fonctionne au maximum de sa capacité, de fournir les trois quarts de la consommation d’électricité en France. Or entre 2012 et 2021, la production nucléaire, relève l’étude, n’a cessé de diminuer. Alors qu’elle représentait 73 % de la production électrique en 2012, elle ne s’élève plus qu’à 67 % au premier semestre 2021. En dix ans, la production nucléaire a chuté de 11 %, note-t-elle

L’utilisation des centrales à gaz se maintient même les jours de consommation la plus basse, quand les centrales nucléaires, l’hydraulique et les énergies renouvelables suffisent largement pour couvrir les besoins. L’étude a ainsi repris la production électrique par heure des 544 dimanches – jour de plus faible consommation – entre janvier 2012 et mai 2022 pour suivre le fonctionnement du marché. _« Dans 52 % des cas, les centrales au gaz ont été mises en fonctionnement alors que cela n’apparaissait pas nécessaire, sachant que les autres énergies permettaient déjà de satisfaire la demande »_, constate l’étude , qui se demande pourquoi les centrales à gaz ont été mises en marche dans de telles conditions.

La présence constante du gaz dans la production électrique française est tout sauf neutre. Cette énergie fossile coûte beaucoup plus cher que le nucléaire et les énergies renouvelables. Surtout, compte tenu de l’organisation du marché de gros européen de l’électricité, qui fonctionne au coût marginal, c’est le gaz qui détermine le prix spot sur le marché (les prix établis sur le marché de l'électricité par les bourses le jour J pour le lendemain).

C’est un des derniers enseignements de cette étude. Alors que le marché de l’électricité est peut-être [un des plus manipulables](https://www.bloomberg.com/graphics/2023-uk-power-electricity-market-manipulating/?sref=fo6OHuy7#xj4y7vzkg), la [[CRE]] (commission de régulation de l’énergie), se conformant aux habitudes de pratiquement toutes les autorités indépendantes liées à l’économie et à la finance en France, est quasiment inexistante. Depuis sa création, les seuls critères pour analyser le fonctionnement du marché électrique en France ont été l’augmentation de la redistribution de la production nucléaire historique aux fournisseurs alternatifs concurrents d’EDF et la surveillance du nombre de clients que perdait l’ancien monopole public.

## Référence

## Liens 
[[Nucléaire]]
[[Énergie]]
[[Noeuds/Gaz]]