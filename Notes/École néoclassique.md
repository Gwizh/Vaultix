MOC : 
Source : [[École néoclassique]]
Date : 2023-06-28
***

## Résumé
L'école néoclassique est une école de pensée économique dont la thèse centrale est que les marchés disposent de mécanismes autorégulateurs qui, en l'absence d'intervention extérieure, conduisent à l'optimum économique ; l'État n'a ainsi qu'un rôle très mineur à jouer dans le domaine économique. Fondée par les économistes marginalistes [[Léon Walras]], [[William Stanley Jevons]] et [[Carl Menger]] à la fin du XIXe siècle, elle a dominé la science économique jusqu'à l'avènement du [[Keynésianisme]] amendé. L'école s'est ensuite en partie fondue dans la synthèse néoclassique, qui a synthétisé des pans de la théorie keynésienne avec des modèles néoclassiques. 

### Fondements théoriques
##### Naturalisme
Les néoclassiques considèrent que les phénomènes économiques peuvent et doivent être étudiés à l’aide des mêmes méthodes que les phénomènes physiques. La science économique doit s'inspirer des sciences de la nature, comme la science physique, pour atteindre un degré d'exactitude similaire. 

##### Rationalité des acteurs
Les agents sont considérés comme rationnels, calculateurs et maximisateurs. Leurs préférences peuvent être clairement identifiées et quantifiées. Ils cherchent à maximiser l'utilité ou la satisfaction des biens consommés, tandis que les entreprises cherchent à maximiser leur profit, et uniquement cela.

Selon la théorie du consommateur, l’individu adopte une attitude rationnelle visant à maximiser son utilité. À chaque dépense, il compare l’utilité marginale des biens afin de hiérarchiser ses préférences et s’oriente vers le plus utile. Cette étude de l’individu, comme producteur ou consommateur rationnel et autonome, rejoint le principe de l’individualisme méthodologique. 

##### Société d'individus
Parce que les néoclassiques adoptent la perspective de l'[[Individualisme]] méthodologique, ils pensent la société comme une agrégation d'individus et non comme une société de classes. Ils se distinguent ainsi des classiques, qui adoptaient une perspective basée sur une tripartition sociale entre les détenteurs du facteur travail, du facteur capital, et des ressources terriennes. ==Ainsi, les comportements individuels ne relèvent jamais d'une logique de classe. ==



## Référence

## Liens 
[[École autrichienne (économie)]]