MOC : 
Source : [[Wuambushu]]
Date : 2023-04-25
***

## Résumé
[[Mayotte]]

L’opération « Wuambushu » menée sur le cent unième département français est une monstruosité politique qui prolonge un crime juridique. Maintenant sa souveraineté sur Mayotte en violation flagrante du droit international, la [[France]] y met en scène l’expulsion massive d’êtres humains au prétexte qu’ils seraient étrangers alors même qu’ils font partie du même peuple que les autochtones.

Une unité de maintien de l’ordre supposée d’élite, la CRS 8, qui, au premier jour de son intervention, revendique non seulement l’usage de 650 grenades lacrymogènes, 85 grenades de désencerclement et 60 tirs de LBD, mais assume aussi avoir ouvert le feu à douze reprises en tirant vers le sol pour repousser la population civile qui lui résiste.

C’est peu dire que, contrairement aux fanfaronnades du ministre de l’intérieur, l’opération « Wuambushu » qu’il a mise en œuvre (et en scène) à 8 000 kilomètres de Paris au nom de la lutte contre « l’immigration illégale » est à mille lieues de « la restauration de la paix républicaine » revendiquée encore par [[Gérald Darmanin]] mardi 25 avril, en soutien de l’appel du préfet de Mayotte contre la décision judiciaire.

Devenue département français depuis un référendum en 2009, Mayotte est le fruit d’un rapt. Violant la règle internationale de respect des frontières, la France l’a arrachée à l’archipel dont elle faisait partie, les Comores, lors de la décolonisation de ce territoire en 1975. Cette annexion est illégale au regard du droit international, qu’il s’agisse des résolutions de l’ONU ou de celles de l’Union africaine. De ce même droit que l’on invoque, à juste titre, pour combattre les annexions russes qui ont précédé la guerre d’invasion contre l’Ukraine. La France qui vote à l’ONU les résolutions qui condamnent la Russie en viole donc allègrement les principes.

C’est une guerre aux pauvres qu’a donc lancée Gérald Darmanin, et pas seulement aux migrants. Car les populations visées par cette opération spectaculaire sont les mêmes que celles qu’elle prétend protéger. À Mayotte, les Comoriens et Comoriennes que la France prétend expulser de l’île, en détruisant d’abord leurs habitations ([_lire le reportage de Nejma Brahim_](https://www.mediapart.fr/journal/france/240423/mayotte-des-habitants-terrorises-l-idee-d-etre-expulses-demolissent-eux-memes-leur-maison)), puis en les parquant dans des camps, ne sont pas des étrangers. C’est le même peuple, la même culture, la même langue, la même religion. Le gouvernement, rappelle l’ethnologue [Sophie Blanchy](https://www.lemonde.fr/afrique/article/2023/04/25/a-mayotte-les-comoriens-ne-sont-pas-des-etrangers_6170923_3212.html), _« a face à lui une seule et même population »._ La seule distinction, c’est que certains ont la nationalité française et d’autres non.

Dès lors, l’on devine combien ce qui se joue là-bas nous concerne ici. Cette grande rafle de Mayotte fait la promotion de la pire idéologie d’extrême droite, le « [[Grand remplacement]] ». Elle montre que l’on peut faire le tri au sein d’un même peuple, en ayant installé l’idée monstrueuse d’une occupation étrangère qui légitimerait l’expulsion des indésirables. À la face du monde, la France des droits de l’homme abdique ainsi sur l’égalité des droits, donnant le feu vert à tous les régimes autoritaires – et ils ne manquent pas, en Afrique même, comme l’a démontré récemment l’autocrate président tunisien – qui feront la chasse aux humanités en mouvement pour ne pas avoir de comptes à rendre à leurs peuples.

## Référence

## Liens 