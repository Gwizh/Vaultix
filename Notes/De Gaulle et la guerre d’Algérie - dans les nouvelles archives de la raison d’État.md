[[Article]]
MOC : [[POLITIQUE]] [[Histoire]]
Titre : [[De Gaulle et la guerre d’Algérie - dans les nouvelles archives de la raison d’État]]
Auteur : [[Mediapart]]
Date : 2022-07-05
Lien : https://www.mediapart.fr/studio/panoramique/de-gaulle-et-la-guerre-d-algerie-dans-les-nouvelles-archives-de-la-raison-d-etat
Fichier : ![[De Gaulle et la guerre d’Algérie _ dans les nouvelles archives de la raison d’État _ Panoramiques _ Mediapart.pdf]]
***

## Introduction
Il n’y a pratiquement pas une famille française qui n’ait un lien, direct ou indirect, avec la guerre d’Algérie. On pourrait penser que c’est une histoire ancienne, sans présent ni futur. Pour se convaincre du contraire, il suffit de réécouter le discours inaugural de la nouvelle législature de l’Assemblée nationale [prononcé](https://www.lemonde.fr/politique/article/2022/06/29/jose-gonzalez-le-doyen-de-l-assemblee-nationale-qui-a-ouvert-la-legislature-en-invoquant-l-algerie-francaise_6132494_823448.html), mardi 28 juin, par le doyen de la Chambre, le député d’extrême droite José Gonzalez ([[Rassemblement National]] – RN), qui a dit à la tribune sa nostalgie de la France coloniale puis, dans la foulée, s’est montré incapable devant les journalistes qui l’interrogeaient d’en reconnaître les crimes en Algérie.

## 1 - Le « secret » de l’affaire Audin
Nous sommes en juin 1957, au début de la [« bataille d’Alger »*](https://www.mediapart.fr/studio/panoramique/de-gaulle-et-la-guerre-d-algerie-dans-les-nouvelles-archives-de-la-raison-d-etat#annotation-03) La « bataille d’Alger » a débuté le 7 janvier 1957, lorsque le général Massu, chef de la 10e DP (division parachutiste), a reçu les pouvoirs de police pour détruire la Zone autonome d’Alger du FLN., cinq mois après le transfert à l’armée des pouvoir civils, notamment de police. La consigne donnée par le chef militaire de l’Algérie, le général Salan, est claire : il ne faut pas hésiter à faire usage d’_« interrogatoires poussés à fond et immédiatement exploitables »_ afin d’obtenir un maximum de renseignements contre les partisans de l’indépendance.

En d’autres termes : torturer.

Dans _La Question_, son auteur, Henri Alleg, directeur du quotidien _Alger républicain_ et membre du Parti communiste algérien (dissous en 1955), raconte les sévices administrés par les hommes de la 10e division parachutiste. Les militaires suspectent alors l’existence d’un réseau clandestin d’entraide aux militants du Front de libération nationale (FLN), auquel est accusé d’être également associé un jeune professeur de mathématiques de l’université d’Alger, Maurice Audin, 25 ans.

Arrêté la veille d’Henri Alleg, Maurice Audin ne pourra, lui, témoigner de rien : il sera tué dans des circonstances qui demeurent, 65 ans plus tard, toujours inconnues dans le détail. Pendant des années, l’armée a défendu la thèse d’une évasion pour expliquer la disparition d’Audin.

Dans une note du 8 juin 1960, on peut par exemple lire : _« Af. Audin : l’intime conviction du JI_ [juge d’instruction – ndlr] _= assassinat. Mais pas de preuve »_. Une autre note du 2 juillet laisse apparaître cette fois une forte inquiétude au sommet de l’État s’agissant de _« la prochaine déposition »_ de l’ancien procureur général d’Alger, Jean Reliquet, qui, note le conseiller de De Gaulle, _« va déclarer que le Gal_ [général] _Allard_ [qui a commandé le corps d’armée d’Alger] _lui avait confié qu’Audin avait été assassiné et que cet officier sait tout sur l’af._[affaire] _»_. Le conseiller de De Gaulle ajoute que _« le Gal Gardon_ [chef du service commun des justices des armées] _connaît le secret de l’af. Audin »_, sans dire quel est ce _« secret »_.

Dans cette même note, Jean-Jacques Bresson rappelle que, selon le magistrat Jean Reliquet, _«_ _l’état d’esprit qui avait conduit les auteurs du meurtre à le commettre était essentiellement imputable à MM. Lacoste_ [ministre  de l’Algérie entre 1956 et 1958] _et Bourgès-Maunoury_ [ministre de la défense puis de l’intérieur entre 1956 et 1958]_, qui avaient constamment toléré, sinon encouragé, l’exercice de la “justice parallèle” »_. Les ministres cités dans la note Bresson sont des hommes de gauche, socialiste pour le premier et radical-socialiste pour le second, tous deux membres du gouvernement de Guy Mollet (février 1956-mai 1957), sous la IVe République.

Deux ans plus tard, en avril 1962, l’instruction de Rennes débouchera sur un non-lieu et l’affaire Audin restera, à jamais, un crime impuni par la justice. Plus d’un demi-siècle plus tard, en 2018, Emmanuel Macron [reconnaîtra](https://www.mediapart.fr/journal/france/130918/macron-reconnait-la-responsabilite-de-l-etat-francais-dans-l-assassinat-de-maurice-audin) _« au nom de la République »_ le supplice de Maurice Audin sous la férule de l’armée française et de ses pouvoirs spéciaux en Algérie.

## 2 - La République sur écoutes
Il y a au Service historique de la Défense (SHD), situé dans le fort de Vincennes, des archives d’un genre un peu particulier. Ce ne sont pas des documents écrits, mais des paroles. Plus précisément, il s’agit des enregistrements de témoignages oraux de militaires de carrière et de hauts fonctionnaires à la retraite, qui ont accepté de raconter les moments forts de leur vie professionnelle à des historiens. Ils sont facilement disponibles à la consultation ; il suffit pour cela de s’installer derrière un ordinateur du SHD, de chercher et d’écouter au casque.

J’allais découvrir dans le fonds d’archives récemment déclassifié de la présidence de Gaulle que la surveillance de certains fonctionnaires pendant la guerre d’Algérie était, sinon monnaie courante, du moins une pratique habituelle. J’en veux pour preuve une _« fiche de transmission »_ siglée _« urgent »_, laissée le 30 mai 1961 à l’attention du secrétaire général de l’Élysée, Geoffroy de Courcel, par Jacques Foccart, le « Monsieur Afrique » du général de Gaulle.

## 3 - Le détenu illégal
La conversation téléphonique de mai 1961 entre l’avocat Pierre Stibbe et le conseiller ministériel Hervé Bourges, interceptée par les barbouzeries de Jacques Foccart, portait sur le sort d’un Algérien qui venait de passer cinq ans en prison pour rien : Mostefa Lacheraf. Le long calvaire judiciaire qu’il a subi montre aujourd’hui, grâce aux archives récemment déclassifiées, l’absolu cynisme d’État dont il fut la victime.

L’histoire commence le 22 octobre 1956. Ce jour-là, les services secrets français jouent les pirates de l’air et mènent l’une des opérations les plus folles de leur histoire. Le roi du Maroc Mohammed V doit quitter Rabat pour la Tunisie, où il est attendu pour une visite officielle. Le SDECE (l’ancêtre de la DGSE, la Direction générale de la sécurité extérieure) apprend que plusieurs dirigeants du FLN, parmi lesquels le futur président algérien Ahmed Ben Bella, ne voyageront pas, contrairement à ce qui était initialement prévu, dans l’appareil officiel du sultan, mais dans un avion civil à sa suite.

L’occasion est trop belle. _« On avait la possibilité de cravater Ben Bella ! »_, se souvient le directeur de cabinet du préfet d’Alger, Pierre Bolotte, selon son témoignage oral au Service historique de la Défense. Profitant d’une escale de l’appareil aux Baléares, les services spéciaux français infiltrent l’équipage et les pilotes dans le but de détourner l’avion de sa destination finale : direction Alger, au lieu de Tunis.

L’opération est, du point de vue français, un succès total : Ben Bella et ses compagnons du FLN, accusés de comploter contre l’État français, sont arrêtés et la valise avec laquelle voyageait le leader indépendantiste est récupérée _« toutes affaires cessantes »_ par les services secrets.

Seulement voilà, parmi les six personnes appréhendées par les Français se trouve un homme, le journaliste et écrivain Mostefa Lacheraf, qui, s’il est clairement un militant de la cause indépendantiste, n’est pas de l’équipe Ben Bella. Il n’en sera pas moins arrêté et emprisonné lui aussi. Son calvaire durera cinq ans. En dépit de mouvements publics de protestation pour demander sa libération et d’une grande gêne au sein même du gouvernement, Mostefa Lacheraf ne sera libéré qu’en mai 1961.

_Aucune charge précise n’a été relevée jusqu’ici à l’encontre de l’intéressé. L’aspect judiciaire de l’affaire s’efface donc entièrement devant son côté politique._

...

Il mènera par la suite une carrière d’ambassadeur et sera même ministre de l’éducation nationale de l’Algérie. Il est mort en 2007.

## 4 - La « justice parallèle »
Le 26 mai 1960, un homme de 29 ans, Mohamed Kader, grutier de profession qui dit s’être toujours tenu à bonne distance des activités politiques, se présente au tribunal d’Alger. Il vient déposer plainte. Sa femme, Saadia Mebarek, qui était enceinte, est morte la nuit précédente. Elle avait été arrêtée le 24 mai un peu avant minuit et livrée sans vie au petit matin suivant, sur une civière, par un militaire français, à l’hôpital Mustapha-Pacha.

Pour le pouvoir politique français, l’affaire est potentiellement explosive. Elle intervient deux ans après le retour au pouvoir du général de Gaulle, qui avait promis de mettre fin aux dérives de l’armée. L’inquiétude est d’autant plus grande que dès le mois de février 1960, un membre du ministère de la justice avait écrit une note au ministre Edmond Michelet pour lui faire savoir que, selon les confidences d’un magistrat militaire _« d’un très haut grade »_, _« une justice parallèle se traduisant par des tortures et des exécutions sommaires »_ continuait de s’exercer sans vergogne en Algérie.

L’histoire racontée par l’armée est celle-ci : dans la nuit du 24 au 25 mai, cinq femmes _« suspectées de propagande abstentionniste pour les élections cantonales dans le cadre des ordres diffusés par le FLN »_ sont arrêtées par l’armée. Des _« renseignements d’informateurs recoupés »_ ont convaincu les militaires que certaines des suspectes ont fait pression sur des habitants pour qu’ils n’aillent pas voter, jusqu’à proférer des menaces de mort.

Si des interrogatoires ont bien été menés et si la force a dû être employée face aux résistances des personnes arrêtées, _« en aucun cas, elles ne furent l’objet de sévices »_, écrit l’auteur du rapport. Saadia Mebarek y est présentée, elle, comme une femme _« atterrée »_, ayant _« perdu tout contrôle »_. Le rapport transmis par Messmer affirme qu’une demi-heure seulement après sa libération survenue à 3 h 30 du matin, une patrouille militaire la découvre inanimée à 100 mètres de chez elle, complètement par hasard. Encore en vie, d’après le rapport, elle est transportée à l’hôpital, où son décès est constaté. Fin de l’histoire. Du moins de l’histoire officielle.

_« À titre confidentiel_, écrit le magistrat, _le Docteur m’a laissé entendre qu’il était très vraisemblable que l’asphyxie était le résultat d’une électrocution_ […]_. Selon toute vraisemblance, la dame Mebarek est morte des suites de sévices qu’on a exercés sur elle. »_ L’autopsie révèle notamment des lésions sur les seins, au bras droit, à la cheville gauche et sur les parties génitales de la suppliciée, dont les cris pendant son interrogatoire ont été entendus par des témoins.

Il conclut son rapport avec ces mots : _« Il y va de l’autorité du gouvernement et du prestige du chef de l’État. Il a été dit et répété aux musulmans qu’il ne serait plus exercé de sévices sur les suspects. Il semblait que les ordres donnés à ce sujet par les autorités administratives et militaires aient été, dans les derniers temps, mieux respectés que par le passé, mais voilà que les mêmes faits se reproduisent. Si dans le passé des sanctions ont été prises par des autorités militaires, aucune d’elles n’a été connue de la population, qui est entretenue dans le sentiment qu’il existe deux poids et deux mesures et que les plus hautes autorités de l’État n’arrivent pas à imposer leur volonté aux organes d’exécution. »_

Cinq jours plus tard, Maurice Patin, magistrat de profession et membre du Conseil constitutionnel, lui répond d’une plume désabusée : _« Il m’apparaît d’ores et déjà que ces informations, comme mes interventions, seront vaines, si les hautes autorités militaires doivent manifester autant d’hésitation que par le passé à assurer, à cet égard comme à tous autres, la discipline de l’armée et à prononcer elles-mêmes les sanctions impitoyables contre les auteurs de pareils excès, dont la persistance est décourageante. »_

Le 18 janvier 1962, au terme d’un procès à huis clos, le tribunal militaire de Paris décidera d’acquitter totalement les trois militaires poursuivis pour avoir provoqué la mort de Saadia Mebarek après l’avoir torturée.

## Référence
[[Histoire de la France au XXe Siècle - Livre 3]]
[[Charles de Gaulle]]
[[Guerre d'Algérie]]

## Liens 

