MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081430
Tags : #Fini
***

# La complexité accélère la perte du savoir
Plus le système est complexe et moins les individus le comprennent et connaissent les réels savoirs nécessaires à son fonctionnement. Les savoirs globaux et cruciaux sont noyés dans la connaissances des dépendances et de tous les correctifs qui font que cela fonctionne dans l'état actuelle.

Un problème dans un système très complexe peut provenir de tout un tas de dépendances qui devraient pouvoir être négligeable.

## Référence
[[@Jonathan Blow - Preventing the Collapse of Civilization (English only),]]