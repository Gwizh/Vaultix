MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081614
Tags : #Fini
***

# Compétition entre les nations sur les innovations
p147
C'est très souvent une compétition pour savoir qui innove le +. Modification de l'histoire pour se rappeler d'exploits nationaux. Ex ?
Les musées des techniques sont en concurrence par exemple. Qui a vraiment inventé le cinéma, l'avion, la voiture ? On aime placer un personnage national comme figure centrale.

Concept de [[Techno-nationalisme]]
On pense qu'il nous faut innover plus que les autres pays pour ne pas perdre contre eux. Pourtant, des exemple montrent que ce n'est pas si simple.

## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]