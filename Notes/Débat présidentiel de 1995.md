MOC : 
Source : [[Débat présidentiel de 1995]]
Date : 2023-04-10
***

## Résumé

#### Questions sur les institutions
Chirac pense que les institutions sont très bien mais que le président ne joue plus le rôle originel, il constate une dérive monarchique. Il critique une sorte de Super premier ministre. Lui propose de rééquilibre les institutions comme réduire le nombre de dirigeants d'entreprises publiques nommées par le président. Il parle d'une sorte de sobriété du gouvernement. Il critique le dévelppement des cabinets minitériels. Il parle du grand nombre de lois qu'il faut améliorer et moderniser. 

Jospin propose la réduction à 5 ans du mandat présidentielle et dit que ça s'aplliquera à lui. Il propose un contrôle plus strict du cumul des mandats. Il veut une coupure entre la justice et le Ministère de la Justice.  

Chirac était à l'époque contre le quinquenat évoquant les trop nombreux problèmes de l'époque. Il veut de la transparence, aller au dela des rapports et des experts. Il veut aller au contact avec les citoyens. Il s'en sert pour justifier le cumul des mandats qui permet alors d'avoir un poste local pour obliger les élus à aller voir les citoyens. Il parle des changments dans les hayuts fonctionnaires en 1981 et 1988, où ils ont été changé pour avoir plus de socialistes à des postes haut placés. L'administration a été modifiée pour être socialiste. Chirac veut une administration impartiale.

Jospin revient sur le cumul du mandat. Il est d'accord pour l'impartialité de l'Etat. Il va plus loin cependant en évoquant le pantouflage, qu'il veutr controler pour éviter que des fonctionnaires puissent passer du public au privé dans des secteurs où ils ont grandement contribué. 

Chirac est d'accord sur le pantouflage et dit qu'il est contre le fait que des dirigents de banques françaises ne sortaient pas directement de la direction du Trésor. 

Jospin pense que s'il est élu, il aurait une majorité à l'Assemblée. Chirac pense au contraire qu'il y aurait une cohabitation. Jospin a eu raison ici.

#### Questions sociales et économiques
Chirac veut donner de l'oxygène aux entrepreneurs pour qu'ils puissent créer de l'emploi. Pour ça, il faut leur donner la possibilité d'accéder au crédit, et diminuer la paperasserie gouvernementale. Il faut aussi réduire le cout du travail. Pour lui, il faut réduire les charges sur les entreprises pour améliorer les condiutions des chomeurs de longues durées et des jeunes. "Le travail est trop rare aujourd'hui pour être sur-taxé." Il veut revoir l'Education (permet de critiquer Jospin qui était ministre de l'Education). 

Jospin répond sur le choùage que premièrement ce n'est pas que de la faute des socialistes mais aussi que globalement ils ont fait progresser sur tous les points sauf celui-là qui par ailleurs et un problème pour de nombreux pays d'Europe. Très fort. Vu la conjecture actuelle (forte [[Croissance économique]] mondiale) il dit que la priorité est le chomage en l'attaquant de plusieurs côtés en même temps. Lui veut augmenter les salaires pour augmenter la consommation intérieure. Il veut aussi passer aux 37 heures mais je n'ai pas compris le lien direct. Il veut créer des emplois dans le domaine du logment, de l'environnement, del'humanitaire, des emplois locaux. Le livre blanc de Jacques Delors. Il dit justement que Chirac n'a pas donné d'arguments aussi précis. 

Selon Chirac, le gouvernement socialiste aurait créer une économie de spéculation en mettant trop de charges sur les entrerprises qui donc préfèreraient l'épargne à l'investissement. Il dit que tous les experts sont d'accord, ok. Il critique le crédit qui serait trop difficilement donné aux petits entrepreneurs.

Jospin dit que les 39 heures ont permis de créer 150 000 emplois. 

Chirac dit que les 37 heures ne vont pas créer des emplois ou alors seulement dans la fonction publique, fonctionnaires qu'il faudra ensuite financer.  Il dit même que ça n'avait pas été chiffré dans le programme de Jospin. Chirac parle toujours des conséquences supposées du programme de Jospin. Il ne propose pas grand chose lui-même. Je suis biaisé ? Peut-être. 

Les PME/PMI créent la moitié des emplois français. Jospin est d'accord pour dire qu'il faut aménager un crédit spécifique pour ces entreprises et leur permettre des droits de succession plus souples. Jospin suit la ligne socialiste du siècle en rappelant que le temps de travail a grandement diminué sans que les salaires baissent mais surtout avec un grand gain de productivité. Il faut diminuer le temps de travail pour créer de l'emploi si ce n'est par principe. En Allemagne, les 35 heures ont déjà été accepté en mettalurgie. 

Chirac est contre les éco-taxes par exemple mais plus généralement tout ce qui touche au cout du travail. Il est pourtant pour l'augmentation des salaires. Pour ça il veut diminuer les salaires indirects, ça fait sens. Chirac parle de l'endettement du pays et veut poursuivre les privatisations. Il critique le ni-ni socialiste qui priverait de 50 Mds. Il parle du trou de la sécurité sociale. Il prone potentiellement un nouvel impot et préfère TVA à CSG. 

Jospin est contre les privatisations de Chirac et Balladur. Il veut préserver les services publics français qu'il veut protéger des offensives européennes. Il met en garde contre l'entrée des assurances privées dans le système de la sécurité sociale. Il est aussi contre la TVA qui s'apparente surtout à une flat taxe. On fait mine de donner mais pour reprendre ensuite de manière injuste. 

Chirac est pour la [[Retraite par répartition]] 

#### Questions sur les banlieues, les logements sociaux et l'exclusion sociale
Jospin parle de 2 France qui s'éloignent : "une faille". Il parle des artisans, des commerçants et autres classes à revenus moyens ou modestes. Il axe ses propositions sur le chomage, l'augmentation du salaire, l'éducation et le logement. Il faut une priorité pour le logement social. Plus en construire, mieux les attribuer et les rénover.  

Chirac : "Oui, il y a deux France. Une qu'on est obligé d'assiter. Et une qui est de plus en plus taxée pour permette d'aider les premiers. Un système diabolique et je m'excuse de vous le dire mais c'est un système socialiste." Jospin le reprend. Il veut créer un prêt pour la propriété plus facile d'acès avec un taux 0 et des conditions en cas d'événements comme un divorce ou le chomage. Cela incitera les gens à prendre le prêt et à améliorer leur logements tout en laissant de la place aux plus démunis. Pour les SDF, il faut les accompagner pour les réinsérer dans la vie sociale. Il faut donc l'aide des grandes associations sociales notamment pour leur faire comprendre qu'il faut payer un loyer, même symbolique. Un chomeur coute. 

#### Immigration
On l'attendait.. pas. 
Chirac dit qu'il y a eu la régularisation de 1981 par [[François Mitterand]]. Et Jospin dit qu'il n'y en a plus eu ensuite, comme si c'était une bonne chose. MMMmmmm. Il parle de contrôle aux frontières. 

"Et le code de la nationalité par ailleurs je pense que ça date de 1515, Marignan, les rois et bah le pacte du sol c'est le pacte républicain et le droit du sang c'est pas la République."

Chirac parle de la [[Loi du 22 juillet 1993 réformant le droit de la nationalité]]. Il est totalement pour l'idée qu'on ait à demander pour avoir la nationalité, qu'on ne l'ait pas "par hasard".

#### Sida
Jospin parle de la guerre au [[Sida]]. Une grande volonté des services publiques de limitations des risques et s'accompagnement (avec les associations) des personnes séropositives. Il parle de la prévention envers les populations à risque, "les toxicomanes, les homosexuels et les hommes dans les prisons". Chirac ne dit rien de plus.

#### Agriculture
"L'agriculture est une carte maîtresse de la France" dit Chirac. Il veut que la France continue sa politique d'exportation. Il veut une agriculture diversifiée et donc pas seulement des produits compétitifs. Il parle d'aides aux agricultueurs dans ce cas là et de fermeté vis-à-vis de Bruxelles. Et il sait de quoi il parle apparemment j'ai pas la réf.

Jospin : "diversité et qualité". Protection et aides aux jeunes agriculteurs. 

#### Europe et politique étrangère
Jospin est contre la reprise des essais nucléaires car il voit là dedans un problème vis a vis de la non prolifération des armes nucléaires. Jospin veut un serice militaire de conscription, pas une armée de métier.

Chirac était pour le [[Traité de Maastricht]]. Chirac constate que les plus aisés peuvent aller faire leurs classes à Singapour mais les plus modestes et que les armes et techniques militaires sont devenus plus complexes et demande une professionalisation de l'armée. Lui est plutôt pour un service civique. Chirac est pragmatique, il attend à ce qu'on est bel et bien la simulation pour arrêter les essais nucléaires. Dans les faits, la France va en effet continuer les essais nucléaires.

#### Conclusion
Chirac dit que la vraie question c'est de savoir si les français veulent un 3e septenat socialiste. Lui parle d'un vrai changement qu'il pourrait apporter. Il dit que c'est toujours la meme chose avec les socialistes, dès qu'on gratte un peu la surface on voit que c'est un système de répartition de la pénurie, on étrangle petit à petit la poule aux oeufs d'or. Il veut rendre à la France son "esprit de conquête". Il faut faire confiance aux français et aimer la France.

Jospin traite les candidats et les français avec respect et sans polémique. Il propose un projet tourné vers l'avenir et non le passé. il vise les problématiques de français comme l'emploi, le logement et le salaire, la sécu, le service public, l'éducation et la recherche, l'écologie. Il s'est attaché aux valeurs de la République. "J'ai dit ce que je ferai. Je ferai ce que j'ai dit." 

## Méta
Chirac fait moins professionnel je trouve. Il sort des chiffres de manière puérile. Jospin parle de moins de sujets différents. Les deux font preuve de mauvaise foi sur les sujets. Les deux sont bons pour mettre les sujets dans leur sens, ils sont très habiles.
Chirac est très souvent taquin et se dit souvent "choqué".

## Liens 