MOC : 
Source : [[Drahi vide les caisses de SFR]]
Date : 2023-04-13
***

## Résumé
7Mds d'€ piqués ce qui a pour conséquence une désorganisation de l'entreprise et plus de la moitié des salariés en moins. Il a aussi délocalisé au Maroc car il y a là-bas un salaire dix fois moins cher. 


## Référence
[[Patrick Drahi]]
[[SFR]]


## Liens 