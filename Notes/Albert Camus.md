MOC : 
Source : [[Albert Camus]]
Date : 2023-06-18
***

## Résumé

> Il n'y a pas de vie valable sans projection sur l'avenir, sans promesse de mûrissement et de progrès. Vivre contre un mur, c'est la vie des chiens.

## Référence

## Liens 