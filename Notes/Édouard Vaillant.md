MOC : 
Source : [[Édouard Vaillant]]
Date : 2023-06-09
***

## Résumé
Édouard Vaillant, né le 29 janvier 1840 à Vierzon (Cher) et mort le 18 décembre 1915 à Paris, est un homme politique français. [[Notes/Socialisme|Socialiste]], il est l'un des élus majeurs de la [[Commune de Paris]]. Il forme, avec [[Jean Jaurès]], [[Jules Guesde]] et [[Jean Allemane]], le quatuor majeur du socialisme français de la fin du xixe siècle et du début du xxe siècle.

## Référence

## Liens 