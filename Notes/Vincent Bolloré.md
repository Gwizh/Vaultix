MOC : 
Source : [[Vincent Bolloré]]
Date : 2023-04-21
***

## Résumé

### Des ports aux médias : la nouvelle stratégie de Bolloré en Afrique
https://www.alternatives-economiques.fr/ports-aux-medias-nouvelle-strategie-de-bollore-afrique/00106905

L’homme d’affaires quitte le monde du transport pour se recentrer sur les médias. Un changement de stratégie particulièrement sensible en [[Afrique]] subsaharienne. Les raisons de ce retrait sont économiques, mais aussi juridiques et politiques.

C’est tout un symbole : le nom de Bolloré disparait du secteur des ports et de la logistique. L’homme d’affaires vient d’accepter, lundi 8 mai, la promesse d’achat de l’armateur français [[CMA-CGM]], qui met donc la main sur [[Bolloré Logistics]] pour 5 milliards d’euros.

Bolloré Logistics était devenu l’un des cinq principaux opérateurs de transport de marchandises en Europe, assurant un service par fret maritime, aérien ou terrestre. Cette opération fait suite à une autre cession de l’homme d’affaires breton. Sa branche africaine de logistique, [[Bolloré Africa Logistics]], a été vendue le 22 décembre dernier à l’armateur italo-suisse [[MSC]]. La somme de cette transaction était encore plus importante : 5,7 milliards d’euros.

Ces rachats accentuent en tout cas encore plus le phénomène de concentration du transport maritime entre des entreprises familiales européennes (le français CMA-CGM, l’italo-suisse MSC, le danois [[Maersk]], l’allemand [[Hapag-Lloyd]]).

L’Elysée a-t-il regardé de près ces transactions ? Le sujet est sensible… En septembre dernier, [[Alexis Kohler]], secrétaire général de l’Elysée, a été mis en examen, soupçonné d’avoir participé à des décisions relatives à MSC, auquel appartiennent des membres de sa famille. Quant à l’Union européenne, elle se montre étonnamment conciliante, en permettant au secteur du transport maritime de bénéficier d’une exemption dans ses règles de concurrence.

==C’est en Afrique subsaharienne que le changement de cap du groupe Bolloré se fait le plus sentir. _« Une page se tourne_, observe Cyrille P. Coutansais. _Le groupe Bolloré, qui avait racheté les activités de Delmas, était d’une certaine façon l’héritier de l’empire colonial français. Ce changement va assurément impacter la France en termes d’influence._ _»_==

Une chose est sûre : en Afrique subsaharienne, on ne pleure pas le départ de l’homme d’affaires breton. Au Cameroun, le nom de Bolloré reste associé à la catastrophe ferroviaire d’Eséka ([[Accident ferroviaire d'Éséka]]), qui, en octobre 2016, a officiellement fait 79 morts, un bilan jugé par beaucoup sous-estimé. Les critiques envers l’homme d’affaires se retrouvent aussi bien dans la rue qu’au sein de l’Etat. _« Lorsque Bolloré avait repris en 1999 la gestion de la société camerounaise de chemins de fer Camrail, il avait promis de la moderniser. Mais il n’a rien fait et n’a même pas garanti la sécurité »_, nous dit Denis Omgba Bomba, directeur de l’observatoire des médias et des opinions publiques au ministère de l’Information.

Le climat politique a également changé. Vincent Bolloré ne peut plus compter sur sa proximité avec certains dirigeants africains. En Guinée, [[Alpha Condé]] a été renversé par un coup d’état en 2021. Au [[Cameroun]] se prépare la succession de [[Paul Biya]], qui vient de fêter ses 90 ans. Alors que s’accumulent les tensions géopolitiques, l’Afrique subsaharienne fait face à une montée en puissance du sentiment anti-Français. _« Ce sentiment, Vincent Bolloré y a contribué_, nous dit  l’artiste camerounais [[Blick Bassy]], qui, à la demande d’[[Emmanuel Macron]], co-dirige la commission sur la guerre d’indépendance au Cameroun. _Regardez le contenu de ses médias, comme [[C8]] ou [[CNews]], ils sont ouvertement anti-Africains. »_

###### Se recentrer sur les médias

Pour autant, Vincent Bolloré entend bien conserver sur le continent son activité audiovisuelle, via [[Vivendi]]. Le groupe affirme même officiellement vouloir se recentrer sur les médias. Outre Canal+, Vivendi compte en son sein Hachette, [[Prisma]], [[Havas]]. Nombreux sont ceux à parier aujourd’hui sur un renforcement du groupe Bolloré au sein du capital de Vivendi. 

Canal + compte 7,6 millions d’abonnés en Afrique (9,5 millions en France). Alors que le nombre d’abonnés a longtemps décliné en France, il a au contraire enregistré une forte croissance en Afrique subsaharienne. Mais là aussi, les temps changent. Canal + doit affronter la concurrence chinoise, avec l’arrivée de l’opérateur [[Star Times]], qui compte 13 millions d’abonnés en Afrique, ainsi que le développement d’entreprises locales. C’est pour cette raison que le groupe français est devenu le premier actionnaire de la société sud-africaine Multichoice.

Sur le campus de l’université camerounaise, les étudiants ne le regrettent pas. Ils veulent tourner la page Bolloré. _« On en a marre de l’ingérence de la France »_, nous dit immédiatement l’un d’entre eux. Dans les manifestations « France dégage », notamment au [[Sénégal]], revient en boucle le nom de Bolloré. Il est devenu l’archétype des dérives de la [[Françafrique]]. _« Pour les manifestants, le colonialisme existe encore à travers ces entreprises »_, pointe Yacine Diagne.

L’aventure africaine de Bolloré irait-elle donc sur sa fin ? La stratégie interne compte aussi : l’homme d’affaires cède ces actifs africains au moment où il transmet les rennes du groupe à ses deux fils, Cyrille et Yannick. Une manière d’organiser sa succession, en se concentrant désormais principalement sur les médias français, bien loin des ports africains.

### La vraie fausse retraite de Vincent Bolloré
Article du monde : 

En théorie, le milliardaire breton a laissé la gestion de son groupe à ses deux fils. En pratique, il continue de tout décider, notamment ce qui concerne les médias. A 71 ans, ce [[Catholique]] « ultra » se sent investi d’une mission : utiliser son dernier pouvoir pour défendre l’« Occident chrétien »

Son nouveau projet s’appelle « Bien mourir ». Vincent Bolloré le destine aux résidents d’[[Ehpad]] et le proposera sans doute sur des tablettes offertes aux pensionnaires des maisons de retraite qu’il finance sans bruit. S’y déroule un _« parcours »_ en plusieurs étapes pour mieux se préparer à la mort : faire la paix avec soi-même, parler à ses enfants, ou, pourquoi pas, écrire un _« récit de vie »_, sorte de petite autobiographie à transmettre à ses descendants. L’industriel breton, 71 ans, confie à ses visiteurs avoir été très marqué par le scandale [[Orpea]], né des révélations du livre _Les Fossoyeurs_ , du journaliste Victor Castanet, sur ce réseau de 350 Ehpad et cliniques spécialisées.

Depuis le 17 février 2022, année de ses 70 ans et du bicentenaire de son groupe, il a laissé officiellement la gestion quotidienne de ses affaires à ses fils : Cyrille à la tête du groupe et Yannick à celle de [[Vivendi]], son plus beau fleuron. Il a également changé de repaire, en même temps que d’emploi du temps. Il a établi son bureau au 51, boulevard de Montmorency, au cœur du 16e arrondissement de Paris, dans l’immeuble de trois étages abritant La Compagnie de l’Odet, holding familiale propriétaire de 64 % du groupe.

Tout lâcher, prendre du recul… L’omnipotent Vincent Bolloré jurait attendre sa retraite avec gourmandise. Longtemps, il s’est amusé à montrer à ses visiteurs l’application Compte à rebours, installée sur son portable : le temps lui restant avant de passer la main. L’ancien capitaine d’industrieétait censé n’être plus qu’un simple « conseiller » du groupe et gérer de loin la stratégie de cette multinationale spécialisée dans le transport, la logistique, les médias et la communication. Mais le mot « retraité » est tabou chez les grands patrons.

Ce n’est pas seulement le pouvoir qui s’éloigne, c’est la mort qui se rapproche, et, pour le très pieux Bolloré, cette obsession s’accompagne de la lancinante question du salut. Jusqu’à ces dernières années, sa foi le poussait même à se rendre, chaque année, en pèlerinage à Lourdes (Hautes-Pyrénées), une journée au moins, en jet privé. _« Heureusement que j’ai ça, avec toutes les conneries que je fais »_, s’était-il épanché, un jour, devant l’ex-producteur artistique de l’émission « Les Guignols de l’info », [[Yves le Rolland]], avant de le licencier en 2016, deux ans après la prise de contrôle de [[Canal+]].

La main du faux retraitése devine partout. D’i-Télé, transformée en CNews après un mois de grève en 2016, aux journaux de Prisma Press (_Capital_, _Gala_, _Géo_), dont 238 journalistes ont quitté le navire après le rachat par Bolloré en 2021, les manières varient peu : prise de pouvoir à la hussarde, martingale de départs plus ou moins forcés, silences achetés avec de grosses indemnités de départ, installation de nouvelles équipes… _« **Il est très dangereux de dire non à Bolloré** »_ , avait osé souffler Rodolphe Belmer, en 2015, après son éviction de Canal+. A l’époque, le directeur général du groupe Canal n’avait dû qu’à l’intervention de son chauffeur, Diego, de pouvoir repasser dans son bureau avant l’arrivée du personnel de ménage chargé de vider ses affaires. Huit ans plus tard, rien n’a changé. _« [[Cyrille Bolloré]] est comme son père »_ , dit-on au sein du groupe. Sous-entendu : « Vincent » est toujours là, dans l’ombre.

Les nouveaux dirigeants sont choisis d’abord pour leur loyauté, les rebelles licenciés. Les médias désireux d’enquêter sur le groupe s’exposent à des poursuites ou à des résiliations de budgets publicitaires. Les journalistes qui s’intéressent de trop près aux affaires d’[[Editis]] voient leurs contrats d’édition auscultés.Et gare aux médias jugés hostiles…Séduit par les jeunes dirigeants du magazine d’extrême droite [[Valeurs actuelles]] (_VA_) comme par les vedettes montantes de [[CNews]], le directeur général de Vivendi et président du conseil d’administration d’Editis, [[Arnaud de Puyfontaine]], a d’ailleurs un temps caressé l’idée suggérée par des journalistes de _VA_de publier un livre contre _Le Monde_ .

Lors du rachat de Canal+, en 2015, Vincent Bolloré se targuait d’être _« le meilleur programmateur de la télé »_ . Il se veut aujourd’hui également spécialiste de la radio et va jusqu’à se mêler du recrutement des journalistes. Il vient ainsi de sortir son carnet de chèques pour confier à [[Pascal Praud]], tête d’affiche de CNews, la mission de remonter les audiences catastrophiques d’[[Europe 1]], station menée en deux ans au bord du gouffre.

A chaque rachat, à chaque embauche, il faut donner des gages à « Bollo ». Le cas de [[Paris Match]] est exemplaire. _«_Match _, c’est sa génération. Ça l’intéresse »_, explique l’une de ses amies. Après avoir volé au secours d’un [[Arnaud Lagardère]] ultra-endetté, le groupe Bolloré fait d’abord mine de laisser l’hebdomadaire vivre bride large, le temps que la [[Commission européenne]] valide l’offre publique d’achat sur Lagardère.

Mais, très vite, des mini-coups de force inquiètent les piliers et les historiques de la rédaction. En avril 2022, au lendemain de l’élection présidentielle, M. Mahé et sa directrice de la rédaction, Caroline Mangez, annoncent qu’ils ne feront pas la couverture sur le vainqueur, [[Emmanuel Macron]]. Leur argument ? _« Les photos ne sont pas terribles. »_Les patrons du magazine devancent-ils les désirs ou suivent-ils les ordres de Vincent Bolloré, adversaire déclaré du président ? _« On a donné une petite claque à Macron »_, se vante, en tout cas, M. Mahé devant des caciques de la rédaction.

###### Le plus choquant
[[Bernard Arnault]] lui-même ménage Bolloré. Nicolas Barré, le directeur de la rédaction du journal _[[Les Echos]]_ (propriété du PDG de LVMH), l’a appris à ses dépens. Le 24 février, il croit bien faire en laissant passer dans le quotidien économique une critique du fameux pamphlet d’Orsenna. Or Bernard Arnault rêve encore de racheter _Paris Match_ à Vincent Bolloré et ne veut rien faire qui puisse le contrarier. Quelques heures plus tard, l’un des barons de LVMH, [[Nicolas Bazire]] annonce à [[Nicolas Barré]] son éviction de la tête de la rédaction.

Mais lorsqu’il reçoit _Le Monde_ au siège de Vivendi, dans l’ancien bureau paternel, Yannick Bolloré se contente de hausser les épaules lorsqu’on évoque devant lui l’invitation sur CNews de [[Renaud Camus]], le théoricien du « [[Grand remplacement]] », la plate-forme offerte au tribun d’extrême droite [[Eric Zemmour]] sur la même chaîne ou encore les saillies complotistes entendues dans l’émission de [[Cyril Hanouna]] sur [[C8]] : _« Il n’y a pas de projet idéologique, il y a des programmes qui intéressent les téléspectateurs, et le succès de CNews est la meilleure preuve que la diversité et la liberté d’expression rencontrent un public »_ , oppose-t-il dans un sourire charmeur, comme si seul comptait le spectacle.

Il s’adresse ensuite à l’homme assis au premier rang, face à lui : [[Nicolas Sarkozy]]. Arnaud Lagardère le remercie d’avoir joué pour lui l’entremetteur avec l’industriel breton. _« Nous étions à genoux, il nous a sauvés_, lance-t-il devant l’assistance, éberluée. _Il ne me doit rien, je lui dois tout. C’est mon ami, c’est donc le vôtre. »_ Ce jour-là, au moment de quitter la pièce où se tient encore l’ancien chef de l’Etat, Arnaud Lagardère prend à part quelques éditeurs présents et glisse : _« Quand vous saurez qui nous avons choisi pour diriger Hachette, vous serez sur le cul, sur le cul ! »_ Nul doute que cet outsider de l’édition aura, lui aussi, été « casté » par « Vincent » le retraité.

## Référence

## Liens 