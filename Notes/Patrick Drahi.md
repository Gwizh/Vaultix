MOC : 
Source : [[Patrick Drahi]]
Date : 2023-04-12
***

## Résumé
Patrick Drahi est un homme d’affaires puissant. **11e fortune française** bien que domicilié en Suisse, il est à la tête du groupe Altice. Un empire tentaculaire qui réunit notamment des entreprises de télécom ([[SFR]], Cablevision…), des médias ([[BFM TV]], [[RMC]]…) ou de commerce d’art (Sotheby’s)…

Niveau Télécom, il possède aussi Hot en Israël, Portugal Télécom, Altice USA…

Il a toujours une grande influence sur le quotidien [[Libération]].

Il n'aime pas payer ses impôts et a déménagé en Suisse et acheté la nationalité de Kitts and Nevis

## Référence
[[DrahiLeaks]]

## Liens 