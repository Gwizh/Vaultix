MOC : 
Source : [[Secteur de l'électricité en Grande Bretagne à la fin du 19e siècle]]
Date : 2023-04-20
***
#Adam
## Résumé
Contrairement aux US et à l'Allemagne où il y avait 2 entreprises principales contrôlant le marché, il y avait en GB plein de petites entreprises qui se bataient pour les parts de marché.
--> [[Siemens]], [[Brush]], [[Crompton]], [[Mather & Platt]] and the Electric Construction Company

Les manufacture britanniques faisaient 100 000£ tandis que [[General Electric]] faisait $400 millions !

Aux US aussi : [[George Westinghouse]]

Les britanniques ne voulaient pas investir sans avoir des retours directement ou sans avoir un certain contrôle dans l'entreprise. Ce n'était pas propice à l'innovation risquée. Les entreprises britanniques préféraient consulter des boites extérieurs pour à peu près tout et non des procédés standards. Il y avait aussi une emphase sur la qualité qui n'était moins présente chez les américains. Ces derniers étaient donc capables d'aller bien plus vite pour créer un système global et standard. 

## Référence

## Liens 
[[Electric Lighting Acts]]
[[Sebastian Ziani de Ferranti]]