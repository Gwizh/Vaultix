MOC : 
Source : [[Les Grands Cimetières sous la lune]]
Date : 2023-06-17
***

## Résumé
Les Grands Cimetières sous la lune est un pamphlet de l'écrivain français [[Georges Bernanos]], paru en 1938, dans lequel celui-ci dénonce violemment les répressions franquistes de la guerre d'Espagne.

« Témoignage d'un homme libre », Les Grands Cimetières sous la lune est la deuxième œuvre de Bernanos en tant que pamphlétaire, après La Grande Peur des bien-pensants (1931). L'ouvrage est d'abord publié sous formes d'articles réguliers dans la revue dominicaine Sept (très ouverte à l'autonomie des laïcs par rapport à l'Église), à laquelle succédera Temps présent.

Le pamphlet consommera définitivement la rupture avec l'Action française entamée six ans plus tôt.

## Référence

## Liens 