MOC : [[Notes/Prise de notes]]
Type : Note littéraire
Titre : [[Note éphémère]]
Date : 2023-11-05
***

## Idée
La note éphémère est une note qui permet d'expliquer rapidement une idée sans structure. 

Elle n'a pas besoin de venir de quoi que ce soit.

Elle n'a pas besoin d'être prouvée par quelques références.

Elle est ensuite combinée avec des notes éphémères et littéraires pour donner une [[Note permanente|note permanente]] 

## Références 
[[@Understanding note-taking _ Zettelkasten,]]

## Mises en lien