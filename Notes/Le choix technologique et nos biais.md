MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081614
Tags : #Fini
***

# Le choix technologique et nos biais
Nous n'avons pas choisi le courant alternatif parce qu'on avait prouvé que c'était mieux. On l'a choisit car c'est ce qui semblait, **pour les scientifiques et industriels qui avaient le pouvoir de choisir**, la meilleure option au long terme. Nous l'avons alors mis partout même pour tout un tas d'applications où il n'était pas adapté. p35. Les innovations existent bel et bien mais dépendent en dernière instance du du contexte. Nous avons le pouvoir de réaliser des auto-prophéties.

Exemples :
- Il y avait des voitures électriques dans les années 1910. À Berlin, elles représentaient 20% des taxis motorisés. En plein pendant le boom de l'électricité, on pensait que tout allait fonctionnait avec. Pourtant, ces voitures étaient perdantes comparés aux voitures à essence. Elles avaient besoin de rouler dans des zones électrifiés et avaient des problèmes de batteries. On a donc ici l'exemple d'une technologie qui n'était pas prête mais qui a été poussé plus que de mesure par les industriels. Cette fois-ci, l'auto-prophétie n'a pas pris.
- Jusque dans l'entre deux guerres, on se servait de bois pour faire les avions. On a alors choisi de tout reconstruire en utilisant du métal. Pourtant, rien n'établissait la supériorité du métal dans ce contexte, on avait juste l'intuition que c'était meilleur. On pensait surtout que c'était plus moderne. Nous sommes partis dans cette direction et l'auto-prophétie a fonctionné. Nous ne saurons peut-être jamais ce qu'auraient donné des avions en bois des années 80 ou 90.

## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]