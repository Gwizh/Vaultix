MOC : 
Source : [[Franklin D Roosevelt]]
Date : 2023-05-10
***

## Résumé
Franklin Delano Roosevelt, né le 30 janvier 1882 à Hyde Park (État de New York) et mort le 12 avril 1945 à Warm Springs (État de Géorgie), est un homme d'État américain, 32e président des [[États-Unis]], en fonction de 1933 à sa mort en 1945.

## Référence

## Liens 
[[Parti démocrate (États-Unis)]]