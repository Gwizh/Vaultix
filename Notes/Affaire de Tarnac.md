MOC : 
Source : [[Affaire de Tarnac]]
Date : 2023-07-16
***

## Résumé
L'affaire de Tarnac des sabotages de caténaires commence le 11 novembre 2008, lorsque Julien Coupat est arrêté par la police antiterroriste avec neuf autres personnes et mis en garde à vue1 dans le cadre de l'enquête sur le sabotage de lignes de TGV du 8 novembre 2008. Les personnes arrêtées sont désignées par la ministre de l'Intérieur Michèle Alliot-Marie comme appartenant à un groupuscule dit de « l'ultra-gauche, mouvance anarcho-autonome ».

## Référence

## Liens 