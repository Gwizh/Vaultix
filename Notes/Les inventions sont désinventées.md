MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081614
Tags : #Fini
***

# Les inventions sont désinventées
C'est une façon de dire que nous perdons les savoirs. Cela peut être fait volontairement en pensant qu'une nouvelle technologie remplacera complètement une autre et qu'il est temps de passer à autre chose. C'est aussi possible de le faire de nous-même avec de la volonté, comme l'amiante dans les bâtiment et les fluors dans les réfrigérateurs. Nous avons désinventé les avions en bois aussi j'imagine pour les mêmes raisons.

## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]