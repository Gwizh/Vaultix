MOC : 
Titre : [[Alfred Marshall]]
Date : 2023-08-23
***

## Résumé

### ChatGPTing
Alfred Marshall (1842-1924) était un économiste britannique renommé du XIXe et du début du XXe siècle. Il est souvent considéré comme l'un des fondateurs de l'économie néoclassique, une école de pensée économique qui a eu une grande influence sur la théorie économique moderne.

## Références
![[1888, Jack l'Éventreur#^f2d3f9]]


## Liens 