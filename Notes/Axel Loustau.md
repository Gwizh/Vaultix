MOC : 
Source : [[Axel Loustau]]
Date : 2023-05-08
***

## Résumé
Axel Loustau, qui a connu [[Marine Le Pen]] à la faculté de droit d’ [[Assas]] à Paris, a joué, ces dix dernières années, un rôle à la fois financier et politique. Fondateur d’un groupe de sociétés (sécurité, imprimerie) qui a travaillé pour le Front national, il fut trésorier de [[Jeanne]] (2012-2022), conseiller régional [[Rassemblement National]] d’Île-de-France (2015-2021), président de la fédération des Hauts-de-Seine et il a pris, en 2017, les commandes de la cellule financière de la campagne présidentielle de Marine Le Pen. 

## Référence

## Liens 