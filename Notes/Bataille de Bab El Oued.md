[[Note Permanente]]
MOC : [[Histoire]]
Titre : [[Bataille de Bab El Oued]]
Date : 2022-07-11
***

## Résumé
La bataille de Bab El Oued, appelée parfois siège de Bab El Oued est un épisode de guerre civile durant la guerre d'Algérie (1954-1962). Il a opposé le 23 mars 1962 l'[[OAS]] à l'armée française dans Bab El Oued, alors quartier européen et populaire d'Alger, et un blocus du quartier par l'armée française a été mis en place jusqu'au 6 avril 1962. 

Rejetant le cessez-le-feu entre l'armée française et le [[FLN]] qui est proclamé le 19 mars 1962 par le président de la République française [[Charles de Gaulle]], l'OAS se retranche dans le quartier de Bab-El-Oued pour s'opposer par les armes au processus d'indépendance défini aux accords d'Évian. 

À la suite de l'annonce du cessez-le-feu, le général Raoul Salan, chef de l'Organisation armée secrète (OAS), s'adresse par le biais d'une allocution radiodiffusée aux anti-indépendantistes d'Algérie qu'il invite à la rébellion contre l'État français :

    « Ici Radio-France, la voix de l'Algérie française. Français, Françaises, un cessez-le feu qui livre à l'ennemi des terres françaises vient d'être consenti. Il s'agit là d'un crime contre l'Histoire de notre nation. Je donne l'ordre à nos combattants de harceler toutes les positions ennemies dans les grandes villes d'Algérie. »

Dans Le Monde du 25 mars 1972, Jean Lacouture rapporte qu'un tract énonce qu'à compter de minuit, le 22 mars 1962, officiers, sous-officiers et soldats de l'armée française en Algérie sont considérés par l'OAS comme des troupes au service d'un gouvernement étranger.

**Le 22 mars 1962, 18 gendarmes mobiles sont tués dans une embuscade, et le 23 mars 1962, 7 appelés du contingent qui avaient refusé de livrer leurs armes à l'OAS sont abattus.** L'affrontement entre l'armée française et l'OAS devient inévitable. 

La bataille s'engage dans la journée du 23 mars 1962 entre les commandos Delta de l'OAS et la Gendarmerie mobile. À la fin de la journée, les commandos de l'OAS parviennent à s'échapper du quartier assiégé, un colonel de l'armée ayant retardé à l'extrême l'ordre qui lui était donné de boucler son secteur. **Le bilan de la journée est de 15 morts et 77 blessés parmi les forces de l'ordre, et 20 morts et 60 blessés environ parmi les insurgés.** 

## Référence
[[Histoire de la France au XXe Siècle - Livre 3]]

## Liens 
[[Histoire de l'Algérie]]
[[Histoire de la France]]
[[Guerre d'Algérie]]