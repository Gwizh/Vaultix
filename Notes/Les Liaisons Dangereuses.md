[[Livre]]
MOC : [[LITTERATURE]]
Titre : [[Les Liaisons Dangereuses]]
Auteur : [[Pierre Choderlos de Laclos]]
Date : 1782
***

## Résumé


## Notions clés
- Roman épistolaire dont l'histoire n'est écrite qu'à travers des lettres. Il n'y a donc pas de narrateur et c'est très ingénieux dans la construction.
- Le but est de créer une aversion de ce genre de pratiques, pour éduquer le lecteur à y faire attention et à ne pas le reproduire. Combattre le feu par le feu.
- On comprend par les dialogues les réels intentions du personnage à force de les connaître. On arrive à déceler quand tel ou tel personnage ment, sans que la narration ne l'indique. 
- La mort de Valmont en duel est si pathétique, ça arrive d'un coup entre deux lettre (logique dans un roman sans narration). 

## Citations

## Passages amusants

## Références
