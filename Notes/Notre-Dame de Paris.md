[[Livre]]
MOC : [[LITTERATURE]]
Titre : [[Notre-Dame de Paris]]
Auteur : [[Victor Hugo]]
Date : 1831
***

## Résumé

## Notions clés
- On voit le style de Hugo avec ses nombreuses parenthèses et interpellation du lecteur. Il y a beaucoup d'explications historiques très documentées.
- Le chapitre sur l'architecture est formidable, possiblement le meilleur chapitre tous romans confondus. Livre 5, chapitre 2.
- C'est un roman qui reste très drole, ce qui est fascinant vu son age.

## Citations
> Une fois que le livre est publié, une fois que le sexe de l’œuvre, virile ou non, a été reconnu et proclamé, une fois que l’enfant a poussé son premier cri, il est né, le voilà, il est ainsi fait, père ni mère n’y peuvent plus rien, il appartient à l’air et au soleil, laissez-le vivre ou mourir comme il est.

> Vous avez été enfant, lecteur, et vous êtes peut-être assez heureux pour l’être encore.

> – Savez-vous ce que c’est que l’amitié ? demanda-t-il. – Oui, répondit l’égyptienne. C’est être frère et sœur, deux âmes qui se touchent sans se confondre, les deux doigts de la main.

> Sans doute, c’est encore aujourd’hui un majestueux et sublime édifice que l’église de Notre-Dame de Paris. Mais, si belle qu’elle se soit conservée en vieillissant, il est difficile de ne pas soupirer, de ne pas s’indigner devant les dégradations, les mutilations sans nombre que simultanément le temps et les hommes ont fait subir au vénérable monument, sans respect pour Charlemagne qui en avait posé la première pierre, pour Philippe-Auguste qui en avait posé la dernière.

> Chaque flot du temps superpose son alluvion, chaque race dépose sa couche sur le monument, chaque individu apporte sa pierre. Ainsi font les castors, ainsi font les abeilles, ainsi font les hommes. Le grand symbole de l’architecture, Babel, est une ruche.

> Mais une ville comme Paris est dans une crue perpétuelle. Il n’y a que ces villes-là qui deviennent capitales. Ce sont des entonnoirs où viennent aboutir tous les versants géographiques, politiques, moraux, intellectuels d’un pays, toutes les pentes naturelles d’un peuple ; des puits de civilisation, pour ainsi dire, et aussi des égouts, où commerce, industrie, intelligence, population, tout ce qui est séve, tout ce qui est vie, tout ce qui est âme dans une nation, filtre et s’amasse sans cesse, goutte à goutte, siècle à siècle.

> Pour qui sait le déchiffrer, le blason est une algèbre, le blason est une langue. L’histoire entière de la seconde moitié du moyen âge est écrite dans le blason, comme l’histoire de la première moitié dans le symbolisme des églises romanes. Ce sont les hiéroglyphes de la féodalité après ceux de la théocratie.

> Et si vous voulez recevoir de la vieille ville une impression que la moderne ne saurait plus vous donner, montez, un matin de grande fête, au soleil levant de Pâques ou de la Pentecôte, montez sur quelque point élevé d’où vous dominiez la capitale entière, et assistez à l’éveil des carillons. Voyez, à un signal parti du ciel, car c’est le soleil qui le donne, ces mille églises tressaillir à la fois. Ce sont d’abord des tintements épars, allant d’une église à l’autre, comme lorsque des musiciens s’avertissent qu’on va commencer. Puis, tout à coup, voyez, car il semble qu’en certains instants l’oreille aussi a sa vue, voyez s’élever au même moment de chaque clocher comme une colonne de bruit, comme une fumée d’harmonie.

> Il y a pour chacun de nous de certains parallélismes entre notre intelligence, nos mœurs et notre caractère, qui se développent sans discontinuité, et ne se rompent qu’aux grandes perturbations de la vie.

> Quand la mémoire des premières races se sentit surchargée, quand le bagage des souvenirs du genre humain devint si lourd et si confus que la parole, nue et volante, risqua d’en perdre en chemin, on les transcrivit sur le sol de la façon la plus visible, la plus durable et la plus naturelle à la fois. On scella chaque tradition sous un monument.

> C’était comme un flambeau qu’on venait d’apporter du grand jour dans l’ombre. Les nobles damoiselles en furent malgré elles éblouies. Chacune se sentit en quelque sorte blessée dans sa beauté.

> Ce soleil cabalistique semble trembler à l’œil et remplit la blafarde cellule de son rayonnement mystérieux. C’est horrible et c’est beau.

> Le joyeux écolier n’avait jamais songé à ce qu’il y a de lave bouillante, furieuse et profonde sous le front de neige de l’Etna.

> La main de l’ombre sortit de dessous son manteau et s’abattit sur le bras de Phœbus avec la pesanteur d’une serre d’aigle.

> un de ces horribles cris qui n’ont d’orthographe dans aucune langue humaine

> Ce n’était pas là une simple fille faite avec un peu de notre terre, et pauvrement éclairée à l’intérieur par le vacillant rayon d’une âme de femme. C’était un ange ! mais de ténèbres, mais de flamme et non de lumière.

> Cette douleur-là ne vieillit pas. Les habits de deuil ont beau s’user et blanchir, le cœur reste noir.

> Cela se fit avec une telle rapidité que si c’eût été la nuit, on eût pu tout voir à la lumière d’un seul éclair.

> L’air était froid ; le ciel charriait des nuages dont les larges lames blanches débordaient les unes sur les autres en s’écrasant par les angles, et figuraient une débâcle de fleuve en hiver. Le croissant de la lune, échoué au milieu des nuées, semblait un navire céleste pris dans ces glaçons de l’air.

> Le cœur humain (dom Claude avait médité sur ces matières) ne peut contenir qu’une certaine quantité de désespoir. Quand l’éponge est imbibée, la mer peut passer dessus sans y faire entrer une larme de plus.

> C’était le prêtre. Il avait l’air de son fantôme. C’est un effet du clair de lune. Il semble qu’à cette lumière on ne voie que les spectres des choses.

> Il ne prononçait pas un mot ; seulement, à de longs intervalles, un sanglot remuait violemment tout son corps, mais un sanglot sans larmes, comme ces éclairs d’été qui ne font pas de bruit.

## Passages amusants
> Certes, ce fut un triste jeu Quand à Paris dame Justice, Pour avoir mangé trop d’épice, Se mit tout le palais en feu. 


> Holà hé ! qui chante cette gamme ? quel est le chat-huant de malheur ?


> Maître Andry leva les yeux, parut mesurer un instant la hauteur du pilier, la pesanteur du drôle, multiplia mentalement cette pesanteur par le carré de la vitesse, et se tut.


> Cette foule attendait depuis le matin trois choses, midi, l’ambassade de Flandre, le mystère. Midi seul était arrivé à l’heure


> Le premier des personnages portait en main droite une épée, le second deux clefs d’or, le troisième une balance, le quatrième une bêche ; et pour aider les intelligences paresseuses qui n’auraient pas vu clair à travers la transparence de ces attributs, on pouvait lire en grosses lettres noires brodées, au bas de la robe de brocart, JE M’APPELLE NOBLESSE ; au bas de la robe de soie, JE M’APPELLE CLERGÉ ; au bas de la robe de laine, JE M’APPELLE MARCHANDISE ; au bas de la robe de toile, JE M’APPELLE LABOUR.


> Aussi avait-il coutume de dire que l’année 1476 avait été pour lui noire et blanche ; entendant par là qu’il avait perdu dans cette même année sa mère, la duchesse de Bourbonnais, et son cousin le duc de Bourgogne, et qu’un deuil l’avait consolé de l’autre.


> Certainement il y aurait injustice et mauvais goût à huer un cardinal pour s’être fait attendre au spectacle, lorsqu’il est bel homme et qu’il porte bien sa robe rouge.


> La grimace était son visage.

> S’il avait eu le Pérou dans sa poche, certainement il l’eût donné à la danseuse ; mais Gringoire n’avait pas le Pérou, et d’ailleurs l’Amérique n’était pas encore découverte.

> C’est pourtant de cette ville que Voltaire a dit qu’avant LouisXIV elle ne possédait que quatre beaux monuments : le dôme de la Sorbonne, le Val-de-Grâce, le Louvre moderne, et je ne sais plus le quatrième, le Luxembourg peut-être. Heureusement Voltaire n’en a pas moins fait Candide, et n’en est pas moins, de tous les hommes qui se sont succédé dans la longue série de l’humanité, celui qui a le mieux eu le rire diabolique.

> Pour détruire la parole écrite, il suffit d’une torche et d’un turc.

> – Voilà, en effet, une effroyable histoire, dit Oudarde, et qui ferait pleurer un bourguignon

> – Figure à faire avorter une grossesse mieux que toutes médecines et pharmaques !

> Du reste, tout cela se mêlait chez lui à de grandes prétentions d’élégance, de toilette et de belle mine. Qu’on arrange ces choses comme on pourra. Je ne suis qu’historien.

> Je présume que ce trou est habité concurremment par les chauves-souris et les araignées, et que par conséquent il s’y fait aux mouches une double guerre d’extermination.

> Et puis, en son âme et conscience, le philosophe n’était pas très sûr d’être éperdument amoureux de la bohémienne. Il aimait presque autant la chèvre.

> Guillaume ! Guillaume ! tu es le plus gros

> je me soucie de la pierre philosophale comme d’un caillou, et j’aimerais mieux trouver sur son fourneau une omelette d’œufs de Pâques au lard que la plus grosse pierre philosophale du monde !

> – Corne et tonnerre ! répondit le capitaine. – Corne et tonnerre vous-même ! répliqua l’écolier.

> Regardez, Coppenole, disait Rym à voix basse. Le voilà entre Coictier et Tristan. C’est là toute sa cour. Un médecin pour lui, un bourreau pour les autres.




## Vocabulaire 


## Références
