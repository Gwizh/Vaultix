[[Article]]
MOC : [[MOC/ECONOMIE]] [[POLITIQUE]]
Titre : [[L’Ukraine profite de la guerre pour accélérer les réformes ultralibérales]]
Auteur : [[Mediapart]]
Date : 2022-07-03
Lien : https://www.mediapart.fr/journal/international/030722/l-ukraine-profite-de-la-guerre-pour-accelerer-les-reformes-ultraliberales
Fichier : ![[L’Ukraine profite de la guerre pour accélérer les réformes ultralibérales _ Mediapart.pdf]]
***

## Résumé
- Début avril dernier, la Banque mondiale estimait ainsi que le PIB ukrainien devrait se contracter de 45 % en 2022, et les prévisions se font chaque jour plus pessimistes, alors qu’aucune issue au conflit n’est pour l’heure envisageable.
- À ces destructions considérables viennent donc s’ajouter plusieurs textes achevant la déréglementation du marché du travail, notamment celui sur la réglementation des relations de travail. Il prévoit que les employeurs peuvent, entre autres, augmenter le temps de travail de 40 à 60 heures hebdomadaires durant le conflit. Les entreprises n’étant pas en mesure de fonctionner peuvent au contraire licencier leurs salarié·es dans un délai de dix jours.
- Le projet de loi a été adopté par le Parlement ukrainien sans être examiné ni discuté par les député·es.
- Ce projet de loi [loi 5371] concerne les entreprises de moins de 250 salarié·es, qui emploient 70 % des travailleurs et travailleuses ukrainiennes. Il permet aux employeurs et aux employé·es de négocier directement des contrats sans être tenu·es de respecter le code du travail ukrainien.
- _« Nous sommes dans l’un des pays les plus corrompus d’Europe, et le droit du travail était l’une des dernières choses qui protégeaient les plus pauvres_, souligne-t-il. _Dans un contexte de crise comme celui que nous traversons, cette loi transforme les travailleurs en esclaves._ _»_
- Le président Zelensky avait dès son arrivée à la présidence en 2019 expliqué son souhait de _«_ _modifier le Code du travail en faveur du business_ _»_.
- En clair, il sera bientôt possible en Ukraine de faire légalement travailler des personnes migrantes pour des salaires de misère, sans que ces dernières soient protégées par un quelconque droit du travail.

## Référence
[[Guerre russo-ukrainienne de 2022]]
[[Libéralisation]]

## Liens 
