[[Peinture]]
MOC : [[CULTURE]]
Titre : [[Le Déjeuner des canotiers]]
Auteur : [[Auguste Renoir]]
Date : 2022-07-06
***

![[Pasted image 20220706200459.png]]

## Notions clés
Représentation d'une [[Guinguette]]. On peut y voir la Seine ainsi que des canotiers (chapeau).

## Références
