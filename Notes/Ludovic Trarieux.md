MOC : 
Source : [[Ludovic Trarieux]]
Date : 2023-07-13
***

## Résumé
Jacques Ludovic Trarieux, né à Aubeterre-sur-Dronne le 30 novembre 1840 et mort à Paris le 13 mars 1904, est un avocat et homme politique français. Instigateur de la révision du procès du capitaine Alfred Dreyfus, il est le fondateur et le premier président (1898-1903) de la [[Ligue des droits de l'homme]].

Il avait participé à la rédaction des [[Lois scélérates]] mais a ensuite en tant que dreyfusard combattus contre. 

## Référence

## Liens 
[[Affaire Dreyfus]]