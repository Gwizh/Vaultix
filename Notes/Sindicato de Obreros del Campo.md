MOC : 
Source : [[Sindicato de Obreros del Campo]]
Date : 2023-06-17
***

## Résumé
Le Sindicato de Obreros del Campo est un syndicat espagnol qui défend les intérêts des travailleurs andalous.
Il peut être considéré comme un syndicat de tendance anarchiste-communiste.
Ce syndicat a une certaine audience dans les provinces de Cordoue et de Séville.
Son secrétaire général est Diego Cañamero Valle.



## Référence

## Liens 