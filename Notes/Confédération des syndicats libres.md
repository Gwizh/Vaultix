MOC : 
Source : [[Confédération des syndicats libres]]
Date : 2023-06-11
***

## Résumé
La Confédération des syndicats libres (CSL) était une confédération syndicale créée dans les années 1950, considérée proche du [[Patronat]] et formée par des syndicalistes issus des réseaux de droite et d'extrême droite collaborationiste. D'abord appelée Confédération française du travail (CFT), elle change de nom en 1977 après l'indignation suscitée par la mort d'un syndicaliste, abattu d'une balle dans la tête par des membres de la CFT. Elle s'est dissoute en 2002. Elle était présente dans le secteur de l'industrie automobile, à la Poste, à la Ville de Paris.

## Référence

## Liens 