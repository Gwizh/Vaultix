[[Note Permanente]]
MOC : [[Histoire]]
Titre : [[Affaire Massu]]
Date : 2022-07-11
***

## Résumé
Le 18 janvier 1960 lors d'une entrevue accordée à Hans Ulrich Kempski, journaliste au quotidien ouest-allemand Süddeutsche Zeitung, le général parachutiste Massu, « héros local » de la bataille d'Alger (1957) et du putsch d'Alger (1958), a commis une infraction envers le devoir de réserve lié à sa profession, ce qui lui a valu son départ d'Algérie.

Les circonstances de cette mutation sont directement liées à un passage équivoque paru dans le journal allemand où l'officier émet son scepticisme quant à la conduite des affaires algériennes par l'Élysée : « [[Charles de Gaulle]] était le seul homme à notre disposition. Peut-être l'armée a-t-elle fait une erreur ». Bien que Massu ait toujours contesté avoir tenu de tels propos, l'article est paru dans la presse internationale et Paris a mis en doute sa loyauté ce qui a entraîné son rapatriement immédiat en métropole. 

Elle a conduit à la [[Semaine des barricades d’Alger]].

## Référence
[[Histoire de la France au XXe Siècle - Livre 3]]

## Liens 