[[Note Permanente]]
MOC : [[POLITIQUE]]
Titre : [[Shinzo Abe]]
Date : 2022-07-09
***

## Résumé
Shinzō Abe (安倍 晋三), né le 21 septembre 1954 à Nagato (préfecture de Yamaguchi) et mort le 8 juillet 2022 à Kashihara (préfecture de Nara), est un homme d'État japonais. Il est Premier ministre du [[Japon]] du 26 septembre 2006 au 26 septembre 2007 et du 26 décembre 2012 au 16 septembre 2020.

Sa politique est selon moi de droite conservatrice et libérale. Il prône un nationalisme assez fort en mettant grandement en avant la culture et la puissance japonaise. Il cherche notamment à réécrire la Constitution japonaise pour ne plus obliger au Japon le pacifisme et pour développer l'armée. Souvent révisionniste, on lui reproche sa lecture de la [[Seconde Guerre mondiale]]. Il cherche aussi un grand rapprochement avec les [[États Unis]], notamment dans les domaines militaires et économiques. 

Il fait aussi de la relance économique son principal fer de lance dans son mandat. Il met en place l'[[Abenomics]], un politique économique cherchant à inverser la déflation japonaise en proposant une politique monétaire très accommodante et audacieuse, une relance budgétaire et une stratégie de croissance à long terme.

Il est assassiné le 8 juillet 2022.

## Référence

## Liens 