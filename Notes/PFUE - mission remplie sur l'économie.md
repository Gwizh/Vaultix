[[Article]]
MOC : [[Politique]]
Titre : [[PFUE - mission remplie sur l'économie]]
Auteur : [[L'Opinion]]
Date : 2022-06-23
Lien : https://www.lopinion.fr/economie/pfue-mission-remplie-sur-leconomie
Fichier : 
***

## Résumé
Les priorités dressées par [[Emmanuel Macron]] en décembre dernier ont été respectées en dépit de l'éclatement de la guerre en [[Ukraine]].

Parmi les priorités, l’environnement avec la mise en place d’une taxe carbone aux frontières ; le numérique avec la régulation des plateformes numériques, et enfin le social avec une directive sur les salaires minimums. Alors qu’Emmanuel Macron s’est envolé mercredi pour son dernier conseil sous présidence française, le Président pourra se féliciter d’un bilan plutôt bon. En dépit de la [[Guerre en Ukraine]], une majorité des sujets mis sur la table en décembre ont bien été traités.

Cela aura mis plus de temps alors que le texte n’est pas contraignant, le pari est aussi réussi sur la mise en place d’un salaire minimum dans chacun des Etats membres. Le 7 juin au petit matin, après une énième nuit de négociation, les Etats membres et le Parlement ont finalement réussi à se mettre d’accord sur un texte commun. Pas question d’imposer un modèle unique européen alors que les salaires minimums varient de 332 euros en [[Bulgarie]] à 2 202 euros au [[Luxembourg]] au sein de l’UE. Le projet de directive prévoit simplement une batterie d’outils pour s’assurer que le salaire minimum garantit dans chaque pays « un niveau de vie décent, compte tenu de leurs propres conditions socio-économiques, du pouvoir d’achat », indique le Parlement.



## Référence
[[Emmanuel Macron]]
[[Union Européenne]]

## Liens 