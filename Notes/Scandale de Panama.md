MOC : 
Source : [[Scandale de Panama]]
Date : 2023-06-09
***

## Résumé
Le scandale de Panama est une affaire de corruption liée au percement du canal de [[Panama]], qui éclabousse plusieurs hommes politiques et industriels français durant la Troisième République (1870-1940) et ruine des centaines de milliers d'épargnants, en pleine expansion internationale de la Bourse de Paris.

Le scandale est lié aux difficultés de financement de la Compagnie universelle du canal interocéanique de Panama, la société créée par [[Ferdinand de Lesseps]] pour réunir les fonds nécessaires et mener à bien le projet. Alors que le chantier se révèle plus onéreux que prévu, Lesseps lance une souscription publique. Une partie de ces fonds est utilisée par le financier Jacques de Reinach pour soudoyer des journalistes et obtenir illégalement le soutien de personnalités politiques. Après la mise en liquidation judiciaire de la compagnie, qui ruine les souscripteurs, le baron de Reinach est retrouvé mort, tandis que plusieurs hommes politiques sont accusés de corruption. Le scandale éclate alors au grand jour. Un scandale financier du même type, l'affaire Arthur Raffalovich sur les emprunts russes, est révélé dans les années 1920.

## Référence

## Liens 