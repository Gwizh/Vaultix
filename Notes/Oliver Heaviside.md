MOC : 
Source : [[Oliver Heaviside]]
Date : 2023-04-20
***

## Résumé
Oliver Heaviside, né le 18 mai 1850 à Camden Town et mort le 3 février 1925 à Torquay, est un physicien et mathématicien britannique autodidacte. Malgré ses difficultés avec la communauté scientifique, il a beaucoup apporté aux domaines des mathématiques, de la physique et des communications télégraphiques.

Heaviside est principalement connu pour avoir reformulé et simplifié les équations de Maxwell sous leur forme actuelle utilisée en calcul vectoriel. Il a également établi l'équation des télégraphistes et développé une méthode de résolution des équations différentielles équivalente à l'emploi de la transformation de Laplace. Enfin, on lui doit l'usage des nombres complexes pour l'étude des circuits électriques.

## Référence

## Liens 