MOC : 
Titre : [[Fédération sociale démocratique]]
Date : 2023-08-23
***
#Adam 
## Résumé
La Social Democratic Federation fut la première véritable organisation socialiste du Royaume-Uni. Elle fut fondée le 7 juin 1881 par [[Henry Hyndman]] avec comme premier nom Democratic Federation. Elle devint Social Democratic Federation lors de son quatrième congrès annuel, le 4 août 1884.
## Références

## Liens 
[[Notes/Socialisme]]