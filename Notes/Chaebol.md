MOC : 
Source : [[Chaebol]]
Date : 2023-06-09
***

## Résumé
Les chaebol (wealth + family) (hangeul : 재벌, prononciation coréenne /tɕɛ.pʌɭ/, [tɕɛ.bʌɭ ]) sont en Corée des ensembles d'entreprises, de domaines variés, entretenant entre elles des participations croisées. Ils sont les équivalents coréens des anciens zaibatsu japonais (dissous par les autorités d'occupation américaines et réapparus sous la forme des keiretsu). Les deux expressions s'écrivent d'ailleurs avec les mêmes caractères chinois (財閥), la différence étant dans la prononciation apparentée.

Il en existe une trentaine, chacun détenu par une grande famille dynastique.


Après la [[Guerre de Corée]], le gouvernement coréen a choisi de donner tous les avantages possibles à ces entreprises et ces familles. Le fait de passer outre la plupart des lois par exemple, des prêts avec des taux quasiment nuls, etc...

Elles représentent environ 85% du PIB dy pays.

## Référence

## Liens 