MOC : 
Source : [[Grève]]
Date : 2023-04-14
***

## Résumé


#MIE 
	Faire grève est héroïque. Ça demande de se mettre à dos son employeur et les collègues qui ne font pas grève pour améliorer les conditions de travail et de vie de soi-même mais aussi des personnes dans le même cas de figure. On perd l'argent qu'on était sensé toucher mais surtout on se bloque dans les perspectives au travail. On rate une journée pendant laquelle on aurait pu proposer des changements, où notre présence aurait un impact. On laisse ça de côté pour la solidarité. Après ça, on ne se verra moins proposer des augmentations sauf si on les demande, on aura moins de perspectives internes car on sera vu comme moins fiables. On ne fait jamais grève par plaisir. 

## Référence

## Liens 