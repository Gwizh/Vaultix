[[Film]]
MOC : [[CINEMA]]
Titre : [[Léon]]
Auteur : [[Luc Besson]]
Date : 1994
Date de visionnage : 29-07-2022

***

## Résumé
C'est l'histoire d'un tueur à gages italien (Leon) à New York qui vit dans un appartement à problèmes :). Un jour, la famille de son voisin se faire descendre par les stups pour trafic de drogues. La petite fille, elle, échappe à ce désastre car elle était partie faire les courses. Elle se faisait de toutes façons frapper par ses parents et sa soeur, elle n'aimait que son frère, c'est d'ailleurs sa mort qui la hante le plus. 

Elle décide donc de rester avec Léon qui accepte avec beaucoup de réserve au début. C'est un gars un peu simplet qui ne sait ni lire ni écrire. On apprendra plus tard qu'il a eu une petite amie mais que le père de celle-ci l'a tué parce qu'elle traînait avec lui. Léon avait donc butter ce père et fuit à NY, à 19 ans. 

Mathilda veut venger sa famille et donc apprendre à utiliser des armes pour "nettoyer". Ils formeront donc un duo insolite dans lequel la fille appatera les cibles au pied de la porte pour que Léon les assassine, elle peut comme ça s'entraîner avec de fausses balles. 

Un jour, elle va essayer de butter l'assassin de son frère toute seule. Ce mec est fou, il prend des médicaments (drogue) pour rester calme mais finit toujours par butter tout le monde. Et d'ailleurs la petite, qui avait pourtant un stratagème, a failli se faire butter car il est ultra smart. 

Finalement Léon arrive, tue le bras droit du bad Guy et reprend Mathilda. Le méchant n'est pas content, il va prends en embuscade Léon et la petite. La petite s'échappe mais Léon meurt en emportant le tueur. 

Elle continuera sa vie dans un orphelinat. 

## Faits importants
- Venus as a Boy <3
- Meilleure musique pour l'instant je kiffe tellement
- Les deux personnages apprennent de l'autre. L'un à lire, l'autre à tuer. Les deux à aimer. 
- Y'a des trucs limite pédophile mais bon euuh rien dd'explicite au moins
- Les persos sont bien écrits je trouve, jamais trop loin (j'avais peur pour le méchant et Beethoven, ça fait très manga de merde mais non tout va bien)
- j'aime beaucoup la morale, la petite plante la plante de Léon et va poursuivre ses études. Le cercle de la vengeance est brisé. Ça contraste entre avec [[La Haine]] mdr 
- les deux personnages ont un super design, trop cool ! 

## Acteurs
[[Jean Reno]] Léon
[[Natalie Portman]] Mathilda

## Référence

## Liens 
Dans l'ordre : 
<-- [[L'armée des ombres]]
--> [[Les Tontons flingueurs]]