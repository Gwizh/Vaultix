MOC : 
Source : [[Frédéric Chatillon]]
Date : 2023-05-09
***

## Résumé
Frédéric Chatillon est un vieil ami de [[Marine Le Pen]]. Ils se sont rencontré sur les bancs de la faculté de droit Paris [[Assas]]. Ils ne se sont jamais séparé et lorsque Marine prend le contrôle du parti, Fredo devient indispensable pour le [[Front National]].  

Il est resté longtemps un très proche conseiller de Marine Le Pen, l’accompagnant dans ses déplacements. Il a même été salarié du parti en 2017 pour la campagne de Le Pen. 

Il a lui même été le chef du [[GUD]]. On rappelle que l’un d’entre eux ont tué [[Martin Aramburu]] à l’arme à feu il y a 1an.

## Référence

## Liens 