MOC : 
Source : [[L’Occident doit-il s’engager et défendre le Rojava]]
Date : 2023-06-08
***

## Résumé
C’est une véritable guerre que mène la [[Turquie]] contre l’administration autonome du [[Rojava]] (“l’Ouest” en kurde). Cette région reste pourtant l’alliée de la coalition internationale contre [[Daech]] et garde de nombreux partisans en Europe. Le résultat de l’élection présidentielle turque – [[Recep Tayyip Erdoğan]] ou Kiliçdaroglu ? – va peser sur son avenir.

Virginie Godet : L’Autorité autonome du nord et de l’est de la Syrie – AANES – puisque s’étendant désormais au-delà du Rojava proprement dit, applique les principes du confédéralisme démocratique, notamment inspiré du communalisme libertaire et de l’écologie sociale. Cette région autonome est régie depuis 2014 par un Contrat social, sorte de constitution de la fédération qui énonce ses principes de fonctionnement : une démocratie s’organisant depuis une unité de base, la commune, multiculturelle, écologique, où l’égalité des genres est garantie et dont l’économie transitionne vers un modèle coopératif.

**À l’occasion des élections en Turquie, vous attirez l’attention sur le Rojava, pourquoi ?**

P.E. : Parce que cette région située sur le territoire syrien connaît une agression militaire continue de la part du régime d’Erdogan, lequel a aidé des djihadistes à prendre la ville d’Afrin, a envahi une partie du territoire causant d’importants déplacements de population, mène des attaques contre des responsables politiques, mais aussi contre des civils avec des drones, et conduit une guerre de l’eau (environ deux tiers du débit de l’Euphrate ont été détournés) en totale contravention des accords internationaux, guerre qui coûte de nombreuses vies. Si Recep Tayyip Erdogan est réélu et qu’on le laisse faire, nous craignons le pire pour cette expérience démocratique unique.

**Inspirée par le PKK, l’administration autonome du Rojava n’est-elle pas un danger pour l’identité nationale turque ?**

P.E. : Elle constitue un danger pour les nationalistes parce que c’est la démonstration qu’il est possible de vivre ensemble. Aujourd’hui, l’Autorité autonome est constituée de représentants kurdes, mais également arabes, syriaques, turkmènes, arméniens. Chacun est représenté dans les institutions, peut parler sa langue, pratiquer sa religion s’il en a une, voir sa culture respectée.

V.G : Et elle constitue également un danger aux yeux des réactionnaires et des tenants d’une société patriarcale, car c’est une société qui prône l’égalité entre hommes et femmes. Mais pour la Turquie, qui est un pays traversé par diverses populations, cette expérience ne constitue pas un danger, c’est au contraire la preuve qu’un autre futur est possible. Et, outre la question kurde, c’est sans doute ce qui énerve le plus le régime d’Ankara.

**Pourquoi le Rojava séduit-il des militants des verts et de l’extrême gauche qui le soutiennent jusqu’à partir se battre contre l’armée turque et ses alliés ? Quels ressorts idéologiques – voire romantiques – les motivent ?**

P.E. : C’est une véritable révolution qui est en cours. Et, fait unique, c’est une révolution qui s’appuie sur la démocratie locale, et qui intègre pleinement les dimensions féministes et écologiques. Dans l’imaginaire “occidental” précisément, nous sommes ceux qui apportent la démocratie, ce qui est d’ailleurs très discutable. Cette idée, c’est un peu la version relookée d’une chrétienté civilisatrice. Mais dans les faits, on voit que l’expérience démocratique la plus originale de ce siècle est occupée à naître au Moyen-Orient.

V.G. : Comme historienne, je note que c’est aussi au cœur de ce qui fut la Mésopotamie, là où sont nés l’écriture ainsi que l’agriculture et le logement comme nous les connaissons. Défendre le Rojava, c’est donc défendre l’idée que notre démocratie ici, fragile et contestée, devrait sans doute s’inspirer de ce qui est fait là-bas, et notamment le fait d’ancrer la décision démocratique à l’échelle locale, au plus près de nous.

## Référence

## Liens 