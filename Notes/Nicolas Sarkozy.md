MOC : 
Source : [[Nicolas Sarkozy]]
Date : 2023-04-14
***

## Résumé


Sur le point de vue économique :
> En mai 2007, Nicolas Sarkozy, candidat de la majorité sortante de droite est élu président de la République sur un programme économique fondé sur le volontarisme et la fidélité aux dogmes du [[Libéralisme]] économique. Son succès est largement dû à sa critique à peine voilée de l’immobilisme et de la résignation au déclin français qui aurait été le fait de son prédécesseur [[Jacques Chirac]] et à sa double promesse d’aller chercher la [[Croissance économique]] par tous les moyens et d’être « le président du [[Pouvoir d’achat]] ». Quant aux moyens d’y parvenir, ils reposent sur la poursuite de la dérégulation permettant aux entreprises de développer sans entrave leurs initiatives  économiques, sur la baisse de la pression fiscale des entreprises et des particuliers conduisant à libérer des capitaux pour l’investissement économique et sur la possibilité de « travailler plus pour gagner plus » par l’assouplissement des règles sur la durée effective du travail et le recours aux heures supplémentaires, défiscalisées, afin d’encourager les entreprises et les salariés à y recourir largement. Toutefois, dès l’été 2007, la conjoncture mondiale vient contrarier les projets présidentiels en interdisant aux mesures prises de produire leurs effets en raison de modifications structurelles des données économiques internationales qui échappent à l’action gouvernementale.

## Référence

## Liens 