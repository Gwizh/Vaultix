MOC : 
Titre : [[Soulèvement du 18 mars 1871]]
Date : 2022-08-05
***

## Résumé
Le soulèvement du 18 mars 1871 est la riposte des révolutionnaires parisiens à la décision du gouvernement d'[[Adolphe Thiers]] de leur retirer leurs armes et leurs canons. En 24 h, le gouvernement et les troupes régulières se replient sur Versailles et abandonnent la capitale aux insurgés. C'est le début de la Commune de Paris, la plus importante des communes insurrectionnelles de France en 1870-1871. 

Le 18 mars à trois heures du matin, les soldats se mettent en marche vers leurs objectifs qui sont atteints avant 6 h, mais les chevaux et attelages prévus sont en retard et le retrait des premiers canons prend du retard. La population qui se réveille, se rassemble ; les gardes nationaux arrivent en armes. L'un d'eux, Turpin, en faction au parc d'artillerie est blessé. Il décède en dépit des soins prodigués par [[Louise Michel]]. Le Comité central alerté du mouvement des troupes fait battre l'alarme dans le XIe arrondissement et ordonne d'élever des barricades dans le quartier. 


## Référence
[[Commune de Paris]]
[[La Commune de 1871 - Que sais je]] - 3.B.1

## Liens 