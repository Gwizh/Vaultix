MOC : 
Source : [[Charles II (roi d'Espagne)]]
Date : 2023-05-08
***

## Résumé
Charles II (Carlos II von Habsburg ou Carlos II, Rey de España en espagnol), dit l'Ensorcelé, né le 6 novembre 1661 à Madrid et mort le 1er novembre 1700 dans la même ville, fils de Philippe IV et de Marianne d'Autriche, a été roi d’[[Espagne]], des Indes, de Naples, de Sardaigne et de Sicile, duc de Bourgogne et de Milan et souverain des Pays-Bas, entre 1665 et 1700, après la mort de son père.


## Référence

## Liens 