MOC : 
Source : [[Max Horkheimer]]
Date : 2023-06-18
***

## Résumé
Max Horkheimer (né le 14 février 1895 et mort le 7 juillet 1973) est un philosophe et un sociologue allemand, connu pour être le directeur de l'Institut de recherche sociale (Institut für Sozialforschung), à l'origine de la célèbre École de Francfort de 1930 à 1969, et un des fondateurs de la théorie critique (Kritische Theorie).

## Référence

## Liens 