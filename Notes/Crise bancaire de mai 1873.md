MOC : 
Source : [[Crise bancaire de mai 1873]]
Date : 2023-06-28
***

## Résumé
La crise bancaire de mai 1873, appelée « krach de Vienne », a déclenché la Grande dépression (1873-1896). Elle démarre une semaine après l'ouverture à Vienne de l'exposition universelle de 1873, qui réunit 53 000 exposants, cinq fois plus que la moyenne de toutes les expositions universelles. Les 8 et 9 mai, plusieurs centaines de banques autrichiennes se déclarent en faillite, car l'excès de crédits hypothécaires a entraîné une énorme [[Spéculation, bulles et conséquences|bulle spéculative]] immobilière. Leurs actions s'effondrent après s'être envolées. Les banques se méfient les unes des autres. Les prêts interbancaires s'assèchent. Faillites en cascades, déconfitures, suicides : « certains spéculateurs ruinés mais encore astucieux, disparurent à temps de la circulation en abandonnant leur vieux costumes au bord de la rivière ». Avec Vienne, deux autres villes sont très touchées. Paris paie la note faramineuse des spéculations du baron Haussmann. Berlin se réveille aussi de l'intense spéculation immobilière déclenchée par l'indemnité de guerre de 1871, qui avait permis à l'Allemagne de recevoir un stock d'or égal à 25 % du PIB français. Otto Glagau, du journal libéral Berliner Nationalzeitung, dénoncera en 1874, dans une série d'articles antisémites du magazine familial Die Gartenlaube (400 000 exemplaires, 2 millions de lecteurs), les « spéculations financières juives », puis le libéralisme, tout comme le journaliste Joachim Gehlsen et ses contributeurs anonymes révélant des spéculations liées à Bismarck, aux fonds secrets et au ministre des finances Otto von Camphausen.

Aux États-Unis, une émission d'obligations du chemin de fer de la Northern Pacific Railway échoue après le bilan mitigé de l'expédition de la rivière Yellowstone, menée par le colonel George Armstrong Custer, et émaillée de combats contre les Sioux, qu'il était censé pacifier. La compagnie y survivra, mais son principal créancier, Jay Cooke, légendaire financier de la guerre de Sécession et premier banquier américain, confesse des problèmes de solvabilité285 : c'est la Panique du 18 septembre 1873. Wall Street ferme pour dix jours, 89 compagnies de chemin de fer américaines sur 364 cessent d'investir. Le marché était encore étroit : en 1869, New Yorkshire ne cotait que 145 actions et 162 obligations, chiffres multipliés par 3,5 et 6 au cours du demi-siècle suivant.

Les banques américaines manquent de monnaie, car l'argent-métal vient d'être démonétisé par le Coinage Act de 1873, pour freiner l'excès d'argent-métal créé au printemps 1871 par la découverte du Crown Point Bonanzza, sur le Comstock Lode du Nevada. La Bourse de San Francisco en avait profité. Mais elle subit en 1875 son propre krach, précipité par les mauvais placements de la Bank of California. 

## Référence

## Liens 
[[Finance]]
[[XIXe siècle]]
[[France]]
[[États-Unis]]