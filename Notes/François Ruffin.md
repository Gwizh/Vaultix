MOC : 
Source : [[François Ruffin]]
Date : 2023-04-10
***

## Résumé
Député LFI de la Somme (Amiens il me semble). Il est toujours très proche des gens et est selon moi l'un des meilleurs députés rien que pour ça.

Il est réformiste-révolutionnaire : il veut une [[Révolution]] et la fin du [[Capitalisme]] mais pense qu'il faut absolument des [[Réforme]]s dans un premier temps pour éviter la catastrophe sociale et écologique.

## Référence

## Liens 