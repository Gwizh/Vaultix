MOC : 
Source : [[Panoptique]]
Date : 2023-05-05
***

## Résumé
Le panoptique (en anglais, panopticon) est un type d'architecture carcérale imaginée par le philosophe utilitariste Jeremy Bentham et son frère Samuel à la fin du XVIIIe siècle. L'objectif de la structure panoptique est de permettre à un gardien, logé dans une tour centrale, d'observer tous les prisonniers, enfermés dans des cellules individuelles autour de la tour, sans que ceux-ci puissent savoir s'ils sont observés. Ce dispositif devait ainsi donner aux détenus le sentiment d'être surveillés constamment et ce, sans le savoir véritablement, c'est-à-dire à tout moment. Le philosophe et historien [[Michel Foucault - Que sais-je]], dans [[Michel Foucault - Que sais-je#Surveiller et Punir]] (1975), en fait le modèle abstrait d'une société disciplinaire, axée sur le contrôle social. 

## Référence

## Liens 
[[Prison]]
[[Discipline]]