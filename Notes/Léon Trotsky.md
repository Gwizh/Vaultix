MOC : 
Source : [[Léon Trotsky]]
Date : 2023-06-01
***

Soit le trotskyste est réel, assumant la répression violente du léninisme, soit c'est un faux qui se sert de lui pour dire qu'il est communiste non-stalinien voire même non-violent.

## Résumé
Léon Trotsky (1879-1940) fut un théoricien [[Marxisme|marxiste]] et l’un des principaux dirigeants de la [[Révolution russe de 1917]], il fut également le fondateur de l’[[Armée Rouge]], avant de devoir fuir le stalinisme, ce qui l’emmènera au Mexique où il fut assassiné. Il fut aussi l’initiateur de la [[IVe Internationale]] en 1938 (une alliance internationale des partis communistes de tendance trotskiste).

https://www.frustrationmagazine.fr/trotsky/

Critique de la bureaucratie de l'[[URSS]]. Il énonce l'idée de la [[Révolution Permanente]] au sein de celui-ci pour combattre le pouvoir. 
Il défend la répression massive chez [[Vladimir Lénine]], les exécutions, les goulags, qui n'ont pas été inventés par [[Joseph Staline]] qui visait dès 1917 les [[Anarchie|anarchistes]], les [[Réforme|réformistes]] etc... 

En 1919, c’est bien l’Armée rouge, à l’époque dirigée par Trotsky, qui envahit l’[[Ukraine]] alors en proie à la guerre civile. Cette campagne militaire est marquée par un certain nombre d’atrocités, notamment à l’encontre de civils (bombardements, exécutions sommaires, déportations, pillages…). Il est également en grande partie responsable de l’écrasement, en 1921, de la [Makhnovtchina](https://www.youtube.com/watch?v=567XlivrW9c), un mouvement anarchiste et paysan ukrainien qui défendait une forme de gouvernement libertaire basée sur les soviets locaux par opposition à la centralisation du pouvoir bolchévique.

## Référence

## Liens 
[[Trotskisme]]