[[Article]]
MOC : [[Politique]]
Titre : [[Arlette Laguiller - Une révolutionnaire très secrète]]
Auteur : [[Usul]] [[Ostpolitik]]
Date : 2022-03-02
Lien : https://www.youtube.com/watch?v=K3O36BFA-mU&t=909s
Fichier : 
***

## Résumé
Elle s'est présentée 6 fois, de 1974 à 2007.
Elle est d'etreme gauche, révolutionnaire, trotskiste.

Elle fait partie de [[Lutte ouvrière]], ou plutôt l'union [[Communiste]] dont voici l'histoire : 
Pendant la [[Seconde Guerre mondiale]], c'était un petit groupe trotskiste (donc anti stalinien). Après la Guerre, de nombreux partis se formeront pour former au final LO en 1968.

Elle est la première femme a se présenter aux élections présidentielles, en 1974. Elle connaitra critiques a la fois pour le fait quelle soit une femme et qu'elle fut une [[Dactylographe]]. Ça plus le fait qu'elle s'est présenté 6 fois faisait qu'a la longue elle ne faisait plus peur aux autres candidats. 

Ce parti politique fonctionne autant pendant les élections que dans la rue et dans les entreprises. Ils y dialoguent avec les employés pour qu'ils puissent connaitre le tenant de leur lutte. 

Cette approche marche plutot bien car elle fera plus de 5% aux Élections présidentielles de 1995. 

Un article de [[Libération]] de 98 montre ensuite des déviances de sélection dans les membres du parti, qui doivent porter des vetements sobres, pas trop mode, qui sont fortement déconseillé d'avoir des enfants, etc... Il y a des tests et un Comité d'intégration mdr. 

C'est l'organisation communiste avec les controles les plus stricts. Il y a aussi une vraie volonté d'anonymat. Le président de LO, Robert Garcia, fut inconnu de tous pendant des décénies. Cette peur du jour vient surement des assassinats en masse en [[URSS]]. De meme, il était inconnu du système de police et sa mort fut passer sous silence pendant 1 an entier. L'idée du parti étant qu'on se doit d'être prêt pour la révolution. 

Toutes ses polémiques feront chuter le score a 1,7 en 2002. 

## Référence

## Liens 