MOC : 
Source : [[Bourses et marchés au XIXe siècle - entreprises, matières premières et spéculation]]
Date : 2023-06-23
***

## Résumé

1. **Les bourses au XIXe siècle et leur rôle dans l'économie** :
Au XIXe siècle, les bourses étaient des institutions financières où les investisseurs pouvaient acheter et vendre des actifs tels que des actions et des obligations. Elles jouaient un rôle essentiel dans le financement des entreprises et permettaient aux investisseurs de participer au capital de ces sociétés. Les bourses offraient également un lieu de négociation des produits industriels et des matières premières.

2. **Les sociétés minières et l'essor des marchés boursiers** :
Les sociétés minières étaient particulièrement importantes au XIXe siècle, en raison de l'expansion de l'industrie minière et de l'exploitation des ressources naturelles. Ces entreprises cherchaient à lever des capitaux pour financer leurs activités d'exploration et d'exploitation minières. Pour ce faire, elles faisaient appel au marché boursier, permettant aux investisseurs d'acheter des actions de ces sociétés et de participer à leur croissance.

3. **Les spécificités des produits industriels côtés en bourse** :
Outre les sociétés minières, d'autres produits industriels étaient également côtés en bourse au XIXe siècle. Cela comprenait des produits tels que le coton, le blé, le charbon, etc. Les marchés boursiers offraient un espace de négociation pour ces produits, permettant aux spéculateurs de parier sur les fluctuations des prix. Les prix de ces produits étaient influencés par l'offre et la demande sur les marchés boursiers, ainsi que par d'autres facteurs économiques.

4. **Les conséquences de la spéculation sur les prix des matières premières** :
La spéculation sur les marchés à terme des matières premières pouvait avoir des répercussions sur les prix réels de ces produits. Lorsque la spéculation était intense, une augmentation de la demande sur les marchés à terme pouvait exercer une pression à la hausse sur les prix. De plus, les opérateurs sur les marchés physiques prenaient en compte les positions prises par les spéculateurs et ajustaient leurs prix en conséquence. Ainsi, la spéculation pouvait influencer les prix des matières premières sur les marchés physiques, avec des conséquences pour les entreprises et les consommateurs qui dépendaient de ces produits.

## Référence

## Liens 
[[Finance]]
[[XIXe siècle]]