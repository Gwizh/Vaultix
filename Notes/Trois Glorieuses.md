MOC : 
Source : [[Trois Glorieuses]]
Date : 2023-05-18
***

## Résumé
La révolution de Juillet est la deuxième révolution française, après celle de 1789. Elle porte sur le trône un nouveau roi, [[Louis-Philippe Ier]], à la tête d'un nouveau régime, la monarchie de Juillet, qui succède à la [[Seconde Restauration]]. Cette révolution se déroule sur trois journées, les 27, 28 et 29 juillet 1830, dites « Trois Glorieuses ».


## Référence

## Liens 