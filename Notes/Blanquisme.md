MOC : 
Source : [[Blanquisme]]
Date : 2023-06-02
***

## Résumé
Le blanquisme est un courant politique qui tire son nom d'[[Auguste Blanqui]], socialiste français du xixe siècle.

Blanqui affirmait que la révolution devait être le résultat d'une impulsion donnée par un petit groupe organisé de révolutionnaires, qui donneraient le « coup de main » nécessaire à amener le peuple vers la révolution. Les révolutionnaires arrivant ainsi au pouvoir seraient chargés d'instaurer le nouveau système [[Notes/Socialisme|socialiste]].

## Référence

## Liens 