MOC : 
Source : [[La Revue blanche]]
Date : 2023-07-07
***

## Résumé
La Revue blanche (1889-1903) est une revue littéraire et artistique belge puis française, de sensibilité anarchiste, à laquelle collaborèrent beaucoup parmi les plus grands écrivains et artistes de langue française de l'époque. 

Dénonce les [[Lois scélérates]], les qualifiant de "législation vexatoire, moralement contestable, pratiquement impuissante".

Allant contre les courants [[Naturalisme]] et [[Réalisme]] qui voulaient séparer l'art du politique.

## Référence

## Liens 
[[Anarchie]]
[[Journalisme]]