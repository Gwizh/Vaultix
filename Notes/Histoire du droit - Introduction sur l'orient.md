MOC : 
Source : [[Histoire du droit - Introduction sur l'orient]]
Date : 2023-06-16
***

## Résumé
Vers 3000 avant notre ère, [[Némès]], premier pharaon connu, unit le delta du Nil. On s’intéressera ensuite à la [[Mésopotamie]] (le pays entre les deux fleuves : Tigre et Euphrate, situé dans le croissante fertile, voir ci-dessous.) Au nord de cette région, ce sont les montagnes de l’Arménie, il y a ensuite la partie Est de la Turquie, il y a aussi la Syrie et surtout l’Irak. Population assez sédentaire qui développe une vie sociale vers le quatrième millénaire avant notre ère. Cela était sans doute lié à la nécessité de lutter contre les crues des fleuves. On voit alors apparaître les premières traces (cunéiformes) de droit, car écrit en langue cunéiforme (écrit avec des coins), vers le troisième millénaire ; dans la ville d’[[Ur]] par exemple, et dans la région de Sumer, situé dans un delta.

Ces civilisations sont entrées dans les temps historiques avec l’invention de l’écriture : les événements deviennent documentés. Les institutions de l’époque sont des institutions royales et impériales. La royauté est considérée comme un don des dieux. On se pose la question de savoir si ces premiers régimes ont d’abord été des théocraties, c’est-à-dire des gouvernements où les chefs sont soit considérés comme des dieux, soit comme des prêtres ou des représentants des dieux, des intermédiaires entre le monde divin et le monde humain. La royauté était marquée par le divin, ce que l’on retrouve dans de nombreux régimes durant des siècles. Le deuxième caractère de cette royauté mésopotamienne et qu’elles ne sont pas toujours dynastiques. Ce sont des rois charismatiques, ils ont des dons importants, sont sages, grands, justes. Le roi est « aimé des dieux », parfois couronné par un prêtre et souvent inspiré par les dieux pour rendre la justice et élaborer le droit. Il est jus suprême.

[[Hammourabi]] (premier roi de l’empire babylonien et auteur des premières lois écrites) dit : « Je suis le roi du droit, à qui Shamash a fait présent des lois. » (Shamash étant le dieu du soleil en Mésopotamie et également le représentant de la justice)

Ce sont des royautés de ce qu’on appelle parfois des cités états, comme Babel (qui est une ziggourat : un édifice religieux mésopotamien constitué d'une haute pyramide à plusieurs niveaux dont le dernier porte un sanctuaire)

Vient ensuite l’Empire. Ce terme revêt deux sens :
- Une autorité absolue exercée par un individu,
- Un territoire dominé par une autorité variable, pouvant être une république. On peut ainsi parler de l’Empire français, surtout développé sous la troisième République, ou de l’Empire britannique, devenu aujourd’hui le Commonwealth.
On retrouve ces deux sens dans l’Empire de l’ère mésopotamienne.
On voit ainsi des autorités suprêmes comme Sargon l’Ancien, qui se prétendait le roi des quatre régions du monde (aux quatre points cardinaux) : aspiration à la domination universelle, l’Empire repousse les frontières. Ces empires nécessitent une forte organisation dans la mesure où la domination du territoire requiert une armée, une politique militaire. D’autre part, la domination d’un territoire vaste nécessite la création d’une administration puissante, hétérogène. Ils doivent être très fortement organisés. Souvent ces régimes cherchent à développer une religion commune, une culture commune; notamment ils cherchent la diffusion d’une langue commune dont ils ont besoin pour leur administration. On peut citer ainsi l’araméen, langue ancienne du Proche-Orient, langue administrative des Perses. Il faut aussi des recherches : invention des décimales, division de l’espace en 360 degrés, division de l’année en 12 mois…

Les Mèdes et les Perses :
- Les [[Mèdes]] forment une population établie à proximité de Téhéran.
- Les [[Perse]] sont plus proches des plateaux iraniens. Ces rois ont formé un empire extrêmement puissant qui était relativement étendu.

Ces deux empires étaient gouvernés par un Grand Roi (Cyrus, Xerxès etc.) Ces rois ont développé une administration partagée entre les civils et les militaires, ils disposaient d’espions qui surveillaient l’activité des gouverneurs militaires. Administration locale de contrôle donc.
L’empire perse : du 6ème au 3ème siècle.

L’empire hébraïque :
Les hébreux, installés à Canaan, ont d’abord été nomades. Ils ont fondé des royaumes. D’abord sous la direction de David, Salomon etc. Ce dernier ayant échoué dans sa tentative d’établissement d’un régime dynastique. Ces rois ont servi de modèle à la monarchie franque : leur pouvoir leur vient de Dieu, ils sont oints, et en reçoivent un caractère divin.

Les Phéniciens :
Villes de Sidon, [[Beyrouth]] sur la côte libanaise. Ils vont établir des colonies sur tout le pourtour méditerranéen. Il est possible qu’autour de 500 avant notre ère ils aient fait le tour de l’Afrique. Ils pratiquaient le commerce muet avec les Africains (échanges de nuit, sans se rencontrer, en déposant des biens et en venant en chercher d’autres le lendemain.) Cette civilisation a essaimé dans toute la méditerranée en créant des comptoirs sur des îlots dans la méditerranée, comme la ville de Tyr par exemple.

## Référence

## Liens 