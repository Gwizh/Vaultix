MOC : 
Source : [[Former Socialism's Faults]]
Date : 2023-07-03
***

## Résumé

#### Grand Competition
Plutôt l'[[URSS]]. Forcé à être opposé au capitalisme des [[États-Unis]]. Passer d'une situation proche du Brésil à la deuxième économie mondiale n'est pas rien et pourtant on va toujours faire la comparaison peu flatteuse avec les Etats Unis

#### Military Overspending
Toujours l'URSS Protéger son pays est une chose mais essayer de suivre la folie militaire des Etats Unis est une autre. Ça demandait trop de ressources. Tous les grands esprits soviétiques étaient envoyés vers ce domaine de force.

#### Lack of economic diversity
Dans les petits états socialistes plutôt. Cuba par exemple avec l'agriculture. Ces petits états dépendaient trop sur l'URSS pour certains pans entiers de leur économie. 

#### Not enough light industry
Heavy industry is good pour le niveau national
Mais les petites entreprises n'existaient pas assez et ça jouait beaucoup sur le niveau de vie. La première critique qu'on fait aux anciens régimes socialistes est le manque de variété dans les produits de consommation.

#### Not enough democratic participation
Democratie d'un autre genre
En URSS
Les populations étaient plus participative.
Système de participation des syndicats n'étaient pas aussi indépendants qu'on pouvait l'imaginer. Et il y avait un rôle trop important de l'exécutif quand même
[[Cuba]] fonctionne mieux dans ce domaine et est en train de corriger les problèmes.
#Alire Cuba and its neighbours et How the workers parliament saved the cuban revolution de Pedro Ross

#### Restriction on cultural expression
Severe point
Strong drive at first for local cultural expression in the URSS 
Dans de nombreuses régions il y avait une pression pour devenir russe et cela a provoqué des sentiments nationalistes très forts.
L'[[Albanie]] donnait une liste de nom qu'on devait prendre et c'était bien sûr terrible et stupide. La [[Bulgarie]] aussi.

#### Restriction on Religious Practice
Peut être la plus grande erreur selon lui
Le fait de restreindre voire pire réprimer l'expression religieuse est une immense erreur. Ça donne une voix aux réactionnaires.

#### Deportations
URSS les ont faite pendant la guerre à des populations qui étaizent suspectées de travailler pour l'ennemi. Ce n'est pas basé sur des éléments matériels donc c'est complètement stupide et terrible.
#Alire Stalin: The History and Critique of a Black Legend

#### Purges
C'est trop complexe je ne comprends pas assez.

#### Limitations of planning
l'économie planifiée ne pouvait pas marcher avec les moyens de l'époque, ce serait bien plus simple aujourd'hui.

#### Profit reorientation
Changement des critères pour l'économie qui se basait sur des profits plutôt que sur les besoins des gens


## Référence

## Liens 