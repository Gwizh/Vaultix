MOC : 
Source : [[Pauline Harmange]]
Date : 2023-06-10
***

## Résumé
Pauline Harmange, née le 6 décembre 1994, est une écrivaine et féministe française.

Elle fait l'objet d'une couverture médiatique internationale après que son essai publié à 450 exemplaires, intitulé Moi les hommes, je les déteste (2020), est épuisé quelques jours après sa sortie alors qu'un chargé de mission du ministère chargé des Droits des femmes a demandé d'en interdire la vente.

## Référence

## Liens 