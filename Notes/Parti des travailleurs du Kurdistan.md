MOC : 
Source : [[Parti des travailleurs du Kurdistan]]
Date : 2023-06-08
***

## Résumé
Le Parti des travailleurs du Kurdistan (en kurde : Partiya Karkerên Kurdistan, abrégé PKK, prononcé [pɛ.kɛ.kɛ]), formé en 1978, est une organisation politique armée kurde. Il est considéré comme terroriste par une grande partie de la communauté internationale, dont la Turquie, l'Australie, le Canada, les États-Unis, la Nouvelle-Zélande, l'Union européenne et le Royaume-Uni.

Le PKK est actif principalement en Turquie, en Syrie, en Iran et en Irak. Il a inspiré la création de plusieurs autres organisations dans les autres parties du Kurdistan, comme le Parti de l'union démocratique (PYD) en Syrie, le Parti de la solution démocratique du Kurdistan (PÇDK) en Irak et le Parti pour une vie libre au Kurdistan (PJAK), en Iran, qui lui sont liés, notamment dans le cadre du Koma Civakên Kurdistanê (KCK).

Son fondateur et dirigeant, Abdullah Öcalan, est détenu sur l'île-prison d'İmralı au nord-ouest de la Turquie depuis 1999.

Attentat ==!!== du 01/10/2023 ==!!!!!!!!==
## Référence

## Liens 