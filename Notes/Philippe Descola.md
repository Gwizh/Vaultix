MOC : 
Source : [[Philippe Descola]]
Date : 2023-07-23
***

## Résumé
Philippe Descola, né le 19 juin 1949 à Paris, est un anthropologue français. Fils de l'écrivain et historien hispaniste Jean Descola, ses recherches de terrain en Amazonie équatorienne, auprès des Jivaros Achuar, ont fait de lui une des grandes figures américanistes de l'anthropologie. 

## Référence

## Liens 
[[Anthropologie]]