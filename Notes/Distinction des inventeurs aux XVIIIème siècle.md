MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311080949
Tags : #Fini
***

# Distinction des inventeurs aux XVIIIème siècle
« Tout au long du XVIIIe siècle, tant en France qu’en Angleterre, la construction de la figure de l’inventeur s’est effectuée en suivant deux registres, celui de l’académie et celui du marché. » (Chauveau, 2014, p. 5) 

Les inventeurs sont devenus de plus en plus appréciés au long du XVIIIe siècle. Leurs inventions étaient montrés dans les Académies des sciences par exemple. Une fois ces distinctions obtenus, iels pouvaient aller proposer ces inventions sur les marchés.
## Référence
[[@Science, industrie, innovation et société au XIX _sup_e__sup_ siècle,Sophie Chauveau]]

