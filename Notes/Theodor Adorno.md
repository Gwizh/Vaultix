MOC : 
Source : [[Theodor Adorno]]
Date : 2023-06-18
***

## Résumé
Theodor W. Adorno [ˈteːodoːɐ̯ veː ʔaˈdɔɐ̯no]1, né Theodor Ludwig Wiesengrund le 11 septembre 1903 à Francfort-sur-le-Main et mort le 6 août 1969 à Viège, est un philosophe, sociologue, compositeur et musicologue allemand.

En tant que philosophe, il est avec Herbert Marcuse et Max Horkheimer l'un des principaux représentants de l'École de Francfort, au sein de laquelle a été élaborée la théorie critique. En tant que musicien et musicologue, il est représentant de la seconde école de Vienne et théoricien de la nouvelle musique. Il introduit avec Max Horkheimer la notion interdisciplinaire d'industrie culturelle, dont ils traitent en particulier dans l'essai Kulturindustrie de Dialectique de la Raison.

## Référence

## Liens 