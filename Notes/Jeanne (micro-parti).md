MOC : 
Source : [[Jeanne (micro-parti)]]
Date : 2023-05-08
***

## Résumé
Jeanne est un micro-parti politique créé en 2010 par des proches de [[Marine Le Pen]]. Le nom de cette dernière n'apparaît pas dans l'organigramme de Jeanne. 

L'association a pour objet « de définir et de promouvoir les conditions d'une nouvelle offre politique. Elle regroupe toutes celles et tous ceux qui veulent participer au redressement de la France. » La structure, pratiquement sans militant, a d'abord une fonction financière. Elle constitue un relais entre les candidats du [[Front national]] et des prestataires spécialisés en événementiel et communication politique.


https://www.francetvinfo.fr/replay-magazine/france-2/complement-d-enquete/video-jeanne-micro-parti-maxi-profits_872781.html


## Référence

## Liens 