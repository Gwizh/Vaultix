MOC : 
Titre : [[Cagoule (Osarn)]]
Date : 2023-09-25
***

## Résumé
La Cagoule est une organisation politique et militaire clandestine de nature terroriste, active dans les années 1930 en France.

Originellement nommé Organisation secrète d'action révolutionnaire nationale (Osarn) par ses fondateurs, puis abrégé Osar, le groupe est devenu dans la presse Comité secret d'action révolutionnaire (CSAR) à la suite d'une faute dans un rapport d'informateur. Il est plus connu sous le surnom « la Cagoule », sobriquet choisi par Maurice Pujo pour exprimer son dédain envers cette organisation fondée par des dissidents de l'[[Action française]].

D'[[Extrême droite|extrême droite]], anticommuniste, [[Antisémitisme|antisémite]], antirépublicaine et proche du [[Fascisme|fascisme]], la Cagoule commet plusieurs crimes de droit commun (assassinats, attentat à la bombe, sabotages et trafics d'armes)2. Parallèlement, elle tente une « intox » auprès de l'armée en ébruitant de fausses rumeurs relatives à une insurrection communiste. L'organisation est démantelée par la police en 1937-1938. 
## Références

## Liens 