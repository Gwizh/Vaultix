MOC : 
Source : [[Assassinat de Sadi Carnot]]
Date : 2023-07-07
***

## Résumé
L'assassinat de [[Sadi Carnot (homme d_État)]], président de la République française en exercice, a lieu à Lyon le 24 juin 1894, à quelques mois de la fin de son mandat et suite à l'adoption des deux premières « lois scélérates » visant le mouvement anarchiste français.

Évènement déterminant de l'histoire de l'anarchisme en France, cet attentat a pour conséquence l'adoption par la Chambre des députés de la dernière et de la plus marquante des « lois scélérates » interdisant tout type de propagande anarchiste en France. Le texte sera abrogé en 1992. 

## Référence

## Liens 