MOC : 
Source : [[Tournant de la rigueur]]
Date : 2023-04-14
***

## Résumé
Le « tournant de la rigueur » désigne le changement radical de politique économique décidé en mars 1983 par le président de la République [[François Mitterrand]] après l’échec d'une politique de relance keynésienne, la relance Mauroy, inspirée par le Programme commun de la gauche et dans le contexte d’attaques contre le franc. [[Jacques Delors]], ministre de l'Économie, des Finances et du Budget, dans le [[Gouvernement Pierre Mauroy (3)]], met en place une politique de rigueur à partir du 21 mars 1983, qui sera poursuivie par les gouvernements socialistes successifs.

## Référence

## Liens 
[[Keynésianisme]]