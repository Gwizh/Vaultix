MOC : 
Source : [[Gouvernement Alain Juppé (1)]]
Date : 2023-04-11
***

## Résumé
Le premier gouvernement de [[Jacques Chirac]] est nommé autour d'[[Alain Juppé]]. C'est un échec et la preuve que le programme de Chirac aux élections était surtout une question d'image. Le gouvernement ne dure que 5 mois et le ministre de l'Economie [[Alain Madelin]] est forcé de démissioner après avoir voulu remettre en cause le statut des fonctionnaires, une décision très impopulaire. 

## Référence

## Liens 