MOC : 
Source : [[Georges Perec]]
Date : 2023-06-10
***

## Résumé
Georges Perec est un écrivain, un poète et verbicruciste français né le 7 mars 1936 à Paris 19e et mort le 3 mars 1982 à Ivry-sur-Seine (Val-de-Marne). Membre de l'Oulipo à partir de 19671, il fonde ses œuvres sur l'utilisation de contraintes formelles, littéraires ou mathématiques, qui marquent son style2.

Il se fait connaître dès son premier roman, Les Choses. Une histoire des années soixante (prix Renaudot 1965), qui restitue l'air du temps à l'aube de la société de consommation. Suivent, entre autres, Un homme qui dort, portrait d'une solitude urbaine, ==puis La Disparition, où il reprend son obsession de l'absence douloureuse. Ce premier roman oulipien de Perec est aussi un roman lipogrammatique (il ne comporte aucun « e »).== Paraît ensuite, en 1975, W ou le Souvenir d'enfance, qui alterne fiction olympique fascisante et écriture autobiographique fragmentaire. La Vie mode d'emploi (prix Médicis 1978), dans lequel il explore de façon méthodique et contrainte la vie des différents habitants d'un immeuble, lui apporte la consécration. En 2012 paraît le roman Le Condottière, dont il avait égaré le manuscrit en 1966 pendant un déménagement et qui ne fut retrouvé qu'en 1992, dix ans après sa mort.

En 2017, il entre dans « La Pléiade ».

## Référence

## Liens 