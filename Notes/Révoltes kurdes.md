MOC : 
Source : [[Révoltes kurdes]]
Date : 2023-07-19
***

## Résumé
On nomme révoltes kurdes (isyan en turc; raperîn, serhildan ou, parfois, șoreș en kurde) des soulèvements menés par des Kurdes contre l'[[Empire ottoman]], puis contre la République turque, les gouvernements iraniens ou l’État irakien.

On distingue le plus souvent deux grandes séries de révoltes. On peut compter vingt grandes révoltes des Kurdes contre l'empire ottoman entre 1806 et 1912. On dénombre ensuite vingt-neuf révoltes, plus ou moins importantes contre la République turque, entre 1921 et 1938. Aucune d’entre elles ne parvient à avoir une ampleur nationale, elles restent régionales ou locales5. À celle-ci, il faut ajouter, depuis les lendemains de la [[Première Guerre mondiale]], les soulèvements des Kurdes d'Irak et d'Iran. 

## Référence

## Liens 
[[Kurdistan]]