***

La misogynoir est une forme de [[Misogynie]] envers les femmes noires dans laquelle la race et le genre jouent un rôle concomitant. Le concept est inventé par [[Moya Bailey]], féministe noire et queer, qui crée le terme pour décrire une misogynie dirigée spécifiquement envers les femmes noires en Amérique et dans la culture populaire. Trudy de Gradient de Lair, un blog womaniste sur les femmes noires dans l'art, les médias, les médias sociaux, la socio-politique et la culture, a également été créditée dans l'élaboration de la définition lexicale du terme.

Le concept repose sur la théorie de l'[[Intersectionnalité]], qui analyse comment les différentes identités sociales telles que la race, le genre, la classe et l'orientation sexuelle sont reliés au sein des systèmes d'oppression.

Lu dans [[Réinventer l'Amour]]
Femmes asiatiques sont vues comme docile et plus serrée, font de bonne mères et travaillent bien.
Les femmes noires par contre ne sont pas adaptées à une relation à long terme.


### Références
[[Une théorie féministe de la violence]]
[[Racisme]]
[[Féminisme]]