MOC : 
Source : [[RIC]]
Date : 2023-07-21
***

## Résumé
Référendum d'initiative citoyenne

Qui existe dans de nombreux pays, proposé par les [[Gilets jaunes]].

[[François Bégaudeau]] pense que c'est un énième vote anonyme qui pourrait créer des vagues radicales et comme elle ne crée pas plus le débat qu'un vote normal, les votes pourraient tout aussi réactionnaires.

## Référence

## Liens 