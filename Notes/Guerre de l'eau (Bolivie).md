MOC : 
Source : [[Guerre de l'eau (Bolivie)]]
Date : 2023-08-09
***

## Résumé
La guerre de l'eau en Bolivie, aussi appelée la guerre de l'eau de Cochabamba, désigne des séries de mobilisations qui se déroulent à Cochabamba, la quatrième ville de Bolivie, entre janvier et avril 2000. Des associations, syndicats et paysans organisent de grandes manifestations à la suite de la privatisation du système municipal de gestion de l'eau. Consécutif au doublement des prix de l'entreprise Aguas del Tunari, filiale du groupe nord-américain Bechtel, ce cycle de protestation s'est conclu par l'annulation du contrat de concession de service public de dollars américains à la [[Bolivie]]. La médiatisation de cet événement a fait de Cochabamba un symbole international de la résistance des populations face aux multinationales.
## Détails
En septembre 1999, sous l'impulsion de la [[Banque Mondiale]], la multinationale Bechtel signe un contrat avec [[Hugo Banzer]], président et ancien dictateur de Bolivie, pour privatiser le service des eaux de Cochabamba.
## Référence

## Liens 
[[Paysan]]
[[Association]]
[[Syndicat]]
[[Eau]]