MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081614
Tags : #Fini
***

# Laboratoires technologiques des monopoles économiques
p252
Avant le XXème siècle, il y avait beaucoup d'inventeurs indépendants. Mais petit à petit, les inventeurs se sont mis à travailler pour les entreprises et les industries. Très vite, les monopoles économiques de l'époque ont cherché à récupérer les chercheurs les plus innovants au sein de leurs tous nouveaux laboratoires technologiques. Le but étant de ne pas se faire dépasser par les autres monopoles ou par de nouveaux entrepreneurs. Il faut les engager ou les acheter avant qu'ils deviennent des concurrents.
L'exemple de tout ça est la General Electric Laboratory, fondée en 1900 car la General Electric était déjà leader du domaine.

Le modèle universitaire à aussi changé pour s'adapter et proposer aussi des solutions aux entreprises. Recherches financées par le secteur privée.

Il y a aussi dans ce chapitre la suite des changements structurels.
## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]