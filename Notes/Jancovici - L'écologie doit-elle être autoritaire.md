MOC : 
Source : [[Jancovici - L'écologie doit-elle être autoritaire]]
Date : 2023-07-12
***

## Résumé
[[Jean-Marc Jancovici]]

Modèle communiste.

Le coup des 4 vols d'avions vient juste d'une règle de trois en se basant sur l'[[Accord de Paris]] et le nombre de personne sur Terre. Mais ce n'était pas une proposition, juste un calcul. 

Le [[Shift Project]] avait fait déjà des calculs et pour respecter les accords il pensait déjà qu'il fallait limiter le trafic aérien.

Mais comment on se répartit démocratiquement les ressources. Il disait que c'était une blague quand il parlait de communisme... Il disait juste que privilégier les quantités vaut plus que privilégier le prix. Il préfère le système de quotat, qui est indépendant des revenus. 

Tous les économistes depuis deux siècles ont créés des outils pour et dans un monde en expansion et il faut absolument réfléchir à l'organisation de la société et de l'économie dans un monde stable ou en décroissance. Même si des penseurs y travaillent, les gens qui sont aux commandes des institutions productrices utilisent tous les outils de la croissance.  

Il faudra pouvoir décider collectivement des contraintes. Et pour ça la démocratie représentative est un système. Mais ce système implique de faire confiance à une partie de la population et aussi implique la présence de lobby, poussant des intérêts privés précis. Pour huiler ce système, il y a la possibilité des faire des formations pour les gens qui décident, la mise en place de corps intermédiaires pour ces problèmes précis et la mise en place de système du genre de la convention nationale pour le climat.

Il a émis l'hypothèse intéressante que les démocraties que nous connaissons n'ont existé et ne pourrait exister que dans une économie en croissance. Même les démocraties grecques et romaines n'ont existées que par la domination de classes sur d'autres et dans des économies qui étaient favorables. 

Euh. Pour lui l'esclavage de l'époque a été remplacé par l'esclavage de 400 milliards de machines par 2 milliards d'humains. (?)

Les questions de société doivent être instruites par les corps intermédiaires, les organisations, les conventions, les associations, les groupes d'experts, etc... La démocratie ne fonctionne que dans ce cas selon lui. Si on demande à n'importe qui de dire s'il est pour ou contre telle ou telle solution il ne pourra répondre.


## Référence

## Liens 