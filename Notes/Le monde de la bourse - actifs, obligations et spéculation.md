MOC : 
Source : [[Le monde de la bourse - actifs, obligations et spéculation]]
Date : 2023-06-23
***

## Résumé
1. **Les bases de la bourse et des marchés financiers** : La bourse est un lieu où les actifs financiers sont échangés entre les investisseurs. Cela permet aux entreprises de lever des capitaux en émettant des actions et des obligations. Les investisseurs peuvent acheter et vendre ces titres sur les marchés financiers. Les prix des actifs sont déterminés par l'offre et la demande, ainsi que par d'autres facteurs économiques.

2. **Comprendre les obligations et les titres gouvernementaux** : Les obligations sont des titres de créance émis par des entités, qu'il s'agisse d'entreprises ou de gouvernements. Lorsqu'un investisseur achète une obligation, il prête de l'argent à l'émetteur en échange d'un paiement d'intérêts régulier et du remboursement du capital à l'échéance. Les titres gouvernementaux sont des obligations émises par les gouvernements pour financer leurs dépenses. Ils sont considérés comme des actifs sûrs car ils sont généralement considérés comme peu risqués.

3. **L'impact de la spéculation sur les prix et l'économie réelle** : La spéculation se produit lorsque des investisseurs achètent ou vendent des actifs financiers dans le but de réaliser des profits à court terme, en misant sur les fluctuations des prix. La spéculation peut avoir un impact sur les prix en créant une demande artificielle sur les marchés. Cela peut conduire à une augmentation des prix, en particulier sur les marchés à terme. Les spéculateurs peuvent également influencer les prix sur les marchés physiques, car les opérateurs peuvent ajuster leurs prix en fonction des positions prises par les spéculateurs. Cela peut avoir des répercussions sur l'économie réelle, notamment dans le cas des matières premières.

4. **Les principaux actifs financiers et leur évolution au fil du temps** : Les actifs financiers englobent une large gamme de produits tels que les actions, les obligations, les devises, les matières premières, les options, les contrats à terme, etc. Au fil du temps, de nouveaux actifs financiers ont été créés pour répondre aux besoins des investisseurs et aux évolutions du marché. Par exemple, au XIXe siècle, les actions de sociétés minières étaient des actifs importants en raison de l'essor de l'industrie minière. De nos jours, de nouveaux types d'actifs ont émergé, tels que les crypto-monnaies et les produits dérivés complexes. L'évolution des actifs financiers reflète les changements économiques et technologiques de la société.

## Référence

## Liens 
[[Finance]]