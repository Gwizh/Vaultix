MOC : 
Source : [[Pierre Bourdieu]]
Date : 2023-05-11
***

## Résumé
Figure majeure de la sociologie. 
Étude des systèmes de domination qui font systèmes. Critique de l'idée que nous sommes entrepreneurs de nous-mêmes. On reprend des façons communes de penser et d'agir qui nous violente sans qu'on s'en rende compte. Interroger cette légitimité. 

Il a une mère qui est d'une forme de noblesse locale dans un petit village du Béarn. Après le lycée de Pau, il rentre à l'École normale supérieure après le khâgne au lycée Louis Le Grand. Il est en Algérie entre 1955 et 1960 pendant la [[Guerre d'Algérie]]. Il devient alors pplus sociologue et ethnologue que philosophe et restera là-dessus. Ultra productif à partir des années 70, il deviendra une figure pour les mouvements antilibéraux notamment avec son [[Discours de Pierre Bourdieu du 12 Décembre 1995]].  

J'ai lu d'abord le [[Découvrir Bourdieu]]

### Le savoir, la sociologie
"Porter à la conscience des mécanismes qui rendent la vie douloureuse, voire invivable, ce n'est pas les neutraliser; porter au jour les  contradictions, ce n'est pas les résoudre. Mais, pour si sceptique que l'on puisse être sur l'efficacité sociale du message sociologique, on ne peut pas tenir pour nul l'effet qu'il peut exercer en permettant à ceux qui souffrent de découvrir la possibilité d'imputer leur souffrance à des causes sociales et de se sentir ainsi disculpés; et collectivement occultée, du malheur sous toutes ses formes, y compris les plus intimes et les plus secretes. Rien n'est moins innocent que de laisser-faire : s'il est vrai que la plupart des mécanismes économiques et sociaux qui sont au principe des souffrances les plus cruelles, notamment ceux qui règlent le marché du travail et le marché scolaire, ne sont pas faciles à enrayer ou à modifier, il reste que toute politique qui ne tire pas pleinement parti des possibilités, si réduites soient-elles, qui sont offertes à l'action, et que la science peut aider à découvrir, peut être considérée comme coupable de non-assistance à personne en danger." 

La sociologie dérange les dominants car elle remet en qestion leut position et leur rôle face aux problèmes de la société. Il n'est jamais question d'une nature immuable qui explique ces problèmes, c'est systématiquement lié à une domination qui peut être changé. Mettre la lumière sur le système empêche de souscrire à l'idée d'une fatalité et du mérite. La partie sur le collectivement occultée évoque le fait qu'on masque nos manquements à la norme pour ne pas mettre de la lumière sur une situation peu enviable. Il est difficile de critiquer quelque chose qui est symbolique, caché, qu'on occulte collectivement. On fait même face à une incompréhension, même venant des dominés.

Il parle de l'école dans ce livre : _[[La Reproduction]]_

### La violence symbolique
En France au XXe siècle, les femmes paysannes se mariaient avec des hommes d'un statut supérieur tandis que les hommes paysans se mariaient avec des paysannes. Le nombre de paysans a alors drastiquement chuté comme en [[URSS]] où la politique violente d'Etat avait le même but mais pas les mêmes conséquences dans l'esprit des gens. Cette supériorité de l'homme citadin sur l'homme paysan, qui demande donc aux autres paysans de leur trouver une femme paysanne plutôt que d'aller chercher un citadin est une violence symbolique, intériorisée. Cette nouvelle configuration est apparue au XXe siècle du fait de la libre concurrence et de l'ouverture des milieux agricoles. Pour en savoir plus : [[Le Bal des Célibataires]]

[[La domination masculine]]
Les formes de domination sont profondément inégale, personne ne devrait être au dessus d'un autre. Pourtant les institutions privilégient les causes internés aux causes externes. [[Erreur ultime d'attribution]]. La domination masculine est ordinaire, dans le sens qu'elle est devenue tellement la norme qu'elle est invisible, alors qu'elle devrait tous nous indigner. 

Superbe idée : nos prises de paroles sont toujours dans un marché, ce qu'on dit à une certaine valeur en fonction de l'environnement, du contexte, des personnes présentes etc... On va s'auto censurer en fonction de ces cas. On va laisser les autres parler lorsqu'on est pas confiant sur la forme le fond, lorsqu'on sent que notre Capital n'est pas le bon. *Ce que parler veut dire*

### Le champ, l'illusio, le capital 
Les thèses selon [[Découvrir Bourdieu]]
Premièrement, un champ est un réseau dans lequel on trouve des positions qui sont en relation (par exemple, un individu dans la position de chef de chantier est situé dans le champ de l'industrie du bâtiment, sa position est en relation avec d'autres positions, par exemple avec les ouvriers à qui il donne des ordres, et aec les architectes, qui lui donnent des ordres.)
Deuxièmement, des pouvoirs et des capitaux (par exemple, le pouvoir de donner certains ordres aux ouvriers, et le capital de bénéficier, par l'entreprise, d'un téléphone portable, d'un bureau et d'une voiture de fonction) sont associés à des positions, ce qui permet de définir ces positions (si un ouvrier veut devenir chef de chantier, il visera - notamment - les pouvoirs et capitaux associés au poste).
Troisièmement, la possession des pouvoirs et capitaux associés à la position permet de profit, c'est à dire donne des avantages (matériels et immatériels).
Quatrièmement, on trouve dans les champs une course au profit, une compétition, un effort pour chercher de meilleurs profits, ou au moins conserver ceux dont on bénéficie déjà. Notons que la structure du champ peut de par son agencement même susciter la concurrence.
Et enfin, cinquièmement, si les positions sont définies par la façon dont pouvoirs et capitaux leur sont associés, elles le sont aussi parallèlement dans leurs rapports avec les autres positions (par exemple, être chef de chantier, c'est en pratique commander les ouvriers, obéir aux architectes, etc...)

Bourdieu n'est pas déterministe, il estime que l'individu n'est pas pris dans une mécanique qui ferait que ses comportements seraient tout à fait prévisible. 

Il y a ensuite une analogie du jeu. Dans chaque champ, il y a des enjeux, des pouvoirs ou des capitaux qui sont en jeu, que l'on peut gagner acquérir. Il dit que la compétition précède même la désignation des enjeux. Entrer dans le jeu, c'est accepter sans discuter ses règles ainsi que la compétition. Or, ce qui a de la valeur dans un jeu n'en a plus en dehors de celui-ci, un artiste a moins besoin d'argent que de capital culturel, d'entraînement, de relationnel, etc... 

### Habitus 
Différence entre habitude et habitus : on prend chacun des habitudes mais l'habitus est vraiment propre à l'individu et surtout produit par lui. On est la somme de déterminations qu'on a pu recevoir et on peut les utiliser consciemment nous-mêmes. 

### Le corps
Pour Bourdieu, tout se passe plus par le corps que l'esprit. La société forme l'individu par l'intermédiaire de son corps, plus que par son esprit. C'est du corps que vient la sensibilité et c'est par là que les effets agréables ou désagréables de la société nous arrive. Il est possible de façonner et d'assujettir un individu sans qu'une instruction explicite soit nécessaire, simplement par un lent travail du corps. Par exemple dans un champ, mes relations sociales seront plus ou moins risquées en fonction de mon capital. Pour Bourdieu il y a dans ce cas là deux façons de procéder : une première, plus ou moins volontaire qui vient des institutions (école, usine, etc... ). Il rejoint complètement Foucault là dessus. Mais il y a surtout une inculcation qui est faite par les micros actions du quotidien. Très dépendant des conditions matérielles d'existence qui permettent à des individus fortunés d'échapper à certaines exigences de l'ordre social. Psychosomatique définit comme interagissant avec le corps et l'esprit. 

### L'agent, les schèmes et le sens pratique
Au quotidien, on ne pense pas à tout ce qu'on fait, on se sert de toutes nos dispositions pour faire avec ce qu'on a. On fait ça de façon empirique et non réflective. Opposition entre agent et sujet. Agent permet de dire que l'individu n'est pas maître de tout ce qu'il fait. La plupart du temps lorsque nous sommes dans une situation ordinaire, les actions semblent tellement sensée que nous ne semblons pas déterminés par quoique ce soit. 

### Représentation des classes sociales
Les dominants qui ont les moyens de production des représentations se représentent eux-mêmes mais aussi les autres classes sociales, toujours inconsciemment ou consciemment en leur faveur. Les dominés se déprécient eux-mêmes. 

## Référence

## Liens 
[[Sociologie]]
