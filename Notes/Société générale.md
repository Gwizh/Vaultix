MOC : 
Source : [[Société générale]]
Date : 2023-04-15
***

## Résumé
En 2016, la Société générale est la principale banque française concernée par le scandale des [[Panama Papers]]. La députée PCF [[Marie-George Buffet]] demande alors la démission de Frédéric Oudéa ainsi que des suites judiciaires, étant avéré que celui-ci a menti quatre ans plus tôt, en assurant lors de son audition sous serment par la commission d’enquête sur l’évasion fiscale, que sa banque ne détenait plus de filiale dans des paradis fiscaux. Le sénateur PCF [[Éric Bocquet]] a quant à lui annoncé qu'il allait saisir le bureau du Sénat en vue de poursuivre en justice Frédéric Oudéa pour faux témoignage, au sujet de ces déclarations faites sous serment lors d'une audition au Sénat. Le 27 mai, le bureau du Sénat permet à [[Frédéric Oudéa]] d'éviter le passage en justice, au grand étonnement du groupe communist e qui avait sollicité le bureau.

## Référence

## Liens 