[[Note Permanente]]
MOC : [[Histoire]]
Source : [[Putsch des généraux]]
Date : 2022-07-08
***

## Résumé
Le putsch des généraux du 21 avril 1961, également appelé putsch d'Alger, est une tentative de coup d'État, fomentée par une partie des militaires de carrière de l'armée française en Algérie, et conduite par quatre généraux cinq étoiles (Maurice Challe, Edmond Jouhaud, Raoul Salan et André Zeller). Ils déclenchent cette opération en réaction à la politique choisie par le président de la République, [[Charles de Gaulle]], et son gouvernement, qu'ils considèrent comme une politique d'abandon de l'Algérie française. Pour sa part, le général Jacques Massu reste à l'écart après s'être vu proposer le rôle de chef.

D'autres généraux participent au putsch, de sorte qu'il est impropre de parler uniquement de « quatre généraux putschistes ». On peut citer par exemple les généraux Paul Gardy et Jacques Faure, même s'ils n'ont pas le même prestige que les quatre généraux d'armée. 

## Référence
[[Histoire de la France au XXe Siècle - Livre 3]]

## Liens 
[[Histoire de l'Algérie]]
[[Guerre d'Algérie]]