MOC : 
Source : [[Période d'Uruk]]
Date : 2023-06-18
***

## Résumé
La période d'Uruk est une période située à la charnière entre la préhistoire et l'histoire de la Mésopotamie, qui couvre à peu près le IVe millénaire av. J.-C. 

Durant cette période ces régions reçoivent une forte influence culturelle du Sud mésopotamien, phénomène qualifié d'« expansion urukéenne », qui a pu aller par endroits jusqu'à la création de comptoirs, voire de véritables colonies, mais qui ne semble pas reposer sur une domination politique. C'est en tout état de cause une période qui voit les échanges matériels et immatériels se développer sur de longues distances. 

## Référence

## Liens 