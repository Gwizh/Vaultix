MOC : 
Titre : [[En Espagne, l’affaire Rubiales déclenche une nouvelle vague féministe]]
Date : 2023-08-31
***
https://www.mediapart.fr/journal/international/300823/en-espagne-l-affaire-rubiales-declenche-une-nouvelle-vague-feministe
## Résumé
Le baiser de Luis Rubiales imposé à l’attaquante Jenni Hermoso a provoqué un vaste débat sur la culture machiste au sein du foot espagnol, mais aussi sur les violences sexuelles dans le monde du travail. Sur fond de négociations périlleuses, à Madrid, en vue de la formation d’un nouvel exécutif.

### Détails
Madrid (Espagne).– L’histoire est bien connue des féministes espagnoles. En mars 2001, une conseillère municipale de Ponferrada convoque une conférence de presse pour dénoncer le harcèlement sexuel dont elle dit faire l’objet de la part de son supérieur, le maire de cette petite ville de Castille-et-León, affilié au même parti qu’elle.

Mais sa démarche se retourne contre elle : Nevenka Fernández, 26 ans à l’époque, devient la cible d’insultes publiques et finit par démissionner. Même si le maire en question, Ismael Álvarez, 49 ans, a bien été jugé coupable par un tribunal. Un documentaire, Nevenka, mis en ligne sur Netflix en 2021, a fait connaître l’épisode aux plus jeunes générations. Une pièce de théâtre est aussi revenue, en début d’année, sur ce qui est souvent présenté comme le point de départ du #MeToo espagnol.
## Références

## Liens 