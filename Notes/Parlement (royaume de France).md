MOC : 
Source : [[Parlement (royaume de France)]]
Date : 2023-05-22
***

## Résumé
Un parlement est, sous [[Ancien Régime]] dans le royaume de France, une cour de justice d'appel, dite aussi improprement cour souveraine, puis cour supérieure à partir de 1661, qui rend la justice au nom du roi, dans un territoire délimité. Le plus haut degré de juridiction était le [[Conseil du roi]], véritable cour souveraine, qui pouvait soit être saisi par le justiciable, soit se saisir d'office de toutes les causes pendantes devant une juridiction du royaume. 

Les parlements avaient l'obligation d'enregistrer les actes royaux, c'est-à-dire de les inscrire dans leurs registres (ce qui tenait lieu de publication en l'absence de journal officiel), après avoir vérifié leur compatibilité avec le droit, les usages et les coutumes locales. Ils avaient par ailleurs un pouvoir réglementaire.

Comme ils étaient des « cours de dernier ressort », ils avaient l'obligation de faire épisodiquement une synthèse ou une refonte de la jurisprudence sur une question donnée, dans des décisions solennelles aboutissant à un arrêt de règlement. 

À la fin du règne de [[Louis XIV]] et sous [[Louis XV]], de nombreux membres des différents parlements de France animent un mouvement puissant, appelé fronde parlementaire ou [[Jansénisme parlementaire]], qui exigeait la fusion de tous les parlements en un Parlement national unique, comme il en existait en Angleterre et qui revendiquait un pouvoir législatif qui se serait exercé au nom de la Nation française.

Leur opposition au pouvoir royal va ainsi durer un siècle, mais ce sont finalement les États généraux de 1789 qui vont confronter le Roi pour s'établir comme pouvoir législatif. Les anciens parlements sont mis en vacances par décret de l'Assemblée nationale du 3 novembre 1789. La plupart des Parlements se sont inclinés. Le Parlement de Rouen a refusé d'enregistrer le décret. Le parlement de Rennes a aussi tenté de résister mais magistrats convoqués à l'Assemblée nationale le 9 janvier suivant furent frappés d'anathème. ==Les Parlements seront définitivement dissous par décret en 1790.==

## Référence

## Liens 