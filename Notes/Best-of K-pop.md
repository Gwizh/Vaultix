
## BTS
#### Bias 
1. J-Hope
2. RM
3. Suga

#### Songs
1. ON
2. I Need U
3. 134340

## Stray Kids
#### Bias
1. Seungmin
2. Felix
3. Han

#### Songs
1. MIROH
2. Back Door
3. MANIAC

## Twice
#### Bias
1. Dahyun
2. Mina
3. Chae-young

#### Songs
1. The Feels
2. Feel Special
3. Fancy

## Blackpink
#### Bias
1. Rosé
2. Jennie
3. Lisa

#### Songs
1. WHISTLE
2. Shut Down
3. BOOMBAYAH

## Seventeen
#### Bias 
1. Woozi
2. Jun
3. The8

#### Songs
1. Very Nice
2. HOT
3. _WORLD

## Aespa
#### Bias
1. Karina 
2. Winter
3. Ningning

#### Songs
1. Next Level
2. Dreams Come True
3. Savage

## Red Velvet
#### Bias
1. Irene
2. Wendy
3. Seulgi

#### Songs
1. Peek a Boo
2. Psycho
3. Ice Cream Cake

## NCT
#### Bias
1. Kun
2. Lucas
3. Xiaojun

#### Songs
1. The 7th Sense
2. BOOM
3. Candy

## TxT
### Bias
1. Beomgyu
2. Taehyun
3. Huening Kai

#### Songs
1. Frost
2. OX1=LOVESONG
3. No rules

## Enhypen
#### Bias
1. Ni-ki
2. Heeseung
3. Sunghoon

#### Songs
1. FEVER
2. Blessed Cursed
3. Drunked Dazed 

## NMIXX
#### Members
1. Lily 
2. Jiwoo 
3. Kyujin 

#### Songs
1. Dice
2. Love me like this
3. O.O

## THE BOYZ
#### Bias
1. Q
2. Eric
3. Juhaknyeon

#### Songs
1. REVEAL
2. Salty
3. Awake


## AB6IX
#### Bias
1. Daehwi
2. Woong
3. Woojin

#### Songs
1. Moonlight
2. CLOSE
3. THE ANSWER

## MAMAMOO
#### Bias
1. Hwasa
2. Solar
3. Wheein

#### Songs
1. HIP
2. Dingga
3. ILLELLA

## IVE
#### Bias
1. Wonyoung
2. Liz
3. Gaeul

#### Songs
1. LOVE DIVE
2. After LIKE
3. My Satisfaction

## P1Harmony
#### Bias
1. Jiung
2. Soul
3. Intak

#### Songs
1. SIREN
2. Back Down
3. Doom Du Doom

## Cravity
#### Bias
1. Serim
2. Minhee
3. Woobin

#### Songs
1. A to Z
2. New Addiction
3. Groovy

## ITZY
#### Bias
1. Ryujin
2. Yeji
3. Yuna

#### Songs
1. NOT SHY
2. Blah Blah Blah 
3. Wannabe 

## Ateez
#### Bias
1. Yeosang
2. Hongjoong
3. Yunho

#### Songs
1. HALAZIA
2. Guerilla
3. The Real

## EXO
#### Bias
1. Tao
2. Lay
3. Chen

#### Songs
1. Monster
2. Growl
3. Love Shot

## News Jeans
#### Bias
1. Hanni
2. Minji
3. Hyein

#### Songs
1. Hype Boy
2. Attention
3. OMG

## (G)I-DLE
#### Bias
1. Soyeon
2. Miyeon
3. Yuqi

#### Songs
1. Nxde
2. LION
3. TOMBOY

## LE SSERAFIM
#### Bias
1. Kazuha
2. Sakura
3. Chaewon

#### Songs
1. ANTIFRAGILE
2. FEARLESS
3. The Hydra

