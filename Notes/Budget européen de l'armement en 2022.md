MOC : 
Source : [[Budget européen de l'armement en 2022]]
Date : 2023-04-24
***

## Résumé
L’Europe n’avait plus dépensé autant d’argent pour s’armer depuis la [[Guerre froide]]

📈 Entre l’invasion russe de l’Ukraine et les tensions en Asie, les dépenses militaires des pays du continent européen ont atteint 480 milliards l’an dernier.

## Référence

## Liens 
[[Armement]]
[[Guerre]]