MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081430
Tags : #Fini
***

# Une société peut se reposer sur des ressources et techniques clés puis les perdre
Comparaison avec l'[[Âge du bronze]] et sa chute. Nous nous servions du commerce et de la technologie du bronze pour tout faire. Nous avons perdu les moyens de faire ça et les civilisations se sont écroulées. Comme les technologies cruciales n'étaient que dans les mains de certains groupes, la société ne s'est pas reconstruite. Nous avons perdus beaucoup de savoir.

Notamment la lecture et l'écriture. Elles étaient réservées aux élites, ces dernières ont disparus, ces techniques avec.

L'informatique pourrait très bien finir comme ça.

## Référence
[[@Jonathan Blow - Preventing the Collapse of Civilization (English only),]]