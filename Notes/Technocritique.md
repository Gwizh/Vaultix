
## Définitions
Le néologisme technocritique définit un courant de pensée axé sur la critique du concept du « progrès technique », considéré comme une idéologie qui serait née au XVIIIe siècle durant la Révolution industrielle et qui, depuis la Seconde Guerre mondiale, s'ancre dans les consciences, principalement sous les effets de l'automatisation (la mécanisation ou le machinisme) et de l'informatisation. 

## Sources
#Alire Technocritiques - François Jarrige [13€ ici](https://www.chasse-aux-livres.fr/prix/2707189456/technocritiques-francois-jarrige?query=Technocritiques%20Du%20refus%20des%20machines%20%C3%A0%20la%20contestation%20des%20technosciences)
#Alire [Tout ça](https://fr.wikipedia.org/wiki/Technocritique#Liens_externes)
#Alire [Les livres de cet homme](https://fr.wikipedia.org/wiki/Gilbert_Rist)
#Alire [En vrai juste faut piocher](https://fr.wikipedia.org/wiki/Technocritique#Voir_aussi)