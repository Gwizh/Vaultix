Titre : [[Mona Chollet]]
Type : Noeud
Tags : #Noeud
Ascendants : [[Féminisme]]
Date : 2023-04-06
***

## Résumé
Une femme qui a rapport serein avec les hommes même si elle a échappé deux fois à des agressions. Elle a une relation stable de 18 ans avec un homme et n'a pas d'enfants.

[[Réinventer l'Amour]]


## Référence

## Liens 