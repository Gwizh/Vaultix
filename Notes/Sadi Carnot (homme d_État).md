[[Note Permanente]]
MOC : [[Histoire]] [[POLITIQUE]]
Titre : [[Sadi Carnot (homme d'État)]]
Date : 2022-07-29
***

## Résumé
Marie François Sadi Carnot, plus souvent appelé Sadi Carnot, est un homme d'État français, né le 11 août 1837 à Limoges (Haute-Vienne) et mort le 25 juin 1894 à Lyon (Rhône). ==**Il est président de la République du 3 décembre 1887 à sa mort en 1994.**==

À la fin de l’année 1887, à la suite de la démission du président [[Jules Grévy]] en raison du [[Scandale des décorations]], l’Assemblée nationale le place en tête du premier tour de l’élection présidentielle anticipée avec 36 % des suffrages, devant Jules Ferry, dont la candidature divise les parlementaires républicains. Au second tour, après le retrait de ce dernier, Sadi Carnot est élu face au général Saussier avec 74 % des voix. 

Le président Carnot est rapidement confronté à une forte remise en cause des institutions républicaines avec la montée de l’antiparlementarisme, les succès électoraux du [[Boulangisme]] et les attentats anarchistes, alors que se poursuit l’instabilité ministérielle et qu’éclate le [[Scandale de Panama]]. Son mandat est également marqué par le centenaire de la [[Révolution Française]] et l'Exposition universelle de Paris. En politique étrangère, il favorise la signature de l’alliance franco-russe avec l’empereur Alexandre III. 

Il est mort assassiné le 25 juin 1894 par un anarchiste italien ([[Assassinat de Sadi Carnot]]). 

## Référence
Président précédent : [[Jules Grévy]]
Président suivant : [[Jean Casimir-Perier]]

## Liens 