MOC : 
Source : [[Algérianisme]]
Date : 2023-07-09
***

## Résumé
L’algérianisme est un mouvement intellectuel et culturel né en Algérie, dans la première moitié du XXe siècle, en milieu social d'origine européenne, de langue française, principalement urbain. 

L’Algérianisme, apparait aux environs de 1900 avec, essentiellement, les textes de Robert Randau, « Les colons » ; « Les Algérianistes » (Paris, Sansot, 1911) et provoque la résurgence du mythe de l’« _Afrique latine_ ». C’est une littérature qui célèbre et cautionne l’entreprise « _civilisatrice_ » de la [[Colonialisme|colonisation]]. ^09cc42

Fascinés par le [[Naturalisme]] et [[Emile Zola]], ils reproduisaient les poncifs et les stéréotypes du discours colonial, tout en mettant en scène des personnages tirés de la tradition picaresque espagnole. ^bc7073

## Référence

## Liens 