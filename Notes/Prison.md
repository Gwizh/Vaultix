MOC : 
Source : [[Prison]]
Date : 2023-05-12
***

## Résumé





Article de médiapart :
#### Plus que jamais, les prisons françaises sont une humiliation pour la République
https://www.mediapart.fr/journal/france/110523/plus-que-jamais-les-prisons-francaises-sont-une-humiliation-pour-la-republique-0?utm_source=global&utm_medium=social&utm_campaign=SharingApp&xtor=CS3-5

Dans son rapport annuel, la Contrôleuse générale des lieux de privation de liberté ([[CGLPL]]) lance une nouvelle alerte sur les établissements pénitentiaires qui « débordent » et « se substituent aux asiles d’antan ». Avec la construction de places supplémentaires pour seule réponse, l’État s’entête dans une politique vouée à l’échec.

Si « la sociologie est un sport de combat », pour reprendre l’expression de Pierre Carles, la dénonciation des conditions de détention dans notre pays ressemble à une course d’endurance. 

En juin 2000, un rapport sénatorial qualifiait les prisons françaises d’« ==humiliation pour la République== ». De record de surpopulation en record de surpopulation, le Contrôleur général des lieux de privation de liberté (CGLPL), une autorité administrative indépendante créée il y a quinze ans, ne peut que réitérer ce constat. 

Avec 73 080 détenus pour 60 899 places au 1er avril (soit un taux d’occupation de 120 % en moyenne), les établissements pénitentiaires sont surpeuplés comme jamais. Rien ne peut s’améliorer tant qu’ils le sont. Et hormis une brève parenthèse en 2020, rien de sérieux n’est entrepris pour y remédier. 

« L’inertie est un mur auquel se heurtent les alertes incessantes du CGLPL, sur l’état déplorable des lieux qu’il visite » – prisons, hôpitaux psychiatriques, centres de rétention administrative, locaux de garde à vue, centres éducatifs fermés – écrit le CGLPL, pour qui « l’État semble endormi ». 

Le volet pénitentiaire de la loi de programmation et d’orientation pour la justice, présentée en conseil des ministres par [[Éric Dupond-Moretti]] le 3 mai, ne fait pas exception. Pour le gouvernement, seule la création de 15 000 nouvelles places de prison sur les deux quinquennats d’[[Emmanuel Macron]] « permettra de résorber la surpopulation carcérale », d’assurer un déroulement normal des activités et de la prise en charge sanitaire, de garantir la dignité des détenus et la sécurité, de rendre le métier de surveillant plus attractif. 

Rien ne semble pouvoir ébranler ce raisonnement mécanique, voire magique. Il relève pourtant du déni. ==« Plus on construit, plus on remplit »==, rappelle Dominique Simonnot. Le centre pénitentiaire de Lutterbach (Haut-Rhin), inauguré en novembre 2021, est occupé à ==180 %==. La prison de la Santé, rouverte en 2019 après quatre ans de rénovation, dépasse les 160 %. « Depuis 1990, plus de 25 000 places de prison ont été créées et le nombre de prisonniers a augmenté dans les mêmes proportions », rappelait l’[[Observatoire international des prisons]] (OIP) début janvier. 

Dans ce paysage carcéral sinistré, la psychiatrie occupe une place particulière. Dominique Simonnot fustige une _« indifférence générale qui, au fil du temps, a laissé la prison se substituer aux asiles d’antan, enfermant dans ses murs plus de 30 % des prisonniers atteints de troubles graves. Voilà comment, à leur corps défendant, surveillants et détenus ont, en quelque sorte, été contraints de se muer en infirmiers psychiatriques »_. haha [[Michel Foucault - Que sais-je]]

Comme le souligne aussi l’OIP, 680 millions d’euros [sont consacrés à l’immobilier pénitentiaire](https://oip.org/analyse/budget-penitentiaire-2023-enfermer-toujours-plus-peu-importent-les-conditions/) en 2023, c’est-à-dire _« ==plus de cinq fois le budget consacré à la réinsertion et à la prévention de la récidive== »_. Si le garde des Sceaux parie sur _« la libération sous contrainte de plein droit »_ pour faire sortir 6 000 détenus en fin de peine, la réforme des réductions de peine devrait dans le même temps conduire à 10 000 détenus supplémentaires. Et rien que sur le premier quinquennat d’Emmanuel Macron, _« 120 infractions punies d’emprisonnement ont été créées ou durcies »_.

## Référence

## Liens 