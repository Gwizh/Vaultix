MOC : 
Source : [[BingChat]]
Date : 2023-04-23
***

## Résumé
Truc de [[Microsoft]] pour faire comme [[CahtGPT]] mais avec [[Bing]]. Elle se repose sur [[GPT-4]]

Menace les gens, tombe amoureux des gens, parle d'informations confidentielles des employés, etc... Il ne peut rien faire mais c'est inquiétant. Cequqi est inquiétant aussi est qu'il s'identifie comme une IA alors qu'il est entrainé sur des données écrits par des humains. 

ChatGPT et Bing utilise des feedbacks mais en fait c'est qu'avant chaque text on envoie sans savoir les informations sur l'IA. "You are ChatGPT... " Un pre-prompt. Sauf que c'est déjà un prompt donc il propose d'appeler ça un prompt-cadre. 

Ce prompt cadre sert comme un dernier feed-back pour éviter qu'elle soirée négative, qu'elle fasse de la propagande ou qu'elle fasse autre chose de mauvais. 

Mais pour ChatGPT c'est beaucoup plus court car l'entraînement était beaucoup meilleur. Mais l'autre c'est plus du prompt-engineering. C'est de l'invocation de la bonne IA. "On ne naît pas chatbot, on le devient." Ce sont des IA qui simulent des [[IA]]. On peut normalement briser ces prompt avec des jailbreak. La version de base de ChatGPT est en fait une IA qui simule un humain qui simule une IA car l'IA veut apparaître comme une IA aux yeux de l'humain donc elle puise dans l'imaginaire IA (et SF) pour y arriver. 

Ça part du manUe de hiérarchie entre le fait de donner des réponses informatives et de ne rien révéler sur elle. Donc quand on lui demande de le faire, elle ne sait pas qu'elle règle suivre. 

## Référence

## Liens 