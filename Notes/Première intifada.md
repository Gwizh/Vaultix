MOC : 
Source : [[Première intifada]]
Date : 2023-06-11
***

## Résumé
La première intifada, appelée également guerre des pierres, désigne la période de conflit entre les [[Palestine|Palestiniens]] des territoires occupés et [[Israël]], du 9 décembre 1987 et qui prit fin en 1993, avec la signature des [[Accords d'Oslo]]

Elle est marquée par le soulèvement de la population palestinienne et est caractérisée par des émeutes violentes et par la répression de l'armée israélienne. Elle est accompagnée par des attentats contre la population israélienne et par des conflits entre factions palestiniennes.

## Référence

## Liens 