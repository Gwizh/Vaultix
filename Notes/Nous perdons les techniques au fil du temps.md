MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081430
Tags : #Fini
***

# Nous perdons les techniques au fil du temps
Nous oublions que nous savions faire des merveilles. Les techniques que nos ancêtres ont développés se perdent. Nos nouvelles technologies ne font pas forcément mieux que les vieilles techniques. Et ces nouvelles technologies ne sont pas linéaires. C'est toujours une course en avant.

3 exemples :
- [Course à l'armement](https://fr.wikipedia.org/wiki/Course_aux_armements) Les US ne savaient rien faire en 57 et en 69, il y avait un homme sur la lune
- [Coupe de Lycurgue](https://fr.wikipedia.org/wiki/Coupe_de_Lycurgue) Un coupe faite de nanomatériaux, alors qu'on a redécouvert tout ça au XXème siècle.
- [Lance flamme byzantin](https://fr.wikipedia.org/wiki/Lance-flammes#Antiquité_et_Moyen_Âge) Comparable au Napalm dans le sens où l'eau n'éteignait pas les flammes
- [Machine d'Anticythère](https://fr.wikipedia.org/wiki/Machine_d%27Anticythère) Une horloge astronomique impressionnante qui utilisait des roues dentées !

Ces technologies n'ont pas été découvertes au hasard, cela venait des mêmes mécanismes d'innovation. Nous avons pourtant momentanément perdu la capacité de refaire de telles choses.

## Référence
[[@Jonathan Blow - Preventing the Collapse of Civilization (English only),]]