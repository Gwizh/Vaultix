MOC : 
Source : [[Émile Loubet]]
Date : 2023-06-09
***

## Résumé
Émile Loubet, né le 30 décembre 1838 à Marsanne (Drôme) et mort le 20 décembre 1929 à Montélimar (Drôme), est un homme d'État français. Il est ==président de la République française du 18 février 1899 au 18 février 1906.==

Avocat de profession, il est élu député de la Drôme en 1876. Après avoir été ministre des Travaux publics, il est président du Conseil de février à décembre 1892. Il est en parallèle ministre de l'Intérieur, fonction qu'il conserve dans le premier gouvernement Ribot. En 1896, il accède à la présidence du Sénat.

En 1899, seul candidat en lice, il est élu président de la République après la mort soudaine de Félix Faure. Son mandat est notamment marqué par la fin de l'[[Affaire Dreyfus]], par l’affaire des fiches, et par le vote de la loi de séparation des Églises et de l'État à l'initiative d’[[Aristide Briand]]. À l'issue de son septennat, il se retire de la vie politique dans la Drôme.

## Référence
Précédent président : [[Félix Faure]] 

## Liens 
