MOC : 
Source : [[Fonds Marianne]]
Date : 2023-04-13
***

## Résumé
Le fonds Marianne de Schiappa a financé des contenus politiques en période électorale

L’association de Mohamed Sifaoui n’est pas la seule à avoir été grassement rémunérée par le fonds Marianne. Une autre structure a touché plus de **300 000 euros** alors qu’elle venait d’être créée et n’avait aucune activité connue. Sous couvert de lutte contre le séparatisme, elle a diffusé des contenus politiques à l’encontre d’opposants d’Emmanuel Macron pendant les campagnes présidentielle et législatives.

Son interview retentissante dans Playboy n’aura pas suffi à éteindre la polémique. Depuis deux semaines, [[Marlène Schiappa]] n’arrive pas à s’extirper d’une affaire de financements publics, du temps où elle était ministre déléguée chargée de la citoyenneté. En cause : la gestion du fonds Marianne, lancé quelques mois après l’assassinat de [[Samuel Paty]] en octobre 2020.

L’USEPPM (Union des sociétés d’éducation physique et de préparation militaire) a bénéficié de la plus grosse dotation du fonds Marianne (355 000 euros). Mais au-delà de cette seule structure, d’autres subventions interrogent. L’opération a en effet permis de financer grassement une autre association, « Reconstruire le commun », à hauteur de 330 000 euros, ce qui en fait la deuxième bénéficiaire.

Selon nos informations, le projet présenté par « Reconstruire le commun » visait à _« déployer un discours républicain adapté aux codes et référents culturels des 18-25 ans sur les réseaux sociaux et sur le Web sous forme de vidéos, visuels, mèmes, interviews, reportages, documentaires et événements »_. Quelles garanties a offertes cette toute jeune association pour capter plus de 300 000 euros d’argent public, alors que 47 projets concurrents avaient été présélectionnés (et seulement 17 retenus) ?

> Je trouve ça anormal qu’un Mélenchon puisse citer comme il veut Trotski qui est quand même le plus grand inspirateur des camps nazis. - Extrait d’une vidéo financée par le fonds Marianne 

Une des vidéos : https://twitter.com/AvecLePS/status/1646219845812523009?t=ciieDwaRMNiz3orHmrcIuQ&s=09

## Référence

## Liens 