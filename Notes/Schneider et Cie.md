MOC : 
Source : [[Schneider et Cie]]
Date : 2023-04-20
***

## Résumé
Schneider et Cie (se prononce "Schnedr") était une société française fondée au Creusot (Saône-et-Loire) en 1836 par Adolphe et Eugène Schneider. Ses points d'attache étaient les activités minières (charbon, notamment le bassin houiller de Blanzy et minerai de fer), sidérurgiques (fonderie et forge) puis de matériels (locomotives, armement, moteurs, turbines…) et d'équipement (ponts, charpentes…). Elle a constitué le premier groupe industriel français significatif au niveau international. Elle s’est développée pendant plus d’un siècle en se diversifiant fortement. En 1949 elle était la maison-mère de l'holding nommé Groupe Schneider, avec SFAC et CITRA comme principales filiales. En 1966 le Groupe Schneider a fusionné avec le Groupe Empain pour former le Groupe Empain-Schneider avec Creusot-Loire de 1970 à 1984 et Jeumont-Schneider jusqu'en 1986. Schneider Electric en est un descendant.

## Référence

## Liens 
[[Secteur de l'électricité en Grande Bretagne à la fin du 19e siècle]]