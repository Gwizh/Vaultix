MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081629
Tags : #Fini
***

p104
Production en série de l'automobile vue comme l'"industrie moderne", celle qui donne le tempo. On disait des 30 glorieuses qu'elles étaient les années du fordisme. Ensuite dans les années 70, on parlait de post fordisme pour le Japon. 
#Question Pourquoi est-ce que l'automobile est autant considéré comme moteur ? Est-ce réellement si utile et rentable que ça ?

## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]