MOC : 
Source : [[Mai]]
Date : 2023-05-02
***

## Résumé
Le nom du mois de mai vient de la divinité Maia, fille de Faunus et femme de Vulcain. En ancien français, mai, qui symbolisait le plus beau mois de l’année, entrait dans plusieurs locutions avec une idée de bonheur, de prospérité, comme « avoir bon mai ».

## Référence

## Liens 