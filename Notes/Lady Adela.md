MOC : 
Source : [[Lady Adela]]
Date : 2023-07-19
***

## Résumé
Lady Adela Jaff ou Adela Khanem, appelée la Princesse des Braves par les Britanniques, est une dirigeante kurde de la tribu Jaff : elle est l'une des premières femmes dirigeantes célèbres de l'histoire du [[Kurdistan]].

Adela Jaff était l'une des rares femmes dirigeantes de la région. Elle fut même vénérée par les Britanniques à cause de ses actes de miséricorde envers ses captifs britanniques, qui participèrent à l'invasion mésopotamienne pendant la [[Première Guerre mondiale]]. 

## Référence

## Liens 
[[Royaume-Uni]]