[[Note Permanente]]
MOC : [[Histoire]] [[POLITIQUE]]
Source : [[Union des Forces démocratiques]]
Date : 2022-07-08
***

## Résumé
L'Union des forces démocratiques (UFD) est un cartel électoral mis en place en juillet 1958 pour regrouper la gauche non-communiste opposée au retour du général [[Charles de Gaulle]] à la faveur de la crise de mai 1958, et qui appela ainsi à voter non lors du [[Référendum constitutionnel français de 1958]]. L'UFD continua à exister jusqu'à la création du Parti socialiste unifié (PSU), en avril 1960. 

Le bureau national de l'UFD comprenait dix membres, soit, dans l'ordre alphabétique : Albert Châtelet, Edouard Depreux (SFIO), Jean Hyppolyte, Maurice Lacroix (Jeune République), Gilles Martinet (UGS), Pierre Mendès-France (Parti radical), François Mitterrand (UDSR), Joseph Perrin (UDSR), Laurent Schwartz et Robert Verdier (SFIO). 

Elle connaît une déroute totale au [[Élections législatives françaises de 1958]] avec 1,2 % des voix.

## Référence

## Liens 
[[Politque française]]