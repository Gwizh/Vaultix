MOC : 
Source : [[Parrhèsia]]
Date : 2023-05-07
***

## Résumé
L’étymologie du terme laisse penser qu'il a d’abord signifié « la possibilité de tout dire ». Mais il a sans doute dès l’origine une dimension politique. Il désigne le privilège des citoyens athéniens, égaux en droit, de pouvoir prendre la parole à l’Assemblée. La παρρησία représente en quelque sorte l’idéal démocratique des Athéniens. Le terme caractérise d'abord le régime de parole du maître face au disciple attentif : parole de franchise, opposée aux arabesques de la flatterie et aux subtilités rhétoriques. Parole droite et directe. La parrhèsia est une certaine parole de vérité, un dire-vrai qui ne relève ni d'une stratégie de démonstration, ni d'un art de la persuasion, ni d'une pédagogie. Il y a parrhèsia quand un dire-vrai ouvre pour celui qui l'énonce un espace de risque. 

## Référence

## Liens 