MOC : 
Source : [[Ataï]]
Date : 2023-06-08
***

## Résumé
Ataï est le « grand chef » [[Kanak]] de Komalé, près de La Foa. En 1878, il mène la [[Grande révolte kanak de 1878]] contre les colonisateurs français. Après des victoires importantes qui inquiètent l'administration coloniale de la [[IIIe République]], il est tué par un auxiliaire kanak missionné par les colons français. 

## Référence

## Liens 