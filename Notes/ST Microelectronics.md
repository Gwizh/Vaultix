MOC : 
Source : [[ST Microelectronics]]
Date : 2023-06-11
***

## Résumé
« La réindustrialisation de la France se joue ici ! », proclamait à son tour M. Emmanuel Macron le 12 juillet 2022. Accompagné de quatre ministres, le président venait déposer 2,3 milliards d’euros sur la table : la contribution de l’État aux 5 à 7 milliards d’euros que devrait coûter la dernière extension de l’usine de ST.

> _« C’est le plus grand investissement industriel des dernières décennies hors nucléaire et un grand pas pour notre souveraineté industrielle »_

L’annonce tombait en pleine « alerte renforcée sécheresse » (niveau 3 sur 4), requalifiée à la mi-août en « crise sécheresse » (niveau 4). Or l’industrie de la puce électronique est particulièrement gourmande en eau. Avec leur montée en puissance d’ici 2024, l’usine ST et sa voisine du groupe Soitec, également spécialisée dans les semi-conducteurs, devraient consommer près de 29 000 mètres cubes par jour, soit l’équivalent de la ville de Grenoble, ses 160 000 habitants, ses industries, ses labos de recherche, ses activités municipales ou commerciales. Avec le nouvel agrandissement annoncé, ST prévoit d’engloutir à terme 33 000 mètres cubes par jour.

## Référence

## Liens 