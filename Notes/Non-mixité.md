MOC : 
Source : [[Non-mixité]]
Date : 2023-06-11
***

## Résumé
La non-mixité est une pratique consistant à organiser des rassemblements réservés aux personnes appartenant à un ou plusieurs groupes sociaux considérés comme opprimés ou discriminés, en excluant la participation de personnes appartenant à d'autres groupes considérés comme potentiellement discriminants (ou oppressifs), afin de ne pas reproduire les schémas de domination sociale.

Cette pratique est utilisée par certains groupes de divers courants militants, notamment du féminisme, de l'antiracisme, du mouvement LGBT ou de personnes en situation de handicap.

## Référence
[[Féminisme]]
[[LGBTQIA+]]

## Liens 