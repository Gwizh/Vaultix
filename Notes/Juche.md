[[Note Permanente]]
MOC : [[POLITIQUE]]
Source : [[Juche]]
Date : 2022-09-01
***

## Résumé
Le juche, ou juché (en coréen : 주체사상, romanisation nord-coréenne : Juchesasang, , litt., « pensée du corps-maître ») est une idéologie autocratique qui fonde le régime de la République populaire démocratique de Corée et conçue par son premier dirigeant Kim Il-sung. Elle guide les activités du Parti du travail de Corée, dominant en [[Corée du Nord]], et du Front démocratique national anti-impérialiste en [[Corée du Sud]] et a vocation à diriger le destin de chaque citoyen. L'idéologie du juche est accompagnée par une propagande intensive, dont l'un des aspects les plus saillants est un culte de la personnalité autour de la dynastie Kim.

Cette idéologie prône le mouvement de la nation vers le chaju (« indépendance »), à travers la construction du charip (« économie nationale ») et l'accent sur le chawi (« autodéfense ») elle-même garante du chaju, afin d'établir le [[Notes/Socialisme]]. Cette idéologie est portée par trois axes : l'autonomie militaire, l'autosuffisance économique et l'indépendance politique.

Le mot jucheseong dans le langage courant coréen veut dire « autonomie », « indépendance » ou « initiative ».

Cette doctrine est inscrite en 1972 dans la Constitution de la Corée du Nord.


## Référence

## Liens 
