Titre : [[Panique de 1893]]
Type : Noeud
Tags : #Noeud
Ascendants : [[Politique américaine pendant la deuxième révolution industrielle]]
Date : 2023-07-03
***

## Propositions
La panique de 1893 est un krach financier qui se déroule aux États-Unis. Semblable à la panique de 1873, elle est marquée par l'effondrement du financement des sociétés de chemins de fer qui ont construit trop et trop vite. Il en résulte une série de faillites bancaires. S'y ajoute la crise des Silver Certificates que les investisseurs tentent de convertir en or, alors que la réserve américaine n'en dispose pas en suffisance. La dépression économique engendrée est alors la pire qu'ait connu le pays qui subit un taux de chômage de plus de 10 % et un nombre de faillites record.

Lors de cette crise, le Trésor américain en difficulté est en partie sauvé par le financier J. P. Morgan, président de la banque du même nom, la J.P. Morgan & Co., puis, en 1897, par la ruée vers l'or du Klondike. 
## Sources

## Référence
