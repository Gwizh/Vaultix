MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081629
Tags : #Fini
***

# Ingénieurs et entretien
p143
Les ingénieurs ne veulent pas être comparés à des techniciens. Pourtant, en 1980 aux États-Unis, 70% des ingénieurs faisaient principalement de la maintenance, loin du rôle d'innovation qu'on imagine. C'était en plus de l'entretien technique et donc principalement un monde d'hommes. En URSS, c'était des femmes. 
#Alire Ninotchka
En France, nous avons des ingénieurs d'Etat comme ceux de polytechnique. C'est un rôle conservateur du maintien de l'ordre en place. En Chine, les ingénieurs sont en masse dans le Politburo.

## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]