Titre : [[Réinventer l'Amour]]
Type : Source
Tags : #Source
Ascendants : [[Féminisme]]
Date : 2023-04-06
***
## Résumé
### Introduction
Un ennui vis à vis de l'hétérosexualité car les rapports avec les hommes sont complexes. Et puis il y a une libération à ne pas suivre les normes, ne plus avec cette pression sociétale. La pression est autre bien sûr haha. La culture queer est exubérante, confiante, colorée, c'est peut être ce qui manque aux relations hétérosexuelles. C'est aux lesbiennes qu'on doit le fait que des couples hétérosexuels peuvent aller dans des sex-shops sans honte et la démocratisation de pratiques sexuelles plus imaginative.

L'absence de misogynie peut être perçue comme homosexualité.

Une hétérosexualité et un féminisme en même temps et vouloir avec grande volonté des femmes à des positions de pouvoir.

Idée que les hommes aiment les femmes mais seulement sous des critères très précis, surtout pour leur corps. Les lesbiennes arrivent beaucoup mieux à accepter les imperfections. Les hommes peuvent voir les femmes comme des objets et des sujets.

A lire : Jane WARD : _The tragedy of Heterosexuality_
Elle parle du _Génie Lesbien_ d'[[Alice Coffin]]. Elle dit qu'elle peut complètement lacher prise sur son féminisme contrairement à Mona.

### Prologue
Les histoires d'amour se finissent souvent pas le fait de se mettre en couple, la suite est toujours plus dure à décrire. Ou alors, ce sont les problèmes dans une relation déjà établiée qui sont décrits. Ils n'ont qu'à suivre une recette universelle.

"Etre passionnément amoureux pour la première fois et être aimé en retour, c'est apparemment trop banal, trop privé, trop commun. Unamour naufragé, impossible, fait au contraire de la noble littérature."

Contradiction entre affects bourgeois du marriage et de la stabilité et des affects romantiques de l'aventure et des aventures. 

Femmes qui mettent du maquillage participent à créer un contraste avec leur apparence de nuit. Il faut contenir tout ce qui n'est pas présentable de jour mais la nuit on ne peut plus rien cacher.

Critique de Belle du Seigneur d'Albert Cohen. Livre sexiste et réac

L'amour sous algorithme de Duportail : Tinder qui continue de t'envoyer des propositions même une fois un match réussi. On est toujours en compétition

#### Deux êtres qui s'aiment n'en font qu'un : lequel ?
[[Deux êtres qui s'aiment n'en font qu'un - lequel]]
Nancy Hudson : "Deux êtres qui s'aiment n'en font qu'un : lequel ?". Une idée cruelle mais juste qu'un couple crée un rabougrissement de l'identité. Elle trouve que 3 semaines par jour de solitude font beaucoup de bien. Pour ça il faut deux logements séparés (ou + héhé) et donc que chacun puisse s'en payer un. Pour un enfant, on peut imaginer que des parents plus heureux puisse aider, même avec deux foyers. Lire : *Un lit à soi* de Le Garrec

### Chapitre 1 
https://www.lesinrocks.com/actu/pourquoi-la-une-de-paris-match-avec-nicolas-sarkozy-et-carla-bruni-pose-probleme-172017-05-07-2019/

Se faire petite pour être aimé. Par la taille elle montre que les relations hétérosexuelles actuelles sont vu a traverse le patriarcat et centrées sur la domination. L'émancipation des femmes dérange car elle détruit nécessairement cette conception. C'est tout l'idéal de la femme soumise qui s'effondre. 
Lire *La taille des hommes* de [[Nicolas Herpin]]

Au Burkina Faso, les femmes nourrissent au sein les garçons directement mais attendent plus pour les filles. Ça crée dès la naissance le fait que les garçons reçoivent directement ce qu'ils veulent mais que les femmes doivent attendre le bon gré d'une autre personne. 

Problème avec les femmes musclées, qui "font tropde sport". La littérature a longtemps décrit l'idéal féminin, fragile et faible, dont on peut avoir pitié. En sport c'est pareil et paradoxale, Serena Williams recevaient des critiques car trop musclée. Pire, l'entraîneur d'Agnieszka Radwanska voulait la garder féminine et donc de garder une taille moyenne. De même pour Bev Francis. De même pour l'intelligence et la carrière professionnelle. À la suite d'un succès professionnel (Oscar, élections), les femmes ont 2 fois plus de chances de divorcer que les hommes car ceux-ci ne supportent pas le succès de leur conjointe. Les gens ne comprennent pas l'amour et la passion pour le travail. Chez les femmes, la position sexuelle est beaucoup plus probable de mettre la position sociale. 

Erotize Equality : aimer une femme du même âge, du même statut social, de même musculature etc... Un homme qui aime son égal est rare et donc sait qu'il a plus de valeur, et surtout il sait qu'il peut aimer beaucoup plus de femmes. Lire p. 80! 
Il en va de même pour la race évidemment. 
Lire [[Louis Malleret]] *l'exotisme indochinois dans la littérature française depuis 1860*.

Il en va de même pour la culture, on peut aimer une femme pour son appartenance supposée à une culture précise sans l'aimer elle pour ses caractéristiques réelles.
Lire [[Robin Zheng]]
le MRA : "Mere Preference Argument" : dire aimer les asiatiques n'est pas pareil qu'aimer les brunes. 

Pour être en sécurité dans une relation, il est même parfois plus simple de se faire petite.

### Chapitre 2
Une révolution intérieure de [[Gloria Steinem]]. La honte est une caractéristique féminine. Elle met la femme dans une position vulnérable. Il faut combattre ça sans oublier que la seule cause des violences sont les violents eux-même. Prendre en compte notre propre vision permet de mieux se préparer au combat. Même des féministes très lucide se sont faites tuer. Il faut toujours prendre du recul et rester lucide. 

Mettre de côté un homme pour un "détail" qui pourrait être révélateur d'un comportement violent à un moment peut sembler capricieux mais peut faire la vie ou la mort. Il faut donc s'écouter avant tout. On dit aux femmes d'attendre le prince charmant et en même temps de se méfier de tous les autres hommes. Une fois en couple, il est donc difficile de sortir de la situation et de se rendre compte du danger.
_Mon Roi_ de Maïwenn. Violence psychologique.

Le viol conjugal est reconnu par la loi depuis 1992.

L'homme peut répéter que la femme est folle, qu'elle dit n'importe quoi. Et par le manque de confiance en soi on finit par le croire.

Elle parle de [[Bertrand Cantat]] 

Un homme reproduit ce qu'il considère comme plus masculin que lui, et cherche une femme avec qui il va pouvoir faire ça. [[John Stoltenberg]] *Refuser d'être un homme*.
*acquittée* [[Alexandra Lange]]. 

Les femmes ne se défendent souvent pas eux-mêmes mais défendent leurs enfants, ceux-ci comptent plus souvent à leurs yeux. L'homme s'auto culpabilise, il s'en veut de se mettre en colère, d'être jalou etc... Une aide extérieure, même minime, peut permettre lma femme de s'en rendre compte. 

L'homme violent est viril et donc attirant. Les tueurs en série sont entourés de groupies. Et non, ce n'est pas comme dit [[Eric Dupont-Moretti]] que les fous attirent les fous. Ce sont les gens qui sont trop empli des idées propres au patriarcat qui voient en la violence la masculinité.  Les femmes n'aiment pas ces hommes malgré la violence mais parce qu'ils sont vioents. On ne retrouve absolument pas l'inverse, des hommes qui sont attirés par des tueuses en série. 

Repenser l'amour comme étant la "volonté d'étendre son moi dans le but de nourrir sa propre conscience spirituelle et celle de l'autre."

### Chapitre 3
La construction féminine de l'amour : une attente douloureuse, passive, obessessionelle jusqu'au prince charmant : un moment de bonheur ultime. (mieux dit p 156).
On inculque pas du tout aux garçons ces envies d'amour : ils ont plutôt l'impression que c'est un truc de filles et ce font traités comme tel ou même de bébé ou de pédé. Ce n'est pas sérieux pour un homme de placer l'amour comme préoccupation numéro 1. Pour un homme, le travail compte avant tout et si amour il y a c'est plus un soutien moral ou domestique. On apprend à une femme à vivre dans une relation amoureuse stable et aux hommes l'inverse, les loisirs, le travail et le sexe sans procréation.
_Revolution of the Heart_ Wendy Langford. L'idée que le roman (et le roman d'amour) permet au femme de s'enfuir dans un monde où elles ont le contrôle et où elles peuvent faire ce qu'elles veulent contrairement à la vraie vie où elles doivent souvent effacer leurs besoins et envies.
Il y a nécessité économique dans l'amour pour une femme. Ça a longtemps été le seul moyen d'accéder à un bon niveau social et niveau de vie. Le manque d'investissement des hommes dans l'amour et le sexe peut être expliqué par le fait quer cela n'a jamais représenté pour eux une nécessité économique. Toujours d'actualité car depuis 1990, le travail en temps partiel a triplé et concerne 30% des femmes contre 8% des hommes. On compte aussi 2.1 millions de femmes non étudiantes entre 21 et 59 qui sont en couple et sans emploi.
_Educated in Romance_ Dorothy C Holland et Margaret A Eisenhart.  
[[Rupi Kaur]]
Il y a aussi l'idée ancrée que le sexe est nécessaire à la santé de la femme, bien qu'il n'y ai rien qui le prouve. Ça permet soi-disant un équilibre intérieur.

La dépendance émotionnelle des hommes est tout aussi forte mais masquée. Les femmes sans occupent sans s'en rendre compte et quand elles paraissent capricieuses, c'est parce qu'elles réclament la même chose sans le recevoir. Les hommes paraissent sûr d'eux, pouvant garder la tête froide dans émotions mais c'est parce que les femmes s'en occupent dans l'ombre, sans même qu'ils le sachent. Les femmes sont recherchées et méprisées pour leur travail émotionnel. Le fameux terme attachiante est la preuve de l'intériorisation de tout ça. Pourtant, l'inverse est d'autant plus vrai. Les féministes veulent se réapproprier la sexualité masculine : pouvoir baiser autant qu'elles veulent sans reproches. Mais ça aussi pose problème. 

La relation typique peut aussi être celle d'un homme qui ne parle plus, qui reçoit tout le soutien domestique, émotionnel et sexuel de la femme sans s'en rendre compte mais ne peut pas faire d'effort pour en fournir pour elle car il pense déjà en faire beaucoup. La femme pense détenir le pouvoir car c'est elle qui prend le décision, l'homme accepte de suivre pour être tranquille et demande la paix en retour mais aucun ne trouve son compte. Le sexe devient une tâche ménagère comme une autre. 

[[Wendy Langford]] _Revolution of the Heart_
Pseudo-indépendance = masculinité (se passer de relationel, tout pouvoir faire tout seul)
Pseudo-relationel = féminité (se passer soi-même sous silence)

Mais paradoxalement, ce sont les hommes qui manquent le plus d'introspection, les journaux intimes sont féminins. Elles parlent de leur sentiment aux autres femmes tandis que les homme ne disent rien de la relation envers eux-même et de leur relations envers les autres. Le plus gros problème qu'on les hommes est cette exacte chose. 

### Chapitre 4
Concept de love doll que les hommes enmportent partout pour prendre des photos, faire des diners, etc... Le [[Male Gaze]]. Dans les tableaux, le regard se veut masculin, tout est fait pour qu'un homme le voit. 5% des artistes étaient des femmes mais 85% des nus étaient des femmes. La puberté est ce moment où les gens autour regarde la fille comme un objet, pour ce qu'elle est et non qui elle est, et ce avant même que la fille comprenne les changements de son propre corps. Elle est dégoutée de son propre corps. La chirurgie pour avoir de plus gros seins réduit la sensibilité, on veut donner du plaisir à l'autre avant nous-mêmes. Alors que le confinement était l'occasion d'oublier toutes les pressions extérieures, les journaux et magazines continuaient d'asséner de la publicité sur la mode et les produits de beauté. Dans [[The Queen's Gambit]], la personnage principale est toujours bien coiffée, épilée, parfaite alors qu'elle fait des crises de stupéfiants.

_Le consentement_ de [[Vanessa Springora]]

Erika Lust : _Safe Word_ : https://lustcinema.com

Je n'ai pas compris le passage sur l'[[Histoire d'O]] p240.

## Citations
"Les relations amoureuses entre hommes et femmes ne sont pas les seules relations de dominations où le dominant et le dominé doivent s'aimer."
## Source
[[@Réinventer l'amour_ comment le patriarcat sabote les relations hétérosexuelles,Mona Chollet]]