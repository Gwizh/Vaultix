MOC : 
Source : [[1888, Jack l'Éventreur]]
Date : 2023-08-09
***

## Résumé
Livre sur la perception des gens de l'époque mais aussi des historiens et autres personnes qui se sont intéressées à cette histoire.

#### Transformation de la ville
71 % de la population anglaise vit en ville en 1881, 1/5 à Londres. Premiers sentiments d'étouffement et d'ivresse.
##### Rails
Avant de servir vers l'extérieur, les rails ont couvert de chantiers les villes, fragmenter et détruit les quartiers. Exiler les classes pauvres vers les quartiers les moins attrayants. ^fbc40f
##### Odeurs
Chevaux et leurs excréments. Porcs dans les villes. Abattoirs autour. 13000 animaux tués toutes les semaines à Londres en 1892. Pas de toilettes dans les logements. Odeur de sang et de déchets. 30/3000 maisons dans des conditions saines. 
##### Pollution de l'air
Consommation intensive de charbon. Sulfure et carbone hydraté. De littéraux gaz toxiques partout jusque dans les maisons. 1/4 des londoniens vivent dans l'indigence.
##### Sécurité 
Des vols absolument partout et tout le temps, c'est une ville considérée comme extrêmement dangereuse.
##### Les pauvres
Les pauvres sont vus comme des espèces à part, vivant dans des lieux si terrible qu'ils ont affectés leur patrimoine génétique. "Les rues sont pleines d'une race d'hommes nouvelle et différente, de taille plus réduite et d'apparence délabrée ou à l'air abruti par la bière."
Ils parlent d'une "immoralité fondamentale".
#### Mort et maladie
1880, découvertes de nombreux germes dans la ville. Plan de nettoyage. Épidémie qui se propagent dans les "courées" ouvrières, boueuses, sans soleil ni drainage ou pavage. [[Joseph Camberlain]] fait d'ailleurs de la [[Municipalité|municipalité]] le cadre d'un [[Notes/Socialisme|socialisme]] du mieux-être. "Socialisme municipal". Typhoïde, dysenterie, scarlatine, coqueluche, oreillons, tuberculose, apoplexie. Tabagisme et alcoolisme en constante croissance. Espérance de vie : 48 pour les hommes, 52 pour les femmes. Toujours les vieilles méthodes de soin comme la saignée dans certaines cliniques. Les pauvres ne vont pas voir de médecins : reste la charité.
1875 : "minimum sanitaire national" pour limiter tout ça.
Mort omniprésente dans tous les esprits.
#### Sexe
Réalité refoulée. Le sexe est sexiste, les femmes ne doivent en savoir rien et ne pas en parler. Les hommes n'en parlent pas tant que ça non plus.
[[Joséphine Butler]] et les féministes de l'époque étaient de tous les combats.
Toute défense de la liberté sexuelle a été l'objet de scandale. Le sexe n'était que pendant le mariage, dans ce cercle intime restreint. Beaucoup souhaitent la poursuite de la natalité en privilégiant des rapports plus libres. Une sorte d'hystérie prévaut dans le monde médical dès qu'on évoque la seule idée d'une éducation sexuelle.

##### Prostitution
"The workshop of the world gave way to the whoreshop of the world." En croissance, demande comme offre.
#### Couronne
La couronne est appréciée car elle semble modeste. La Reine [[Victoria]] est laide, discrète, vit dans une relative sobriété.

#### Littéralement ce combo !
##### Travail
[[Samuel Smiles]], le philosophe le plus lu du siècle.
De nombreux livres de conseil : Épargne, Caractère, Devoir.
[[Individualisme]] et pro concurrence.
##### Patrie
Caractère national des anglais selon eux-mêmes : humilité, sobriété (haha), absence de sentimentalisme tout plaisir personnel est une marque d'"indulgence envers soi-même" et est nécessairement coupable.
##### Famille
Homme chef du foyer. Femme considérée comme entre l'homme et l'enfant.
Mais féminisme. Premier personnage : [[Mary Wollestonecraft]].
De même pour les enfants avec notamment [[John Locke]], très inspiré de [[Jean-Jacques Rousseau]].
Pourtant, beaucoup considérait que l'éducation des enfants et surtout les affaires conjugales n'était pas du ressort de gouvernement. Les animaux ont eu des droits plus tôt que les enfants.
###### Femme
Idée de permettre à la femme de ne pas travailler pour la protéger.
En 1870, on leur reconnait le droit d'user librement de leurs revenus dans la limite de 200£.
##### Propriété
[[John Locke]] l'avait inscrit dans les droits naturels de l'homme et plus étroitement lié à la liberté, cette dernière n'existant que dans le respect de la première valeur. Supprimer la possibilité d'accumuler du capital c'est risquer de mettre fin au progrès.

#### Darwinisme social
[[Darwinisme Social]]
Un darwinisme social opportunément répandu dans les années 1880 dénonce la prolifération des faibles permise par le laxisme de la charité publique et privée.
En 1885, l'économiste [[Alfred Marshall]] dit :
" La charité et les réglementations sanitaires maintiennent en vie, dans nos  grandes villes, des milliers de personnes qui sérient morts il y a 50 ans. En même temps les forces économiques pèsent lourdement sur eux, car ils sont incapables d'accomplir d'autres taches que des travaux faciles et monotones, dont la plupart seraient aussi bien ou mieux faits par des machines ou des enfants. La charité publique ou privée peut alléger leur misère, mais le seul remède est de prévenir de tels gens de venir au monde... Les personnes de tout rang qui ne sont pas en bonne condition physique et mentale n'ont aucun droit moral d'avoir des enfants." ^f2d3f9

L'apogée de l'âge scientifique apporte ainsi de l'eau nouvelle au moulin des tenants de la hiérarchie sociale.
### Éducation
Éducation investie par les dirigeants pour créer un homme nouveau, créer très jeune l'idée de devoir citoyen. Respect des structures sociales, cadre familial et hiérarchie économique. Modèles économiques sont un pur hymne a l'effort, a l'esprit de compétition, a la collaboration des classes, a l'épargne et a tous les grands thèmes en vogue dans l'école libérale.

L'éducation passe aussi par le sport et l'emploi, qui participent a l'intériorisation des concepts libéraux.

### Des menaces a l'angoisse
J'aime bcp ce passage.
> "Il n'était pas possible , pourtant, de ne pas éprouver parfois l'angoisse de danser sur un volcan : l'image de l'éruption, celle de la lave populaire déferlant sur "la mince pellicule de civilisation", sont des constantes dans la bourgeoisie du XIXe."

G.S. Jones note en 1984 :
> "Les ouvriers de Londres étaient des "païens" ! La "civilisation" ne les avait pas encore atteints. La "lumière de la civilisation" ne brillait pas pour eux parce qu'ils habitaient dans les "coins obscurs", dans l'"ombre"."

Angoisse des classes supérieures a atteint un paroxysme dans les années 1880.
"On a connu alors la peur majeure de voir renversée les barrières contre le mal et on a frissonné devant les insuffisances de la défense de la civilisation."

L'administration se gonfle. Certains voient meme une état tentaculaire.

#### Perte de vitesse du modele anglais
Les innovations américaines commencent a mettre a mal l'Europe.
L'agriculture anglaise notamment perd face aux cow-boys
Les autres nations rattrapent le retard des anglais et n'ont plus besoin d'importer.

> "Curieusement, industriels et commerçants, au lieu de réagir et d'innover, s'enfoncent frileusement dans leurs traditions, font preuve d'un orgueilleux esprit de supériorité, se font battre par sur les marchés par plus entreprenants qu'eux-memes, s'endorment, au pire moment, dans un esprit rentier."

#### Contestation
[[Fédération sociale démocratique]] avec des greves importantes en 1886 et en 1887.
[[Deuxième Internationale]] en 1889 pour spice things up a bit.
Journée de 8 heure devient une revendication internationale.
En 1893, [[Keir Hardie]] devient une figure du socialisme anglais.

#### Littérature
La littérature de l'époque se contentait de dire que les classes populaires étaient malades ou malheureuses sans jamais expliquer pourquoi : exploitation capitaliste.

## Référence

## Liens 