MOC : 
Source : [[Port autonome de Douala - Bolloré perd définitivement le procès à Paris]]
Date : 2023-05-12
***

## Résumé
Dans un arrêt rendu le 10 janvier 2023, la Cour d’appel de Paris annule la sentence arbitrale partielle de la [[Chambre de commerce internationale]] (CCI) de Paris, rejette la demande d’indemnisation pour procédure abusive formulée par Douala international terminal (DIT), condamne DIT aux dépens et à payer au PAD une amende de 50 000 euros.

[[Bolloré Africa Logistics]] à travers DIT qui a géré le terminal à conteneurs du Port autonome de Douala pendant 15 ans (2004-2019), perd ainsi en appel le marché qu’une sentence arbitrale partielle de la CCI tendait à lui remettre après que le PAD l’a écarté du marché.

## Référence

## Liens 
https://actucameroun.com/2023/01/11/port-autonome-de-douala-bollore-perd-definitivement-le-proces-a-paris/