MOC : 
Source : [[Ligue Spartakiste]]
Date : 2023-04-07
***

## Résumé
La Ligue spartakiste (en allemand : Spartacusbund ou Spartakusbund, littéralement « Ligue Spartacus ») est un mouvement politique d’extrême gauche marxiste révolutionnaire, actif en Allemagne pendant la [[Première Guerre mondiale]] et le début de la révolution allemande de 1918-1919.

La Ligue spartakiste tire son nom de Spartacus, meneur de la plus grande rébellion d’esclaves de la République romaine. Ses principaux fondateurs sont [[Karl Liebknecht]] et [[Rosa Luxemburg]].

## Référence

## Liens 