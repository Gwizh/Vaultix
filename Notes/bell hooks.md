MOC : 
Source : [[bell hooks]]
Date : 2023-04-07
***

## Résumé
Une autrice féministe qui souhaite qu'on écrive son nom et prénon en minuscule car ce n'est pas sa personne qui importe mais son Oeuvre.

### Citations
"Il est beaucoup plus facile de parler de perte que de parler d'amour"

"Il ne faut pas penser l'amour comme un sentiment qui autorise toutes sortes de comportements, mais comme un ensemble d'actes."

## Référence

## Liens 
[[Réinventer l'Amour]]