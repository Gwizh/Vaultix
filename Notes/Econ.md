#Econ

[[Bro Leon]]

# Etapes du projet

## Comprendre l'économie de [[Victoria 3]]

### Avoir les formules des calculs des modificateurs
GPD : 
- [18 mars 2023](https://www.reddit.com/r/victoria3/comments/11uk2wn/12_gdp_calculation/jcpchnu/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button)
https://vic3calc.com/
https://github.com/xZylote/vic3calc/blob/master/src/calculator.ts
https://www.reddit.com/r/victoria3/comments/xwkkpt/formula_that_determines_the_price_of_goods/
https://www.reddit.com/r/victoria3/comments/10ij2vr/victoria_3_production_calculator/
https://forum.paradoxplaza.com/forum/threads/victoria-iii-profit-impact-calculation-in-construction-interaction-map-list-is-unhelpful.1553515/
https://forum.paradoxplaza.com/forum/threads/quick-questions-quick-answers.1550280/page-24#post-28585533
https://www.reddit.com/r/victoria3/comments/xwkkpt/formula_that_determines_the_price_of_goods/
https://github.com/Anbeeld/ARoAI/blob/main/src/common/script_values/aroai_weekly_loop_values.txt

### Tester plusieurs méthodes et modèles et noter le résultat

### Créer des mods pour tester des features du jeu ainsi que son modèle de simulation économique
[Modifiers](https://vic3.paradoxwikis.com/Modifier_types)
[Petit guide](https://www.youtube.com/watch?v=QEAC_KRt8cQ)

### Tester plus spécifiquement une forme anarchique du pouvoir et de l'économie

## Apprendre à utiliser les logiciels de simulation de l'économie
[Making An Economy SimulatorMaking An Economy Simulator](https://wbogocki.medium.com/making-an-economy-simulator-97bab95838fa)
[ESL](https://github.com/INET-Complexity/ESL)
[Virtonomics](https://virtonomics.com/) ?
[Avec Python](https://python.quantecon.org/geom_series.html)


## Trouver et comprendre des modèles économiques alternatifs (simples puis difficiles)
https://www.rethinkeconomics.org/2020/08/13/sraffian-economics/