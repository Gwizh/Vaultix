[[Livre]]
MOC : [[Histoire]] [[POLITIQUE]]
Titre : [[Figures du communisme (juste l'intro)]]
Auteur : [[Frédéric Lordon]]
Date : 2022-08-05
***

## Résumé

## Notes
La préhistoire c'est le développement matériel et productif, au point de changer grandement le mode de vie. Le SoL avait grandement augmenté et ils pouvaient donc aller plus loin. Parce que la seule préoccupation était le développement du matériel pour la survie, sans réel but.

Il n'y a pas de santé dans l'inquiétude matérielle. L'adaptation constante et la peur de l'effondrement n'est pas adaptée à la vie humaine. Il nous faut la tranquilité matérielle et pouvoir borner les ressources.

Le marché cherche la rareté, car qui dit rareté dit valeur. Mais doit-on chercher la rareté des biens pour le bien du marché ?

Le but selon lui du monde serait le développement des forces créatrices de tous.  Les Grecs ont pu faire de la philosophie car ils avaient des esclaves. Ils n'avaient plus de travail matériel à réaliser. Qui cultive son esprit cultive celui des autres. Remplacer l'argent par l'Oeuvre (art mais aussi science, savoir etc...). Appeler à la contribution de tous.

Média, trop souvent créateur de contenu et de consentement car victime de subordination. 

Mais comment on fait ? Il rejoint [[Bernard Friot]] sur le salaire à vie. 

Passage Usul :
	Chine et URSS était au contraire : Chercher la production et la compétition, maintenant plus par le privé mais par l'État. Les Russes ont fait leur Révolution trop tôt, ils n'avaient pas de système politique digne de ce nom. Quand le communisme c'est jsute l'utilisation d'une idéologie ce n'est pas le communisme. La décroissance va peut etre venir du manque de fertilité.

L'idée étant qu'on devrait être le moins subordonné possible, par le capitalisme comme par l'État.

Il y a un devenir marchand du monde.

Seul un dégagement politique fort peut sortir le capitalisme de nos vies : une révolution. Car la démocratie est implantée dans le capitalisme et le capitalisme est implantée dans la démocratie.

Il faut que les partis réformistes deviennent révolutionnaires.

Garanti économique générale : jamais à la rue ni mort de faim.

Passage Usul :
	Caisse d'investissement qui ne cherche pas nécessairement le profit, comment ? Grace au salaire, au GEG. 
	Le but c'est de savoir si qualitativement c'est utile. 

Donc deux idées : salaire pour tout le monde et abolution de la propriété lucrative. Il n'y aura plus d'incentive à chercher à battre les autres, à vivre mieux qu'eux ! Par contre il y a propriété d'usage, une maison peut etre vendue, tu peux laisser tes meubles a tes enfants.

La spotaneité n'existe pas, il faut un mode de production. Il y a organisation. 
L'exemple de la colloc mdr, je comprends bien que certains bossent plus que d'autres, donc nécessairement il y aura travail à faire. Il y aura de l'économie, du travail. Il faudra qu'on soit à la hauteur du monde qu'on a déjà. 

La précarité va contre l'écologie. La gauche aisée veut sauver la planete, la gauche pauvre veut manger. Retrouvaille entre fin du monde et fin du mois.

La monnaie fluidifie les échanges. On en aura besoin. Le problème c'est l'accumulation de celle-ci. 


## Notions clés

## Citations

## Passages amusants

## Références
