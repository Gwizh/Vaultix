MOC : 
Source : [[Les Français, plus «écosceptiques» que leurs voisins européens]]
Date : 2023-04-13
***

## Résumé
Selon le CEVIPOF, les contraintes gouvernementales pour protéger l’environnement sont moins bien acceptées en France qu’en Allemagne, en Angleterre et en Italie.

## Référence

## Liens 