MOC : 
Titre : [[Laura Lee Downs]]
Date : 2023-09-28
***

## Résumé
Laura Lee Downs enseigne à l'European University Institute où elle est professeure d'histoire du genre de 1789 à nos jours.

Directrice d’études au Centre de recherches historiques à l’EHESS, elle est l’auteur de _L’Inégalité à la chaîne. La division sexuée du travail dans l’industrie métallurgique en France et en Angleterre_ (Paris, Albin Michel, 2002), de _Childhood in the Promised Land : Working-class movements and the colonies de vacances in France, 1880-1960_ (Durham, Duke University Press, 2002), de _Writing Gender History_ (Londres, Hodder Arnold Press, 2004) et a dirigé, avec Stéphane Gerson, _Pourquoi la France ? Des historiens américains racontent leur passion pour l’Hexagone_ (Seuil, 2007).

## Références

## Liens 