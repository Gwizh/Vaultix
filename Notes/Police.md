MOC : 
Source : [[Police]]
Date : 2023-05-18
***

## Résumé
Le 14 février 2019, 438 eurodéputés s'alarment du "recours à des interventions violentes et disproportionnées par les autorités publiques lors de protestations et de manifestations pacifiques." Le 26 février 2019, le Conseil de l'Europe demande à la France de "suspendre l'usage du LBD dans le cadre des opérations de maintien de l'ordre" afin de "mieux respecter les droits de l'Homme". Le 6 mars 2019, l'ONU réclame une enquête sur l'" usage excessif de la force" lors de la répression des manifestations [[Gilets jaunes]]

## Référence

## Liens 