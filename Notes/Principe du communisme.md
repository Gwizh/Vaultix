[[Livre]]
MOC : [[Politique]] [[Communisme]]
Titre : [[Principe du communisme]]
Auteur : [[Friedrich Engels]]
Date : 1847
***

## Résumé
Explication des termes du communisme et des idées principales.
Donne le contexte historique et économique de l'avenement du capitalisme et du [[Prolétariat]].
[[Féodalité]] -> Manufacture -> Machinisme/ [[Capitalisme]]

En effet, seuls les capitalistes ont les moyens d'acheter les grosses machines et détiennent donc les moyens de production. Les prolétaires ne peuvent etre que serviteur de ces possédants.

Il y a aussi l'idée de la [[Division du travail]] qui permet d'encore plus rendre dépendant un travailleur de sa machine. De l'abtrutir et de retirer tout moyen d'épanouissement.

Il y aussi l'idée de [[Libre échange]]. Machines donnent produits moins chere qui créent concurrence déloyale et qui force l'industrialisation. 

Il y aussi des passages sur les différences entre esclaves / serfs et prolétaires, très intéressants.

Tout débouche sur la suppression de la [[Propriété privée]], comme un moyen de supprimer cette possession par les capitalistes. (Voir proudhon)

## Notions clés

## Citations

## Passages amusants

## Références
[[Pierre-Joseph Proudhon]]