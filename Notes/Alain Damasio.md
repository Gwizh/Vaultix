MOC : 
Source : [[A69]]
Date : 2023-04-23
***

## Résumé

### Liquidité de la commodité
https://www.youtube.com/watch?v=Ebd4BzxrkVQ
Superbe idée : la technologie et le contrôle que les gds plateformes ont sur nous la forme de liquide. C'est tellement facile de tomber dedans car elle réduit au plus possible l'effort qu'on doit y mettre. Il faut remettre en route une vitalité pour sortir de tout ça. Capitalisme crée un désir d'utiliser les outils mais il n'y a aucune obligation. Technologie comportementaliste pour te piéger dedans. Alain Damasio n'a pas de téléphone portable. Ton attention est complètement lié sur ton environnement. Se contraindre à l'effort. 

La zone du dehors : Transhumanisme, on y est déjà.
Smartphone : déjà une forme de transhumanisme.
Auto-servitude : on va acheter un smartphone avec les poignées d'applications.

Un des problèmes : libéralisme couplé au numérique. Toutes les données permettent de manipuler comme jamais.
Trump (un abruti qui faisait le clown) a beaucoup moins modifié la vie des américains que les grandes entreprises et ses technologies. 