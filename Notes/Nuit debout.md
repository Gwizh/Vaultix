### Revendications : Réformes concernant la démocratie, le travail et l'économie (retrait de la « [[Loi Travail]] », [[Altermondialisme]], [[Anti-capitalisme]], etc.)

Nuit debout est un ensemble de manifestations sur des places publiques, principalement en France, ayant commencé le 31 mars 2016 à la suite d'une manifestation contre la [[Loi Travail]].

Ce mouvement social est pluriel et cherche à construire une « [[Convergence des luttes]] ». Sa revendication initiale, le refus de la loi Travail, s'élargit à une contestation plus globale des institutions politiques et du système économique.

Créé à l’issue d’une réunion publique organisée par [[François Ruffin]], le mouvement fonctionne ensuite sans leader ni porte-parole. Il est organisé en commissions et les prises de décisions se font par consensus lors d'assemblées générales, suivant les principes de la [[Démocratie directe]].

Le mouvement s'étend sur une centaine de villes, certaines organisant des assemblées quotidiennes. L'affluence aux assemblées baisse à partir de fin mai 2016.

Il y a un lien direct avec le [[Mouvement Occupy]]