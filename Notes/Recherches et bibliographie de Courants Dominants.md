#Adam 

# Recherches
### Personnages illustres :
#### Royaume-Uni :
[[Sebastian Ziani de Ferranti]]
#### France :
[[Lucien Gaulard]]
[[Marcel Deprez]]
[[Hippolyte Fontaine]]
#### États-Unis :
[[Thomas Edison]]
[[Nikola Tesla]]
[[George Westinghouse]]

# Bibliographie
### Livres

#### Essais
[[1888, Jack l'Éventreur]]
[[Thomas Edison, L'homme aux 1093 brevets]]
[[Jules Ferry#Jules Ferry - Mona Ozouf|Jules Ferry]]
[[Positions politiques d'Émile Zola jusqu'à l'affaire Dreyfus]]
[[Electrical industry lag]]
[[Le Royaume-Uni de 1837 à 1914]]
[[1840-1914 : La République imaginée]]
[[Ferranti and the British electrical industry]]
#### Romans
[[L'Homme aux Lacets Défaits]]
[[Des Éclairs]]
[[Le Tour du Monde en 80 Jours]]

### Vidéos
#### Documentaires

#### Films
[[There Will Be Blood]]
