[[Film]]
MOC : 
Titre : [[La Cité de la peur]]
Auteur :
Date : 
Date de visionnage : 2022-07-29
***

## Résumé
Le film raconte comment au cours du festival de Cannes, une série Z d'horreur toute pourrie qui était destinée à tomber immédiatement dans l'oubli, est soudainement entouré de meurtres qui y font écho perpétré par un tueur masqué équipé d'un marteau et du faucille. La star du film [[Dominique Farrugia]] va se voir assigner un garde du corps [[Alain Chabat]]. 

Quant à la réalisatrice du film [[Chantal Lauby]], elle va jouer sur ces meurtres pour gagner en popularité et faire sa star au festival.





## Faits importants

## Acteurs

## Référence

## Liens 
Dans l'ordre : 
<-- [[Le trou]]
--> [[Asterix et Obélix - mission Cléopâtre]]