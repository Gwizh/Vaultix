MOC : 
Titre : [[Hégésippe Jean Légitimus]]
Date : 2023-10-04
***

## Résumé
Hégésippe Jean Légitimus (1868-1944), est un homme politique français. Il est député, conseiller général et maire de Pointe-à-Pitre. Fondateur principal du mouvement socialiste de la [[Guadeloupe]], il a marqué fortement la vie politique française au début du XXe siècle où on le surnomme le « Jaurès Noir »1. Il fut le premier Noir à siéger à l'assemblée parlementaire depuis 1848 aux côtés de [[Jules Guesde]], [[Jean Jaurès]] et [[Léon Blum]], dont il fut l'ami. Il fut l'un des plus jeunes députés français, âgé de 30 ans au moment de son élection. 

## Références

## Liens 