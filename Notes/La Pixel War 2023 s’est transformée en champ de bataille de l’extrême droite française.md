MOC : 
Source : [[La Pixel War 2023 s’est transformée en champ de bataille de l’extrême droite française]]
Date : 2023-07-28
***

## Résumé
L’édition 2023 de Pixel War s’est achevée par une œuvre d’art virtuelle, instantané des préoccupations des internautes du monde entier. Mais cette année, sur les réseaux français, la fachosphère s’est mobilisée pour imposer ses thèmes et empêcher tout hommage à [[Nahel]]. 

Dans le même temps, sur le serveur bonapartiste nommée « La Grande Armée » que nous avons consulté, les internautes s’organisent et demandent l’aide du mouvement [[Génération Z]] d’[[Éric Zemmour]]. Le 23 juillet à 3 heures du matin, un internaute écrit : « Demain nous aurons le soutien de Reconquête (je voulais pas faire de politique au début mais c’est les seuls qui peuvent nous aider maintenant […]. » Plusieurs centaines de militant·es affluent, gonflant les rangs des bonapartistes et écrasant rapidement l’hommage au jeune Nahel à coup de croix gammées et de pixels désordonnés. Le groupe finira par remercier le parti politique d’extrême droite à la fin de l’événement.

Comme Mediapart l’écrivait en octobre, la mort de cette jeune fille blonde a permis au parti d’extrême droite de relancer son concept fumeux de « francocide », qu’Éric Zemmour avait tenté de lancer en septembre. « Le tabassage, le viol, le meurtre, l’attaque au couteau d’un Français ou d’une Française par un immigré ne sont pas un fait divers. Pas plus un fait divers que le meurtre d’une femme par son mari. C’est un fait politique, que j’appellerai désormais francocide », avait-il alors affirmé.

## Référence

## Liens 