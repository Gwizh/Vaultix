[[Note Permanente]]
MOC : [[ECOLOGIE]] [[POLITIQUE]] [[Noz/ECONOMIE]]
Titre : [[Décroissance]]
Lien : https://fr.wikipedia.org/wiki/D%C3%A9croissance
Date : 2022-07-08
***

## Résumé
La décroissance est un concept à la fois politique, économique et social qui prône la réduction de la consommation. Né dans les années 1970, il s'appuie sur l'idée que la croissance économique (mesurée par des macro-indicateurs tels que le produit intérieur brut (PIB) ou le niveau de population) ne garantit pas (voire contrecarre) l'amélioration des conditions de vie de l'humanité. 

## Notions clés
Selon les acteurs du mouvement de la décroissance, le processus d'industrialisation a trois conséquences négatives : 
- Des dysfonctionnements de l'économie (chômage de masse, précarité, etc.)
- L'aliénation au travail (stress, harcèlement moral, multiplication des accidents, etc.) 
- La pollution, responsable de la détérioration des écosystèmes et de la disparition de milliers d'espèces animales. 

Partant de l'axiome selon lequel, dans un monde fini, une croissance illimitée est impossible, les « décroissants » (ou « objecteurs de croissance », même si certains considèrent ces deux dénominations comme différentes) se prononcent pour une éthique de la simplicité volontaire. Concrètement, ils invitent à réviser les indicateurs économiques de richesse, en premier lieu le PIB, et à repenser la place du travail dans la vie, pour éviter qu'il ne soit aliénant, et celle de l'économie, de sorte à réduire les dépenses énergétiques et ainsi l'empreinte écologique. Leur critique s'inscrit dans la continuité de celle du productivisme, amorcée durant les années 1930 et qui dépasse celle du capitalisme et celle de la société de consommation, menée pendant les années 1960. 

Intitulé _The Limits to Growth_ (_[Les Limites à la croissance](https://fr.wikipedia.org/wiki/Les_Limites_%C3%A0_la_croissance "Les Limites à la croissance")_), le premier rapport, dit _rapport Meadows_, (il y en aura trois en tout) sert de véritable déclencheur au mouvement de la décroissance. Il est publié en mars 1972, trois mois à peine avant la première [Conférence des Nations unies sur l'environnement](https://fr.wikipedia.org/wiki/Conf%C3%A9rence_des_Nations_unies_sur_l%27environnement_de_Stockholm "Conférence des Nations unies sur l'environnement de Stockholm"), à [Stockholm](https://fr.wikipedia.org/wiki/Stockholm "Stockholm") (qui se déroule du 5 au 16 juin). On parle alors de croissance zéro. Il constitue en effet la première étude conséquente soulignant les dangers engendrés par la [société de consommation](https://fr.wikipedia.org/wiki/Soci%C3%A9t%C3%A9_de_consommation "Société de consommation"). Traduit en trente langues, il est édité à douze millions d'exemplaires[5](https://fr.wikipedia.org/wiki/D%C3%A9croissance#cite_note-5).

Les modalités du processus industriel étant considérées par les décroissants comme à l'origine de la [crise écologique](https://fr.wikipedia.org/wiki/Crise_%C3%A9cologique "Crise écologique"), l'économie devient le champ de multiples questionnements, dont les [conférences internationales](https://fr.wikipedia.org/wiki/Conf%C3%A9rence_des_Nations_unies_sur_l%27environnement_et_le_d%C3%A9veloppement "Conférence des Nations unies sur l'environnement et le développement"), les [Sommets de la terre](https://fr.wikipedia.org/wiki/Sommet_de_la_Terre "Sommet de la Terre") et les [Conférences des parties](https://fr.wikipedia.org/wiki/Conf%C3%A9rence_des_parties "Conférence des parties") (comme la [COP21](https://fr.wikipedia.org/wiki/Conf%C3%A9rence_de_Paris_de_2015_sur_les_changements_climatiques "Conférence de Paris de 2015 sur les changements climatiques")) sont le théâtre.

Cinq pistes sont essentiellement explorées :

-   le sens et la place du travail ;
-   la pertinence des indicateurs de richesse, notamment le [PIB](https://fr.wikipedia.org/wiki/Produit_int%C3%A9rieur_brut "Produit intérieur brut") ;
-   la pertinence de la notion de [développement durable](https://fr.wikipedia.org/wiki/D%C3%A9veloppement_durable "Développement durable"), très majoritairement plébiscitée comme réponse à la crise écologique ;
-   la question des équilibres entre pays riches (industrialisés) et les pays pauvres (« en voie de développement ») ;
-   la nécessité de fusionner économie et écologie.

Les partisans de la décroissance reprochent au produit intérieur brut de se focaliser sur le quantitatif sans intégrer le qualitatif : il repose exclusivement sur des valeurs de marché et ne tient pas compte du bien-être des populations ni de l'état des écosystèmes. De surcroît, les décroissants reprochent au PIB de ne pas rendre compte de l'épuisement du stock des matières premières et de ne pas intégrer les dépenses occasionnées par la destruction du biotope. Ils privilégient d'autres indicateurs, tels que l'Indice de développement humain, l'[empreinte écologique](https://fr.wikipedia.org/wiki/Empreinte_%C3%A9cologique "Empreinte écologique") ou l'[indice de santé sociale](https://fr.wikipedia.org/wiki/Indice_de_sant%C3%A9_sociale "Indice de santé sociale").

Les partisans de la décroissance affirment que la recherche d’une évaluation de l’évolution des richesses, liée aussi bien à des besoins politiques que scientifiques, a conduit les économistes à créer des indicateurs ne prenant en compte que les aspects mesurables des richesses qui sont unifiées à travers leur équivalence monétaire. Les tenants de la décroissance arguent que la mesure du PIB est une mesure abstraite ne prenant pas en compte le bien-être des populations ni la pérennité des écosystèmes.

D'autres tenants de la décroissance font valoir que l'expression « développement durable » « devrait à elle seule susciter [la] perplexité, sinon [le] scepticisme » et font observer que les industriels américains et français ont très vite adhéré à ce concept et y ont vu « un accélérateur de croissance pour les entreprises » (slogan du Medef en 2009)



## Références

## Liens 

## A voir
- _Simplicité volontaire et décroissance_ de Jean-Claude Decourt : [2007](http://utopimages.fr/simplicite-volontaire-et-decroissance-1/) [[archive](https://archive.wikiwix.com/cache/?url=http%3A%2F%2Futopimages.fr%2Fsimplicite-volontaire-et-decroissance-1%2F "archive sur Wikiwix")], [2009](http://utopimages.fr/simplicite-volontaire-et-decroissance-2/) [[archive](https://archive.wikiwix.com/cache/?url=http%3A%2F%2Futopimages.fr%2Fsimplicite-volontaire-et-decroissance-2%2F "archive sur Wikiwix")], [2010](http://utopimages.fr/simplicite-volontaire-et-decroissance-3/) [[archive](https://archive.wikiwix.com/cache/?url=http%3A%2F%2Futopimages.fr%2Fsimplicite-volontaire-et-decroissance-3%2F "archive sur Wikiwix")].

- ==_The Entropy Law and the Economic Process_  de Nicholas Georgescu-Roegen==

- ==_La décroissance_ de Serge Latouche==
- https://fr.wikipedia.org/wiki/Haute_qualit%C3%A9_environnementale
- https://fr.wikipedia.org/wiki/Bio%C3%A9conomie
- https://fr.wikipedia.org/wiki/Simplicit%C3%A9_volontaire
- https://fr.wikipedia.org/wiki/Sobri%C3%A9t%C3%A9_%C3%A9conomique