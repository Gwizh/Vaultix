MOC : 
Source : [[Jean-Luc Mélenchon]]
Date : 2023-04-10
***

## Résumé
Répondant à [[Frédéric Lordon]] sur la possibilité d'une guerre financière après sa potentielle élection.
https://www.youtube.com/watch?v=OXhC9042YRg

En 2012, [[Jean-Luc Mélenchon]] dit être contre l'[[Accélérationnisme]]. Il n'est pas partisan du pire, celle qui dit que seules les crises peuvent faire survenir le meilleur. 

### "Quelle objection opposer au mariage homosexuel ? Aucune."
Famille et droits de l'enfant, Société
28/02/2012
Source : extrait d’une interview donnée par Jean-Luc Mélenchon au magazine La Vie le 23 janvier 2012
“La Vie : Le programme du Front de gauche, c’est « l’humain d’abord ». N’y a-t-il pas une contradiction à être antilibéral sur le plan économique, mais libéral sur les questions de mœurs ? À être pour le mariage et l’adoption homosexuels ?
Jean-Luc Mélenchon : L’orientation sexuelle n’est pas un choix. Elle a même posé de grandes difficultés aux homosexuels compte tenu de l’opposition de la société sur la base de préjugés tels que : « l’homosexualité est un choix individuel » ou « un vice ». Ils assument leur situation et pensent que les amours sont égales en dignité. Dès lors que des gens s’aiment, ils doivent pouvoir vivre ensemble normalement. En 1988, le hasard de la politique m’a mené dans une réunion avec une association appelée Gays pour la liberté. Les membres m’avaient interpellé sur les drames affreux que certains vivaient à la mort de leur compagnon. C’est pourquoi j’ai déposé le premier texte de loi sur le partenariat civil, devenu le pacs. J’ai répondu à un appel humain. Ces gens s’étaient choisis d’amour. Quand des homosexuels ont commencé à me parler de mariage, ça m’a bien fait sourire, car je ne suis pas partisan du mariage pour moi-même ! Ce n’est pas pour le proposer aux autres ! Mais raisonnablement, quelle objection opposer au mariage des homosexuels ? Aucune. La communauté humaine a intérêt à ce que les démarches d’amour soient reconnues et validées par elle.”

### "Il n'y a pas de contradiction entre le communisme en tant que doctrine et l'écologie"
Léa Salamé : "Je suis pas sûre sûre. Je suis pas sûre sûre..."
https://youtu.be/kcMMZ7QRnY8?t=791
Il dit qu'il est né à une époque où la gauche c'était la production pour nourrir tout le monde, contraire donc à l'écologie et la sobriété. Mais petit à petit, il a vu la crise climatique et à renouer avec les premiers grands pincipes du communisme et de la gauche qui sont que : les hommes sont égaux et qu'il y a sur terre des biens communs. 

## Référence

## Liens 