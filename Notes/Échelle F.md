MOC : 
Source : [[Échelle F]]
Date : 2023-04-24
***

## Résumé
9 valeurs gardées : 
- Conventionalisme : 
- Soumission à l'autorité
- Agressivité autoritaire : punir les personnes qui ne respectent pas les lois et les valeurs conventionelles. Accepter la violence quand celle-ci vient des autorités. 
- Anti-intraception : le fait de rejeter ce qui provient de l'imagination, de la sensibilité, à la tendresse, aux sentiments ainsi qu'à toute introspection. Pas de retour sur soi-même, pas de recul. Cela peut s'accompagner d'un rejet des sciences humaines et sociales. 
- Superstition et Stereotypie: croire en des composantes mystiques ou des stéréotypes simples pour expliquer le monde. 
- Pouvoir et domination : toujours catégoriser les choses comme avec des dominants et des dominés, des meneurs et des suiveurs. 
- Destructivité et cynisme: Croyance que l'humanité est mauvaise par nature. "L'humanité étant ce qu'elle est : il y aura toujours des guerres ou des conflits."
- Projectivité : l'individu va croire voir dans le monde plein d'exemples et de preuves de sa pensée. 
- Sexualité : Préoccupations exagérées concernant la sexualité. 

Il faut lier les différents points entre eux : un fort conventionalisme peut être réfléchi et ne pas venir d'une soumission à l'autorité. 

Finalement seul 3 critères seront gardés par [[Bob Altemeyer]] : l'agressivité autoritaire, la soumission à l'autorité et le conventionalisme. 

#MIE Il y a un point vraiment important. Être autoritaire n'est pas que se croire supérieur aux autres et dire qu'on est pas un mouton. C'est aussi souvent faire partie d'un groupe qui adhère aux mêmes opinions et de respecter les gens qui les énoncent. La soumission à l'autorité est un comportement autoritaire car on respecte sans réfléchir un groupe et on rejette tout le reste. C'est aussi la définition d'[[Ethnocentrisme]].

#MIE Un point revient souvent, l'idée qu'on peut tout faire si on a suffisamment de force et de volonté. C'est une idée qui n'est pas autoritaire en soi mais qui est tout de même très lié à l'autoritarisme. En effet, on ne voyant que des causes liées à l'individu, on devient aveugle à l'environnement socio-culturel. Croire Qu'on peut tout accomplir par la volonté c'est aussi croire que les gens qui n'ont pas réussi sont fainéant, manquent de force et sont fautifs. Ceux qui ont mieux réussis sont supérieurs et il semble alors logique de leur faire confiance et de les écouter. Ça justifie donc un respect de la hiérarchie. Si une personne n'y arrive pas mais pense être fort et capable, elle va croire qu'un élément extérieur vient l'en empêcher. Cet élément extérieur peut être trouvé dans les membres extérieurs au groupe ou dans du complotisme, ou les deux. 

## Référence

## Liens 