[[Film]]
MOC : 
Titre : [[L'armée des ombres]]
Auteur :
Date : 1969
Date de visionnage : 2022-07-28
***

## Résumé
C'est un film complexe qui se veut comme une succession de moments montrant les actions des résistants français. On assiste à des fuites, des courses poursuites, des exécutions et autres moments horribles.

On ne voit pas vraiment des actions actives mais plutôt réactives.  

## Faits importants
- Tres peu de musique, ça rend vraiment compte de l'ambiance de l'époque. 
- Mathilde est vraiment le meilleur personnage du film, de très loin. 
- Très lent, avec beaucoup de tension. 
- la scène du mur est vraiment incroyable. 
- L'exécution du traitre fait vraiment mal, bien que l'émotion n'est pas si prenante.

## Acteurs
[[Lino Ventura]] : Philippe Gerbier
[[Simone Signoret]] : Mathilde
[[Paul Meurisse]] : Luc Jardie
[[Jean-Pierre Cassel]] : Jean-François Jardie
[[Paul Crauchet]] : Félix Lepercq


## Référence

## Liens 
Dans l'ordre : 
<-- [[Le Fabuleux Destin d'Amélie Poulain]]
--> [[Léon]]