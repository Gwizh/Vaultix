MOC : 
Source : [[Nicolas Sansu]]
Date : 2023-05-02
***

## Résumé
Le député communiste Nicolas Sansu a été condamné à deux mois de prison avec sursis et à une suspension de quatre mois de son permis de conduire. Il devra indemniser trois policiers, après les avoir insultés alors qu'il conduisait en état d'ivresse.

## Référence

## Liens 
[[Parti communiste français]]
[[Alcoolisme]]