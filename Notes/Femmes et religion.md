MOC : 
Source : [[Femmes et religion]]
Date : 2023-06-05
***

## Résumé

L'idée que c'est la mère est responsable de la déviance des enfants. Si elle va travailler ou si elle ne fait pas ce qu'elle doit faire c'est de leur faute si ça ne va pas. C'est le rôle divin de la femme de s'occuper des enfants. 

Quand on meurt, les blessures se guérissent. Une bonne idée mais quand on réfléchit, ça guérit aussi l'homosexualité et la transexualité. Ça mène à des suicides. 

Il y a des entretiens avec les évêques qui posent des questions aux jeunes femmes sur le respect de la chasteté. Jeunes femmes à partir de 12 ans hein. Parler de masturbation ou d'actes sexuels pour savoir si les enfants les faisaient. En plus, l'évêque est sensé avoir le discernement de Dieu et donc peut différencier mensonges et vérités. 

On peut pas refuser un date en tant que femme, on est obligé d'y aller. En effet, refuser c'est mettre de la mauvaise volonté pour le marriage et donc ne pas respecter le rôle divin. 

## Référence

## Liens 