[[Note Permanente]]
MOC : [[Histoire]] [[POLITIQUE]]
Source : [[Élection présidentielle française de 1958]]
Date : 2022-07-08
***

## Résumé
L'élection présidentielle française de 1958, visant à élire le président de la République française et de la Communauté, est la première élection présidentielle qui intervient en France après l'adoption de la [[Constitution de la Ve République]] instituant la [[Cinquième République]]. Elle se tient le 21 décembre 1958.

Conformément à la version de la Constitution alors en vigueur, le chef de l’État est élu par un collège électoral de près de 82 000 grands électeurs, composé des parlementaires, des conseillers généraux et de représentants des conseils municipaux.

[[Charles de Gaulle]], devenu président du Conseil après la crise de mai 1958, en pleine [[Guerre d'Algérie]], l’emporte dès le premier tour de scrutin avec **78,5 % des suffrages** exprimés face au communiste Georges Marrane et au divers gauche Albert Châtelet. Le général de Gaulle prend ses fonctions le mois suivant et fera voter, en 1962, l’instauration du suffrage universel direct pour l’élection présidentielle. 

## Référence
[[Histoire de la France au XXe Siècle - Livre 3]]

## Liens 