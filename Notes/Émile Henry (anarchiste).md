MOC : 
Source : [[Émile Henry (anarchiste)]]
Date : 2023-07-07
***

## Résumé
Émile Henry (Barcelone, 26 septembre 1872 - Paris, 21 mai 1894) est un anarchiste et criminel français, guillotiné pour avoir commis plusieurs attentats, dont le dernier visait les clients d'un café. 

## Référence

## Liens 
[[Anarchie]]
[[Lois scélérates]]