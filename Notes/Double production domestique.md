MOC : [[Sociologie des techniques]]
Type : Note littéraire
Date : 202311081629
Tags : #Fini
***

# Double production domestique
p95
Certains outils et machines permettent une double production domestique. Il y a la production liée à la réparation des biens domestiques et la production de biens pour la vente extérieure. C'est une idée centrale du socialisme même si ce n'est pas vraiment facile à le percevoir. C'est la production par les masses pour les masses. Cela permet l'[[Autonomie|autonomie]] des masses. On compte parmi les outils et machines les plus importantes la faucille, le marteau, la machine à coudre, le rouet, le four, le poste de soudure, etc...
[[Production autonome par les masses et pour les masses]]

## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]