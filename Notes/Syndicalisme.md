Titre : [[Syndicalisme]]
Type : Noeud
Tags : #Noeud
Ascendants : [[Notes/Socialisme|Socialisme]]
Date : 2023-06-11
***

## Propositions
En 1884 la [[Loi relative à la création des syndicats professionnels]] de [[Pierre Waldeck-Rousseau]] permet un essor du syndicalisme en France et aussi les alliances des syndicats. 

En 1886, il y a la création de la fédération nationale des syndicats. Influencés par [[Jules Guesde]]. Dans les bourses du travail on a aussi des syndicats qui eux étaient plus d'influence [[Anarchie]]. En 1995, la fusion des deux devient la [[CGT]]. 
## Sources

