[[Note Permanente]]
MOC : [[Histoire]] [[POLITIQUE]]
Titre : [[Guerre d'Algérie]]
***

## Résumé

## Chronologie
Tiré de [[Mediapart]]

| Date              | Événement                                                                                                                                                              |
| ----------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1 Novembre 1954   | **Début de la rébellion en Algérie**                                                                                                                                       |
| 3 Avril 1955      | Loi créant et instituant l'état d'urgence en Algérie                                                                                                                   |
| 31 Janvier 1956   | Formation du gouvernement de Guy Mollet                                                                                                                                |
| 12 Mars 1956      | L'Assemblé Nationale vote la loi sur les pouvoirs spéciaux pour l'Algérie                                                                                              |
| 30 Septembre 1956 | **Premiers attentats à la bombe du Front de Libération Nationale (FLN) à Alger**                                                                                           |
| 22 Octobre 1956   | Détournement par les services secrets français d'un avion civil transportant plusieurs responsables indépendantistes, dont le futur président algérien Ahmed Ben Bella |
| 13 Novembre 1956  | Le général Salan devient commandant en chef de l'Algérie                                                                                                               |
| 24 Décembre 1956  | Découverte par Paul Teitgen, secrétaire général de la préfecture d'Alger, d'un complot visant à l'instauration d'une dictature militaire en Algérie                    |
| 7 Janvier 1957    | Arrêté du préfet d'Alger confiant au général Massu les pouvoirs de police à Alger                                                                                      |
| 5 Mars 1957       | Assassinat de Larbi Ben M'hidi, leader FLN arrêté par les militaires français le 25 février                                                                            |
| 23 Mars 1957      | "Suicide" de l'avocat indépendantiste Ali Boumendjel                                                                                                                   |
| 21 mai 1957       | Chute du gouvernement de Guy Mollet                                                                                                                                    |
| 8 Juin 1957       | **Attentat du FLN au casino de la Corniche (8 morts)**                                                                                                                     |
| 11 Juin 1957      | Arrestation de Maurice Audin                                                                                                                                           |
| 12 Septembre 1957 | Demission de Paul Teitgen                                                                                                                                              |
| 24 Septembre 1957 | Arrestation de Yacef Saadi, responsable du FLN                                                                                                                         |
| 27 Mars 1958      | Saisie de La Question d'Henri Alleg                                                                                                                                    |
| 4 Juin 1958       | Première visite du Général de Gaulle à Alger                                                                                                                           |
| 19 Septembre 1958 | Le Gouvernement provisoire de la République algérienne (GPRA), dirigé par Ferhat Abbas est formé au Caire                                                              |
| 8 Janvier 1959    | **Naissance officielle de la Ve République.** Charles de Gaulle est président. Formation du Gouvernement de Michel Debré.                                                  |
| 16 Septembre 1959 | Le général de Gaulle proclame le principe d'autodétermination en Algérie                                                                                               |
| 19 Janvier 1960   | Rappel à Paris du général Massu, qui a critiqué le chef de l'État dans la presse                                                                                       |
| 4 Novembre 1960   | Le général de Gaulle emploie pour la première fois l'expression "République Algérienne"                                                                                |
| 31 Mars 1960      | **Assassinat du maire d'Évian par l'Organisation de l'armée secrete (OAS)**                                                                                                |
| 22 Avril 1960     | Putsch à Alger par les généraux Challe, Zeller, Jouhaud et Salan                                                                                                       |
| 25 Avril 1960     | Effondrement du putsch                                                                                                                                                 |
| 17 Octobre 1961   | **Manifestation pacifiste d'Algériens à Paris réprimée dans le sang (plus de 100 morts)**                                                                                  |
| 8 Février 1962    | **Manifestations contre l'OAS, 8 personnes meurent au métro Charonne à Paris**                                                                                             |
| 18 Mars 1962      | **Les accords d'Évian mettent un terme à la Guerre d'Algérie**                                                                                                             |
| **5 Juillet 1962**    | **Indépendance de l'Algérie**                                                                                                                                                                       |

## Référence
- [[De Gaulle et la guerre d’Algérie - dans les nouvelles archives de la raison d’État]]
- [[Histoire de la France au XXe Siècle - Livre 3]]
- [[FLN]]
