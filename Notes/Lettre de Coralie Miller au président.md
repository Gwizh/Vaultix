Monsieur le président,

J’ai voté pour vous. Et toujours malgré moi. Je ne voulais pas de vous comme président, mais j’ai serré les dents. Deux fois. J’ai regardé mes enfants, j’ai regardé mon pays, j’ai regardé notre histoire. Et j’ai fait ce que je considérais être mon devoir.

La suite après la publicité

Depuis mes 18 ans, sur cinq élections présidentielles, j’ai participé à trois barrages. A trois reprises, j’ai dû, comme des millions d’électrices et d’électeurs de gauche, taire mes convictions culturelles, mes inquiétudes économiques, mes colères sociales, parce que seule comptait face à l’urne l’impérieuse nécessité d’empêcher l’extrême droite de prendre le pouvoir. Protéger la République, réaffirmer la démocratie, laisser tomber les clivages et les idéologies pour que jamais le pire n’arrive.

_« Plus jamais ça »_, me disait-on dans mon enfance, alors que l’on me racontait l’histoire de ma famille, juive, déportée, exterminée, parce que l’extrême droite, un jour de 1933, dans un pays voisin, _le 30 janvier 1933, Hitler était nommé chancelier en Allemagne, NDLR était arrivée au pouvoir démocratiquement. Moi, le front républicain, j’y croyais. Comme des millions d’électrices et d’électeurs de gauche, je suis faite de ce bois que vous ne semblez pas connaître : je me suis effacée pour l’intérêt général. Je me suis effacée parce qu’il était alors entendu qu’entre la droite et l’extrême droite, il valait mieux toujours la droite. Oui, monsieur le président, vous avez beau nous jouer du _« en même temps »,_ pour moi comme pour des millions d’électrices et électeurs de gauche, vous êtes de droite. Mais j’ai répondu présente et j’ai voté pour vous. Deux fois. En 2017, j’ai fait barrage. En 2022, j’ai fait barrage.

_« Ce vote m’oblige pour les années à venir »,_ avez-vous dit à notre intention, le soir de votre deuxième victoire. L’avez-vous pensé seulement une seconde ? Permettez-moi d’en douter. Car un mois après ce discours, vous commenciez votre travail de sape contre nous. Piétinant une grande partie de vos propres électrices et électeurs, vous aviez choisi votre plan de bataille : renvoyer dos à dos _« les extrêmes »_. Ainsi, ceux-là mêmes à qui vous promettiez la concorde pour avoir battu l’extrême droite à vos côtés, étaient brutalement mis sur le même pied… que l’extrême droite.

Ce n’était que le début. La gauche est rapidement devenue votre bouc émissaire, votre ennemie publique numéro 1. Jusqu’à ce que soit commis l’irréparable : deux vice-présidents RN élus à 280 voix. C’est-à-dire avec celles de votre majorité. Pourtant, j’avais fait barrage.

Depuis près d’un an, avec l’aide de vos ministres et de vos députés, vous avez continué, patiemment, méticuleusement, expulsant de l’arc républicain celles et ceux qui trois fois l’ont forgé. Aujourd’hui, vous criminalisez la moindre de nos contestations. Vous nous accusez de menacer les institutions. Aujourd’hui, vous laissez votre ministre de l’Intérieur parler du « _terrorisme intellectuel de l’extrême gauche »_ pour désigner l’opposition, traiter toute personne issue de la gauche et des écologistes comme un ennemi intérieur potentiel tout en niant la violence d’extrême droite qui sévit dans vos villes et villages, et – ça vient de tomber – menacer le financement de la Ligue des droits de l’Homme parce qu’elle documente l’usage de la force par la police en manifestations. Pourtant j’avais fait barrage.

Votre trahison, je l’ai ressentie dans ma chair le soir du 17 février dernier, quelques minutes après minuit, alors que les débats portant sur la réforme des retraites venaient de s’achever à la faveur de l’article 47.1, sous les huées de la Nupes. Il se trouve, monsieur le président, que je suis documentariste et que ce jour-là, j’étais avec ma caméra à suivre les débats, installée au Guignol – joli nom désignant un des espaces réservés à la presse, qui raconte le spectacle qu’offrent les tribuns du palais Bourbon. Joli nom. Sale moment.

Je ne pensais pas qu’un jour je pleurerais de rage dans l’hémicycle en entendant « la Marseillaise ». Je ne pensais pas ce soir-là qu’en tournant mon regard et mon objectif vers le chant qui s’élevait contre la gauche quittant ses bancs, j’allais assister au spectacle de la honte : la majorité présidentielle – ministres, députés, présidente de l’Assemblée nationale – votre majorité dans sa totalité, debout, chantant notre « Marseillaise » à l’unisson avec l’extrême droite. RN et Renaissance, accompagnés de la droite supposée républicaine, main dans la main contre la Nupes, clamant le chant de la République. Puis applaudissant. Comme un seul homme.

La « Marseillaise » à l’[[Assemblée nationale]] le soir du 17 février. (CORALIE MILLER / DAM PROD)

Pourtant j’avais fait barrage.

Monsieur le président, jusqu’où irez-vous ? Si réellement c’est la France que vous souhaitez protéger, est-ce que vous arrêterez un jour de faire de l’extrême droite votre marchepied ? A ce jeu, vous perdrez. Et nous tous avec vous. Est-ce que c’est là votre projet ?

Et vous, marcheurs et marcheuses « de gauche », parmi lesquels une partie de ma famille et de mes amis… où êtes-vous, exactement ? Vous avec qui j’ai partagé un idéal et des valeurs, vous qui m’avez appelée à la rescousse pour faire front contre front, je vous en conjure : ne vous rendez pas complices par votre silence gêné. J’ai fait barrage. Maintenant c’est à vous.

Par [Coralie Miller](https://www.nouvelobs.com/journalistes/902/coralie-miller.html)
autrice et documentariste