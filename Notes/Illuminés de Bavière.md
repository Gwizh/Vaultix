MOC : 
Source : [[Illuminés de Bavière]]
Date : 2023-06-02
***

## Résumé
Les Illuminés de Bavière (ou parfois Illuminati de Bavière) (en allemand Illuminatenorden) sont une société secrète allemande du xviiie siècle qui se réclamait de l'[[Aufklärung]] et plus généralement de la philosophie des [[Lumières]].

Fondée le 1er mai 1776 par le philosophe et théologien Adam Weishaupt à Ingolstadt, elle eut à faire face à des dissensions internes avant d'être interdite par un édit du gouvernement bavarois en 1785 et de disparaître peu après.

De nombreux mythes et théories du complot ont prétendu que l'ordre survécut à son interdiction et qu'il serait responsable, entre autres, de la Révolution française, de complots contre l'Église catholique romaine ainsi que de la constitution du nouvel ordre mondial.

## Référence

## Liens 