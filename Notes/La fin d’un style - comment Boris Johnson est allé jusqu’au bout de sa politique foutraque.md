[[Article]]
MOC : [[POLITIQUE]]
Titre : [[La fin d’un style - comment Boris Johnson est allé jusqu’au bout de sa politique foutraque]]
Auteur : [[L'Opinion]]
Date : 2022-07-07
Lien : https://www.lopinion.fr/international/la-fin-dun-style-comment-boris-johnson-est-alle-jusquau-bout-de-sa-politique-foutraque
Fichier : 
***

## Résumé
Seul contre tous, ou presque, le Premier ministre britannique passe la main. Il devrait rester en poste jusqu’à ce qu’un nouveau chef du parti conservateur soit élu. A moins que les Tories n’imposent un Premier ministre intérimaire pour éviter encore plus de casse.

Hormis son poste en tant que maire de Londres, il n’a été qu’un médiocre ministre des Affaires étrangères en 2016-2018. Dès cette époque, il avait fait preuve de son talent pour les approximations. Alors que la citoyenne britannico-iranienne Nazanin Zaghari-Ratcliffe était enfermée en Iran, il avait déclaré qu’elle « enseignait simplement le journalisme aux gens ». Les propos de Boris Johnson ont été cités comme preuve qu’elle s'était livrée à une « propagande contre le régime ».

Deux mois après l’avoir remplacée en juillet 2019, « BoJo » enfreint la loi en prorogeant le Parlement pour cinq semaines, fermant la Chambre des Communes jusqu’à la date limite de sortie de l’Union européenne, au 31 octobre. La Cour suprême avait alors jugé que cet acte était illégal et le Parlement avait rouvert dès le lendemain. Ce faisant, le ton était pourtant donné : une règle pour lui, une règle pour les autres.

Juste après cette controverse, Boris Johnson fait l’objet d’une enquête à propos de sa relation avec la femme d’affaires Jennifer Arcuri. L’entreprise technologique de l’Américaine avait pu obtenir 25 000 livres de fonds publics et avait été autorisée à participer à trois missions commerciales à l'étranger. Bien qu’il n’y ait aucun fondement à des accusations criminelles, l’enquête conclut que Boris Johnson aurait dû déclarer un intérêt concernant Jennifer Arcuri et que son omission pourrait avoir enfreint le code de conduite du Parlement. Oups ! Jamais complètement en tort, il n’est jamais complètement en règle non plus.

Son rapport flou avec les règles se vérifie encore en mai 2020. Johnson défend mordicus [son ancien conseiller Dominic Cummings](https://www.lopinion.fr/international/le-grand-deballage-de-lancien-conseiller-tout-puissant-de-boris-johnson-dominic-cummings) qui n’a pas respecté les interdictions liées à la Covid à deux reprises, alors que le reste de la population se cloître chez elle et laisse mourir ses proches dans la solitude. A l’automne 2020, le gouvernement propose un projet de loi sur le marché national qui remet en cause [le protocole nord-irlandais](https://www.lopinion.fr/international/protocole-nord-irlandais-le-nouveau-pragmatisme-de-londres), pourtant signé par Johnson lui-même…

Ce style s’est donc poursuivi _on and on and on_ jusqu’à son point de rupture.[Le scandale du partygate](https://www.lopinion.fr/international/partygate-boris-johnson-saccroche-malgre-le-rapport-accablant-de-sue-gray)avait failli mettre fin à cette cavalcade mais la guerre en Ukraine a déplacé l’attention nationale vers l’est de l’Europe. L’enthousiasme du Premier ministre a soutenir l’Ukraine et à rendre de multiples visites, parfois surprises, au [président Volodymyr Zelensky](https://www.lopinion.fr/international/la-com-lautre-guerre-de-volodymyr-zelensky) a pu être interprété comme une tentative d’échapper à ses démons nationaux.

## Référence
[[Boris Johnson]]
[[Royaume-Uni]]

## Liens 