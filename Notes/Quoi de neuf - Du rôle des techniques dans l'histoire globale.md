MOC : [[Sociologie des techniques]]
Type : Note littéraire
Titre : [[Quoi de neuf - Du rôle des techniques dans l'histoire globale]]
Auteur : [[David Edgerton]]
Date : 2023-11-06
Tags : #Fini 
***

## Idées
##### [[Innovation sans substitut]] 

##### [[Les technologies grandioses et leur cout]]

##### [[Le choix technologique et nos biais]]

##### [[Technologies créoles]] 

##### [[Dualisme technologique de la Chine communiste]]

##### [[Non classification de la production domestique]]

##### [[Compétition entre les nations sur les innovations]]

##### [[Non proportionnalité entre croissance économique, niveau technologique et innovation technologique]]

##### [[Non classification de la production domestique]] 

##### [[Double production domestique]]

##### [[L'entretien dans la production]]

##### [[Fixation de Edgerton sur l'URSS et la Chine]]

##### [[Vision héréditaire des idéologies]]

##### [[Ingénieurs et entretien]]

##### [[Automobile comme technologie cruciale]]
##### [[Déterminisme non-strict de la technologie sur les victoires militaires]]

##### [[Les inventions sur les technologies déjà existantes sont cruciales]]

##### [[Industrialisation de la mise à mort (animal et humaine)]]

##### [[Effet navire à voile]]

##### [[Laboratoires technologiques des monopoles économiques]]

##### [[Sommes nous plus inventifs maintenant qu'avant]]

##### [[Les inventions sont désinventées]]


## Référence
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]