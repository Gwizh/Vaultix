### Quatres engagements majeurs
1- Rejet de toute forme de domination
2- Adhésion à la [[Démocratie directe]] et/ou à la prise de décision par consensus
3- Engagement en faveur de la politique préfigurative
4- Insistance sur l'action directe