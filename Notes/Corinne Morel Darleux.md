MOC : 
Source : [[Corinne Morel Darleux]]
Date : 2023-06-09
***

## Résumé
Corinne Morel Darleux (Morel d'Arleux à l'état civil)1, née le 1er octobre 1973 à Paris, est une autrice, essayiste, chroniqueuse et militante [[Écosocialisme|écosocialiste]] française.

En janvier 2021, elle a intégré le Conseil d’administration de la Fondation Danielle Mitterrand – France Libertés, avec laquelle elle poursuit son engagement auprès de l’expérience démocratique en cours au [[Rojava]] et dans le Nord-Est syrien.

En juillet 2021, elle participe au lancement de l’École des vivants avec [[Alain Damasio]].
## Référence

## Liens 