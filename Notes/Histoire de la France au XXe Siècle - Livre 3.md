[[Livre]]
MOC : [[Histoire]] [[Politique]] 
Titre : [[Histoire de la France au XXe Siècle - Livre 3]]
Auteur : [[Serge Bernstein]] [[Pierre Milza]]
Date : 2021-03
***


## 1- La fondation de la Ve République et le temps du gaullisme triomphant  (1958-1968)

###  Le ministère de Gaulle
**De juin 1958 à janvier 1959** se déroule une phase de transition **entre la IVe et la Ve République.** Durant cette période, le général [[Charles de Gaulle]] est président du Conseil de la IVe République, les institutions de 1946 demeurant en place, tout au moins formellement et [[René Coty]] restant chef de l’Etat. C'est le [[Gouvernement Charles de Gaulle (3) 1]].

### La préparation des nouvelles institutions et le référendum de septembre 1958
Outre l’affaire algérienne, la tâche essentielle du gouvernement de Gaulle est de préparer une nouvelle Constitution, puisque c’est à cette condition expresse que le général de Gaulle est revenu au pouvoir.
Autour de [[Michel Debré]], un groupe de juristes rédige le nouveau texte constitutionnel en s’inspirant des grands principes juridiques français, des idées de Michel Debré, des lignes de force définies par le général de Gaulle.

Ainsi élaborée, la [[Constitution de la Ve République]] est adoptée le **3 septembre 1958** par le Conseil des Ministres. Il faudra attendre le 28 septembre et le [[Référendum constitutionnel français de 1958]] pour qu'elle soit finalement accepté par la population. 

### La mise en place des nouvelles institutions
Pour acter cette nouvelle constitution, il fallait élir un nouveau grouvernement. Les 23 et 30 novembre 1958 se sont déroulés les [[Élections législatives françaises de 1958]]. Elle démontre l'hégémonie de de Gaulle dont les candidats vont largement remporter l'élection. Le **21 décembre 1958** a lieu l’[[Élection présidentielle française de 1958]] par le collège de 80 000 notables, prévu par la Constitution. **Le général de Gaulle est élu sans surprise par 78,5 % des suffrages** contre 13,1 % au communiste Georges Marrane et 8,4 % au doyen Chatelet, un universitaire, candidat de principe présenté par l’Union des Forces démocratiques.

En janvier 1959, le général de Gaulle prend ses fonctions de président de la République et nomme comme nouveau Premier ministre [[Michel Debré]], qui a désormais pour charge de mettre la Constitution en pratique.

### La constitution de la Ve République : un Président aux prérogatives renforcées
La clé de voûte du renforcement du pouvoir exécutif est l’importance nouvelle donnée au président de la République dans les institutions. 

- Il nomme le Premier ministre et, sur proposition de celui-ci, nomme les autres membres du gouvernement et met fin à leurs fonctions.
- Il possède le droit de dissolution sans autre condition que de consulter le Premier ministre et les présidents des deux Chambres (mais sans aucune obligation de suivre leur avis).
- Il peut recourir au référendum en posant des questions au suffrage universel, ce qui lui permet de passer par-dessus la tête des parlementaires en s’adressant directement au peuple – toutefois cette procédure est en principe limitée à des questions concernant l’organisation des pouvoirs publics.
- Enfin, l’article 16 prévoit l’octroi de pouvoirs exceptionnels au président de la République si les institutions de la République, l’indépendance de la nation ou l’intégrité de son territoire sont menacées.

**Entre 1958 et 1969 s’impose en France la tradition de voir le gouvernement dépendre directement du chef de l’Etat, véritable chef du pouvoir exécutif, le contrôle du Parlement tendant à se restreindre.**

(Voir : [[Assemblée Nationale]], [[Sénat]] et [[Conseil Constitutionnel]])


### L’évolution politique du général de Gaulle sur l’affaire algérienne
Le Général va adopter une ligne pragmatique, s’efforçant de préserver au maximum la place de la France dans les départements d’Algérie, mais en s’adaptant sans cesse aux circonstances. Cette évolution s'est déroulée en 4 phases durant la période 1958-1962 :

1) Le 4 juin 1958, le lendemain de son investiture et de la loi qui lui donne le pouvoir de préparer une nouvelle Constitution, le général de Gaulle se rend à Alger et déchaîne l’enthousiasme de la foule en lançant la formule ambiguë « Je vous ai compris... ». Au cours de ce voyage il criera, à Mostaganem, « Vive l’Algérie française ». Le [[FLN]] ne répondra même pas à cette offre ou du moins lui apportera une réponse négative en constituant en décembre au Caire le [[Gouvernement provisoire de la République algérienne]] (GPRA) sous la présidence de Ferhat Abbas.
2) En septembre 1959, prenant acte de son échec de 1958, le général de Gaulle proclame dans une conférence de presse le 16 septembre le droit de l’Algérie à l’autodétermination, affirmant qu’une fois le cessez-le-feu intervenu les Algériens se verront offrir le choix entre trois solutions, la sécession, la francisation, la constitution d’une Algérie gouvernée par les Algériens, mais liée à la France. 
3) En novembre 1960 s’ouvre la troisième étape, la plus difficile, au cours de laquelle le général de Gaulle passe d’une position de maintien de la présence française à l’acceptation d’une Algérie indépendante. Le 4 novembre 1960, lors d’une conférence de presse télévisée, le général de Gaulle parle d’une « République algérienne ». Ce tournant, entériné par un [[Référendum sur l'autodétermination de l'Algérie]] provoque une vive agitation en Algérie et débouche sur une tentative de [[Putsch des généraux]] à Alger en avril 1961. Le 20 mai 1961 s’ouvrent à Evian des négociations avec le FLN rompues dès le 13 juin sur le problème du statut du Sahara que les négociateurs du GPRA exigent de voir reconnu comme partie intégrante du futur Etat algérien.
4) La quatrième phase ouvre une période trouble de vive agitation en métropole et en Algérie. Les Algériens prennent des engagements sur les liens économiques, militaires, culturels, qui lieront, après l’indépendance, la France et l’Algérie. Le 21 février 1962, l’essentiel étant réglé sur le fond, les négociations officielles reprennent à Evian ([[Accords d'Évian]]). Le 18 mars au soir, un cessez-le-feu est conclu, valable à partir du lendemain, Ben Bella est libéré et le _Journal Officiel_ publie la substance des accords. Moins de quatre ans après le 13 mai et l’arrivée au pouvoir du général de Gaulle, l’Algérie est devenue un Etat indépendant.






## Références
