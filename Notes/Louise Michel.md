MOC : 
Source : [[Louise Michel]]
Date : 2023-06-02
***

## Résumé
Louise Michel, alias « Enjolras », née le 29 mai 1830 à Vroncourt-la-Côte (Haute-Marne) et morte le 9 janvier 1905 à Marseille, est une institutrice, écrivaine, militante [[Anarchie|anarchiste]], [[Franc-maçonnerie|franc-maçonne]] française aux idées [[Féminisme|féministes]] et l’une des figures majeures de la [[Commune de Paris]] durant laquelle elle s'implique tant politiquement que militairement en intégrant les rangs de la Garde nationale. Elle est aussi une des représentantes les plus célèbres de la part prise par les femmes dans la Commune de Paris.

Préoccupée très tôt par l'éducation, elle enseigne quelques années avant de se rendre à Paris en 1856. À 26 ans, elle y développe une importante activité littéraire, pédagogique et politique et se lie avec plusieurs personnalités révolutionnaires [[Blanquisme|blanquistes]] de Paris des années 1860.

En 1871, elle participe activement aux événements de la Commune de Paris, autant en première ligne qu'en soutien. S'étant livrée en mai pour faire libérer sa mère, elle est déportée en Nouvelle-Calédonie où elle se convertit à la pensée anarchiste. Elle revient en Métropole en 1880, et, très populaire, multiplie les manifestations et réunions en faveur des prolétaires. Elle reste surveillée par la police et est emprisonnée à plusieurs reprises, mais poursuit son militantisme politique dans toute la France, jusqu'à sa mort à l'âge de 74 ans à Marseille.

Elle demeure une figure révolutionnaire et anarchiste de premier plan dans l'imaginaire collectif. Première à arborer le drapeau noir, elle popularise celui-ci au sein du mouvement libertaire.

### Prise de possession
Merveilleux texte révolutionnaire publié en 1890. Typiquement anarchiste par le refus du vote ou en tout cas une non-illusion quant à son inutilité. Elle perle même d'un parti socialiste arrivé par les urnes mais qui ne ferait rien. Ou plutôt si mais qu'il finiront par se perdre dans un carriérisme fatal. Elle parle du communisme en bon, faudrait que je sache quelles étaient les différences à l'époque. Elle ne parle pas tant que ça de féminisme mais beaucoup de décolonisation, de l'Indochine surtout. Elle parle aussi des guerres récentes comme celle du Mexique. Elle parle d'un XXème siècle avec un avènement salutaire et inéluctable du communisme. Je pense qu'elle fait référence à l'idée marxiste qu'après un capitalisme bourgeois il y a forcément le communisme, raté. 

### À mes frères
#### Citations aléatoires
> "Dans cent ans, dans moins peut-être, il n'y aura plus d'armée permanente; toutes les nations auront désarmé, et la guerre de Trente Ans ne sera plus qu'une légende que les grands-parents conteront à leurs petits-enfants, comme aujourd'hui on leur contre Peau d'âne, et l'histoire de La Belle et la Bête."

> "Notre fondrière aujourd'hui se nomme d'un beau nom république respublica! (chose de tous). Nous l'avons adorée autrefois. Mais ce n'était pas la vraie qui devait venir, c'était un spectre avec le nom magnifique rayonnait sur un cadavre."

> "La guerre est devenue impossible par les inventions qui rendent les engins capables de foudroyer des légions entières, lançant de l'électricité du haut des airs, du fond de l'eau, de partout."

> "Quelque personnalité que ce soit n'y peut pas davantage que ne pourrait une goutte d'eau en face d'un raz-de-marée."

#### Anti-prisons
> "Tout est bagne sur la terre, tout est prison."


### Pensée Politique
Selon Sidonie Verhaeghe, Louise Michel a été dépeinte comme l'image sensible de l'anarchie et de la Commune. Sa réelle pensée est invisibilisée par ses opposants mais aussi par ses les anarchistes ! Clichée sexiste de la folle femme sensible incapable de réfléchir.

Elle est pour l'anarchie individualiste. Elle s'oppose à la [[IIIe République]] et elle ne souhaite pas voter et ne se bat pas pour le pouvoir (elle pense que ça légitimise le pouvoir). Elle est spontanéiste en pensant que la révolution doit venir par le bas et non par le haut pour ne pas risquer une centralisation autoritaire. Elle combat avant tout le pouvoir. Elle s'oppose à [[Jules Guesde]] et [[Paul Lafargue]]. Elle apprécie l'union avec les autres révolutionnaires, qu'ils soient socialistes ou communistes. Les désaccords sont pour elle l'occasion d'échanger.  

### Citations
"Il n'existe aucune différence entre un empiré et tout gouvernement régi par les mêmes moyens, si ce n'est le titre et la quantité de souverains. Notre République a des rois pas milliers. Ce qui pourrait s'appeler respublicæ, ce serait la chose pour tous, l'humanité libre sur le monde libre." 

"Il faudra bien qu'enfin le nid de l'humanité soit sur une branche solide, il faudra bien qu'on en change la base au lieu de perdre le temps à placer autrement les brins de paille. La base ce sera la justice égalitaire au lieu de la force. " 

## Référence

## Liens 