[[Article]]
MOC : [[ECONOMIE]]
Titre : [[Taxation des multinationales - vers un échec «historique»]]
Auteur : [[L'Opinion]]
Date : 2022-07-10
Lien : https://www.lopinion.fr/international/taxation-des-multinationales-vers-un-echec-historique
Fichier : 
***

## Résumé
Entre le chantage de Budapest et les difficultés de Washington, l’accord de l’OCDE, qui promet de remplir les caisses des Etats membres de 50 milliards d’euros chaque année, pourrait ne jamais être mis en œuvre.

La Hongrie bloque toujours la directive vouée à mettre en œuvre l’accord trouvé à l’OCDE sur la taxation minimale de 15 % des multinationales. En répercussion, le département du Trésor américain a annoncé vendredi mettre un terme au traité fiscal liant les Etats-Unis et la Hongrie depuis quarante-trois ans, et qui permet de faciliter les investissements et les activités économiques impliquant les deux pays. La décision américaine sera effective sous six mois. Le gouvernement hongrois a immédiatement réagi en affirmant qu’il ne modifierait pas pour autant sa position sur la directive européenne.

Les dirigeants européens pensaient pourtant avoir fait le plus dur en cédant au chantage de la Pologne, qui était le seul pays à bloquer l’accord, dans le but d’obtenir le déverrouillage de son plan de relance de 36 milliards d’euros. Plier devant Varsovie en lui accordant sa part de l’emprunt européen à des conditions peu exigeantes en matière d’indépendance de la justice a certes permis de lever l’opposition polonaise, mais a du même coup réveillé l’appétit de Viktor Orban. Comprenant que les dirigeants de l’UE tenaient beaucoup à cet accord, la Hongrie, qui soutenait le texte, a ainsi décidé de changer diamétralement de position, pour jouer le même coup que la Pologne.

A défaut d’effet domino, le dossier pourrait subir un syndrome du rétropédalage. En effet, l’accord de l’OCDE offre la possibilité aux pays d’origine d’une multinationale de récupérer les recettes fiscales dans les pays n’appliquant pas la taxation minimale. Concrètement, si les Etats-Unis adoptent l’accord seuls, Washington pourra taxer les bénéfices de Google à hauteur de 15 % sur ses activités irlandaises. Dublin a donc tout intérêt à s’engager… tant que Washington s’engage aussi.

La possibilité d’un échec historique prendra ainsi chaque jour un peu plus d’épaisseur, jusqu’à ce que le blocage hongrois soit outrepassé. L’attente pourrait s’éterniser: le sujet n’est même pas à l’ordre du jour de la réunion des 27 ministres des Finances qui a lieu cette semaine. Il pourrait toutefois s’inviter dans les discussions, Washington ayant ajouté de la pression sur les épaules de Budapest en décidant vendredi de mettre un terme à l’accord fiscal qui lie les deux capitales.


## Référence
[[Hongrie]]
[[Union Européenne]]

## Liens 