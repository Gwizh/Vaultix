[[Note Permanente]]
MOC : [[Politique]]
Source : [[Conseil Constitutionnel]]
Date : 2022-07-08
***

## Résumé
Le Conseil constitutionnel est une institution française créée par la [[Constitution de la Ve République]] du 4 octobre 1958. Il se prononce sur la conformité à la Constitution des lois et de certains règlements dont il est saisi. Il veille à la régularité des élections nationales et des référendums. Il intervient également dans certaines circonstances de la vie parlementaire et publique. Ses membres sont souvent surnommés par les médias « les Sages ». 

De janvier à mars 1974, en trois mois, le Conseil constitutionnel a rendu autant de décisions au titre du contrôle de constitutionnalité des normes que de 1958 à 1974, en quinze ans. En effet, puisqu'il n'y avait pas de recours effectif des citoyens devant le Conseil et seules les quatre plus hautes autorités administratives pouvaient le saisir, les opportunités de saisine étaient réduites, d'autant plus qu'il n'y avait pas de cohabitation. Ainsi, le Conseil ne fut saisi que neuf fois de 1959 à 1974. 

### Membres
[[Jacqueline Gourault]]

## Référence

## Liens 
[[Politque française]]