MOC : 
Source : [[Stagflation]]
Date : 2023-04-14
***

## Résumé
La stagflation est la situation d'une économie qui souffre simultanément d’une [[Croissance économique]] faible ou nulle et d'une forte [[Inflation]] (c’est-à-dire une croissance rapide des prix). Cette situation est souvent accompagnée d'un taux de [[Chômage]] élevé, contredisant ainsi les conclusions du [[Keynésianisme]] et de son carré magique de Kaldor.

## Référence

## Liens 