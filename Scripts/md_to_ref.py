import re
import os

def extract_titles_from_markdown(markdown_file_path):
    if not os.path.isfile(markdown_file_path):
        raise FileNotFoundError(f"The file '{markdown_file_path}' does not exist.")
        
    titles = []
    with open(markdown_file_path, 'r', encoding='utf-8') as file:
        for line in file:
            if line.startswith('###'):
                title = line.lstrip('#').strip()
                titles.append(title)
    return titles

markdown_file_path = os.path.join(os.getcwd(), 'Sources', 'Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité.md')
# markdown_file_path = os.path.normpath(markdown_file_path)
print(markdown_file_path)
try:
    titles = extract_titles_from_markdown(markdown_file_path)
except FileNotFoundError as e:
    print(e)
    markdown_file_path = input("Please provide the correct path to the Markdown file: ")
    titles = extract_titles_from_markdown(markdown_file_path)
    
for title in titles:    
    print(title)