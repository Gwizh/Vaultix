import os
import yaml
import json
from datetime import datetime

# Get the references in the Zaultix file
def get_ids_in_zault(file):
    json_data = file.readlines()
    json_str = ''.join(json_data)
    data = json.loads(json_str)
    ids = [entry['id'] for entry in data]
    titles = [entry['title'] for entry in data]

    zault_dict = dict(zip(ids, titles))

    return zault_dict

# Get the references already present in the reference file
def check_in_ref(zault_dict, file):
    with open(file, 'r+', encoding='utf-8') as ref_file:
        data = yaml.safe_load(ref_file)

    ref_list = [source['id'] for source in data['reference']['sources']]

    id_set = set(zault_dict.keys())
    ref_set = set(ref_list)
    final_list = list(id_set - ref_set)
    print(final_list)
    item = input("Source à ajouter à la référence ?")
    if item in zault_dict.keys():
        new_source = {
            'id': item,
            'titre': zault_dict[item],
            "date_d'ajout": datetime.today().strftime('%Y-%m-%d'),
            'propositions': ""
        }

        data['reference']['sources'].append(new_source)

        with open(file, 'w', encoding='utf-8') as ref_file:
            yaml.safe_dump(data, ref_file, allow_unicode=True, sort_keys=False)


    print(f"{len(zault_dict)} -> {len(final_list)}")


if __name__ == "__main__":
    with open('Zaultix.json', 'r', encoding='utf-8') as zaul_file:
        zault_dict = get_ids_in_zault(zaul_file)

    check_in_ref(zault_dict, 'Fixette.yml')