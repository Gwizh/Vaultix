import yaml
from datetime import datetime

# Path to the YAML file
yaml_file = 'Fixette.yml'

# Reading the YAML file
with open(yaml_file, 'r', encoding='utf-8') as file:
    data = yaml.safe_load(file)

# Get today's date
today_date = datetime.today().strftime('%Y-%m-%d')

# Add 'date_d\'ajout' field with today's date to each source
for source in data['reference']['sources']:
    if not source['date_d\'ajout']:
        source['date_d\'ajout'] = today_date

# Writing the modified content back to the YAML file
with open(yaml_file, 'w', encoding='utf-8') as file:
    yaml.safe_dump(data, file, allow_unicode=True, sort_keys=False)

print(f"Added 'date_d'ajout' field with today's date to all sources in {yaml_file}.")
