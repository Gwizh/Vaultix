## Récupération
1. Accès : 1 bit - Oui/Non
2. Type : 3 bits - Permanent/Temporaire
- Achat : 111
- Pdf : 101
- Perso : 110
- Bibli : 011
- En ligne : 001
- Oral : 000
- Inconnu : xxx
3. Ordre d'importance

## Lecture
4. Lu : 2 bits - Oui/Presque fini/Commencé/Non

## Notes
5. Type de notes : 2 bits - Numériques, Scannées, Non scannées, Pas de notes

## Stockage
6. Dossier créé : 1 bit - Oui/Non
7. Document dans la référence : 1 bit - Oui/Non
8. Document dans dossier : 1 bit - Oui/Non
9. Notes dans dossier : 1 bit - Oui/Non
10. Notes dans la référence : 1 bit - Oui/Non

## Exemples
1101111-11-11-11111
