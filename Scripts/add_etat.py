import yaml
from datetime import datetime

def convert_binary_to_decimal(binary_str):
    # Séparer la chaîne binaire en groupes de nombres binaires
    binary_groups = binary_str.split('-')
    
    # Convertir chaque groupe binaire en nombre décimal
    decimal_groups = [str(int(group, 2)) for group in binary_groups]
    
    # Joindre les groupes décimaux en une seule chaîne séparée par des tirets
    decimal_str = '-'.join(decimal_groups)
    
    return decimal_str

def convert_decimal_to_binary(decimal_str):
    # Split the input string into a list of decimal numbers
    decimal_groups = decimal_str.split('-')
    
    # Convert each decimal number to its binary equivalent
    binary_groups = [format(int(group), 'b') for group in decimal_groups]
    
    # Join the binary numbers into a single string separated by hyphens
    binary_str = '-'.join(binary_groups)
    
    return binary_str

# Path to the YAML file
yaml_file = 'Fixette.yml'

# Reading the YAML file
with open(yaml_file, 'r', encoding='utf-8') as file:
    data = yaml.safe_load(file)

for source in data['reference']['sources']:
    if 'etat' not in source:
        source['etat'] = 'xxxxxxx-xx-xx-x1xxx'

# Writing the modified content back to the YAML file
with open(yaml_file, 'w', encoding='utf-8') as file:
    yaml.safe_dump(data, file, allow_unicode=True, sort_keys=False)
