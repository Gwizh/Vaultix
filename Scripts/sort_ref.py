import yaml

# Path to the YAML file
yaml_file = 'Fixette.yml'

# Reading the YAML file
with open(yaml_file, 'r', encoding='utf-8') as file:
    data = yaml.safe_load(file)

# Sort the sources by 'titre'
data['reference']['sources'].sort(key=lambda x: x['titre'])

# Writing the sorted dictionary back to the YAML file
with open(yaml_file, 'w', encoding='utf-8') as file:
    yaml.safe_dump(data, file, allow_unicode=True, sort_keys=False)

print(f"Sorted entries by 'titre' and updated {yaml_file}.")