import os
import io

with open('Fixette.yml', 'r') as file:
    stream = file.read()

# Modifier le contenu lu
modified_stream = stream.replace('- id:', '\n  - id:')

# Écrire le contenu modifié dans le fichier (écrasant l'ancien contenu)
with open('Fixette.yml', 'w') as file:
    file.write(modified_stream)