import os
import yaml

def main():
    base_path = 'G:\Fichiers\Documents\Vaultix\Fixette'
    entries = os.listdir(base_path)
    print(entries)
    
    with open('Fixette.yml', 'r') as file:
        data = yaml.safe_load(file)
        ids = [source['id'] for source in data['reference']['sources']]
        print(ids)
    
    for id in ids:
        path = os.path.join(base_path, id)
        os.makedirs(path, exist_ok=True)
        

if __name__ == "__main__":
    main()