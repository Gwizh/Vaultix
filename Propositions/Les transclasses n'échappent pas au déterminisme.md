Titre : [[Les transclasses n'échappent pas au déterminisme]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Transclasse]], [[Frédéric Lordon]], [[Déterminisme]]
Date : 2023-12-09
***

Les transclasses sont généralement utilisés pour contrer Bourdieu et le déterminisme. Le self-made-man a pu pourtant très largement faire ce changement par des affects. Eth IV, 7.

Par définition, les transclasses dépassent l'entendement sociologique car on ne peut voir que les affects locaux.
## Sources
[[Spinoza et les passions du social]]