Titre : [[Définitions de la liberté selon Pettit]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Liberté]], [[Philip Pettit]]
Date : 2023-12-09
***

Négative : être libre par défaut mais à condition de n'avoir aucunes entraves à notre volonté -> libéral
Positive : être libre et être déterminé rationnellement et non affectivement -> populiste
Non domination : être libre c'est n'être soumis à aucun autre. Refus de toute interférence arbitraire
## Sources
[[Spinoza et les passions du social]]