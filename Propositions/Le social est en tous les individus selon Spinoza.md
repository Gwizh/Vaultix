Titre : [[Le social est en tous les individus selon Spinoza]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Spinoza]], [[Frédéric Lordon]]
Date : 2023-12-09
***

Nous avons le global en nous du fait de tous les affects que nous avons reçu dans notre vie. Nous avons gardés des traces, nous sommes nous-mêmes trace et ces traces sont notre ingenium, notre complexion corporelle. Nous recevons les normes du global par des acteurs locaux.
## Sources
[[Spinoza et les passions du social]]