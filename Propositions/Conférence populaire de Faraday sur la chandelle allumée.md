Titre : [[Conférence populaire de Faraday sur la chandelle allumée]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Michael Faraday]], [[Vulgarisation scientifique]]
Date : 2024-01-21
***

Faraday donnait des cours du soir pendant lesquels il faisait des expériences. Voir Histoire d'une chandelle de Faraday.
## Sources
[[La flamme d'une chandelle]]