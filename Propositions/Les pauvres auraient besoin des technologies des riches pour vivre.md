MOC : [[Sociologie des techniques]]
Type : Note éphémère
Titre : [[Les pauvres auraient besoin des technologies des riches pour vivre]]
Date : 2023-11-06
***

Si nous n'imaginons pas une vie sans certaines technologies qui nous considérons comme essentiels, nous regardons alors avec pitié les populations que ne les ont pas. La vie nous semblent alors impossible et on ne comprend même pas comment ces populations vivent. Méconnaissance et ignorance. 
## Provenance
[[@Quoi de neuf_ du rôle des techniques dans l'histoire globale,]]
Mélange de [[Quoi de neuf - Du rôle des techniques dans l'histoire globale#Innovation sans substitut]] et [[Quoi de neuf - Du rôle des techniques dans l'histoire globale#Technologies créoles]]
