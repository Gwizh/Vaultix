Titre : [[Républicanisme de Pettit]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Philip Pettit]], [[Républicanisme]]
Date : 2023-12-09
***

Pousser au + loin la non-domination grâce à la rationalité et les contre-pouvoirs.
Projet dynamique qui n'a pas de forme fixe. Pour pouvoir jouer son rôle de gestionnaire de la vie publique le pouvoir doit être discrétionnaire.
Comme on ne peut pas assurer le consentement de tous, il y a un besoin vital de contre-pouvoirs à toutes les échelles.
## Sources
[[Spinoza et les passions du social]]