Titre : [[Révolution de l'éclairage par la bougie selon Schivelbusch]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Éclairage]], [[Wolfgang Schivelbusch]]
Date : 2024-01-11
***

La bougie a été la première utilisation du feu uniquement dédiée à l'éclairage. Le feu est selon lui rendu docile, il ne détient quasiment plus le lieu de combustion. C'est encore plus le cas avec la lampe à huile dont la mèche ne s'épuise quasiment pas.
## Sources
[[La Nuit Désenchantée]]