Titre : [[Obéissance de Hobbes et Kant vs celle de Spinoza]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Spinoza]], [[Emmanuel Kant]], [[Thomas Hobbes]]
Date : 2023-12-10
***

Hobbes et Kant sont considérés libéraux.
L'obéissance pour eux dépend du libre-arbitre et de la loi souveraine.
Spinoza dépasse complètement ça. Ce n'est pas des "formes de composition rationnelle" mais un procès de rationalisation continuelle.
## Sources
[[Spinoza et les passions du social]]