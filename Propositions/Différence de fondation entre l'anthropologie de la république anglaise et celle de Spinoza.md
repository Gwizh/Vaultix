Titre : [[Différence de fondation entre l'anthropologie de la république anglaise et celle de Spinoza]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Spinoza]], [[Anthropologie]], [[République anglaise]]
Date : 2023-12-09
***

Si la République anglaise cherche avant tout à permettre à chacun leur définition de la liberté, cela ne permet pas selon Spinoza d'échapper aux conflits, aux haines, colères, ruses, etc...
La brique élémentaire d'un système politique selon lui devrait être les rapports de domination et non une liberté arbitraire. Il n'y a pas de légitimité selon Spinoza.

-> Autoconstitution démocratique de la puissance commune par les puissances individuelles.
## Sources
[[Spinoza et les passions du social]]