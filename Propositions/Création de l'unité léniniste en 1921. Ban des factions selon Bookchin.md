Titre : [[Création de l'unité léniniste en 1921. Ban des factions selon Bookchin]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Murray Bookchin]], [[Vladimir Lénine]]
Date : 2024-01-19
***

Bookchin analyse les débats des bolcheviques de 1904 à 1921. Il observe un mouvement rempli de factions, de divisions, etc... Lénine dut combattre une direction bolchevique très conservatrice. En 1921,lors du Xe congrès, il convainc la direction de bannir les factions "anarchistes petit bourgeois".
## Sources
[[Écoute, camarade !]]