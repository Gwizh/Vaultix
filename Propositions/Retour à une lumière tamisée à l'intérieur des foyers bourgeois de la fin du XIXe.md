Titre : [[Retour à une lumière tamisée à l'intérieur des foyers bourgeois de la fin du XIXe]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lumière électrique]]
Date : 2024-01-30
***

La lumière devient si intense qu'on ne devait plus le regarder pour des raisons de santé.
Création d'abat-jours pour diffuser mais aussi colorer la lumière. 
Les lampes Tiffany sont l'exemple de cet obscurcissement progressif. Il en va de même pour les fenêtres qui se sont perfectionnées et agrandies. Il fallait maintenant diluer cette lumière derrière un rideau qui devenait de + en + opaque. La coloration du verre comme au Moyen-âge revient à cet effet.
p142 : Lumière et bourgeoisie.

## Sources
[[La Nuit Désenchantée]]