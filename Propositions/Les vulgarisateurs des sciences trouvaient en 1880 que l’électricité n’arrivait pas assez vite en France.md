Titre : [[Les vulgarisateurs des sciences trouvaient en 1880 que l’électricité n’arrivait pas assez vite en France]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Électricité]]
Date : 2024-01-28
***

Louis Figuier, se lamentait en 1882 dans son livre sur _les Merveilles de l’électricité_ de la lenteur avec laquelle elle était installée dans Paris. En cette même année, dans la préface de leur livre _la Lumière électrique_, Émile Alglave et Jean Boulard en appelaient au conseil municipal, lui demandant d’agir sans hésitation « dans une question où l’esprit démocratique s’allie si heureusement à l’esprit scientifique »
## Sources
[[Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité]]