Titre : [[Magouilles sur les salaires et montants d'indemnités après la catastrophe de Courrières]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Courrières]], [[Droit du travail]]
Date : 2024-03-27
***

Salaires plus bas liés à la perte de production. Gestion par l'entreprise de la caisse de solidarité, etc...
Mais le directeur aussi obtient une grosse augmentation temporaire
## Sources
[[Une entreprise face à la gestion de « risques majeurs »]]