Titre : [[Création d'un véritable lobby électrique de 1875 à 1881 selon Beltran et Carré]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Électricité]], [[Exposition internationale d'Électricité de Paris en 1881]]
Date : 2024-01-28
***

Au début dans les Annales de Télégraphie, les articles sur les nombreuses innovations passionnant savant comme hommes d'affaires, comme Herz. Admirateur d'Edison, il souhaite voir surgir autant d'innovations en France
-> Syndicat français d'Électricité
Lui et d'autres sont sont prolifiques
76 : L'Électricité est né (le journal)
79 : La Lumière Électrique
Ils participent à l'exposition de 1881.
## Sources
[[La fée et la servante]]