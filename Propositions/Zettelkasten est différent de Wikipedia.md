MOC : [[Notes/Prise de notes]]
Type : Note éphémère
Titre : [[Zettelkasten est différent de Wikipedia]]
Date : 2023-11-05
***

## Idée

Ils n'utilisent pas les mêmes concepts.

Zettelkasten est là pour prendre des notes personnelles alors que Wikipedia est là pour répertorier tout le savoir de manière collective.

## Provenance
https://github.com/numpy/numpy/issues/18748