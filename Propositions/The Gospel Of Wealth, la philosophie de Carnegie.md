Titre : [[The Gospel Of Wealth, la philosophie de Carnegie]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Andrew Carnegie]], [[Philanthropie]]
Date : 2023-12-27
***

Canergie est à la tête d'un empire métallurgique, tiré par l'industrie ferroviaire.
Il a écrit The Gospel of Wealth, qui sont ses réflexions personnelles sur son succès.

"Les inégalités sont naturelles et bénéfiques car seuls les plus capables s'enrichissent. Les millionnaires ont le devoir d'utiliser leur fortune utilement. Il ne doit rien léguer à sa dépendance. Le millionnaire doit consacrer toute sa fortune à la philanthropie."
## Sources
[[Arte - Capitalisme américain, le culte de la richesse (1-3)]]