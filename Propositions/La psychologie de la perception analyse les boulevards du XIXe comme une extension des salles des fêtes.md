Titre : [[La psychologie de la perception analyse les boulevards du XIXe comme une extension des salles des fêtes]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lumières et villes]], [[Gaston Bachelard]]
Date : 2024-01-30
***

Les vitrines des magasins, décorées, illuminées, sont perçues comme des murs. Là où les lanternes sont placées devient un plafond. Le fort contraste entre l'ombre et la lumière devient aussi fort que l'intérieur et l'extérieur.
## Sources
[[La Nuit Désenchantée]]