Titre : [[Promotion du nationalisme aux JT]]
Type : Proposition
Tags : #Proposition
Ascendants :
Date : 2023-12-07
***

Le sport mis en avant

La tradition et la culture comme réconfortant

Les actualités internationales ou faits divers comme anxiogène.

L’idée est de promouvoir comme sensée, raisonnable les habitudes propre à notre pays et de rejeter comme différent, bizarre, dangereux ce qui vient d’ailleurs.
## Sources
[[@Horizon - La France a peur_ le syndrome du grand méchant monde,]]