Titre : [[La bourgeoisie utilisa la lumière comme outil de travail dès le XVIe siècle]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Bourgeoisie]], [[Éclairage]]
Date : 2024-01-11
***

L'heure commença alors à être mesurée mécaniquement. La bourgeoisie souhaite alors rendre la journée de travail indépendante de la journée du soleil. Il fallait pouvoir travailler à heure fixe, été comme hiver. L'utilisation de l'éclairage était limitée et rationnelle. Elle était aussi individuelle.
## Sources
[[La Nuit Désenchantée]]