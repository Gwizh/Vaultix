Titre : [[L'éclairage électrique a été tout de suite été préféré au gaz dans les parcs pour des raisons environnementales]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lumière électrique]], [[Éclairage au gaz]]
Date : 2024-01-28
***

On savait que le gaz polluait les sols où passait les tuyaux - n2
Le sol devenait noir, infecte et les arbres mouraient. 
C'est pour ça que les parcs furent les premiers lieux de l'éclairage électrique. On disait que la couleur de sa lumière allait mieux avec le vert l'herbe.
## Sources
[[La fée et la servante]]