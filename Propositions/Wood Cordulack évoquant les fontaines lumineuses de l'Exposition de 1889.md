Titre : [[Wood Cordulack évoquant les fontaines lumineuses de l'Exposition de 1889]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lumière électrique]], [[Exposition universelle de 1889]]
Date : 2024-01-28
***

Un des spectacles les plus populaires de l’exposition était « les fontaines lumineuses ». Une de ces fontaines représentait une statue tournée vers la tour Eiffel, nommée « La France éclairant le monde » avec sa torche et ses figures allégoriques arrosées de jets d’eau illuminés électriquement. Au cours de cette année, la statue de la Liberté fut momentanément illuminée et photographiée de nuit par S.R. Stoddard, et le modèle réduit de _la Liberté éclairant le monde_ fut transporté de la place des États-Unis à l’île des Cygnes, où elle se trouvait derrière la tour Eiffel.
## Sources
[[Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité]]