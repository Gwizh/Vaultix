Titre : [[Bookchin pense le parti comme une institution fondamentalement bourgeoise]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Murray Bookchin]], [[Parti]]
Date : 2024-01-19
***

Le but d'un parti est de prendre et de garder le pouvoir et non le dissoudre. Le parti n'a donc psa lieu d'être au sein du communisme. Le parti respecte des considérations hiérarchiques, il y a dogmes, discipline et manipulation. C'est encore plus le cas lorsqu'il se prend au jeu électoral bourgeois. Il devient magouilleur !
## Sources
[[Écoute, camarade !]]