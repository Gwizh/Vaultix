Titre : [[L'utilisation du charbon demandait de changer les industries]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Charbon]], [[Révolution industrielle]]
Date : 2024-01-21
***

La combustion du charbon provoquait une chaleur plus intense que les autres sources d'énergie. Il fallait donc des fours plus hauts et globalement des outils plus résistants. C'était donc tout l'équipement qu'il fallait revoir.
## Sources
[[La Nuit Désenchantée]]