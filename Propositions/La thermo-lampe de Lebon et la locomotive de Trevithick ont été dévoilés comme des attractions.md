Titre : [[La thermo-lampe de Lebon et la locomotive de Trevithick ont été dévoilés comme des attractions]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Éclairage au gaz]]
Date : 2024-01-21
***

"Faiseur de projets" à la Argand, Lebon inventa en 1801 la thermolampe, lampe à gaz alimentée par des tuyaux, disposés au centre des maisons et servant d'éclairage ou de chauffage. Lire p26 comment il présente son produit.
Il chercha la commercialisation et fit payer l'entrée à sa propre maison p27 pour montrer les qualités de son invention.
## Sources
[[La Nuit Désenchantée]]