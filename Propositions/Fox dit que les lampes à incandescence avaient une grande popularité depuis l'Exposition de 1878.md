Titre : [[Fox dit que les lampes à incandescence avaient une grande popularité depuis l'Exposition de 1878]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Bougies de Jablochkoff]]
Date : 2024-02-02
***

La bougie de Jablochkoff a été présentée en 78 et sembla avoir attiré du monde. C'est ce qui a permis d'affirmer le nom de ville lumière pour Paris.
## Sources
[[Thomas Edison's Parisian campaign]]