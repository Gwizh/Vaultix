Titre : [[La télégraphie électrique est d'abord conservée par l'État, avant d'être étandue au public]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Télégraphie]]
Date : 2024-01-27
***

La première ligne en France a été construite en 1845. En 46, une ligne existe de Paris à Lille. Ce n'est qu'en 50 que le ministre de l'intérieur (chargé de la télégraphie d'État) ouvre la technologie au public. Le rail et le télégraphe se développe ensuite très vite au Second Empire pour intérêt commercial.
## Sources
[[La fée et la servante]]