Titre : [[Beaucoup de penseurs du XIXe catégorisaient Spinoza comme un contractualiste optimiste]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Spinoza]], [[Thomas Hobbes]], [[Jean-Jacques Rousseau]], [[Contrat social]]
Date : 2023-12-10
***

Ils font le rapprochement entre Spinoza, Hobbes et Rousseau avec cette idée de création rationnelle d'un système avec comme élément central un contrat social. Souvent en échec car Spinoza réfute l'idée que le contrat social soit si important.
## Sources
[[Spinoza et les passions du social]]