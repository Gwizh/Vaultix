Titre : [[La lumière de la statue de la liberté comme symbole de l’avance de la France]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lumière électrique]]
Date : 2024-01-28
***

Conçue et construite par les Français [[29]L’image d’une femme brandissant une lumière n’était pas en soi…](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no29) comme un présent aux États-Unis d’Amérique, la statue de la Liberté célébrait l’amitié franco-américaine et l’unité philosophique.

« Les amis des États-Unis ont pensé que le génie de la France devait se déployer sous une forme éblouissante. Un artiste français a exprimé cette pensée… La Liberté éclairant le monde. La nuit, une auréole resplendissante sur son front va lancer ses rayons par delà le vaste océan. » [[30]](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no30)

L’illumination électrique de la statue « servirait à illustrer le triomphe des derniers développements de l’électricité, ainsi que l’amitié unissant les deux grandes Républiques ».
## Sources
[[Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité]]