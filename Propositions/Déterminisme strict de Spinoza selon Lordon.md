Titre : [[Déterminisme strict de Spinoza selon Lordon]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Spinoza]], [[Frédéric Lordon]]
Date : 2023-12-09
***

Tout dépend des affects qui existent déjà autour de nous. Tel affect provenant de tel autre corps à travers notre corps, nous affecte, nous fait désirer puis nous fait bouger .Un corps qui bouge est un corps qui a été affecté, déterminé à un certain désir de mouvement pour une certaine affection.

Les institutions sont régies par des corps affectés et agissent pareil

Lire Eth III def 27.

## Sources
[[Spinoza et les passions du social]]