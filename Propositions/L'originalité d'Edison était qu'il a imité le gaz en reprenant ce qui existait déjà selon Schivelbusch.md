Titre : [[L'originalité d'Edison était qu'il a imité le gaz en reprenant ce qui existait déjà selon Schivelbusch]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Thomas Edison]], [[Lumière électrique]], [[Éclairage au gaz]]
Date : 2024-01-30
***

Davy avait découvert l'incandescence. Le fait de mettre dans le vide De Moleyne 1841
Göbel le filament carbonique en 1850
Edison a perfectionné et mis en pratique en un produit prêt à l'application 
-> Comparaison avec Winsor
Il voulait faire tout pareil sauf utiliser l'air ! Aussi - d'intensité.
Citations de la page 58 : au lieu du blanc, dur, il y a alors une lumière "civilisée", accommodée à nos habitudes, même luminosité que le gaz mais juste, constant et sans pollution et sans danger.
Même coloration car filament carbonique
On peut alors voir l'ancien dans le nouveau.
## Sources
[[La Nuit Désenchantée]]