Titre : [[Les premiers utilisateurs de la lampe d'Edison expliquent en partie son succès]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Thomas Edison]], [[John Pierpont Morgan]]
Date : 2024-01-10
***

En 1882, quelques entreprises et particuliers, comme J.P. Morgan, avaient également été équipés de leur propre installation, dotée d’un générateur individuel. Aspect non négligeable en terme de communication, le New York Times et le New York Herald figuraient parmi ces premiers utilisateurs de la lumière électrique [JONNES, 2003, p. 85]. 
## Sources