Titre : [[Les usines furent le lieu où l'éclairage a eu le plus d'importance]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Éclairage]], [[Révolution industrielle]]
Date : 2024-01-11
***

Dans les usines, l'éclairage n'était plus individuel et pourtant la taille du lieu rivalisait avec les châteaux des nobles. Un éclairage centralisé et peu couteux est alors devenu nécessaire. Il fallait aussi grandement augmenter la puissance des lampes.
## Sources
[[La Nuit Désenchantée]]