Titre : [[Contrairement au gaz, on confiait à l'électricité plein de caractéristiques vitales - tout électrique]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Électricité]]
Date : 2024-01-30
***

On fit entrer l'électricité directement dans les grandes demeures bourgeoises ou aristocratiques car elle faisait moins peur que le gaz. On disait de l'électricité que c'était de l'Energie.
Un moyen de lutter contre la fatigue.
Engrais pour l'agriculture.
Electrothérapie.
Citation note117
## Sources
[[La Nuit Désenchantée]]