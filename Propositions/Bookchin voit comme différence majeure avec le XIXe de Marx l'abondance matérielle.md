Titre : [[Bookchin voit comme différence majeure avec le XIXe de Marx l'abondance matérielle]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Murray Bookchin]]
Date : 2024-01-13
***

Aujourd'hui notre vie est banale et caractérisée par l'abondance matérielle, il n'y a plus de précarité des objets. Bien sûr, il s'agit d'avoir les moyens de consommer mais le XIXe n'offrait pas à tous assez de bien. La faim existait avant tout par manque, pas par volonté politique.
## Sources
[[Écoute, camarade !]]