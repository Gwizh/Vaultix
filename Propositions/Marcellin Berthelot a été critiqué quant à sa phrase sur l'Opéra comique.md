Titre : [[Marcellin Berthelot a été critiqué quant à sa phrase sur l'Opéra comique]]
Type : Proposition
Tags : #Proposition #Fixette I
Ascendants : [[L'incendie de l'Opéra comique]], [[Marcellin Berthelot]]
Date : 2023-12-29
***

Monsieur Steenackers, député de la Haute-Marne interpelle, lors d'une séance houleuse à l'Assemblée Nationale le 12 mai 1887, Monsieur Berthelot ministre des Beaux Arts. Il dénonce la vétusté du bâtiment, son extrême exigüité, rappelant les dangers encourus tant par le public que par le personnel, ainsi que les nombreux rapports établis sur le sujet. La réponse du ministre surprend : « *Nous pouvons considérer comme probable que l'Opéra-Comique brûlera, c'est un fait statistique.* » Tout le monde conservait en mémoire les incendies survenus ces dernières décennies (Opéra Le Peletier 1873 ; Montpellier 1876 ; Nice et Ring Theater de Vienne en 1881 avec ses quatre cent soixante-dix morts). 

Cette phrase lui coûtera son poste après le drame.
## Sources
https://artlyriquefr.fr/dicos/Opera-Comique%20incendie.html