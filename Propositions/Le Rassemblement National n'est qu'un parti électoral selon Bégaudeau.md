Titre : [[Le Rassemblement National n'est qu'un parti électoral selon Bégaudeau]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Rassemblement National]], [[François Bégaudeau]]
Date : 2023-12-10
***

Le Rassemblement National n'est qu'un parti électoral, il n'a pas de militants, ne défendent pas de projets en dehors des élections, ne participent pas aux manifestations. Il ne joue que sur le jeu des élections pour survivre. 
## Sources
[[Les sondages sont-ils dangereux pour la démocratie]]