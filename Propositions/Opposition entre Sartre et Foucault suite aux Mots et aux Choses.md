Titre : [[Opposition entre Sartre et Foucault suite aux Mots et aux Choses]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Michel Foucault]], [[Jean-Paul Sartre]]
Date : 2023-12-19
***

La sortie des Mots et des Choses est controversée parmi les intellectuels de gauche.
"Ce qui me fâche contre l'humanisme, c'est qu'il est désormais ce paravent derrière lequel se réfugie la pensée la plus réactionnaire." - Foucault

Jean-Paul Sartre est un des principaux critiques de cette nouvelle théorie portant sur l’homme. Il accuse Foucault d’être « le dernier rempart que la bourgeoisie puisse encore dresser contre Marx ». Selon Sartre, Foucault n’agit pas en tant qu’archéologue, mais en tant que géologue, puisqu’il n’explique pas comment les changements de l’épistémè se passent, ni pourquoi les hommes passent d’une pensée à l’autre.

Michel Foucault se défendra dans des écrits ultérieurs, afin de dénoncer une certaine hypocrisie liée à la notion de l’humaniste.
## Sources
https://www.dygest.co/michel-foucault/les-mots-et-les-choses
[[Michel Foucault - Que sais-je]]