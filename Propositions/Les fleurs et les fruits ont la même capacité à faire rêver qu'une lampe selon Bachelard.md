Titre : [[Les fleurs et les fruits ont la même capacité à faire rêver qu'une lampe selon Bachelard]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Gaston Bachelard]]
Date : 2024-01-21
***

Les fleurs et les fruits naissent du soleil. Leurs vives couleurs proviennent de cette lumière, de cette chaleur. Mettre une tulipe rouge dans un vase au centre de la table, c'est créer un centre d'attention semblable au feu. On peut tout autant se perdre dans ses pensées.
## Sources
[[La flamme d'une chandelle]]