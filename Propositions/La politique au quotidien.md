Titre : [[La politique au quotidien]]
Type : Proposition
Tags : #Proposition
Ascendants : [[François Bégaudeau]], [[Démocratie]]
Date : 2023-12-10
***

Le mode de scrutin est important pour permettre aux gens de participer à la politique et la démocratie mais ça ne suffit pas. Encore une fois, le vote en soi ne permet rien. Ce qui compte c'est la manière de faire de la politique et les mandats par exemple. Une femme de ménage ou un ouvrier ne peut pas aujourd'hui entrer en politique par le vote et conserver son travail. On ne peut pas associer les deux. Il faudrait pouvoir faire de la politique tout en étant travailleur. 
## Sources
[[Les sondages sont-ils dangereux pour la démocratie]]