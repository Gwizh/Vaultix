Titre : [[Jusqu'au XIXe, le public des théâtres et opéra était éclairé]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lumières et théâtres]]
Date : 2024-02-11
***

On savait depuis la Renaissance qu'on voyait d'autant plus la scène que le public était dans le noir.
Pourtant, ce n'est qu'au XIXe siècle qu'on a commencé à éteindre le public.
Cela venait du public et non des metteurs en scène. Ces derniers voulaient au contraire illuminé au mieux mais étaient contraints par les volontés du public.
Pour ceux-ci, le théâtre était un lieu de rencontre, de regroupement, le parfait éclairage de la scène n'était pas crucial.
À la fin du XIXe, il n'était pas rare d'entendre les vieux se plaindre de l'obscurité dans la salle.
Pourtant, la culture a changé et il est devenu normal d'avoir le public dans le noir à la fin du XIXe 
## Sources
[[La Nuit Désenchantée]]