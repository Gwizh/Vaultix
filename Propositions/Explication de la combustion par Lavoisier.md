Titre : [[Explication de la combustion par Lavoisier]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Antoine Lavoisier]]
Date : 2024-01-11
***

On pensait que la flamme se nourrissait d'une substance nommée phlogiston. Les écrits de Lavoisier nous ont permis de contrôler les flammes. On a pu grandement augmenter le rendement de toutes formes de combustion. Ça a eu des conséquences pour la machine à vapeur de Watt par exemple.
## Sources