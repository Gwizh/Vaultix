Titre : [[Caractéristiques du désir pour Girard]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Désir]], [[René Girard]], [[Eva Debray]]
Date : 2023-12-09
***

Valeur d'un objet ne provient que de la résistance du modèle (la personne qui possède). Il y a forcément déception au moment où obtient sans qu'on voit que ça provenant du mimétisme.
Rivalité : disciple comme modèle ont mais différemment et jamais assez. Finalement, le disciple ne revendiquera pas l'objet mais aussi la violence que le modèle utilise pour interdire l'objet.
## Sources
[[Spinoza et les passions du social]]