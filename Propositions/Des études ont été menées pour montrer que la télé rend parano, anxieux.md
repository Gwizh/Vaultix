Titre : [[Des études ont été menées pour montrer que la télé rend parano, anxieux]]
Type : Proposition
Tags : #Proposition
Ascendants :
Date : 2023-12-07
***

La télé montre le spectaculaire avant l’informatif et les émotions qui retient le plus l’attention est la peur, la colère ou la tristesse. Les infos jouent là dessus pour créer de l’audimat. On se demande ce qu’il se passerait si on ne montrait que des documentaires et des résultats de recherches.

[Violence Profile no. 10 - George Gerbner Archive](https://web.asc.upenn.edu/gerbner/archive.aspx?sectionID=155&packageID=90)
## Sources
[[@Horizon - La France a peur_ le syndrome du grand méchant monde,]]