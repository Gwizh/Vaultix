Titre : [[Lectures postrévolutionnaires et anachroniques de Spinoza]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Spinoza]], [[Révolution française]]
Date : 2023-12-11
***

Il faut faire attention à ne pas déformer la pensée des philosophes d'avant la Révolution française. Spinoza en particulier ne décrivait pas les révolutions comme une bonne chose en soi car elles apportent nécessairement une perte de stabilité, un arrêt de la conservation. Et l'après marx voit aussi une interprétation sur la multitude et une politique du commun. 
## Sources
[[Spinoza et les passions du social]]