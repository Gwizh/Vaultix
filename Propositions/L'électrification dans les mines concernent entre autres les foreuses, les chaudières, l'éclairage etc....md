Titre : [[L'électrification dans les mines concernent entre autres les foreuses, les chaudières, l'éclairage etc...]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Mines]], [[Électricité]]
Date : 2024-03-27
***

À Courrières, après la 1GM, une politique du matériel est entrepris. L'électricité remplace alors la vapeur et offre puissance, sécurité et maniabilité. Pas simplement les lampes, plein de différentes machines.
## Sources
[[Une entreprise face à la gestion de « risques majeurs »]]