Titre : [[Le nationalisme des expositions du XIXe siècle est aussi couplé à une volonté d'unification du monde]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Expositions du XIXe siècle]]
Date : 2024-01-06
***

En cette fin du siècle, la mondialisation commence à vraiment s'imposer. Une volonté partagée par de nombreuses figures du siècle comme Victor Hugo, Napoléon III et Saint-Simon souhaite voir l'unification du monde. 

Werner Plum parle de luttes pacifiques. C'est en tout cas un oxymore retrouvé souvent par les historiens et écrivains de l'époque. On comprend que l'idée est de séduire par le soft power, de gagner par le prestige. 
## Sources
[[Des luttes pacifiques. Les expositions de la seconde moitié du XIXe siècle]]