Titre : [[Conception avec l'État de l'Exposition de 1881]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Exposition internationale d'Électricité de Paris en 1881]]
Date : 2024-01-28
***

Adolphe Cochery devient ministre des Postes et Télégraphes en 1879. Il semble être proche du "lobby" électrique (Voir [[Création d'un véritable lobby électrique de 1875 à 1881 selon Beltran et Carré]]) qui s'est formé depuis 75
En octobre 80, il remet une lettre au Président Jules Grévy suggérant la tenue d'une Exposition dédiée aux nouvelles innovations du domaine.
Des moyens sont dégagés, du public comme du privé. 
En moins d'un an, les divers organismes nécessaires sont dégagés. Jules Grévy nomme Gaston Berger.
Financement p64 !
## Sources
[[La fée et la servante]]