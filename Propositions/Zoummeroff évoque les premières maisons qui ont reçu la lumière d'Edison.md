Titre : [[Zoummeroff évoque les premières maisons qui ont reçu la lumière d'Edison]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Thomas Edison]]; [[J.-P. Morgan]]
Date : 2024-02-02
***

Edison voulait éclairer tout Manhattan avant 83. La première maison fut celle de J.P. Morgan, puis la New York Herald. En mai, c'était les maisons des 3 Vanderbilt. Morgan eut des problèmes, murs et tapis brulés, bruit et fumée. Il dit "on ne fait pas de fumée sans feu."
## Sources
[[Edison, l'homme aux 1093 brevets]]