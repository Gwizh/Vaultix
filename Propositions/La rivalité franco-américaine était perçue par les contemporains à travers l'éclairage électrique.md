Titre : [[La rivalité franco-américaine était perçue par les contemporains à travers l'éclairage électrique]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lumière électrique]]
Date : 2024-01-28
***

Dans son essai sur l’Exposition internationale de 1889, Figuier révèle que les rivalités sous-jacentes entre les États-Unis et la France étaient bien perceptibles dans la conception de l’éclairage électrique, et faisait la liaison entre l’électricité et le chauvinisme français, caractérisant l’électricité comme « l’événement pacifique le plus colossal du XIXe siècle, celui qui, pour relever le juste prestige de notre patrie, a fait plus qu’une guerre victorieuse »  [[57]Louis Figuier, « L’Exposition universelle de 1889 », L’Année…](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no57).
## Sources
[[Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité]]