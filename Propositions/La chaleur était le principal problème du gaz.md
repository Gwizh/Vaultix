Titre : [[La chaleur était le principal problème du gaz]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Éclairage au gaz]], [[Lumières et théâtres]]
Date : 2024-01-30
***

La fréquentation des théâtres causait des maux de tête et des nausés -> +15 à 38°C à cause de l'éclairage !
Aller chercher 1 livre en haut de la bibliothèque -> température de four
Combustion -> acide carbonique, eau, souffre et ammoniaque -> toxique
## Sources
[[La Nuit Désenchantée]]