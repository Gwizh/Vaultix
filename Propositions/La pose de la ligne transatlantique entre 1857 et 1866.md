Titre : [[La pose de la ligne transatlantique entre 1857 et 1866]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Télégraphie]]
Date : 2024-01-27
***

On trouva dans les années 40 la gutta-percha, substance semblable au caoutchouc, qu'on utilisa vite pour isoler et protéger les cables. C'est von Siemens qui met au point la machine pour enrober les cables. En 57, le projet est lancé mais est interrompu en 58 par la guerre de Sécession. Il est reprit en 65 et achevé en 66. On a toute l'histoire dans les journaux 
-> Cyrus Field.
## Sources
[[La fée et la servante]]