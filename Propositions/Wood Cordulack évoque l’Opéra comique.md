Titre : [[Wood Cordulack évoque l’Opéra comique]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[L'incendie de l'Opéra comique]], [[Lumières et théâtres]]
Date : 2024-01-28
***

L’année 1887 marqua un tournant dans l’histoire de l’électricité à cause de l’horrible incendie qui se déclara le 25 mai à l’Opéra comique. En l’espace de quelques mois, pratiquement tous les théâtres avaient installé l’éclairage électrique par mesure de sécurité pour se prémunir contre les risques de l’éclairage au gaz.

La source citée est celle-ci :

[https://www.persee.fr/docAsPDF/hes_0752-5702_1985_num_4_3_1402.pdf](https://www.persee.fr/docAsPDF/hes_0752-5702_1985_num_4_3_1402.pdf)

## Sources
[[Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité]]