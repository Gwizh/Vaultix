Titre : [[Spinoza aurait rejoint Breaugh sur la plèbe]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Spinoza]], [[Martin Breaugh]]
Date : 2023-12-09
***

La pensée de la plèbe heurte la logique délibératrice car elle a trait immédiatement aux affects. Ça rapproche le concept spinoziste de multitude. C'est l'unité d'une pluralité réelle.
## Sources
[[Spinoza et les passions du social]]