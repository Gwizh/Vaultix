Titre : [[Méfiance passagère face au tram électrique et au métro chez les Parisiens du XIXe]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Transports au XIXe siècle]]
Date : 2024-01-28
***

On se moque des trams qui passent vide mais lorsqu'on est en retard ça devient intéressant. C'est ainsi qu'on apprivoise la vitesse.
Pourtant, il y a eu de nombreux accidents comme celui de 1903 !
--> Rapprochement avec l'Opéra Comique
## Sources
[[La fée et la servante]]