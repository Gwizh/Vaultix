Titre : [[La notion controversée de totalitarisme dans le socialisme d'Europe de l'Est]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Totalitarisme]], [[URSS]]
Date : 2023-12-07
***



Il faut lire ce livre qui date d'il y a 50 ans mais qui explore cette notion et la controverse qu'il y a eu dans les années 60. C'est un anti-socialiste qui l'a écrit.
https://www.chasse-aux-livres.fr/prix/0333113128/totalitarianism-leonard-schapiro?query=Leonard%20Schapiro%2C%20Totalitarianism

## Sources