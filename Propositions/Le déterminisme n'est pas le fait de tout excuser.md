Titre : [[Le déterminisme n'est pas le fait de tout excuser]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Déterminisme]], [[Frédéric Lordon]]
Date : 2023-12-09
***

"Si tout est écrit, pourquoi agir et si j'ai ça, c'est parce que j'étais déterminé"
#Alire [[Bernard Lahire]], Pour la sociologie
Nous sommes puissants, nous affectons le monde autour. Il ne faut pas comprendre le déterminisme comme un abandon. Il est un moyen d'entendre la complexité du monde alors ne faisons pas exprès de rendre ce concept simple. Comme nous affectons, nous pouvons travailler à ce que certains affects touchent les gens. Comme nous ne pouvons pas entendre la complexité, nous ne savons pas si notre présence dans une situation (pourtant typique), pourrait provoquer une réaction bénéfique ou mauvaise.

## Sources