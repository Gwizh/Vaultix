Titre : [[Projet de Tour Soleil de Jules Bourdais, alternative rejetée de la Tour Eiffel]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Exposition universelle de 1889]], [[Lumière électrique]]
Date : 2024-01-11
***

360m de haut. 
But : éclairer tout Paris pas dans chaque rue mais depuis le haut. 
On pensa ça possible, la Tour Eiffel semblait en tout cas tout aussi farfelue.
Ce projet a été rejeté car on craignait d'être ébloui par autant de lumière centralisée
## Sources
[[La Nuit Désenchantée]]