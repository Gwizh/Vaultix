Titre : [[Le capitalisme libéral a pu offrir ce que la dictature du prolétariat devait organiser selon Bookchin]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Murray Bookchin]], [[Capitalisme]], [[Dictature du prolétariat]]
Date : 2024-01-19
***

Le but de la période de transition était de supprimer la contre-révolution et aussi de permettre l'abondance des conditions matérielles. Or le capitalisme a su organiser cette dernière grace aux technos et à la logistique. Il a aussi nationalisé, laissé fondre l'État dans certains secteurs, il a su proposer des avantages sociaux (tout ça sous des formes qu'on n'accepte pas mais ça pose quand même problème).
## Sources
[[Écoute, camarade !]]