Titre : [[Edison lors de l'exposition de 1889 par Wood Cordulack]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Thomas Edison]], [[Exposition universelle de 1889]]
Date : 2024-01-28
***

Une photographie de l’Exposition de 1889, rappelle l’exposition d’Edison qui comprenait une copie de grande dimension du tableau de Robert Outcault de 1880 montrant Menlo Park avec une ampoule à incandescence géante superposée au paysage.
L’exposition comportait aussi les drapeaux américain et français illuminés avec un portrait en buste de l’inventeur américain surmonté de l’aigle américain et l’inscription « Edison 1889 » dans des ampoules allumées.
## Sources
[[Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité]]