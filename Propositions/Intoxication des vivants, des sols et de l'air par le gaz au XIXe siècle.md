Titre : [[Intoxication des vivants, des sols et de l'air par le gaz au XIXe siècle]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Noeuds/Gaz]]
Date : 2024-01-30
***

Intoxication si gaz laissé la nuit. Etudier l'emplacement des usines pour ne pas polluer atteignent des habitants de même pour les rivières (aval). Mais pollution du sol par les conduites non étanches. Noircissement du sol et odeur, vie sous terraine impossible. Souffre et ammoniaque.
## Sources
[[La Nuit Désenchantée]]