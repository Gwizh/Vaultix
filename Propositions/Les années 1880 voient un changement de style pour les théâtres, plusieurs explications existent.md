Titre : [[Les années 1880 voient un changement de style pour les théâtres, plusieurs explications existent]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lumières et théâtres]]
Date : 2024-01-28
***

[Wood Cordulack](https://www.cairn.info/publications-de-Shelley-Wood-Cordulack--112345.htm) nous dit sur les théâtres :

“Bien que ceci n’ait pas encore été confirmé auparavant, cette époque du début de l’électrification coïncida avec ce que plusieurs érudits décrivirent comme une nouvelle période dans l’art de Chéret. Le changement de style qu’il opéra au début des années 1880 est caractérisé par une simplification et une absence de relief. On a soutenu que ceci était certainement une conséquence de l’influence des estampes japonaises mais il est également très probable que ce fut le résultat de l’intensité de la nouvelle forme de lumière.”

Cela semble aller dans le sens de Schivelbusch.
## Sources
[[Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité]]