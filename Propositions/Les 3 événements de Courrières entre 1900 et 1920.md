Titre : [[Les 3 événements de Courrières entre 1900 et 1920]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Mines]], [[Grisou]]
Date : 2024-03-27
***

Il y a eu le coup de grisou en 1906 et ses 1099 morts, la destruction en 1914 de la mine par l'Allemagne et après la reconstruction la réaction à la contraction du marché charbonnier.
## Sources