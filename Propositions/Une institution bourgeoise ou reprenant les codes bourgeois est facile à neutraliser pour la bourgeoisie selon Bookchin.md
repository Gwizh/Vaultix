Titre : [[Une institution bourgeoise ou reprenant les codes bourgeois est facile à neutraliser pour la bourgeoisie selon Bookchin]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Murray Bookchin]], [[Parti]]
Date : 2024-01-19
***

Si la bourgeoisie capture la direction d'une institution centralisée type bourgeoise, elle peut la neutraliser. Si le parti est habitué à l'obéissance, les membres seront d'autant moins à même de réagir !
## Sources
[[Écoute, camarade !]]