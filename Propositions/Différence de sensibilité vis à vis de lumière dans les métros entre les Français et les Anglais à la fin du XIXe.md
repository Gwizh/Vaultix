Titre : [[Différence de sensibilité vis à vis de lumière dans les métros entre les Français et les Anglais à la fin du XIXe]]
Type : Proposition
Tags : #Proposition 
Ascendants : [[Éclairage]], [[Transports au XIXe siècle]]
Date : 2024-01-28
***
Il fallait aux Parisiens de la lumière dans le métro là où les Londoniens s'accommodaient de l'obscurité.
## Sources
[[La fée et la servante]]