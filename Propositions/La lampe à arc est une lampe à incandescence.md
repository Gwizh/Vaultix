Titre : [[La lampe à arc est une lampe à incandescence]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lumière électrique]]
Date : 2024-01-30
***

L'arc produit de la lumière mais le gros de l'éclairage venait du chauffage des électrodes de charbon, il y a combustion. On a ensuite mis le vide dans ces lampes pour ne pas avoir cette destruction. Inventée en 1830 ?
-> Vraiment trop clair pour le coup.
## Sources
[[La Nuit Désenchantée]]