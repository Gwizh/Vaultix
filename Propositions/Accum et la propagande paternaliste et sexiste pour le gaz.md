Titre : [[Accum et la propagande paternaliste et sexiste pour le gaz]]
Type : Proposition
Tags : #Proposition
Ascendants :
Date : 2024-01-21
***

Comme il y avait littéralement des robinets de gaz, ils pouvaient rester allumés et répandre le gaz et potentiellement causer des explosions. Accum, propagandiste du gaz en profitait pour vanter les mérites d'un père assurant la fermeture du gaz pour la famille.
## Sources
[[La Nuit Désenchantée]]