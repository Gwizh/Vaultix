Titre : [[La lumière à incandescence au gaz, come-back du gaz en 1886]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Éclairage au gaz]]
Date : 2024-01-30
***

Auer von Welsbach inventa en 1886 une lampe qui se servait d'un bec Bunsen pour pousser à incandescence un alliage. La lumière était très intense et ça consommait bcp moins de gaz que la flamme. Concurrent sérieux à l'électrique.
Pourtant la tech existait depuis 1830 mais il fallait le succès de l'élec pour que ça fonctionne.
Effet navire à voile. 
En 1900, des interruptions pour le gaz ont été inventé pour concurrencer l'élec.
## Sources
[[La Nuit Désenchantée]]