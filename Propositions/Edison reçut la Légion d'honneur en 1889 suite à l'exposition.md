Titre : [[Edison reçut la Légion d'honneur en 1889 suite à l'exposition]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Thomas Edison]], [[Exposition universelle de 1889]]
Date : 2024-01-28
***

```
La presse continua de rendre compte de l’énorme battage publicitaire qui entoura l’arrivée d’Edison à Paris en août de cette année, y compris les délégations officielles, les interviews, les foules et l’écharpe de la Légion d’honneur remise par le président français ; ainsi que le déjeuner à la tour Eiffel avec une invitation d’Eiffel lui-même. La photo d’Edison fut publiée dans le numéro du 28 août 1889 du _Bulletin officiel de l’Exposition universelle,_ mais les articles de presse n’étaient pas tous très flatteurs à son égard [[52]](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no52)[[52]Voir la brève discussion dans Les dossiers du Musée d’Orsay,…](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no52). Lors de sa participation à l’Exposition, Edison acheta à A. Bordiga une statue nommée « _Le Génie de l’électricité_ », qui brandissait et célébrait le triomphe de l’ampoule à incandescence. Edison exposa cette pièce bien en vue dans sa bibliothèque de West Orange dans le New Jersey, où elle est toujours visible.
```

On peut aussi le lire p343 de [[Thomas Edison, L'homme aux 1093 brevets]]
## Sources
[[Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité]]