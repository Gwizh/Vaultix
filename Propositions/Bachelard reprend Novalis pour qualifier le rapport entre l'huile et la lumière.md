Titre : [[Bachelard reprend Novalis pour qualifier le rapport entre l'huile et la lumière]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Gaston Bachelard]], [[Novalis]], [[Lampe à huile]]
Date : 2024-01-21
***

L'huile est la matière même de la lumière. L'homme libérer avec la flamme la lumière emprisonnée dans la matière. Le pétrole est aussi de l'huile pétrifiée.
## Sources
[[La flamme d'une chandelle]]