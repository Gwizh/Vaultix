Titre : [[Gaz et Rail perçus par certains au XIXe siècle comme une perte de liberté individuelle et rattachement à des monopoles]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Noeuds/Gaz]], [[Ferroviaire]]
Date : 2024-01-21
***

Deux techs très liées par leur proximité technologique mais aussi par le fait nouveau qu'il fallait se connecter à un réseau. Réseau qui était mis en place de très loin. Généralement l'État contrôlait (ou essayait en tout cas) ces monopoles
En France, le département de la Seine a imposé d'avoir une seule compagnie par rue ! 1839, Paris découpée en arrondissements donnés aux métropoles.
## Sources
[[La Nuit Désenchantée]]