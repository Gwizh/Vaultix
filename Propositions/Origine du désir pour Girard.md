Titre : [[Origine du désir pour Girard]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Désir]], [[René Girard]], [[Eva Debray]]
Date : 2023-12-09
***

Le désir vient quand on voit un autre désirer et posséder un objet. C'est l'interdiction du partage qui provoque le désir. Différence entre la plénitude de l'un et le vide de l'autre. On imagine l'autosuffisance de l'autre.
## Sources
[[Spinoza et les passions du social]]