Titre : [[Évolution de la scène au XIXe du fait de l'éclairage]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lumières et théâtres]]
Date : 2024-01-30
***

Il faut d'abord comprenre qu'on n'arrivait pas à éclairer correctement les scènes avant le XIXe. On note la frustration des pros qui sont déçus des technos disponibles. Mais lorsque les technos arrivent, les changements sont marquants !
Les couleurs changent et il s'agit alors de choisir si on s'y fait ou si on adapte pour aue ça rende comme avant. 
Or l'esthétique bourgeoise diffère de l'aristocratie, la sobriété s'oppose au faste. 
Et Diderot note26. De même les décors, devenus trop éclairés perdent en reliefs et en illusion. Un plus gros travail était alors nécessaire note28.
## Sources
[[La Nuit Désenchantée]]