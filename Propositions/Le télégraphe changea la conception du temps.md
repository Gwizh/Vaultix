Titre : [[Le télégraphe changea la conception du temps]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Télégraphie]]
Date : 2024-01-27
***

Le plus gros changement qu'a causé le télégraphe fut pour la presse et la communication. S'instaure alors une véritable chasse à la nouvelle. En 71, le Parlement est connecté au télégraphe pour retransmettre les comptes rendus. 75 pour le Congrès de Versailles. Début du reporter
## Sources
[[La fée et la servante]]