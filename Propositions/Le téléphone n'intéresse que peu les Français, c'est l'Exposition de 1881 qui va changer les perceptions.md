Titre : [[Le téléphone n'intéresse que peu les Français, c'est l'Exposition de 1881 qui va changer les perceptions]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Téléphone]], [[Exposition internationale d'Électricité de Paris en 1881]]
Date : 2024-01-28
***

Les Français furent peu réceptifs à l'idée de dialoguer en direct (contrairement aux américains, anglais et allemands). L'idée qui a été utilisée pour susciter l'attention fut de transmettre en direct des opéras.
La marche du progrès et les idéaux de démocratisation furent utilisés pour montrer les capacités égalisatrices de cette technologique
Citations p50 !
## Sources
[[La fée et la servante]]