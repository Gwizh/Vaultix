Titre : [[Concept de série chez Sartre]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Jean-Paul Sartre]]
Date : 2023-12-13
***

La série c'est pour Sartre l'unité collective du hasard, regroupement de gens au guichet de la poste à la même heure par exemple. Mus par de mêmes besoins et sûrement contraints de manière similaire. La série est une unité d'impuissance
## Sources
[[Spinoza et les passions du social]]