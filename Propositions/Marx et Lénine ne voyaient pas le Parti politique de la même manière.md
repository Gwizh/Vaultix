Titre : [[Marx et Lénine ne voyaient pas le Parti politique de la même manière]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Communisme]], [[Lénine]], [[Marx]], [[Parti]]
Date : 2023-12-07
***

La source n'évoque pas trop les différences mais c'est tout de même super intéressant !

Nous pouvons apparemment le lire sur Pages Choisies Pour Une Ethique Socialiste [ici](https://www.chasse-aux-livres.fr/prix/F035234798/pages-pour-une-ethique-socialiste-choisies-traduites-et-presentees-par-maximilien?query=Pages%20choisies%20pour%20une%20%C3%A9thique%20socialiste)
## Sources
[[Le parti polymorphe en Europe de Est]]