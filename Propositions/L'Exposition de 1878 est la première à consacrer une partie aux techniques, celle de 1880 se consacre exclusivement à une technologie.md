Titre : [[L'Exposition de 1878 est la première à consacrer une partie aux techniques, celle de 1880 se consacre exclusivement à une technologie]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Expositions du XIXe siècle]], [[Exposition internationale de Paris de 1878]], [[Exposition universelle de 1880]]
Date : 2024-01-06
***

> Après l’Exposition universelle de 1878, dans un souci d’approfondissement pédagogique et de renouvellement, les autorités de l’Union centrale décident d’organiser des « expositions technologiques ». Celles-ci présentent les matières premières, les outils de travail et un ensemble d’œuvres remarquables du point de vue de l’art et de la technique.

Celle de 1880, la première « exposition technologique », est consacrée au métal avec l’orfèvrerie, la joaillerie, la bijouterie, les bronzes d’art et d’ameublement, ainsi que la fonte.
## Sources
[[Des luttes pacifiques. Les expositions de la seconde moitié du XIXe siècle]]