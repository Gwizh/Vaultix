Titre : [[Bachelard utilise les mots de Bosco pour redémontrer l'importance de l'attention qu'on porte aux objets]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Gaston Bachelard]], [[Henri Bosco]]
Date : 2024-01-21
***

Le poêle est pour Bachelard qq'1 qui se pose sur les choses, qui étudie et décrit nos affections, notre séduction. Il critique l'ustensilité, trop froid.
Bosco pense la lampe comme une créature et comme un être créant. La lampe pense à nous.
"Donnez des qualités aux choses, donnez du fond du cœur, leur juste puissance aux êtres agissants et l'univers resplendit"
## Sources
[[La flamme d'une chandelle]]