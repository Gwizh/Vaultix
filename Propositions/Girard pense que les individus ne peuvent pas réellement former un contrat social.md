Titre : [[Girard pense que les individus ne peuvent pas réellement former un contrat social]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Contrat social]], [[Raison]], [[Passion]], [[Eva Debray]] [[René Girard]]
Date : 2023-12-09
***

Un contrat social enraciné dans la "raison", le "bon sens", la "bienveillance". L'auto-contrainte des individus ou des institutions est impossible à bien des niveaux. On ne peut que produire des affects. Il ne faut pas fonder la politique sur la raison mais sur la passion.
## Sources
[[Spinoza et les passions du social]]