Titre : [[La réflexivité ne sort pas du déterminisme]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Libre-arbitre]], [[Déterminisme]], [[Frédéric Lordon]]
Date : 2023-12-09
***

Réfléchir à nos actions n'est pas en soi du libre-arbitre. Le fait d'être conduit à la réflexion est déterminée. Au final, l'action n'en est que mieux réfléchie mais c'est tout.
## Sources
[[Spinoza et les passions du social]]