MOC : [[Sociologie des techniques]]
Type : Note permanente
Titre : [[Les savoirs ne sont pas permanents]]
Date : 2023-11-08
Tags : #EnCours 
***

Nous pensons que découvrir ou inventer une technologie est permanent. Qu'une fois fait, cela ne se perd plus, comme dans un jeu vidéo. Pourtant, nous avons de nombreux exemple qui montrent que nous n'avons qu'un ensemble défini de technologies que notre société actuelle peut réellement appliquer et que cet ensemble augmente et diminue constamment, volontairement ou non. En réalité, à mesure que nous inventons et utilisons de nouvelles technologiques, nous oublions celles qui ne nous "servent plus", même lorsque elles sont en fait la fondations de notre monde actuelle.

Parmi les techniques que nous avons perdus puis retrouvés, il y a :

Il y a ensuite pour les techniques actuellement perdues :

Il y a aussi les techniques que nous avons volontairement "désinventées" :


## Références 
[[Les inventions sont désinventées]]
[[Nous perdons les techniques au fil du temps]]

## Mises en lien