Titre : [[L'incendie de l'Opéra comique de 1887 tombe en plein remaniement ministériel]]
Type : Proposition
Tags : #Proposition
Ascendants : [[L'incendie de l'Opéra comique]]
Date : 2023-12-29
***

Le 17 mai 1887, René Goblet remet la démission du Gouvernement au président de la République, Jules Grévy.

Le 25 mai 1887, l'incendie de l'Opéra comique survient.

Le 30 mai 1887, Jules Grévy nomme Maurice Rouvier président du Conseil des ministres. Le nouveau gouvernement est formé.
## Sources
