Titre : [[Les dons pour les victimes de l'opéra comique font réagir suite aux récents morts dans les mines de Saint-Etienne]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[L'incendie de l'Opéra comique]]
Date : 2023-12-29
***

```
Certains considérèrent en effet que la justice sociale était bien mal respectée : le journal satirique _Le Grelot_ fit à la une, sous le titre « Le cabotinage de la charité », un cinglant parallèle entre les largesses des mélomanes parisiens et la souscription, peu suivie d’effet, lancée en faveur des soixante-dix-neuf mineurs, victimes en mars d’un coup de grisou à Saint-Etienne.
```

Voici la description de cet incident
```
Le 1er mars 1887 à 8 h 20, explosion au [puits Chatelus](http://noms.rues.st.etienne.free.fr/rues/chatelus-puits.html) n°1 appartenant à la compagnie de Beaubrun et situé rue Marthourey près du Clapier. Le compte-rendu du procès indique 78 morts, 13 victimes ont été retirées, un blessé a succombé le 6 mars et 64 corps sont encore ensevelis lors du procès-verbal du 18 mars qui révèle leurs noms publiés à l'état-civil le 22 mai. Quelques corps seront retrouvés et identifiés grâce à leur lampe. On parle aussi de la catastrophe du puits de la Culatte car les deux puits communiquaient, le puits Chatelus étant le puits d'extraction, celui de la Culatte étant le puits d'aération par lequel les mineurs descendaient.
```

Il y aura un autre accident juste après : 
```
Le 3 juillet 1889 à 11 h 45, explosion [au puits Verpilleux](http://noms.rues.st.etienne.free.fr/rues/verpilleux-puits.html), sur 214 mineurs présents, 5 sont sortis indemnes, 3 blessés sur 13 ont survécu, il y a donc eu 206 morts auxquels il faut ajouter 1 mort et 3 blessés parmi les sauveteurs. [En savoir plus sur cette catastrophe](http://noms.rues.st.etienne.free.fr/rues/catastrophe1889.html).
```
## Sources
https://musee-mine.saint-etienne.fr/sites/default/files/ckeditor_uploads/dossier_les_dangers2017.pdf