Titre : [[Fox note la supériorité des lampes à incandescence et du système d'Edison après l'Exposition de 1881]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Thomas Edison]], [[Exposition internationale d'Électricité de Paris en 1881]]
Date : 2024-02-02
***
Alors qu'il y avait peu d'opinions de formulées sur les différents systèmes, l'expo semble avoir changé les avis.
La lampe à incandescence s'est montré digne de concurrencer les lampes à gaz et les bougies de Jablo. Mais il semble que la supériorité d'Edison s'est aussi établie par rapport à ses rivaux : Maxim, Swan et Lane-Fox.
## Sources
[[Thomas Edison's Parisian campaign]]