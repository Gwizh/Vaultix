Titre : [[Murray Bookchin souhaite transcender le marxisme et donc suivre la dialectique de Marx]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Murray Bookchin]], [[Karl Marx]]
Date : 2024-01-19
***

Il considère que le marxisme ne décrit plus assez le monde d'aujourd'hui mais c'est plutôt le fait de suivre les analyses passées qui lui pose problème. Il veut utiliser la dialectique comme Marx l'a fait avec Hegel.
## Sources
[[Écoute, camarade !]]