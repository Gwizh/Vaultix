Titre : [[Bookchin questionne la dialectique marxiste de la transition entre les sociétés]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Murray Bookchin]], [[Karl Marx]]
Date : 2024-01-19
***

Bookchin note que Marx étudie la transition d'une société de classes vers une société d'autres classes (féodalisme -> bourgeoisie) par l'opposition entre agriculture et artisanat. Bookchin s'interroge alors sur l'utilité de ce raisonnement pour étudier la transition d'une société de classes à une société sans classes. Comment on transitionne de la dictature du prolétariat à la suite en somme.
## Sources
[[Écoute, camarade !]]