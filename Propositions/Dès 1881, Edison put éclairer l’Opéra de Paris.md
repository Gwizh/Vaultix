Titre : [[Dès 1881, Edison put éclairer l’Opéra de Paris]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Thomas Edison]], [[Lumière électrique]], [[Lumières et théâtres]]
Date : 2024-01-28
***

En 1881, l’Opéra de Paris ouvrit la voie à une conversion complète à l’électricité en installant des lampes Edison dans le foyer. En 1883, l’Opéra fit d’autres installations électriques destinées à préserver les peintures du plafond exécutées par Paul Baudry. Cet effort fut acclamé comme un « succès pour la société Edison »

Voir à ce sujet le document récapitulatif :

[P84.18 : p.585 - vue 589 sur 646 - Cnum](https://cnum.cnam.fr/pgi/fpage.php?P84.18/589/80/646/0/0)
## Sources
[[Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité]]