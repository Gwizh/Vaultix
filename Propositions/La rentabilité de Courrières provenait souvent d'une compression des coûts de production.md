Titre : [[La rentabilité de Courrières provenait souvent d'une compression des coûts de production]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Courrières]]
Date : 2024-03-27
***

Les actionnaires n'augmentaient pas le capital de l'entreprise car ils ne voulaient pas perdre en dividendes. Dans un sens, l'entreprise était trop rentable. Il fallait alors prendre des emprunts et réduire les couts. Construction de fosses simples (4/11 en 1905) au lieu des fosses doubles plus sécurisées. Toujours des lampes à flamme nue dans les parties nous grisouteux. Les bois usagés sont abandonnés. L'arrosage du front de taille et des galeries n'est pas pratiqué. Travail de 12h attesté et permis par des dérogations.
## Sources
[[Une entreprise face à la gestion de « risques majeurs »]]