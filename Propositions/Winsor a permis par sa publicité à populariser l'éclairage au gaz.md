Titre : [[Winsor a permis par sa publicité à populariser l'éclairage au gaz]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Frederick Albert Winsor]]
Date : 2024-01-21
***

Connaissances assez limitées en chimie et mécanique mais doué en conférences !
Il a rendu l'éclairage au gaz séduisant malgré ses descriptions extravagantes et trompeuses. Il connu une quasi aussi grande renommée que Lebon.
Popularisa le concept de tuyaux pour le gaz en faisant 1 rapprochement avec l'eau (acheminée dans les maisons dès 1726 au Royaume-Uni)
## Sources
[[La Nuit Désenchantée]]