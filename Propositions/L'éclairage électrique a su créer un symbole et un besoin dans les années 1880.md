Titre : [[L'éclairage électrique a su créer un symbole et un besoin dans les années 1880]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lumière électrique]]
Date : 2024-01-28
***

Démonstration politique au 15 aout napoléonien et au 14 juillet républicain. Représente les Lumières pour ces derniers.
C'est aussi un marqueur social jusqu'à lasser. n4
De nouveau la citation comme quoi l'électricité illumine - n7
## Sources
[[La fée et la servante]]