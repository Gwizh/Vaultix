Titre : [[Concept de praxis chez Sartre]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Jean-Paul Sartre]]
Date : 2023-12-13
***

"C'est un projet organisateur dépassant la condition matérielle vers une fin..."
Possible incompatibilité avec le rejet de la finalité de Spinoza.
Rappel : Spinoza s'oppose à l'explication de la nature par les causes finales. Seul le conatus est une finalité.
La "mens" = conscience chez Sartre.
## Sources
[[Spinoza et les passions du social]]