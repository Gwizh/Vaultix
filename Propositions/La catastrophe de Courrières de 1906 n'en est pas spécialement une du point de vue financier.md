Titre : [[La catastrophe de Courrières de 1906 n'en est pas spécialement une du point de vue financier]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Mines]], [[Sécurité au travail]]
Date : 2024-03-27
***

Malgré les lourds investissements que l'entreprise a du faire pour construire de nouveaux tunnels, payer les indemnités et reprendre la production normale, l'entreprise ne fut pas tant impactée d'un point de vue financier.

Les dividendes par exemple n'ont que légèrement baissées. En à peine 4 ans la capitalisation était revenue au niveau d'avant la catastrophe. Parmi les investissements faits, il y a eu l'installation de certaines infrastructures plus sécurisées que le voulait la norme.
## Sources
[[Une entreprise face à la gestion de « risques majeurs »]]