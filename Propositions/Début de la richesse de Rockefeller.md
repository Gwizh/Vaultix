Titre : [[Début de la richesse de Rockefeller]]
Type : Proposition
Tags : #Proposition
Ascendants : [[John Davison Rockefeller]]
Date : 2023-12-27
***

Il a commencé par acheté pour qq milliers de dollars la Standard Oil en Ohio.
Il va s'arranger avec l'entreprises de chemins de fer pour avoir des prix plus bas que la trentaine de concurrent qu'il veut faire couler. En 2 ans, il va racheter ou couler toutes les raffineries de Cleveland puis va finir par contrôler 90% du pétrole américain. 

Un fois le monopole atteint, il a augmenté les prix.

Dans les années 1890, c'était l'homme le plus riche du monde mais aussi le plus détesté des Etats Unis.
## Sources
[[Arte - Capitalisme américain, le culte de la richesse (1-3)]]