Titre : [[La grève de Homestead est un symbole de la colère sociale américaine au moment du krash de 1893]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Politique américaine pendant la deuxième révolution industrielle]], [[Andrew Carnegie]], [[Syndicalisme]]
Date : 2023-12-27
***

Dans une période de colère sociale liée à une baisse de performance de l'économie américaine, les grèves se multiplient dans le pays.

La deuxième plus grande bataille du syndicalisme américain est alors la grève de Homestead. 
Le conflit opposait la Amalgamated Association of Iron and Steel Workers à la Carnegie Steel Company. Il a culminé avec une bataille entre les grévistes et des agents de sécurité privés le 6 juillet 1892, qui a mené à la défaite des grévistes et à une diminution de la volonté de syndiquer les travailleurs de l'acier. 15 morts sont comptés.

Publiquement, Carnegie est en faveur des syndicats. Il condamne l'utilisation de briseurs de grève et affirme à des associés qu'aucune aciérie ne mérite de verser une seule goutte de sang. Il défend qu'il n'a fait que protéger sa propriété privée.
## Sources