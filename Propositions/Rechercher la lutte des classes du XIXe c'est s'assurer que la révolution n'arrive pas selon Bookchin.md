Titre : [[Rechercher la lutte des classes du XIXe c'est s'assurer que la révolution n'arrive pas selon Bookchin]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Murray Bookchin]], [[Prolétariat]]
Date : 2024-01-19
***

Le prolétariat d'aujourd'hui n'est plus le même qu'au XIX. Nous avons l'abondance matérielle (+ ou -), nous avons en Europe de nombreux avantages sociaux dignes du communisme. Il faut constituer une image plus parlante aux masses d'aujourd'hui. Une sensation de dissonance avec ce que nous vivons et une image (le problème) de ce qui pourrait advenir.

## Sources
[[Écoute, camarade !]]