Titre : [[Il y a plusieurs interprétations sur le réel système politique en Europe de l'Est durant la Guerre Froide]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Noeuds/Socialisme|Socialisme]], [[Guerre froide]], [[Communisme]]
Date : 2023-12-07
***
Cela va vraiment dans tous les sens et il n'y a pas de vision claire aujourd'hui.
Certains proposent la théorie du Socialisme d'Etat (Naville et Lane).
D'autres le capitalisme d'Etat (Bettelheim, Voline, Cliff)
D'autres un état ouvrier bureaucratisé (Trotski)
D'autres un état fasciste (Plioutch) !

La source ici propose le parti polymorphe. 
## Sources
[[Le parti polymorphe en Europe de Est]]