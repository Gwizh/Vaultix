Titre : [[L'installation de la télégraphie électrique a été ralentie en France par la télégraphie aérienne et son utilisation]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Télégraphie]]
Date : 2024-01-27
***

Mais pas que. La lente appariation du rail en France en est aussi la cause.
Mais la télégraphique aérienne existait déjà depuis 1794. C'était un ensemble d'avant-postes munis de sémaphores. D'abord pour l'armée mais ensuite pour l'administration. La télégraphie inquiétait l'État, et si on coupait les fils ?
## Sources
[[La fée et la servante]]