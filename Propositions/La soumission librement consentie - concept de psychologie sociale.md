Titre : [[La soumission librement consentie - concept de psychologie sociale]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Psychologie]]
Date : 2023-11-02
***

La soumission librement consentie est la traduction de Robert-Vincent Joule et Jean-Léon Beauvois (1987) d'un concept de psychologie sociale (Compliance without pressure) introduit par Jonathan L. Freedman et Scott C. Fraser en 1966 pour décrire la conséquence d'un procédé de persuasion qui conduit à donner l'impression aux individus concernés qu'ils sont les auteurs de certaines décisions. De cette manière, une personne pourrait ainsi modifier son comportement, ses objectifs et ses choix avec le sentiment d'être responsable de ces modifications. 
## Sources


