Titre : [[Bachelard pense qu'on ne rêve pas en allumant la pièce immédiatement et uniformément avec l'électricité]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Gaston Bachelard]], [[Lumière électrique]]
Date : 2024-01-21
***

Bachelard a connu la transition de la lampe à huile à l'ampoule électrique. Cette lumière est administrée, nous sommes un sujet mécanique inclut dans un procédé mécanique. Nous ne sommes plus le seul sujet du verbe allumer. Entre l'espace sombre et l'espace tout-de-suite-clair, il n'y a plus qu'un instant, il n'y a plus qu'un geste, il n'y a plus de conscience, il n'y a plus de conscience, il n'y a plus l'"amitié attentive" qu'on donnait à cette action, à cette lumière.
## Sources
[[La flamme d'une chandelle]]