Titre : [[La course à l’audimat et le financement avec les pubs renforce la manipulation de la télévision]]
Type : Proposition
Tags : #Proposition
Ascendants :
Date : 2023-12-07
***

La course à l’audimat accélère continuellement. Les chaînes doivent toujours faire plus pour battre les concurrents, qui eux-même, en font plus en retour. On montre encore plus de faits divers, encore plus de sensationnel, on fait peur volontairement.

Les publicités fonctionnent bien avec ce système. Une fois l’attention acquise, elles sont plus efficaces. C’est donc non seulement une course à l’audimat mais aussi une compétition au temps de cerveau disponible. C’est doublement rentable !

On remarquera d’ailleurs que les deux grandes chaînes qui font la surenchère de ce type de sujet, TF1 et M6, ont quelque chose en commun : ce sont les deux seules chaînes privées en claires (hors TNT), autrement dit des chaînes qui se financent en grande partie par la publicité.

En ce qui nous concerne, la télévision n’est pas adaptée pour suivre les actualités. Dès qu’il y a de la publicité derrière, il y a des intérêts privés, et on doit donc se méfier du choix des sujets.
## Sources
[[@Horizon - La France a peur_ le syndrome du grand méchant monde,]]