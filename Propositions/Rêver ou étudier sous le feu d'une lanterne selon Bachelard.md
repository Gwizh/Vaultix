Titre : [[Rêver ou étudier sous le feu d'une lanterne selon Bachelard]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Gaston Bachelard]]
Date : 2024-01-21
***

Étudier sous le feu d'une lanterne c'est être constamment entre l'étude de l'objet éclairé (un livre par exemple) et la rêverie provoquée par le feu. Le temps qu'on passe à l'un n'est pas passé à l'autre. Dynamique intéressante entre les deux actions.
## Sources
[[La flamme d'une chandelle]]