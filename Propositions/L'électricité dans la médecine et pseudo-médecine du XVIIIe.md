Titre : [[L'électricité dans la médecine et pseudo-médecine du XVIIIe]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Électricité]]
Date : 2024-01-27
***

Il y a confusion entre magnétisme et électricité. Il y a des cures (soin collectif). "Toute maladie provient d'une obstruction à la circulation du fluide vital, donc la guérison est facile".
Le magnétisme est utilisé comme remède aux maladies nerveuses mais aussi aux problèmes d'érections. La volupté semble avoir une place importante dans ces sessions.
Le magnétiseur doit avoir un rapport physique avec le (souvent la) malade.
## Sources
[[La fée et la servante]]