Titre : [[Edison a eu du retard sur ce qu'il avait promis aux investisseurs à la fondation de son entreprise]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Thomas Edison]]
Date : 2024-01-10
***

Il a directement voulu apporter l'éclairage dans les foyers et a fondé l'Edison Electric Light Company en 1878 avec le soutien de riches investisseurs de New York. Il promit qu'il serait rapidement capable d'apporter l'éclairage électrique dans les rues de New York. Il fit tout de suite des démonstrations. [M. Akrich, M. Callon et B. Latour 1988] pour patienter les investisseurs et journalistes.

Et, en septembre 1882, avec beaucoup de retard sur le planning annoncé, tout un quartier d’affaires de New York était illuminé par du courant électrique provenant de la première centrale de production centralisée installée par Edison.
## Sources