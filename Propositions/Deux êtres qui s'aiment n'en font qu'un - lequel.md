Titre : [[Deux êtres qui s'aiment n'en font qu'un - lequel]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Féminisme]], [[Couple]], [[Mona Chollet]]
Date : 2023-12-19
***

Nancy Hudson : "Deux êtres qui s'aiment n'en font qu'un : lequel ?". Une idée cruelle mais juste qu'un couple crée un rabougrissement de l'identité. Elle trouve que 3 semaines par jour de solitude font beaucoup de bien. Pour ça il faut deux logements séparés (ou + héhé) et donc que chacun puisse s'en payer un. Pour un enfant, on peut imaginer que des parents plus heureux puisse aider, même avec deux foyers. Lire : *Un lit à soi* de Le Garrec
## Sources
[[Réinventer l'Amour]]