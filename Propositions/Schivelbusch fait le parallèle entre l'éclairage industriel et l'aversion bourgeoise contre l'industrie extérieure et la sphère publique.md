Titre : [[Schivelbusch fait le parallèle entre l'éclairage industriel et l'aversion bourgeoise contre l'industrie extérieure et la sphère publique]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lumière électrique]], [[Bourgeoisie]]
Date : 2024-01-30
***

Au XIXe seulement l'intérieur a commencé à être éclairé par l'extérieur du fait des lampes à arc particulièrement puissantes des grandes fenêtres. La protection face à cette intrusion, qui pouvait être perçue comme des regards, commença. De plus, l'éclairage au gaz ou électrique venait d'usines lointaines, le fichage de la consommation commença. On comprend alors la volonté d'autarcie et les stratégie de voilage.
## Sources
[[La Nuit Désenchantée]]