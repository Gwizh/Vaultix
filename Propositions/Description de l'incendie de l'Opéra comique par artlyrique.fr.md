Titre : [[Description de l'incendie de l'Opéra comique par artlyrique.fr]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[L'incendie de l'Opéra comique]]
Date : 2023-12-29
***

```
L'incendie de l'Opéra-Comique a été raconté par la presse quotidienne tout entière ; il nous suffira de rappeler qu'il a eu lieu subitement le mercredi 25 mai, au moment même de la représentation de Mignon, et pendant le premier acte de cette pièce. A 20 h 55, sous l'action d'un bec de gaz, le feu prit tout à coup aux frises de la scène ; en quelques secondes, les décors se trouvèrent enflammés, et le foyer ne tarda pas à acquérir une intensité et un développement que rien alors ne pouvait arrêter.

A 22 heures la toiture et le chapeau de la salle s'écroulent. Le feu fera rage jusqu'à 1 heure du matin, attirant quelques deux cent mille curieux, que l'infanterie et la Garde républicaine devront contenir. Au matin du 26 un spectacle de souffrance et d'horreur s'offre aux sauveteurs. Lourd bilan : cent dix morts environ dont seize membres du personnel (danseuses, choristes, ouvreuses...) et plus de deux cents blessés. Des scènes éprouvantes d'identification se déroulent aux ambulances ouvertes rue Drouot, à la Bibliothèque Nationale ainsi qu'à l'Hôtel-Dieu. La presse se déchaîne, les compositeurs et les chansonniers s'emparent des faits. Les chambres votent l'octroi de dons en aide aux familles des victimes et des galas de soutien s'organisent dans les théâtres. Des funérailles nationales pour les vingt-deux victimes non identifiées ou non réclamées ont lieu à Notre-Dame le 30 mai 1887.
```

## Sources
https://artlyriquefr.fr/dicos/Opera-Comique%20incendie.html