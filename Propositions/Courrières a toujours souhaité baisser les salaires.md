Titre : [[Courrières a toujours souhaité baisser les salaires]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Courrières]], [[Droit du travail]]
Date : 2024-03-27
***

Malgré les nombreuses grèves, les salaires ont été baissés dès qu'ils pouvaient. Il y a un recourt à l'immigration pour combler l'insuffisance de main-d'œuvre.
## Sources
[[Une entreprise face à la gestion de « risques majeurs »]]