Titre : [[Bourdieu n'est pas assez déterministe pour Lordon]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Pierre Bourdieu]], [[Frédéric Lordon]], [[Déterminisme]]
Date : 2023-12-09
***

Bourdieu n'est pas un pur déterministe, il a laissé une part de liberté à sa théorie, une part de l'homme qui échappe au déterminisme. Il a aussi laissé entendre qu'il y aurait plusieurs déterminismes.
## Sources
[[Spinoza et les passions du social]]