Titre : [[Organisme pratique chez Sartre]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Jean-Paul Sartre]]
Date : 2023-12-13
***

Conception marxiste du besoin couplé à la phénoménologie qui dément le besoin comme purement interne. Il y a toujours transcendance, l'organisme (vivant) pratique va vers l'extérieur, s'ouvre, pour répondre au besoin. Ce dépassement implique que pour persévérer dans son être, il faut le dépasser, le transcender. Nous avons à être son être.
## Sources
[[Spinoza et les passions du social]]