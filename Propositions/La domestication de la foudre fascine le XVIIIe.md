Titre : [[La domestication de la foudre fascine le XVIIIe]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Électricité]]
Date : 2024-01-27
***

L'expérience de l'électricité fascine les savants et élites en France.
Voir image p21
C'est un spectacle de cour. Louis XV fait passer "la commotion électrique à travers [80] militaires en même temps."
La Révolution fait un constat par Robespierre, celui que les Hommes vont dominer les cieux grâce à la puissance de la foudre.
Voir image p23 !
## Sources
[[La fée et la servante]]