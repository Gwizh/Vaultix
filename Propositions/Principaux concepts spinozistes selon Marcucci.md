Titre : [[Principaux concepts spinozistes selon Marcucci]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Spinoza]], [[Nicolas Marcucci]]
Date : 2023-12-10
***

Il liste certains concepts géniaux : dépassement de l'individualisme moderne, théorie naturaliste des affects / passions, critique du contractualisme, réalisme conflictuel venant de Machiavel, critique du finalisme, conception du droit naturel.
## Sources
[[Spinoza et les passions du social]]