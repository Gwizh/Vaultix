Titre : [[On considérait le gaz comme un déchet avant la révolution industrielle]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Noeuds/Gaz]], [[Révolution industrielle]]
Date : 2024-01-11
***

Dès 1691, ont prit conscience en Europe du gaz et sa combustibilité. On produisait même du gaz avec la cokéfaction de la houille, pour la production de goudron -> Derby
On ne savait simplement pas quoi faire avec ce gaz qui émanait de cette production.
## Sources
[[La Nuit Désenchantée]]