Titre : [[L’imagerie féminine de l’électricité influencée par la statue de la liberté]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Électricité]]
Date : 2024-01-28
***

Avant 1884, la lumière électrique était représentée avec une simple tour ou un phare. Mais petit à petit si je comprends bien, on commença à utiliser une métaphore féminine pour évoquer l’électricité. → [https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#pa17](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#pa17)

Les artistes français s’appropriaient donc graduellement des images réunissant les rayons de l’électricité et de la statue de la Liberté qui, ironiquement, ne fonctionnèrent jamais comme l’avait prévu Bartholdi, pour vendre la modernité.
## Sources
[[Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité]]