Titre : [[Les groupes d'avant garde de la gauche française n'a pas tout de suite suivi le mouvement de mai 68 selon Bookchin]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Murray Bookchin]], [[Mai 68]]
Date : 2024-01-19
***

Il y a eu un certain dédain de la plupart des groupes de type bolchevique français vis à vis des révoltes de mai 68. Le PCF tenta de manipuler les Assemblées de la Sorbonne en targant qu'il fallait une direction centralisée.
## Sources
[[Écoute, camarade !]]