Titre : [[Politique américaine pendant la deuxième révolution industrielle]]
Type : Proposition
Tags : #Proposition
Ascendants : [[États-Unis]], [[Deuxième révolution industrielle]]
Date : 2023-12-27
***
Les Républicains sont le parti de Lincoln, des industriels et du monde des affaires.
Les démocrates englobent tout le reste et notamment le Sud ségrégationniste et NY.

Le Sénat est alors un groupe de millionnaires. Ce sont eux qui contrôlent toutes les lois. Il n'y a pas de droit du travail, pas de contrôle des banques, du sexisme, du racisme et du travail des enfants.
## Sources
[[Arte - Capitalisme américain, le culte de la richesse (1-3)]]