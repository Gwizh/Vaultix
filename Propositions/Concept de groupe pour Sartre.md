Titre : [[Concept de groupe pour Sartre]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Jean-Paul Sartre]]
Date : 2023-12-13
***

Le groupe se définit par le praxis de l'auto-organisation. Toujours basé sur un collectif précédent qui l'engendre et le soutient. Une grève existe dans le contexte industriel.
## Sources
[[Spinoza et les passions du social]]