Titre : [[Jean-Marie Guyau et son esquisse d'une morale sans obligation ni sanction]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Jean-Marie Guyau]], [[Sociologie]]
Date : 2023-12-10
***

La morale ne peut pas être que philosophie nous dit-il. La science sociale doit pouvoir faire sans obligation ni sanction. Pas de métaphysique. L'utilitariste cherche un but et une finalité. Le sociologue cherche les infinies causes. Question de la source de l'obligation. Si ce n'est pas une rationalisation externe (naturel ou autoritaire), alors ça vient des existences individuelles.
## Sources
[[Spinoza et les passions du social]]