Titre : [[Girard et sa critique d'une conception monadique du comportement humain]]
Type : Proposition
Tags : #Proposition
Ascendants : [[René Girard]], [[Comportment]], [[Eva Debray]]
Date : 2023-12-09
***

"Monadique" signifie qu'on observe qu'1 seul élément de la globalité. La conception actuelle veut qu'on étudie un homme seul et qu'on détermine les interactions ensuite. Un malade serait un "monade" au sein du monde social. Le désir est typiquement vu comme "choisi".
## Sources
[[Spinoza et les passions du social]]