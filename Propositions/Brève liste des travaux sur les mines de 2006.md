Titre : [[Brève liste des travaux sur les mines de 2006]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Mines]]
Date : 2024-03-27
***

De nombreuses recherches ont été consacrées à l'histoire de bassins miniers : sans
être exhaustif, nous citerons les travaux de Marcel Gillet, d'Odette Hardy-Hémery et de
Louis Fontvieille sur le bassin du Nord-Pas-de-Calais, ceux de Rolande Trempé sur le
bassin houiller de Carmaux et plus récemment de Philippe Mioche, Xavier Daunalin et
Olivier Raveux sur le bassin des Bouches-du-Rhône. Les monographies d'entreprises
houillères sont moins nombreuses : la compagnie des mines de la Loire par Pierre
Guillaume, la Société des Mines de Lens par Olivier Kourchid, la Compagnie des mines
de la Grand-Combe par Jean-Michel Gaillard.
## Sources