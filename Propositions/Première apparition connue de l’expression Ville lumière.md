Titre : [[Première apparition connue de l’expression Ville lumière]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Ville Lumière]]
Date : 2024-01-28
***

Paris, incarnation de la « Ville Lumière » a une longue et fière histoire qui remonte jusqu’à 1470, date à laquelle le premier livre issu de la première presse typographique en France était consacré à Paris. « De même que le soleil répand partout la lumière, ainsi toi, ville de Paris, capitale du royaume, nourricière des muses, tu verses la science sur le monde. »
## Sources
[[Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité]]