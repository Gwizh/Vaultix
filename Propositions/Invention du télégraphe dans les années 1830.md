Titre : [[Invention du télégraphe dans les années 1830]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Télégraphie]]
Date : 2024-01-27
***

Le télégraphie a été permise par la pile de Volta et l'électroaimant. C'est aux US que le télégraphe fut inventé en 1830. C'est donc contemporain du rail !
Aux R-U, où les réseaux ferroviaire sont importants, la communication rapide de l'information est cruciale. C'est donc là-bas que se développe cette technologie. Le télégraphe électrique correspond à 1 besoin.
## Sources
[[La fée et la servante]]