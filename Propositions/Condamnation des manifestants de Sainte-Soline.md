Titre : [[Condamnation des manifestants de Sainte-Soline]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Sainte-Soline]]
Date : 2023-07-29
***

## Résumé
Quatre personnes ont été condamnées, jeudi 27 juillet, à des peines de prison à l’issue d’une longue audience sur des faits commis lors de la manifestation contre les mégabassines en mars dernier. 

## Référence
