Titre : [[État vis à vis du danger du gaz au XIXe siècle selon Schivelbusch]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Wolfgang Schivelbusch]], [[Éclairage au gaz]]
Date : 2024-01-21
***

Les explosions des réservoirs arrivaient régulièrement. 1862 à Paris et 1865 à Londres. Commission Congreve en 1823. Eloigner les usines à gaz et réduire la taille des récipients. Les industries du gaz se sont révoltées en évoquant la pression des contrôles, le viol de leur propriété privée et les conséquences économiques néfastes.
## Sources
[[La Nuit Désenchantée]]