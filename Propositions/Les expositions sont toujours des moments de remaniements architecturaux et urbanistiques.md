Titre : [[Les expositions sont toujours des moments de remaniements architecturaux et urbanistiques]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Expositions du XIXe siècle]]
Date : 2024-01-06
***

En tant que vitrines de pays en représentation, les Expositions universelles sont aussi des moments de profonds remaniements urbanistiques et architecturaux, que certains peintres se plaisent à illustrer, comme Georges Clairin avec _Les Fontaines lumineuses à l’Exposition de 1889_.
## Sources
[[Des luttes pacifiques. Les expositions de la seconde moitié du XIXe siècle]]