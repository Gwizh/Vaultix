Titre : [[Comprendre le déterminisme global pour être libre selon Guyau et Spinoza]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Spinoza]], [[Jean-Marie Guyau]]
Date : 2023-12-10
***

Il n'y a de libre que le monde entier, le déterminisme global. Tenter de le comprendre, c'est participer à la liberté. La science est la nécessité ne fait qu'un avec la liberté. Notre existence, notre pensée, notre désir dépendent de l'existence, la pensée, le désir du monde entier.
## Sources
[[Spinoza et les passions du social]]