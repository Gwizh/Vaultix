Titre : [[Zoummeroff note qu'Edison a fondé 3 entreprises et 1 usine fin 1881]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Thomas Edison]]
Date : 2024-02-02
***

Il parle de l'Edison Continental Company (200 000$), Société Elec Edison, Société Industrielle et Commerciale.
Achat d'une usine à Vitry-sur-Seine. Une société habilitée à prendre toutes les commandes en provenance de l'Europe Théâtre Brünn de Vienne, la raffinerie de sucre d'Anvers, le bureau central de Budapest, grande arcane commerciale à Milan.
## Sources
[[Edison, l'homme aux 1093 brevets]]