Titre : [[La maison Beau était en charge de l'éclairage de plusieurs théâtres dont l'Opéra-Comique]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lumière électrique]]
Date : 2024-02-11
***

La maison _Beau_ et _Bertrand-Taillet_ employait 60 ouvriers en 1885. Elle était alors chargée de l’électricité de plusieurs théâtres, notamment l’Opéra et l’Opéra-Comique.
« la maison de M. Beau est renommée à juste titre pour tout ce qui a trait à l’éclairage ; elle est l’une des premières qui s’y soient consacrées et qui, à l’apparition du gaz, construisit des appareils étudiés en vue de cette lumière alors nouvelle. C’est encore la maison Beau [en fait _Lecoq et Beau_] qui, en 1877, s’occupa de rendre pratique l’éclairage électrique, et, en 1889, elle exposait dans la classe du bronze (où elle était hors concours, M. Beau faisant partie du jury d’une autre classe) une série de modèles spéciaux, qui par leur appropriation judicieuse au nouveau mode d’éclairage, étaient, pour cette époque, un progrès réel »[8](https://journals.openedition.org/sabix/991#ftn8).
## Sources
[[Henri Beau, pionnier de l’éclairage électrique]]