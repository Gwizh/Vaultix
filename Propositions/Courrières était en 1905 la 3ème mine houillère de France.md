Titre : [[Courrières était en 1905 la 3ème mine houillère de France]]
Type : Proposition
Tags : #Proposition 
Ascendants : [[Mines]]
Date : 2024-03-27
***

Avec 7% de la prod natio, derrière Anzin et Lens et leur 8%. Excellente santé financière. C'est la compagnie houillère du Nord qui a distribué les plus gros dividendes, eu égard à l'importance de son extraction1.
## Sources