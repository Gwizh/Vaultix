Titre : [[Le passage à la lampe électrique ne se fait en partie pas en 1919 à Courrières car la lampisterie n'a pas été détruite]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lampes de mines]]
Date : 2024-03-27
***

"Ils minimisent les dépenses d'investissement liées au travail : la lampisterie ayant été épargnée par les destructions, les lampes de sûreté à flamme et à benzine sont remises en usage alors que les compagnies dont la lampisterie a été détruite, investissent dans l'achat de lampes électriques portatives."
## Sources
[[Une entreprise face à la gestion de « risques majeurs »]]