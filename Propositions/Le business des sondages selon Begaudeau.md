Titre : [[Le business des sondages selon Begaudeau]]
Type : Proposition
Tags : #Proposition
Ascendants : [[François Bégaudeau]], [[Sondages]]
Date : 2023-12-10
***

Le nombre de sondage publié l'année de la présidentielle est passé de 120 à 560 entre 2002 et 2017. Bégaudeau parle de produits, à la fois pour les sondages que pour l'élection et son traitement. Avant, les élections étaient commentées un mois avant, maintenant c'est plutôt 8-9. C'est du business. Bégaudeau pense qu'il faut pas seulement faire de la pédagogie dessus, il faut le mettre hors d'état de nuire. Et pour ça, il faut mettre hors d'état de nuire le système qu'il appelle totalitarisme marchand. La politique devient marchandise.
#MIE Je n'ai en effet jamais aimé les sondages mdr
## Sources
[[Les sondages sont-ils dangereux pour la démocratie]]