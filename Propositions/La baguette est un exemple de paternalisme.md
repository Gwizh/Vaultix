Titre : [[La baguette est un exemple de paternalisme]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Paternalisme]]
Date : 2023-11-18
***

La baguette a été inventée pour que les ouvriers n’aient pas à apporter des couteaux sur leur lieu de travail. C’était donc pour les protéger entre elleux en partant du principe qu’iels ne pouvaient pas ne pas se battre et faire leur travail sans violence.

## Sources