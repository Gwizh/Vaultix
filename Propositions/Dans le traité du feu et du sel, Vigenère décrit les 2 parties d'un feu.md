Titre : [[Dans le traité du feu et du sel, Vigenère décrit les 2 parties d'un feu]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Gaston Bachelard]]
Date : 2024-01-21
***

Il voit deux flammes, l'une blanche qui éclaire et l'autre rouge qui brule. Bachelard dit que la lumière est alors sur-valorisation du feu. L'éclairage (blanc) est une conquête de la flamme vulgaire, dont la couleur et l'intensité changent. La flamme blanche est non-corrompue.
## Sources
[[La flamme d'une chandelle]]