Titre : [[Corbel considère que rien ne garantit que la technologie la plus performante s'impose]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Déterminisme technologique]]
Date : 2024-01-10
***

Les économistes évolutionnistes parlent d'externalités du réseau avec l'exemple du clavier Qwerty. [David 1985] et l'accroissement de l'utilité d'une techno en fct du nb d'utilisateur.

Les sociologues regardent aussi la nécessité d'avoir le soutien des acteurs clés du système [Akrich 1988]. En effet, outre les nouveaux utilisateurs et promoteurs, les acteurs de l'ancienne technologie sont tout autant importants. Le passage avec l'ancienne technologie peut mener à des négociations pour garder les mêmes normes, les mêmes utilisations (comme le DVD qui est compatible avec les CD ROM).
## Sources