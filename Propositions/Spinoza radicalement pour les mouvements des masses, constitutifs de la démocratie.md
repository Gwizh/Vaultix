Titre : [[Spinoza radicalement pour les mouvements des masses, constitutifs de la démocratie]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Spinoza]], [[Thomas Hobbes]], [[Démocratie]]
Date : 2023-12-09
***

Contrairement aux visions de l'époque (théologie de Hobbes) la plèbe pour Spinoza constitue un mouvement démocratique en soi, passionnel par nature. Ensuite, le caractère démocratique ou despotique des institutions vient de la conquête ou non de la liberté commune.
## Sources
[[Spinoza et les passions du social]]