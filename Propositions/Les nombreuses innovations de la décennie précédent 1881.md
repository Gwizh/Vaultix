Titre : [[Les nombreuses innovations de la décennie précédent 1881]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Électricité]]
Date : 2024-01-28
***

1871 : Dynamo de Gramme
1873 : Transport d'électricité par Fontaine
1875 : régulateur pour lampes à arc de Brusch
1876 : Bougie de Joblochkoff
1879 : Lampes incandescentes de Edison et Swan
1879 : Siemens et la locomotive électrique
## Sources
[[La fée et la servante]]