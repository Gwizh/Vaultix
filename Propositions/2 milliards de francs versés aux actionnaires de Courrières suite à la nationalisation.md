Titre : [[2 milliards de francs versés aux actionnaires de Courrières suite à la nationalisation]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Finance]], [[Courrières]]
Date : 2024-03-27
***

"La Seconde Guerre mondiale se traduit pour les mineurs de Courrières et du Nord-Pas-de-Calais par une régression considérable : allongement de la durée de travail sans augmentation de salaire, dégradation des conditions de travail et de vie, arrestations suite à la grève du 28 mai au 7 juin 1941 83... Les mineurs devront attendre la fin de la « bataille du charbon » pour connaître des lendemains meilleurs. Entre temps, l'histoire de la compagnie des mines de Courrières, en tant que société privée, s'est achevée par l'ordonnance du 13 décembre 1944 réquisitionnant les houillères du Nord et du Pas-de-Calais. La loi du 17 mai 1946, portant nationalisation de la quasi-totalité des mines de combustibles minéraux solides, intègre la compagnie au sein du groupe Hénin-Liétard
du bassin du N.P.C. À cette occasion, le montant des obligations de Charbonnages de France versées aux actionnaires de la société en dédommagement de la nationalisation fournit une dernière information sur sa santé financière : avec près de 2 milliards de francs, il s'agit du 4e montant versé aux actionnaires des anciennes sociétés privées du bassin du Nord-Pas-de-Calais 84."
## Sources
[[Une entreprise face à la gestion de « risques majeurs »]]