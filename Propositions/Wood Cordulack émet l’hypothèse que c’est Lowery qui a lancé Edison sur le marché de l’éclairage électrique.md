Titre : [[Wood Cordulack émet l’hypothèse que c’est Lowery qui a lancé Edison sur le marché de l’éclairage électrique]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Thomas Edison]], [[Lumière électrique]]
Date : 2024-01-28
***

Visitant l’Exposition en 1878, Grosvenor P. Lowery, un avocat américain spécialisé en propriété industrielle, fut captivé par l’installation de lumière à l’arc de Jablochkoff [[33]La bougie de Jablochkoff, inventée en 1876 par le Parisien…](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no33) et envoya un dossier sur le sujet à son ami Thomas Edison, avec l’espoir d’inspirer l’inventeur et de susciter une nouvelle expérience pour une application pratique de l’éclairage électrique.
## Sources
[[Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité]]