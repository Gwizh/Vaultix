Titre : [[Quelques chiffres de l'Exposition de 1881 de Beltran et Carré]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Exposition internationale d'Électricité de Paris en 1881]]
Date : 2024-01-28
***

Du 10/08 au 20/11, 900 000 visiteurs sont allés au Palais de l'Industrie.
1768 exposants, 16 pays (55% des exposants étaient français)
## Sources
[[La fée et la servante]]