Titre : [[Marshall Sahlins et sa remise en question du concept de rareté]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Marshall Sahlins]]
Date : 2023-12-13
***

Pour lui, la rareté est un concept de l'économie classique. Selon lui et les preuves ethnographiques, l'économie primitive n'était pas de subsistance. Nous avons connu des situations d'abondance sans capitalismes ni féodalisme. L'économie ne tournait pas alors sur la rareté. Mais il est donc avec Sartre que le ration population ressource compte.
## Sources
[[Spinoza et les passions du social]]