Titre : [[Les lampes Argand, révolution de l'éclairage par la mise en pratique de la théorie de Lavoisier]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Éclairage au gaz]]
Date : 2024-01-11
***

Les lampes d'Argand de 1783 ont été une révolution par l'utilisation d'une mèche plate en prenant en compte de notre compréhension nouvelle de l'importance de l'air dans la combustion. C'était une lumière très vive qui ne produisait pas de fumée.
Le cylindre de verre protégeait la flamme et un système permettait de changer l'intensité de la lumière !
## Sources
[[La Nuit Désenchantée]]