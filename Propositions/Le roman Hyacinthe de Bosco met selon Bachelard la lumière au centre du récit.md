Titre : [[Le roman Hyacinthe de Bosco met selon Bachelard la lumière au centre du récit]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Gaston Bachelard]], [[Henri Bosco]]
Date : 2024-01-21
***

Bosco évoque une lampe dès la première page. Il parle d'une lampe lointaine, une lampe d'un autre. C'est génial pour Bachelard qui voit toutes les implications d'une lampe d'autrui. Est-ce pour le soir ou le matin ? La lampe dérange le narrateur, elle veille sur cet autre mais surveille le narrateur.
## Sources
[[La flamme d'une chandelle]]