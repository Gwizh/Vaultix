Titre : [[Comprendre l'État avec Spinoza]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Spinoza]], [[État]]
Date : 2023-12-11
***

La souveraineté ou l'État est la puissance de la multitude. Elle n'est pas rationnelle mais affective. On ne peut donc pas chercher et expliquer l'État par la Raison mais par les lois de la nature (pourquoi on en vient à constituer ces institutions).

## Sources
[[Spinoza et les passions du social]]