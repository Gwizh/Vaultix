Titre : [[Résumé de l’ascension d’Edison à l’aube des années 1880 par Wood Cordulack]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Thomas Edison]]
Date : 2024-01-28
***

L’évènement étant bien couvert par la presse, les Français furent donc ainsi presque immédiatement informés des prouesses électriques américaines [[23]](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no23) [Edmond de Goncourt publia son roman Les Frères Zemganno…](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no23) . Edison présenta son système pour la lampe incandescente à l’Exposition internationale d’électricité à Paris de 1881, dont son exposition fut l’attraction principale

[[24]Pour une discussion sur la manipulation de la presse et de…](https://www.cairn.info/revue-annales-historiques-de-l-electricite-2006-1-page-7.htm#no24)

La même exposition avait aussi en vitrine une galerie de peintures illuminées par des lampes à incandescence, donnant lieu à une gravure qui parut dans _La Lumière électrique_ (1881).
## Sources
[[Une bataille de rayons franco-américaine. L’électricité ou comment vendre la modernité]]