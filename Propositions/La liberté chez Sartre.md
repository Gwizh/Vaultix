Titre : [[La liberté chez Sartre]]
Type : Proposition
Tags : #Proposition
Ascendants : [[Jean-Paul Sartre]], [[Liberté]]
Date : 2023-12-13
***

La liberté est cardinale pour la philosophie, même pour les déterministes radicaux. Sartre ne parle pas de déterminisme mais admet un "destin social", l'individu reste un projet libre. La liberté se trouve dans la marge entre les effets du déterminisme sur l'individu et ce qu'il va vraiment faire un retour. Les hommes sont fait par et font l'histoire.
## Sources
[[Spinoza et les passions du social]]