Titre : [[Se coucher tard est un marqueur social fort de la société bourgeoise du XIXe]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Lumière électrique]], [[Bourgeoisie]]
Date : 2024-01-30
***

Lorsque le travail s'est détaché du cycle du jour, les bourgeois aimaient rester plus tard pour faire la fête ou s'amuser. Le fait de se lever tard était très bien vu. Toujours aujourd'hui on parle des gens qui se lavent tôt comme les laborieux.
p114 -> les heures se décalent !
## Sources
[[La Nuit Désenchantée]]