---
title: Les hormones : mythes et réalités - Regarder le documentaire complet
authors: 
year: 
---

Lien : https://www.arte.tv/fr/videos/096279-000-A/les-hormones-mythes-et-realites/

Description : Quelle est l’influence réelle des hormones sur le genre ? Ce dernier n’est-il pas plus encore déterminé par les normes sociales ? S’appuyant sur des recherches récentes, ce documentaire interroge la part de nature et celle de culture dans nos comportements.
