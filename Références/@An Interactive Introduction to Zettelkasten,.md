---
title: An Interactive Introduction to Zettelkasten
authors: 
year: 
---

Lien : https://binnyva.com/zettelkasten/

Description : Zettelkasten is a note taking system that will supercharge your learning process. It will give you an external place to store your knowledge. This tutorial is a introduction to the system in a practical way. The idea is to get you to practice the process and giving you all the tools necessary to start your own Zettelkasten system.
