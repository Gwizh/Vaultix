---
title: Understanding note-taking | Zettelkasten
authors: 
year: 2021
---

Lien : https://www.youtube.com/watch?v=-r6fnC5lVfE

Description : In this video I'll talk about a Zettelkasten note-taking system, invented by Niklas Luhmann and why it works, so that you can take inspiration from these principles and incorporate them into your own workflow. 

OUTLINE:
0:00 - Introduction
2:13 - What is Zettelkasten?
4:01 - Emergent
5:55 - Dynamic
6:23 - Personalized
7:12 - Interlinking
8:29 - Externalization
8:55 - Clarity
10:40 - Investment in future
11:52 - Conclusion

Socials:
VK: https://vk.com/atpsynthase
