---
title: Prendre de bonnes notes dans OBSIDIAN - ZETTELKASTEN dans obsidian
authors: 
year: 2021
---

Lien : https://www.youtube.com/watch?v=Zawx-N3-iko

Description : Dans cette vidéo, je vais vous montrer comment je passe des notes que je prends sur des contenus à des notes permanentes, densément liées entre elles et reliées par des cartes de contenu dans obsidian. 
C'est en fait la méthode ZETTELKASTEN appliquée à obsidian.
Nous allons voir : 
00:00 - Prendre de bonnes notes dans OBSIDIAN - ZETTELKASTEN dans obsidian
00:27 - Première phase de Zettelkasten dans Obsidian (notes littéraires)
02:18 - Note refactor pour faciliter Zettelkasten dans Obsidian
07:27 - Deuxième phase de Zettelkasten dans Obsidian (notes permanentes)
11:40 - Troisième phase de Zettelkasten dans Obsidian (les notes du jour)
13:26 - Quatrième phase de Zettelkasten dans Obsidian (les cartes de contenus, les sommaires)
16:53 - Zettelkasten dans Obsidian (ma formation Atomic Thinking)

🌟 Découvrez mon système de prise de notes et d'organisation (1h30 de vidéos exclusives gratuites) : https://school.atomicthinking.fr/iat

___________
MON ÉCOLE DE FORMATION :
J'ai créé mon école de formations en ligne "Atomic Thinking", dans laquelle je pense et transmets des systèmes innovants pour gérer vos connaissances, vous organiser et mener des projets.

Si vous voulez découvrir les méthodes que j'ai créées, cliquez sur le lien qui vous décrit le mieux :

🤓 Atomic Knowledge
Problème ciblé : J'oublie tout ce que j'apprends. Je consomme beaucoup de contenu, mais j'ai l'impression de tout oublier.
👉 Créez votre second cerveau numérique qui centralise tout ce que lisez, voyez et entendez et qui vous permet de monter des projets, vous instruire et vous organiser sans effort.
[https://fr.atomicthinking.fr/knowledge](https://fr.atomicthinking.fr/knowledge)

🎨 Atomic Creator
Problème ciblé : La création est longue et fastidieuse. J'en ai marre des longues heures de recherche et de blocage devant la page blanche.
👉 Apprenez à utiliser le nouveau système de création de contenu, qui vous permet de créer plus rapidement des contenus riches et pertinents sans effort et de faire grandir votre audience.
[https://fr.atomicthinking.fr/creator](https://fr.atomicthinking.fr/creator)

⏰ Atomic Timing
Problème ciblé : Je n'arrive pas à être productif. Je me sens dispersé, je n'arrive pas à me concentrer et à planifier mes journées.
👉 Le système qui supporte votre créativité et votre pensée, qui oriente vos actions vers votre mission et vous permet de vivre une vie à la hauteur de vos ambitions.
[https://fr.atomicthinking.fr/timing](https://fr.atomicthinking.fr/timing)

👨‍🎓 Atomic Student
Problème ciblé : Je suis étudiant. J'ai du mal à organiser mes cours, à être productif et à produire de bons devoirs sans y passer 12 heures.
👉 Créez votre second cerveau numérique pour mieux gérer votre vie en tant qu’étudiant, de la mémorisation à l'écriture de mémoires en passant par la prise de notes, l'organisation et la productivité.
[https://fr.atomicthinking.fr/student](https://fr.atomicthinking.fr/student)

---

LIENS PRATIQUES :
📗 Mon livre sur la prise de notes : [https://www.eliottmeunier.com/livre](https://www.eliottmeunier.com/livre)
💻 Mon blog : [https://eliottmeunier.com/](https://eliottmeunier.com/)
😏 Mon instagram : [  / eliott.meunier  ](  / eliott.meunier  ) 🌟 Découvrez toutes mes formations, sur la prise de notes, la productivité et le marketing ici : https://fr.atomicthinking.fr/
