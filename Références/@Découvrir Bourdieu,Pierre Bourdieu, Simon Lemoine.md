---
title: Découvrir Bourdieu
authors: Pierre Bourdieu, Simon Lemoine
year: 2020
---

Lien : 

Description : Les concepts de Pierre Bourdieu font école tant et si bien qu'au-delà même de la sociologie critique, on les retrouve dans la littérature journalistique, dans les textes d'organisations, dans les discours politiques... Le livre de Simon Lemoine, docteur en philosophie, professeur de lycée, veut éclairer pour un public non savant les articulations principales d'une pensée qui s'est vouée à la connaissance des mouvements de la société. Comment les dominants dominent-ils ? La sociologie peut-elle servir à transformer les rapports de pouvoir, quelle est la puissance de la violence symbolique, qu'est-ce que l'habitus, la distinction, comment fonctionne la reproduction sociale ? Autant de questions qui animent ce "Découvrir" et en font une introduction essentielle à la sociologie de Bourdieu, et à la compréhension du présent
