---
title: Introduction to the Zettelkasten Method
authors:  sascha
year: 
---

Lien : https://zettelkasten.de/introduction/

Description : Learn how the Zettelkasten works as a system, what a Zettel is made of, and how to grow an organic web of knowledge.
