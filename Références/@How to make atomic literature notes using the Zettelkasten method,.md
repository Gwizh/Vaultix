---
title: How to make atomic literature notes using the Zettelkasten method
authors: 
year: 2021
---

Lien : https://meda.io/how-to-make-atomic-literature-notes/

Description : Find out how to make your Zettelkasten literature notes a single idea when the book you’re reading contains many different ideas.
