---
title: Quoi de neuf? du rôle des techniques dans l'histoire globale
authors: 
year: 2013
---

Lien : 

Description : Voici un essai capital sur les rapports entre technique et société. Cette histoire globale de la technologie moderne prend le contre-pied du récit habituel centré sur les inventions de quelques individus géniaux pour mettre au premier plan l'analyse des usages collectifs. David Edgerton est ainsi amené à réévaluer profondément le rôle des technologies dans la société. Bien au-delà de la liste usuelle des innovations modernes couramment tenues pour avoir transformé notre existence - la pilule, l'informatique, la bombe atomique, l'aviation -, il faut prendre en compte une grande variété de technologies moins visibles mais non moins importantes, venant de diverses parties du monde ... (Source : 4e de couv.)
