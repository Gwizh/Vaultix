---
title: Le parti polymorphe en Europe de l'Est
authors: Thomas Lowit
year: 1979
---

Lien : https://www.persee.fr/doc/rfsp_0035-2950_1979_num_29_4_418649

Description : THE POLYMORPHOUS PARTY IN EASTERN EUROPE, by THOMAS LOWIT It is frequently presumed that the regimes in Eastern Europe include many various institutions. This is wrong, however; the administrative, economic and social organisations of the state, seemingly functionally differentiated, are merely " branches " of the one basic institution, the Party. The notion of the " polymorphous party " is a useful concept for the understanding of the system with all its dysfunctions and crises.
