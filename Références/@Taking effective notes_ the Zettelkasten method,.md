---
title: Taking effective notes: the Zettelkasten method
authors: 
year: 2020
---

Lien : https://gianmarcodavid.com/posts/zettelkasten/

Description : A Personal Knowledge Management system that helps stimulate understanding, remember the content you consume, and produce your own. These are the main reasons that led me to adopt the Zettelkasten.
