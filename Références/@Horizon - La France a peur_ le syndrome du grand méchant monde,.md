---
title: Horizon - La France a peur: le syndrome du grand méchant monde
authors: 
year: 2014
---

Lien : https://www.youtube.com/watch?v=8WiiqssAME4

Description : Pour nous soutenir : https://fr.tipeee.com/hacking-social
https://liberapay.com/Hacking-social/
https://www.paypal.com/donate/?hosted...

Horizon - Épisode 3
La France a peur: le syndrome du grand méchant monde

Criminalité, délinquance, violence, maladie, immigration, chômage... Il semble qu'on ait toutes les raisons d'avoir peur! Mais d'où vient ce sentiment de danger, d'insécurité? De notre propre expérience?
"Le syndrome du grand méchant monde" est une expression forgée par George Gerbner, expression mettant en avant l'idée que le contenu homogène et répété des images à la télévision augmente notre méfiance, engendre de la peur. 
Dans cet épisode, Gull se propose d'étudier cette hypothèse en se recentrant sur les journaux télé et autres reportages.

Nous avons indiqué nos sources et références utilisées dans la vidéo en bas de page de l'article de la vidéo: https://www.hacking-social.com/2014/1...


Pour aller plus loin, n'hésitez pas à faire un tour sur le blog du hacking social où vous retrouverez tous nos articles: 
www.hacking-social.com
