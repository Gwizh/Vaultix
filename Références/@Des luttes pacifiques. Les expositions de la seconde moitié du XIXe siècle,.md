---
title: Des luttes pacifiques. Les expositions de la seconde moitié du XIXe siècle
authors: 
year: 
---

Lien : https://madparis.fr/des-luttes-pacifiques-les-expositions-de-la-seconde-moitie-du-xixe-siecle

Description : Les origines de l’Union centrale et ses premières années d’existence sont intimement liées aux expositions contemporaines : celles de 1851 à Londres et de 1855 à Paris, mais également celles organisées par la Société du progrès de l’art industriel en 1861 et 1863, puis celles qui en sont les héritières. Qu’elles soient universelles, internationales ou françaises, les expositions constituent dans la seconde moitié du XIXe siècle autant de moments fondateurs pour l’identité et les collections de l’institution. Ces luttes pacifiques, selon l’oxymore fréquemment employé par les commentateurs et les écrivains de (...)
