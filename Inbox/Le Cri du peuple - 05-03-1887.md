Titre : [[Le Cri du peuple - 05-03-1887]]
Type : Source
Tags : #Source
Ascendants : [[Le Cri du Peuple]]
Date : 2024-04-01
***

https://gallica.bnf.fr/ark:/12148/bpt6k4682359q/f2.item.zoom
## Propositions
### Marcel Rondet fait un discours le 3 mars 1887 pour dénoncer la Compagnie par rapport au drame du Châtelus
[[Michel Rondet fait un discours le 3 mars 1887 pour dénoncer la Compagnie par rapport au drame du Châtelus]]
Dans le passage "Défenseur de Maheu"

### Le Cri du Peuple note que le 3 mars que la Compagnie met la catastrophe du Châtelus sur le compte des tremblements de terre du Midi
[[Le Cri du Peuple note que le 3 mars que la Compagnie met la catastrophe du Châtelus sur le compte des tremblements de terre du Midi]]

### Basly fait un discours le 3 mars 1887 pour valoriser le projet de loi des délégués mineurs
[[Basly fait un discours le 3 mars 1887 pour valoriser le projet de loi des délégués mineurs]]