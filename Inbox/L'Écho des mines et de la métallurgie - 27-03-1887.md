Titre : [[L'Écho des mines et de la métallurgie - 27-03-1887]]
Type : Source
Tags : #Source
Ascendants :
Date : 2024-04-01
***

https://gallica.bnf.fr/ark:/12148/bpt6k57403210/f3.image
## Propositions

### Francis Laur compte en 1887 s'appuyer sur la catastrophe du Châtelus pour faire passer une loi interdisant le tirage à la poudre
[[Francis Laur compte en 1887 s'appuyer sur la catastrophe du Châtelus pour faire passer une loi interdisant le tirage à la poudre]]