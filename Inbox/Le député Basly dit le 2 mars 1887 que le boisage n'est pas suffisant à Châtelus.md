Titre : [[Le député Basly dit le 2 mars 1887 que le boisage n'est pas suffisant à Châtelus]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Coup de grisou au Puits Châtelus le 1er mars 1887]], [[Émile Basly]]
Date : 2024-04-01
***

## Sources
[[Le Cri du peuple - 04-03-1887]]