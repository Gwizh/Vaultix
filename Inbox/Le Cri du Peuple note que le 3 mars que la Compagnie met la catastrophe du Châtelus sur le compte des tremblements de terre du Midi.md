Titre : [[Le Cri du Peuple note que le 3 mars que la Compagnie met la catastrophe du Châtelus sur le compte des tremblements de terre du Midi]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Coup de grisou au Puits Châtelus le 1er mars 1887]], [[Le Cri du Peuple]]
Date : 2024-04-01
***

## Sources
[[Le Cri du peuple - 05-03-1887]]