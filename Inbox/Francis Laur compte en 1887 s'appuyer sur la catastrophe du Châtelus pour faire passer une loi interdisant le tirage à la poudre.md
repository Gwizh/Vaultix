Titre : [[Francis Laur compte en 1887 s'appuyer sur la catastrophe du Châtelus pour faire passer une loi interdisant le tirage à la poudre]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Francis Laur]], [[Coup de grisou au Puits Châtelus le 1er mars 1887]]
Date : 2024-04-01
***

## Sources
[[L'Écho des mines et de la métallurgie - 27-03-1887]]