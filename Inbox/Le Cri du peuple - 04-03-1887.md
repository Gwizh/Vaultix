Titre : [[Le Cri du peuple - 04-03-1887]]
Type : Source
Tags : #Source
Ascendants : [[Le Cri du Peuple]]
Date : 2024-04-01
***

## Propositions

### Le 4 mars 1887, il y a plusieurs erreurs dans le compte rendu du Cri du Peuple du drame du Châtelus
[[Le 4 mars 1887, il y a plusieurs erreurs dans le compte rendu du Cri du Peuple du drame du Châtelus]]

### Le député Basly dit le 2 mars 1887 que le boisage n'est pas suffisant à Châtelus
[[Le député Basly dit le 2 mars 1887 que le boisage n'est pas suffisant à Châtelus]]

### Le sous-gouverneur Badon a été retrouvé blessé au Châtelus le 2 mars 1887
[[Le sous-gouverneur Badon a été retrouvé blessé au Châtelus le 2 mars 1887]]
