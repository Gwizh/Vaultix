Titre : [[Visite musée de la mine Saint-Étienne]]
Type : Source
Tags : #Source
Ascendants :
Date : 2024-04-07
***

## Propositions
On visite avant tout le Puit Curiot à travers la journée typique d'un mineur (l'accueil du musée est l'accueil par lequel passaient les mineurs)

Curiot ouvert de 1919 à 1973 et ensuite ouverture du musée en 1991.
=> Ne correspond pas à Germinal

Salle des pendus : faire sécher les vêtements en gagnant le plus de place. Vêtements pendus avec des chaînes numérotées.
Des mineurs étaient gardes-lavabos, lavaient les vêtements, s'occupaient des chaudières des douches.
70 douches car 70 mineurs allaient à la fois dans la mine.

Voir le film "Le Brasier"

Douche à poids, voir les notes, c'est un poids qu'on laisser tomber qui lance l'eau, système économqiue

1950 -> lampes élec à batterie 1.5kg

Le couloir avant la lampisterie était appelée l'allée des cigarettes parce que c'était le dernier endroit où les mineurs étaient autorisés de miner.

Pointer avec les jetons

Les journées de travail étaient découpées en 3 : matin (6 à 14h), aprem (14 à 22h) et nuit (22 à 6h)

Femmes seulement à la surface par exemple clappeuse ou lampiste.

Le tri du charbon était nécessaire, on se servait des déchets pour remblayer les mines. Mais après quand on faisait écrouler les étages au fur et à mesure on avait plus besoin de cette technique donc on a fait des tas de déchets (Crassiers). Mais les gros tas ne sont pas si importants -> seulement 4% de l'extraction de 1930 à 1960.

Grandes cuves pour récupérer l'eau qu'on pompait.

727m de profondeur descendu en 1 minute dans l'ascenseur. Dans le musée c'était qu'une ruse, on ne descend en fait que de 7m. C'est une reconstruction du puit du XXe
à 727m de prof, il fait 45°C

Les galeries faisaient plusieurs km de long et il n'y avait pas de trains partout donc on marchait bcp.

