Titre : [[Des lampes Mueseler à fermeture ordinaire étaient utilisée à Châtelus dans les années 1880s]]
Type : Proposition
Tags : #Proposition #Fixette 
Ascendants : [[Coup de grisou au Puits Châtelus le 1er mars 1887]], [[Lampes de mines]]
Date : 2024-04-01
***

## Sources
[[L'Écho des mines et de la métallurgie - 13-03-1887]]