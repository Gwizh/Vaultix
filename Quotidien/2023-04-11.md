Date : 2023-04-11
***

# Idées 
LES MANIFESTANTS RENNAIS OBTIENNENT LA DÉFAITE PROVISOIRE DE CLEAR CHANNEL. Clear Channel (devenu groupe iHeartMedia) est une entreprise spécialisée dans le mobilier urbain publicitaire. Régulièrement ciblées durant les mobilisations, les vitres de ses 70 panneaux n’ont pas survécu à la détermination des manifestants rennais, bien décidés à installer un "Adblock IRL" dans la ville. On apprend dans un article de France Bleu Armorique que : « Clear Channel, qui possède la concession de ce mobilier urbain dans la Métropole rennaise jusqu’en 2025, estime le coût à plus de 80.000 euros pour le seul mois de mars.